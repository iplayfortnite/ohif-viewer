import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import ConnectedViewerRetrieveStudyData from '../connectedComponents/ConnectedViewerRetrieveStudyData';
import useServer from '../customHooks/useServer';
import { default as OHIF, utils as CoreUtils } from '@ohif/core';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import {
  filteredFiles,
  getProcessesFileData,
} from '@ohif/core/src/hanging-protocols/ProtocolEngineUtils';
const { setLayout } = OHIF.redux.actions;

const { urlUtil: UrlUtil } = CoreUtils;
const { isImageOpenedFromSession, isCurrentWebImage } = FlywheelCommonUtils;

/**
 * It returns an array with any existing segmentation file names from query params
 * @param {Object} queryString query string object
 * @return {Array<string> | undefined}
 */
const getSegmentationFileNames = (queryString = {}) => {
  const segmentationFileNamesStr = queryString.segmentations;
  return (
    (segmentationFileNamesStr && segmentationFileNamesStr.split(',')) ||
    undefined
  );
};
const ThumbnailsService = {
  prepareToDisplay: displaySet => {
    return ThumbnailsService.filterDisplaySet(displaySet);
  },
  shouldSortDisplaySet: false,
  filterDisplaySet: displaySets => {
    return displaySets.filter(displaySet => {
      return !displaySet.temporalSet;
    });
  },
};
function FileViewerRouting({ match: routeMatch, location: routeLocation }) {
  const {
    project,
    location,
    dataset,
    dicomStore,
    containerId,
    fileName,
  } = routeMatch.params;

  const [containerIds, setContainerIds] = useState([]);
  const [selectedFileNames, setSelectedFileNames] = useState([]);
  const [files, setFiles] = useState([]);

  const queryFilters = UrlUtil.queryString.getQueryFilters(routeLocation);
  const segmentationFileNames = getSegmentationFileNames(queryFilters);

  const server = useServer({
    project,
    location,
    containerId,
  });
  const dispatch = useDispatch();
  const numOfViewports = FlywheelCommonUtils.isCurrentWebImage() ? 1 : 3;
  const projectConfig = useSelector(store => {
    return store?.flywheel?.projectConfig;
  });

  useEffect(() => {
    dispatch(
      setLayout({
        numRows: 1,
        numColumns: numOfViewports,
        viewports: new Array(numOfViewports).fill({
          height: '100%',
          width: '100%',
        }),
      })
    );
  }, []);

  useEffect(() => {
    if (isCurrentWebImage() && isImageOpenedFromSession(routeMatch.url)) {
      filteredFiles(projectConfig, containerId).then(selectedFiles => {
        setFiles(selectedFiles);
      });
    }
  }, [projectConfig]);

  useEffect(() => {
    const { fileNames, containerIds } = getProcessesFileData(files);
    setSelectedFileNames(fileNames);
    setContainerIds(containerIds);
  }, [files]);

  const isMultiSessionViewEnabled = useSelector(store => {
    return store?.multiSessionData?.isMultiSessionViewEnabled;
  });
  const acquisitionData = useSelector(store => {
    return store?.multiSessionData?.acquisitionData;
  });

  let fileNames = [];
  let containers = [];

  if (isMultiSessionViewEnabled) {
    acquisitionData.forEach(data => {
      fileNames.push(data.studyUid), containers.push(data.acquisitionId);
    });
  } else {
    fileNames = fileName ? [fileName] : selectedFileNames;
    containers = containerIds.length > 0 ? containerIds : [containerId];
  }

  if (server) {
    return (
      <ConnectedViewerRetrieveStudyData
        studyInstanceUIDs={[]}
        seriesInstanceUIDs={[]}
        fileNames={fileNames}
        containerIds={containers}
        segmentationFileNames={segmentationFileNames}
        thumbnailsService={ThumbnailsService}
      />
    );
  }

  return null;
}

FileViewerRouting.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      dataset: PropTypes.string,
      dicomStore: PropTypes.string,
      location: PropTypes.string,
      project: PropTypes.string,
    }),
  }),
};

export default FileViewerRouting;
