import OHIF from '@ohif/core';
import { asyncComponent, retryImport } from '@ohif/ui';

import { Components as FlywheelComponents } from '@flywheel/extension-flywheel';

const { ProjectList: FlywheelProjectList, ErrorPage } = FlywheelComponents;

const { urlUtil: UrlUtil } = OHIF.utils;

// Dynamic Import Routes (CodeSplitting)
const IHEInvokeImageDisplay = asyncComponent(() =>
  retryImport(() =>
    import(
      /* webpackChunkName: "IHEInvokeImageDisplay" */ './IHEInvokeImageDisplay.js'
    )
  )
);
const ViewerRouting = asyncComponent(() =>
  retryImport(() =>
    import(/* webpackChunkName: "ViewerRouting" */ './ViewerRouting.js')
  )
);

const FileViewerRouting = asyncComponent(() =>
  import(/* webpackChunkName: "FileViewerRouting" */ './FileViewerRouting.js')
);

const StudyListRouting = asyncComponent(() =>
  retryImport(() =>
    import(
      /* webpackChunkName: "StudyListRouting" */ '../studylist/StudyListRouting.js'
    )
  )
);
const StandaloneRouting = asyncComponent(() =>
  retryImport(() =>
    import(
      /* webpackChunkName: "ConnectedStandaloneRouting" */ '../connectedComponents/ConnectedStandaloneRouting.js'
    )
  )
);
const ViewerLocalFileData = asyncComponent(() =>
  retryImport(() =>
    import(
      /* webpackChunkName: "ViewerLocalFileData" */ '../connectedComponents/ViewerLocalFileData.js'
    )
  )
);

const reload = () => window.location.reload();

const ROUTES_DEF = {
  default: {
    viewer: {
      path: '/viewer/:studyInstanceUIDs',
      component: ViewerRouting,
    },
    standaloneViewer: {
      path: '/viewer',
      component: StandaloneRouting,
    },
    list: {
      path: ['/'],
      component: FlywheelProjectList,
    },
    local: {
      path: '/local',
      component: ViewerLocalFileData,
    },
    IHEInvokeImageDisplay: {
      path: '/IHEInvokeImageDisplay',
      component: IHEInvokeImageDisplay,
    },
  },
  gcloud: {
    viewer: {
      path:
        '/projects/:project/locations/:location/datasets/:dataset/dicomStores/:dicomStore/study/:studyInstanceUIDs',
      component: ViewerRouting,
      condition: appConfig => {
        return !!appConfig.enableGoogleCloudAdapter;
      },
    },
    list: {
      path:
        '/projects/:project/locations/:location/datasets/:dataset/dicomStores/:dicomStore',
      component: StudyListRouting,
      condition: appConfig => {
        const showList = appConfig.showStudyList;

        return showList && !!appConfig.enableGoogleCloudAdapter;
      },
    },
  },
  flywheel: {
    viewer: {
      path: [
        '/project/:projectId/viewer/:studyInstanceUIDs\\?taskId=:taskId&slice=:slice&labels=:labels&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/viewer/:studyInstanceUIDs\\?taskId=:taskId&slice=:slice&labels=:labels',
        '/project/:projectId/viewer/:studyInstanceUIDs\\?taskId=:taskId&slice=:slice&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/viewer/:studyInstanceUIDs\\?taskId=:taskId&slice=:slice',
        '/project/:projectId/viewer/:studyInstanceUIDs\\?taskId=:taskId&labels=:labels&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/viewer/:studyInstanceUIDs\\?taskId=:taskId&labels=:labels',
        '/project/:projectId/viewer/:studyInstanceUIDs\\?taskId=:taskId&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/viewer/:studyInstanceUIDs\\?taskId=:taskId',
        '/project/:projectId/viewer/:studyInstanceUIDs\\?slice=:slice&labels=:labels',
        '/project/:projectId/viewer/:studyInstanceUIDs\\?slice=:slice',
        '/project/:projectId/viewer/:studyInstanceUIDs\\?labels=:labels',
        '/project/:projectId/viewer/:studyInstanceUIDs',
        '/project/:projectId/viewer/:studyInstanceUIDs/:seriesInstanceUIDs\\?taskId=:taskId&slice=:slice&labels=:labels&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/viewer/:studyInstanceUIDs/:seriesInstanceUIDs\\?taskId=:taskId&slice=:slice&labels=:labels',
        '/project/:projectId/viewer/:studyInstanceUIDs/:seriesInstanceUIDs\\?taskId=:taskId&slice=:slice&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/viewer/:studyInstanceUIDs/:seriesInstanceUIDs\\?taskId=:taskId&slice=:slice',
        '/project/:projectId/viewer/:studyInstanceUIDs/:seriesInstanceUIDs\\?taskId=:taskId&labels=:labels&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/viewer/:studyInstanceUIDs/:seriesInstanceUIDs\\?taskId=:taskId&labels=:labels',
        '/project/:projectId/viewer/:studyInstanceUIDs/:seriesInstanceUIDs\\?taskId=:taskId&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/viewer/:studyInstanceUIDs/:seriesInstanceUIDs\\?taskId=:taskId',
        '/project/:projectId/viewer/:studyInstanceUIDs/:seriesInstanceUIDs\\?slice=:slice&labels=:labels',
        '/project/:projectId/viewer/:studyInstanceUIDs/:seriesInstanceUIDs\\?slice=:slice',
        '/project/:projectId/viewer/:studyInstanceUIDs/:seriesInstanceUIDs\\?labels=:labels',
        '/project/:projectId/viewer/:studyInstanceUIDs/:seriesInstanceUIDs',
      ],
      component: ViewerRouting,
    },
    fileViewer: {
      path: [
        '/project/:projectId/(nifti|metaio)/:containerId/:fileName\\?taskId=:taskId&slice=:slice&labels=:label&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/(nifti|metaio)/:containerId/:fileName\\?taskId=:taskId&slice=:slice&labels=:label',
        '/project/:projectId/(nifti|metaio)/:containerId/:fileName\\?taskId=:taskId&slice=:slice&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/(nifti|metaio)/:containerId/:fileName\\?taskId=:taskId&slice=:slice',
        '/project/:projectId/(nifti|metaio)/:containerId/:fileName\\?taskId=:taskId&labels=:labels&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/(nifti|metaio)/:containerId/:fileName\\?taskId=:taskId&labels=:labels',
        '/project/:projectId/(nifti|metaio|image|tiff)/:containerId/:fileName\\?taskId=:taskId&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/(nifti|metaio|image|tiff)/:containerId/:fileName\\?taskId=:taskId',
        '/project/:projectId/(nifti|metaio|image|tiff)/:containerId/:fileName\\?slice=:slice&labels=:labels',
        '/project/:projectId/(nifti|metaio)/:containerId/:fileName\\?slice=:slice',
        '/project/:projectId/(nifti|metaio)/:containerId/:fileName\\?labels=:labels',
        '/project/:projectId/(nifti|metaio|image|tiff)/:containerId/:fileName',
        '/project/:projectId/image/:containerId',
      ],
      component: FileViewerRouting,
      condition: appConfig => {
        return !!appConfig.enableFileAdapter;
      },
    },
    resolve: {
      path: [
        '/project/:projectId/viewer\\?taskId=:taskId&slice=:slice&labels=:label&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/viewer\\?taskId=:taskId&slice=:slice&labels=:label',
        '/project/:projectId/viewer\\?taskId=:taskId&slice=:slice&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/viewer\\?taskId=:taskId&slice=:slice',
        '/project/:projectId/viewer\\?taskId=:taskId&labels=:labels&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/viewer\\?taskId=:taskId&labels=:labels',
        '/project/:projectId/viewer\\?taskId=:taskId&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/viewer\\?taskId=:taskId',
        '/project/:projectId/viewer\\?slice=:slice&labels=:labels',
        '/project/:projectId/viewer\\?slice=:slice',
        '/project/:projectId/viewer\\?labels=:labels',
        '/project/:projectId/viewer',
      ],
      component: () => null,
    },
    list: {
      path: [
        '/project/:projectId/\\?taskId=:taskId&slice=:slice&labels=:label&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/\\?taskId=:taskId&slice=:slice&labels=:label',
        '/project/:projectId/\\?taskId=:taskId&slice=:slice&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/\\?taskId=:taskId&slice=:slice',
        '/project/:projectId/\\?taskId=:taskId&labels=:labels&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/\\?taskId=:taskId&labels=:labels',
        '/project/:projectId/\\?taskId=:taskId&ignoreUserSettings=:ignoreUserSettings',
        '/project/:projectId/\\?taskId=:taskId',
        '/project/:projectId/\\?slice=:slice&labels=:labels',
        '/project/:projectId/\\?slice=:slice',
        '/project/:projectId/\\?labels=:labels',
        '/project/:projectId',
        '/project/:projectId/studylist',
      ],
      component: StudyListRouting,
      condition: appConfig => {
        return appConfig.showStudyList !== undefined
          ? appConfig.showStudyList
          : true;
      },
    },
    init: {
      path: ['/init'],
      component: () => null,
    },
    error: {
      path: ['/error'],
      component: ErrorPage,
    },
  },
};

const getRoutes = appConfig => {
  const routes = [];
  for (let keyConfig in ROUTES_DEF) {
    const routesConfig = ROUTES_DEF[keyConfig];

    for (let routeKey in routesConfig) {
      const route = routesConfig[routeKey];
      const validRoute =
        typeof route.condition === 'function'
          ? route.condition(appConfig)
          : true;

      if (validRoute) {
        routes.push({
          path: route.path,
          Component: route.component,
        });
      }
    }
  }

  return routes;
};

const parsePath = (path, server, params) => {
  let _path = path;
  const _paramsCopy = Object.assign({}, server, params);

  for (let key in _paramsCopy) {
    _path = UrlUtil.paramString.replaceParam(_path, key, _paramsCopy[key]);
  }

  _path = _path.replace('\\?taskId=:taskId', '');
  _path = _path.replace(/[?&]slice=:slice/i, '');
  _path = _path.replace(/[?&]labels=:labels/i, '');
  _path = _path.replace(/[?&]ignoreUserSettings=:ignoreUserSettings/i, '');

  return _path;
};

const parseViewerPath = (appConfig = {}, server = {}, params) => {
  let viewerPath = ROUTES_DEF.default.viewer.path;
  if (appConfig.enableGoogleCloudAdapter) {
    viewerPath = ROUTES_DEF.gcloud.viewer.path;
  }
  if (params.projectId) {
    viewerPath = ROUTES_DEF.flywheel.viewer.path[0];
  }

  return parsePath(viewerPath, server, params);
};

const parseStudyListPath = (appConfig = {}, server = {}, params) => {
  let studyListPath = ROUTES_DEF.default.list.path;
  if (appConfig.enableGoogleCloudAdapter) {
    studyListPath = ROUTES_DEF.gcloud.list.path || studyListPath;
  }
  if (params.projectId) {
    studyListPath = ROUTES_DEF.flywheel.list.path[0];
  }

  return parsePath(studyListPath, server, params);
};

export { getRoutes, parseViewerPath, parseStudyListPath, reload };
