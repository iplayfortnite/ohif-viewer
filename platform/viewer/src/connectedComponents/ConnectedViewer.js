import { connect } from 'react-redux';
import Viewer from './Viewer.js';
import OHIF from '@ohif/core';

import {
  Utils as FlywheelUtils,
  Redux as FlywheelRedux,
} from '@flywheel/extension-flywheel';
import {
  Utils as FlywheelCommonUtils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';

import {isWebImageDataProtocol} from './isWebImageDataProtocol.js';

const { isViewerSessionReady } = FlywheelUtils;
const { setTimepoints, setMeasurements } = OHIF.redux.actions;
const {
  setStudyFormAvailableStatus,
  changeRightHandPanelMode,
} = FlywheelCommonRedux.actions;
const { RightHandPanelModes } = FlywheelCommonRedux.constants;

const getActiveServer = servers => {
  const isActive = a => a.active === true;
  return servers.servers.find(isActive);
};

const mapStateToProps = (state, ownProps) => {
  const { viewports, servers } = state;
  const projectConfig = state.flywheel?.projectConfig;
  const shoStudyForm = FlywheelCommonUtils.shouldShowStudyForm(
    projectConfig,
    state,
    window.location
  )
    ? true
    : false;
  const isStudyFormExist =
    state.ohifConfigProperties.isStudyFormExist && shoStudyForm;
  const { panelMode } = state.rightHandPanel;
  return {
    isSessionReady: ownProps.isStudyLoaded && isViewerSessionReady(state),
    activeViewportIndex: viewports.activeViewportIndex,
    seriesOrder: state.flywheel.seriesOrder,
    leftSidePanelDisabled: false,
    viewports: viewports.viewportSpecificData,
    activeServer: getActiveServer(servers),
    projectConfig: FlywheelRedux.selectors.selectProjectConfig(state),
    isStudyFormExist,
    panelMode,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTimepointsUpdated: timepoints => {
      dispatch(setTimepoints(timepoints));
    },
    onMeasurementsUpdated: measurements => {
      dispatch(setMeasurements(measurements));
    },
    setStudyFormAvailability: status => {
      dispatch(setStudyFormAvailableStatus(status));
    },
    dispatch,
  };
};

const mergeProps = (propsFromState, propsFromDispatch, ownProps) => {
  const isWebImage = ownProps.studies.some(study =>
    isWebImageDataProtocol(study.dataProtocol)
  );
  const props = {
    ...propsFromState,
    ...propsFromDispatch,
    ...ownProps,
    rightHandPanelStatus: isRightSidePanelOpen => {
      const { NORMAL, EXPAND, COLLAPSE } = RightHandPanelModes;
      const { dispatch } = propsFromDispatch;
      const { panelMode } = propsFromState;
      let mode = '';

      switch (panelMode) {
        case NORMAL:
          mode = panelMode;
          break;
        case EXPAND:
          mode = isRightSidePanelOpen ? panelMode : NORMAL;
          break;
        case COLLAPSE:
          if (isRightSidePanelOpen) {
            mode = NORMAL;
          }
          break;
      }

      if (mode) {
        dispatch(changeRightHandPanelMode(mode));
      }
    },
    studyInstanceUIDs: isWebImage ? [] : ownProps.studyInstanceUIDs,
  };
  return props;
};

const ConnectedViewer = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Viewer);

export default ConnectedViewer;
