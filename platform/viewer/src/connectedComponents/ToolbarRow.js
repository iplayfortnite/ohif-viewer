import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import intersection from 'lodash/intersection';
import flatten from 'lodash/flatten';
import compact from 'lodash/compact';
import _ from 'lodash';
import csTools from 'cornerstone-tools';

import { MODULE_TYPES } from '@ohif/core';
import {
  ExpandableToolMenu,
  RoundedButtonGroup,
  ToolbarButton,
  withModal,
  withDialog,
} from '@ohif/ui';

import './ToolbarRow.css';
import { commandsManager, extensionManager } from './../App.js';

import ConnectedCineDialog from './ConnectedCineDialog';
import ConnectedCineTemporalDialog from './ConnectedCineTemporalDialog';
import ConnectedLayoutButton from './ConnectedLayoutButton';
import { withAppContext } from '../context/AppContext';
import store from '../store';

import {
  Redux as FlywheelCommonRedux,
  Context as FlywheelCommonContext,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';

import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';
import { Link, withRouter } from 'react-router-dom';

const {
  selectProjectConfig,
  selectReaderTask,
  selectUser,
  hasPermission,
} = FlywheelRedux.selectors;

const { withAllowedActiveTools } = FlywheelCommonContext;
const {
   filterToolbarButtonList,
   getRouteParams,
   isMicroscopyData,
   isCurrentWebImage,
   isCurrentMetaImage
  } = FlywheelCommonUtils;

class ToolbarRow extends Component {
  // TODO: Simplify these? isOpen can be computed if we say "any" value for selected,
  // closed if selected is null/undefined
  static propTypes = {
    activeTool: PropTypes.string,
    activeVtkTool: PropTypes.string,
    isLeftSidePanelOpen: PropTypes.bool.isRequired,
    isRightSidePanelOpen: PropTypes.bool.isRequired,
    leftSidePanelDisabled: PropTypes.bool,
    selectedLeftSidePanel: PropTypes.string.isRequired,
    selectedRightSidePanel: PropTypes.string.isRequired,
    handleSidePanelChange: PropTypes.func.isRequired,
    activeContexts: PropTypes.arrayOf(PropTypes.string).isRequired,
    studies: PropTypes.array,
    t: PropTypes.func.isRequired,
    linkText: PropTypes.string,
    linkPath: PropTypes.string,
    // NOTE: withDialog, withModal HOCs
    dialog: PropTypes.any,
    modal: PropTypes.any,
  };

  static defaultProps = {
    studies: [],
  };

  constructor(props) {
    super(props);

    const toolbarButtonDefinitions = _getVisibleToolbarButtons.call(this);
    const buttonGroups = _getVisibleButtonGroups.call(this);
    // TODO:
    // If it's a tool that can be active... Mark it as active?
    // - Tools that are on/off?
    // - Tools that can be bound to multiple buttons?

    // Normal ToolbarButtons...
    // Just how high do we need to hoist this state?
    // Why ToolbarRow instead of just Toolbar? Do we have any others?
    this.state = {
      toolbarButtons: toolbarButtonDefinitions,
      buttonGroups,
      displayLayoutButton: true,
      activeButtons: [],
    };

    this.seriesPerStudyCount = [];

    this._handleBuiltIn = _handleBuiltIn.bind(this);
  }

  componentDidMount() {
    this.setActiveToolButton();
    this.updateButtonGroups();
  }

  updateButtonGroups() {
    const panelModules = extensionManager.modules[MODULE_TYPES.PANEL];

    this.buttonGroups = {
      left: [],
      right: [],
    };

    // ~ FIND MENU OPTIONS
    panelModules.forEach(panelExtension => {
      const panelModule = panelExtension.module;
      const defaultContexts = Array.from(panelModule.defaultContext);
      if (panelExtension.extensionId !== 'navigate-panel') {
        panelModule.menuOptions.forEach(menuOption => {
          const contexts = Array.from(menuOption.context || defaultContexts);
          const hasActiveContext = this.props.activeContexts.some(actx =>
            contexts.includes(actx)
          );

          // It's a bit beefy to pass studies; probably only need to be reactive on `studyInstanceUIDs` and activeViewport?
          // Note: This does not cleanly handle `studies` prop updating with panel open
          const isDisabled =
            typeof menuOption.isDisabled === 'function' &&
            menuOption.isDisabled(
              this.props.studies,
              this.props.activeViewport
            );

          if (hasActiveContext && !isDisabled) {
            const menuOptionEntry = {
              value: menuOption.target,
              icon: menuOption.icon,
              class: menuOption.class || '',
              bottomLabel: menuOption.label,
            };
            const from = menuOption.from || 'right';

            this.buttonGroups[from].push(menuOptionEntry);
          }
        });
      }
    });

    // TODO: This should come from extensions, instead of being baked in
    this.buttonGroups.left.unshift({
      value: 'studies',
      icon: 'th-large',
      bottomLabel: this.props.t('Series'),
    });

    // Disabled navigate menu temporarily in SM as it is not implemented yet.
    if (!isMicroscopyData(this.props.studies)) {
      this.buttonGroups.left.unshift({
        value: 'navigate-panel',
        icon: 'folder',
        class: 'material-icons',
        bottomLabel: this.props.t('Nav'),
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const activeContextsChanged =
      prevProps.activeContexts !== this.props.activeContexts;
    const allowedActiveToolsChanged = !_.isEqual(
      prevProps.allowedActiveTools,
      this.props.allowedActiveTools
    );

    const prevStudies = prevProps.studies;
    const prevActiveViewport = prevProps.activeViewport;
    const activeViewport = this.props.activeViewport;
    const studies = this.props.studies;
    const seriesPerStudyCount = this.seriesPerStudyCount;

    let shouldUpdate = false;

    if (
      prevStudies.length !== studies.length ||
      prevActiveViewport !== activeViewport
    ) {
      shouldUpdate = true;
    } else {
      for (let i = 0; i < studies.length; i++) {
        if (studies[i].series.length !== seriesPerStudyCount[i]) {
          seriesPerStudyCount[i] = studies[i].series.length;

          shouldUpdate = true;
          break;
        }
      }
    }

    if (shouldUpdate) {
      this.updateButtonGroups();
    }

    if (activeContextsChanged || allowedActiveToolsChanged) {
      const toolbarButtonDefinitions = _getVisibleToolbarButtons.call(this);
      const buttonGroups = _getVisibleButtonGroups.call(this);
      const shouldDisplayLayoutButton = _shouldDisplayLayoutButton.call(this);
      this.setState({
        toolbarButtons: toolbarButtonDefinitions,
        buttonGroups,
        displayLayoutButton: shouldDisplayLayoutButton,
      });
    }

    if (this.props.activeTool !== prevProps.activeTool) {
      this.setActiveToolButton();
    }
    if (
      this.props.activeVtkTool !== prevProps.activeVtkTool ||
      prevState.toolbarButtons !== this.state.toolbarButtons
    ) {
      this.setActiveVtkToolButton();
    }
  }

  setActiveToolButton() {
    const setToolActiveButtons = flattenButtons(
      this.state.toolbarButtons
    ).filter(button => button.type === 'setToolActive');
    const activeToolButton = setToolActiveButtons.find(
      button => button.id === this.props.activeTool
    );
    const setToolActiveIds = setToolActiveButtons.map(button => button.id);
    const activeButtons = this.state.activeButtons.filter(
      button => !setToolActiveIds.includes(button.id)
    );

    const _activeButtons = [...activeButtons, ...[activeToolButton || []]];
    this.setState({ activeButtons: _activeButtons });
  }

  setActiveVtkToolButton() {
    const curToolBtn = this.state.toolbarButtons.find(
      button => button.id === this.props.activeVtkTool
    );
    if (curToolBtn) {
      this.setState({
        activeButtons: [curToolBtn],
      });
    }
  }

  closeCineDialogIfNotApplicable = () => {
    const { dialog } = this.props;
    let { dialogId, activeButtons, toolbarButtons } = this.state;
    if (dialogId) {
      const cineButtonPresent = toolbarButtons.find(
        button => button.options && button.options.behavior === 'CINE'
      );
      if (!cineButtonPresent) {
        dialog.dismiss({ id: dialogId });
        activeButtons = activeButtons.filter(
          button => button.options && button.options.behavior !== 'CINE'
        );
        this.setState({ dialogId: null, activeButtons });
      }
    }
  };

  render() {
    const buttonComponents = _getButtonComponents.call(
      this,
      this.state.toolbarButtons,
      this.state.activeButtons
    );
    const { leftButtons, rightButtons } = buttonComponents;
    const hasLink = this.props.linkText && this.props.linkPath;
    const { taskId } = getRouteParams();

    const onPress = (side, value) => {
      this.props.handleSidePanelChange(side, value);
    };
    const onPressLeft = onPress.bind(this, 'left');
    const onPressRight = onPress.bind(this, 'right');

    let newTabLink;
    const projectConfig = selectProjectConfig(store.getState());
    const showNewTabLink =
      window.self !== window.top && !projectConfig?.toolbar?.hideOpenNewTabLink;
    if (showNewTabLink) {
      newTabLink = (
        <div>
          <a href={window.location.href} target="_blank" title="New Tab">
            <ToolbarButton
              key="NewTab"
              label="New Tab"
              icon="open_in_new"
              class="material-icons"
            ></ToolbarButton>
          </a>
        </div>
      );
    }

    const studyListButton = leftButtons.find(
      button => button.key === 'StudyList'
    );

    return (
      <>
        <div className="ToolbarRow" id="toolbarRow">
          <div className="pull-left m-t-1 p-y-1" style={{ padding: '10px' }}>
            <RoundedButtonGroup
              options={this.state.buttonGroups.left}
              value={this.props.selectedLeftSidePanel || ''}
              onValueChanged={onPressLeft}
            />
          </div>
          {hasLink &&
            !taskId &&
            leftButtons.find(button => button.key === 'StudyList')}
          {hasLink && !taskId && <div className="btn-separator"></div>}
          {this.state.displayLayoutButton && <ConnectedLayoutButton />}
          {leftButtons.filter(button => button.key !== 'StudyList')}
          {rightButtons && (
            <div
              className="pull-right"
              style={{
                marginLeft: 'auto',
                display: 'flex',
              }}
            >
              {rightButtons}
            </div>
          )}
          {newTabLink}
          <div
            className="pull-right m-t-1 rm-x-1"
            style={{
              marginLeft: '40px',
              display: 'flex',
              marginRight: window.self !== window.top ? '40px' : '0px',
            }}
          >
            {this.state.buttonGroups.right.length && (
              <RoundedButtonGroup
                options={this.state.buttonGroups.right}
                value={this.props.selectedRightSidePanel || ''}
                onValueChanged={onPressRight}
              />
            )}
          </div>
        </div>
      </>
    );
  }
}

function _getCustomButtonComponent(button, activeButtons) {
  const CustomComponent = button.CustomComponent;
  const isValidComponent =
    typeof CustomComponent === 'function' ||
    (CustomComponent && typeof CustomComponent.WrappedComponent === 'function');

  // Check if its a valid customComponent. Later on an CustomToolbarComponent interface could be implemented.
  if (isValidComponent) {
    const parentContext = this;
    const activeButtonsIds = activeButtons.map(button => button.id);
    const isActive = activeButtonsIds.includes(button.id);

    return (
      <CustomComponent
        parentContext={parentContext}
        toolbarClickCallback={_handleToolbarButtonClick.bind(this)}
        button={button}
        key={button.id}
        disabled={button.disabled}
        activeButtons={activeButtonsIds}
        isActive={isActive}
      />
    );
  }
}

function _getExpandableButtonComponent(button, activeButtons) {
  // Iterate over button definitions and update `onClick` behavior
  const childButtons = button.buttons.map(childButton => ({
    ...childButton,
    onClick: _handleToolbarButtonClick.bind(this, childButton),
    isActive: activeButtons.map(button => button.id).includes(childButton.id),
  }));

  return (
    <ExpandableToolMenu
      key={button.id}
      label={button.label}
      icon={button.icon}
      class={button.class}
      buttons={childButtons}
    />
  );
}

function _getDefaultButtonComponent(button, activeButtons) {
  return (
    <ToolbarButton
      key={button.id}
      label={button.label}
      class={button?.class || ''}
      icon={button.icon}
      disabled={button.disabled}
      onClick={_handleToolbarButtonClick.bind(this, button)}
      isActive={activeButtons.map(button => button.id).includes(button.id)}
    />
  );
}
/**
 * Determine which extension buttons should be showing, if they're
 * active, and what their onClick behavior should be.
 */
function _getButtonComponents(toolbarButtonCollection, activeButtons) {
  const state = store.getState()
  const projectConfig = selectProjectConfig(state);
  let toolbarButtons = filterToolbarButtonList(
    toolbarButtonCollection,
    projectConfig
  );
  if (state.microscopy.isEnabledMicroscopy) {
    toolbarButtons = toolbarButtons.filter(item => {
      if (!['AnnotateMenu'].includes(item.id)) {
        return item;
      }
    });
  }
  const _this = this;
  const buttonMapper = button => {
    const hasCustomComponent = button.CustomComponent;
    const hasNestedButtonDefinitions = button.buttons && button.buttons.length;
    const isSeparator = button.type === 'separator';

    if (isSeparator) {
      return <div className="btn-separator"></div>;
    }

    if (hasCustomComponent) {
      return _getCustomButtonComponent.call(_this, button, activeButtons);
    }

    if (hasNestedButtonDefinitions) {
      return _getExpandableButtonComponent.call(_this, button, activeButtons);
    }

    return _getDefaultButtonComponent.call(_this, button, activeButtons);
  };
  const rightButtons = toolbarButtons.filter(
    button =>
      button.options && button.options.pull && button.options.pull === 'right'
  );
  const leftButtons = toolbarButtons.filter(
    button => !rightButtons.includes(button)
  );

  return {
    leftButtons: leftButtons.map(buttonMapper),
    rightButtons: rightButtons.map(buttonMapper),
  };
}

function _isAnnotationsReadonly(id) {
  const toolbarModules = extensionManager.modules[MODULE_TYPES.TOOLBAR];
  const cornerstoneTools = toolbarModules.find(
    module => module.extensionId === 'cornerstone'
  );
  const annotateTools = cornerstoneTools.module.definitions.find(
    def => def.id === 'AnnotateMenu'
  ) || {buttons:[]};
  const segmentTools = cornerstoneTools.module.definitions.find(
    def => def.id === 'SegmentMenu'
  ) || {buttons:[]};
  const buttons = [...annotateTools.buttons, ...segmentTools.buttons];
  return buttons.find(button => button.id === id);
}

/**
 * TODO: DEPRECATE
 * This is used exclusively in `extensions/cornerstone/src`
 * We have better ways with new UI Services to trigger "builtin" behaviors
 *
 * A handy way for us to handle different button types. IE. firing commands for
 * buttons, or initiation built in behavior.
 *
 * @param {*} button
 * @param {*} evt
 * @param {*} props
 */
async function _handleToolbarButtonClick(button, evt, props) {
  const state = store.getState();
  const user = selectUser(state);
  const isSiteAdmin = user.roles.includes('site_admin');
  const readerTask = await selectReaderTask(state);
  let hasEditAnnotationsOthersPermission = await hasPermission(
    state,
    'annotations_edit_others'
  );
  let ownAnnotationPermission =
    (await hasPermission(state, 'annotations_own')) ||
    (await hasPermission(state, 'annotations_manage'));
  if (isSiteAdmin) {
    ownAnnotationPermission = true;
    hasEditAnnotationsOthersPermission = true;
  }
  if (
    readerTask &&
    ((readerTask.assignee !== user._id &&
      !hasEditAnnotationsOthersPermission) ||
      (readerTask.assignee === user._id && !ownAnnotationPermission))
  ) {
    if (_isAnnotationsReadonly(button.id)) {
      return;
    }
  }

  let { activeButtons } = this.state;
  const activeButtonsIds = activeButtons.map(button => button.id);

  if (button.commandName) {
    const options = Object.assign({ evt }, button.commandOptions);
    commandsManager.runCommand(button.commandName, options);
  }

  // TODO: Use Types ENUM
  // TODO: We can update this to be a `getter` on the extension to query
  //       For the active tools after we apply our updates?
  if (button.type === 'setToolActive') {
    // call external consumer (it store active button on app redux state)
    FlywheelCommonRedux.actions.setActiveTool(button.id);
    // this component stores current activeButtons (also its on app redux state). OHIF to provide a way to consume from app redux state.
    this.setState({
      activeButtons: [button],
    });
  } else if (button.type === 'stateful') {
    // stateful type buttons can be active in conjuncture with other buttons
    if (activeButtonsIds.includes(button.id)) {
      activeButtons = activeButtons.filter(active => active.id !== button.id);
    } else {
      activeButtons.push(button);
    }
    this.setState({
      activeButtons,
    });
  } else if (button.type === 'builtIn') {
    this._handleBuiltIn(button);
  }

  if (button.id === 'Exit2DMPR') {
    addMouseActions();
  }
}

function getMouseButtonMask(button) {
  switch (button) {
    case 'left':
      return { mouseButtonMask: 1 };
    case 'right':
      return { mouseButtonMask: 2 };
    case 'middle':
      return { mouseButtonMask: 4 };
    case 'wheel':
      return {};
    default:
      return 0;
  }
}

function addMouseActions() {
  const config = store.getState()?.flywheel?.projectConfig;
  const mouseActions = config?.mouseActions;
  const wheelActions = ['StackScrollMouseWheel', 'ZoomMouseWheel'];
  if (mouseActions) {
    mouseActions.forEach(action => {
      if (
        action.button === 'wheel' &&
        !wheelActions.includes(action.toolName)
      ) {
        csTools.setToolActive('StackScrollMouseWheel', {});
      } else {
        csTools.setToolActive(
          action.toolName,
          getMouseButtonMask(action.button)
        );
      }
    });
  } else {
    csTools.setToolActive('Pan', { mouseButtonMask: 4 });
    csTools.setToolActive('Zoom', { mouseButtonMask: 2 });
    csTools.setToolActive('Wwwc', { mouseButtonMask: 1 });
    csTools.setToolActive('StackScrollMouseWheel', {});
    csTools.setToolActive('PanMultiTouch', { pointers: 2 });
    csTools.setToolActive('ZoomTouchPinch', {});
    csTools.setToolEnabled('Overlay', {});
  }
}

function _shouldDisplayLayoutButton() {
  const toolbarModules = extensionManager.modules[MODULE_TYPES.TOOLBAR];
  let shouldDisplay = true; // default value is to display

  if (
    !this.props ||
    !this.props.activeContexts ||
    !this.props.activeContexts.length
  ) {
    return shouldDisplay;
  }
  toolbarModules.forEach(extension => {
    const { defaultContext, shouldDisplayLayoutButton } = extension.module;

    if (typeof shouldDisplayLayoutButton === 'boolean') {
      const contextArray = Array.isArray(defaultContext)
        ? defaultContext
        : [defaultContext];

      const contextIntersection = intersection(
        contextArray,
        this.props.activeContexts
      );
      // it hide if at least one active context says to.
      if (contextIntersection && contextIntersection.length) {
        shouldDisplay = shouldDisplay && shouldDisplayLayoutButton;
      }
    }
  });

  return shouldDisplay;
}

function _getVisibleButtonGroups() {
  const state = store.getState()
  const shouldDisableNavigateMenu =
    isCurrentWebImage(state) ||
    isCurrentMetaImage(state) ||
    isMicroscopyData(this.props.studies);
  const panelModules = extensionManager.modules[MODULE_TYPES.PANEL];
  const buttonGroups = {
    // TODO: This should come from extensions, instead of being baked in
    left: [],
    right: [],
  };
  if (!this.props.leftSidePanelDisabled) {
    buttonGroups.left.push({
      value: 'studies',
      icon: 'th-large',
      bottomLabel: this.props.t('Series'),
    });
    // Disabled navigate menu temporarily in SM, MHD, WebImages as it is not implemented yet.
    if (!shouldDisableNavigateMenu ) {
      buttonGroups.left.push({
        value: 'navigate-panel',
        icon: 'folder',
        class: 'material-icons',
        bottomLabel: this.props.t('Nav'),
      });
    }
  }
  panelModules.forEach(panelExtension => {
    const panelModule = panelExtension.module;
    const defaultContexts = Array.from(panelModule.defaultContext);

    if (panelExtension.extensionId !== 'navigate-panel') {
      // MENU OPTIONS
      panelModule.menuOptions.forEach(menuOption => {
        const contexts = Array.from(menuOption.context || defaultContexts);

        const activeContextIncludesAnyPanelContexts = this.props.activeContexts.some(
          actx => contexts.includes(actx)
        );
        // It's a bit beefy to pass studies; probably only need to be reactive on `studyInstanceUIDs` and activeViewport?
        // Note: This does not cleanly handle `studies` prop updating with panel open
        const isDisabled =
          typeof menuOption.isDisabled === 'function' &&
          menuOption.isDisabled(this.props.studies);

        if (activeContextIncludesAnyPanelContexts && !isDisabled) {
          const menuOptionEntry = {
            value: menuOption.target,
            icon: menuOption.icon,
            class: menuOption.class,
            bottomLabel: menuOption.label,
            isVisible: menuOption.isVisible,
          };
          const from = menuOption.from || 'right';

          if (!buttonGroups[from]) {
            buttonGroups[from] = [];
          }
          buttonGroups[from].push(menuOptionEntry);
        }
      });
    }
  });

  return buttonGroups;
}
/**
 *
 */
function _getVisibleToolbarButtons() {
  const toolbarModules = extensionManager.modules[MODULE_TYPES.TOOLBAR];
  const toolbarButtonDefinitionsMap = [];
  // Filter out buttons that aren't visible or in the current context(s)
  toolbarModules.forEach(extension => {
    const { definitions, defaultContext, defaultPrecedence } = extension.module;
    definitions.forEach(definition => {
      const context = definition.context || defaultContext;
      const precedence = defaultPrecedence || 0;

      const contextArray = Array.isArray(context) ? context : [context];
      if (definition.getState) {
        const buttonState = definition.getState();
        if (buttonState.visible === false) {
          return;
        }
      }

      const contextIntersection = intersection(
        contextArray,
        this.props.activeContexts
      );
      if (contextIntersection && contextIntersection.length) {
        if (!toolbarButtonDefinitionsMap[precedence]) {
          toolbarButtonDefinitionsMap[precedence] = [];
        }
        // comment this part considering we are doing this adaptation under adaptToolDefinition method
        //const disabled = commandsManager.isDisabled(definition.commandName);

        adaptToolDefinition(
          definition,
          this.props.allowedActiveTools,
          commandsManager
        );

        toolbarButtonDefinitionsMap[precedence].push(definition);
      }
    });
  });

  return compact(flatten(toolbarButtonDefinitionsMap));
}

/**
 * It mutates given definition param.
 */
const adaptToolDefinition = (
  definition,
  allowedActiveTools,
  commandsManager
) => {
  const disabledFromCommandsManager = commandsManager.isDisabled(
    definition.commandName
  );
  if (definition.id in allowedActiveTools) {
    const allowedTool = allowedActiveTools[definition.id];
    definition.disabled = !allowedTool;
  } else {
    definition.disabled = disabledFromCommandsManager;
  }

  if (Array.isArray(definition.buttons)) {
    return definition.buttons.forEach(_definition =>
      adaptToolDefinition(_definition, allowedActiveTools, commandsManager)
    );
  }
};

function _handleBuiltIn(button) {
  /* TODO: Keep cine button active until its unselected. */
  const { dialog, t } = this.props;
  const { dialogId } = this.state;
  const { id, options } = button;

  if (options.behavior === 'CINE') {
    if (dialogId) {
      dialog.dismiss({ id: dialogId });
      this.setState(state => ({
        dialogId: null,
        activeButtons: [
          ...state.activeButtons.filter(button => button.id !== id),
        ],
      }));
    } else {
      const spacing = 20;
      const { x, y } = document
        .querySelector(`.ViewerMain`)
        .getBoundingClientRect();
      const newDialogId = dialog.create({
        content: options.isCineTemporal
          ? ConnectedCineTemporalDialog
          : ConnectedCineDialog,
        defaultPosition: {
          x: x + spacing || 0,
          y: y + spacing || 0,
        },
      });
      this.setState(state => ({
        dialogId: newDialogId,
        activeButtons: [...state.activeButtons, button],
      }));
    }
  }

  if (options.behavior === 'DOWNLOAD_SCREEN_SHOT') {
    commandsManager.runCommand('showDownloadViewportModal', {
      title: t('Download High Quality Image'),
    });
  }
}

function flattenButtons(buttons) {
  return buttons.reduce(
    (all, btn) => all.concat(btn.buttons ? flattenButtons(btn.buttons) : btn),
    []
  );
}

// OHIF to provide a way to customize it
const withCustomContext = withAllowedActiveTools;

export default withTranslation(['Common', 'ViewportDownloadForm'])(
  withRouter(
    withModal(withDialog(withAppContext(withCustomContext(ToolbarRow))))
  )
);
