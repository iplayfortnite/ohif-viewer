import React, { useState, useEffect, useContext, useCallback } from 'react';
import { metadata, studies, utils, log, redux } from '@ohif/core';
import usePrevious from '../customHooks/usePrevious';
import { Components as FlywheelComponents } from '@flywheel/extension-flywheel';
import {
  HTTP as FlywheelCommonHTTP,
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
import ConnectedViewer from './ConnectedViewer.js';
import PropTypes from 'prop-types';
import { extensionManager } from './../App.js';
import { useSnackbarContext } from '@ohif/ui';

// Contexts
import AppContext from '../context/AppContext';
import { useDispatch, useSelector } from 'react-redux';
import cornerstone from 'cornerstone-core';
import {
  isWebImageDataProtocol,
  isZipWebImageProtocol,
} from './isWebImageDataProtocol';

const { Loader } = FlywheelComponents;
const {
  setLoaderDisplayStatus,
  removeSegmentationData,
} = FlywheelCommonRedux.actions;
const { selectLoaderDisplayStatus } = FlywheelCommonRedux.selectors;
const { OHIFStudyMetadata, OHIFSeriesMetadata } = metadata;
const {
  retrieveStudiesMetadata,
  retrieveStudyMetadata,
  deleteStudyMetadataPromise,
  services,
} = studies;
const {
  studyMetadataManager,
  makeCancelable,
  loadPendingDerivedDisplaySets,
  updateSegmentationData,
  performanceTracking,
} = utils;
const {
  performanceEnd,
  performanceRemove,
} = performanceTracking.setPerformance;
const { SEGMENTATION_LOAD } = redux.performanceTrackerKeys;
const { getAcquisition, getProjectAssociations } = FlywheelCommonHTTP.services;
const { getRouteParams, showErrorPage } = FlywheelCommonUtils;

const refreshViewport = () => {
  cornerstone.getEnabledElements().forEach(enabledElement => {
    if (enabledElement.image) {
      cornerstone.updateImage(enabledElement.element);
    }
  });
};

const _getStudyInstanceUIDs = (studies = [], studyInstanceUIDs) => {
  if (studyInstanceUIDs && studyInstanceUIDs.length) {
    return studyInstanceUIDs;
  }

  const _studyInstanceUIDs = [];
  for (let study of studies) {
    if (study && study.StudyInstanceUID) {
      _studyInstanceUIDs.push(study.StudyInstanceUID);
    }
  }
  return _studyInstanceUIDs;
};

const _promoteToFront = (list, values, searchMethod) => {
  let listCopy = [...list];
  let response = [];
  let promotedCount = 0;

  const arrayValues = values.split(',');
  arrayValues.forEach(value => {
    const index = listCopy.findIndex(searchMethod.bind(undefined, value));

    if (index >= 0) {
      const [itemToPromote] = listCopy.splice(index, 1);
      response[promotedCount] = itemToPromote;
      promotedCount++;
    }
  });

  return {
    promoted: promotedCount === arrayValues.length,
    data: [...response, ...listCopy],
  };
};

/**
 * Promote series to front if find found equivalent on filters object
 * @param {Object} study - study reference to promote series against
 * @param {Object} [filters] - Object containing filters to be applied
 * @param {string} [filter.seriesInstanceUID] - series instance uid to filter results against
 * @param {boolean} isFilterStrategy - if filtering by query param strategy ON
 */
const _promoteList = (study, studyMetadata, filters, isFilterStrategy) => {
  let promoted = false;
  // Promote only if no filter should be applied
  if (!isFilterStrategy) {
    promoted = _promoteStudyDisplaySet(study, studyMetadata, filters);
  }

  return promoted;
};

const _promoteStudyDisplaySet = (study, studyMetadata, filters) => {
  let promoted = false;
  const queryParamsLength = Object.keys(filters).length;
  const shouldPromoteToFront = queryParamsLength > 0;

  if (shouldPromoteToFront) {
    const { seriesInstanceUID } = filters;

    const _seriesLookup = (valueToCompare, displaySet) => {
      return displaySet.SeriesInstanceUID === valueToCompare;
    };
    const promotedResponse = _promoteToFront(
      studyMetadata.getDisplaySets(),
      seriesInstanceUID,
      _seriesLookup
    );

    study.displaySets = promotedResponse.data;
    promoted = promotedResponse.promoted;
  }

  return promoted;
};

/**
 * Method to identify if query param (from url) was applied to given list
 * @param {Object} study - study reference to promote series against
 * @param {Object} [filters] - Object containing filters to be applied
 * @param {string} [filter.seriesInstanceUID] - series instance uid to filter results against
 * @param {boolean} isFilterStrategy - if filtering by query param strategy ON
 */
const _isQueryParamApplied = (study, filters = {}, isFilterStrategy) => {
  const { seriesInstanceUID } = filters;
  let applied = true;
  // skip in case no filter or no toast manager

  if (!seriesInstanceUID) {
    return applied;
  }
  const seriesInstanceUIDs = seriesInstanceUID.split(',');

  let validateFilterApplied = () => {
    const sameSize = arrayToInspect.length === seriesInstanceUIDs.length;
    if (!sameSize) {
      return;
    }

    return arrayToInspect.every(item =>
      seriesInstanceUIDs.some(
        seriesInstanceUIDStr => seriesInstanceUIDStr === item.SeriesInstanceUID
      )
    );
  };

  let validatePromoteApplied = () => {
    let isValid = true;
    for (let index = 0; index < seriesInstanceUIDs.length; index++) {
      const seriesInstanceUIDStr = seriesInstanceUIDs[index];
      const resultSeries = arrayToInspect[index];

      if (
        !resultSeries ||
        resultSeries.SeriesInstanceUID !== seriesInstanceUIDStr
      ) {
        isValid = false;
        break;
      }
    }
    return isValid;
  };

  const { series = [], displaySets = [] } = study;
  const arrayToInspect = isFilterStrategy ? series : displaySets;
  const validateMethod = isFilterStrategy
    ? validateFilterApplied
    : validatePromoteApplied;

  if (!arrayToInspect) {
    applied = false;
  } else {
    applied = validateMethod();
  }

  return applied;
};
const _showUserMessage = (queryParamApplied, message, dialog = {}) => {
  if (queryParamApplied) {
    return;
  }

  const { show: showUserMessage = () => {} } = dialog;
  showUserMessage({
    message,
  });
};

const _addSeriesToStudy = (studyMetadata, series, shouldSortDisplaySet) => {
  const sopClassHandlerModules =
    extensionManager.modules['sopClassHandlerModule'];
  const study = studyMetadata.getData();
  const seriesMetadata = new OHIFSeriesMetadata(series, study);
  const existingSeries = studyMetadata.getSeriesByUID(series.SeriesInstanceUID);
  if (existingSeries) {
    studyMetadata.updateSeries(series.SeriesInstanceUID, seriesMetadata);
  } else {
    studyMetadata.addSeries(seriesMetadata);
  }

  studyMetadata.createAndAddDisplaySetsForSeries(
    sopClassHandlerModules,
    seriesMetadata,
    shouldSortDisplaySet
  );

  study.displaySets = studyMetadata.getDisplaySets();
  study.derivedDisplaySets = studyMetadata.getDerivedDatasets({});

  _updateStudyMetadataManager(study, studyMetadata);
};

/**
 * Update the segmentations correspnding to the reference UID passed
 * @param {Array} studies - studies arrya
 * @param {string} referenceUID - reference UID for the segmentation(for DICOM study it will be series instance UID, for Nifti study instance UID)
 * @param {Array} prevSegments - previous segments list corresponding to the reference UID
 * @param {Array} newSegments - new segments list corresponding to the reference UID
 */
const _updateSegmentationsForReference = (
  studies,
  referenceUID,
  prevSegments,
  newSegments,
  modality
) => {
  studies.forEach(study => {
    const studyUid = study.studyInstanceUID || study.StudyInstanceUID;
    updateSegmentationData.updateSegmentDisplayData(
      studyUid,
      studies,
      referenceUID,
      prevSegments,
      newSegments,
      modality
    );
  });
};

/**
 * Remove the derived/segmentation series's from study
 * @param {object} study
 */
const _removeAllSegmentaionDerivedSeries = study => {
  const studyUid = study.studyInstanceUID || study.StudyInstanceUID;
  const studyMetaData = studyMetadataManager.get(studyUid);
  updateSegmentationData.removeAllSegments(studyUid, [study]);
  const derivedDisplaySets = !!study.derivedDisplaySets
    ? study.derivedDisplaySets
    : [];
  derivedDisplaySets.forEach(displaySet => {
    if (displaySet.Modality === 'SEG' || displaySet.Modality === 'RTSTRUCT')
      _removedDerivedSeriesFromStudy(
        studyMetaData,
        displaySet.SeriesInstanceUID,
        true
      );
  });
};

/**
 * Remove the derived/segmentation series's from study corresponding to the series instance UID
 * @param {object} studyMetadata
 * @param {string} seriesInstanceUID
 * @param {boolean} shouldSortDisplaySet
 */
const _removedDerivedSeriesFromStudy = (
  studyMetadata,
  seriesInstanceUID,
  shouldSortDisplaySet
) => {
  const study = studyMetadata.getData();
  const seriesMetadata = studyMetadata.getSeriesByUID(seriesInstanceUID);
  const series = seriesMetadata._data;
  if (!!series.instances && series.instances.length > 0) {
    const referencedSeriesInstanceUID =
      series.Modality === 'RTSTRUCT'
        ? series.instances[0].metadata.ReferencedSeriesSequence?.[0]
            .SeriesInstanceUID
        : series.instances[0].ReferencedSeriesSequence?.SeriesInstanceUID;
    studyMetadata.removeDisplaySetsForSeries(
      seriesMetadata,
      referencedSeriesInstanceUID,
      shouldSortDisplaySet
    );
    studyMetadata.removeSeries(series.SeriesInstanceUID);

    study.displaySets = studyMetadata.getDisplaySets();
    study.derivedDisplaySets = studyMetadata.getDerivedDatasets({});
    const index = study.series.findIndex(seriesData => {
      return seriesData.SeriesInstanceUID === seriesInstanceUID;
    });

    if (index >= 0) {
      study.series.splice(index, 1);
    }

    _updateStudyMetadataManager(study, studyMetadata);
  }
};

const _updateStudyMetadataManager = (study, studyMetadata) => {
  const { StudyInstanceUID } = study;
  if (!studyMetadataManager.get(StudyInstanceUID)) {
    studyMetadataManager.add(studyMetadata);
  }
};

const _updateStudyDisplaySets = (
  study,
  studyMetadata,
  shouldSortDisplaySet
) => {
  const sopClassHandlerModules =
    extensionManager.modules['sopClassHandlerModule'];

  if (!study.displaySets) {
    study.displaySets = studyMetadata.createDisplaySets(
      sopClassHandlerModules,
      shouldSortDisplaySet
    );
  } else if (
    study.displaySets.length > 0 &&
    studyMetadata.getDisplaySets().length === 0
  ) {
    studyMetadata.setDisplaySets(study.displaySets, false);
    study.displaySets = studyMetadata.getDisplaySets();
  }

  if (study.derivedDisplaySets) {
    studyMetadata._addDerivedDisplaySets(study.derivedDisplaySets);
  }
};

const _thinStudyData = study => {
  return {
    StudyInstanceUID: study.StudyInstanceUID,
    series: study.series.map(item => ({
      SeriesInstanceUID: item.SeriesInstanceUID,
    })),
  };
};

const getRetrieveStudiesMethod = (
  studyInstanceUIDs,
  seriesInstanceUIDs,
  fileNames,
  containerIds,
  server,
  filters,
  isFilterStrategy,
  segmentationFileNames
) => {
  const retrieveParams = [server];
  let retrieveMethod;

  if (studyInstanceUIDs && studyInstanceUIDs.length) {
    retrieveParams.push(studyInstanceUIDs);
    const seriesInstanceUID = seriesInstanceUIDs && seriesInstanceUIDs[0];

    if (seriesInstanceUID && seriesInstanceUIDs.length === 1) {
      filters.seriesInstanceUID = seriesInstanceUID;
      // Query param filtering controlled by appConfig property
      if (isFilterStrategy) {
        retrieveParams.push(filters);
      }
    }

    retrieveMethod = () => retrieveStudiesMetadata(...retrieveParams);
  }

  if (fileNames) {
    retrieveParams.push(containerIds);
    retrieveParams.push(fileNames);
    if (segmentationFileNames) {
      retrieveParams.push(segmentationFileNames);
    }
    retrieveMethod = () => services.FILE.RetrieveData(...retrieveParams);
  }

  return retrieveMethod;
};

const getRetrieveStudyMethod = (
  studyInstanceUIDs,
  seriesInstanceUIDs,
  server,
  filters,
  isFilterStrategy
) => {
  const retrieveParams = [server];
  let retrieveMethod;

  if (studyInstanceUIDs && studyInstanceUIDs.length) {
    retrieveParams.push(studyInstanceUIDs);
    const seriesInstanceUID = seriesInstanceUIDs && seriesInstanceUIDs[0];

    if (seriesInstanceUID && seriesInstanceUIDs.length === 1) {
      filters.seriesInstanceUID = seriesInstanceUID;
      // Query param filtering controlled by appConfig property
      if (isFilterStrategy) {
        retrieveParams.push(filters);
      }
    }

    retrieveMethod = () => retrieveStudyMetadata(...retrieveParams);
  }

  return retrieveMethod;
};

function ViewerRetrieveStudyData({
  server,
  studyInstanceUIDs,
  seriesInstanceUIDs,
  clearViewportSpecificData,
  fileNames,
  containerIds,
  thumbnailsService = {},
  setStudyData,
  segmentationFileNames = {},
  projectConfig = null,
}) {
  // hooks
  const dispatch = useDispatch();
  const [error, setError] = useState(null);
  const [studies, setStudies] = useState([]);
  const [isStudyLoaded, setIsStudyLoaded] = useState(false);
  const [isSegmentLoading , setSegmentLoading] = useState (false)
  const snackbarContext = useSnackbarContext();
  const { appConfig = {} } = useContext(AppContext);
  const { shouldSortDisplaySet = true } = thumbnailsService;
  const segmentationData = useSelector(store => {
    return store?.segmentation?.segmentationData;
  });
  const enableLoader = useSelector(store => {
    return selectLoaderDisplayStatus(store);
  });
  const evaluatePerformance = useSelector(store => {
    return store?.flywheel?.projectConfig?.evaluatePerformance;
  });
  // Holds the previous data to identify the updates(addition, update fields or removal)
  const prevSegmentationData = usePrevious(segmentationData);

  const prevEnableLoader = usePrevious(enableLoader);

  const multiSessionData = useSelector(store => {
    return store?.multiSessionData;
  });
  const prevMultiSessionData = usePrevious(multiSessionData);

  const filterSegmentListByModality = segmentList => {
    const segmentModalityMap = {};
    if (segmentList) {
      segmentList.forEach(segment => {
        if (!segmentModalityMap[segment.modality]) {
          segmentModalityMap[segment.modality] = [];
        }
        segmentModalityMap[segment.modality].push(segment);
      });
    }
    return segmentModalityMap;
  };

  /**
   * Load the new segmentation data and render it in viewport
   */
  const addSegment = () => {
    const addedSegments = {};
    let isPendingLoading = false;
    const loadedSegments = {};
    Object.keys(segmentationData).forEach(key => {
      const prevSegments = !prevSegmentationData[key]
        ? []
        : prevSegmentationData[key];
      const newSegments = segmentationData[key];
      const newSegmentModalityMap = filterSegmentListByModality(newSegments);
      const prevSegmentModalityMap = filterSegmentListByModality(prevSegments);
      Object.keys(newSegmentModalityMap).forEach(modality => {
        newSegmentModalityMap[modality].forEach((segData, index) => {
          if (
            !prevSegmentModalityMap[modality] ||
            prevSegmentModalityMap[modality].length <= index
          ) {
            if (!addedSegments[modality]) {
              addedSegments[modality] = {};
            }
            if (!addedSegments[modality][segData.referenceUID]) {
              addedSegments[modality][segData.referenceUID] = [];
            }
            if (!segData.isLoadedInitially) {
              addedSegments[modality][segData.referenceUID].push(segData);
              isPendingLoading = true;
            } else {
              if (!loadedSegments[modality]) {
                loadedSegments[modality] = [];
              }
              loadedSegments[modality].push(segData);
            }
          }
        });
      });
    });
    if (isPendingLoading) {
      Object.keys(addedSegments).forEach(modality => {
        if (addedSegments[modality]) {
          loadSegmentationSeries(addedSegments[modality]);
        }
      });

      dispatch(setLoaderDisplayStatus(true));
    } else {
      dispatch(setLoaderDisplayStatus(false));
      Object.keys(loadedSegments).forEach(modality => {
        loadPendingSegments(loadedSegments[modality], modality, studies);
      });
    }
  };

  // Loading the pending derived display sets if it is not loaded (one scenario is like the reference
  // series is not the active viewport series, so in that case readjust the reference UID of segmentation also)
  const loadPendingSegments = (loadedSegments, modality, studies) => {
    const promises = loadPendingDerivedDisplaySets(studies, modality);
    if (promises.length > 0) {
      Promise.all(promises)
        .then(() => {
          if (loadedSegments) {
            updateSegmentationData.resetReferenceUID(loadedSegments, modality);
          }
        })
        .catch(error => {
          (loadedSegments || []).forEach(segment => {
            const segmentSeriesInstanceUID = segment.seriesInstanceUID;
            const study = (studies || []).find(item =>
              (item.series || []).find(
                series => series.SeriesInstanceUID === segmentSeriesInstanceUID
              )
            );
            // Remove invalid segment series already added to the study
            if (study && segmentSeriesInstanceUID) {
              const studyUid = study.studyInstanceUID || study.StudyInstanceUID;
              const studyMetaData = studyMetadataManager.get(studyUid);
              _removedDerivedSeriesFromStudy(
                studyMetaData,
                segmentSeriesInstanceUID,
                true
              );
            }
            dispatch(removeSegmentationData(segment));
          });
          dispatch(setLoaderDisplayStatus(false));
          _showUserMessage(false, error.message, snackbarContext);
        });
    } else {
      if (loadedSegments[modality]) {
        let referenceUID = loadedSegments[modality][0].referenceUID;
        _updateSegmentationsForReference(
          studies,
          referenceUID,
          loadedSegments[modality],
          loadedSegments[modality],
          modality
        );
      }
    }
  };

  /**
   * Identify the updated segmentation property and apply it on view
   * @param {boolean} updateAll - update the segmentations which have changes or update all segmentations
   */
  const updateSegmentField = (updateAll = false) => {
    let isUpdated = false;
    let prevDataMissing = false;
    let prevSegments = [];
    let newSegments = [];
    let referenceUID = '';
    let newSegmentModalityMap = {};
    let prevSegmentModalityMap = {};
    Object.keys(segmentationData).every(key => {
      prevSegments = prevSegmentationData[key] || [];
      newSegments = segmentationData[key];
      newSegmentModalityMap = filterSegmentListByModality(newSegments);
      prevSegmentModalityMap = filterSegmentListByModality(prevSegments);
      referenceUID = key;
      if (updateAll) {
        Object.keys(newSegmentModalityMap).forEach(modality => {
          _updateSegmentationsForReference(
            studies,
            referenceUID,
            prevSegmentModalityMap[modality],
            newSegmentModalityMap[modality],
            modality
          );
        });
        return true;
      }
      newSegments.every((segData, index) => {
        for (const key of Object.keys(segData)) {
          if (!prevSegments[index]) {
            isUpdated = true;
            prevDataMissing = true;
            break;
          }
          if (segData[key] !== prevSegments[index][key]) {
            isUpdated = true;
            break;
          }
        }
        return !isUpdated;
      });
      return !isUpdated;
    });
    if (updateAll) {
      refreshViewport();
    } else if (isUpdated) {
      const referenceData = prevDataMissing
        ? newSegmentModalityMap
        : prevSegmentModalityMap;
      Object.keys(newSegmentModalityMap).forEach(modality => {
        _updateSegmentationsForReference(
          studies,
          referenceUID,
          referenceData[modality],
          newSegmentModalityMap[modality],
          modality
        );
      });
      refreshViewport();
    }
  };

  /**
   * Remove the segmentation data and update the view
   */
  const removeSegmentData = () => {
    let prevSegments = [];
    let newSegments = [];
    let referenceUID = '';
    let removedModality = '';
    let newSegmentModalityMap = {};
    let prevSegmentModalityMap = {};
    Object.keys(prevSegmentationData).every(key => {
      prevSegments = prevSegmentationData[key];
      newSegments = segmentationData[key];
      newSegmentModalityMap = filterSegmentListByModality(newSegments);
      prevSegmentModalityMap = filterSegmentListByModality(prevSegments);
      Object.keys(prevSegmentModalityMap).forEach(modality => {
        const prevCount = prevSegmentModalityMap[modality]
          ? prevSegmentModalityMap[modality].length
          : 0;
        const newCount = newSegmentModalityMap[modality]
          ? newSegmentModalityMap[modality].length
          : 0;
        if (prevCount > newCount) {
          referenceUID = key;
          removedModality = modality;
          return false;
        }
        return true;
      });
      return removedModality.length > 0 ? false : true;
    });
    studies.forEach(study => {
      const studyUid = study.studyInstanceUID || study.StudyInstanceUID;
      const studyMetaData = studyMetadataManager.get(studyUid);
      const existingSeries = studyMetaData.getSeriesByUID(referenceUID);
      if (!!existingSeries || studyUid === referenceUID) {
        // For Nifty study, study instance UID is consider as segment list reference UID
        const removedSegmentSeriesInstanceUIDs = updateSegmentationData.updateSegmentDisplayData(
          studyUid,
          studies,
          referenceUID,
          prevSegmentModalityMap[removedModality]
            ? prevSegmentModalityMap[removedModality]
            : [],
          newSegmentModalityMap[removedModality]
            ? newSegmentModalityMap[removedModality]
            : [],
          removedModality,
          true
        );
        removedSegmentSeriesInstanceUIDs.forEach(segmentSeriesInstanceUID => {
          _removedDerivedSeriesFromStudy(
            studyMetaData,
            segmentSeriesInstanceUID,
            true
          );
        });
      }
    });
    refreshViewport();
  };

  useEffect(() => {
    // Handles the segment addition, update and removal
    if (!!prevSegmentationData) {
      const prevSegDataList = [];
      const newSegDataList = [];
      Object.keys(prevSegmentationData).forEach(key => {
        const segments = prevSegmentationData[key];
        if (!!segments && segments.length > 0) {
          segments.forEach(segData => {
            prevSegDataList.push(segData);
          });
        }
      });
      Object.keys(segmentationData).forEach(key => {
        const segments = segmentationData[key];
        if (!!segments && segments.length > 0) {
          segments.forEach(segData => {
            newSegDataList.push(segData);
          });
        }
      });
      if (prevSegDataList.length < newSegDataList.length) {
        addSegment();
      } else if (prevSegDataList.length === newSegDataList.length) {
        updateSegmentField();
      } else if (prevSegDataList.length > newSegDataList.length) {
        removeSegmentData();
      }
    }
  }, [segmentationData]);

  useEffect(() => {
    if (prevEnableLoader && !enableLoader) {
      // If new segmentation loading completed, update view
      updateSegmentField(true);
      setSegmentLoading(false)
    }
  }, [enableLoader]);

  const {
    filterQueryParam: isFilterStrategy = false,
    maxConcurrentMetadataRequests,
    history,
  } = appConfig;

  let cancelableSeriesPromises;
  let cancelableStudiesPromises;
  /**
   * Callback method when study is totally loaded
   * @param {object} study study loaded
   * @param {object} studyMetadata studyMetadata for given study
   * @param {Object} [filters] - Object containing filters to be applied
   * @param {string} [filter.seriesInstanceUID] - series instance uid to filter results against
   */
  const studyDidLoad = (study, studyMetadata, filters, isAllStudiesLoaded) => {
    // User message
    const promoted = _promoteList(
      study,
      studyMetadata,
      filters,
      isFilterStrategy
    );

    // Clear viewport to allow new promoted one to be displayed
    if (promoted) {
      clearViewportSpecificData(0);
    }

    const isQueryParamApplied = _isQueryParamApplied(
      study,
      filters,
      isFilterStrategy
    );
    // Show message in case not promoted neither filtered but should to
    _showUserMessage(
      isQueryParamApplied,
      'Query parameters were not totally applied. It might be using original series list for given study.',
      snackbarContext
    );

    const studys = studyInstanceUIDs.length > 0 ? studyInstanceUIDs : fileNames;
    if (studys.includes(study.studyInstanceUID || study.StudyInstanceUID)) {
      if (
        !studies.find(
          item =>
            (study.studyInstanceUID || study.StudyInstanceUID) ===
            (item.studyInstanceUID || item.StudyInstanceUID)
        )
      ) {
        setStudies(
          [...studies, study].filter(study =>
            studys.includes(study.studyInstanceUID || study.StudyInstanceUID)
          )
        );
      }
    }
    setIsStudyLoaded(isAllStudiesLoaded);
  };

  /**
   * Method to process studies. It will update displaySet, studyMetadata, load remaining series, ...
   * @param {Array} studiesData Array of studies retrieved from server
   * @param {Object} [filters] - Object containing filters to be applied
   * @param {string} [filters.seriesInstanceUID] - series instance uid to filter results against
   */
  const processStudies = (studiesData, filters) => {
    if (Array.isArray(studiesData) && studiesData.length > 0) {
      // Map studies to new format, update metadata manager?
      let count = studiesData.length;
      const loadedStudies = [];
      const studies = studiesData.map(study => {
        // TODO: containerId and fileName for webimages are temporarily stored in studies in store
        // Need to refactor code to store currentWebImages from a proper place
        // before retrieving measurements.
        setStudyData(study.StudyInstanceUID, filteredData(study));
        const studyMetadata = new OHIFStudyMetadata(
          study,
          study.StudyInstanceUID
        );

        _updateStudyDisplaySets(study, studyMetadata, shouldSortDisplaySet);
        _updateStudyMetadataManager(study, studyMetadata);

        // Ensure the component update is not due to change in multi-session before
        // removing the segmentation series'.
        if (
          prevMultiSessionData?.isMultiSessionViewEnabled ===
            multiSessionData?.isMultiSessionViewEnabled &&
          prevMultiSessionData.acquisitionData.forEach(prevAcq =>
            multiSessionData.acquisitionData.find(
              acqData => prevAcq.seriesUid === acqData.seriesUid
            )
          )
        ) {
          // Removed all the segmentation series's loaded on study change or reopen
          _removeAllSegmentaionDerivedSeries(study);
        }

        // Attempt to load remaning series if any
        cancelableSeriesPromises[study.StudyInstanceUID] = makeCancelable(
          loadRemainingSeries(studyMetadata, shouldSortDisplaySet)
        )
          .then(result => {
            if (result && !result.isCanceled) {
              count--;
              loadedStudies.push(study);
              studyDidLoad(study, studyMetadata, filters, count === 0);
              if (count === 0) {
                setStudies(loadedStudies);
              }
            }
          })
          .catch(error => {
            handleError(error);
          });

        return study;
      });

      setStudies(studies);
    }
  };

  const forceRerender = () => setStudies(studies => [...studies]);

  const handleError = error => {
    dispatch(setLoaderDisplayStatus(false));
    if (error && !error.isCanceled) {
      const errorMessage = error?.message ? error.message : error;
      history.push({
        pathname: '/error',
        state: { errorMessage },
      });
      setError(error);
      log.error(error);
    }
  };

  const loadRemainingSeries = async (studyMetadata, shouldSortDisplaySet) => {
    const { seriesLoader } = studyMetadata.getData();
    if (!seriesLoader) {
      return Promise.resolve(true);
    }

    const loadNextSeries = async () => {
      if (!seriesLoader.hasNext()) return;
      const series = await seriesLoader.next();
      let seriesArray = [series];
      if (Array.isArray(series)) {
        seriesArray = series;
      }
      seriesArray.forEach(series => {
        _addSeriesToStudy(studyMetadata, series, shouldSortDisplaySet);
      });
      if (!Array.isArray(series)) {
        forceRerender();
      }
      return loadNextSeries();
    };

    const concurrentRequestsAllowed =
      maxConcurrentMetadataRequests || studyMetadata.getSeriesCount();
    const promises = Array(concurrentRequestsAllowed)
      .fill(null)
      .map(loadNextSeries);

    return await Promise.all(promises);
  };

  /**
   * Load the segmentation data as derived series's
   * @param {object} collection of new segmentation data's
   */
  const loadSegmentationSeries = async seriesSegments => {
    try {
      setSegmentLoading(true);
      let nextIndexMap = {};
      Object.keys(segmentationData).forEach(key => {
        const segments = segmentationData[key];
        if (!!segments && segments.length > 0) {
          segments.forEach(segData => {
            if (!nextIndexMap[segData.modality]) {
              nextIndexMap[segData.modality] = -1;
            }
            nextIndexMap[segData.modality] =
              segData.originalId > nextIndexMap[segData.modality]
                ? segData.originalId
                : nextIndexMap[segData.modality];
          });
        }
      });
      const keys = Object.keys(seriesSegments);
      let segments = seriesSegments[keys[keys.length - 1]];
      const modality = segments[0].modality;
      if (modality === 'RTSTRUCT') {
        loadRTOverlaySeries(segments, nextIndexMap[modality]);
        return;
      }
      const nextIndex = segments.findIndex(
        segment => segment.originalId === nextIndexMap[modality]
      );
      const nextSegment = segments[nextIndex];
      studies.forEach(study => {
        const studyUid = study.studyInstanceUID || study.StudyInstanceUID;
        const studyMetaData = studyMetadataManager.get(studyUid);

        setIsStudyLoaded(false);
        const keys = Object.keys(seriesSegments);
        let segments = seriesSegments[keys[keys.length - 1]];

        const loadNextSeries = async () => {
          if (!segments || (!!segments && segments.length === 0)) {
            return;
          }
          const segmentIndex = nextIndexMap[modality];
          const series = await services.FILE.RetrieveSegmentData(
            server,
            study,
            keys[keys.length - 1],
            containerIds,
            studyUid,
            segments,
            {},
            segmentIndex
          );
          let seriesArray = [series];
          if (Array.isArray(series)) {
            seriesArray = series;
          }
          seriesArray.forEach(series => {
            _addSeriesToStudy(studyMetaData, series, shouldSortDisplaySet);
          });
          if (!Array.isArray(series)) {
            forceRerender();
          }
          nextIndexMap[modality] = nextIndexMap[modality] + 1;
          if (segments.length === 0) {
            keys.pop();
            if (keys.length > 0) {
              segments = seriesSegments[keys[keys.length - 1]];
            }
          }
          return loadNextSeries();
        };

        const promises = Array(maxConcurrentMetadataRequests)
          .fill(null)
          .map(loadNextSeries);

        Promise.all(promises)
          .then(result => {
            if (result && !result.isCanceled) {
              setIsStudyLoaded(true);
            }
            if (evaluatePerformance) {
              const endEntry = {
                key: SEGMENTATION_LOAD,
                id: nextSegment.fileId,
                endTime: Date.now(),
              };
              performanceEnd(endEntry);
            }
          })
          .catch(error => {
            if (error && !error.isCanceled) {
              snackbarContext.show({
                title: 'Error loading the segmentation file:',
                message: error.message,
                type: 'error',
                error,
                autoClose: false,
              });
            }
            dispatch(removeSegmentationData(nextSegment));
            dispatch(setLoaderDisplayStatus(false));
            if (evaluatePerformance) {
              const removeEntry = {
                key: SEGMENTATION_LOAD,
                id: nextSegment.fileId,
              };
              performanceRemove(removeEntry);
            }
          });
      });
    } catch (error) {
      handleError(error);
      setSegmentLoading(false)
    }
  };

  /**
   * Load the RT overlay segmentation data as derived series's
   * @param {object} collection of new segmentation data's
   * @param {object} index of next segment to load
   */
  const loadRTOverlaySeries = async (segments, nextIndex) => {
    try {
      nextIndex = segments.findIndex(
        segment => segment.originalId === nextIndex
      );
      const nextSegment = segments[nextIndex];
      const filters = {};
      let studyUid = '';
      studies.forEach(study => {
        const studyUID = study.studyInstanceUID || study.StudyInstanceUID;
        if (nextSegment.referenceUID !== studyUID) {
          if (
            study.series.find(
              series => series.SeriesInstanceUID === nextSegment.referenceUID
            )
          ) {
            studyUid = studyUID;
          }
        }
      });
      const studyMetaData = studyMetadataManager.get(studyUid);
      const retrieveStudyMethod = getRetrieveStudyMethod(
        [nextSegment.studyInstanceUID],
        [nextSegment.seriesInstanceUID],
        server,
        filters,
        isFilterStrategy
      );
      setIsStudyLoaded(false);
      retrieveStudyMethod()
        .then(result => {
          if (result && !result.isCanceled) {
            //processStudies(result, filters);
            const series =
              Array.isArray(result) && result.length > 0
                ? result[0].series
                : result.series;
            let seriesArray = [series];
            if (Array.isArray(series)) {
              seriesArray = series;
            }

            if (
              (seriesArray.length > 0 &&
                seriesArray[0].Modality !== 'RTSTRUCT') ||
              seriesArray.length === 0
            ) {
              dispatch(removeSegmentationData(nextSegment));
              dispatch(setLoaderDisplayStatus(false));
              _showUserMessage(
                false,
                'Selected segmentation file is not a valid RT/NIFTI Overlay file',
                snackbarContext
              );
              if (evaluatePerformance) {
                const removeEntry = {
                  key: SEGMENTATION_LOAD,
                  id: nextSegment.fileId,
                };
                performanceRemove(removeEntry);
              }
              return;
            }

            const study = studyMetaData.getData();
            seriesArray.forEach(series => {
              if (
                !study.series.find(
                  existingSeries =>
                    existingSeries.SeriesInstanceUID ===
                    series.SeriesInstanceUID
                )
              ) {
                study.series.push(series);
              }
              _addSeriesToStudy(studyMetaData, series, shouldSortDisplaySet);
            });

            setIsStudyLoaded(true);

            // Load the pending derived display sets if it is not already
            // gets loaded as part of active viewport loading triggered from viewportgrid
            setTimeout(
              () => loadPendingSegments(segments, 'RTSTRUCT', studies),
              10
            );
            if (evaluatePerformance) {
              const endEntry = {
                key: SEGMENTATION_LOAD,
                id: nextSegment.fileId,
                endTime: Date.now(),
              };
              performanceEnd(endEntry);
            }
            if (!Array.isArray(series)) {
              forceRerender();
            }
          }
        })
        .catch(error => {
          if (error && !error.isCanceled) {
            dispatch(setLoaderDisplayStatus(false));
            setError(error);
            log.error(error);
            if (evaluatePerformance) {
              const removeEntry = {
                key: SEGMENTATION_LOAD,
                id: nextSegment.fileId,
              };
              performanceRemove(removeEntry);
            }
          }
        });
    } catch (error) {
      if (error) {
        dispatch(setLoaderDisplayStatus(false));
        setError(true);
        log.error(error);
      }
    }
  };

  const loadStudies = async () => {
    try {
      const filters = {};
      const serverData = { ...server, ...appConfig?.servers?.dicomWeb?.[0] };
      const retrieveStudiesMethod = getRetrieveStudiesMethod(
        studyInstanceUIDs,
        seriesInstanceUIDs,
        fileNames,
        containerIds,
        serverData,
        filters,
        isFilterStrategy,
        segmentationFileNames
      );

      let resource;
      if (studyInstanceUIDs.length > 0) {
        resource = studyInstanceUIDs;
      } else {
        resource = fileNames;
      }
      cancelableStudiesPromises[resource] = makeCancelable(
        retrieveStudiesMethod()
      )
        .then(result => {
          if (result && !result.isCanceled) {
            processStudies(result, filters);
            if (studyInstanceUIDs[0]) {
              const { projectId, sessionId } = getRouteParams(location);
              const queryParams = studyInstanceUIDs[0]
                ? { study_uid: studyInstanceUIDs[0] }
                : { session_id: sessionId };
              getProjectAssociations(projectId, queryParams).then(
                associations => {
                  const currentAssociation = associations.find(
                    projectAssociation =>
                      projectAssociation.study_uid === studyInstanceUIDs[0] &&
                      projectAssociation.acquisition_id !== 'None'
                  );
                  let isFileIdMatched = false;
                  const acquisitionPromise = getAcquisition(
                    currentAssociation.acquisition_id
                  );
                  acquisitionPromise.then(acquisition => {
                    associations.forEach(projectAssociations => {
                      acquisition.files.length &&
                        acquisition.files.forEach(file => {
                          if (projectAssociations.file_id === file._id) {
                            isFileIdMatched = true;
                          }
                        });
                    });
                    if (!isFileIdMatched) {
                      showErrorPage(
                        history,
                        'Could not open requested study. Unable to fetch associated acquisition file.',
                        associations[0].project_id
                      );
                    }
                  });
                }
              );
            }
          }
        })
        .catch(error => {
          handleError(error);
        });
    } catch (error) {
      handleError(error);
    }
  };

  const filteredData = study => {
    if (isZipWebImageProtocol(study.dataProtocol)) {
      const containerId = study.StudyInstanceUID;
      const index = containerIds.indexOf(containerId);
      const fileName = fileNames[index];
      return {
        ..._thinStudyData(study),
        containerId,
        fileName,
      };
    } else if (isWebImageDataProtocol(study.dataProtocol)) {
      const fileName = study.StudyInstanceUID;
      const containerId = containerIds[fileNames.indexOf(fileName)];
      return {
        ..._thinStudyData(study),
        containerId,
        fileName,
      };
    } else {
      return _thinStudyData(study);
    }
  };

  const purgeCancellablePromises = useCallback(() => {
    for (let studyInstanceUIDs in cancelableStudiesPromises) {
      if ('cancel' in cancelableStudiesPromises[studyInstanceUIDs]) {
        cancelableStudiesPromises[studyInstanceUIDs].cancel();
      }
    }

    for (let studyInstanceUIDs in cancelableSeriesPromises) {
      if ('cancel' in cancelableSeriesPromises[studyInstanceUIDs]) {
        cancelableSeriesPromises[studyInstanceUIDs].cancel();
        deleteStudyMetadataPromise(studyInstanceUIDs);
        studyMetadataManager.remove(studyInstanceUIDs);
      }
    }
  });

  const prevStudyInstanceUIDs = usePrevious(studyInstanceUIDs);
  const prevSeriesInstanceUIDs = usePrevious(seriesInstanceUIDs);
  const prevFileNames = usePrevious(fileNames);
  const prevStudys =
    prevStudyInstanceUIDs?.length > 0 ? prevStudyInstanceUIDs : prevFileNames;
  const prevProjectConfig = usePrevious(projectConfig);

  useEffect(() => {
    const studys = studyInstanceUIDs.length > 0 ? studyInstanceUIDs : fileNames;
    const hasStudyInstanceUIDsChanged = !(
      prevStudys && prevStudys.every(e => studys.includes(e))
    );

    const seriesInstanceUIDsChanged =
      seriesInstanceUIDs &&
      seriesInstanceUIDs.length >
        (prevSeriesInstanceUIDs ? prevSeriesInstanceUIDs.length : 0);

    if (hasStudyInstanceUIDsChanged || seriesInstanceUIDsChanged) {
      studyMetadataManager.purge();
      purgeCancellablePromises();
      setIsStudyLoaded(false);
      if (!hasStudyInstanceUIDsChanged && seriesInstanceUIDsChanged) {
        studyInstanceUIDs.forEach(studyUID =>
          deleteStudyMetadataPromise(studyUID)
        );
        cancelableSeriesPromises = {};
        cancelableStudiesPromises = {};
        clearViewportSpecificData();
        loadStudies();
      }
    }
  }, [
    prevStudyInstanceUIDs,
    prevFileNames,
    purgeCancellablePromises,
    studyInstanceUIDs,
    fileNames,
  ]);

  useEffect(() => {
    if (projectConfig === null) {
      // do not load study until projectConfig is retrieved
      return;
    }
    // If StudyInstanceUIDs change was triggered before projectConfig retrieval,
    // force reordering of displaySet when required
    const loadedStudies =
      studyInstanceUIDs.length > 0 ? studyInstanceUIDs : fileNames;
    const hasConfigLoadedAfterStudy =
      loadedStudies.length && prevProjectConfig == null;
    const shouldRefreshDisplaySets =
      hasConfigLoadedAfterStudy && projectConfig?.sortByTriggerTime === false;
    if (shouldRefreshDisplaySets) {
      studyInstanceUIDs.forEach(studyUID => {
        const studyMetaData = studyMetadataManager.get(studyUID);
        studyMetaData?.sortDisplaySets();
      });
    }
  }, [projectConfig]);

  useEffect(() => {
    cancelableSeriesPromises = {};
    cancelableStudiesPromises = {};
    clearViewportSpecificData();
    loadStudies();

    return () => {
      purgeCancellablePromises();
    };
  }, [
    studyInstanceUIDs[0],
    studyInstanceUIDs[1],
    fileNames?.[0],
    fileNames?.[1] || '',
  ]);

  if (error) {
    return <div className="error">Error: {JSON.stringify(error)}</div>;
  }

  const _studyInstanceUIDs = _getStudyInstanceUIDs(studies, studyInstanceUIDs);

  return (
    <>
      <Loader loader={enableLoader} />
      <ConnectedViewer
        studies={studies}
        singleFileMode={
          (seriesInstanceUIDs && seriesInstanceUIDs.length === 1) ||
          (!!fileNames && !!containerIds)
        }
        studyInstanceUIDs={_studyInstanceUIDs}
        thumbnailsService={thumbnailsService}
        isStudyLoaded={isStudyLoaded}
        isSegmentLoading={isSegmentLoading}
      />
    </>
  );
}

ViewerRetrieveStudyData.propTypes = {
  thumbnailsService: PropTypes.object,
  fileNames: PropTypes.array,
  // TODO nifti maybe pass query string instead
  segmentationFileNames: PropTypes.array,
  containerIds: PropTypes.array,
  studyInstanceUIDs: PropTypes.array.isRequired,
  seriesInstanceUIDs: PropTypes.array,
  server: PropTypes.object,
  clearViewportSpecificData: PropTypes.func.isRequired,
  setStudyData: PropTypes.func.isRequired,
  projectConfig: PropTypes.object,
};

export default ViewerRetrieveStudyData;
