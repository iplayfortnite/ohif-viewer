import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import OHIF, { MODULE_TYPES, DICOMSR } from '@ohif/core';
import { withDialog } from '@ohif/ui';
import moment from 'moment';
import debounce from 'lodash/debounce';
import ConnectedHeader from './ConnectedHeader.js';
import ConnectedToolbarRow from './ConnectedToolbarRow.js';
import ConnectedStudyBrowser from './ConnectedStudyBrowser.js';
import ConnectedViewerMain from './ConnectedViewerMain.js';
import SidePanel from './../components/SidePanel.js';
import ErrorBoundaryDialog from './../components/ErrorBoundaryDialog';
import { extensionManager, commandsManager } from './../App.js';
import ConnectedStudyLoadingMonitor from './ConnectedStudyLoadingMonitor.js';
import FlywheelLogo from './../components/FlywheelLogo/FlywheelLogo';

// Contexts
import WhiteLabelingContext from '../context/WhiteLabelingContext.js';
import UserManagerContext from '../context/UserManagerContext';
import AppContext from '../context/AppContext';

import {
  Utils as FlywheelCommonUtils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import {
  Components as FlywheelComponents,
  Utils as FlywheelUtils,
} from '@flywheel/extension-flywheel';

import './Viewer.css';
import { finished } from 'stream';

const { RightHandPanelModes } = FlywheelCommonRedux.constants;

const { LabellingOverlay: FlywheelLabellingOverlay } = FlywheelComponents;

class Viewer extends Component {
  static propTypes = {
    seriesOrder: PropTypes.array,
    studies: PropTypes.array,
    studyInstanceUIDs: PropTypes.array,
    activeServer: PropTypes.shape({
      type: PropTypes.string,
      wadoRoot: PropTypes.string,
    }),
    onTimepointsUpdated: PropTypes.func,
    onMeasurementsUpdated: PropTypes.func,
    singleFileMode: PropTypes.bool,
    leftSidePanelDisabled: PropTypes.bool,
    // window.store.getState().viewports.viewportSpecificData
    viewports: PropTypes.object.isRequired,
    // window.store.getState().viewports.activeViewportIndex
    activeViewportIndex: PropTypes.number.isRequired,
    isStudyLoaded: PropTypes.bool,
    dialog: PropTypes.object,
    projectConfig: PropTypes.shape({
      showStudyList: PropTypes.bool,
    }),
    isStudyFormExist: PropTypes.bool,
    setStudyFormAvailability: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.prefetchStudies = window.config.prefetchStudies || false;

    const { activeServer } = this.props;
    const server = Object.assign({}, activeServer);

    OHIF.measurements.MeasurementApi.setConfiguration({
      dataExchange: {
        retrieve: FlywheelUtils.Measurements.dataExchange.retrieveMeasurements,
        store: FlywheelUtils.Measurements.dataExchange.storeMeasurements,
      },
      server,
    });

    OHIF.measurements.TimepointApi.setConfiguration({
      dataExchange: {
        retrieve: this.retrieveTimepoints,
        store: this.storeTimepoints,
        remove: this.removeTimepoint,
        update: this.updateTimepoint,
        disassociate: this.disassociateStudy,
      },
    });

    this._getActiveViewport = this._getActiveViewport.bind(this);
    window.addEventListener('resize', this.onResize.bind(this), true);
  }

  state = {
    isLeftSidePanelOpen: true,
    isRightSidePanelOpen: true,
    selectedRightSidePanel: 'annotations-measurement-panel',
    selectedLeftSidePanel: 'studies', // TODO: Don't hardcode this
    thumbnails: [],
    toolbarScrollHeight: '',
  };

  componentWillUnmount() {
    if (this.props.projectConfig?.studyForm?.components) {
      this.props.setStudyFormAvailability(true);
    }
    if (this.props.dialog) {
      this.props.dialog.dismissAll();
    }

    // Clear all annotation before leave
    commandsManager.runCommand('clearAllAnnotations', {
      syncWithMeasurementAPI: false,
    });

    // Clear all Segmentation Data
    commandsManager.runCommand('clearSegmentationData', {});

    // Clear all multi-session view related Data
    commandsManager.runCommand('clearMultiSessionViewData', {});

    // Clear contour related properties
    commandsManager.runCommand('clearContourProperties', {});

    // Clear applied protocol layout
    commandsManager.runCommand('clearProtocolLayout', {});

    // Clear all undo-redo action states
    commandsManager.runCommand('clearAllActionStates', {});

    // Clear all fov grid restore states
    commandsManager.runCommand('clearAllFovGridRestoreStates', {});

    // TODO: Clear the color map on exiting from viewer
    // Later may be removed for enhancement to keep the color map for individual session
    commandsManager.runCommand('clearColormap', {});

    // Clear requested id cache
    commandsManager.runCommand('clearAllRequestedIdCache', {});

    // Clear acquisition cache
    commandsManager.runCommand('clearAcquisitionCache', {});

    // Clear all microscopy toolState
    commandsManager.runCommand('clearMicroscopyToolState', {});

    // Clear scale indicators and its info.
    commandsManager.runCommand('clearScaleDisplay', {});

    window.removeEventListener('resize', this.onResize.bind(this), true);
  }

  retrieveTimepoints = filter => {
    OHIF.log.info('retrieveTimepoints');

    // Get the earliest and latest study date
    let earliestDate = new Date().toISOString();
    let latestDate = new Date().toISOString();
    if (this.props.studies) {
      latestDate = new Date('1000-01-01').toISOString();
      this.props.studies.forEach(study => {
        const StudyDate = moment(study.StudyDate, 'YYYYMMDD').toISOString();
        if (StudyDate < earliestDate) {
          earliestDate = StudyDate;
        }
        if (StudyDate > latestDate) {
          latestDate = StudyDate;
        }
      });
    }

    // Return a generic timepoint
    return Promise.resolve([
      {
        timepointType: 'baseline',
        timepointId: 'TimepointId',
        studyInstanceUIDs: this.props.studyInstanceUIDs,
        PatientID: filter.PatientID,
        earliestDate,
        latestDate,
        isLocked: false,
      },
    ]);
  };

  storeTimepoints = timepointData => {
    OHIF.log.info('storeTimepoints');
    return Promise.resolve();
  };

  updateTimepoint = (timepointData, query) => {
    OHIF.log.info('updateTimepoint');
    return Promise.resolve();
  };

  removeTimepoint = timepointId => {
    OHIF.log.info('removeTimepoint');
    return Promise.resolve();
  };

  disassociateStudy = (timepointIds, StudyInstanceUID) => {
    OHIF.log.info('disassociateStudy');
    return Promise.resolve();
  };

  onTimepointsUpdated = timepoints => {
    if (this.props.onTimepointsUpdated) {
      this.props.onTimepointsUpdated(timepoints);
    }
  };

  onMeasurementsUpdated = measurements => {
    if (this.props.onMeasurementsUpdated) {
      this.props.onMeasurementsUpdated(measurements);
    }
  };

  prefetch = debounce(() => {
    this.prefetcher.setStudies(this.props.studies);
    const numOfViewports = Object.keys(this.props.viewports || {}).length;
    let config = null; // it means prefetcher to use default behavior (in this case prefetch the first)

    // use order as 'downward' to load next n series (where n is numOfViewports - 1, as the first one is automatically prefetched). It will load exactly the number of viewports (grid layout) only.
    if (numOfViewports > 1) {
      config = {
        order: 'downward',
        displaySetCount: numOfViewports - 1,
      };
    }

    this.prefetcher.prefetch(config, false);
  }, 500);

  componentDidMount() {
    const {
      studies,
      thumbnailsService,
      isStudyLoaded,
      singleFileMode,
    } = this.props;
    const { TimepointApi, MeasurementApi } = OHIF.measurements;
    const { StudyPrefetcher } = OHIF.classes;
    const currentTimepointId = 'TimepointId';
    if (this.prefetchStudies) {
      this.prefetcher = StudyPrefetcher.getInstance();
    }

    if (this.props.leftSidePanelDisabled) {
      this.setState({ isLeftSidePanelOpen: false });
    } else {
      this.setState({ isLeftSidePanelOpen: !this.props.singleFileMode });
    }

    const timepointApi = new TimepointApi(currentTimepointId, {
      onTimepointsUpdated: this.onTimepointsUpdated,
    });

    const measurementApi = new MeasurementApi(timepointApi, {
      onMeasurementsUpdated: this.onMeasurementsUpdated,
    });

    window.addEventListener('resize', this.onResize.bind(this), true);

    this.currentTimepointId = currentTimepointId;
    this.timepointApi = timepointApi;
    this.measurementApi = measurementApi;

    if (FlywheelCommonUtils.isCurrentWebImage()) {
      return;
    }

    if (studies) {
      const PatientID = studies[0]?.PatientID;

      if (PatientID) {
        timepointApi.retrieveTimepoints({ PatientID });
        if (isStudyLoaded) {
          this.measurementApi.retrieveMeasurements(
            PatientID,
            [currentTimepointId],
            { singleFileMode }
          );
        }
      }

      this.setState({
        thumbnails: _mapStudiesToThumbnails(
          studies,
          thumbnailsService,
          this.props.seriesOrder
        ),
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      isStudyLoaded,
      isSessionReady = true,
      singleFileMode,
      isStudyFormExist,
      isSegmentLoading,
    } = this.props;

    if (this.prefetchStudies && this.props.viewports != prevProps.viewports) {
      this.prefetcher.setViewports(
        this.props.viewports,
        this.props.activeViewportIndex
      );

      // in case number of viewports has increased more series should be auto prefetched
      const prevKeys = Object.keys(prevProps.viewports || {});
      const curKeys = Object.keys(this.props.viewports || {});
      if (curKeys.length > prevKeys.length && prevKeys.length > 0) {
        const displaySetInstanceUIDs = [];
        curKeys.forEach(key => {
          if (
            !displaySetInstanceUIDs.includes(
              this.props.viewports[key].displaySetInstanceUID
            )
          ) {
            displaySetInstanceUIDs.push(
              this.props.viewports[key].displaySetInstanceUID
            );
          }
        });
        let isNewSeriesExist = false;
        displaySetInstanceUIDs.forEach(displaySetInstanceUID => {
          if (
            !prevKeys.find(
              key =>
                prevProps.viewports[key].displaySetInstanceUID ===
                displaySetInstanceUID
            )
          ) {
            isNewSeriesExist = true;
          }
        });
        if (isNewSeriesExist) {
          this.prefetch();
        }
      }
    }

    // Close the side panel if we're in single file mode, or the side panel is disabled
    if (this.props.leftSidePanelDisabled !== prevProps.leftSidePanelDisabled) {
      this.setState({ isLeftSidePanelOpen: !this.props.leftSidePanelDisabled });
    } else if (this.props.singleFileMode !== prevProps.singleFileMode) {
      this.setState({ isLeftSidePanelOpen: !this.props.singleFileMode });
    }

    // if (studies !== prevProps.studies) {
    //   this.setState({
    //     thumbnails: _mapStudiesToThumbnails(studies),
    //   });
    // }

    if (
      !isSegmentLoading &&
      isStudyLoaded &&
      isSessionReady &&
      (isStudyLoaded !== prevProps.isStudyLoaded ||
        isSessionReady !== prevProps.isSessionReady)
    ) {
      const { studies, thumbnailsService } = this.props;
      const PatientID = studies[0] && studies[0].PatientID;
      const currentTimepointId = this.currentTimepointId;

      this.timepointApi.retrieveTimepoints({ PatientID });
      this.measurementApi
        .retrieveMeasurements(PatientID, [currentTimepointId], {
          singleFileMode,
        })
        .then(() => {
          // ensure measurements are at least passive
          commandsManager.runCommand('setAllAnnotationsState');
        });
      this.setState({
        thumbnails: _mapStudiesToThumbnails(
          studies,
          thumbnailsService,
          this.props.seriesOrder
        ),
      });

      // A patch to adjust the viewport image to fit to the viewport on switching back to study from studyList.
      if (this.props.leftSidePanelDisabled || !this.state.isLeftSidePanelOpen) {
        setTimeout(() => {
          const enabledElements = cornerstone.getEnabledElements();
          enabledElements.forEach(enabledElement =>
            cornerstone.resize(enabledElement.element)
          );
        }, 500);
      }
    }

    if (this.state.isLeftSidePanelOpen && !prevState.isLeftSidePanelOpen) {
      if (this.prefetchStudies) {
        this.prefetch();
      }
    }
    if (
      this.props.panelMode === RightHandPanelModes.COLLAPSE &&
      this.state.selectedRightSidePanel
    ) {
      this.setState({ selectedRightSidePanel: '' });
    }

    if (this.props.studies.length > 0 && isStudyFormExist && !FlywheelCommonUtils.isMicroscopyData(this.props.studies)) {
      this.setState({ selectedRightSidePanel: 'form-measurement-panel' });
      this.props.setStudyFormAvailability(null);
    }
  }

  _getActiveViewport() {
    return this.props.viewports[this.props.activeViewportIndex];
  }

  getRightHandPanelStyleClass() {
    const panelMode = this.props.panelMode;
    if (panelMode === RightHandPanelModes.EXPAND) {
      return 'expand-panel';
    } else if (panelMode === RightHandPanelModes.COLLAPSE) {
      return 'hide-panel';
    }
    return '';
  }

  onResize() {
    const scrollHeight = this.getToolbarRowScrollWidth();
    if (this.state.toolbarScrollHeight !== scrollHeight) {
      this.setState({ toolbarScrollHeight: scrollHeight });
    }
  }

  getToolbarRowScrollWidth() {
    const toolbarRowElement = document.getElementById('toolbarRow');
    if (toolbarRowElement) {
      return toolbarRowElement.scrollWidth;
    }
    return '';
  }

  render() {
    const rightHandPanelStyleClass = this.getRightHandPanelStyleClass();
    let VisiblePanelLeft, VisiblePanelRight;
    const panelExtensions = extensionManager.modules[MODULE_TYPES.PANEL];
    panelExtensions.forEach(panelExt => {
      panelExt.module.components.forEach(comp => {
        if (comp.id === this.state.selectedRightSidePanel) {
          VisiblePanelRight = comp.component;
        } else if (comp.id === this.state.selectedLeftSidePanel) {
          VisiblePanelLeft = comp.component;
        }
      });
    });
    const showStudyList =
      typeof this.props.projectConfig?.showStudyList === 'boolean'
        ? this.props.projectConfig?.showStudyList
        : true;
    return (
      <>
        {/* TOOLBAR */}
        <ErrorBoundaryDialog context="ToolbarRow">
          <ConnectedToolbarRow
            activeViewport={
              this.props.viewports[this.props.activeViewportIndex]
            }
            isLeftSidePanelOpen={this.state.isLeftSidePanelOpen}
            isRightSidePanelOpen={this.state.isRightSidePanelOpen}
            leftSidePanelDisabled={this.props.leftSidePanelDisabled}
            selectedLeftSidePanel={
              this.state.isLeftSidePanelOpen
                ? this.state.selectedLeftSidePanel
                : ''
            }
            selectedRightSidePanel={
              this.state.isRightSidePanelOpen
                ? this.state.selectedRightSidePanel
                : ''
            }
            handleSidePanelChange={(side, selectedPanel) => {
              const sideClicked = side?.[0].toUpperCase() + side.slice(1);
              const openKey = `is${sideClicked}SidePanelOpen`;
              const selectedKey = `selected${sideClicked}SidePanel`;
              const updatedState = Object.assign({}, this.state);

              const isOpen = updatedState[openKey];
              const prevSelectedPanel = updatedState[selectedKey];
              // RoundedButtonGroup returns `null` if selected button is clicked
              const isSameSelectedPanel =
                prevSelectedPanel === selectedPanel || selectedPanel === null;

              updatedState[selectedKey] = selectedPanel || prevSelectedPanel;

              const toggleOpenState = !isOpen || isSameSelectedPanel;
              if (toggleOpenState) {
                updatedState[openKey] = !updatedState[openKey];
              }
              if (side === 'right') {
                this.props?.rightHandPanelStatus(
                  updatedState['isRightSidePanelOpen']
                );
              }

              this.setState(updatedState);
            }}
            studies={this.props.studies}
            linkText={showStudyList ? 'Study List' : undefined}
            linkPath={showStudyList ? '/' : undefined}
          />
        </ErrorBoundaryDialog>

        {this.props.studies && this.prefetchStudies && (
          <ConnectedStudyLoadingMonitor studies={this.props.studies} />
        )}
        {/*<StudyPrefetcher studies={this.props.studies} />*/}

        {/* VIEWPORTS + SIDEPANELS */}
        <div
          className="FlexboxLayout"
          style={{ width: this.state.toolbarScrollHeight }}
        >
          {/* LEFT */}
          <ErrorBoundaryDialog context="LeftSidePanel">
            <SidePanel
              id={this.state.selectedLeftSidePanel}
              from="left"
              isOpen={this.state.isLeftSidePanelOpen}
            >
              {VisiblePanelLeft ? (
                <VisiblePanelLeft
                  isOpen={this.state.isLeftSidePanelOpen}
                  viewports={this.props.viewports}
                  studies={this.props.studies}
                  activeIndex={this.props.activeViewportIndex}
                  commandsManager={commandsManager}
                />
              ) : (
                <ConnectedStudyBrowser
                  studies={this.state.thumbnails}
                  studyMetadata={this.props.studies}
                />
              )}
              <FlywheelLogo></FlywheelLogo>
            </SidePanel>
          </ErrorBoundaryDialog>

          {/* MAIN */}
          <div className={classNames('main-content')}>
            <ErrorBoundaryDialog context="ViewerMain">
              <ConnectedViewerMain
                studies={this.props.studies}
                isStudyLoaded={this.props.isStudyLoaded}
              />
            </ErrorBoundaryDialog>
          </div>

          {/* RIGHT */}
          <ErrorBoundaryDialog context="RightSidePanel">
            <SidePanel
              id={this.state.selectedRightSidePanel}
              from="right"
              isOpen={this.state.isRightSidePanelOpen}
              rootClass={rightHandPanelStyleClass}
            >
              {VisiblePanelRight && (
                <VisiblePanelRight
                  isOpen={this.state.isRightSidePanelOpen}
                  viewports={this.props.viewports}
                  studies={this.props.studies}
                  activeIndex={this.props.activeViewportIndex}
                  activeViewport={
                    this.props.viewports[this.props.activeViewportIndex]
                  }
                  getActiveViewport={this._getActiveViewport}
                />
              )}
            </SidePanel>
          </ErrorBoundaryDialog>
        </div>
        <FlywheelLabellingOverlay />
      </>
    );
  }
}

export default withDialog(Viewer);

/**
 * What types are these? Why do we have "mapping" dropped in here instead of in
 * a mapping layer?
 *
 * TODO[react]:
 * - Add showStackLoadingProgressBar option
 *
 * @param {Study[]} studies
 * @param {DisplaySet[]} studies[].displaySets
 */
const _mapStudiesToThumbnails = function(studies, thumbnailsService, order) {
  // This reorders the study's displaySets to match the order of the SeriesInstanceUIDs (determined from the acquisitionIds from Flywheel)
  const sortDisplaySets = study => {
    const orderedDisplaySets = order
      .map(uid => {
        return study.displaySets.find(ds => ds.SeriesInstanceUID === uid);
      })
      .filter(ds => ds !== undefined);
    const remainingDisplaySets = study.displaySets.filter(
      ds => ds !== undefined && !order.includes(ds.SeriesInstanceUID)
    );
    return Object.assign({}, study, {
      displaySets: orderedDisplaySets.concat(remainingDisplaySets),
    });
  };

  let orderedStudies = studies;
  if (order?.length) {
    orderedStudies = studies.map(sortDisplaySets);
  }
  return orderedStudies.map(study => {
    const { StudyInstanceUID } = study;

    const displaySets = thumbnailsService?.filterDisplaySet
      ? thumbnailsService.prepareToDisplay(study.displaySets)
      : study.displaySets;
    const thumbnails = displaySets.map(displaySet => {
      const {
        displaySetInstanceUID,
        SeriesDescription,
        InstanceNumber,
        numImageFrames,
        SeriesNumber,
      } = displaySet;

      let imageId;
      let altImageText;
      let imageRotation;

      if (displaySet.Modality && displaySet.Modality === 'SEG') {
        // TODO: We want to replace this with a thumbnail showing
        // the segmentation map on the image, but this is easier
        // and better than what we have right now.
        altImageText = 'SEG';
      } else if (displaySet?.isMultiFrame && displaySet?.numImageFrames > 1) {
        const imageIndex = Math.floor(displaySet.numImageFrames / 2);

        imageId = displaySet.images[0].getImageId(imageIndex);
      } else if (displaySet?.images?.length) {
        const imageIndex = Math.floor(displaySet.images.length / 2);

        imageId = displaySet.images[imageIndex].getImageId();
      } else if (
        displaySet.Modality &&
        displaySet.Modality === 'SM' &&
        displaySet.imageId
      ) {
        imageId = displaySet.imageId;
        imageRotation = displaySet.imageRotation
          ? displaySet.imageRotation
          : imageRotation;
      } else {
        altImageText = displaySet.Modality ? displaySet.Modality : 'UN';
      }

      return {
        imageId,
        altImageText,
        displaySetInstanceUID,
        SeriesDescription,
        InstanceNumber,
        numImageFrames,
        SeriesNumber,
        imageRotation,
      };
    });

    return {
      StudyInstanceUID,
      thumbnails,
    };
  });
};
