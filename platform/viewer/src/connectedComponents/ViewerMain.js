import './ViewerMain.css';
import { Component } from 'react';
import { ConnectedViewportGrid } from './../components/ViewportGrid/index.js';
import PropTypes from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router-dom';
import isEqual from 'lodash/isEqual';
import cornerstoneTools from 'cornerstone-tools';
import memoize from 'lodash/memoize';
import _values from 'lodash/values';
import { commandsManager, servicesManager } from './../App.js';
import store from './../store';

import {
  Components as FlywheelComponents,
  Redux as FlywheelRedux,
} from '@flywheel/extension-flywheel';
import OHIF from '@ohif/core';
import {isWebImageDataProtocol} from './isWebImageDataProtocol';

const { ProtocolEngineUtils, addCustomAttribute } = OHIF.hangingProtocols;
const { selectActiveProtocolStore } = FlywheelRedux.selectors;
const { setActiveProtocolStore } = FlywheelRedux.actions;
const { setViewportLayoutAndData, setViewportActive } = OHIF.redux.actions;
const { studyMetadataManager } = OHIF.utils;

const {
  ToolContextMenu: FlywheelToolContextMenu,
  DisplaySetDragDropMenuDialog,
} = FlywheelComponents;
var values = memoize(_values);

class ViewerMain extends Component {
  static propTypes = {
    activeViewportIndex: PropTypes.number.isRequired,
    match: PropTypes.object.isRequired,
    studies: PropTypes.array,
    viewportSpecificData: PropTypes.object.isRequired,
    layout: PropTypes.object.isRequired,
    setViewportSpecificData: PropTypes.func.isRequired,
    clearViewportSpecificData: PropTypes.func.isRequired,
    setProtocolLayout: PropTypes.func.isRequired,
    protocolLayout: PropTypes.object,
    waitForProtocol: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    this.state = {
      displaySets: [],
      protoColApplied: false,
      viewportSpanData: null,
    };
  }

  getDisplaySets(studies) {
    const displaySets = [];
    studies.forEach(study => {
      study.displaySets.forEach(dSet => {
        if (!dSet.plugin) {
          const _plugin = dSet.dataProtocol
            ? `cornerstone::${dSet.dataProtocol}`
            : 'cornerstone';
          dSet.plugin = _plugin;
        }
        displaySets.push(dSet);
      });
    });

    return displaySets;
  }

  findDisplaySet(studies, StudyInstanceUID, displaySetInstanceUID) {
    const study = studies.find(study => {
      return study.StudyInstanceUID === StudyInstanceUID;
    });

    if (!study) {
      return;
    }

    return study.displaySets.find(displaySet => {
      return displaySet.displaySetInstanceUID === displaySetInstanceUID;
    });
  }

  getMetaDataCollection(studies) {
    let isReady = false;
    const studyMetaDataCollection = [];
    studies.forEach(study => {
      const studyMetaData = studyMetadataManager.get(
        study.studyInstanceUID || study.StudyInstanceUID
      );
      if (studyMetaData) {
        studyMetaData.forEachSeries(series => {
          isReady = series.getFirstInstance() ? true : false;
        });
      }
      if (isReady) {
        studyMetaDataCollection.push(studyMetaData);
      }
    });
    return studyMetaDataCollection.length === studies.length
      ? studyMetaDataCollection
      : [];
  }

  componentDidMount() {
    // Add beforeUnload event handler to check for unsaved changes
    //window.addEventListener('beforeunload', unloadHandlers.beforeUnload);

    // Get all the display sets for the viewer studies
    if (this.props.studies) {
      const displaySets = this.getDisplaySets(this.props.studies);
      this.setState({ displaySets }, this.fillEmptyViewportPanes);
    }
  }

  componentDidUpdate(prevProps) {
    const prevViewportAmount = prevProps.layout.viewports.length;
    const viewportAmount = this.props.layout.viewports.length;
    const isVtk =
      this.props.layout.viewports.some(vp => !!vp.vtk) ||
      this.props.layout.viewports.some(vp => vp.plugin === 'vtk');
    const displaySets = this.props.studies
      ? this.getDisplaySets(this.props.studies)
      : [];

    if (this.props.protocolLayout !== prevProps.protocolLayout) {
      // Clear the viewports, wait for rendering to finish, then proceed to render viewports
      if (
        !this.props.protocolLayout ||
        this.props.protocolLayout.totalViewports !==
          this.state.displaySets.length
      ) {
        this.props.clearViewportSpecificData();
        return setTimeout(() => {
          this.setState(
            { displaySets, viewportSpanData: null },
            this.fillEmptyViewportPanes
          );
        }, 0);
      }
      this.fillEmptyViewportPanes();
      return;
    }

    const protocolStore = selectActiveProtocolStore(store.getState());
    const studyMetaDataCollection = this.getMetaDataCollection(
      this.props.studies
    );
    if (
      protocolStore &&
      !this.state.protoColApplied &&
      studyMetaDataCollection.length > 0 &&
      this.state.displaySets.length > 0
    ) {
      const cur = this;
      function setAllViewportSpecificData(
        layout,
        displaySets,
        viewportOrientations = null,
        viewportSpanData = null
      ) {
        // This sets the layout and viewport data
        if (viewportSpanData) {
          const protocolDisplaySets = [];

          Object.keys(displaySets).forEach(key =>
            protocolDisplaySets.push(displaySets[key])
          );

          cur.props.setProtocolLayout({
            viewports: layout.viewports,
            totalViewports: protocolDisplaySets.length,
            numRows: layout.numRows,
            numColumns: layout.numColumns,
          });
          store.dispatch(
            setActiveProtocolStore(
              protocolStore.projectId,
              protocolStore.protocolStore,
              false
            )
          );

          cur.setState(
            {
              displaySets: protocolDisplaySets,
              protoColApplied: true,
              viewportSpanData,
            },
            () => {
              if (viewportOrientations) {
                // Switch to protocol view with vtk js viewport only or a mix of vtk js and cornerstone viewports.
                setTimeout(() => {
                  commandsManager.runCommand('protocolView', {
                    viewportSpecificData: displaySets,
                    viewportProps: viewportOrientations,
                    layout: {
                      numRows: layout.numRows,
                      numColumns: layout.numColumns,
                      viewportProperties: layout.viewports,
                    },
                  });
                }, 0);
              } else {
                const viewports = [];

                layout.viewports.forEach(columnViewports => {
                  columnViewports.forEach(viewport => {
                    viewports.push({ readOnly: viewport.readOnly });
                  });
                });

                store.dispatch(
                  setViewportLayoutAndData(
                    {
                      numRows: layout.numRows,
                      numColumns: layout.numColumns,
                      viewports,
                    },
                    displaySets
                  )
                );
              }
            }
          );
        } else {
          if (!cur.state.protoColApplied) {
            if (!layout) {
              store.dispatch(
                setActiveProtocolStore(
                  protocolStore.projectId,
                  protocolStore.protocolStore,
                  false
                )
              );
              const displaySetsForStudies = cur.props.studies
                ? cur.getDisplaySets(cur.props.studies)
                : [];
              // Resets the original study display sets when no protocol layout data available.
              if (displaySetsForStudies.length > 0) {
                cur.setState({
                  displaySets: displaySetsForStudies,
                  protoColApplied: true,
                });
              } else {
                cur.setState({ protoColApplied: true });
              }
            } else {
              const protocolViewports = [];
              let columnViewports = [];
              layout.viewports.forEach((viewport, index) => {
                columnViewports.push(viewport);
                if (columnViewports.length === layout.numColumns) {
                  protocolViewports.push(columnViewports);
                  columnViewports = [];
                }
              });
              const protocolDisplaySets = [];
              Object.keys(displaySets).forEach(key =>
                protocolDisplaySets.push(displaySets[key])
              );
              cur.props.setProtocolLayout({
                viewports: protocolViewports,
                totalViewports: protocolDisplaySets.length,
                numRows: layout.numRows,
                numColumns: layout.numColumns,
              });
              store.dispatch(
                setActiveProtocolStore(
                  protocolStore.projectId,
                  protocolStore.protocolStore,
                  false
                )
              );
              cur.setState(
                {
                  displaySets: protocolDisplaySets,
                  protoColApplied: true,
                  viewportSpanData: null,
                },
                () => {
                  store.dispatch(setViewportLayoutAndData(layout, displaySets));
                }
              );
            }
          }
        }

        // A patch to adjust the viewport image to fit to the viewport on loading
        setTimeout(() => {
          cornerstone
            .getEnabledElements()
            .forEach(enabledElement =>
              cornerstone.resize(enabledElement.element)
            );
        }, 10);
      }

      const setDefaultViewport = layoutIndex => {
        if (layoutIndex > -1) {
          setTimeout(() => {
            store.dispatch(setViewportActive(layoutIndex));
          }, 0);
        }
      };

      const options = {
        setViewportSpecificData: this.props.setViewportSpecificData,
        clearViewportSpecificData: this.props.clearViewportSpecificData,
        setAllViewportSpecificData: setAllViewportSpecificData,
        setDefaultViewport: setDefaultViewport,
      };
      const dataProtocol = this.props.studies?.[0]?.dataProtocol;
      if (dataProtocol === 'nifti' || dataProtocol === 'metaimage') {
        // Adding custom implementation for retrieving the orientation of a nifti/metaimage display-set as the series description will be the orientation name for Nifti/metaimage.
        addCustomAttribute('orientation', 'orientation', instance => {
          return instance.getTagValue('SeriesDescription') || '';
        });
      }
      if (isWebImageDataProtocol(dataProtocol)) {
        addCustomAttribute('fileName', 'fileName', instance => {
          return instance.getTagValue('dataProtocol') === 'zip'
            ? instance.getSeriesInstanceUID()
            : instance.getStudyInstanceUID() || '';
        });
      }
      ProtocolEngineUtils.findMathingProtocolLayout(
        protocolStore.protocolStore,
        studyMetaDataCollection,
        options
      );
    } else if (
      this.state.protoColApplied &&
      this.props.waitForProtocol &&
      !this.props.protocolLayout
    ) {
      this.setState({ protoColApplied: false });
    }

    const stateNomEmptyDs = [];
    this.state.displaySets.forEach(stateDs => {
      if (
        stateDs.displaySetInstanceUID &&
        !stateNomEmptyDs.find(
          ds => ds.displaySetInstanceUID === stateDs.displaySetInstanceUID
        )
      ) {
        stateNomEmptyDs.push(stateDs);
      }
    });
    let isDisplaySetsAreSame =
      displaySets.length !== stateNomEmptyDs.length ? false : true;
    displaySets.forEach(ds => {
      if (
        !stateNomEmptyDs.find(
          stateDs => stateDs.SeriesInstanceUID === ds.SeriesInstanceUID
        )
      ) {
        isDisplaySetsAreSame = false;
      }
    });
    if (!protocolStore || this.state.protoColApplied || !isDisplaySetsAreSame) {
      if (
        this.props.studies !== prevProps.studies ||
        displaySets.length > this.state.displaySets.length ||
        (viewportAmount !== prevViewportAmount && !isVtk) ||
        !isEqual(this.props.protocolLayout, prevProps.protocolLayout)
      ) {
        this.fixStackScrolling();
        if (
          (!this.props.waitForProtocol || !isDisplaySetsAreSame) &&
          (!this.props.protocolLayout ||
            this.props.protocolLayout.totalViewports !==
              this.state.displaySets.length)
        ) {
          this.setState({ displaySets, viewportSpanData: null }, () => {
            this.fillEmptyViewportPanes();
          });
        } else {
          this.fillEmptyViewportPanes();
        }
      }
    }
  }

  fixStackScrolling = () => {
    const config = store.getState()?.flywheel?.projectConfig;
    const mouseActions = config?.mouseActions;
    if (config && mouseActions) {
      const wheelAction = mouseActions.find(
        action => action.button === 'wheel'
      );
      if (
        wheelAction?.toolName === 'StackScrollMouseWheel' ||
        wheelAction?.toolName === 'ZoomMouseWheel'
      ) {
        cornerstoneTools.setToolActive(wheelAction.toolName, {});
      } else {
        cornerstoneTools.setToolActive('StackScrollMouseWheel', {});
      }
    } else {
      cornerstoneTools.setToolActive('StackScrollMouseWheel', {}); // fix stack scrolling
    }
  };

  fillEmptyViewportPanes = () => {
    // TODO: Here is the entry point for filling viewports on load.
    const dirtyViewportPanes = [];
    const { layout, match, viewportSpecificData, protocolLayout } = this.props;
    let { displaySets } = this.state;

    if (!displaySets || !displaySets.length) {
      return;
    }

    const seriesInstanceUIDs =
      (match.params.seriesInstanceUIDs &&
        match.params.seriesInstanceUIDs.split(',')) ||
      [];
    const routeDisplaySets = seriesInstanceUIDs.map(uid =>
      displaySets.find(ds => ds.SeriesInstanceUID === uid)
    );
    const filteredRouteDisplaySets = routeDisplaySets.filter(ds => ds);
    displaySets = [
      ...filteredRouteDisplaySets,
      ...displaySets.filter(ds => !filteredRouteDisplaySets.includes(ds)),
    ];

    if (protocolLayout && protocolLayout.viewports) {
      return this.setupProtocolLayout(displaySets);
    }

    for (let i = 0; i < layout.viewports.length; i++) {
      const viewportPane = viewportSpecificData[i];
      const isNonEmptyViewport =
        viewportPane &&
        viewportPane.StudyInstanceUID &&
        viewportPane.displaySetInstanceUID &&
        !Object.values(viewportSpecificData).some(
          vp =>
            vp !== viewportPane &&
            vp.displaySetInstanceUID === viewportPane.displaySetInstanceUID
        );

      if (isNonEmptyViewport) {
        dirtyViewportPanes.push({
          StudyInstanceUID: viewportPane.StudyInstanceUID,
          displaySetInstanceUID: viewportPane.displaySetInstanceUID,
        });

        continue;
      }

      const foundDisplaySet =
        displaySets.find(
          ds => ds.displaySetInstanceUID === viewportPane?.displaySetInstanceUID
        ) ||
        displaySets.find(
          ds =>
            !dirtyViewportPanes.some(
              v => v.displaySetInstanceUID === ds.displaySetInstanceUID
            )
        ) ||
        displaySets[displaySets.length - 1];

      dirtyViewportPanes.push(foundDisplaySet);
    }

    dirtyViewportPanes.forEach((vp, i) => {
      if (vp && vp.StudyInstanceUID) {
        this.setViewportData({
          viewportIndex: i,
          StudyInstanceUID: vp.StudyInstanceUID,
          displaySetInstanceUID: vp.displaySetInstanceUID,
        });
      }
    });
  };

  buildLayout = () => {
    const numRows = this.props.protocolLayout.viewports.length;
    const numColumns = this.props.protocolLayout.viewports.reduce(
      (biggest, viewport) =>
        viewport.length > biggest ? viewport.length : biggest,
      0
    );
    return { numRows, numColumns, viewports: this.props.layout.viewports };
  };

  setupProtocolLayout = displaySets => {
    let viewportPanes = [];
    const { protocolLayout } = this.props;
    // Get the largest number of columns
    const numColumns = protocolLayout.viewports.reduce(
      (biggest, viewport) =>
        viewport.length > biggest ? viewport.length : biggest,
      0
    );
    let index = 0;
    for (let row = 0; row < protocolLayout.viewports.length; row++) {
      for (let col = 0; col < numColumns; col++) {
        const viewport = protocolLayout.viewports[row][col];
        if (!displaySets[index] || !viewport) {
          continue;
        }
        if (viewport.plugin === displaySets[index].plugin) {
          viewportPanes.push(displaySets[index]);
        } else {
          viewportPanes.push({});
        }
        index++;
      }
    }
    viewportPanes.forEach((vp, i) => {
      if (vp && vp.StudyInstanceUID && vp.displaySetInstanceUID) {
        if (
          this.props.protocolLayout.totalViewports !==
          this.state.displaySets.length
        ) {
          this.setViewportData({
            viewportIndex: i,
            StudyInstanceUID: vp.StudyInstanceUID,
            displaySetInstanceUID: vp.displaySetInstanceUID,
          });
        }
      } else if (
        !this.state.displaySets[i]?.displaySetInstanceUID &&
        this.props.viewportSpecificData[i]?.displaySetInstanceUID
      ) {
        this.props.clearViewportSpecificData(i);
      }
    });
  };

  setViewportData = ({
    viewportIndex,
    StudyInstanceUID,
    displaySetInstanceUID,
    dragDropEventInfo = null,
  }) => {
    let displaySet = this.findDisplaySet(
      this.props.studies,
      StudyInstanceUID,
      displaySetInstanceUID
    );

    if (displaySet.isDerived) {
      const { Modality } = displaySet;
      displaySet = displaySet.getSourceDisplaySet(this.props.studies);

      if (!displaySet) {
        throw new Error(
          `Referenced series for ${Modality} dataset not present.`
        );
      }
    }
    if (dragDropEventInfo) {
      const canPerformFusion = (sourceDs, targetDs) => {
        if (!sourceDs.isReconstructable || !targetDs.isReconstructable) {
          return false;
        }
        const sourceFirstImgMetaData = cornerstone.metaData.get(
          'imagePlaneModule',
          sourceDs.images?.[0]?.getImageId()
        );
        const targetFirstImgMetaData = cornerstone.metaData.get(
          'imagePlaneModule',
          targetDs.images?.[0]?.getImageId()
        );
        if (
          sourceFirstImgMetaData.frameOfReferenceUID !==
          targetFirstImgMetaData.frameOfReferenceUID
        ) {
          return false;
        }
        return true;
      };
      const PopupFromLeft = 100;
      const { viewportSpecificData, activeViewportIndex } = this.props;
      const activeDisplaySet = this.findDisplaySet(
        this.props.studies,
        viewportSpecificData[activeViewportIndex].StudyInstanceUID,
        viewportSpecificData[activeViewportIndex].displaySetInstanceUID
      );
      const isFusionApplicable = canPerformFusion(activeDisplaySet, displaySet);
      const clientOffset = dragDropEventInfo.clientOffset;
      const { UIDialogService } = servicesManager.services;
      let dialogId = UIDialogService.create({
        isDraggable: true,
        content: DisplaySetDragDropMenuDialog,
        defaultPosition: {
          x: clientOffset.x - PopupFromLeft,
          y: clientOffset.y,
        },
        useLastPosition: true,
        showOverlay: true,
        contentProps: {
          title: 'Select drag drop option',
          hideFusionButton: !isFusionApplicable,
          onApply: (applyFusion = false) => {
            UIDialogService.dismiss({ id: dialogId });
            if (applyFusion) {
              this.setState({
                overlayDisplaySet: displaySet,
                viewportSpanData: null,
              });
              setTimeout(() => {
                commandsManager.runCommand('mpr2dFusion', {});
              }, 2);
            } else {
              this.props.setViewportSpecificData(viewportIndex, displaySet);
            }
          },
          onClose: () => {
            UIDialogService.dismiss({ id: dialogId });
          },
        },
      });
    } else {
      this.setState({
        overlayDisplaySet: null,
      });
      this.props.setViewportSpecificData(viewportIndex, displaySet);
    }
  };

  render() {
    const { viewportSpecificData } = this.props;
    const viewportData = values(viewportSpecificData);
    const overlayViewportData = this.state.overlayDisplaySet
      ? [this.state.overlayDisplaySet]
      : [];

    return (
      <div className="ViewerMain">
        {this.state.displaySets.length && (
          <ConnectedViewportGrid
            isStudyLoaded={this.props.isStudyLoaded}
            studies={this.props.studies}
            viewportData={viewportData}
            overlayViewportData={overlayViewportData}
            setViewportData={this.setViewportData}
            viewportSpanData={this.state.viewportSpanData}
          >
            {/* Children to add to each viewport that support children */}
            <FlywheelToolContextMenu />
          </ConnectedViewportGrid>
        )}
      </div>
    );
  }

  componentWillUnmount() {
    // Clear the entire viewport specific data
    const { viewportSpecificData } = this.props;
    Object.keys(viewportSpecificData).forEach(viewportIndex => {
      this.props.clearViewportSpecificData(viewportIndex);
    });

    // TODO: These don't have to be viewer specific?
    // Could qualify for other routes?
    // hotkeys.destroy();

    // Remove beforeUnload event handler...
    //window.removeEventListener('beforeunload', unloadHandlers.beforeUnload);
    // Destroy the synchronizer used to update reference lines
    //OHIF.viewer.updateImageSynchronizer.destroy();
    // TODO: Instruct all plugins to clean up themselves
    //
    // Clear references to all stacks in the StackManager
    //StackManager.clearStacks();
    // @TypeSafeStudies
    // Clears OHIF.viewer.Studies collection
    //OHIF.viewer.Studies.removeAll();
    // @TypeSafeStudies
    // Clears OHIF.viewer.StudyMetadataList collection
    //OHIF.viewer.StudyMetadataList.removeAll();
  }
}

export default withRouter(ViewerMain);
