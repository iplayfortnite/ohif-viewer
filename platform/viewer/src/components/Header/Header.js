import React, { useState, useEffect } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Dropdown, AboutContent, withModal } from '@ohif/ui';
import {
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
//
import { UserPreferences } from './../UserPreferences';
import OHIFLogo from '../OHIFLogo/OHIFLogo.js';
import './Header.css';

const { getRouteParams } = FlywheelCommonUtils;

function Header(props) {
  const {
    t,
    user,
    userManager,
    modal: { show },
    match,
    home,
    useLargeLogo,
    linkPath,
    linkText,
    location,
    children,
    projectConfig,
  } = props;

  const [options, setOptions] = useState([]);
  const hasLink = linkText && linkPath;
  const { taskId } = getRouteParams();

  useEffect(() => {
    const optionsValue = [
      {
        title: t('About'),
        icon: { name: 'info' },
        onClick: () =>
          show({
            content: AboutContent,
            title: t('OHIF Viewer - About'),
          }),
      },
      {
        title: t('Preferences'),
        icon: {
          name: 'user',
        },
        onClick: () =>
          show({
            content: UserPreferences,
            title: t('User Preferences'),
          }),
      },
    ];

    if (user && userManager) {
      optionsValue.push({
        title: t('Logout'),
        icon: { name: 'power-off' },
        onClick: () => userManager.signoutRedirect(),
      });
    }

    setOptions(optionsValue);
  }, [setOptions, show, t, user, userManager]);

  const { projectId } = match.params;
  const studyListPath = projectId ? `/project/${projectId}` : '/';

  let newTabLink;
  let closeButtonMargin = 0;
  if ((window.self !== window.top) && (!projectConfig?.toolbar?.hideOpenNewTabLink)) {
    newTabLink = (
      <a href={window.location.href} target="_blank" className="new-tab">
        Open in New Tab ↗
      </a>
    );
    closeButtonMargin = 25;
  }

  // ANTD -- Hamburger, Drawer, Menu
  return (
    <>
      <div className="notification-bar">{t('INVESTIGATIONAL USE ONLY')}</div>
      <div
        className={classNames('entry-header', { 'header-big': useLargeLogo })}
      >
        <div className="header-left-box">
          {location && location.studyLink && (
            <Link
              to={location.studyLink}
              className="header-btn header-viewerLink"
            >
              {t('Back to Viewer')}
            </Link>
          )}

          {children}

          {(hasLink && !taskId) && (
            <Link
              className="header-btn header-studyListLinkSection"
              to={{
                pathname: studyListPath,
                state: { studyLink: location.pathname },
              }}
            >
              {t(linkText)}
            </Link>
          )}
        </div>

        <div className="header-menu" style={{ marginRight: closeButtonMargin }}>
          {newTabLink}
          <span className="research-use">{t('INVESTIGATIONAL USE ONLY')}</span>
          {/* Hide options menu
          <Dropdown title={t('Options')} list={options} align="right" />
          */}
        </div>
      </div>
    </>
  );
}

Header.propTypes = {
  // Study list, /
  linkText: PropTypes.string,
  linkPath: PropTypes.string,
  useLargeLogo: PropTypes.bool,
  //
  location: PropTypes.object.isRequired,
  children: PropTypes.node,
  t: PropTypes.func.isRequired,
  userManager: PropTypes.object,
  user: PropTypes.object,
  modal: PropTypes.object,
};

Header.defaultProps = {
  useLargeLogo: false,
  children: OHIFLogo(),
};

export default withTranslation(['Header', 'AboutModal'])(
  withRouter(withModal(Header))
);
