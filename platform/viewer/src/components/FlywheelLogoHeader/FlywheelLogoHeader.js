import './FlywheelLogoHeader.css';
import { Tooltip, OverlayTrigger, Icon, ToolbarButton } from '@ohif/ui';
import React from 'react';
import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';
import store from '@ohif/viewer/src/store';

const { selectProjectConfig } = FlywheelRedux.selectors;

class FlywheelLogoHeader extends React.Component {
  render() {
    let newTabLink;
    const projectConfig = selectProjectConfig(store.getState());
    const showNewTabLink =
      window.self !== window.top && !projectConfig?.toolbar?.hideOpenNewTabLink;
    if (showNewTabLink) {
      newTabLink = (
        <div className="new-tab-link">
          <a href={window.location.href} target="_blank" title="New Tab">
            <ToolbarButton
              key="NewTab"
              label="New Tab"
              icon="open_in_new"
              class="material-icons"
            ></ToolbarButton>
          </a>
        </div>
      );
    }

    return (
      <div className="flywheel-logo-header">
        <div className="brand-logo">
          <Icon name="flywheel-logo" className="flywheel-logo" />
        </div>
        <div className="buttons-wrapper">
          <div className="investigation-use">
            <span name="info" className="material-icons">
              info
            </span>
            <OverlayTrigger
              key="investigation-use"
              placement="bottom"
              delayHide={1500}
              overlay={
                <Tooltip
                  placement="top"
                  className="in tooltip-info"
                  id="tooltip-left"
                >
                  <div className="tooltip-content">
                    <span>
                      This imaging viewer is based on an open source,
                      zero-footprint DICOM viewer that is licensed by MIT. For
                      additional details please visit the link provided below.
                    </span>
                    <span
                      style={{ marginTop: '10px', textDecoration: 'underline' }}
                    >
                      <a
                        href="https://github.com/ohif"
                        target="_blank"
                        className="github"
                      >
                        github.com/ohif
                      </a>
                    </span>
                  </div>
                </Tooltip>
              }
            >
              <span className="details-subheader-label">Details</span>
            </OverlayTrigger>
          </div>
          {newTabLink}
        </div>
      </div>
    );
  }
}

export default FlywheelLogoHeader;
