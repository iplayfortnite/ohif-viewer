import OHIF from '@ohif/core';
import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';

import {
  getToolLabellingFlowCallback,
  getOnRightClickCallback,
  getOnTouchPressCallback,
  getResetLabellingAndContextMenu,
} from './labelingFlowCallbacks.js';

import {
  Reactive as FlywheelReactive,
  Utils as FlywheelUtils,
  Components as FlywheelComponents,
} from '@flywheel/extension-flywheel';
import _ from 'lodash';
import {
  Tools as FlywheelCommonTools,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';

// TODO to revisit
// TODO: This only works because we have a hard dependency on this extension
// We need to decouple and make stuff like this possible w/o bundling this at
// build time
import store from './../../store';
//import LabellingFlow from '../../components/Labelling/LabellingFlow';
//import ToolContextMenu from '../../connectedComponents/ToolContextMenu';
const { ContourCollisionDialog } = FlywheelComponents;
const { getBoundaryErrorMessage } = FlywheelCommonUtils;
const {
  onAdded,
  onRemoved,
  onModified,
} = OHIF.measurements.MeasurementHandlers;

const { next: handleMeasurementEvent } = FlywheelReactive.measurements;
const {
  handleMeasurementModified: onModifiedMeasurement,
  _deferOnMeasurementModified,
} = FlywheelUtils.Measurements;

/**
 *
 *
 * @export
 * @param {Object} servicesManager
 * @param {Object} configuration
 */
export default function init({
  servicesManager,
  commandsManager,
  configuration,
  hotkeysManager,
}) {
  const callContourCollisionDialog = (
    measurementData,
    overLappedMeasurements,
    boundaryMeasurements
  ) => {
    const { UIDialogService } = servicesManager.services;
    const boundaryError = getBoundaryErrorMessage(
      boundaryMeasurements,
      measurementData
    );
    UIDialogService.dismiss({ id: 'collisionError' });
    const dialogId = UIDialogService.create({
      id: 'collisionError',
      content: ContourCollisionDialog,
      defaultPosition: { x: 420, y: 420 },
      showOverlay: true,
      contentProps: {
        measurementData: { ...measurementData },
        overlappedMeasurements: overLappedMeasurements,
        label:
          'Overlapping contours are not allowed based on viewer configuration. How would you like to proceed?' +
          boundaryError,
        onClose: () => UIDialogService.dismiss({ id: 'collisionError' }),
      },
    });
  };
  const MEASUREMENT_ACTION_MAP = {
    added: onAdded,
    removed: onRemoved,
    modified: onModifiedMeasurement(
      event => onModified(event, callContourCollisionDialog),
      300
    ),
  };
  const { UIDialogService } = servicesManager.services;
  const callInputDialog = (data, event, callback) => {
    let dialogId = UIDialogService.create({
      content: SimpleDialog.InputDialog,
      defaultPosition: {
        x: (event && event.currentPoints.canvas.x) || 0,
        y: (event && event.currentPoints.canvas.y) || 0,
      },
      showOverlay: true,
      contentProps: {
        title: 'Enter your annotation',
        label: 'New label',
        defaultValue: data ? data.text : '',
        onClose: () => UIDialogService.dismiss({ id: dialogId }),
        onSubmit: value => {
          callback(value);
          UIDialogService.dismiss({ id: dialogId });
        },
      },
    });
  };

  const getHotKeyConfigForCommand = commandName => {
    return hotkeysManager.getHotKeyDefinition(commandName);
  };

  const showCollisionDialog = (evt, measurement, element) => {
    onModified(
      {
        detail: {
          toolName: measurement.toolType,
          measurementData: measurement,
          element,
        },
      },
      callContourCollisionDialog
    );
    evt.detail.measurementData = measurement;
    evt.detail.toolName = measurement.toolType;
    _deferOnMeasurementModified(
      evt,
      onMeasurementsChanged.bind(this, 'modified')
    );
  };

  // If these tools were already added by a different extension, we want to replace
  // them with the same tools that have an alternative configuration. By passing in
  // our custom `getMeasurementLocationCallback`, we can...
  const toolLabellingFlowCallback = getToolLabellingFlowCallback(
    callContourCollisionDialog,
    store
  );

  // Removes all tools from all enabled elements w/ provided name
  // Not commonly used API, so :eyes: for unknown side-effects
  csTools.removeTool('Rotate');
  csTools.removeTool('Bidirectional');
  csTools.removeTool('Length');
  csTools.removeTool('Parallel');
  csTools.removeTool('Angle');
  csTools.removeTool('FreehandRoi');
  csTools.removeTool('OpenFreehandRoi');
  csTools.removeTool('EllipticalRoi');
  csTools.removeTool('CircleRoi');
  csTools.removeTool('RectangleRoi');
  csTools.removeTool('ArrowAnnotate');
  csTools.removeTool('FreehandRoiSculptor');
  csTools.removeTool('Zoom');
  csTools.removeTool('Pan');
  // Re-add each tool w/ our custom configuration
  csTools.addTool(csTools.RotateTool, {
    configuration: {
      getMeasurementLocationCallback: toolLabellingFlowCallback,
    },
  });
  csTools.addTool(FlywheelCommonTools.BidirectionalTool, {
    configuration: {
      getMeasurementLocationCallback: toolLabellingFlowCallback,
    },
  });
  csTools.addTool(FlywheelCommonTools.LengthTool, {
    configuration: {
      getMeasurementLocationCallback: toolLabellingFlowCallback,
    },
  });
  csTools.addTool(FlywheelCommonTools.ParallelTool, {
    configuration: {
      getMeasurementLocationCallback: toolLabellingFlowCallback,
    },
  });
  csTools.addTool(FlywheelCommonTools.AngleTool, {
    configuration: {
      getMeasurementLocationCallback: toolLabellingFlowCallback,
    },
  });
  csTools.addTool(FlywheelCommonTools.FreehandRoiTool, {
    configuration: {
      getMeasurementLocationCallback: toolLabellingFlowCallback,
    },
  });
  csTools.addTool(FlywheelCommonTools.CircleCursorTool, {
    configuration: {},
  });

  // let hotKeyConfig = null;
  // if (hotkeysManager?.hotkeyDefaults) {
  //   hotKeyConfig = hotkeysManager?.hotkeyDefaults.find(x => x.commandName === "fragmenterKey");
  // }
  csTools.addTool(FlywheelCommonTools.OpenFreehandRoiTool, {
    configuration: {
      getMeasurementLocationCallback: toolLabellingFlowCallback,
      getHotKeyConfigForCommand,
    },
  });
  csTools.addTool(FlywheelCommonTools.EllipticalRoiTool, {
    configuration: {
      getMeasurementLocationCallback: toolLabellingFlowCallback,
    },
  });
  csTools.addTool(FlywheelCommonTools.CircleRoiTool, {
    configuration: {
      getMeasurementLocationCallback: toolLabellingFlowCallback,
    },
  });
  csTools.addTool(FlywheelCommonTools.RectangleRoiTool, {
    configuration: {
      getMeasurementLocationCallback: toolLabellingFlowCallback,
    },
  });
  csTools.addTool(FlywheelCommonTools.ArrowAnnotateTool, {
    configuration: {
      // disable ugly text prompts
      getTextCallback: () => {},
      changeTextCallback: () => {},
      getMeasurementLocationCallback: toolLabellingFlowCallback,
      getTextCallback: (callback, eventDetails) => {},
      changeTextCallback: (data, eventDetails, callback) => {},
    },
  });
  csTools.addTool(FlywheelCommonTools.FreehandRoiSculptorTool, {
    configuration: {
      getHotKeyConfigForCommand,
      showCollisionDialog,
    },
  });

  csTools.removeTool('ContourRoi');

  csTools.addTool(FlywheelCommonTools.ContourRoiTool, {
    configuration: {
      getMeasurementLocationCallback: toolLabellingFlowCallback,
    },
  });
  csTools.addTool(FlywheelCommonTools.ZoomTool, {
    configuration: {},
  });
  csTools.addTool(FlywheelCommonTools.PanTool, {
    configuration: {},
  });

  // TODO: MEASUREMENT_COMPLETED (not present in initial implementation)
  const onMeasurementsChanged = (action, event) => {
    handleMeasurementEvent(event);
    return MEASUREMENT_ACTION_MAP[action]?.(event);
  };
  const onMeasurementAdded = onMeasurementsChanged.bind(this, 'added');
  const onMeasurementRemoved = onMeasurementsChanged.bind(this, 'removed');
  const onMeasurementModified = onMeasurementsChanged.bind(this, 'modified');
  const onMeasurementCompleted = onMeasurementsChanged.bind(this, 'completed');
  const onLabelmapModified = onMeasurementsChanged.bind(
    this,
    'labelmapModified'
  );

  /** Override default OHIF behavior */
  const onRightClick = getOnRightClickCallback(store);
  const onTouchPress = getOnTouchPressCallback(store);
  const onNewImage = getResetLabellingAndContextMenu(store);
  const onMouseClick = getResetLabellingAndContextMenu(store);
  const onTouchStart = getResetLabellingAndContextMenu(store);

  const _getDefaultPosition = event => ({
    x: (event && event.currentPoints.client.x) || 0,
    y: (event && event.currentPoints.client.y) || 0,
  });

  const _updateLabellingHandler = (labellingData, measurementData) => {
    const { location, description, response } = labellingData;

    if (location) {
      measurementData.location = location;
    }

    measurementData.description = description || '';

    if (response) {
      measurementData.response = response;
    }

    commandsManager.runCommand(
      'updateTableWithNewMeasurementData',
      measurementData
    );
  };

  const showLabellingDialog = (props, contentProps, measurementData) => {
    if (!UIDialogService) {
      console.warn('Unable to show dialog; no UI Dialog Service available.');
      return;
    }

    UIDialogService.create({
      id: 'labelling',
      isDraggable: false,
      showOverlay: true,
      centralize: true,
      content: null, //LabellingFlow,
      contentProps: {
        measurementData,
        labellingDoneCallback: () =>
          UIDialogService.dismiss({ id: 'labelling' }),
        updateLabelling: labellingData =>
          _updateLabellingHandler(labellingData, measurementData),
        ...contentProps,
      },
      ...props,
    });
  };

  const _onRightClick = event => {
    if (!UIDialogService) {
      console.warn('Unable to show dialog; no UI Dialog Service available.');
      return;
    }

    UIDialogService.dismiss({ id: 'context-menu' });
    UIDialogService.create({
      id: 'context-menu',
      isDraggable: false,
      preservePosition: false,
      defaultPosition: _getDefaultPosition(event.detail),
      content: null, //ToolContextMenu,
      contentProps: {
        eventData: event.detail,
        onDelete: (nearbyToolData, eventData) => {
          const element = eventData.element;
          commandsManager.runCommand('removeToolState', {
            element,
            toolType: nearbyToolData.toolType,
            tool: nearbyToolData.tool,
          });
        },
        onClose: () => UIDialogService.dismiss({ id: 'context-menu' }),
        onSetLabel: (eventData, measurementData) => {
          showLabellingDialog(
            { centralize: true, isDraggable: false },
            { skipAddLabelButton: true, editLocation: true },
            measurementData
          );
        },
        onSetDescription: (eventData, measurementData) => {
          showLabellingDialog(
            { defaultPosition: _getDefaultPosition(eventData) },
            { editDescriptionOnDialog: true },
            measurementData
          );
        },
      },
    });
  };

  const _onTouchPress = event => {
    if (!UIDialogService) {
      console.warn('Unable to show dialog; no UI Dialog Service available.');
      return;
    }

    UIDialogService.create({
      eventData: event.detail,
      content: null, //ToolContextMenu,
      contentProps: {
        isTouchEvent: true,
      },
    });
  };

  const _onTouchStart = () => _resetLabelligAndContextMenu();

  const _onMouseClick = () => _resetLabelligAndContextMenu();

  const _resetLabelligAndContextMenu = () => {
    if (!UIDialogService) {
      console.warn('Unable to show dialog; no UI Dialog Service available.');
      return;
    }

    UIDialogService.dismiss({ id: 'context-menu' });
    UIDialogService.dismiss({ id: 'labelling' });
  };

  // TODO: This makes scrolling painfully slow
  // const onNewImage = ...

  /*
   * Because click gives us the native "mouse up", buttons will always be `0`
   * Need to fallback to event.which;
   *
   */
  const handleClick = cornerstoneMouseClickEvent => {
    const mouseUpEvent = cornerstoneMouseClickEvent.detail.event;
    const isRightClick = mouseUpEvent.which === 3;

    if (isRightClick) {
      onRightClick(cornerstoneMouseClickEvent);
    } else {
      onMouseClick(cornerstoneMouseClickEvent);
    }
  };

  function elementEnabledHandler(evt) {
    const element = evt.detail.element;

    element.addEventListener(csTools.EVENTS.MOUSE_UP, onMeasurementAdded);
    element.addEventListener(
      csTools.EVENTS.MEASUREMENT_ADDED,
      onMeasurementAdded
    );
    element.addEventListener(
      csTools.EVENTS.MEASUREMENT_REMOVED,
      onMeasurementRemoved
    );
    element.addEventListener(
      csTools.EVENTS.MEASUREMENT_MODIFIED,
      onMeasurementModified
    );
    element.addEventListener(
      csTools.EVENTS.LABELMAP_MODIFIED,
      onLabelmapModified
    );
    element.addEventListener(
      csTools.EVENTS.MEASUREMENT_COMPLETED,
      onMeasurementCompleted
    );

    element.addEventListener(csTools.EVENTS.TOUCH_PRESS, onTouchPress);
    element.addEventListener(csTools.EVENTS.MOUSE_CLICK, handleClick);
    element.addEventListener(csTools.EVENTS.TOUCH_START, onTouchStart);

    // TODO: This makes scrolling painfully slow
    // element.addEventListener(cornerstone.EVENTS.NEW_IMAGE, onNewImage);
  }

  function elementDisabledHandler(evt) {
    const element = evt.detail.element;

    element.removeEventListener(csTools.EVENTS.MOUSE_DRAG, onMeasurementAdded);
    element.removeEventListener(csTools.EVENTS.MOUSE_UP, onMeasurementAdded);
    element.removeEventListener(
      csTools.EVENTS.MEASUREMENT_ADDED,
      onMeasurementAdded
    );
    element.removeEventListener(
      csTools.EVENTS.MEASUREMENT_REMOVED,
      onMeasurementRemoved
    );
    element.removeEventListener(
      csTools.EVENTS.MEASUREMENT_MODIFIED,
      onMeasurementModified
    );
    element.removeEventListener(
      csTools.EVENTS.LABELMAP_MODIFIED,
      onLabelmapModified
    );
    element.removeEventListener(
      csTools.EVENTS.MEASUREMENT_COMPLETED,
      onMeasurementCompleted
    );

    element.removeEventListener(csTools.EVENTS.TOUCH_PRESS, onTouchPress);
    element.removeEventListener(csTools.EVENTS.MOUSE_CLICK, handleClick);
    element.removeEventListener(csTools.EVENTS.TOUCH_START, onTouchStart);

    // TODO: This makes scrolling painfully slow
    // element.removeEventListener(cornerstone.EVENTS.NEW_IMAGE, onNewImage);
  }

  cornerstone.events.addEventListener(
    cornerstone.EVENTS.ELEMENT_ENABLED,
    elementEnabledHandler
  );
  cornerstone.events.addEventListener(
    cornerstone.EVENTS.ELEMENT_DISABLED,
    elementDisabledHandler
  );
}
