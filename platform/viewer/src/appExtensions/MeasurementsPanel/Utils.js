import moment from 'moment';
import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';

import OHIF, { DICOMSR, utils } from '@ohif/core';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import jumpToRowItem from './jumpToRowItem.js';
import store from '../../store/index.js';

const { studyMetadataManager } = utils;
const { MeasurementApi, getImageIdForImagePath } = OHIF.measurements;

/**
 * Takes a list of objects and a property and return the list grouped by the property
 *
 * @param {Array} list - The objects to be grouped by
 * @param {string} props - The property to group the objects
 * @returns {Object}
 */
function groupBy(list, props) {
  return list.reduce((a, b) => {
    (a[b[props]] = a[b[props]] || []).push(b);
    return a;
  }, {});
}

/**
 *  Takes a list of tools grouped and return all tools separately
 *
 * @param {Array} [toolGroups=[]] - The grouped tools
 * @returns {Array} - The list of all tools on all groups
 */
function getAllTools(toolGroups = []) {
  let tools = [];
  toolGroups.forEach(toolGroup => (tools = tools.concat(toolGroup.childTools)));

  return tools;
}

/**
 * Takes measurementData and build the measurement text to be used into the table
 *
 * @param {Object} [measurementData={}]
 * @param {string} measurementData.location - The measurement location
 * @param {string} measurementData.description - The measurement description
 * @returns {string}
 */
function getMeasurementText(measurementData = {}) {
  const defaultText = '...';
  const { location = '', description = '' } = measurementData;
  const result = location + (description ? ` (${description})` : '');

  return result || defaultText;
}

/**
 * Takes a list of measurements grouped by measurement numbers and return each measurement data by available timepoint
 *
 * @param {Array} measurementNumberList - The list of measurements
 * @param {Array} timepoints - The list of available timepoints
 * @param {Function} displayFunction - The function that builds the display text by each tool
 * @returns
 */
function getDataForEachMeasurementNumber(
  measurementNumberList,
  timepoints,
  displayFunction
) {
  const data = [];
  // on each measurement number we should get each measurement data by available timepoint
  measurementNumberList.forEach(measurement => {
    timepoints.forEach(timepoint => {
      const eachData = {
        displayText: '...',
      };
      if (measurement.timepointId === timepoint.timepointId) {
        eachData.displayText = displayFunction(measurement);
      }
      data.push(eachData);
    });
  });

  return data;
}

/**
 * Take a measurement toolName and return if any warnings
 *
 * @param {string} toolName - The tool name
 * @returns {string}
 */
function getWarningsForMeasurement(toolName) {
  const isToolSupported = DICOMSR.isToolSupported(toolName);

  return {
    hasWarnings: !isToolSupported,
    warningTitle: isToolSupported ? '' : 'Unsupported Tool',
    warningList: isToolSupported
      ? []
      : [`${toolName} cannot be persisted at this time`],
  };
}

function getTotalSlice(measurementData) {
  const state = store.getState();
  let totalSlice = -1;
  const viewportsState = state.viewports;
  const timepointManagerState = state.timepointManager;

  const options = {
    invertViewportTimepointsOrder: false,
    childToolKey: null,
  };

  const actionData = jumpToRowItem(
    measurementData,
    viewportsState,
    timepointManagerState,
    options
  );
  const displaySet = actionData.viewportSpecificData[0]?.displaySet;
  if (displaySet?.numImageFrames >= 1) {
    totalSlice = displaySet.numImageFrames;
  } else {
    actionData.viewportSpecificData.forEach(viewportData => {
      const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
        viewportData.viewportIndex
      );
      if (element) {
        const toolState = cornerstoneTools.getToolState(element, 'stack');
        const stackData = toolState.data[0];
        totalSlice = stackData.imageIds.length;
      }
    });
  }
  return totalSlice;
}

function getSliceInfo(measurementData) {
  let totalSlice = -1;
  const dataStudyInstanceUID = measurementData.StudyInstanceUID;
  const study = studyMetadataManager.get(dataStudyInstanceUID);
  if (study) {
    const dataSOPInstanceUID =
      measurementData.SOPInstanceUID || measurementData.sopInstanceUid;
    const displaySet = study.findDisplaySet(displaySet => {
      return displaySetContainsSopInstance(displaySet, dataSOPInstanceUID);
    });

    if (displaySet) {
      totalSlice = getTotalSlice(measurementData);
    }
  }

  let sliceNumber = measurementData.sliceNumber;
  if (!sliceNumber) {
    const imageId = getImageIdForImagePath(measurementData.imagePath);
    const studyMetadata = studyMetadataManager.get(dataStudyInstanceUID);
    studyMetadata?.findDisplaySet((displaySet, index) => {
      totalSlice = displaySet.images.length;
      displaySet.images.forEach((x, imageIndex) => {
        if (imageId === x._data.url) {
          sliceNumber = imageIndex + 1;
        }
      });
    });
  }

  return { totalSlice, sliceNumber };
}

/**
 * Takes a list of objects and a property and return the list grouped by the property
 *
 * @param {Array} list - The objects to be grouped by
 * @param {string} props - The property to group the objects
 * @param {string} taskId -
 * @returns {Object}
 */
function groupByForMultiTask(list, props, taskId) {
  return list.reduce((a, b) => {
    if (b['task_id'] === taskId) {
      (a[b[props]] = a[b[props]] || []).push(b);
    }
    return a;
  }, {});
}

function getGroupedMeasurementsKey(
  groupedMeasurements,
  key,
  groupedMeasurementsForTask
) {
  if (groupedMeasurementsForTask[key].length > 1) {
    groupedMeasurements[key].push(groupedMeasurementsForTask[key][0]);
  } else {
    groupedMeasurements[key].push(...groupedMeasurementsForTask[key]);
  }
  return groupedMeasurements;
}

/**
 * Take measurements from MeasurementAPI structure and convert into a measurementTable structure
 *
 * @param {Object} toolCollections - The list of all measurement grouped by groupTool and toolName
 * @param {Array} timepoints - The list of available timepoints
 * @returns
 */
function convertMeasurementsToTableData(
  toolCollections,
  timepoints,
  users,
  enableMeasurementTableWarning
) {
  const config = MeasurementApi.getConfiguration();
  const toolGroups = config.measurementTools;
  const tools = getAllTools(toolGroups);
  const state = store.getState();

  const tableMeasurements = toolGroups.map(toolGroup => {
    return {
      groupName: toolGroup.name,
      groupId: toolGroup.id,
      measurements: [],
    };
  });

  Object.keys(toolCollections).forEach(toolId => {
    const toolMeasurements = toolCollections[toolId];
    const tool = tools.find(tool => tool.id === toolId);
    const { displayFunction } = tool.options.measurementTable;

    let groupedMeasurements = {};
    if (state.multipleReaderTask.TaskIds.length > 1) {
      state.multipleReaderTask.TaskIds.forEach(taskId => {
        // Group by measurementNumber so we can display then all in the same line
        const groupedMeasurementsForTask = groupByForMultiTask(
          toolMeasurements,
          'measurementNumber',
          taskId
        );
        Object.keys(groupedMeasurementsForTask).forEach(key => {
          if (!Array.isArray(groupedMeasurements[key])) {
            groupedMeasurements[key] = [];
          }
          groupedMeasurements = getGroupedMeasurementsKey(
            groupedMeasurements,
            key,
            groupedMeasurementsForTask
          );
        });
      });
    } else {
      // Group by measurementNumber so we can display then all in the same line
      groupedMeasurements = groupBy(toolMeasurements, 'measurementNumber');
    }

    Object.keys(groupedMeasurements).forEach(groupedMeasurementsIndex => {
      const measurementNumberList =
        groupedMeasurements[groupedMeasurementsIndex];
      const multiTaskMeasurements =
        state.multipleReaderTask.TaskIds.length > 1
          ? measurementNumberList
          : [measurementNumberList[0]];
      multiTaskMeasurements.forEach(measurement => {
        const measurementData = measurement;
        let totalSlice = -1;
        const dataStudyInstanceUID = measurementData.StudyInstanceUID;
        const study = studyMetadataManager.get(dataStudyInstanceUID);
        if (study) {
          const dataSOPInstanceUID =
            measurementData.SOPInstanceUID || measurementData.sopInstanceUid;
          const displaySet = study.findDisplaySet(displaySet => {
            return displaySetContainsSopInstance(
              displaySet,
              dataSOPInstanceUID
            );
          });

          if (displaySet) {
            if (displaySet.images?.length) {
              totalSlice = displaySet.images?.length;
              if (displaySet.isMultiFrame && displaySet.numImageFrames > 1) {
                totalSlice = displaySet.numImageFrames;
              }
            } else {
              totalSlice = getTotalSlice(measurementData);
            }
          }
        }

        const {
          user,
          measurementNumber,
          lesionNamingNumber,
          toolType,
          sliceNumber,
          visible,
          readonly,
          description,
          location,
          answer,
          question,
          questionKey,
          subFormName,
          subFormAnnotationId,
          isSubForm,
          uuid,
        } = measurementData;
        const measurementId = measurementData._id;
        let slice_number = sliceNumber;
        if (!slice_number) {
          const imageId = getImageIdForImagePath(measurementData.imagePath);
          const studyMetadata = studyMetadataManager.get(dataStudyInstanceUID);
          studyMetadata?.findDisplaySet((displaySet, index) => {
            totalSlice = displaySet.images?.length;
            displaySet.images?.forEach((x, imageIndex) => {
              if (imageId === x._data.url) {
                slice_number = imageIndex + 1;
              }
            });
          });
        }

        const { hasWarnings = false, warningTitle = '', warningList = [] } =
          (enableMeasurementTableWarning &&
            getWarningsForMeasurement(toolType)) ||
          {};

        //check if all measurements with same measurementNumber will have same LABEL
        const tableMeasurement = {
          user,
          itemNumber: lesionNamingNumber,
          label: getMeasurementText(measurementData),
          measurementId,
          measurementNumber,
          lesionNamingNumber,
          toolType,
          hasWarnings,
          warningTitle,
          warningList,
          isSplitLesion: false,
          sliceNumber: slice_number,
          totalSlice,
          visible,
          readonly,
          description: description || '',
          location,
          data: getDataForEachMeasurementNumber(
            measurementNumberList,
            timepoints,
            displayFunction
          ),
          answer,
          question,
          questionKey,
          subFormName,
          subFormAnnotationId,
          isSubForm,
          uuid,
          ROIContourUid: measurementData.ROIContourUid,
        };
        // Add user info if it exists
        if (
          measurementData.flywheelOrigin &&
          measurementData.flywheelOrigin.id &&
          users
        ) {
          tableMeasurement.user = users[measurementData.flywheelOrigin.id];
        }

        // find the group object for the tool
        const toolGroupMeasurements = tableMeasurements.find(group => {
          return group.groupId === tool.toolGroup;
        });
        // inject the new measurement for this measurementNumer
        toolGroupMeasurements.measurements.push(tableMeasurement);
      });
    });
  });

  // Sort measurements by lesion naming number
  tableMeasurements.forEach(tm => {
    tm.measurements.sort((m1, m2) =>
      m1.lesionNamingNumber > m2.lesionNamingNumber ? 1 : -1
    );
  });

  return tableMeasurements;
}

/**
 * To get displaySet
 * @param {Object} displaySet
 * @param {String} SOPInstanceUID
 * @returns
 */
const displaySetContainsSopInstance = (displaySet, SOPInstanceUID) => {
  if (!displaySet.images || !displaySet.images.length) {
    return;
  }

  return displaySet.images.find(
    image => image.getSOPInstanceUID() === SOPInstanceUID
  );
};

/**
 * Take a list of available timepoints and return a list header information for each timepoint
 *
 * @param {Array} timepoints - The list of available timepoints
 * @param {string} timepoints[].latestDate - The date of the last study taken on the timepoint
 * @returns {{label: string, key: string, date: string}[]}
 */
function convertTimepointsToTableData(timepoints) {
  if (!timepoints || !timepoints.length) {
    return [];
  }

  return [
    {
      label: 'Study date:',
      key: 'StudyDate',
      date: moment(timepoints[0].latestDate).format('DD-MMM-YY'),
    },
  ];
}

/**
 *  Takes server type and return a function or undefined
 *
 * @param {string} serverType - The server type
 * @returns {undefined|Function}
 */
function getSaveFunction(serverType) {
  if (serverType === 'dicomWeb') {
    return () => {
      const measurementApi = MeasurementApi.Instance;
      const promise = measurementApi.storeMeasurements();
      return promise;
    };
  }
}

/**
 *  Synchronize the new tool data
 */
function syncMeasurementsAndToolData() {
  const measurementApi = MeasurementApi.Instance;
  measurementApi.syncMeasurementsAndToolData();
  cornerstone.getEnabledElements().forEach(enabledElement => {
    if (enabledElement.image) {
      cornerstone.updateImage(enabledElement.element);
    }
  });
}

export {
  getTotalSlice,
  convertMeasurementsToTableData,
  convertTimepointsToTableData,
  getSaveFunction,
  syncMeasurementsAndToolData,
  displaySetContainsSopInstance,
  getSliceInfo,
};
