import csTools from 'cornerstone-tools';
import {
  resetLabellingAndContextMenuAction,
  setToolContextMenuDataAction,
  setLabellingFlowDataAction,
} from './actions.js';
import updateTableWithNewMeasurementData from './updateTableWithNewMeasurementData.js';
import * as FlywheelCommon from '@flywheel/extension-flywheel-common';
import { Components as FlywheelComponents } from '@flywheel/extension-flywheel';
import { measurements } from '@ohif/core';

const triggerEvent = csTools.importInternal('util/triggerEvent');

const {
  getAllMeasurementsForImage,
  measurementToolUtils,
  commonToolUtils,
} = FlywheelCommon.Utils;
const { isOverlapWithOtherROI } = measurementToolUtils.checkMeasurementOverlap;
const { roiTools } = measurementToolUtils;
const { saveActionState } = commonToolUtils;

const {
  MeasurementHandlers,
  getImageIdForImagePath,
  getModality,
} = measurements;

const VIEWPORT_INDEX = 0;

let labellingInProgress = true;

function getOnRightClickCallback(store) {
  const setToolContextMenuData = (viewportIndex, toolContextMenuData) => {
    store.dispatch(resetLabellingAndContextMenuAction());
    store.dispatch(
      setToolContextMenuDataAction(viewportIndex, toolContextMenuData)
    );
  };

  const getOnCloseCallback = viewportIndex => {
    return function onClose() {
      const toolContextMenuData = {
        visible: false,
      };

      store.dispatch(
        setToolContextMenuDataAction(viewportIndex, toolContextMenuData)
      );
    };
  };

  return function onRightClick(event) {
    const eventData = event.detail;
    const viewportIndex = VIEWPORT_INDEX; // parseInt(eventData.element.dataset.viewportIndex, 10);

    const toolContextMenuData = {
      eventData,
      isTouchEvent: false,
      onClose: getOnCloseCallback(viewportIndex),
    };

    // setToolContextMenuData(viewportIndex, toolContextMenuData);
    setToolContextMenuData(0, toolContextMenuData);
  };
}

function getOnTouchPressCallback(store) {
  const setToolContextMenuData = (viewportIndex, toolContextMenuData) => {
    store.dispatch(resetLabellingAndContextMenuAction());
    store.dispatch(
      setToolContextMenuDataAction(viewportIndex, toolContextMenuData)
    );
  };

  const getOnCloseCallback = viewportIndex => {
    return function onClose() {
      const toolContextMenuData = {
        visible: false,
      };

      store.dispatch(
        setToolContextMenuDataAction(viewportIndex, toolContextMenuData)
      );
    };
  };

  return function onTouchPress(event) {
    const eventData = event.detail;
    const viewportIndex = parseInt(eventData.element.dataset.viewportIndex, 10);

    const toolContextMenuData = {
      eventData,
      isTouchEvent: true,
      onClose: getOnCloseCallback(viewportIndex),
    };

    setToolContextMenuData(viewportIndex, toolContextMenuData);
  };
}

function getResetLabellingAndContextMenu(store) {
  return function resetLabellingAndContextMenu() {
    store.dispatch(resetLabellingAndContextMenuAction());

    labellingInProgress = false;
  };
}

function triggerLabellingUpdateCallback(measurementData, isNewlyLabelled) {
  const state = store.getState();
  const eventName = measurementData.toolType + 'measurementpropertychangeevent';
  const element = FlywheelCommon.Utils.cornerstoneUtils.getEnabledElement(
    state.viewports.activeViewportIndex
  );
  if (!element) {
    return;
  }
  triggerEvent(element, eventName, {
    measurementData,
    eventType: isNewlyLabelled ? 'Initialize' : 'Modify',
  });
}

/**
 *
 *
 * @param {*} store
 * @returns
 */
function getToolLabellingFlowCallback(showOverlappingDialog, store) {
  const setLabellingFlowData = labellingFlowData => {
    store.dispatch(setLabellingFlowDataAction(labellingFlowData));
  };

  return function toolLabellingFlowCallback(
    measurementData,
    eventData,
    doneCallback,
    options = {}
  ) {
    let labellingDone = false;
    labellingInProgress = true;

    const updateLabelling = ({
      location,
      response,
      description,
      color,
      questionKey,
      answer,
      isSubForm,
      subFormName,
      subFormAnnotationId,
      question,
    }) => {
      // Update the measurement data with the labelling parameters
      let modified = false;
      labellingDone = true;
      let isNewlyLabelled = false;

      if (location && measurementData.location !== location) {
        isNewlyLabelled = !measurementData.location;
        measurementData.location = location;
        modified = true;
      }
      if (measurementData.description !== description) {
        measurementData.description = description || '';
        modified = true;
      }

      if (response && measurementData.response !== response) {
        measurementData.response = response;
        modified = true;
      }

      if (color && measurementData.color !== color) {
        measurementData.color = color;
        modified = true;
      }

      if (questionKey && answer) {
        measurementData.questionKey = questionKey;
        measurementData.answer = answer;
        measurementData.isSubForm = isSubForm;
        measurementData.subFormName = subFormName;
        measurementData.subFormAnnotationId = subFormAnnotationId;
        measurementData.question = question;
      }

      if (modified) {
        measurementData.dirty = true;
        triggerLabellingUpdateCallback(measurementData, isNewlyLabelled);
      }

      updateTableWithNewMeasurementData(measurementData);
    };

    const labellingDoneCallback = () => {
      labellingDone = true;
      const state = store.getState();
      const projectConfig = state.flywheel.projectConfig;
      const labels = projectConfig ? projectConfig.labels || [] : [];
      const contourUnique =
        projectConfig && projectConfig.contourUnique ? true : false;

      //Needs to handle boundary and overlapping conditions for SM
      if (getModality(state) !== 'SM') {
        const element = FlywheelCommon.Utils.cornerstoneUtils.getEnabledElement(
          state.viewports.activeViewportIndex
        );

        if (roiTools.hasOwnProperty(measurementData.toolType)) {
          const imageId = getImageIdForImagePath(measurementData.imagePath);
          let measurements = getAllMeasurementsForImage(
            measurementData.imagePath,
            state.timepointManager.measurements,
            roiTools
          );

          let boundaryMeasurements = [];
          if (labels.some(item => item.boundary && item.boundary.length)) {
            // Boundary compared measurements will filter out from the image measurements list
            boundaryMeasurements = MeasurementHandlers.handleContourBoundary(
              measurementData,
              imageId,
              labels,
              measurements,
              element
            );
            const overLappedMeasurements = isOverlapWithOtherROI(
              measurementData.toolType,
              measurementData,
              imageId,
              element
            );
            if (boundaryMeasurements && boundaryMeasurements.length) {
              const boundaryMeasures = boundaryMeasurements.filter(
                x => !x.readonly
              );
              if (!measurementData.readonly) {
                showOverlappingDialog(
                  measurementData,
                  [...boundaryMeasures, ...overLappedMeasurements],
                  boundaryMeasurements
                );
              } else if (boundaryMeasures.length) {
                showOverlappingDialog(boundaryMeasures[0], boundaryMeasures);
              }
            }
          }

          if (boundaryMeasurements.length === 0 && contourUnique) {
            const overLappedMeasurements = isOverlapWithOtherROI(
              measurementData.toolType,
              measurementData,
              imageId,
              element
            );
            const filteredMeasurements = [];
            // Check the overlapped measurements are there in the pending measurement list
            overLappedMeasurements.forEach(overlappedMeasure => {
              if (
                measurements.find(
                  measurement => measurement._id === overlappedMeasure._id
                )
              ) {
                let labelConfig = labels.find(
                  label => label.value === overlappedMeasure.location
                );
                if (labelConfig) {
                  if (
                    !labelConfig.boundary ||
                    labelConfig.boundary.length === 0
                  ) {
                    filteredMeasurements.push(overlappedMeasure);
                  } else if (
                    labelConfig.boundary.length &&
                    !labelConfig.boundary.find(
                      x => x.label === measurementData.location
                    )
                  ) {
                    filteredMeasurements.push(overlappedMeasure);
                  }
                }
              }
            });
            if (filteredMeasurements && filteredMeasurements.length) {
              const boundaryMeasures = filteredMeasurements.filter(
                x => !x.readonly
              );
              if (!measurementData.readonly) {
                showOverlappingDialog(measurementData, boundaryMeasures);
              } else if (boundaryMeasures.length) {
                showOverlappingDialog(boundaryMeasures[0], boundaryMeasures);
              }
            }
          }
        }
      }
      if (measurementData.toolType !== 'ContourRoi') {
        saveActionState(measurementData, 'measurements', 'Add');
      } else if (measurementData.ROIContourUid) {
        triggerLabellingUpdateCallback(measurementData, false);
      }
      setLabellingFlowData({ visible: false });
    };

    const labellingFlowData = {
      visible: true,
      measurementPanelData: options.measurementPanelData || false,
      eventData,
      measurementData,
      skipAddLabelButton: options.skipAddLabelButton,
      editLocation: options.editLocation,
      editDescription: options.editDescription,
      editResponse: options.editResponse,
      editDescriptionOnDialog: options.editDescriptionOnDialog,
      editLabelDescriptionOnDialog: options.editLabelDescriptionOnDialog,
      tableType: options.tableType,
      labellingDoneCallback,
      updateLabelling,
    };

    setLabellingFlowData(labellingFlowData);

    // To display labelling dialog in case labelling data is reset on the
    // final click that completes the measurement. Click event is fired if
    // mouse up happens within 200 ms after mouse down. 'toolLabellingFlowCallback'
    // is fired after a delay of 100ms once 'measurement completed' mouse down event occurs.
    // An additional delay of 100 ms here adds up to click event time range which
    // resets labelling data.
    setTimeout(() => {
      if (!labellingInProgress && !labellingDone) {
        setLabellingFlowData(labellingFlowData);
      }
    }, 100);
  };
}

export {
  getToolLabellingFlowCallback,
  getOnRightClickCallback,
  getOnTouchPressCallback,
  getResetLabellingAndContextMenu,
};
