import React from 'react';
import { ContainerTile, MeasurementTile, UserTile, HeaderTile } from './AnnotationTableComponents';

const componentTypes = {
  ContainerTile: (newProps) => <ContainerTile metadata={{}} {...newProps} />,
  HeaderTile: (newProps) => <HeaderTile metadata={{}} {...newProps} />,
  UserTile: (newProps) => <UserTile metadata={{}} {...newProps} />,
  MeasurementTile: (newProps) => <MeasurementTile metadata={{}} {...newProps} />,
}

export default componentTypes;
