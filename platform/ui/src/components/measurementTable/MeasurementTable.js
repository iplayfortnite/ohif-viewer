import './MeasurementTable.styl';
import classnames from 'classnames';

import React, { Component } from 'react';
import { withTranslation } from '../../contextProviders';
import cloneDeep from 'lodash.clonedeep';
import * as FlywheelCommon from '@flywheel/extension-flywheel-common';

import { Icon } from './../../elements/Icon';
import { MeasurementTableItem } from './MeasurementTableItem.js';
import { OverlayTrigger } from './../overlayTrigger';
import PropTypes from 'prop-types';
import { ScrollableArea } from './../../ScrollableArea/ScrollableArea.js';
import { TableList } from './../tableList';
import { Tooltip } from './../tooltip';
import { measurements } from '@ohif/core';
import store from '@ohif/viewer/src/store';

const { MeasurementHandlers, getImageIdForImagePath } = measurements;

const FlywheelCommonUtils = FlywheelCommon.Utils;
const {
  getAllMeasurementsForImage,
  measurementToolUtils,
  isCurrentFile,
} = FlywheelCommonUtils;
const { selectCurrentVolumetricImage, selectCurrentWebImage } = FlywheelCommon.Redux.selectors;
const { isOverlapWithOtherROI } = measurementToolUtils.checkMeasurementOverlap;
const { roiTools } = measurementToolUtils;

const ColorPicker = require('a-color-picker');
const defaultColor = 'rgba(255, 255, 0, 0.2)';

class MeasurementTable extends Component {
  static propTypes = {
    measurementCollection: PropTypes.array.isRequired,
    timepoints: PropTypes.array.isRequired,
    overallWarnings: PropTypes.object.isRequired,
    readOnly: PropTypes.bool,
    onItemClick: PropTypes.func,
    onRelabelClick: PropTypes.func,
    onDeleteClick: PropTypes.func,
    onVisibleClick: PropTypes.func,
    onChangeColor: PropTypes.func,
    onEditDescriptionClick: PropTypes.func,
    onClickUndo: PropTypes.func,
    onClickRedo: PropTypes.func,
    selectedMeasurementNumber: PropTypes.number,
    t: PropTypes.func,
    saveFunction: PropTypes.func,
    onSaveComplete: PropTypes.func,
    appConfig: PropTypes.object,
    measurements: PropTypes.object,
    dispatchUpdateRoiColor: PropTypes.func,
    dispatchUpdatePaletteColors: PropTypes.func,
    palette: PropTypes.array.isRequired,
    paletteIndex: PropTypes.number.isRequired,
    sliceInfo: PropTypes.shape({
      imageId: PropTypes.string,
      sliceNumber: PropTypes.number,
    }),
  };

  static defaultProps = {
    overallWarnings: {
      warningList: [],
    },
    readOnly: false,
  };

  state = {
    selectedKey: null,
    selectedColor: defaultColor,
    showColorPicker: false,
    colorPickCoordinates: { left: 0, top: 0 },
    measurementData: null,
    selectedSession: {},
  };

  componentDidMount() {
    onclick = () => {
      if (this.state.showColorPicker) {
        this.closeColorPicker();
      }
    };
  }

  render() {
    const { overallWarnings, saveFunction, t, appConfig = {} } = this.props;
    const { enableMeasurementSaveBtn = true } = appConfig;
    const hasOverallWarnings = overallWarnings.warningList.length > 0;

    return (
      <div className="measurementTable">
        <div className="measurementTableHeader">
          {hasOverallWarnings && (
            <OverlayTrigger
              key={'overwall-warning'}
              placement="left"
              overlay={
                <Tooltip
                  placement="left"
                  className="in tooltip-warning"
                  id="tooltip-left"
                  style={{}}
                >
                  <div className="warningTitle">
                    {t('Criteria nonconformities')}
                  </div>
                  <div className="warningContent">
                    {this.getWarningContent()}
                  </div>
                </Tooltip>
              }
            >
              <span className="warning-status">
                <span className="warning-border">
                  <Icon name="exclamation-triangle" />
                </span>
              </span>
            </OverlayTrigger>
          )}
        </div>
        <ScrollableArea>
          <div>{this.getMeasurementsGroups()}</div>
        </ScrollableArea>
        <div className="measurementTableFooter">
          {saveFunction && enableMeasurementSaveBtn && (
            <button
              onClick={this.saveFunction}
              className="saveBtn"
              data-cy="save-measurements-btn"
            >
              <Icon name="save" width="14px" height="14px" />
              Save measurements
            </button>
          )}
        </div>
        {this.state.showColorPicker && this.getColorSelectorDialog()}
      </div>
    );
  }

  getMeasurementData(measurements, toolType, measurementId) {
    measurements = measurements[toolType];
    const measurement = measurements.find(
      measure => measure._id === measurementId
    );
    return measurement;
  }

  saveFunction = async event => {
    const { saveFunction, onSaveComplete } = this.props;
    if (saveFunction) {
      try {
        const result = await saveFunction();
        if (onSaveComplete) {
          onSaveComplete({
            title: 'STOW SR',
            message: result.message,
            type: 'success',
          });
        }
      } catch (error) {
        if (onSaveComplete) {
          onSaveComplete({
            title: 'STOW SR',
            message: error.message,
            type: 'error',
          });
        }
      }
    }
  };

  getMeasurementsGroups = () => {
    return this.props.measurementCollection.map((measureGroup, index) => {
      return (
        <TableList
          key={index}
          customHeader={this.getCustomHeader(measureGroup)}
        >
          {this.getMeasurements(measureGroup)}
        </TableList>
      );
    });
  };

  setSubjectList(
    subjectDetails,
    sessionDetails,
    subjectList,
    item,
    subjectId,
    subjectLabel,
    sessionId,
    sessionLabel,
    sliceNumber
  ) {
    if (subjectList[subjectId]) {
      let subject_ = subjectList[subjectId];
      let session_ = subject_.session;
      if (sliceNumber) {
        item.sliceNumber = sliceNumber;
      }
      session_.measurements.push(cloneDeep(item));
      subject_.session = cloneDeep(session_);
      subjectList[subjectId] = cloneDeep(subject_);
    } else {
      let subjectObj = cloneDeep(subjectDetails);
      subjectObj.label = subjectLabel;
      subjectObj.subjectId = subjectId;
      let sessionObj = cloneDeep(sessionDetails);
      sessionObj.label = sessionLabel;
      sessionObj.sessionId = sessionId;
      if (sliceNumber) {
        item.sliceNumber = sliceNumber;
      }
      sessionObj.measurements.push(cloneDeep(item));
      subjectObj.session = cloneDeep(sessionObj);
      subjectList[subjectId] = cloneDeep(subjectObj);
    }
    return subjectList;
  }

  getMeasurementList(measureGroup) {
    let subjectList = {};
    let state = store.getState();
    const isFile = isCurrentFile(state);
    const projectConfig = state.flywheel.projectConfig;
    let measurements = state.timepointManager.measurements;
    let subjectDetails = {
      subjectId: '',
      label: '',
      session: {},
    };
    let sessionDetails = {
      sessionId: '',
      label: '',
      measurements: [],
    };

    if (FlywheelCommonUtils.isCurrentWebImage()) {
      const allSessions = state.flywheel.allSessions;
      let sessionId = selectCurrentWebImage(state)?.parents.session;
      measureGroup.measurements.forEach(item => {
        Object.keys(measurements).forEach(measurementKey => {
          let m = measurements[measurementKey];
          m = m.find(x => x._id === item.measurementId);
          let sliceNumber = null;
          if (!isFile && projectConfig?.bSlices && m) {
            sliceNumber = m.sliceNumber;
          }
          if (m) {
            subjectList = this.setSubjectList(
              subjectDetails,
              sessionDetails,
              subjectList,
              item,
              allSessions[sessionId].parents.subject,
              allSessions[sessionId].subject.label,
              sessionId,
              allSessions[sessionId].label,
              sliceNumber
            );
          }
        });
      });
      return subjectList;
    } else {
      let associations = state.flywheel.associations;
      let sessions = state.flywheel.sessions;
      measureGroup.measurements.forEach(item => {
        Object.keys(measurements).forEach(measurementKey => {
          let m = measurements[measurementKey];
          m = m.find(x => x._id === item.measurementId);
          let sliceNumber = null;
          if (!isFile && projectConfig?.bSlices && m) {
            sliceNumber = m.sliceNumber;
          }
          if (m) {
            const studyInstanceUid = m.StudyInstanceUID || m.studyInstanceUid;
            if (associations[studyInstanceUid]) {
              let associationDetails = associations[studyInstanceUid];
              let session = sessions.find(
                x => x._id === associationDetails.session_id
              );
              if (session) {
                let subject = session.subject;
                subjectList = this.setSubjectList(
                  subjectDetails,
                  sessionDetails,
                  subjectList,
                  item,
                  subject._id,
                  subject.label,
                  session._id,
                  session.label,
                  sliceNumber
                );
              }
            }
          }
        });
      });
      return subjectList;
    }
  }

  isOpened(key) {
    if (!this.state.selectedSession[key]) {
      return true;
    }
    return false;
  }

  getMeasurements = measureGroup => {
    let state = store.getState();
    const isFile = isCurrentFile(state);
    const currentVolumetricImage = selectCurrentVolumetricImage(state);
    if (currentVolumetricImage) {
      return this.getMeasurementsData(measureGroup);
    }
    let measurementList = this.getMeasurementList(measureGroup);
    const { measurements, sliceInfo } = this.props;
    const projectConfig = state.flywheel.projectConfig;
    return Object.keys(measurementList).map((subjectId, subjectIndex) => {
      let subject = measurementList[subjectId];
      let session = subject.session;
      let isSubjectOpen = this.isOpened(subjectId);
      let isSessionOpen = this.isOpened(session.sessionId);
      let sessionMeasurements = session.measurements;
      if (!isFile && projectConfig?.bSlices) {
        sessionMeasurements = session.measurements.filter(
          item => item.sliceNumber === sliceInfo?.sliceNumber
        );
      }
      return (
        <div key={subjectIndex} className="mt-4">
          <div
            className="measurement-title"
            onClick={() => {
              this.setState({
                selectedSession: {
                  ...this.state.selectedSession,
                  [subjectId]: !this.state.selectedSession[subjectId],
                },
              });
            }}
          >
            {subject.label}
          </div>
          {isSubjectOpen && (
            <div className="measurement-view">
              <div
                className="measurement-title"
                onClick={() => {
                  this.setState({
                    selectedSession: {
                      ...this.state.selectedSession,
                      [session.sessionId]: !this.state.selectedSession[
                        session.sessionId
                      ],
                    },
                  });
                }}
              >
                {session.label}
              </div>
              {isSessionOpen && (
                <div>
                  {sessionMeasurements &&
                    sessionMeasurements.map((measurement, index) => {
                      const key = measurement.measurementNumber;
                      const itemIndex = measurement.itemNumber || index + 1;
                      let itemClass =
                        this.state.selectedKey === key && !this.props.readOnly
                          ? 'selected'
                          : '';
                      if (this.isOverLappedMeasurement(measurement)) {
                        itemClass += ' contourUniqueBorderColor';
                      }

                      const measurementForColor = measurements[
                        measurement.toolType
                      ].find(
                        m =>
                          m.measurementNumber === measurement.measurementNumber
                      );
                      const isMeasurementReadOnly =
                        measurementForColor.readonly || false;

                      const selectedColor =
                        !this.props.readOnly && measurementForColor?.color
                          ? measurementForColor?.color
                          : defaultColor;

                      return (
                        <MeasurementTableItem
                          key={index}
                          itemIndex={itemIndex}
                          itemClass={itemClass}
                          measurementData={measurement}
                          onItemClick={this.onItemClick}
                          onRelabel={this.props.onRelabelClick}
                          onDelete={this.props.onDeleteClick}
                          onVisible={this.props.onVisibleClick}
                          onEditDescription={this.props.onEditDescriptionClick}
                          onClickColorPicker={this.onClickColorPicker}
                          selectedColor={selectedColor}
                          closeColorPicker={this.closeColorPicker}
                          readonly={isMeasurementReadOnly}
                        />
                      );
                    })}
                </div>
              )}
            </div>
          )}
        </div>
      );
    });
  };

  getMeasurementsData = measureGroup => {
    const selectedKey = this.props.selectedMeasurementNumber
      ? this.props.selectedMeasurementNumber
      : this.state.selectedKey;
    const { measurements } = this.props;

    return measureGroup.measurements.map((measurement, index) => {
      const key = measurement.measurementNumber;
      const itemIndex = measurement.itemNumber || index + 1;
      let itemClass =
        selectedKey === key && !this.props.readOnly ? 'selected' : '';
      if (this.isOverLappedMeasurement(measurement)) {
        itemClass += ' contourUniqueBorderColor';
      }
      const measurementForColor = measurements[measurement.toolType].find(
        m => m.measurementNumber === measurement.measurementNumber
      );

      const isMeasurementReadOnly = measurementForColor.readonly || false;

      const selectedColor =
        !this.props.readOnly && measurementForColor?.color
          ? measurementForColor?.color
          : defaultColor;

      return (
        <MeasurementTableItem
          key={key}
          itemIndex={itemIndex}
          itemClass={itemClass}
          measurementData={measurement}
          onItemClick={this.onItemClick}
          onRelabel={this.props.onRelabelClick}
          onDelete={this.props.onDeleteClick}
          onVisible={this.props.onVisibleClick}
          onChangeColor={this.props.onChangeColor}
          onEditDescription={this.props.onEditDescriptionClick}
          onClickColorPicker={this.onClickColorPicker}
          selectedColor={selectedColor}
          closeColorPicker={this.closeColorPicker}
          readonly={isMeasurementReadOnly}
        />
      );
    });
  };

  isOverLappedMeasurement(measurement) {
    const state = store.getState();

    const projectConfig = state.flywheel.projectConfig;
    const contourUnique =
      projectConfig && projectConfig.contourUnique ? true : false;
    const labels = projectConfig ? projectConfig.labels || [] : [];
    const isBoundaryCheck = labels.some(
      item => item.boundary && item.boundary.length
    );

    if (
      !roiTools.hasOwnProperty(measurement.toolType) ||
      (!isBoundaryCheck && !contourUnique) ||
      !state.contourProperties.highlightOverlapped
    ) {
      return false;
    }

    const measurementData = this.getMeasurementData(
      state.timepointManager.measurements,
      measurement.toolType,
      measurement.measurementId
    );
    const imageId = getImageIdForImagePath(measurementData.imagePath);
    let measurements = getAllMeasurementsForImage(
      measurementData.imagePath,
      state.timepointManager.measurements,
      roiTools
    );

    const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
      state.viewports.activeViewportIndex
    );

    let boundaryMeasurements = [];
    if (isBoundaryCheck) {
      boundaryMeasurements = MeasurementHandlers.handleContourBoundary(
        measurementData,
        imageId,
        labels,
        measurements,
        element
      );
      if (boundaryMeasurements && boundaryMeasurements.length) {
        return true;
      }
    }
    if (boundaryMeasurements.length === 0 && contourUnique) {
      const overLappedMeasurements = isOverlapWithOtherROI(
        measurementData.toolType,
        measurementData,
        imageId,
        element
      );
      const filteredMeasurements = [];
      // Check the overlapped measurements are there in the pending measurement list
      overLappedMeasurements.forEach(overlappedMeasure => {
        if (
          measurements.find(
            measurement => measurement._id === overlappedMeasure._id
          )
        ) {
          let labelConfig = labels.find(
            label => label.value === overlappedMeasure.location
          );
          if (labelConfig) {
            if (!labelConfig.boundary || labelConfig.boundary.length === 0) {
              filteredMeasurements.push(overlappedMeasure);
            } else if (
              labelConfig.boundary.length &&
              !labelConfig.boundary.find(
                x => x.label === measurementData.location
              )
            ) {
              filteredMeasurements.push(overlappedMeasure);
            }
          }
        }
      });
      if (filteredMeasurements && filteredMeasurements.length) {
        return true;
      }
    }

    return false;
  }

  onItemClick = (event, measurementData) => {
    this.closeColorPicker();
    if (this.props.readOnly) return;

    const { measurements } = this.props;

    const measurement = measurements[measurementData.toolType].find(
      m => m.measurementNumber === measurementData.measurementNumber
    );

    this.setState({
      selectedKey: measurementData.measurementNumber,
      selectedColor: measurement.color || defaultColor,
      measurementData,
    });

    if (this.props.onItemClick) {
      this.props.onItemClick(event, measurementData);
    }
  };

  getCustomHeader = measureGroup => {
    const state = store.getState();
    const projectConfig = state.flywheel.projectConfig;
    let sessionMeasurements = [];
    const measurementList = this.getMeasurementList(measureGroup);
    Object.keys(measurementList).map(subjectId => {
      let subject = measurementList[subjectId];
      let session = subject.session;
      session.measurements.forEach(measurement => {
        sessionMeasurements.push(measurement);
      });
    });
    let numberOfMeasurements = measureGroup.measurements.length;
    if (!isCurrentFile(state) && projectConfig?.bSlices) {
      const { sliceInfo } = this.props;
      sessionMeasurements = sessionMeasurements.filter(
        item => item.sliceNumber === sliceInfo?.sliceNumber
      );
      numberOfMeasurements =
        sessionMeasurements.length + `/` + measureGroup.measurements.length;
    }
    return (
      <React.Fragment>
        <div className="tableListHeaderTitle">
          {this.props.t(measureGroup.groupName)}
        </div>
        {measureGroup.maxMeasurements && (
          <div className="maxMeasurements">
            {this.props.t('MAX')} {measureGroup.maxMeasurements}
          </div>
        )}
        <div className="numberOfItems">{numberOfMeasurements}</div>
        <Icon
          name={'undo'}
          onClick={this.props.onClickUndo}
          className={classnames('caret', 'material-icons', 'undo')}
          width="14px"
          height="14px"
        />
        <Icon
          name={'redo'}
          onClick={this.props.onClickRedo}
          className={classnames('caret', 'material-icons', 'redo')}
          width="14px"
          height="14px"
        />
      </React.Fragment>
    );
  };

  getTimepointsHeader = () => {
    const { timepoints, t } = this.props;

    return timepoints.map((timepoint, index) => {
      return (
        <div key={index} className="measurementTableHeaderItem">
          <div className="timepointLabel">{t(timepoint.key)}</div>
          <div className="timepointDate">{timepoint.date}</div>
        </div>
      );
    });
  };

  getWarningContent = () => {
    const { warningList = '' } = this.props.overallWarnings;

    if (Array.isArray(warningList)) {
      const listedWarnings = warningList.map((warn, index) => {
        return <li key={index}>{warn}</li>;
      });

      return <ol>{listedWarnings}</ol>;
    } else {
      return <React.Fragment>{warningList}</React.Fragment>;
    }
  };

  // To Show the color selector dialog
  onClickColorPicker = (event, measurementData) => {
    const screenHeight = window.innerHeight;

    // Align dialog 20px above or below from clicked point to not overlap the picker dialog over the palette and other buttons.
    const offset = 20;

    const pickerSize = {
      height: 297, // Color selector dialog height
      width: 232, // Color selector dialog width
    };

    const colorPickCoordinates = {
      left: event.clientX - pickerSize.width / 2,
      top:
        screenHeight / 2 > event.clientY
          ? event.clientY + offset
          : event.clientY - offset - pickerSize.height,
    };

    this.setState({ showColorPicker: false }, () => {
      this.setState(
        {
          showColorPicker: true,
          colorPickCoordinates,
          measurementData,
        },
        () => this.onColorChangeHandler()
      );
    });
  };

  closeColorPicker = () => {
    const { measurements } = this.props;
    this.props.dispatchUpdatePaletteColors(measurements);
    this.setState({ showColorPicker: false });
  };

  onColorChangeHandler = () => {
    ColorPicker.from('.picker').on('change', picker => {
      const { palette } = this.props;
      const selectedColor = `rgba(${picker.rgba.join(',')})`;
      this.setState(
        {
          selectedColor,
        },
        () => {
          this.props.onChangeColor(selectedColor, this.state.measurementData);
          this.props.dispatchUpdateRoiColor({
            current: selectedColor,
            next: palette[this.props.paletteIndex],
          });
        }
      );
    });
  };

  getColorSelectorDialog = () => {
    return (
      <div
        className="color-picker"
        style={{
          left: this.state.colorPickCoordinates.left,
          top: this.state.colorPickCoordinates.top,
        }}
        onClick={e => e.stopPropagation()}
      >
        <div
          className="picker"
          acp-color={this.state.selectedColor}
          acp-palette={this.props.palette.join('|')}
          acp-show-rgb="no"
          acp-show-hsl="no"
          acp-show-hex="no"
          acp-show-alpha="yes"
          acp-palette-editable="no"
          acp-use-alpha-in-palette="no"
        ></div>
      </div>
    );
  };
}

const connectedComponent = withTranslation(['MeasurementTable', 'Common'])(
  MeasurementTable
);
export { connectedComponent as MeasurementTable };
export default connectedComponent;
