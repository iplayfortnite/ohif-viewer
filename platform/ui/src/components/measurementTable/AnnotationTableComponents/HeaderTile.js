import React, { memo, useCallback, useState } from 'react';

import { createChildComponents, preventPropagation } from '../Utils';
import { ToolbarButton } from '../../../viewer/ToolbarButton';

import './CommonStyle.styl';


const ContainerTile = (props) => {
  const [hide, setHide] = useState(false);
  const { parentProps, metadata } = props;
  const { label, children } = metadata;

  const onClickItem = useCallback((e) => {
    preventPropagation(e);
    setHide(!hide);
  }, [hide]);

  const childComponents = createChildComponents(children, { ...props });
  return (<div className='display-flex-column enable-cursor-pointer'>
    <div className='collapsible-tab margin-bottom-5' onClick={onClickItem}>
      <span className='display-flex padding-left-5'>
        <ToolbarButton
          {...parentProps}
          label=''
          className='material-icons'
          icon={hide ? 'arrow_drop_up' : 'arrow_drop_down'} />
      </span>
      <span>{label}</span>
      <span />
    </div>
    <div> {!hide && childComponents}</div>
  </div>);
}

export default memo(ContainerTile);
