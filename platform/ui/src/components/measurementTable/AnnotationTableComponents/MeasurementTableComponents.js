import React, { memo } from 'react';

import DetailsPane from './DetailsPane/DetailsPane';
import { ToolbarButton } from '../../../viewer/ToolbarButton';
import store from '@ohif/viewer/src/store';

import './CommonStyle.styl';
import { getActionButton, preventPropagation } from '../Utils';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
const { RightHandPanelModes } = FlywheelCommonRedux.constants;
import { getAvatarStyle } from '@ohif/ui/src/components/measurementTable/Utils.js';

const SingleLabelHeaderComponent = (props) => {
  const {
    label, dim, measurementColor, showEditIcon, onEditLabelDescriptionClick,
    onClickColorPicker, parentProps, component, onEditSubFormClick, measurement
  } = props;

  const color = measurementColor.replace('0.2', '1');
  const state = store.getState();
  const studyForm = state.flywheel.projectConfig?.studyForm?.components;
  const studyFormWorkflow = state.flywheel.projectConfig?.studyFormWorkflow;
  let showSubForm = false;
  if (studyForm?.length && (studyFormWorkflow === 'ROI' || studyFormWorkflow === 'Mixed')) {
    const answer = typeof measurement.answer === 'object' ? measurement.answer.value : measurement.answer;
    const questionKey = measurement.questionKey;
    const data = studyForm.find(item => item.key === questionKey);
    const dataValue = data && data.values?.find(value => value.value === answer);
    if (dataValue?.subForm) {
      showSubForm = true;
    }
  }
  const labelWidth = showEditIcon ? '60px' : '';
  const singleAvatar = state.viewerAvatar.singleAvatar;
  const isDisabled = state.multipleReaderTask.TaskIds.length > 1 && !singleAvatar;

  return (<div className='display-flex width-100 margin-right-5'>
    <div
      onClick={onClickColorPicker}
      className='color-picker-container margin-right-5'
      style={{ backgroundColor: color }}
    />
    {component ? component :
      <div className='display-style-grid'><div className={`text-overflow ${dim}`} style={{}} title={label}>{label}</div> </div>
    }
    {showEditIcon && <div className='margin-left-5 flex-align-center' style={{ opacity: isDisabled ? .5 : 1 }}
      onClick={isDisabled ? null : onEditLabelDescriptionClick}>
      <ToolbarButton
        {...parentProps}
        className={'material-icons icon-size icon-color'}
        icon={'create'}
        label=''
      />
    </div>
    }
    {showEditIcon && showSubForm && <div className='margin-left-5 flex-align-center'
      onClick={onEditSubFormClick}>
      <ToolbarButton
        {...parentProps}
        className={'material-icons icon-size icon-color'}
        icon={'list_alt'}
        label=''
      />
    </div>
    }
  </div>);
}


const SingleLabelIconsComponent = (props) => {
  const { dim, measurement, toolObj, orientation, toolIconComponent,
    visibleButton, deleteButton, showEditIcon, hideOrientationAndSlice } = props;

  return (<div className='flex-align-center-end'>
    {!hideOrientationAndSlice && <div className={`margin-right-5 orientation-slice-color nowrap ${dim}`}>S: {measurement.sliceNumber}
      <span className='secondary-label-color'>/{measurement.totalSlice}</span></div>}
    {!hideOrientationAndSlice && <div className={`margin-right-5 orientation-slice-color ${dim}`}>O:{orientation}</div>}
    {toolIconComponent && <div className='margin-right-5 icon-container' title={toolObj.label}>{toolIconComponent}</div>}
    <div className='margin-right-5 icon-color' onClick={(e) => preventPropagation(e)}>{visibleButton}</div>
    <div className='icon-color'>{deleteButton}</div>
  </div>);
}

const isPanelExpanded = () => {
  const state = store.getState();
  const panelMode = state.rightHandPanel.panelMode;
  return panelMode === RightHandPanelModes.EXPAND;
}

const GroupedLabelHeaderComponent = (props) => {
  const { dim, showEditIcon, measurementColor, measurement,
    onEditLabelDescriptionClick, parentProps, subComponent,
    onEditSubFormClick, sliceInfo, avatarClick, onAvatarMouseEnter,
    onAvatarMouseLeave, measurements
  } = props;
  const color = measurementColor.replace('0.2', '1');
  const { description, measurementNumber } = measurement;
  const measurementTitle = description ? description : `# ${measurementNumber}`;
  const state = store.getState();
  const studyForm = state.flywheel.projectConfig?.studyForm?.components;
  const studyFormWorkflow = state.flywheel.projectConfig?.studyFormWorkflow;
  let showSubForm = false;
  if (studyForm?.length && (studyFormWorkflow === 'ROI' || studyFormWorkflow === 'Mixed')) {
    const answer = typeof measurement.answer === 'object' ? measurement.answer.value : measurement.answer;
    const questionKey = measurement.questionKey;
    const data = studyForm.find(item => item.key === questionKey);
    const dataValue = data && data.values?.find(value => value.value === answer);
    if (dataValue?.subForm) {
      showSubForm = true;
    }
  }
  const labelWidth = showEditIcon ? '60px' : '';
  const singleAvatar = state.viewerAvatar.singleAvatar;
  const isDisabled = state.multipleReaderTask.TaskIds.length > 1 && !singleAvatar;
  const taskIds = state.multipleReaderTask?.TaskIds;
  const viewerAvatar = state.viewerAvatar.viewerAvatar;
  const marginLeft = isPanelExpanded() ? 'margin-left-4' : ""

  return (
    <div className='flex-align-center width-100'>
      <span className=' margin-left-5' style={{ color: color, stroke: color }}>
        <ToolbarButton
          {...parentProps}
          className='material-icons subdirectory-icon'
          icon='subdirectory_arrow_right'
          label=''
        />
      </span>
      <div className="display-style-grid pos-relative width-100" title={measurementTitle}>
        {subComponent && <span className='pos-absolute'>{subComponent}</span>}
        <span className={`${dim} measurement-number pos-relative pointer-events-none ${marginLeft} `} title={measurementTitle}>{measurementTitle}</span>
      </div>
      {showEditIcon && <div className='margin-left-5 flex-align-center' style={{ opacity: isDisabled ? .5 : 1 }}
        onClick={isDisabled ? null : onEditLabelDescriptionClick}>
        <ToolbarButton
          {...parentProps}
          className={'material-icons icon-size icon-color'}
          icon={'create'}
          label=''
        />
      </div>
      }
      {showEditIcon && showSubForm && <div className='margin-left-5 flex-align-center'
      onClick={onEditSubFormClick}>
      <ToolbarButton
        {...parentProps}
        className={'material-icons icon-size icon-color'}
        icon={'list_alt'}
        label=''
      />
      </div>
      }
      {(() => {
        if (taskIds.length > 1) {
          const allUsers = state.flywheel.allUsers;
          const users = [];
          state.multipleReaderTask?.multipleTaskResponse?.forEach(task => {
            let userData = { ...allUsers[task.assignee] };
            userData.task_id = task._id;
            users.push(userData);
          });
          let dispatchActions = null;
          let userAvatarClickedDetails = [];
          const slice = sliceInfo?.sliceNumber;
          const userId = measurement?.flywheelOrigin?.id;

          if (userId) {
            return (
              <div>
                {
                  users?.map(user => {
                    const { avatarStyle, initials, avatar } = getAvatarStyle(user);
                    avatarStyle.color = 'black';
                    avatarStyle.borderWidth = '1px';
                    avatarStyle.borderStyle = 'solid';

                    if (!userAvatarClickedDetails.find(task => task[initials + '-' + user.task_id] === true || task[initials + '-' + user.task_id] === false)) {
                      userAvatarClickedDetails.push({
                        [initials + '-' + user.task_id]: false,
                        initials: initials,
                        taskId: user.task_id,
                        slice: slice,
                        visible: false,
                      });
                    }

                    if (user._id === userId) {
                      if (viewerAvatar.length) {
                        viewerAvatar.forEach(avatar => {
                          const data = avatar.initials + '-' + avatar.taskId;
                          const avatarElements = document.getElementsByClassName(data).length;
                          if (!avatar[data] && avatarElements > 0) {
                            for (let n = 0; n < avatarElements; n++) {
                              document.getElementsByClassName(data).item(n).style.opacity = 0.3;
                            }
                          } else if (avatarElements > 0 && avatar[data]) {
                            for (let n = 0; n < avatarElements; n++) {
                              document.getElementsByClassName(data).item(n).style.opacity = 1;
                            }
                          }
                        });
                      }

                      return (
                        <div className='avatar-wrapper'>
                          <div className={`avatar viewerFormAvatar ${initials + '-' + user.task_id}`} style={avatarStyle}
                            onClick={(e) => avatarClick(userAvatarClickedDetails, slice, initials, user.task_id, e, dispatchActions, measurements)}
                            onMouseEnter={(e) => onAvatarMouseEnter(e, dispatchActions, measurements, user, measurement.questionKey, user.task_id,
                              slice,
                              initials)}
                            onMouseLeave={(e) => onAvatarMouseLeave(e, dispatchActions, measurements, user.task_id,
                              slice, initials)}
                          >
                            {avatar ? '' : initials}
                          </div>
                        </div>
                      );
                    }
                  })
                }
              </div>
            );
          }
        }
      })()}
    </div>
  );
}

const GroupedLabelIconsComponent = (props) => {
  const { dim, measurement, toolObj, orientation, toolIconComponent,
    visibleButton, deleteButton, showEditIcon, hideOrientationAndSlice } = props;

  return (<div className='flex-align-center-end'>
    {!hideOrientationAndSlice && <div className={`margin-right-5 orientation-slice-color nowrap ${dim}`}>S: {measurement.sliceNumber}
      <span className='secondary-label-color'>/{measurement.totalSlice}</span></div>}
    {!hideOrientationAndSlice && <div className={`margin-right-5 orientation-slice-color ${dim}`}>O:{orientation}</div>}
    {toolIconComponent && <div className='margin-right-5 icon-container icon-color' title={toolObj.label}>{toolIconComponent}</div>}
    <div className='margin-right-5 icon-color'>{visibleButton}</div>
    <div className='mr-m4 icon-color'>{deleteButton}</div>
  </div>);
}


const SingleMeasurementBoxComponent = (props) => {
  const { dim, measurement, label, toolObj, orientation, toolIconComponent,
    visibleButtonObj, deleteButtonObj, onMouseEnter, onMouseLeave, onClickColorPicker,
    onEditLabelDescriptionClick, onItemClick, detailsPaneObj, isOverlapped, showEditIcon,
    parentProps, measurementColor, rootClass, component, onEditSubFormClick, hideOrientationAndSlice
  } = props;
  const color = measurementColor.replace('0.2', '1');
  const visibleButton = getActionButton(
    visibleButtonObj.isVisible ? 'eye' : 'eye-closed',
    visibleButtonObj.onClick,
    !visibleButtonObj.isVisible,
    visibleButtonObj.isVisible ? 'Visible' : 'Not Visible',
  );

  const state = store.getState();
  const singleAvatar = state.viewerAvatar.singleAvatar;
  const isDisabled = state.multipleReaderTask.TaskIds.length > 1 && !singleAvatar;
  const deleteButton = getActionButton('trash', isDisabled ? null : deleteButtonObj.onClick, isDisabled);

  const { infoHeader, toolName, user, field, measureInfo, showDetailsPane } = detailsPaneObj;
  return (<div className={`measurement-tile-item padding ${rootClass}`}
    style={{ border: isOverlapped ? '1px solid red' : '' }}
    onMouseEnter={onMouseEnter}
    onMouseLeave={onMouseLeave}
    onClick={onItemClick}>
    <div className='item-content'>
      <SingleLabelHeader
        component={component}
        dim={dim}
        label={label}
        onClickColorPicker={onClickColorPicker}
        measurementColor={color}
        onEditLabelDescriptionClick={onEditLabelDescriptionClick}
        onEditSubFormClick={onEditSubFormClick}
        showEditIcon={showEditIcon}
        parentProps={parentProps}
        measurement={measurement}
      />
      <SingleLabelIcons
        dim={dim}
        showEditIcon={showEditIcon}
        measurement={measurement}
        toolObj={toolObj}
        orientation={orientation}
        toolIconComponent={toolIconComponent}
        visibleButton={visibleButton}
        deleteButton={deleteButton}
        hideOrientationAndSlice={hideOrientationAndSlice}
      />
    </div>
    {
      showDetailsPane && <DetailsPane
        infoHeader={infoHeader}
        toolName={toolName}
        user={user}
        field={field}
        measureInfo={measureInfo}
      />
    }
  </div >);
}

const GroupedMeasurementContainerComponent = (props) => {
  const {
    onMouseEnter, onMouseLeave, onGroupClick, onClickColorPicker, measurementColor, dim, groupTitle,
    measurementCount, visibleButtonObj, deleteButtonObj, subComponent, activeBorderBottom, activeBorder, component,
    rootClass
  } = props;

  const state = store.getState();
  const singleAvatar = state.viewerAvatar.singleAvatar;
  const isDisabled = state.multipleReaderTask.TaskIds.length > 1 && !singleAvatar;
  const deleteGroupButton = getActionButton('trash',
    isDisabled ? null : deleteButtonObj.onClick,
    isDisabled,
    'Delete Group Measurements'
  );

  const visibleGroupButton = getActionButton(
    !visibleButtonObj.isVisible ? 'eye' : 'eye-closed',
    visibleButtonObj.onClick,
    visibleButtonObj.isVisible,
    !visibleButtonObj.isVisible ? 'Visible' : 'Not Visible',
  );
  const color = measurementColor.replace('0.2', '1');
  return <div className={`measurement-tile-item ${activeBorder} ${rootClass}`}>
    <div className={`item-content p-8 ${activeBorderBottom}`}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      onClick={onGroupClick}
    >
      <div className='flex-align-center width-100'>
        <div
          onClick={onClickColorPicker}
          className='color-picker-container margin-right-5'
          style={{ backgroundColor: color }} />
        <div className='label-container'>
          {component ? component :
            <div className='display-style-grid'><div className={`margin-right-5 text-overflow label ${dim}`} title={groupTitle}>{groupTitle}</div> </div>
          }
          <div className='measure-count'>{measurementCount}</div>
        </div>
      </div>
      <div className='display-flex' >
        <div className='flex-justify-space-between'>
          <div className='margin-right-5 font-size-10 secondary-label-color flex-align-center nowrap'>Multiple Measurements</div>
          <div className='margin-right-5 icon-color' onClick={(e) => preventPropagation(e)}>{visibleGroupButton}</div>
          <div className='icon-color'>{deleteGroupButton}</div>
        </div>
      </div>
    </div>
    {subComponent}
  </div>;
}

const GroupedMeasurementBoxComponent = (props) => {
  const {
    onMouseEnter, onMouseLeave, isOverlapped, onItemClick, dim, measurementColor,
    measurement, onEditLabelDescriptionClick, parentProps, showEditIcon, toolObj,
    orientation, toolIconComponent, visibleButtonObj, deleteButtonObj, detailsPaneObj, rootClass,
    subComponent, onEditSubFormClick, hideOrientationAndSlice,
    sliceInfo, avatarClick, onAvatarMouseEnter, onAvatarMouseLeave, measurements
  } = props;

  const visibleButton = getActionButton(
    visibleButtonObj.isVisible ? 'eye' : 'eye-closed',
    visibleButtonObj.onClick,
    !visibleButtonObj.isVisible,
    visibleButtonObj.isVisible ? 'Visible' : 'Not Visible',
  );
  const state = store.getState();
  const singleAvatar = state.viewerAvatar.singleAvatar;
  const isDisabled = state.multipleReaderTask.TaskIds.length > 1 && !singleAvatar;
  const deleteButton = getActionButton('trash',
    isDisabled ? null : deleteButtonObj.onClick,
    isDisabled,
    'Delete'
  );

  const { infoHeader, toolName, user, field, measureInfo, showDetailsPane } = detailsPaneObj;
  return <div className={`measurement-group-tile-item ${rootClass}`}
    onMouseEnter={onMouseEnter}
    onMouseLeave={onMouseLeave}
    style={{ border: isOverlapped ? '1px solid red' : '' }}
    onClick={onItemClick}>
    <div className='flex-justify-space-between flex-align-center'>
      <GroupedLabelHeader
        dim={dim}
        measurementColor={measurementColor}
        measurement={measurement}
        onEditLabelDescriptionClick={onEditLabelDescriptionClick}
        onEditSubFormClick={onEditSubFormClick}
        parentProps={parentProps}
        showEditIcon={showEditIcon}
        subComponent={subComponent}
        sliceInfo={sliceInfo}
        avatarClick={avatarClick}
        onAvatarMouseEnter={onAvatarMouseEnter}
        onAvatarMouseLeave={onAvatarMouseLeave}
        measurements={measurements}
      />
      <GroupedLabelIcons
        dim={dim}
        measurement={measurement}
        showEditIcon={showEditIcon}
        toolObj={toolObj}
        orientation={orientation}
        toolIconComponent={toolIconComponent}
        visibleButton={visibleButton}
        deleteButton={deleteButton}
        hideOrientationAndSlice={hideOrientationAndSlice}
      />
    </div>
    {
      showDetailsPane && <DetailsPane
        infoHeader={infoHeader}
        toolName={toolName}
        user={user}
        field={field}
        measureInfo={measureInfo}
      />
    }
  </div>;
}


const NewAnnotationContainer = (props) => {
  const { onClick, disabled } = props;
  const disabledClass = disabled ? 'disabled-new-annotation-box' : '';
  return <div className={`new-annotation-box padding-left-9 ${disabledClass}`}>
    <div >
      <ToolbarButton
        onClick={onClick}
        t={() => { }}
        className={'material-icons icon-size add-annotation '}
        icon={'addbox'}
        label=''
      /></div>
    <div className='add-annotation-label margin-left-15'>New Annotation</div></div>;
}

const SingleLabelHeader = memo(SingleLabelHeaderComponent);
const SingleLabelIcons = memo(SingleLabelIconsComponent);
const GroupedLabelHeader = memo(GroupedLabelHeaderComponent);
const GroupedLabelIcons = memo(GroupedLabelIconsComponent);
const SingleMeasurementBox = memo(SingleMeasurementBoxComponent);
const GroupedMeasurementBox = memo(GroupedMeasurementBoxComponent);
const GroupedMeasurementContainer = memo(GroupedMeasurementContainerComponent);
const NewAnnotationBox = memo(NewAnnotationContainer);

export const MeasurementTableComponents = {
  SingleLabelHeader,
  SingleLabelIcons,
  GroupedLabelHeader,
  GroupedLabelIcons,
  SingleMeasurementBox,
  GroupedMeasurementBox,
  GroupedMeasurementContainer,
  NewAnnotationBox,
};
