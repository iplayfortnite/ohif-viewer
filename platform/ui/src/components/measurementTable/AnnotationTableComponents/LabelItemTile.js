import React, { memo, useState } from 'react';
import { measurements } from '@ohif/core'
import { getItemVisibilityStatus, getMeasurementColor, getToolDetailsFromToolbarDefinition, getToolIcon } from './Utils';
import { getTextInfo, preventPropagation } from '../Utils';
import isOverLappedMeasurement from './isOverLappedMeasurement';

import { MeasurementTableComponents } from './MeasurementTableComponents';

import './CommonStyle.styl';
const { GroupedMeasurementBox } = MeasurementTableComponents;


const LabelItemTile = (props) => {
  const { parentProps, measurement, updateState, componentStatus } = props;
  const { questionKeysByToolTypes, onMouseLeave, tableType } = parentProps;
  const measureInfo = getTextInfo(measurement);
  const [hover, setHover] = useState(false);
  const modality = measurements.getModality(store.getState());

  const { toolObj, tool } = getToolDetailsFromToolbarDefinition(measurement);
  const toolIconComponent = getToolIcon(measurement, parentProps);

  const isVisible = getItemVisibilityStatus(measurement);
  const visibleButtonObj = {
    onClick: (e) => {
      preventPropagation(e);
      parentProps.onVisibleClick(e, measurement);
    },
    isVisible: isVisible,
  }
  const deleteButtonObj = {
    onClick: (e) => {
      preventPropagation(e);
      if (measurement.readonly) {
        return;
      }
      parentProps.onDeleteClick(e, measurement);
      onMouseLeave(e);
    }
  };

  const onEditLabelDescriptionClick = (e) => {
    preventPropagation(e);
    if (measurement.readonly) {
      return;
    }
    parentProps.onEditLabelDescriptionClick(e, measurement, tableType);
  }

  const onEditSubFormClick = (e) => {
    preventPropagation(e);
    if (measurement.readonly) {
      return;
    }
    parentProps.onEditSubFormClick(measurement);
  }

  const onItemClick = (e) => {
    preventPropagation(e);
    updateState(measurement.measurementId);
    parentProps.onItemClick(e, measurement);
  }

  const username = measurement && measurement.user ? measurement.user.email : '';
  const measurementColor = getMeasurementColor(measurement, parentProps.measurements).replace('0.2', '1');
  const isOverlapped = isOverLappedMeasurement(measurement);
  const dim = parentProps.isMouseEntered && !hover ? 'dim' : '';
  const { location, toolType } = measurement;
  let questionKey = questionKeysByToolTypes?.[location]?.[toolType] ? questionKeysByToolTypes[location][toolType].join().replaceAll(',', `, `) : '';
  if (measurement?.isSubForm) {
    questionKey = measurement?.questionKey;
  }
  return (<GroupedMeasurementBox
    onMouseEnter={(e) => {
      setHover(true);
      parentProps?.setActiveHoveredMeasurements([measurement]);
    }}
    onMouseLeave={(e) => {
      setHover(false);
      parentProps?.setActiveHoveredMeasurements([]);
    }}
    isOverlapped={isOverlapped}
    onItemClick={onItemClick}
    onClick={onItemClick}
    dim={dim}
    measurementColor={measurementColor}
    measurement={measurement}
    onEditLabelDescriptionClick={onEditLabelDescriptionClick}
    onEditSubFormClick={onEditSubFormClick}
    parentProps={parentProps}
    isVisibleEditIcon={parentProps.isMouseEntered && hover}
    toolObj={toolObj}
    orientation={parentProps?.getImageOrientation(measurement)}
    toolIconComponent={toolIconComponent}
    visibleButtonObj={visibleButtonObj}
    deleteButtonObj={deleteButtonObj}
    showEditIcon={hover}
    hideOrientationAndSlice={modality === 'SM'}
    detailsPaneObj={{
      infoHeader: tool.label,
      toolName: toolObj.label,
      user: username,
      field: questionKey,
      measureInfo: measureInfo,
      showDetailsPane: componentStatus[measurement.measurementId]
    }}
  />);
}

export default memo(LabelItemTile);
