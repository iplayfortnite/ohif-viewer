import React, { memo, useCallback, useState } from 'react';
import { isEmpty } from 'lodash';

import {
  createChildComponents,
  getAvatarStyle,
  preventPropagation,
} from '../Utils';
import { ToolbarButton } from '../../../viewer/ToolbarButton';
import { getItemVisibilityStatus } from './Utils';
import { Icon } from '../../..';
import OHIF from '@ohif/core';

import './CommonStyle.styl';

const measurementApi = OHIF.measurements.MeasurementApi;
const UserTile = props => {
  const { parentProps, metadata } = props;
  const { updateState, componentState } = parentProps;
  const { label, children, data } = metadata;

  const newState = componentState[metadata.metadata.key]
    ? componentState[metadata.metadata.key]
    : {};
  const [hide, setHide] = useState(false);
  const [componentStatus, setComponentStatus] = useState(newState);

  const measurement = data;

  if (!children.length) {
    return null;
  }

  const updateNewState = useCallback(
    (key, data = null) => {
      let newState = { ...componentStatus };
      Object.keys(newState).map(x => (newState[x] = false));
      if (data) {
        newState[key] = { ...data };
      } else {
        newState[key] = !componentStatus[key];
      }
      setComponentStatus(newState);
      updateState({ [metadata.metadata.key]: newState });
    },
    [componentStatus]
  );

  const onClickItem = useCallback(
    e => {
      preventPropagation(e);
      setHide(!hide);
    },
    [hide]
  );

  function onVisibleClick(e) {
    preventPropagation(e);
    const isVisible = !isHidden();
    children.forEach(labelObj => {
      labelObj.children.forEach(measure => {
        const isItemVisible = !getItemVisibilityStatus(measure);
        if (isVisible != isItemVisible) {
          parentProps.onVisibleClick(e, measure, false);
        }
      });
    });
    measurementApi.Instance.syncMeasurementsAndToolData();
    measurementApi.Instance.onMeasurementsUpdated();
  }

  const childComponents = createChildComponents(children, {
    ...props,
    componentStatus: componentStatus,
    updateState: updateNewState,
  });
  const { avatarStyle, initials, avatar } = getAvatarStyle(measurement);

  const isHidden = () =>
    children.find(labelObj => {
      return labelObj.children.some(measure => {
        return !getItemVisibilityStatus(measure);
      });
    });
  const isVisible = !isHidden();
  const dim = isVisible ? '' : 'dim';
  const iconStyle = `icon-color ${dim}`;

  return (
    <div className="mt-4">
      <div className="user-wise-measurement-tile" onClick={onClickItem}>
        <div className="flex-align-center">
          <div className="display-flex">
            <ToolbarButton
              {...parentProps}
              label=""
              className="material-icons"
              icon={hide ? 'arrow_drop_up' : 'arrow_drop_down'}
            />
          </div>
          <div className="avatar margin-right-5" style={avatarStyle}>
            {avatar ? '' : initials}
          </div>
          <div>{label}</div>
        </div>
        <div
          className={`${iconStyle} flex-align-center`}
          onClick={onVisibleClick}
        >
          <Icon
            name={isVisible ? 'eye' : 'eye-closed'}
            width="20px"
            height="20px"
          />
        </div>
      </div>
      <div className="horizontal-line"></div>
      <div
        onMouseEnter={parentProps.onMouseEnter}
        onMouseMoveCapture={parentProps.onMouseEnter}
        onMouseLeave={parentProps.onMouseLeave}
      >
        {' '}
        {!hide && childComponents}{' '}
      </div>
    </div>
  );
};

export default memo(UserTile);
