import { measurements } from '@ohif/core';
import store from '@ohif/viewer/src/store';
import * as FlywheelCommon from '@flywheel/extension-flywheel-common';

const FlywheelCommonUtils = FlywheelCommon.Utils;
const {
  getAllMeasurementsForImage,
  measurementToolUtils
} = FlywheelCommonUtils;
const { isOverlapWithOtherROI } = measurementToolUtils.checkMeasurementOverlap;
const { roiTools } = measurementToolUtils;

const { MeasurementHandlers, getImageIdForImagePath } = measurements;

export default function isOverLappedMeasurement(measurement) {
  const state = store.getState();

  const projectConfig = state.flywheel.projectConfig;
  const contourUnique =
    projectConfig && projectConfig.contourUnique ? true : false;
  const labels = projectConfig ? projectConfig.labels || [] : [];
  const isBoundaryCheck = labels.some(
    item => item.boundary && item.boundary.length
  );

  if (
    !roiTools.hasOwnProperty(measurement.toolType) ||
    (!isBoundaryCheck && !contourUnique) ||
    !state.contourProperties.highlightOverlapped
  ) {
    return false;
  }

  const measurementData = getMeasurementData(
    state.timepointManager.measurements,
    measurement.toolType,
    measurement.measurementId
  );
  const imageId = getImageIdForImagePath(measurementData.imagePath);
  let measurements = getAllMeasurementsForImage(
    measurementData.imagePath,
    state.timepointManager.measurements,
    roiTools
  );

  const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
    state.viewports.activeViewportIndex);

  let boundaryMeasurements = [];
  if (isBoundaryCheck) {
    boundaryMeasurements = MeasurementHandlers.handleContourBoundary(
      measurementData,
      imageId,
      labels,
      measurements,
      element
    );
    if (boundaryMeasurements && boundaryMeasurements.length) {
      return true;
    }
  }
  if (boundaryMeasurements.length === 0 && contourUnique) {
    const overLappedMeasurements = isOverlapWithOtherROI(
      measurementData.toolType,
      measurementData,
      imageId,
      element
    );
    const filteredMeasurements = [];
    // Check the overlapped measurements are there in the pending measurement list
    overLappedMeasurements.forEach(overlappedMeasure => {
      if (
        measurements.find(
          measurement => measurement._id === overlappedMeasure._id
        )
      ) {
        let labelConfig = labels.find(
          label => label.value === overlappedMeasure.location
        );
        if (labelConfig) {
          if (!labelConfig.boundary || labelConfig.boundary.length === 0) {
            filteredMeasurements.push(overlappedMeasure);
          } else if (
            labelConfig.boundary.length &&
            !labelConfig.boundary.find(
              x => x.label === measurementData.location
            )
          ) {
            filteredMeasurements.push(overlappedMeasure);
          }
        }
      }
    });
    if (filteredMeasurements && filteredMeasurements.length) {
      return true;
    }
  }

  return false;
}


function getMeasurementData(measurements, toolType, measurementId) {
  measurements = measurements[toolType];
  const measurement = measurements.find(
    measure => measure._id === measurementId
  );
  return measurement;
}
