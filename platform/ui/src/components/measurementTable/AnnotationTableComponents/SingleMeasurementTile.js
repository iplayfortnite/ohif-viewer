import React, { memo, useState } from 'react';
import { measurements } from '@ohif/core'
import { getItemVisibilityStatus, getMeasurementColor, getToolDetailsFromToolbarDefinition, getToolIcon } from './Utils';
import { getTextInfo, preventPropagation } from '../Utils';
import isOverLappedMeasurement from './isOverLappedMeasurement';

import './CommonStyle.styl';

import { MeasurementTableComponents } from './MeasurementTableComponents';
const { SingleMeasurementBox } = MeasurementTableComponents;

const SingleMeasurementTile = (props) => {
  const { parentProps } = props;
  const { questionKeysByToolTypes, onMouseLeave, tableType } = parentProps;
  const { label, updateState, componentStatus, children } = props.metadata;
  const data = children[0];
  const measureInfo = getTextInfo(data);
  const [hover, setHover] = useState(false);
  const [closeColorPicker, setCloseColorPicker] = useState(false);

  const { toolObj, tool } = getToolDetailsFromToolbarDefinition(data);
  const modality = measurements.getModality(store.getState());

  const toolIconComponent = getToolIcon(data, parentProps);
  const isVisible = getItemVisibilityStatus(data);

  const visibleButtonClick = (e) => {
    preventPropagation(e);
    parentProps.onVisibleClick(e, data);
  }

  const deleteButtonClick = (e) => {
    preventPropagation(e);
    if (data.readonly) {
      return;
    }
    parentProps.onDeleteClick(e, data);
    onMouseLeave(e);
  }

  const onEditLabelDescriptionClick = (e) => {
    preventPropagation(e);
    if (data.readonly) {
      return;
    }
    parentProps.onEditLabelDescriptionClick(e, data, tableType);
  }

  const onEditSubFormClick = (e) => {
    preventPropagation(e);
    if (data.readonly) {
      return;
    }
    parentProps.onEditSubFormClick(data);
  }

  const onClickColorPicker = (e) => {
    preventPropagation(e);
    if (data.readonly) {
      return;
    }
    setCloseColorPicker(!closeColorPicker);

    if (closeColorPicker) {
      parentProps.closeColorPicker();
      return;
    }
    parentProps.onClickColorPicker(e, data);
  }

  const onItemClick = (e) => {
    parentProps.onItemClick(e, data);
    updateState(data.measurementId);
  }

  const username = data?.user?.email;
  const isOverlapped = isOverLappedMeasurement(data);
  const dim = parentProps.isMouseEntered && !hover ? 'dim' : '';
  const { location, toolType } = data;
  const measurementColor = getMeasurementColor(data, parentProps.measurements).replace('0.2', '1');
  let questionKey = questionKeysByToolTypes?.[location]?.[toolType] ? questionKeysByToolTypes[location][toolType].join().replaceAll(',', `, `) : '';
  if (data?.isSubForm) {
    questionKey = data?.questionKey;
  }
  return (<SingleMeasurementBox
    onMouseEnter={(e) => {
      setHover(true);
      parentProps?.setActiveHoveredMeasurements([data]);
    }}
    onMouseLeave={(e) => {
      setHover(false);
      parentProps?.setActiveHoveredMeasurements([]);
    }}
    onItemClick={onItemClick}
    dim={dim}
    label={label}
    onClickColorPicker={onClickColorPicker}
    measurementColor={measurementColor}
    onEditLabelDescriptionClick={onEditLabelDescriptionClick}
    onEditSubFormClick={onEditSubFormClick}
    isVisibleEditIcon={parentProps.isMouseEntered && hover}
    parentProps={parentProps}
    measurement={children[0]}
    toolObj={toolObj}
    orientation={parentProps?.getImageOrientation(data)}
    toolIconComponent={toolIconComponent}
    visibleButtonObj={{ isVisible: isVisible, onClick: visibleButtonClick }}
    deleteButtonObj={{ onClick: deleteButtonClick }}
    detailsPaneObj={{
      infoHeader: tool.label,
      toolName: data.toolType,
      user: username,
      field: questionKey,
      measureInfo: measureInfo,
      showDetailsPane: componentStatus[data.measurementId]
    }}
    isOverlapped={isOverlapped}
    showEditIcon={hover}
    hideOrientationAndSlice={modality === 'SM'}
  />);
}

export default memo(SingleMeasurementTile);
