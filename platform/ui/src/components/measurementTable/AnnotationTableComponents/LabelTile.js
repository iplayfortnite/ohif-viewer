import React, { memo, useState } from 'react';

import { getItemVisibilityStatus, getMeasurementColor } from './Utils';
import { preventPropagation } from '../Utils';
import LabelItemTile from './LabelItemTile';
import isOverLappedMeasurement from './isOverLappedMeasurement';
import { MeasurementTableComponents } from './MeasurementTableComponents';
import OHIF from '@ohif/core';

import './CommonStyle.styl';

const { GroupedMeasurementContainer } = MeasurementTableComponents;
const measurementApi = OHIF.measurements.MeasurementApi;

const LabelTile = props => {
  const [hover, setHover] = useState(false);
  const [closeColorPicker, setCloseColorPicker] = useState(false);
  const { parentProps } = props;
  const {
    label,
    key,
    children,
    data,
    updateState,
    componentStatus,
  } = props.metadata;

  if (!children.length) {
    return null;
  }

  const measurements = children;

  const updateNewState = key => {
    let newState = { ...componentStatus[label] };
    Object.keys(newState).map(x => (newState[x] = false));
    newState[key] = !componentStatus[label][key];
    updateState(label, newState);
  };

  const hasContainReadOnly = children.some(x => x.readonly === true);
  const deleteGroupButton = {
    onClick: (e) => {
      preventPropagation(e);
      if (hasContainReadOnly) {
        return;
      }
      parentProps.onDeleteGroupMeasurementClick(e, children);
    }
  }

  const isVisibleGroupButton = children.some(x => {
    return !getItemVisibilityStatus(x);
  });

  const visibleGroupButton = {
    onClick: (e) => {
      preventPropagation(e);
      children.forEach(measurement => {
        if (isVisibleGroupButton !== getItemVisibilityStatus(measurement)) {
          parentProps.onVisibleClick(e, measurement, false);
        }
      });
      measurementApi.Instance.syncMeasurementsAndToolData();
      measurementApi.Instance.onMeasurementsUpdated();
    },
    isVisible: isVisibleGroupButton,
  }
  const onClickColorPicker = (e) => {
    preventPropagation(e);
    if (hasContainReadOnly) {
      return;
    }
    setCloseColorPicker(!closeColorPicker);
    if (closeColorPicker) {
      parentProps.closeColorPicker();
      return;
    }
    parentProps.onClickColorPicker(e, children);
  };
  const isOverlapped = children.some(x => isOverLappedMeasurement(x) === true);
  const dim = parentProps.isMouseEntered && !hover ? 'dim' : '';
  const measurementColor = getMeasurementColor(
    data,
    parentProps.measurements
  ).replace('0.2', '1');
  const activeBorder = isOverlapped
    ? 'boundary-boundary-measurements'
    : componentStatus[label]
    ? 'active-grouped-measurements'
    : 'inactive-grouped-measurements';
  const activeBorderBottom = componentStatus[label]
    ? 'active-border-bottom'
    : 'inactive-border-bottom';

  return (
    <GroupedMeasurementContainer
      activeBorder={activeBorder}
      activeBorderBottom={activeBorderBottom}
      measurementColor={measurementColor}
      measurementCount={children.length}
      visibleButtonObj={visibleGroupButton}
      deleteButtonObj={deleteGroupButton}
      onClickColorPicker={onClickColorPicker}
      dim={dim}
      groupTitle={key}
      onMouseEnter={(e) => {
        setHover(true);
        parentProps?.setActiveHoveredMeasurements(measurements);
      }}
      onMouseLeave={(e) => {
        setHover(false);
        parentProps?.setActiveHoveredMeasurements([]);
      }}
      onGroupClick={(e) => {
        preventPropagation(e);
        if (measurements.length) {
          parentProps.onItemClick(e, measurements[0]);
        }
        updateState(label);
        parentProps.closeColorPicker();
      }}
      subComponent={
        componentStatus[label] && <div>
          {
            measurements.map((measurement, index) => {
              return <LabelItemTile key={measurement.measurementId + index}
                measurement={measurement}
                {...props}
                componentStatus={componentStatus[label] ? componentStatus[label] : {}}
                updateState={updateNewState}
              />;
            })
          }
        </div>
      }
    />
  );
}


export default memo(LabelTile);
