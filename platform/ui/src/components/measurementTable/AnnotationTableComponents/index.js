import UserTile from './UserTile';
import ContainerTile from './ContainerTile';
import MeasurementTile from './MeasurementTile';
import HeaderTile from './HeaderTile';

const ComponentTypes = {
  'HeaderTile': 'HeaderTile',
  'ContainerTile': 'ContainerTile',
  'UserTile': 'UserTile',
  'MeasurementTile': 'MeasurementTile',
}

export {
  UserTile,
  ContainerTile,
  MeasurementTile,
  HeaderTile,
  ComponentTypes
};
