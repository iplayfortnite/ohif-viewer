import React, { memo } from 'react';
import PropTypes from 'prop-types';

import './DetailsPane.styl';
import { preventPropagation } from '../../Utils';

function DetailsPane(props) {
  const { infoHeader, user, toolName, measureInfo, field } = props;
  const detailsPaneClassName = `details-pane-container ${
    !measureInfo.length ? 'details-pane-height' : ''
  }`;

  return (
    <div className={detailsPaneClassName} onClick={e => preventPropagation(e)}>
      <div className="info-title">{infoHeader} Information:</div>
      <div className="sub-info-container">
        <div className="info-container">
          <InfoContent title="User" label={user} />
          <InfoContent title="Tool" label={toolName} />
          <InfoContent title="Field" label={field} />
        </div>
        <div className="info-container">
          {measureInfo &&
            measureInfo.map((x, index) => {
              const data = x.split(':');
              if (data.length == 2) {
                return (
                  <InfoContent key={index} title={data[0]} label={data[1]} />
                );
              }
              return <InfoContent key={index} title={''} label={data[0]} />;
            })}
        </div>
      </div>
    </div>
  );
}

const InfoContent = props => {
  const { title, label } = props;
  if (!label) {
    return null;
  }
  return (
    <div className="display-flex">
      {title ? <div className="title">{title}: </div> : null}{' '}
      <div className="info-content" title={label}>
        {label}
      </div>
    </div>
  );
};

DetailsPane.propTypes = {
  infoHeader: PropTypes.string,
  user: PropTypes.string,
  toolName: PropTypes.string,
  measureInfo: PropTypes.array,
  field: PropTypes.string,
};

export default memo(DetailsPane);
