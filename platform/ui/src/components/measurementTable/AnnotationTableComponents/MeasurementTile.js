import React, { memo } from 'react';

import LabelTile from './LabelTile';
import SingleMeasurementTile from './SingleMeasurementTile';


const MeasurementTile = (props) => {
  const { children } = props.metadata;


  if (children.length === 1) {
    return <SingleMeasurementTile {...props} />;
  }
  return <LabelTile {...props} />;
}

export default memo(MeasurementTile);
