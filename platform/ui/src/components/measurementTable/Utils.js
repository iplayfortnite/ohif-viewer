
import React from 'react';
import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import { isEmpty } from 'lodash';

import * as FlywheelCommon from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import OHIF from '@ohif/core';
import { Icon } from '@ohif/ui';
import componentTypes from './ComponentTypeFactory';

import './AnnotationTableComponents/CommonStyle.styl';
import getAnnotationText from '@ohif/extension-dicom-microscopy/src/annotations/getAnnotationText';


const { getters } = csTools.store;
const { getImageIdForImagePath, getModality } = OHIF.measurements;
const FlywheelCommonUtils = FlywheelCommon.Utils;
const {
  isCurrentFile
} = FlywheelCommonUtils;

export const createChildComponents = (childList, props = {}) => {
  if (!childList?.length) {
    return null;
  }
  return childList.map((item, index) => {
    const { parentProps } = props;
    const TableComponent = componentTypes[item.typeOfComponent];
    return <TableComponent key={item.key + index} metadata={{ ...item, ...props }} parentProps={parentProps} />;
  })
}

export const getAvatarStyle = (data) => {
  const { _id: userId, avatar, firstname, lastname } = data || {};
  const initials =
    !isEmpty(firstname) && !isEmpty(lastname)
      ? firstname[0] + lastname[0]
      : '';
  let avatarStyle = {};
  if (userId) {
    avatarStyle = {
      backgroundSize: 'cover',
    };
    if (avatar) {
      avatarStyle.backgroundImage = `url(${avatar})`;
    } else {
      avatarStyle.backgroundColor = getAvatarColor(userId);
    }
  }
  return { avatarStyle, initials, avatar };
}

export const getAvatarColor = userId => {
  let hash = 0;
  for (let i = 0; i < userId.length; i++) {
    hash = userId.charCodeAt(i) + ((hash << 5) - hash);
  }
  const c = (hash & 0x00ffffff).toString(16).toUpperCase();
  return '#' + '00000'.substring(0, 6 - c.length) + c;
};

export const getActionButton = (icon, onClickCallback, isClosed = false, title = '', disablePointerEvents = false) => {
  const pointerEvent = disablePointerEvents ? 'pointer-events-none' : '';
  return (<span className={`icon-size action-button-container ${pointerEvent}`} style={{ opacity: isClosed ? .5 : 1 }} onClick={onClickCallback}>
    <svg>
      <title>{title || ''}</title>
      <Icon name={icon} className={icon} width='14px' height='14px' />
    </svg>
  </span>
  );
};

export const preventPropagation = (evt) => {
  evt.stopPropagation();
  evt.preventDefault();
}

export const getAnnotationClass = (toolType) => {
  return getters.mouseTools().find(x => x.name === toolType);
}

export const getTextInfo = (measurement) => {
  const tool = getAnnotationClass(measurement.toolType);
  const storeMeasurement = getMeasurementFromStore(measurement);
  let textBoxContent = [];
  if (storeMeasurement) {
    if (getModality(store.getState()) === "SM") {
      textBoxContent = getAnnotationText(storeMeasurement);
    } else {
      cornerstone.getEnabledElements().forEach(enabledElement => {
        const element = enabledElement.element;
        const imageId = getImageIdForImagePath(storeMeasurement.imagePath);
        if (enabledElement && enabledElement.image && enabledElement.image.imageId === imageId) {
          if (tool.getTextInfo) {
            textBoxContent = tool.getTextInfo(storeMeasurement, { imageId, element, image: enabledElement.image });
          }
        }
      });
    }
  }

  return textBoxContent;
}

export const getMeasurementFromStore = (measurement) => {
  const state = store.getState();
  if (state.timepointManager && state.timepointManager.measurements) {
    const measurements = state.timepointManager.measurements;
    const toolData = measurements[measurement.toolType];
    return toolData && toolData.find(x => x._id === measurement.measurementId);
  }
  return null;
}

export const getMeasurementsBySlice = (measureGroup, sliceNumber) => {
  const state = store.getState();
  const projectConfig = state.flywheel.projectConfig;
  let sessionMeasurements = measureGroup.measurements;
  if (!isCurrentFile(state) && projectConfig?.bSlices) {
    sessionMeasurements = sessionMeasurements.filter(item => item.sliceNumber === sliceNumber);
  }
  return sessionMeasurements;
}

export const getQuestionKeyFromStudyForm = (toolType) => {
  const state = store.getState();
  const projectConfig = state.flywheel.projectConfig;
  const studyForm = projectConfig?.studyForm?.components || [];
  const question = studyForm.find((x) => {
    const option = x.values && x.values.find(val => {
      if ((val['measurementTools'] || []).includes(toolType)) {
        return val;
      }
    });
    if (option) {
      return x;
    }
  });

  if (question) {
    return question.key;
  }

  return '';
}

export const getQuestionKeysByToolTypes = () => {
  const state = store.getState();
  const projectConfig = state.flywheel.projectConfig;
  const studyForm = projectConfig?.studyForm?.components || [];
  const labels = {};
  studyForm.forEach((component) => {
    component.values && component.values.forEach(item => {
      item?.requireMeasurements?.forEach((label) => {
        if (!labels[label]) {
          labels[label] = {};
        }
        item?.measurementTools?.forEach(toolType => {
          if (!labels[label][toolType]) {
            labels[label][toolType] = [];
          }
          if (!labels[label][toolType].includes(component.key)) {
            labels[label][toolType].push(component.key);
          }
        });
      });
    });
  });
  return labels;
}
