import './FormMeasurementTable.styl';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import * as FlywheelCommon from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import { withTranslation } from '../../contextProviders';
import { Icon } from './../../elements/Icon';
import { OverlayTrigger } from './../overlayTrigger';
import { ScrollableArea } from './../../ScrollableArea/ScrollableArea.js';
import { TableList } from './../tableList';
import { Tooltip } from './../tooltip';
import { createChildComponents, getQuestionKeysByToolTypes } from './Utils';
import { ComponentTypes } from './AnnotationTableComponents';
import { getMeasurementsBySlice } from './Utils';
import UndoRedo from './UndoRedo';
import {CollapseButton} from './CollapseButton/CollapseButton';

const FlywheelCommonUtils = FlywheelCommon.Utils;
const { isCurrentFile, isBSliceApplicable } = FlywheelCommonUtils;
const { selectCurrentWebImage } = FlywheelCommon.Redux.selectors;

const ColorPicker = require('a-color-picker');
const defaultColor = 'rgba(255, 255, 0, 0.2)';

class FormMeasurementTable extends Component {
  static propTypes = {
    measurementCollection: PropTypes.array.isRequired,
    timepoints: PropTypes.array.isRequired,
    overallWarnings: PropTypes.object.isRequired,
    readOnly: PropTypes.bool,
    onItemClick: PropTypes.func,
    onDeleteClick: PropTypes.func,
    onDeleteGroupMeasurementClick: PropTypes.func,
    onVisibleClick: PropTypes.func,
    onChangeColor: PropTypes.func,
    onEditLabelDescriptionClick: PropTypes.func,
    onEditSubFormClick: PropTypes.func,
    onClickUndo: PropTypes.func,
    onClickRedo: PropTypes.func,
    getImageOrientation: PropTypes.func,
    selectedMeasurementNumber: PropTypes.number,
    t: PropTypes.func,
    saveFunction: PropTypes.func,
    onSaveComplete: PropTypes.func,
    appConfig: PropTypes.object,
    measurements: PropTypes.any,
    dispatchUpdateRoiColor: PropTypes.func,
    dispatchUpdatePaletteColors: PropTypes.func,
    palette: PropTypes.array.isRequired,
    paletteIndex: PropTypes.number.isRequired,
    setMeasurementColorIntensity: PropTypes.func,
    setActiveHoveredMeasurements: PropTypes.func,
    sliceInfo: PropTypes.shape({
      imageId: PropTypes.string,
      sliceNumber: PropTypes.number,
    }),
  };

  static defaultProps = {
    overallWarnings: {
      warningList: [],
    },
    readOnly: false,
  };

  state = {
    selectedKey: null,
    selectedColor: defaultColor,
    showColorPicker: false,
    colorPickCoordinates: { left: 0, top: 0 },
    measurementData: null,
    selectedSession: {},
    isMouseEntered: false,
    componentState: {},
  };

  updateState(newState) {
    const newComponentState = { ...this.state.componentState, ...newState };
    this.setState({ componentState: newComponentState });
  }

  componentDidMount() {
    onclick = () => {
      if (this.state.showColorPicker) {
        this.closeColorPicker();
      }
    };
  }

  render() {
    const { overallWarnings, saveFunction, t, appConfig = {} } = this.props;
    const { enableMeasurementSaveBtn = true } = appConfig;
    const hasOverallWarnings = overallWarnings.warningList.length > 0;

    return (
      <div className="annotationTable">
        <div className="measurementTableHeader">
          {hasOverallWarnings && (
            <OverlayTrigger
              key={'overwall-warning'}
              placement="left"
              overlay={
                <Tooltip
                  placement="left"
                  className="in tooltip-warning"
                  id="tooltip-left"
                >
                  <div className="warningTitle">
                    {t('Criteria nonconformities')}
                  </div>
                  <div className="warningContent">
                    {this.getWarningContent()}
                  </div>
                </Tooltip>
              }
            >
              <span className="warning-status">
                <span className="warning-border">
                  <Icon name="exclamation-triangle" />
                </span>
              </span>
            </OverlayTrigger>
          )}
        </div>
        <div>
          {this.props.measurementCollection.map((measureGroup, index) => (
            <TableList
              key={index}
              customHeader={this.getCustomHeader(measureGroup)}
            >
              <></>
            </TableList>
          ))}
        </div>
        <ScrollableArea>
          <div className="footer-bottom-margin hide-footer-height">
            {this.getMeasurementsGroups()}
          </div>
        </ScrollableArea>
        <div className="measurementTableFooter">
          {saveFunction && enableMeasurementSaveBtn && (
            <button
              onClick={this.saveFunction}
              className="saveBtn"
              data-cy="save-measurements-btn"
            >
              <Icon name="save" width="14px" height="14px" />
              Save measurements
            </button>
          )}
        </div>
        {this.state.showColorPicker && this.getColorSelectorDialog()}
      </div>
    );
  }

  getMeasurementData(measurements, toolType, measurementId) {
    measurements = measurements[toolType];
    const measurement = measurements.find(
      measure => measure._id === measurementId
    );
    return measurement;
  }

  saveFunction = async event => {
    const { saveFunction, onSaveComplete } = this.props;
    if (saveFunction) {
      try {
        const result = await saveFunction();
        if (onSaveComplete) {
          onSaveComplete({
            title: 'STOW SR',
            message: result.message,
            type: 'success',
          });
        }
      } catch (error) {
        if (onSaveComplete) {
          onSaveComplete({
            title: 'STOW SR',
            message: error.message,
            type: 'error',
          });
        }
      }
    }
  };

  getMeasurementsGroups = () => {
    return this.props.measurementCollection.map((measureGroup, index) => {
      return (
        <TableList key={index} customHeader={null}>
          {this.getMeasurementsByUser(measureGroup)}
        </TableList>
      );
    });
  };

  setSubjectList(
    subjectList,
    item,
    subjectId,
    subjectLabel,
    sessionId,
    sessionLabel,
    sliceNumber,
    isFile,
    projectConfig
  ) {
    if (sliceNumber) {
      item.sliceNumber = sliceNumber;
    }
    const { sliceInfo } = this.props;
    const isBSliceOrCurrentFileApplicable = this.isBSliceOrCurrentFileApplicable(
      sliceInfo,
      item,
      projectConfig,
      isFile
    );
    if (
      subjectList.some(
        x => x.key === subjectId && x.children.some(s => s.key === sessionId)
      )
    ) {
      const subject = subjectList.find(
        x => x.key === subjectId && x.children.some(s => s.key === sessionId)
      );
      const session = subject.children.find(s => s.key === sessionId);
      if (isBSliceOrCurrentFileApplicable) {
        this.getUserWiseMeasurementListBySession(
          item,
          session.children,
          isFile,
          projectConfig
        );
      }
    } else {
      const subjectObj = {};
      const sessionObj = {};

      subjectObj.label = subjectLabel;
      subjectObj.key = subjectId;
      subjectObj.typeOfComponent = ComponentTypes.ContainerTile;
      subjectObj.children = [];

      sessionObj.label = sessionLabel;
      sessionObj.key = sessionId;
      sessionObj.typeOfComponent = ComponentTypes.ContainerTile;
      sessionObj.children = [];
      if (isBSliceOrCurrentFileApplicable) {
        this.getUserWiseMeasurementListBySession(
          item,
          sessionObj.children,
          isFile,
          projectConfig
        );
        subjectObj.children.push(sessionObj);
        subjectList.push(subjectObj);
      }
    }
    return subjectList;
  }

  getBSliceWhenMultiSeries() {
    const state = store.getState();
    let projectConfig = state.flywheel?.projectConfig;
    // Special handling to restrict the b slice configurations only for single series
    // Todo - Need to revisit once this restriction can be relaxed
    if (!isBSliceApplicable(state)) {
      projectConfig = {
        ...projectConfig,
        ...{ bSlices: null },
      };
    }
    return projectConfig;
  }

  isBSliceOrCurrentFileApplicable(sliceInfo, item, projectConfig, isFile) {
    return (
      (sliceInfo?.sliceNumber === item?.sliceNumber &&
        projectConfig?.bSlices) ||
      !projectConfig?.bSlices ||
      isFile
    );
  }

  getMeasurementList(measureGroup, isFile) {
    let subjectList = [];
    const state = store.getState();
    const projectConfig = this.getBSliceWhenMultiSeries();
    const measurements = state.timepointManager.measurements;

    if (FlywheelCommonUtils.isCurrentWebImage()) {
      const allSessions = state.flywheel.allSessions;
      const sessionId = selectCurrentWebImage(state)?.parents.session;
      measureGroup.measurements.forEach(item => {
        Object.keys(measurements).forEach(measurementKey => {
          let m = measurements[measurementKey];
          m = m.find(x => x._id === item.measurementId);
          let sliceNumber = null;
          if (!isFile && projectConfig?.bSlices && m) {
            sliceNumber = m.sliceNumber;
          }
          if (m) {
            subjectList = this.setSubjectList(
              subjectList,
              item,
              allSessions[sessionId].parents.subject,
              allSessions[sessionId].subject.label,
              sessionId,
              allSessions[sessionId].label,
              sliceNumber,
              isFile,
              projectConfig
            );
          }
        });
      });
      return subjectList;
    } else {
      const associations = state.flywheel.associations;
      const sessions = state.flywheel.sessions;
      measureGroup.measurements.forEach(item => {
        Object.keys(measurements).forEach(measurementKey => {
          let m = measurements[measurementKey];
          m = m.find(x => x._id === item.measurementId);
          let sliceNumber = null;
          if (!isFile && projectConfig?.bSlices && m) {
            sliceNumber = m.sliceNumber;
          }
          if (m) {
            const studyInstanceUid = m.StudyInstanceUID || m.studyInstanceUid;
            if (associations[studyInstanceUid]) {
              const associationDetails = associations[studyInstanceUid];
              const session = sessions.find(
                x => x._id === associationDetails.session_id
              );
              if (session) {
                const subject = session.subject;
                this.setSubjectList(
                  subjectList,
                  item,
                  subject._id,
                  subject.label,
                  session._id,
                  session.label,
                  sliceNumber,
                  isFile,
                  projectConfig
                );
              }
            }
          }
        });
      });
      return subjectList;
    }
  }

  getUserWiseMeasurementListBySession(
    measurement,
    userWiseMeasurementList,
    isFile,
    projectConfig
  ) {
    // to create new user
    if (
      !userWiseMeasurementList.some(
        user => measurement.user && user.key === measurement.user.email
      )
    ) {
      const userObj = {};
      userObj.label = measurement?.user?.email || '';
      userObj.key = userObj.label;
      userObj.typeOfComponent = ComponentTypes.UserTile;
      userObj.data = { ...measurement.user };
      userObj.children = [];
      userWiseMeasurementList.push({ ...userObj });
    }

    const user = userWiseMeasurementList.find(
      user => user.key === (measurement?.user?.email || '')
    );
    // to create label object
    if (!user.children.some(x => x.key === measurement.location)) {
      const newObj = {};
      newObj.label = measurement.label;
      newObj.key = measurement.location;
      newObj.typeOfComponent = ComponentTypes.MeasurementTile;
      newObj.data = { ...measurement };
      newObj.children = [];
      user.children.push(newObj);
    }
    const label = user?.children?.find(x => x.key === measurement.location);
    // to add measurement
    if (
      label &&
      !label.children.find(x => x.key === measurement.measurementId)
    ) {
      label.children.push(measurement);
      if (!isFile && projectConfig?.bSlices) {
        const { sliceInfo } = this.props;
        label.children = label.children.filter(
          x => x.sliceNumber === sliceInfo?.sliceNumber
        );
      }
    }

    return userWiseMeasurementList;
  }

  onMouseEnter(e) {
    if (!this.state?.isMouseEntered) {
      this.setState({ isMouseEntered: true }, () => {
        this.props.setMeasurementColorIntensity(true);
      });
    }
  }

  onMouseLeave(e) {
    if (this.state?.isMouseEntered) {
      this.setState({ isMouseEntered: false }, () => {
        this.props.setMeasurementColorIntensity(false);
      });
    }
  }

  getMeasurementsByUser = measureGroup => {
    const state = store.getState();
    const isFile = isCurrentFile(state);
    let currentVolumetricImage =
      state.flywheel.currentMetaImage || state.flywheel.currentNiftis;
    let subjectList = {};
    const volumetricImages = Array.isArray(currentVolumetricImage)
      ? currentVolumetricImage
      : currentVolumetricImage
        ? [currentVolumetricImage]
        : [];
    if (volumetricImages?.length > 0) {
      subjectList = this.getMeasurementsDataByUser(
        measureGroup,
        volumetricImages,
        isFile
      );
    } else {
      subjectList = this.getMeasurementList(measureGroup, isFile);
    }
    const newProps = {
      parentProps: {
        ...this.props,
        onClickColorPicker: this.onClickColorPicker,
        isMouseEntered: this.state.isMouseEntered,
        componentState: this.state.componentState,
        updateState: newState => this.updateState(newState),
        onItemClick: this.onItemClick,
        closeColorPicker: this.closeColorPicker,
        questionKeysByToolTypes: getQuestionKeysByToolTypes(),
        onMouseEnter: this.onMouseEnter.bind(this),
        onMouseLeave: this.onMouseLeave.bind(this),
        tableType: 'FORM_ANNOTATION_TABLE',
      },
    };
    const childComponents = createChildComponents(subjectList, { ...newProps });
    return <div className="subject-container">{childComponents}</div>;
  };

  getMeasurementListForVolumetricImage(measureGroup, volumetricImages, isFile) {
    const state = store.getState();
    const allSessions = state.flywheel.allSessions;
    const measurements = state.timepointManager.measurements;
    const subjectList = [];
    const projectConfig = this.getBSliceWhenMultiSeries();
    (volumetricImages || []).forEach(volumetricImage => {
      if (
        volumetricImage.parents &&
        allSessions[volumetricImage.parents.session]
      ) {
        const session = allSessions[volumetricImage.parents.session];
        const subject = session.subject;

        let isNewSession = false;
        let isNewSubject = false;

        const subjectObj = {};
        const sessionObj = {};

        subjectObj.key = subject._id;
        subjectObj.label = subject.label;
        subjectObj.typeOfComponent = ComponentTypes.ContainerTile;
        subjectObj.children = [];
        isNewSubject = true;

        sessionObj.key = volumetricImage.parents.session;
        sessionObj.label = session.label;
        sessionObj.typeOfComponent = ComponentTypes.ContainerTile;
        sessionObj.children = [];
        isNewSession = true;

        measureGroup.measurements.map(measurement => {
          const measureInfo = (measurements[measurement.toolType] || []).find(
            x => x._id === measurement.measurementId
          );
          if (measureInfo) {
            const studyInstanceUID =
              measureInfo.StudyInstanceUID || measureInfo.studyInstanceUid;
            if (studyInstanceUID === volumetricImage.filename) {
              const { sliceInfo } = this.props;
              if (
                sliceInfo?.sliceNumber === measurement?.sliceNumber ||
                (isFile && !projectConfig?.bSlices)
              ) {
                this.getUserWiseMeasurementListBySession(
                  measurement,
                  sessionObj.children,
                  isFile,
                  projectConfig
                );
              }
            }
          }
        });
        if (isNewSession) {
          subjectObj.children.push(sessionObj);
        }
        if (isNewSubject) {
          subjectList.push(subjectObj);
        }
        return subjectList;
      }
    });
    return subjectList;
  }

  getMeasurementsDataByUser = (
    measureGroup,
    currentVolumetricImage,
    isFile
  ) => {
    if (currentVolumetricImage) {
      const measurements = this.getMeasurementListForVolumetricImage(
        measureGroup,
        currentVolumetricImage,
        isFile
      );
      return measurements;
    }
    return null;
  };

  onItemClick = (event, measurementData) => {
    this.closeColorPicker();
    if (this.props.readOnly) return;

    const { measurements } = this.props;

    const measurement = measurements[measurementData.toolType].find(
      m => m.measurementNumber === measurementData.measurementNumber
    );

    this.setState({
      selectedKey: measurementData.measurementNumber,
      selectedColor: measurement.color || defaultColor,
      measurementData,
    });

    if (this.props.onItemClick) {
      this.props.onItemClick(event, measurementData);
    }
  };

  getCustomHeader = measureGroup => {
    const { sliceInfo } = this.props;
    const measurements = getMeasurementsBySlice(
      measureGroup,
      sliceInfo?.sliceNumber
    );
    return (
      <React.Fragment>
        <div className="tableListHeaderTitle flex-align-center">
          <CollapseButton />
          Annotations{' '}
          <div className="total-measurement-count">{measurements.length}</div>
        </div>
        {measureGroup.maxMeasurements && (
          <div className="maxMeasurements">
            {this.props.t('MAX')} {measureGroup.maxMeasurements}
          </div>
        )}
        <UndoRedo
          onClickUndo={this.props.onClickUndo}
          onClickRedo={this.props.onClickRedo}
        />
      </React.Fragment>
    );
  };

  getWarningContent = () => {
    const { warningList = '' } = this.props.overallWarnings;

    if (Array.isArray(warningList)) {
      const listedWarnings = warningList.map((warn, index) => {
        return <li key={index}>{warn}</li>;
      });

      return <ol>{listedWarnings}</ol>;
    } else {
      return <React.Fragment>{warningList}</React.Fragment>;
    }
  };

  // To Show the color selector dialog
  onClickColorPicker = (event, measurementData) => {
    const screenHeight = window.innerHeight;

    // Align dialog 20px above or below from clicked point to not overlap the picker dialog over the palette and other buttons.
    const offset = 20;

    const pickerSize = {
      height: 297, // Color selector dialog height
      width: 232, // Color selector dialog width
    };

    const colorPickCoordinates = {
      left: event.clientX - pickerSize.width / 2,
      top:
        screenHeight / 2 > event.clientY
          ? event.clientY + offset
          : event.clientY - offset - pickerSize.height,
    };

    let selectedColor = defaultColor;
    let currentData = {};
    if (Array.isArray(measurementData)) {
      currentData = measurementData[0];
    } else {
      currentData = measurementData;
    }
    const measure = this.props.measurements[currentData.toolType].find(
      x => x._id === currentData.measurementId
    );
    if (measure) {
      selectedColor = measure.color;
    }

    this.setState({ showColorPicker: false }, () => {
      this.setState(
        {
          showColorPicker: true,
          colorPickCoordinates,
          measurementData,
          selectedColor,
        },
        () => this.onColorChangeHandler()
      );
    });
  };

  closeColorPicker = () => {
    const { measurements } = this.props;
    this.props.dispatchUpdatePaletteColors(measurements);
    this.setState({ showColorPicker: false });
  };

  onColorChangeHandler = () => {
    ColorPicker.from('.picker').on('change', picker => {
      const { palette } = this.props;
      const selectedColor = `rgba(${picker.rgba.join(',')})`;
      this.setState(
        {
          selectedColor,
        },
        () => {
          if (Array.isArray(this.state.measurementData)) {
            this.state.measurementData.forEach(x =>
              this.props.onChangeColor(selectedColor, x)
            );
          } else {
            this.props.onChangeColor(selectedColor, this.state.measurementData);
          }

          this.props.dispatchUpdateRoiColor({
            current: selectedColor,
            next: palette[this.props.paletteIndex],
          });
        }
      );
    });
  };

  getColorSelectorDialog = () => {
    return (
      <div
        className="color-picker"
        style={{
          left: this.state.colorPickCoordinates.left,
          top: this.state.colorPickCoordinates.top,
          zIndex: 3,
        }}
        onClick={e => e.stopPropagation()}
      >
        <div
          className="picker"
          acp-color={this.state.selectedColor}
          acp-palette={this.props.palette.join('|')}
          acp-show-rgb="no"
          acp-show-hsl="no"
          acp-show-hex="no"
          acp-show-alpha="yes"
          acp-palette-editable="no"
          acp-use-alpha-in-palette="no"
        ></div>
      </div>
    );
  };
}

const connectedComponent = withTranslation(['FormMeasurementTable', 'Common'])(
  FormMeasurementTable
);
export { connectedComponent as FormMeasurementTable };
export default connectedComponent;
