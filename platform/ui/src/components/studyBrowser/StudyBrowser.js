import React from 'react';
import PropTypes from 'prop-types';
import { Thumbnail } from './Thumbnail.js';
import './StudyBrowser.styl';

function StudyBrowser(props) {
  const {
    studies,
    onThumbnailClick,
    onThumbnailDoubleClick,
    supportsDrag,
  } = props;

  return (
    <div className="study-browser">
      <div className="scrollable-study-thumbnails">
        {studies
          .map((study, studyIndex) => {
            const { StudyInstanceUID } = study;
            return study.thumbnails.map((thumb, thumbIndex) => {
              // TODO: Thumb has more props than we care about?
              const {
                altImageText,
                displaySetInstanceUID,
                imageId,
                InstanceNumber,
                numImageFrames,
                SeriesDescription,
                SeriesNumber,
                stackPercentComplete,
                imageRotation,
              } = thumb;

              return (
                <div
                  key={thumb.displaySetInstanceUID}
                  className="thumbnail-container"
                  data-cy="thumbnail-list"
                >
                  <Thumbnail
                    supportsDrag={supportsDrag}
                    key={`${studyIndex}_${thumbIndex}`}
                    id={`${studyIndex}_${thumbIndex}`} // Unused?
                    // Study
                    StudyInstanceUID={StudyInstanceUID} // used by drop
                    // Thumb
                    altImageText={altImageText}
                    imageId={imageId}
                    InstanceNumber={InstanceNumber}
                    displaySetInstanceUID={displaySetInstanceUID} // used by drop
                    numImageFrames={numImageFrames}
                    SeriesDescription={SeriesDescription}
                    SeriesNumber={SeriesNumber}
                    stackPercentComplete={stackPercentComplete}
                    imageRotation={imageRotation}
                    // Events
                    onClick={onThumbnailClick.bind(
                      undefined,
                      displaySetInstanceUID
                    )}
                    onDoubleClick={onThumbnailDoubleClick}
                  />
                </div>
              );
            });
          })
          .flat()}
      </div>
    </div>
  );
}

const noop = () => {};

StudyBrowser.propTypes = {
  studies: PropTypes.arrayOf(
    PropTypes.shape({
      StudyInstanceUID: PropTypes.string.isRequired,
      thumbnails: PropTypes.arrayOf(
        PropTypes.shape({
          altImageText: PropTypes.string,
          displaySetInstanceUID: PropTypes.string.isRequired,
          imageId: PropTypes.string,
          InstanceNumber: PropTypes.number,
          numImageFrames: PropTypes.number,
          SeriesDescription: PropTypes.string,
          SeriesNumber: PropTypes.number,
          stackPercentComplete: PropTypes.number,
        })
      ),
    })
  ).isRequired,
  supportsDrag: PropTypes.bool,
  onThumbnailClick: PropTypes.func,
  onThumbnailDoubleClick: PropTypes.func,
};

StudyBrowser.defaultProps = {
  studies: [],
  supportsDrag: true,
  onThumbnailClick: noop,
  onThumbnailDoubleClick: noop,
};

// Improve the performance by skipping unwanted rendering of the
// series panel thumbnails and allow update only study/series loading update
// or during the color map change
const areEqual = (prevProps, nextProps) => {
  let isSame = true;
  (nextProps.studies || []).forEach((study, index) => {
    const prevStudy = prevProps.studies?.[index];
    if (!prevStudy || !isSame) {
      isSame = false;
      return;
    }
    study.thumbnails.forEach((thumbnail, thumbIndex) => {
      const prevThumbnail = prevStudy.thumbnails?.[thumbIndex];
      if (!prevThumbnail) {
        isSame = false;
        return;
      }
      if (
        prevThumbnail.stackPercentComplete !== thumbnail.stackPercentComplete ||
        prevThumbnail.colorMapId !== thumbnail.colorMapId
      ) {
        isSame = false;
      }
    });
  });
  return isSame; // If false, then allow re-render else skip the re-rendering
};

const studyBrowserMemo = React.memo(StudyBrowser, areEqual);

export { studyBrowserMemo as StudyBrowser };
