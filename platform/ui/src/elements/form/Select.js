import './Select.css';

import React, { Component } from 'react';

import PropTypes from 'prop-types';
import { Icon } from '../Icon';

class Select extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    options: PropTypes.arrayOf(
      PropTypes.shape({
        key: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired,
      })
    ),
    value: PropTypes.string,
    onChange: PropTypes.func,
    showIcon: PropTypes.bool,
    rootClass: PropTypes.string,
  };

  handleChange = event => {
    const value = event.target.value;
    this.setState({ value });
    if (this.props.onChange) this.props.onChange(value);
  };

  render() {
    const rootClass = this.props.rootClass || '';
    const css_classes = `form-control select-ohif ${rootClass} border-radius-0`;
    return (
      <div className="select-ohif-container form-select-container">
        {this.props.label && (
          <label className="select-ohif-label" htmlFor={this.id}>{this.props.label}</label>
        )}
        <select className={css_classes} {...this.props} title={this.props.title ? this.props.title : this.props.value}>
          {this.props.options.map(({ key, value }) => {
            return (
              <option key={key} value={value}>
                {value}
              </option>
            );
          })}
        </select>
        {this.props.showIcon ?
          <span className='drop-down-icon-container'>
            <Icon name='arrow_drop_down' className='material-icons' width='14px' height='14px' />
          </span> : null}
      </div>
    );
  }
}

export { Select };
