import Protocol from '../classes/Protocol';
import ViewportStructure from '../classes/ViewportStructure';
import Viewport from '../classes/Viewport';
import Stage from '../classes/Stage';

import { ProtocolStore, ProtocolStrategy } from './classes';

export function loadProtocols(projectConfig) {
  const configLayout = projectConfig.layouts;
  const protocolStrategy = new ProtocolStrategy();
  const protocolStore = new ProtocolStore(protocolStrategy);
  if (Array.isArray(configLayout)) {
    configLayout.forEach((layout, index) => {
      const proto = new Protocol(layout.name);
      const protoRule = {
        id: 'rule_' + (index + 1),
        weight: 2,
        required: false,
        attribute: layout.selector?.tag || 'fileName',
        constraint: {
          contains: {
            value: layout.selector?.match,
          },
        },
      };
      let numRows = 1;
      let numCols = 1;
      const newViewports = [];
      let layoutViewports = layout.viewports || [];

      // For backward compatibility to support the multi-level
      // layout configuration for grid layouts.
      if (!layout.type || layout.type === 'grid') {
        numRows = layout.viewports.length;
        numCols = 1;
        layoutViewports.forEach((columnViewports, rowIndex) => {
          numCols =
            columnViewports.length > numCols ? columnViewports.length : numCols;
          columnViewports.forEach((viewport, colIndex) => {
            const seriesMatchingRule = {
              id: 'viewport_' + rowIndex + '_' + colIndex,
              weight: 1,
              required: false,
              attribute: viewport.tag || 'fileName',
              constraint: {
                contains: {
                  value: viewport.match || viewport.fileName,
                },
              },
            };
            const newViewport = new Viewport();
            newViewport.seriesMatchingRules.push(seriesMatchingRule);
            newViewport.viewportSettings = {
              type: viewport.type || '2D',
              spanData: viewport.span || null,
              readOnly: viewport.readOnly || false,
            };
            if (viewport.fileType === 'image') {
              newViewport.viewportSettings = {
                ...newViewport.viewportSettings,
                fileName: viewport.fileName || null,
              };
            } else {
              newViewport.viewportSettings = {
                ...newViewport.viewportSettings,
                orientation: viewport.orientation || null,
              };
            }
            newViewports.push(newViewport);
          });
        });
      } else if (layout.type === 'asymmetric') {
        const grid = getGridLayout(layoutViewports);
        numRows = grid.numRows;
        numCols = grid.numCols;
        layoutViewports = grid.viewports;
        layoutViewports.forEach((columnViewports, rowIndex) => {
          numCols =
            columnViewports.length > numCols ? columnViewports.length : numCols;
          columnViewports.forEach((viewport, colIndex) => {
            const newViewport = new Viewport();

            if (viewport.fileType !== 'image') {
              const seriesMatchingRule = {
                id: 'viewport_' + rowIndex + '_' + colIndex,
                weight: 1,
                required: false,
                attribute: viewport.tag || 'SeriesDescription',
                constraint: {
                  contains: {
                    value:
                      viewport.match || viewport.SeriesDescription || '^.*',
                  },
                },
              };
              newViewport.seriesMatchingRules.push(seriesMatchingRule);
            }
            if (viewport.type === 'nifti' || viewport.type === 'metaimage') {
              const seriesMatchingRule = {
                id: 'viewport_' + rowIndex + '_' + colIndex,
                weight: 1,
                required: false,
                attribute: 'orientation',
                constraint: {
                  contains: {
                    value: viewport.orientation || viewport.SeriesDescription,
                  },
                },
              };
              newViewport.seriesMatchingRules.push(seriesMatchingRule);
            }
            newViewport.viewportSettings = {
              type: viewport.type || '2D',
              spanData: viewport.span || null,
              readOnly: viewport.readOnly || false,
              isDefaultLayout: viewport.isDefaultLayout || false,
            };
            if (viewport.fileType === 'image') {
              const seriesMatchingRule = {
                id: 'viewport_' + rowIndex + '_' + colIndex,
                weight: 1,
                required: false,
                attribute: 'fileName',
                constraint: {
                  contains: {
                    value: viewport.fileName,
                  },
                },
              };
              newViewport.seriesMatchingRules.push(seriesMatchingRule);
              newViewport.viewportSettings = {
                ...newViewport.viewportSettings,
                fileName: viewport.fileName || null,
              };
            } else {
              newViewport.viewportSettings = {
                ...newViewport.viewportSettings,
                orientation: viewport.orientation || null,
              };
            }
            if (viewport.type === '2D MPR' && viewport.showFov) {
              newViewport.viewportSettings.showFov = viewport.showFov;
              // If diameters not configured, then take [1, 3, 6] diameter fov indicators as default
              newViewport.viewportSettings.fovDiameters = viewport.fovDiameters || [
                1,
                3,
                6,
              ];
              newViewport.viewportSettings.hideFovGrid =
                viewport.hideFovGrid || !viewport.showFov;
            }
            newViewports.push(newViewport);
          });
        });
      }

      const viewportStruct = new ViewportStructure('grid', {
        Rows: numRows,
        Columns: numCols,
      });
      const stage1 = new Stage(viewportStruct, 'stage_' + index);
      stage1.viewports = newViewports;
      proto.stages.push(stage1);
      proto.addProtocolMatchingRule(protoRule);
      protocolStore.addProtocol(proto);
    });
  }
  return protocolStore;
}

/**
 * Get the grid layout corresponding to the viewports
 * @param {Array} viewports
 * @returns Object - layout data
 */
function getGridLayout(viewports) {
  const toleranceFactor = 1;
  let viewportDim = {};
  let rowIndex = -1;

  const minimumSize = getMinimumSize(viewports, toleranceFactor);
  const minWidth = minimumSize.minWidth;
  const minHeight = minimumSize.minHeight;

  // From the minimum size, define that many rows and columns, then define each actual viewport
  // position as by spanning in to multiple rows/columns
  const totalSpanRows = Math.round(100 / minHeight);
  const totalSpanColumns = Math.round(100 / minWidth);

  const rowSize = 100 / totalSpanRows;
  const columnSize = 100 / totalSpanColumns;

  // Map the viewports in each position and also list each viewport span in to how many rows/columns from the starting
  // row/column index.
  viewports.forEach((viewport, viewportIndex) => {
    if (!viewport.position) {
      return;
    }
    if (!viewportDim[viewport.position.y]) {
      rowIndex++;
      viewportDim[viewport.position.y] = {
        count: 0,
        index: rowIndex,
        viewports: [],
      };
    }
    viewportDim[viewport.position.y].count += 1;
    viewportDim[viewport.position.y].viewports.push(viewport);

    let xPos = 0;
    let startXIndex = 0;
    let endXIndex = 0;
    const startPos = {
      x: scaleNumberIn0To100Range(viewport.position.x),
      y: scaleNumberIn0To100Range(viewport.position.y),
    };
    const endPos = {
      x: scaleNumberIn0To100Range(viewport.position.x + viewport.size.width),
      y: scaleNumberIn0To100Range(viewport.position.y + viewport.size.height),
    };

    while (
      xPos < startPos.x + toleranceFactor &&
      Math.abs(startPos.x - xPos) > toleranceFactor
    ) {
      xPos += columnSize;
      startXIndex++;
    }

    xPos = 0;
    while (
      xPos < endPos.x + toleranceFactor &&
      Math.abs(endPos.x - xPos) > toleranceFactor
    ) {
      xPos += columnSize;
      endXIndex++;
    }

    let yPos = 0;
    let startYIndex = 0;
    let endYIndex = 0;
    while (
      yPos < startPos.y + toleranceFactor &&
      Math.abs(startPos.y - yPos) > toleranceFactor
    ) {
      yPos += rowSize;
      startYIndex++;
    }

    yPos = 0;
    while (
      yPos < endPos.y + toleranceFactor &&
      Math.abs(endPos.y - yPos) > toleranceFactor
    ) {
      yPos += rowSize;
      endYIndex++;
    }
    viewport.isDefaultLayout = viewportIndex === 0;
    viewport.span = {
      startX: startXIndex,
      startY: startYIndex,
      endX: endXIndex,
      endY: endYIndex,
      totalSpanRows,
      totalSpanColumns,
    };
  });

  const keys = Object.keys(viewportDim) || [];
  let numRows = keys.length;
  let numCols = 0;
  const multiLevelViewports = [];
  keys.forEach(key => {
    multiLevelViewports.push([]);
  });
  keys.forEach(key => {
    viewportDim[key].viewports.sort((v1, v2) => v1.position.x - v2.position.x);
    numCols =
      viewportDim[key].count > numCols ? viewportDim[key].count : numCols;
    multiLevelViewports[viewportDim[key].index] = viewportDim[key].viewports;
  });

  return { numRows, numCols, viewports: multiLevelViewports };
}

/**
 * Get the minimum width and height as per the protocol configuration
 * @param {Array} viewports
 * @param {number} toleranceFactor
 * @returns Object - minimum width and height values
 */
function getMinimumSize(viewports, toleranceFactor) {
  let minWidth = 100;
  let minHeight = 100;

  // Find minimum width and height specified in the protocol configuration
  viewports.forEach(viewport => {
    if (!viewport.size || !viewport.size.width || !viewport.size.height) {
      return;
    }

    minWidth =
      minWidth < scaleNumberIn0To100Range(viewport.size.width)
        ? minWidth
        : scaleNumberIn0To100Range(viewport.size.width);
    minHeight =
      minHeight < scaleNumberIn0To100Range(viewport.size.height)
        ? minHeight
        : scaleNumberIn0To100Range(viewport.size.height);
  });

  const totalSpanRows = Math.round(100 / minHeight);
  const totalSpanColumns = Math.round(100 / minWidth);

  const rowSize = 100 / totalSpanRows;
  const columnSize = 100 / totalSpanColumns;

  minWidth = columnSize;
  minHeight = rowSize;

  // Adjust the minimum width and height by checking the position specified in the protocol configuration
  viewports.forEach(viewport => {
    if (!viewport.position) {
      return;
    }

    const startPos = {
      x: scaleNumberIn0To100Range(viewport.position.x),
      y: scaleNumberIn0To100Range(viewport.position.y),
    };
    let i = 0;
    let xPos = 0;
    do {
      i++;
      xPos = i * minWidth;
      if (
        Math.abs(startPos.x - xPos) > toleranceFactor &&
        Math.abs(startPos.x - xPos) < minWidth - toleranceFactor
      ) {
        minWidth = Math.abs(startPos.x - xPos);
        xPos = i * minWidth;
      }
    } while (Math.abs(startPos.x - xPos) > minWidth);

    i = 0;
    let yPos = 0;
    do {
      i++;
      yPos = i * minHeight;
      if (
        Math.abs(startPos.y - yPos) > toleranceFactor &&
        Math.abs(startPos.y - yPos) < minHeight - toleranceFactor
      ) {
        minHeight = Math.abs(startPos.y - yPos);
        yPos = i * minHeight;
      }
    } while (Math.abs(startPos.y - yPos) > minHeight);
  });
  return { minWidth, minHeight };
}

function scaleNumberIn0To100Range(number) {
  if (Math.abs(number) <= 1) {
    return Math.round(Math.abs(number * 100));
  }
  return 100;
}
