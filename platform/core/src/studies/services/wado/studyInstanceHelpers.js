import DICOMWeb from '../../../DICOMWeb';
import metadataProvider from '../../../classes/MetadataProvider';
import getWADORSImageId from '../../../utils/getWADORSImageId';
import cornerstoneWADOImageLoader from 'cornerstone-wado-image-loader';
import getReferencedSeriesSequence from './getReferencedSeriesSequence';
import { adapters as SiemensAdapaters } from '@flywheel/extension-flywheel-dicom-siemens';
import { adapters as MultiFrameAdapaters } from '@flywheel/extension-flywheel-dicom-multiframe';
import _ from 'lodash';

const getSourceImageSequence = instance => {
  const tagKey = '00082112';
  const tag = instance[tagKey] || {};

  const sequence = [];
  const tagValue = tag.Value || [];

  tagValue.forEach(item => {
    sequence.push({
      referencedSOPClassUID: DICOMWeb.getString(item['00081150']),
      referencedSOPInstanceUID: DICOMWeb.getString(item['00081155']),
    });
  });

  return sequence;
};
/**
 * Create a plain JS object that describes a study (a study descriptor object)
 * @param {Object} server Object with server configuration parameters
 * @param {Object} aSopInstance a SOP Instance from which study information will be added
 */
function createStudy(server, aSopInstance) {
  // TODO: Pass a reference ID to the server instead of including the URLs here
  return {
    series: [],
    seriesMap: Object.create(null),
    seriesLoader: null,
    wadoUriRoot: server.wadoUriRoot,
    wadoRoot: server.wadoRoot,
    qidoRoot: server.qidoRoot,
    PatientName: DICOMWeb.getName(aSopInstance['00100010']),
    PatientID: DICOMWeb.getString(aSopInstance['00100020']),
    PatientAge: DICOMWeb.getNumber(aSopInstance['00101010']),
    PatientSize: DICOMWeb.getNumber(aSopInstance['00101020']),
    PatientWeight: DICOMWeb.getNumber(aSopInstance['00101030']),
    AccessionNumber: DICOMWeb.getString(aSopInstance['00080050']),
    StudyTime: DICOMWeb.getString(aSopInstance['00080030']),
    StudyDate: DICOMWeb.getString(aSopInstance['00080020']),
    FrameOfReferenceUID: DICOMWeb.getString(aSopInstance['00200052']),
    ReferencedSeriesSequence: getReferencedSeriesSequence(aSopInstance),
    modalities: DICOMWeb.getString(aSopInstance['00080061']), // TODO -> Rename this.. it'll take a while to not mess this one up.
    StudyDescription: DICOMWeb.getString(aSopInstance['00081030']),
    NumberOfStudyRelatedInstances: DICOMWeb.getString(aSopInstance['00201208']),
    StudyInstanceUID: DICOMWeb.getString(aSopInstance['0020000D']),
    InstitutionName: DICOMWeb.getString(aSopInstance['00080080']),
    Manufacturer: DICOMWeb.getString(aSopInstance['00080070']),
    SourceImageSequence: getSourceImageSequence(aSopInstance),
  };
}

/** Returns a WADO url for an instance
 *
 * @param StudyInstanceUID
 * @param SeriesInstanceUID
 * @param SOPInstanceUID
 * @returns  {string}
 */
function buildInstanceWadoUrl(
  server,
  StudyInstanceUID,
  SeriesInstanceUID,
  SOPInstanceUID
) {
  // TODO: This can be removed, since DICOMWebClient has the same function. Not urgent, though
  const params = [];

  params.push('requestType=WADO');
  params.push(`studyUID=${StudyInstanceUID}`);
  params.push(`seriesUID=${SeriesInstanceUID}`);
  params.push(`objectUID=${SOPInstanceUID}`);
  params.push('contentType=application/dicom');
  params.push('transferSyntax=*');

  const paramString = params.join('&');

  return `${server.wadoUriRoot}?${paramString}`;
}

function buildInstanceWadoRsUri(
  server,
  StudyInstanceUID,
  SeriesInstanceUID,
  SOPInstanceUID
) {
  return `${server.wadoRoot}/studies/${StudyInstanceUID}/series/${SeriesInstanceUID}/instances/${SOPInstanceUID}`;
}

function buildInstanceFrameWadoRsUri(
  server,
  StudyInstanceUID,
  SeriesInstanceUID,
  SOPInstanceUID,
  frame
) {
  const baseWadoRsUri = buildInstanceWadoRsUri(
    server,
    StudyInstanceUID,
    SeriesInstanceUID,
    SOPInstanceUID
  );
  frame = frame != null || 1;

  return `${baseWadoRsUri}/frames/${frame}`;
}

async function makeSOPInstance(server, study, instance) {
  const naturalizedInstance = await metadataProvider.addInstance(instance, {
    server,
  });

  const {
    StudyInstanceUID,
    SeriesInstanceUID,
    SOPInstanceUID,
    SourceInstanceSequence = {},
  } = naturalizedInstance;

  let series = study.seriesMap[SeriesInstanceUID];

  if (!series) {
    series = {
      SeriesInstanceUID,
      SeriesDescription: naturalizedInstance.SeriesDescription,
      Modality: naturalizedInstance.Modality,
      SeriesNumber: naturalizedInstance.SeriesNumber,
      SeriesDate: naturalizedInstance.SeriesDate,
      SeriesTime: naturalizedInstance.SeriesTime,
      instances: [],
    };
    study.seriesMap[SeriesInstanceUID] = series;
    study.series.push(series);
  }

  const wadouri = buildInstanceWadoUrl(
    server,
    StudyInstanceUID,
    SeriesInstanceUID,
    SourceInstanceSequence.ReferencedSOPInstanceUID || SOPInstanceUID
  );
  const baseWadoRsUri = buildInstanceWadoRsUri(
    server,
    StudyInstanceUID,
    SeriesInstanceUID,
    SourceInstanceSequence.ReferencedSOPInstanceUID || SOPInstanceUID
  );
  const wadorsuri = buildInstanceFrameWadoRsUri(
    server,
    StudyInstanceUID,
    SeriesInstanceUID,
    SourceInstanceSequence.ReferencedSOPInstanceUID || SOPInstanceUID
  );

  const sopInstance = {
    metadata: naturalizedInstance,
    baseWadoRsUri,
    wadouri,
    wadorsuri,
    wadoRoot: server.wadoRoot,
    imageRendering: server.imageRendering,
    thumbnailRendering: server.thumbnailRendering,
  };

  if (instance.getImageId) {
    sopInstance.getImageId = instance.getImageId;
  }

  // let vendors create frames once the common dataset is naturalized
  const sopInstances = _vendorMakeSopInstanceList(sopInstance, study);

  Array.prototype.push.apply(series.instances, sopInstances);

  return sopInstances[0];
}

/**
 * Let vendors make, split or add private tags for SOP InstanceMetadata, and provide custom image urls when necessary
 * Also convert the bit allocation from 8-bit to 16-bit for MPR integration with VTK js(VTK js not supporting 8-bit data)
 * @param {InstanceMetadata} sopInstance
 * @param {StudyMetadata} study
 * @returns InstanceMetadata after vendor processing; Use first if vendor splits the input instance to multiple instances
 */
const _vendorMakeSopInstanceList = (sopInstance, study) => {
  const firstInstance = _.cloneDeep(sopInstance.metadata);
  const {
    Modality,
    PerFrameFunctionalGroupsSequence,
    NumberOfFrames,
  } = firstInstance;

  if (study.Manufacturer === 'SIEMENS') {
    const metadataList = SiemensAdapaters.makeSopInstanceList(
      [sopInstance.metadata],
      study
    );
    const sopInstances = metadataList.map(metadata => {
      metadataProvider.addInstance(metadata);
      return {
        ...sopInstance,
        ...{
          metadata,
          SOPInstanceUID: metadata.SOPInstanceUID,
          getImageId: metadata.getImageId,
        },
      };
    });
    return sopInstances;
  } else {
    if (NumberOfFrames > 1 && Modality !== 'SM') {
      const multiFrameAdapter = MultiFrameAdapaters;
      const metaDataList = multiFrameAdapter.makeSopInstanceList(
        [firstInstance],
        study
      );
      if (sopInstance.metadata.BitsAllocated === 8 && Modality === 'OPT') {
        sopInstance.metadata.BitsAllocated = 16;
      }
      const instance = {
        ...sopInstance,
        ...{
          metadata: metaDataList[0],
          SOPInstanceUID: metaDataList[0].SOPInstanceUID,
          getImageId: metaDataList[0].getImageId,
        },
      };
      return [instance];
    }
  }

  return [sopInstance];
};

const updateCSWadoImageLoader = (sopInstance, originalInstance) => {
  if (
    sopInstance.thumbnailRendering === 'wadors' ||
    sopInstance.imageRendering === 'wadors'
  ) {
    // If using WADO-RS for either images or thumbnails,
    // Need to add this to cornerstoneWADOImageLoader's provider
    // (it won't be hit on cornerstone.metaData.get, but cornerstoneWADOImageLoader
    // will try if you don't add data to cornerstoneWADOImageLoader.wadors.metaDataManager).

    const wadoRSMetadata = Object.assign(originalInstance);

    const { NumberOfFrames } = sopInstance.metadata;

    if (NumberOfFrames) {
      for (let i = 0; i < NumberOfFrames; i++) {
        const wadorsImageId = getWADORSImageId(sopInstance, i);

        cornerstoneWADOImageLoader.wadors.metaDataManager.add(
          wadorsImageId,
          wadoRSMetadata
        );
      }
    } else {
      const wadorsImageId = getWADORSImageId(sopInstance);

      cornerstoneWADOImageLoader.wadors.metaDataManager.add(
        wadorsImageId,
        wadoRSMetadata
      );
    }
  }
};

/**
 * Add a list of SOP Instances to a given study object descriptor
 * @param {Object} server Object with server configuration parameters
 * @param {Object} study The study descriptor to which the given SOP instances will be added
 * @param {Array} sopInstanceList A list of SOP instance objects
 */
async function addInstancesToStudy(server, study, sopInstanceList) {
  const sopInstanceMap = {};
  return Promise.all(
    sopInstanceList.map(sopInstance => {
      const sopInstancePromise = makeSOPInstance(server, study, sopInstance);
      sopInstancePromise.then(sopi => {
        if (
          sopi.thumbnailRendering === 'wadors' ||
          sopi.imageRendering === 'wadors'
        ) {
          const imageKey = sopi.wadorsuri || sopi.getImageId?.();
          sopInstanceMap[imageKey] = sopInstance;
        }
      });
      return sopInstancePromise;
    })
  ).then(results => {
    const seriesInstanceUIDs = _.uniq(
      results.map(result => result.metadata.SeriesInstanceUID)
    );
    const wadoRequest = sopi => {
      if (
        sopi.thumbnailRendering === 'wadors' ||
        sopi.imageRendering === 'wadors'
      ) {
        const imageKey = sopi.wadorsuri || sopi.getImageId?.();
        const sopInstance = sopInstanceMap[imageKey];
        updateCSWadoImageLoader(sopi, sopInstance);
      }
    };
    // since dicom is loading from middle slice request images closer to middle slice initially for better scroll experience.
    seriesInstanceUIDs.forEach(seriesInstanceUID => {
      const sopInstances = study.seriesMap[seriesInstanceUID].instances;
      let middleSlice = Math.ceil((sopInstances.length || 1) / 2);
      if (sopInstances.length !== 1) {
        wadoRequest(sopInstances[middleSlice]);
      }
      for (let i = 1; i <= middleSlice; i++) {
        wadoRequest(sopInstances[middleSlice - i]);
        if (middleSlice + i < sopInstances.length) {
          wadoRequest(sopInstances[middleSlice + i]);
        }
      }
    });
  });
}

const createStudyFromSOPInstanceList = async (server, sopInstanceList) => {
  if (Array.isArray(sopInstanceList) && sopInstanceList.length > 0) {
    const firstSopInstance = sopInstanceList[0];
    const study = createStudy(server, firstSopInstance);
    await addInstancesToStudy(server, study, sopInstanceList);
    return study;
  }
  throw new Error('Failed to create study out of provided SOP instance list');
};

export { createStudyFromSOPInstanceList, addInstancesToStudy };
