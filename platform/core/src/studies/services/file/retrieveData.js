import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import fileLoaders from './loaders';
const fileStudyMetaDataPromises = new Map();

async function RetrieveData(server, fileContainerId, fileName, ...rest) {
  let retrieveMetadataLoaders = [];
  const studyPromiseCollection = [];
  const shouldUpdateUrl =
    fileName &&
    Array.isArray(fileName) &&
    FlywheelCommonUtils.isCurrentWebImage();
  const getServer = shouldUpdateUrl
    ? containerId => {
        return {
          ...server,
          baseURL: getUrl(server, containerId),
        };
      }
    : () => server;

  // identify loader
  const fileNames =
    fileName && Array.isArray(fileName)
      ? fileName
      : !fileName
      ? []
      : [fileName];
  const containers =
    fileContainerId && Array.isArray(fileContainerId)
      ? fileContainerId
      : !fileContainerId
      ? []
      : [fileContainerId];

  fileNames.forEach((fName, index) => {
    fName = decodeURIComponent(fName);
    if (fileStudyMetaDataPromises.has(fName)) {
      studyPromiseCollection.push(fileStudyMetaDataPromises.get(fName));
      return;
    }
    for (let FileLoader of fileLoaders) {
      const extensionRegExp = FileLoader.extensionRegExp;

      if (extensionRegExp && extensionRegExp.test(fName)) {
        retrieveMetadataLoaders.push(
          new FileLoader(
            getServer(containers[index], fName),
            containers[index],
            fName,
            ...rest
          )
        );
        break;
      }
    }
  });

  retrieveMetadataLoaders.forEach(loader => {
    const promise = loader.execLoad();
    studyPromiseCollection.push(promise);
    loader.fileName = decodeURIComponent(loader.fileName);
    fileStudyMetaDataPromises.set(loader.fileName, promise);
  });

  return Promise.all(studyPromiseCollection).then(result => {
    if (result && Array.isArray(result)) {
      const studies = [];
      result.forEach(study => {
        if (Array.isArray(study) && study.length > 0) {
          const isUpdated = updateStudies(studies, study[0]);

          if (!isUpdated) {
            studies.push(study[0]);
          }
        } else {
          studies.push(study);
        }
      });
      return studies;
    }
  });
}

const getUrl = (server, containerId) => {
  const baseUrl = server.baseURL;
  let url;

  if (baseUrl === containerId) {
    return baseUrl;
  } else {
    url = baseUrl.replace(server.container, containerId);
  }

  return url;
};

/**
 * Method to retrieve the segmentation data
 * @param {Object} server server object
 * @param {Object} [study] - parent study
 * @param  {string} seriesUid series instance UID.
 * @param {string} [fileContainerId] - active container ID
 * @param {string} [fileName] - current displayed file name
 * @param {Array} [segments] - array of segmentation datas
 * @param {Object} [metadata]
 * @param {number} [derivedIndex] - derived series index
 */
async function RetrieveSegmentData(
  server,
  study,
  seriesUid,
  fileContainerId,
  fileName,
  segments,
  metadata = {},
  derivedIndex = -1
) {
  let retrieveMetadataLoader;
  // identify loader
  for (let FileLoader of fileLoaders) {
    const extensionRegExp = FileLoader.extensionRegExp;

    if (extensionRegExp && extensionRegExp.test(segments[0].name)) {
      retrieveMetadataLoader = new FileLoader(
        server,
        fileContainerId,
        fileName
      );
      break;
    }
  }

  if (!retrieveMetadataLoader) {
    return;
  }

  const derivedSeriesList = retrieveMetadataLoader.loadDerivedSeries(
    server,
    study,
    seriesUid,
    segments,
    metadata,
    derivedIndex
  );

  return derivedSeriesList;
}

const updateStudies = (studies, currentStudy) => {
  return studies.some((study, index) => {
    const isZipFile =
      study.dataProtocol === 'zip' && currentStudy.dataProtocol === 'zip';
    const isSameZipStudy =
      study.StudyInstanceUID === currentStudy.StudyInstanceUID && isZipFile;
    if (isSameZipStudy) {
      // Updating base url in the case of multiple series under same study
      // so that multiple series can use same baseurl (excluding fileName from baseUrl).
      const url = currentStudy.baseURL;
      const lastIndex = url.lastIndexOf('/');
      const secondLastIndex = url.lastIndexOf('/', lastIndex - 1);
      const updatedUrl = url.slice(0, secondLastIndex);
      studies[index].baseURL = updatedUrl;

      // Adding series to the series array from same study.
      studies[index].series.push(...currentStudy.series);
    }
    return isSameZipStudy;
  });
};

export { RetrieveData, RetrieveSegmentData };
