import _addInstancesToStudy from '../webImage/addInstancesToStudy';
import WebImageDataLoaderSync from '../webImage/retrieveDataLoader';

export default class TiffImageDataLoaderSync extends WebImageDataLoaderSync {
  static extensionRegExp = /\.tiff?$/;

  constructor(server, containerId, fileName) {
    super(server, containerId, fileName);
    this.DATA_PROTOCOL = 'tiff';
  }

  _getSOPInstanceUIDFromUrl(url = '') {
    return url.replace(/^tiff:\/\//, '');
  }

  async addInstancesToStudy(study, dataProtocol, metadata) {
    return _addInstancesToStudy(
      study,
      dataProtocol,
      this._getSOPInstanceUIDFromUrl
    );
  }
}
