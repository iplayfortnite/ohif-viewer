import _ from 'lodash';
import cornerstoneMath from 'cornerstone-math';
import metadataProvider from '@ohif/core/src/classes/MetadataProvider';

const derivedSuffix = '-derived'; // Derived segmentation series suffix

const XYZ = 'xyz'.split('');
function getNormalAxis(imageOrientation) {
  if (!imageOrientation || imageOrientation.length < 6) {
    return undefined;
  }
  const rowCosines = new cornerstoneMath.Vector3(
    parseFloat(imageOrientation[0]),
    parseFloat(imageOrientation[1]),
    parseFloat(imageOrientation[2])
  );
  const columnCosines = new cornerstoneMath.Vector3(
    parseFloat(imageOrientation[3]),
    parseFloat(imageOrientation[4]),
    parseFloat(imageOrientation[5])
  );
  const columnAxis = XYZ.reduce((result, axis) =>
    Math.abs(columnCosines[axis]) > Math.abs(columnCosines[result])
      ? axis
      : result
  );
  const rowAxis = XYZ.reduce((result, axis) =>
    Math.abs(rowCosines[axis]) > Math.abs(rowCosines[result]) ? axis : result
  );
  return XYZ.find(axis => axis !== columnAxis && axis !== rowAxis);
}

class Dimension {
  constructor(axis, sliceName, index) {
    this.axis = axis;
    this.sliceName = sliceName;
    this.index = index;
  }
}

class VolumetricImageStudyPreparer {
  constructor() {

  }

  getDataProtocol() { }

  async getFileUrl(
    study,
    header,
    baseURL,
    isSource = false
  ) {
    return baseURL || study.baseURL;
  }

  createImageId(url) {
    return null;
  }

  getNumberOfSpatialSlices(
    header, sliceDimensionIndex
  ) {
    return 0;
  }

  loadHeader(url, supportsRangeRead, isMetaHeader = false) {
    return null;
  }

  getOriginalSeriesUID(derivedSeriesInstanceUID) {
    if (derivedSeriesInstanceUID.indexOf(derivedSuffix) >= 0) {
      return derivedSeriesInstanceUID.split(derivedSuffix)[0];
    }
    return derivedSeriesInstanceUID;
  }

  async setImageFileURlFromHeader(
    study,
    header,
    baseURL
  ) {
    return header;
  }

  /**
   * createSpatialSeries - Creates (3) series for x, y and z dimensions on the
   * provided volumetric(Nifti, Meta-image like MHD etc) study.
   *
   * @param  {Object} study  The volumetric study that will receive the spatial series.
   * @param  {string} seriesUid series instance UID.
   * @param  {Object} header The header for the volumetric file referenced in the study.
   * @param  {Object} metadata  Metadata for given volumetric study.
   * @param  {string} dataProtocol it represents protocol of given data.
   * @param  {boolean} [isDerived=false] It defines if series is derived or not
   * @param  {number} [derivedIndex=-1] It defines the index of the derived series, if -1 then assumes not a derived series
   * @return {Object} returns header
   */
  async createSpatialSeries(
    study,
    seriesUid,
    header,
    metadata = {},
    dataProtocol,
    isDerived = false,
    derivedIndex = -1
  ) {
    let indexSuffix = isDerived && derivedIndex >= 0 ? derivedIndex + '' : '';
    let suffix = isDerived ? derivedSuffix + indexSuffix : '';
    let spatialDimensions = [];
    if (study.dataProtocol === this.getDataProtocol()) {
      // In case of segmentation in volumetric study(Nifti, MHD etc), create all 3 derived series irrespective of series UID passed
      spatialDimensions = [
        new Dimension('z', 'Axial' + suffix, 1),
        new Dimension('y', 'Coronal' + suffix, 2),
        new Dimension('x', 'Sagittal' + suffix, 3),
      ];
    } else {
      study.series.forEach(series => {
        // Create segmentation derived series corresponding to the series instance UID passed
        if (
          series.SeriesInstanceUID.indexOf(derivedSuffix) < 0 &&
          (!seriesUid || (!!seriesUid && series.SeriesInstanceUID === seriesUid))
        ) {
          let dimension = 'z';
          if (!!series && !!series.instances && series.instances.length > 0) {
            let imageOrientation =
              series.instances[0].metadata.ImageOrientationPatient;
            if (!imageOrientation && series.instances[0].metadata.NumberOfFrames > 1) {
              const imageId = series.instances[0].getImageId(0);
              const imagePlane = cornerstone.metaData.get('imagePlaneModule', imageId);
              imageOrientation = imagePlane?.ImageOrientationPatient;
            }
            if (imageOrientation) {
              dimension = getNormalAxis(imageOrientation);
            }
          }
          spatialDimensions.push(
            new Dimension(dimension, series.SeriesInstanceUID + suffix)
          );
        }
      });
    }

    study.timeSlices = header.timeSlices;

    study.series = study.series || [];
    study.series.push(
      ...spatialDimensions.map(dimension => {
        return {
          SeriesInstanceUID: dimension.sliceName,
          SeriesDescription: dimension.sliceName,
          SeriesNumber: header.SeriesNumber || dimension.index,
          Modality: header.Modality || metadata.Modality,
          SeriesDate: header.seriesDate || metadata.seriesDate,
          SeriesTime: header.seriesTime || metadata.seriesTime,
          dimension: dimension.axis,
          instances: [],
          dataProtocol,
        };
      })
    );

    return header;
  }

  /**
   * createSpatialInstances - For each spatial series (x, y, z) in the study,
   * creates the instances for each spatial slice.
   * Derived class should have the implementation with corresponding SOP Class UIDS
   *
   * @param  {Object} study The volumetric study whose series will receive instances for each spatial slice.
   * @param  {Object} header The file header.
   * @param  {Boolean} supportsRangeRead Whether the server hosting the volumetric file supports range
   * read requests (to fetch just the initial bytes).
   * @param  {Object} metadata  Metadata for given volumetric study.
   * @param  {boolean} [isSeg=false] It defines if series is segmentation or not
   * @param  {string} [baseURL=false] external baseURL to be used
   * @param  {string} [sourceBaseURL=false] external sourceBaseURL to be used in case isSeg true
   * @return {Promise}                    A promise that is fulfilled when
   * all the spatial slices for all series in the study have been created.
   */
  createSpatialInstances(
    study,
    header,
    supportsRangeRead,
    metadata = {},
    isSeg = false,
    baseURL,
    sourceBaseURL = ''
  ) {
    return null;
  }

  /**
 * createTemporalSeries - For a study of a 4D files (with temporal
 * volumes), this function creates one series containing a 3D volume
 * for each time point.
 *
 * @param  {Object} study The study.
 */
  createTemporalSeries(study) {
  }

  /**
   * prepareStudies - Changes the data object that is going to be supplied
   * to the viewer by finding all volumetric files, opening them up to check for:
   * (1) the number of spatial slices and expanding the instances array by
   * filling them with a instance for each slice.
   * (2) the number of time slices (for 4D images) and making a new series for
   * each time point and
   *
   * @param  {Object} studies  The data being supplied to the viewer. It gets
   * changed when the promise returned by the function is fulfilled.
   * @param  {string} seriesUid series instance UID.
   * @param  {Object[]} metadata  Metadata for given volumetric studies.
   * @param  {Boolean} supportsRangeRead Indicates whether the server hosting the
   * volumetric files support range reading to allow querying just the initial bytes
   * of the files (as only the header is necessary at this time).
   * @param  {string} dataProtocol it represents protocol of given data.
   * @param  {string} [baseURL=false] external baseURL to be used
   * @param  {string} [sourceBaseURL=false] external sourceBaseURL to be used in case isSeg true
   * @param  {number} [derivedIndex=-1] It defines the index of the derived series, if -1 then assumes not a derived series
   * @return {Promise}        a promise resolved when the volumetric studies in the
   * data object has been properly prepared for the viewer.
   */
  prepareStudies(
    studies,
    seriesUid,
    metadata = [],
    supportsRangeRead = true,
    dataProtocol,
    baseURL,
    sourceBaseURL = '',
    derivedIndex = -1
  ) {
    const isDerived = !!sourceBaseURL;
    // this is the returned promise and it resolves when:
    // 1. all volumetric files have been downloaded (if supportsRangeRead,
    //    fetches only the initial bytes, for the volumetric header)
    // 2. one series will be created
    // 3. all volumetric instances have been "expanded", ie,
    //    a new instance has been created for each slice
    // 4. the metadata for each slice has been gathered
    const prepareStudiesPromise = new Promise((resolve, reject) => {
      // an array of promises of loading the header of the unique files that appear
      // on the instances on the data object that is being supplied to the viewer
      const loadHeaderTasks = studies.map((study, index) => ({
        study,
        index,
        promise: this.loadHeader(baseURL || study.baseURL, supportsRangeRead, true),
      }));

      const studiesPreparedTasks = [];
      // For each volumetric file, when we have its header read, we prepare it
      // for the viewer
      loadHeaderTasks.forEach(({ promise, study, index }) => {
        studiesPreparedTasks.push(
          promise
            .then(header =>
              this.setImageFileURlFromHeader(
                study,
                header,
                baseURL
              )
            )
            // creates 3 spatial series for this study (x,y,z)
            .then(header =>
              this.createSpatialSeries(
                study,
                seriesUid,
                header,
                metadata[index],
                dataProtocol,
                isDerived,
                derivedIndex
              )
            )
            // creates the instances for the 3 spatial series
            .then(header =>
              this.createSpatialInstances(
                study,
                header,
                supportsRangeRead,
                metadata[index],
                isDerived,
                baseURL,
                sourceBaseURL
              )
            )
            // creates the corresponding 3D series for each time point (4D only)
            .then(() => this.createTemporalSeries(study))
            .then(() => metadataProvider.updateFromStudy(study))
            .catch(reject)
        );
      });

      // the returned promise is resolved only when the promises of preparing
      // all studies have been fulfilled
      Promise.all(studiesPreparedTasks)
        .then(resolve)
        .catch(reject);
    });

    return prepareStudiesPromise;
  }

  _getSOPInstanceUIDFromUrl(url) {
    return url;
  }

  _decorateSOPInstance(
    instance,
    SeriesInstanceUID,
    SeriesDescription,
    SeriesNumber,
    sourceBaseURL
  ) {
    // into SOPInstanceUID method ensure inner context to be instance var
    const getSOPInstanceUIDFn = this._getSOPInstanceUIDFromUrl.bind(this);
    const getOriginalSeriesUIDFn = this.getOriginalSeriesUID.bind(this);
    Object.defineProperty(instance, 'SOPInstanceUID', {
      enumerable: true,
      configurable: true,
      get: function () {
        return getSOPInstanceUIDFn(this.url);
      }.bind(instance),
    });

    if (sourceBaseURL) {
      Object.defineProperty(instance, 'ReferencedSOPInstanceUID', {
        enumerable: true,
        configurable: true,
        get: function () {
          return getSOPInstanceUIDFn(sourceBaseURL);
        }.bind(instance),
      });
    }

    Object.defineProperty(instance, 'getImageId', {
      enumerable: true,
      configurable: true,
      value: function () {
        return this.url;
      }.bind(instance),
    });

    // expose seriesInstanceUID
    Object.defineProperty(instance, 'SeriesInstanceUID', {
      enumerable: true,
      configurable: true,
      writable: true,
      value: SeriesInstanceUID,
    });

    // TODO volumetric image segmentation, check if other values are required
    // expose ReferencedSeriesSequence
    Object.defineProperty(instance, 'ReferencedSeriesSequence', {
      enumerable: true,
      configurable: true,
      writable: true,
      value: {
        SeriesInstanceUID: getOriginalSeriesUIDFn(SeriesInstanceUID),
      },
    });

    // expose seriesInstanceUID
    Object.defineProperty(instance, 'SeriesDescription', {
      enumerable: true,
      configurable: true,
      writable: true,
      value: SeriesDescription,
    });

    // expose SeriesNumber
    Object.defineProperty(instance, 'SeriesNumber', {
      enumerable: true,
      configurable: true,
      writable: true,
      value: SeriesNumber,
    });

    // into SOPInstanceUID method ensure inner context to be instance var
    const metadata = Object.assign({}, instance);
    Object.defineProperty(instance, 'metadata', {
      enumerable: true,
      configurable: true,
      writable: true,
      value: metadata,
    });
  }

}

export default VolumetricImageStudyPreparer;
