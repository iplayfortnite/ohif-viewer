import {
  Utils as FlywheelCommonUtils,
  HTTP as FlywheelCommonHTTP,
} from '@flywheel/extension-flywheel-common';
import FileDataLoaderSync from '../FileDataLoaderSync';
import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import createSegmentNiftiFile from '@ohif/extension-cornerstone/src/utils/segmentSave/createSegmentNiftiFile';
import * as cornerstoneNIFTIImageLoader from '@cornerstonejs/nifti-image-loader';

const { getRequestURL } = FlywheelCommonHTTP.utils.REQUEST_UTILS;
const { GET_CONTAINER_FILES } = FlywheelCommonHTTP.urls;

const { getFileMetadata } = FlywheelCommonHTTP.services;
const { getSmartCTRangeList, cornerstoneUtils } = FlywheelCommonUtils;
const { getFileName } = cornerstoneUtils;
const segmentationModule = cornerstoneTools.getModule('segmentation');

export default class VolumetricImageDataLoaderSync extends FileDataLoaderSync {
  // Properties to be overwrite
  static extensionRegExp = '';
  DATA_PROTOCOL = '';

  constructor(server, containerId, fileName, segmentationFileNames) {
    super(server, containerId, fileName);
    this.segmentationFileNames = Array.isArray(segmentationFileNames)
      ? segmentationFileNames
      : [];
  }

  getStudyPreparer() {
    return null;
  }

  getOptions() {
    const { segmentationFileNames } = this;

    const options = {
      ...super.getOptions(),
      segmentationFileNames,
    };

    return options;
  }

  configLoad() {
    const { containerId } = this;
    const client = {
      retrieveMetadata: fileName => {
        return getFileMetadata(containerId, fileName);
      },
    };

    this.client = client;
  }

  createStudy(server, fileName, metadata = {}) {
    const fileNameSegs = this.segmentationFileNames || [];
    const dataProtocol = this.DATA_PROTOCOL;
    let fileBaseUrl = server.baseURL;
    if (server.container && server.container !== this.containerId) {
      fileBaseUrl = fileBaseUrl.replace(server.container, this.containerId);
    }
    const study = {
      dataProtocol,
      series: [],
      seriesMap: Object.create(null),
      seriesLoader: Object.freeze({
        hasNext() {
          return fileNameSegs.length > 0;
        },
        async next() {
          const segmentsSeriesLength = 3;
          const _fileName = fileNameSegs.shift();
          await this.getStudyPreparer().prepareStudies(
            [study],
            null,
            [metadata.info],
            undefined,
            dataProtocol,
            `${dataProtocol}:${fileBaseUrl}/${_fileName}`,
            `${dataProtocol}:${fileBaseUrl}/${fileName}`,
            fileNameSegs.length
          );

          return study.series.slice(study.series.length - segmentsSeriesLength);
        },
      }),
      baseURL: `${dataProtocol}:${fileBaseUrl}/${fileName}`,
      PatientName: metadata.PatientName,
      PatientID: metadata.PatientID,
      patientAge: metadata.PatientBirthDate,
      patientWeight: metadata.PatientWeight,
      StudyDate: metadata.AcquisitionDateTime,
      modalities: metadata.Modality,
      StudyInstanceUID: fileName,
      institutionName: metadata.InstitutionName,
    };

    return study;
  }

  /**
   * Method to load the segmentations as derived series
   * @param {Object} server server object
   * @param {Object} [study] - parent study
   * @param  {string} seriesUid series instance UID.
   * @param {Array} [segments] - array of segmentation datas
   * @param {Object} [metadata]
   * @param {number} [derivedIndex] - derived series index
   */
  async loadDerivedSeries(
    server,
    study,
    seriesUid,
    segments,
    metadata = {},
    derivedIndex
  ) {
    const segmentsSeriesLength = study.series.length;
    if (!segments || segments.length <= 0) {
      return [];
    }
    const segment = segments.shift();
    // Update the container id from base URL to that of selected segment file
    const serverBaseUrl = server.container
      ? server.baseURL
      : study.wadoRoot
      ? study.wadoRoot
      : '';
    let segmentBaseUrl = '';
    if (!server.container) {
      // If study is DICOM and loading nifti, the nifti file url will be missing in server, so finding it
      const urlParams = {
        pathParams: {
          containerId: segment.containerId || '',
        },
      };
      const config = { prefixRoot: false };
      segmentBaseUrl = getRequestURL(GET_CONTAINER_FILES, urlParams, config);
    } else {
      segmentBaseUrl = serverBaseUrl.replace(
        server.container,
        segment.containerId
      );
    }
    const checkSegmentDetails = segment.containerId === '';

    if (checkSegmentDetails) {
      const requestURL = await this.getBaseUrl();
      segmentBaseUrl = requestURL;
    }

    const segmentFileName = segment.name;
    const dataProtocol = server.container ? this.DATA_PROTOCOL : 'viewer';
    const dataProtocolSeg = this.DATA_PROTOCOL;
    await this.getStudyPreparer().prepareStudies(
      [study],
      seriesUid,
      [metadata.info],
      undefined,
      dataProtocolSeg,
      checkSegmentDetails
        ? `${segmentBaseUrl}`
        : `${dataProtocolSeg}:${segmentBaseUrl}/${segmentFileName}`,
      `${dataProtocol}:${serverBaseUrl}/${study.studyInstanceUID ||
        study.StudyInstanceUID}`,
      derivedIndex
    );

    // For volumetric image(Nifti, Meta-image like MHD etc) study, creating all axial, sagittal and coronal derived series's for loading segment.
    // For DICOM, creating only to the corresponding series plane.
    const newCount = dataProtocol === this.DATA_PROTOCOL ? 3 : 1;
    const startIndex = study.series.length - newCount;
    return study.series.slice(startIndex);
  }

  async getBaseUrl() {
    const state = store.getState();
    const activeViewport = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
      state.viewports.activeViewportIndex
    );
    const toolState = cornerstoneTools.getToolState(activeViewport, 'stack');
    const stackData = toolState.data[0];
    const imageId = stackData.imageIds[0];
    const metaData = cornerstone.metaData.get('imagePlaneModule', imageId);
    const activeLabelmapIndex = segmentationModule.getters.activeLabelmapIndex(
      activeViewport
    );
    const labelmap3D = segmentationModule.getters.labelmap3D(
      activeViewport,
      activeLabelmapIndex
    );
    const extension = '.nii.gz';
    const sliceSpacing = [1, 1, 1];
    const baseSeriesMetaInfo = {
      metaData: { ...metaData, sliceSpacing },
      numberOfFrames: stackData.imageIds.length,
    };
    const fileName = getFileName(imageId);
    const segmentsMap = this.getSegmentationDataMap(
      labelmap3D.labelmaps2D,
      fileName,
      extension
    );

    const segmentDetails = [];
    Object.keys(segmentsMap).forEach(async key => {
      const segmentDetail = segmentsMap[key];
      segmentDetails.push(segmentDetail);
    });

    const file = createSegmentNiftiFile(
      baseSeriesMetaInfo,
      segmentDetails[0],
      labelmap3D.labelmaps2D,
      true,
      extension
    );

    return new Promise(async (resolve, reject) => {
      try {
        const buffer = await file.arrayBuffer();
        const nifti = cornerstoneNIFTIImageLoader.nifti;
        const blob = new Blob([buffer], {
          type: 'application/octet-stream',
        });
        const blobURL = URL.createObjectURL(blob);
        const requestURL = `nifti:${blobURL}`;
        const headerPromise = nifti.loadHeader(requestURL, true);
        const imagePromise = nifti.loadImage(requestURL);
        return Promise.all([(headerPromise, imagePromise)]).then(
          async ([header, image]) => {
            URL.revokeObjectURL(blobURL);
            resolve(requestURL);
            return;
          }
        );
      } catch (err) {
        reject(err);
      }
      return;
    });
  }

  getSegmentationDataMap = (labelMaps2D, fileName, fileExtension) => {
    const segmentsMap = {};
    const allRanges = getSmartCTRangeList();
    const selectedRange = store.getState()?.smartCT?.selectedRange;
    const segmentIndex = allRanges.findIndex(
      r => r.label === selectedRange?.label
    );
    (labelMaps2D || []).forEach((labelMap, index) => {
      if (segmentIndex >= 0) {
        segmentsMap[segmentIndex] = segmentsMap[segmentIndex] || {
          segmentIndex,
          start: Number.MAX_VALUE,
          end: -1,
          slices: [],
          name: fileName,
          fileExtension,
        };
      } else {
        return;
      }

      segmentsMap[segmentIndex].start =
        segmentsMap[segmentIndex].start > index
          ? index
          : segmentsMap[segmentIndex].start;
      segmentsMap[segmentIndex].end =
        segmentsMap[segmentIndex].end < index
          ? index
          : segmentsMap[segmentIndex].end;
      if (!segmentsMap[segmentIndex].slices.includes(index)) {
        segmentsMap[segmentIndex].slices.push(index);
      }
    });

    return segmentsMap;
  };

  async addInstancesToStudy(study, dataProtocol, metadata) {
    return this.getStudyPreparer().prepareStudies(
      [study],
      [metadata.info],
      undefined,
      undefined,
      dataProtocol
    );
  }
}
