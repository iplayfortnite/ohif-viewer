// Note: This file is the moved and renamed version of file/loaders/nifti/retrieveDataLoader.js.

import NiftiImageStudyPreparer from './NiftiImageStudyPreparer';
import VolumetricImageDataLoaderSync from '../retrieveDataLoader';

export default class NiftiDataLoaderSync extends VolumetricImageDataLoaderSync {
  static extensionRegExp = /\.[nii|nii.gz]+$/;

  constructor(server, containerId, fileName, segmentationFileNames) {
    super(server, containerId, fileName, segmentationFileNames);
    this.DATA_PROTOCOL = 'nifti';
  }

  getStudyPreparer() {
    return new NiftiImageStudyPreparer();
  }
}
