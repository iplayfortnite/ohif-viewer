// Note: This file is the moved and renamed version of file/loaders/metaImage/retrieveDataLoader.js.

import MetaImageStudyPreparer from './MetaImageStudyPreparer';
import VolumetricImageDataLoaderSync from '../retrieveDataLoader';
import { META_IMAGE_DATA_PROTOCOL } from '../LoaderConstants';

export default class MhdDataLoaderSync extends VolumetricImageDataLoaderSync {
  static extensionRegExp = /\.[mhd]+$/;

  constructor(server, containerId, fileName, segmentationFileNames) {
    super(server, containerId, fileName, segmentationFileNames);
    this.DATA_PROTOCOL = META_IMAGE_DATA_PROTOCOL;
  }

  getStudyPreparer() {
    return new MetaImageStudyPreparer(this.DATA_PROTOCOL);
  }
}
