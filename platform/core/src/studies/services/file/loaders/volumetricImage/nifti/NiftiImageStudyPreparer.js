// Note: This file is the moved and renamed version of file/loaders/nifti/prepareNiftiStudies.js.
// It is the refactored version of previous functional component changed to class structure with common
// functionalities moved to parent class

import _ from 'lodash';
import * as cornerstoneNIFTIImageLoader from '@cornerstonejs/nifti-image-loader';
import VolumetricImageStudyPreparer from '../VolumetricImageStudyPreparer';
import { NIFTI_DATA_PROTOCOL } from '../LoaderConstants';
import { HTTP as FlywheelCommonHTTP } from '@flywheel/extension-flywheel-common';

const { getRequestURL } = FlywheelCommonHTTP.utils.REQUEST_UTILS;

const nifti = cornerstoneNIFTIImageLoader.nifti;
const ImageId = nifti.ImageId;
const URL_SCHEMA = NIFTI_DATA_PROTOCOL + ':';

const SOP_CLASS_UIDS = {
  MR: '1.2.840.10008.5.1.4.1.1.4',
  SEG: '1.2.840.10008.5.1.4.1.1.66.4',
};
const apiRoot = window.config.apiRoot || '';

// Get the full file request url with configured api root if any
function getFileRequestURL(url) {
  const splitSubStr = url.split(URL_SCHEMA) || [];
  const relativeUrl = splitSubStr[1] || imageIdFormat.url;
  const fileUrl = splitSubStr[0] + URL_SCHEMA + getRequestURL(relativeUrl);
  return fileUrl;
}

class NiftiImageStudyPreparer extends VolumetricImageStudyPreparer {
  constructor() {
    super();
  }

  getDataProtocol() {
    return NIFTI_DATA_PROTOCOL;
  }

  createImageId(url) {
    return new ImageId.fromURL(url);
  }

  getNumberOfSpatialSlices(header, sliceDimensionIndex) {
    return header.voxelLength[sliceDimensionIndex];
  }

  _getSOPInstanceUIDFromUrl(url) {
    if (url.indexOf(URL_SCHEMA) >= 0) {
      const urlWithoutDataProtocol = url.substr(URL_SCHEMA.length);
      // Return the sop instance UID by removing the schema and the configured api root if exist
      if (apiRoot.length > 0 && urlWithoutDataProtocol.includes(apiRoot)) {
        const splitParams = urlWithoutDataProtocol.split(apiRoot);
        return splitParams[0] + splitParams[1];
      }
      return urlWithoutDataProtocol;
    } else {
      return url;
    }
  }

  /**
   * createSpatialInstances - For each spatial series (x, y, z) in the study,
   * creates the instances for each spatial slice.
   *
   * @param  {Object} study               The NIfTI study whose series will
   * receive instances for each spatial slice.
   * @param  {Object} header              The file header.
   * @param  {Boolean} supportsRangeRead  Whether the server hosting the NIfTI
   * file supports range read requests (to fetch just the initial bytes).
   * @param  {Object} metadata  Metadata for given nifti study.
   * @param  {boolean} [isSeg=false] It defines if series is segmentation or not
   * @param  {string} [baseURL=false] external baseURL to be used
   * @param  {string} [sourceBaseURL=false] external sourceBaseURL to be used in case isSeg true
   * @return {Promise}                    A promise that is fulfilled when
   * all the spatial slices for all series in the study have been created.
   */
  createSpatialInstances(
    study,
    header,
    supportsRangeRead,
    metadata = {},
    isSeg = false,
    baseURL,
    sourceBaseURL = ''
  ) {
    const spatialInstancesCreationPromise = new Promise(
      async (resolve, reject) => {
        const loadSliceMetaInfoTasks = [];

        const fileUrl = await this.getFileUrl(study, header, baseURL);
        let sourceFileUrl = '';
        if (
          study.series.find(
            series => sourceBaseURL.indexOf(series.dataProtocol) >= 0
          )
        ) {
          const sourceHeader = await this.loadHeader(
            sourceBaseURL,
            supportsRangeRead,
            true
          );
          sourceFileUrl = await this.getFileUrl(
            study,
            sourceHeader,
            sourceBaseURL,
            true
          );
        }
        study.series.forEach(series => {
          if (series.instances.length > 0) {
            return;
          }
          const sliceDimensionIndex = 'xyz'.indexOf(series.dimension);
          const { SeriesInstanceUID, SeriesDescription, SeriesNumber } = series;
          const numberOfSpatialSlices = this.getNumberOfSpatialSlices(
            header,
            sliceDimensionIndex
          );

          // creates one instance in the series for each spatial slice
          for (
            let sliceIndex = 0;
            sliceIndex < numberOfSpatialSlices;
            sliceIndex++
          ) {
            const imageIdFormat = this.createImageId(fileUrl);
            let sourceImageIdFormat = {};
            imageIdFormat.slice.dimension = series.dimension;
            imageIdFormat.slice.index = sliceIndex;
            let sourceImageId = '';

            if (sourceBaseURL) {
              if (sourceFileUrl) {
                sourceImageIdFormat = this.createImageId(sourceFileUrl);
                sourceImageIdFormat.slice.dimension = series.dimension;
                sourceImageIdFormat.slice.index = sliceIndex;
                sourceImageId = sourceImageIdFormat.url;
              } else {
                const originalSeriesUID = this.getOriginalSeriesUID(
                  series.SeriesInstanceUID
                );
                const originalSeries = !!study.seriesMap
                  ? study.seriesMap[originalSeriesUID]
                  : null;

                const isSourceMultiFrame =
                  originalSeries?.instances?.[0]?.metadata?.NumberOfFrames > 1;
                if (isSourceMultiFrame) {
                  sourceImageId =
                    originalSeries.instances[0].metadata.SOPInstanceUID;
                } else {
                  sourceImageId =
                    originalSeries?.instances?.length > sliceIndex
                      ? originalSeries.instances[sliceIndex].metadata
                          .SOPInstanceUID
                      : '';
                }
              }
            }

            // query the slice metadata by using the nifti image loader to get
            // info like the number of rows/columns, pixel spacing etc.
            const loadSliceMetaInfoPromise = this.loadHeader(
              imageIdFormat.url,
              supportsRangeRead
            );

            loadSliceMetaInfoTasks.push(loadSliceMetaInfoPromise);
            loadSliceMetaInfoPromise.then(metaInfo => {
              const sopClassUID = isSeg
                ? SOP_CLASS_UIDS.SEG
                : metadata.sopClassUID || SOP_CLASS_UIDS.MR;
              const instance = {
                url: getFileRequestURL(imageIdFormat.url),
                Columns: metaInfo.columns,
                frame: sliceIndex,
                Rows: metaInfo.rows,
                SOPClassUID: sopClassUID,
                frameOfReferenceUID: metaInfo.frameOfReferenceUID,
                ImagePositionPatient: metaInfo.imagePositionPatient,
                ImageOrientationPatient: metaInfo.imageOrientationPatient,
                PixelSpacing: [
                  metaInfo.rowPixelSpacing,
                  metaInfo.columnPixelSpacing,
                ],
                timeSlice: 0,
                Modality: isSeg ? 'SEG' : 'MR', // MR is dicom tag related for fMRI
                StudyInstanceUID: study.StudyInstanceUID,
              };

              this._decorateSOPInstance(
                instance,
                SeriesInstanceUID,
                SeriesDescription,
                SeriesNumber,
                sourceImageId
              );

              series.instances.push(instance);
            }, reject);
          }
        });

        // only resolves the promise returned by this function when all
        // spatial slices have been created
        Promise.all(loadSliceMetaInfoTasks)
          .then(resolve)
          .catch(reject);
      }
    );

    return spatialInstancesCreationPromise;
  }

  /**
   * createTemporalSeries - For a study of a 4D NIftI files (with temporal
   * volumes), this function creates one series containing a 3D volume
   * for each time point.
   *
   * @param  {Object} study The study.
   */
  createTemporalSeries(study) {
    if (study.timeSlices > 1) {
      const spatialSeriesCopy = [];

      // change the existing series uid and description to include time point information
      study.series.forEach(series => {
        if (series.dataProtocol !== NIFTI_DATA_PROTOCOL) {
          return;
        }
        spatialSeriesCopy.push(_.cloneDeep(series));
        series.SeriesInstanceUID += '-t0';
        series.SeriesDescription += ` (time 1/${study.timeSlices})`;

        const SeriesInstanceUID = series.SeriesInstanceUID;
        const SeriesDescription = series.SeriesDescription;
        const SeriesNumber = series.SeriesNumber;

        series.instances.forEach(instance => {
          this._decorateSOPInstance(
            instance,
            SeriesInstanceUID,
            SeriesDescription,
            SeriesNumber
          );
        });
      });

      for (let timePoint = 1; timePoint < study.timeSlices; timePoint++) {
        // repeat the spatial series for this timePoint
        const spatialSeriesForTimePoint = _.cloneDeep(spatialSeriesCopy);

        // change the image ids on these new series to point to this time point
        spatialSeriesForTimePoint.forEach(series => {
          series.SeriesInstanceUID += `-t${timePoint}`;
          series.SeriesDescription += ` (time ${timePoint + 1}/${
            study.timeSlices
          })`;

          const SeriesInstanceUID = series.SeriesInstanceUID;
          const SeriesDescription = series.SeriesDescription;
          const SeriesNumber = series.SeriesNumber;
          series.instances.forEach(instance => {
            const imageIdParts = ImageId.fromURL(instance.url);

            imageIdParts.timePoint = timePoint;
            instance.url = imageIdParts.url;
            instance.timeSlice = timePoint;
            this._decorateSOPInstance(
              instance,
              SeriesInstanceUID,
              SeriesDescription,
              SeriesNumber
            );
          });
          series.temporalSeries = true;
        });

        study.series.push(...spatialSeriesForTimePoint);
      }
    }
  }

  loadHeader(url, supportsRangeRead, isMetaHeader = false) {
    return nifti.loadHeader(getFileRequestURL(url), supportsRangeRead);
  }
}

export default NiftiImageStudyPreparer;
