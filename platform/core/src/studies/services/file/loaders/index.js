import WebImageDataLoaderSync from './webImage/retrieveDataLoader';
import NiftiDataLoaderSync from './volumetricImage/nifti/retrieveDataLoader';
import MhdDataLoaderSync from './volumetricImage/metaImage/retrieveDataLoaderMHD';
import TiffImageDataLoaderSync from './tiffImage/retrieveDataLoader';
import ZipImageDataLoaderSync from './zipImage/retrieveDataLoader';

const loaders = [
  NiftiDataLoaderSync,
  WebImageDataLoaderSync,
  MhdDataLoaderSync,
  TiffImageDataLoaderSync,
  ZipImageDataLoaderSync
];

export default loaders;
