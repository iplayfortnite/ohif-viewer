export default class FileDataLoaderSync {
  constructor(server, containerId, fileName) {
    this.server = server;
    this.containerId = containerId;
    this.fileName = fileName;
  }

  getOptions() {
    const { fileName } = this;

    const options = {
      fileName,
    };

    return options;
  }

  async execLoad() {
    await this.configLoad();
    const preLoadData = await this.preLoad();
    let metadata;
    try {
      const responseData = await this.load(preLoadData);
      metadata =
        typeof responseData === 'object'
          ? responseData
          : await responseData.json();
    } catch (e) {}
    const postLoadData = await this.postLoad(metadata);

    return [postLoadData];
  }

  /**
   * It iterates over given loaders running each one. Loaders parameters must be bind when getting it.
   * @param {Array} loaders - array of loader to retrieve data.
   */
  async runLoaders(loaders) {
    let result;
    for (const loader of loaders) {
      try {
        result = await loader();
        if (result && result.length) {
          break; // closes iterator in case data is retrieved successfully
        }
      } catch (e) {}
    }

    if (loaders.next().done && !result) {
      throw 'cant find data';
    }

    return result;
  }

  /**
   * @returns {Array} Array of loaders. To be consumed as queue
   */
  *getLoaders() {
    const { fileName, client } = this;
    yield client.retrieveMetadata.bind(client, fileName);
  }

  async load(preLoadData) {
    const loaders = this.getLoaders();
    const result = this.runLoaders(loaders);
    return result;
  }

  async postLoad(loadData) {
    const { server, fileName } = this;
    const study = this.createStudy(server, fileName, loadData);
    const dataProtocol = this.DATA_PROTOCOL;
    const studyLoadPromise = this.addInstancesToStudy(
      study,
      this.DATA_PROTOCOL,
      loadData,
      undefined,
      dataProtocol
    );
    studyLoadPromise.catch(error =>{
      throw error;
    });
    await studyLoadPromise;
    return study;
  }

  // Properties to be overwrite
  static extensionRegExp = '';
  DATA_PROTOCOL = '';

  // Methods to be overwrite
  async configLoad() {}
  async preLoad() {
    return;
  }
  async createStudy() {}
  async addInstancesToStudy() {}
  async loadDerivedSeries(
    server,
    study,
    seriesUid,
    segments,
    metadata = {},
    derivedIndex = -1
  ) {}
}
