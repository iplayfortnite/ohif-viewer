import _addInstancesToStudy from './addInstancesToStudy';
import FileDataLoaderSync from '../FileDataLoaderSync';
import {
  HTTP as FlywheelCommonHTTP,
} from '@flywheel/extension-flywheel-common';

const { getRequestURL } = FlywheelCommonHTTP.utils.REQUEST_UTILS;

export default class WebImageDataLoaderSync extends FileDataLoaderSync {
  static extensionRegExp = /\.(png|jpe?g)$/;

  constructor(server, containerId, fileName) {
    super(server, containerId, fileName);
    this.DATA_PROTOCOL = window.location.protocol.slice(0, -1);
  }
  configLoad() {
    const client = {
      retrieveMetadata: fileName => {
        // bypass metadata
        // There is no study metadata from io-proxy yet.
        return {};
      },
    };

    this.client = client;
  }

  _getSOPInstanceUIDFromUrl(url = '') {
    return url.replace(/^https?:\/\//, '');
  }

  async addInstancesToStudy(study, dataProtocol, metadata) {
    return _addInstancesToStudy(
      study,
      dataProtocol,
      this._getSOPInstanceUIDFromUrl
    );
  }

  createStudy(server, fileName) {
    const baseFileUrl = getRequestURL(`${server.baseURL}/${fileName}`);
    return {
      dataProtocol: this.DATA_PROTOCOL,
      StudyInstanceUID: fileName,
      series: [],
      seriesMap: Object.create(null),
      seriesLoader: null,
      baseURL: `${this.DATA_PROTOCOL}://${window.location.host}${baseFileUrl}`,
    };
  }
}
