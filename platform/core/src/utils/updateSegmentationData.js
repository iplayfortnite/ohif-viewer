import studyMetadataManager from './studyMetadataManager';
import cornerstoneTools from 'cornerstone-tools';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
const { configuration } = cornerstoneTools.getModule('segmentation');
const { rearrangeSegmentationData } = FlywheelCommonRedux.actions;

/**
 * Get derived display set.
 * @param {Array} derivedDisplaySets
 * @param {number} segIndex segmentation index
 * @returns void
 */
const getDerivedDisplaySet = (derivedDisplaySets, displaySetInstanceUIDs) => {
  let displaySet = null;
  if (!derivedDisplaySets.length && derivedDisplaySets.length <= 0) {
    return displaySet;
  }
  derivedDisplaySets.every(segDisplaySet => {
    if (
      !segDisplaySet.isDeleted &&
      displaySetInstanceUIDs?.includes(segDisplaySet.displaySetInstanceUID)
    ) {
      displaySet = segDisplaySet;
      return false;
    }
    return true;
  });
  return displaySet;
};

/**
 * Get derived display set collection.
 * @param {Object} studyMetadata
 * @param {string} SeriesInstanceUID
 * @param {string} modality - segmentation overlay derived series modality
 * @returns void
 */
const getDerivedDisplaySetArrayByReferencedSeries = (
  studyMetadata,
  SeriesInstanceUID,
  modality
) => {
  const derivedDisplaySets = studyMetadata.getDerivedDatasets({
    referencedSeriesInstanceUID: SeriesInstanceUID,
    Modality: modality,
  });
  return derivedDisplaySets;
};

/**
 * Get derived display set by segment index.
 * @param {Object} studyMetadata
 * @param {string} SeriesInstanceUID
 * @param {number} segIndex segment index
 * @param {string} modality - segmentation overlay derived series modality
 * @returns void
 */
const getDerivedDisplaySetByReferencedSeries = (
  studyMetadata,
  SeriesInstanceUID,
  segIndex,
  modality
) => {
  const derivedDisplaySets = getDerivedDisplaySetArrayByReferencedSeries(
    studyMetadata,
    SeriesInstanceUID,
    modality
  );
  return getDerivedDisplaySet(derivedDisplaySets, segIndex);
};

/**
 * Find the removed item id from from the segment collection.
 * @param {Array} previousSegmentData - previous segment data collection
 * @param {Array} newSegmentData - new segment data collection
 * @returns number - item id
 */
const findRemovedItem = (previousSegmentData, newSegmentData) => {
  let removedItem = {};
  previousSegmentData.every(prevSegData => {
    let isRemoved = true;
    newSegmentData.every(segData => {
      if (segData.fileId === prevSegData.fileId) {
        isRemoved = false;
        return false;
      }
      return true;
    });
    if (isRemoved) {
      removedItem = prevSegData;
      return false;
    }
    return true;
  });
  return removedItem;
};

/**
 * Find the order changed segment id from the segment collection.
 * @param {Array} previousSegmentData - previous segment data collection
 * @param {Array} newSegmentData - new segment data collection
 * @returns number - item id
 */
const findOrderChangeItem = (previousSegmentData, newSegmentData) => {
  let prevOrderData = {};
  let newOrderData = {};
  let itemId = 0;
  previousSegmentData.forEach((segData, index) => {
    prevOrderData[segData.originalId] = {};
    prevOrderData[segData.originalId].prev =
      index === 0 ? -1 : previousSegmentData[index - 1].originalId;
    prevOrderData[segData.originalId].next =
      index >= previousSegmentData.length - 1
        ? -1
        : previousSegmentData[index + 1].originalId;
  });
  newSegmentData.forEach((segData, index) => {
    newOrderData[segData.originalId] = {};
    newOrderData[segData.originalId].prev =
      index === 0 ? -1 : newSegmentData[index - 1].originalId;
    newOrderData[segData.originalId].next =
      index >= newSegmentData.length - 1
        ? -1
        : newSegmentData[index + 1].originalId;
  });
  for (const key of Object.keys(newOrderData)) {
    const newData = newOrderData[key];
    const prevData = prevOrderData[key];
    if (newData.prev !== prevData.prev && newData.next !== prevData.next) {
      itemId = parseInt(key);
      break;
    }
  }
  return itemId;
};

/**
 * Remove all segments of the study.
 * @param {string} StudyInstanceUID
 * @param {Study[]} studies Collection of studies
 * @returns void
 */
const removeAllSegments = (StudyInstanceUID, studies) => {
  const studyMetadata = studyMetadataManager.get(StudyInstanceUID);

  if (!studyMetadata) {
    return;
  }
  const displaySets = studyMetadata.getDisplaySets();
  displaySets.forEach(displaySet => {
    if (displaySet.isDerived) {
      return;
    }
    const derivedDisplaySets = studyMetadata.getDerivedDatasets({});
    derivedDisplaySets.forEach(segDisplayset => {
      if (segDisplayset.segmentationIndex > 0 && !segDisplayset.isDeleted) {
        segDisplayset.removeSegment(
          displaySet,
          studies,
          segDisplayset.segmentationIndex
        );
        segDisplayset.isDeleted = true;
      }
    });
  });
};

/**
 * Update order changed segments or atrributes of segment or remove segment from the segment collection.
 * @param {string} StudyInstanceUID
 * @param {Study[]} studies Collection of studies
 * @param {string} referenceSeriesInstanceUID - reference series UID
 * @param {Array} previousSegmentData - previous segment data collection
 * @param {Array} newSegmentData - new segment data collection
 * @param {string} modality - segmentation overlay derived series modality
 * @param {boolean} isRemoved - update the item removal or not
 * @returns {string[]} - collection of removed series instance UID's  of removed segments
 */
const updateSegmentDisplayData = (
  StudyInstanceUID,
  studies,
  referenceUID,
  previousSegmentData,
  newSegmentData,
  modality,
  isRemoved = false
) => {
  const removedItem = findRemovedItem(previousSegmentData, newSegmentData);
  const removedIndex = isRemoved ? removedItem.originalId - 1 : -1;
  const changedSegId = isRemoved
    ? 0
    : findOrderChangeItem(previousSegmentData, newSegmentData);

  const promises = [];

  const removedSeriesInstanceUIDs = [];
  const studyMetadata = studyMetadataManager.get(StudyInstanceUID);

  if (!studyMetadata) {
    return promises;
  }
  const displaySets = studyMetadata.getDisplaySets();
  displaySets.forEach(displaySet => {
    const { SeriesInstanceUID } = displaySet;
    // For DICOM study, the reference UID will be active viewport series instance UID
    // and for Nifti study, the reference UID will be loaded study instance UID
    if (
      referenceUID !== SeriesInstanceUID &&
      referenceUID !== StudyInstanceUID
    ) {
      return;
    }
    const derivedDisplaySets = getDerivedDisplaySetArrayByReferencedSeries(
      studyMetadata,
      SeriesInstanceUID,
      modality
    );
    if (isRemoved && removedIndex >= 0) {
      const segDisplayset = getDerivedDisplaySet(
        derivedDisplaySets,
        removedItem?.displaySetInstanceUIDs
      );
      if (segDisplayset === null) {
        return promises;
      }
      segDisplayset.removeSegment(displaySet, studies, removedIndex + 1);
      removedSeriesInstanceUIDs.push(segDisplayset.SeriesInstanceUID);
      segDisplayset.isDeleted = true;
    }
    const isPropertyUpdate =
      changedSegId > 0 || (isRemoved && removedIndex >= 0) ? false : true;
    updateOpacity(displaySet, newSegmentData, modality);
    // Always skip 0th index as that label map preserved for manual drawn segments.
    let labelMapIndex = newSegmentData.length;
    newSegmentData.every((segData, index) => {
      const segDisplayset = getDerivedDisplaySet(
        derivedDisplaySets,
        segData?.displaySetInstanceUIDs
      );
      if (segDisplayset !== null) {
        if (!isPropertyUpdate) {
          if (segData.originalId === changedSegId) {
            segDisplayset.rePositionSegment(
              displaySet,
              studies,
              segData.modality === 'RTSTRUCT' ? index : labelMapIndex
            );
          }
          segDisplayset.labelmapIndex = labelMapIndex;
          labelMapIndex--;
          if (segData.modality === 'RTSTRUCT') {
            segDisplayset.labelmapIndex = index;
          }
        } else {
          segDisplayset.setSegmentVisibility(segData.isVisible);
        }
      }
      return true;
    });
  });
  return removedSeriesInstanceUIDs;
};

/**
 * Update segmentation opacity configuration.
 * @param {Object} referenceDisplaySet -reference displayset
 * @param {Array} segmentList - segmentation data collection
 * @param {string} modality - segmentation modality
 * @returns void
 */
const updateOpacity = (referenceDisplaySet, segmentList, modality) => {
  configuration.renderOutline = false;
  if (modality === 'RTSTRUCT') {
    const module = cornerstoneTools.getModule('rtstruct');
    segmentList.forEach(segData => {
      const fillAlpha = parseFloat(segData.opacity) / 100;
      if (
        !module.configuration.opacity ||
        !isNaN(module.configuration.opacity)
      ) {
        module.configuration.opacity = {};
      }
      module.configuration.opacity[segData.seriesInstanceUID] = fillAlpha;
    });
  } else {
    const imageId = referenceDisplaySet.images.map(image =>
      image.getImageId()
    )[0];
    if (configuration.fillAlphaPerLabelMap[imageId]?.length > 0) {
      configuration.fillAlphaPerLabelMap[imageId].splice(1);
    } else {
      configuration.fillAlphaPerLabelMap[imageId] = [];
    }
    segmentList
      .slice()
      .reverse()
      .forEach((segData, index) => {
        const fillAlpha = parseFloat(segData.opacity) / 100;
        configuration.fillAlphaPerLabelMap[imageId][index + 1] = fillAlpha;
      });
  }
};

/**
 * Reset the reference UID if there is mismatch in ReferencedSeriesSequence .
 * @param {Array} newSegmentData - segmentation data collection
 * @param {string} modality - segmentation modality
 * @returns void
 */
const resetReferenceUID = (newSegmentData, modality) => {
  newSegmentData.every(segData => {
    const studyMetadata = studyMetadataManager.get(segData.studyInstanceUID);
    const derivedDisplaySets = studyMetadata.getDerivedDatasets({
      Modality: modality,
    });
    const segDisplayset = getDerivedDisplaySet(
      derivedDisplaySets,
      segData?.displaySetInstanceUIDs
    );
    if (
      segDisplayset.metadata.ReferencedSeriesSequence[0].SeriesInstanceUID !==
      segData.referenceUID
    ) {
      window.store.dispatch(
        rearrangeSegmentationData(
          segData,
          segData.referenceUID,
          segDisplayset.metadata.ReferencedSeriesSequence[0].SeriesInstanceUID
        )
      );
    }
  });
};

export { updateSegmentDisplayData, removeAllSegments, resetReferenceUID };
