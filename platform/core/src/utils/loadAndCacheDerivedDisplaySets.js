import studyMetadataManager from './studyMetadataManager';

/**
 * Study schema
 *
 * @typedef {Object} Study
 * @property {Array} series -
 * @property {Object} seriesMap -
 * @property {Object} seriesLoader -
 * @property {string} wadoUriRoot -
 * @property {string} wadoRoot -
 * @property {string} qidoRoot -
 * @property {string} PatientName -
 * @property {string} PatientID -
 * @property {number} PatientAge -
 * @property {number} PatientSize -
 * @property {number} PatientWeight -
 * @property {string} AccessionNumber -
 * @property {string} StudyDate -
 * @property {string} StudyTime -
 * @property {string} modalities -
 * @property {string} StudyDescription -
 * @property {string} imageCount -
 * @property {string} StudyInstanceUID -
 * @property {string} institutionName -
 * @property {Array} displaySets -
 */

/**
 * Factory function to load and cache derived display sets.
 *
 * @param {object} referencedDisplaySet Display set
 * @param {string} referencedDisplaySet.displaySetInstanceUID Display set instance uid
 * @param {string} referencedDisplaySet.SeriesDate
 * @param {string} referencedDisplaySet.SeriesTime
 * @param {string} referencedDisplaySet.SeriesInstanceUID
 * @param {string} referencedDisplaySet.SeriesNumber
 * @param {string} referencedDisplaySet.seriesDescription
 * @param {number} referencedDisplaySet.numImageFrames
 * @param {string} referencedDisplaySet.frameRate
 * @param {string} referencedDisplaySet.Modality
 * @param {boolean} referencedDisplaySet.isMultiFrame
 * @param {number} referencedDisplaySet.InstanceNumber
 * @param {boolean} referencedDisplaySet.isReconstructable
 * @param {string} referencedDisplaySet.StudyInstanceUID
 * @param {Array} referencedDisplaySet.sopClassUIDs
 * @param {Study[]} studies Collection of studies
 * @returns void
 */
const loadAndCacheDerivedDisplaySets = (referencedDisplaySet, studies) => {
  const { StudyInstanceUID, SeriesInstanceUID } = referencedDisplaySet;

  const promises = [];

  const studyMetadata = studyMetadataManager.get(StudyInstanceUID);

  if (!studyMetadata) {
    return promises;
  }

  const derivedDisplaySets = studyMetadata.getDerivedDatasets({
    referencedSeriesInstanceUID: SeriesInstanceUID,
  });

  if (!derivedDisplaySets.length) {
    return promises;
  }

  // Filter by type
  const displaySetsPerModality = {};

  derivedDisplaySets.forEach(displaySet => {
    const Modality = displaySet.Modality;

    if (displaySetsPerModality[Modality] === undefined) {
      displaySetsPerModality[Modality] = [];
    }

    displaySetsPerModality[Modality].push(displaySet);
  });

  // For each type, see if any are loaded, if not load the most recent.
  Object.keys(displaySetsPerModality).forEach(key => {
    const displaySets = displaySetsPerModality[key];
    // TODO - Needs to remove the special handling for SEG
    let displaySetIndex = 0;
    if ((key === 'SEG') || (key === 'RTSTRUCT')) {
      // Check if any of the seg displaysets pending to load, then continue to load that
      const isPendingLoaded = displaySets.some(
        displaySet => !displaySet.isLoaded
      );

      if (!isPendingLoaded) {
        return;
      }
      displaySetIndex = displaySets.length - 1;
    } else {
      const isLoaded = displaySets.some(displaySet => displaySet.isLoaded);

      if (isLoaded) {
        return;
      }
    }

    if (displaySets.some(displaySet => displaySet.loadError)) {
      return;
    }

    // find most recent and load it.
    let recentDateTime = 0;
    let recentDisplaySet = displaySets[displaySetIndex];

    displaySets.forEach((displaySet, index) => {
      const dateTime = Number(
        `${displaySet.SeriesDate}${displaySet.SeriesTime}`
      );
      if (dateTime > recentDateTime) {
        recentDateTime = dateTime;
        recentDisplaySet = displaySet;
        displaySetIndex = index;
      }
      if (key === 'SEG' && displaySet.isLoaded) {
        // Shifting all the loaded segmentation label map index to 1 position up for adjusting the rendering order
        displaySet.labelmapIndex += 1;
      }
    });

    recentDisplaySet.isLoading = true;
    let segIndex = getSegmentationIndexForAddedDisplaySet(recentDisplaySet);
    segIndex = (segIndex === -1) ? displaySetIndex + 1 : segIndex; // Segmentation index to set for newly loading segmentation

    promises.push(
      recentDisplaySet.load(referencedDisplaySet, studies, segIndex)
    );
  });

  return promises;
};

/**
 * Function to load and cache derived display sets which are pending to load.
 *
 * @param {Study[]} studies Collection of studies
 * @param {string} modality
 * @returns void
 */
const loadPendingDerivedDisplaySets = (studies, modality) => {

  const promises = [];

  studies.forEach(study => {
    const studyInstanceUID = study.studyInstanceUID || study.StudyInstanceUID;
    const studyMetadata = studyMetadataManager.get(studyInstanceUID);

    if (!studyMetadata) {
      return;
    }

    const derivedDisplaySets = studyMetadata.getDerivedDatasets({
      Modality: modality,
    });

    if (!derivedDisplaySets.length) {
      return;
    }

    // Filter by type
    const isPendingLoaded = derivedDisplaySets.some(
      displaySet => !displaySet.isLoaded
    );

    if (!isPendingLoaded) {
      return;
    }
    derivedDisplaySets.forEach((displaySet, index) => {
      if (displaySet.isLoaded) {
        if (displaySet.Modality === 'SEG') {
          // Shifting all the loaded segmentation label map index to 1 position up for adjusting the rendering order
          displaySet.labelmapIndex += 1;
        }
        return;
      }
      displaySet.isLoading = true;
      let segIndex = getSegmentationIndexForAddedDisplaySet(displaySet);
      segIndex = (segIndex === -1) ? index + 1 : segIndex; // Segmentation index to set for newly loading segmentation

      promises.push(
        displaySet.load(displaySet, studies, segIndex)
      );
    });
  });

  return promises;
};

const getSegmentationIndexForAddedDisplaySet = (displaySet) => {
  let segIndex = -1;
  const derivedSuffix = '-derived'; // Derived segmentation series suffix for Nifti overlay
  if (displaySet.SeriesInstanceUID.indexOf(derivedSuffix) >= 0) {
    segIndex = parseInt(
      displaySet.SeriesInstanceUID.split(derivedSuffix)[1]
    );
  } else if (displaySet.Modality === "RTSTRUCT") {
    const segmentationData = window.store.getState()?.segmentation?.segmentationData;
    Object.keys(segmentationData).forEach(key => {
      const segments = segmentationData[key];
      if (!!segments && segments.length > 0) {
        segments.forEach(segData => {
          if (segData.modality === displaySet.Modality) {
            segIndex = segData.originalId > segIndex ? segData.originalId : segIndex;
          }
        });
      }
    });
  }
  return segIndex;
}

export { loadAndCacheDerivedDisplaySets, loadPendingDerivedDisplaySets };
