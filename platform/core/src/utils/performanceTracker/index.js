import logPerformance from './logPerformance';
import subscribeCornerstoneEvents from './subscribeCornerstoneEvents';
import setPerformance from './setPerformance';

const enablePerformanceTracker = evaluatePerformance => {
  if (evaluatePerformance) subscribeCornerstoneEvents();
};

const tracker = {
  logPerformance,
  enablePerformanceTracker,
  setPerformance,
};

export default tracker;
