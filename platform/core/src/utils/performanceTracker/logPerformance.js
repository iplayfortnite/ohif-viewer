import store from '@ohif/viewer/src/store';

export default function logPerformance() {
  const performanceTracker = store.getState().performanceTracker
    .performanceTracker;
  const performanceLogger = [];
  for (const [trackerKey, performanceValue] of Object.entries(
    performanceTracker
  )) {
    let time = 0;
    performanceValue.map(({ startTime, endTime }) => {
      time += Number(endTime - startTime);
    });
    const avgTime = time / performanceValue.length;
    let performanceLog = {
      'Feature Name': trackerKey,
      Count: performanceValue.length,
      'Avg. Time': `${avgTime.toFixed(3)} ms`,
    };
    performanceLogger.push(performanceLog);
  }
  return performanceLogger;
}
