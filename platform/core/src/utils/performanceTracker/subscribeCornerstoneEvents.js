import { redux } from '@ohif/core';
import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import setPerformance from './setPerformance';

const { performanceStart, performanceEnd, performanceRemove } = setPerformance;
const { IMAGE_SCROLL } = redux.performanceTrackerKeys;
const eventListeners = {};

export default function subscribeCornerstoneEvents() {
  // subscribe scroll for already enabled elements
  const enabledElements = cornerstone.getEnabledElements();
  enabledElements.forEach(enabledElement => {
    subscribeScrollEvents(enabledElement.element);
  });

  // subscribe for future elements
  cornerstone.events.addEventListener(
    cornerstone.EVENTS.ELEMENT_ENABLED,
    event => {
      const element = event.detail.element;
      if (element) subscribeScrollEvents(element);
    }
  );

  // unsubscribe elements when disabled
  cornerstone.events.addEventListener(
    cornerstone.EVENTS.ELEMENT_DISABLED,
    event => {
      const element = event.detail.element;
      if (element) {
        element.removeEventListener(
          cornerstoneTools.EVENTS.STACK_SCROLL,
          eventListeners[element].removeScrollHandler
        );
        element.removeEventListener(
          cornerstone.EVENTS.IMAGE_RENDERED,
          eventListeners[element].removeImageHandler
        );
      }
    }
  );

  const getImageIdByImageIdIndex = (imageIdIndex, element) => {
    const toolState = cornerstoneTools.getToolState(element, 'stack');
    const stackData = toolState.data[0];
    const newImageId = stackData.imageIds[imageIdIndex];
    const splitImageId = newImageId.split('series/');
    const imageId = splitImageId[1];
    return imageId;
  };

  function subscribeScrollEvents(element) {
    eventListeners[element] = {
      scrollHandler: scrollEvent => {
        const imageId = getImageIdByImageIdIndex(
          scrollEvent.detail.newImageIdIndex,
          element
        );
        const startEntry = {
          key: IMAGE_SCROLL,
          id: imageId,
          startTime: Date.now(),
        };
        performanceStart(startEntry);
      },
      imageHandler: renderedEvent => {
        const splitImageId = renderedEvent.detail.image.imageId.split(
          'series/'
        );
        const imageId = splitImageId[1];
        const endEntry = {
          key: IMAGE_SCROLL,
          id: imageId,
          endTime: Date.now(),
        };
        performanceEnd(endEntry);
      },
      removeScrollHandler: scrollEvent => {
        const imageId = getImageIdByImageIdIndex(
          scrollEvent.detail.newImageIdIndex,
          element
        );
        const removeEntry = {
          key: IMAGE_SCROLL,
          id: imageId,
        };
        performanceRemove(removeEntry);
      },
      removeImageHandler: renderedEvent => {
        const splitImageId = renderedEvent.detail.image.imageId.split(
          'series/'
        );
        const imageId = splitImageId[1];
        const removeEntry = {
          key: IMAGE_SCROLL,
          id: imageId,
        };
        performanceRemove(removeEntry);
      },
    };
    element.addEventListener(
      cornerstoneTools.EVENTS.STACK_SCROLL,
      eventListeners[element].scrollHandler
    );
    element.addEventListener(
      cornerstone.EVENTS.IMAGE_RENDERED,
      eventListeners[element].imageHandler
    );
  }
}
