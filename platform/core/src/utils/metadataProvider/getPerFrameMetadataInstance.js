// "instance" corresponding to the SOP Instance and "frameIndex" is the "0" based index of the frame
export default function getPerFrameMetadataInstance(instance, frameIndex) {
  const {
    PerFrameFunctionalGroupsSequence,
    SharedFunctionalGroupsSequence,
  } = instance;
  let metaDataInstance = instance;
  if (
    (PerFrameFunctionalGroupsSequence || SharedFunctionalGroupsSequence) &&
    isMultiFrame(instance)
  ) {
    // Get the per frame plane and pixel attributes and combine it with the sop instance data
    const frameData = getFrameDataFromSOPInstance(instance, frameIndex);
    if (Object.keys(frameData).length) {
      metaDataInstance = { ...instance, ...frameData };
    }
  }
  return metaDataInstance;
}

const isMultiFrame = instance => {
  return instance?.NumberOfFrames > 1;
};

function getFrameDataFromSOPInstance(instance, frameIndex) {
  const SOPClassUIDs = [
    '1.2.840.10008.5.1.4.1.1.77.1.5.4', // OphthalmicTomographyImageStorage
    '1.2.840.10008.5.1.4.1.1.13.1.3', // Breast Tomosynthesis image
    '1.2.840.10008.5.1.4.1.1.6.2', // US
    '1.2.840.10008.5.1.4.1.1.4.1', // Enhanced MR Image Storage
    '1.2.840.10008.5.1.4.1.1.4.2', // MR Spectroscopy Storage
    '1.2.840.10008.5.1.4.1.1.4.3', // Enhanced MR Color Image Storage
    '1.2.840.10008.5.1.4.1.1.2.1', // Enhanced CT Image Storage
  ];

  const frameData = {};

  if (SOPClassUIDs.includes(instance.SOPClassUID)) {
    const {
      PerFrameFunctionalGroupsSequence,
      SharedFunctionalGroupsSequence,
    } = instance;

    frameData.Rows = instance.Rows;
    frameData.Columns = instance.Columns;

    // Parse the per frame functional entry corresponding to the frame entry(frameIndex) and fill the
    // plane position attributes
    if (PerFrameFunctionalGroupsSequence) {
      const frame =
        Array.isArray(PerFrameFunctionalGroupsSequence) &&
        PerFrameFunctionalGroupsSequence.length > frameIndex
          ? PerFrameFunctionalGroupsSequence[frameIndex]
          : PerFrameFunctionalGroupsSequence;
      const { PlanePositionSequence, PlaneOrientationSequence } = frame;
      frameData.ImagePositionPatient =
        PlanePositionSequence?.ImagePositionPatient || null;
      frameData.ImageOrientationPatient =
        PlaneOrientationSequence?.ImageOrientationPatient || null;
    }

    // Parse the shared functional sequence and fill the shared plane and pixel attributes
    if (SharedFunctionalGroupsSequence) {
      const {
        PlaneOrientationSequence,
        PixelMeasuresSequence,
      } = SharedFunctionalGroupsSequence;
      frameData.ImageOrientationPatient =
        frameData.ImageOrientationPatient ||
        PlaneOrientationSequence?.ImageOrientationPatient ||
        null;
      if (PixelMeasuresSequence) {
        frameData.PixelSpacing = PixelMeasuresSequence?.PixelSpacing || null;
        frameData.SliceThickness =
          PixelMeasuresSequence?.SliceThickness || null;
      }
    }
  }

  return frameData;
}
