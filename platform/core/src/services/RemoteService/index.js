import RemoteService from './RemoteService';

export default {
  name: 'RemoteService',
  create: ({ configuration = {} }) => {
    return new RemoteService();
  },
};
