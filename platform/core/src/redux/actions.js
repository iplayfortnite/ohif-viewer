/** Action Creators:
 *  https://redux.js.org/basics/actions#action-creators
 */

import {
  CLEAR_VIEWPORT,
  SET_ACTIVE_SPECIFIC_DATA,
  SET_SERVERS,
  SET_VIEWPORT,
  SET_ALL_VIEWPORTS,
  SET_VIEWPORT_ACTIVE,
  SET_VIEWPORT_LAYOUT,
  SET_VIEWPORT_LAYOUT_AND_DATA,
  SET_COLORMAP_ID,
  SET_USER_PREFERENCES,
} from './constants/ActionTypes.js';

/**
 * The definition of a viewport layout.
 *
 * @typedef {Object} ViewportLayout
 * @property {number} numRows -
 * @property {number} numColumns -
 * @property {array} viewports -
 */

/**
 * VIEWPORT
 */
export const setViewportSpecificData = (
  viewportIndex,
  viewportSpecificData
) => {
  return {
    type: SET_VIEWPORT,
    viewportIndex,
    viewportSpecificData,
  };
};

export const setAllViewportsSpecificData = viewportsSpecificData => {
  return {
    type: SET_ALL_VIEWPORTS,
    viewportsSpecificData,
  };
};

export const setViewportActive = viewportIndex => ({
  type: SET_VIEWPORT_ACTIVE,
  viewportIndex,
});

/**
 * @param {ViewportLayout} layout
 */
export const setLayout = ({ numRows, numColumns, viewports }) => ({
  type: SET_VIEWPORT_LAYOUT,
  numRows,
  numColumns,
  viewports,
});

/**
 * @param {string} colormapId
 */
export const setColormapId = ({ colormapId }) => ({
  type: SET_COLORMAP_ID,
  colormapId,
});

/**
 * @param {number} layout.numRows
 * @param {number} layout.numColumns
 * @param {array} viewports
 */
export const setViewportLayoutAndData = (
  { numRows, numColumns, viewports },
  viewportSpecificData
) => ({
  type: SET_VIEWPORT_LAYOUT_AND_DATA,
  numRows,
  numColumns,
  viewports,
  viewportSpecificData,
});

export const clearViewportSpecificData = viewportIndex => ({
  type: CLEAR_VIEWPORT,
  viewportIndex,
});

export const setActiveViewportSpecificData = viewportSpecificData => {
  return {
    type: SET_ACTIVE_SPECIFIC_DATA,
    viewportSpecificData,
  };
};

/**
 * NOT-VIEWPORT
 */
export const setStudyLoadingProgress = (progressId, progressData) => ({
  type: 'SET_STUDY_LOADING_PROGRESS',
  progressId,
  progressData,
});

export const clearStudyLoadingProgress = progressId => ({
  type: 'CLEAR_STUDY_LOADING_PROGRESS',
  progressId,
});

export const setUserPreferences = state => ({
  type: SET_USER_PREFERENCES,
  state,
});

export const setExtensionData = (extension, data) => ({
  type: 'SET_EXTENSION_DATA',
  extension,
  data,
});

export const setTimepoints = state => ({
  type: 'SET_TIMEPOINTS',
  state,
});

export const setMeasurements = state => ({
  type: 'SET_MEASUREMENTS',
  state,
});

export const setOrientation = (state, isSessionData = false) => ({
  type: 'SET_ORIENTATION',
  state,
  isSessionData,
});

export const setRotationStatus = state => ({
  type: 'SET_ROTATION_STATUS',
  state,
});

export const setMeasurementDeleted = (state) => ({
  type: 'SET_MEASUREMENT_DELETED',
  state
});

export const setStudyData = (StudyInstanceUID, data) => ({
  type: 'SET_STUDY_DATA',
  StudyInstanceUID,
  data,
});

export const setServers = servers => ({
  type: SET_SERVERS,
  servers,
});

export const addDeletedMeasurementId = (id, _id) => ({
  type: 'ADD_DELETED_MEASUREMENT_ID',
  id,
  _id,
});

export const addDeletedMeasurementIdForStudyForm = (id, _id, uuid, measurement) => ({
  type: 'ADD_DELETED_MEASUREMENT_ID_STUDY_FORM',
  id,
  _id,
  uuid,
  measurement,
});

export const removeDeletedMeasurementId = id => ({
  type: 'REMOVE_DELETED_MEASUREMENT_ID',
  id,
});

export const setSaveStatus = status => ({
  type: 'SET_SAVE_STATUS',
  status,
});

export const setPerformanceStartEntry = entry => ({
  type: 'START_ENTRY',
  ...entry,
});

export const setPerformanceEndEntry = entry => ({
  type: 'END_ENTRY',
  ...entry,
});

export const setPerformanceAddEntry = entry => ({
  type: 'ADD_ENTRY',
  ...entry,
});

export const setPerformanceRemoveEntry = entry => ({
  type: 'REMOVE_ENTRY',
  ...entry,
});

export const clearPerformanceEntry = () => ({
  type: 'CLEAR_LOG',
});

const actions = {
  /**
   * VIEWPORT
   */
  setViewportActive,
  setViewportSpecificData,
  setAllViewportsSpecificData,
  setViewportLayoutAndData,
  setLayout,
  setColormapId,
  clearViewportSpecificData,
  setActiveViewportSpecificData,
  setPerformanceStartEntry,
  setPerformanceEndEntry,
  setPerformanceAddEntry,
  setPerformanceRemoveEntry,
  clearPerformanceEntry,
  /**
   * NOT-VIEWPORT
   */
  setStudyLoadingProgress,
  clearStudyLoadingProgress,
  setUserPreferences,
  setExtensionData,
  setTimepoints,
  setMeasurements,
  setOrientation,
  setRotationStatus,
  setMeasurementDeleted,
  setStudyData,
  setServers,
  addDeletedMeasurementId,
  addDeletedMeasurementIdForStudyForm,
  removeDeletedMeasurementId,
  setSaveStatus,
};

export default actions;
