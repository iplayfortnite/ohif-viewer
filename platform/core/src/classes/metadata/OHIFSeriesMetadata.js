import { SeriesMetadata } from './SeriesMetadata';
import { OHIFInstanceMetadata } from './OHIFInstanceMetadata';

export class OHIFSeriesMetadata extends SeriesMetadata {
  /**
   * @param {Object} Series object.
   */
  constructor(data, study, uid) {
    super(data, uid);
    // Server base url with host and configured api root(if present) from the instance UID.
    // The host and apiRoot will be present for web images and tiff images.
    this.serverBaseURLWithHost = "";
    this.init(study);
  }

  init(study) {
    const series = this.getData();

    // define "_seriesInstanceUID" protected property...
    Object.defineProperty(this, '_seriesInstanceUID', {
      configurable: false,
      enumerable: false,
      writable: false,
      value: series.SeriesInstanceUID,
    });

    // define "_instanceMap" protected property for improving performance when having large number of images...
    Object.defineProperty(this, '_instanceMap', {
      configurable: false,
      enumerable: false,
      writable: false,
      value: {},
    });

    // populate internal list of instances...
    series.instances.forEach(instance => {
      this.addInstance(new OHIFInstanceMetadata(instance, series, study));
    });
  }

  /**
   * Append an instance to the current series. Also caches bu UID for faster lookup
   * @param {OHIFInstanceMetadata} instance The instance to be added to the current series.
   * @returns {boolean} Returns true on success, false otherwise.
   */
  addInstance(instance) {
    if (super.addInstance(instance)) {
      var instanceUID =
        instance.getSOPInstanceUID() ||
        instance.getTagValue('SOPInstanceUID', void 0);
      this._instanceMap[instanceUID] = instance;
      const host = window.location.host;
      const apiRoot = window.config.apiRoot || '';
      if (instanceUID.includes(window.location.host)) {
        // If the configured api root is present, then add that also to the server base url.
        if (apiRoot.length && instanceUID.includes(apiRoot)) {
          const splitUrls = instanceUID.split(apiRoot);
          this.serverBaseURLWithHost = splitUrls[0] + apiRoot;
        } else {
          this.serverBaseURLWithHost = host;
        }
      }
      return true;
    }
    return false;
  }

  /**
   * Find an instance by SOPInstanceUID.
   * @param {string} uid An UID string.
   * @returns {InstanceMetadata} Returns a InstanceMetadata instance when found or undefined otherwise.
   */
  getInstanceByUID(uid) {
    let instance = this._instanceMap[uid];
    // If server base url with host and/or configured api is present, then check the url sub string without the base url
    // is matching with the input uid, if yes, return that instance object.
    if (!instance && this.serverBaseURLWithHost?.length) {
      Object.keys(this._instanceMap).forEach(key => {
        if (key.includes(this.serverBaseURLWithHost)) {
          if (uid.indexOf(key.split(this.serverBaseURLWithHost)[1]) >= 0) {
            instance = this._instanceMap[key];
          }
        }
      });
    }
    return instance;
  }
}
