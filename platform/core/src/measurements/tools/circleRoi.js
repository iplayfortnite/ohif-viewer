import { ToolsUtils as FlywheelCommonToolsUtils } from '@flywheel/extension-flywheel-common';
import getImagePropertyForImagePath from '../lib/getImagePropertyForImagePath';
const displayFunction = data => {
  let meanValue = '';
  const { cachedStats } = data;
  if (cachedStats && cachedStats.mean && !isNaN(cachedStats.mean)) {
    const pixelSpacing = getImagePropertyForImagePath(
      data.imagePath,
      'PixelSpacing'
    );
    const modality = getImagePropertyForImagePath(data.imagePath, 'Modality');
    meanValue =
      cachedStats.mean.toFixed(2) +
      ' ' +
      FlywheelCommonToolsUtils.unitUtils.getUnitByModality(
        modality,
        pixelSpacing
      );
  }
  return meanValue;
};

export const circleRoi = {
  id: 'CircleRoi',
  name: 'Circle',
  toolGroup: 'allTools',
  cornerstoneToolType: 'CircleRoi',
  options: {
    measurementTable: {
      displayFunction,
    },
    caseProgress: {
      include: true,
      evaluate: true,
    },
  },
};
