import { ToolsUtils as FlywheelCommonToolsUtils } from '@flywheel/extension-flywheel-common';
import getImagePropertyForImagePath from '../lib/getImagePropertyForImagePath';
const displayFunction = data => {
  let meanValue = '';
  if (data.meanStdDev && data.meanStdDev.mean && !isNaN(data.meanStdDev.mean)) {
    const modality = getImagePropertyForImagePath(data.imagePath, 'Modality');
    const pixelSpacing = getImagePropertyForImagePath(
      data.imagePath,
      'PixelSpacing'
    );
    meanValue =
      data.meanStdDev.mean.toFixed(2) +
      ' ' +
      FlywheelCommonToolsUtils.unitUtils.getUnitByModality(
        modality,
        pixelSpacing
      );
  }
  return meanValue;
};

export const freehandMouse = {
  id: 'FreehandRoi',
  name: 'Freehand',
  toolGroup: 'allTools',
  cornerstoneToolType: 'FreehandMouse',
  options: {
    measurementTable: {
      displayFunction,
    },
    caseProgress: {
      include: true,
      evaluate: true,
    },
  },
};
