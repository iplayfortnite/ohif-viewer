import { ToolsUtils as FlywheelCommonToolsUtils } from '@flywheel/extension-flywheel-common';
import getImagePropertyForImagePath from '../lib/getImagePropertyForImagePath';
const displayFunction = data => {
  return "";
};

export const openFreehandRoi = {
  id: 'OpenFreehandRoi',
  name: 'OpenFreehand',
  toolGroup: 'allTools',
  cornerstoneToolType: 'FreehandMouse',
  options: {
    measurementTable: {
      displayFunction,
    },
    caseProgress: {
      include: true,
      evaluate: true,
    },
  },
};
