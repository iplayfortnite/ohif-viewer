import { ToolsUtils as FlywheelCommonToolsUtils } from '@flywheel/extension-flywheel-common';
import getImagePropertyForImagePath from '../lib/getImagePropertyForImagePath';
const displayFunction = data => {
  let meanValue = '';
  const { cachedStats } = data;
  if (cachedStats && cachedStats.mean && !isNaN(cachedStats.mean)) {
    const modality = getImagePropertyForImagePath(data.imagePath, 'Modality');
    const pixelSpacing = getImagePropertyForImagePath(
      data.imagePath,
      'PixelSpacing'
    );
    meanValue =
      cachedStats.mean.toFixed(2) +
      ' ' +
      FlywheelCommonToolsUtils.unitUtils.getUnitByModality(
        modality,
        pixelSpacing
      );
  }
  return meanValue;
};

export const rectangleRoi = {
  id: 'RectangleRoi',
  name: 'Rectangle',
  toolGroup: 'allTools',
  cornerstoneToolType: 'RectangleRoi',
  options: {
    measurementTable: {
      displayFunction,
    },
    caseProgress: {
      include: true,
      evaluate: true,
    },
  },
};
