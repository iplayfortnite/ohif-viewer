import cornerstone from 'cornerstone-core';
import cornerstoneMath from 'cornerstone-math';
import cornerstoneTools from 'cornerstone-tools';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import { MeasurementApi } from '../classes';
import log from '../../log';
import handleContourBoundary from './handleContourBoundary';
import getImageIdForImagePath from '../lib/getImageIdForImagePath';
import getModality from '../lib/getModality';

const convertToVector3 = cornerstoneTools.import('util/convertToVector3');

const {
  getAllMeasurementsForImage,
  measurementToolUtils,
} = FlywheelCommonUtils;
const { isOverlapWithOtherROI } = measurementToolUtils.checkMeasurementOverlap;
const { roiTools } = measurementToolUtils;

export default function(
  { eventData, tool, toolGroupId, toolGroup },
  callback = null
) {
  const measurementApi = MeasurementApi.Instance;
  if (!measurementApi) {
    log.warn('Measurement API is not initialized');
  }

  const { measurementData, toolType } = eventData;

  const collection = measurementApi.tools[toolType];

  // Stop here if the tool data shall not be persisted (e.g. temp tools)
  if (!collection) return;

  log.info('CornerstoneToolsMeasurementModified');
  let measurement = collection.find(t => t._id === measurementData._id);

  // Stop here if the measurement is already deleted
  if (!measurement) return;

  measurement.dirty = true;
  measurement = Object.assign(measurement, measurementData);

  if (getModality(store.getState()) !== 'SM') {
    const image = cornerstone.getImage(eventData.element);
    measurement.viewport = cornerstone.getViewport(eventData.element);
    measurement.viewport.displayedArea ||= cornerstone.getDisplayedArea(
      image,
      measurement.viewport
    );

    const handles = measurement.handles;
    const brhc = measurement.viewport.displayedArea.brhc;
    if (handles.points && handles.points.length > 0) {
      // Special handling for the Open Freehand Fragment ROI which have the points
      // as array of array to represent points in each fragment
      if (Array.isArray(handles.points[0])) {
        measurement.handles.points.forEach(points => {
          for (let i = 0; i < points.length; i++) {
            const point = points[i];
            if (point.x < 0) {
              point.x = 0;
              synchWithPrevHandle(i, points, point);
            } else if (point.x > brhc.x) {
              point.x = brhc.x;
              synchWithPrevHandle(i, points, point);
            }

            if (point.y < 0) {
              point.y = 0;
              synchWithPrevHandle(i, points, point);
            } else if (point.y > brhc.y) {
              point.y = brhc.y;
              synchWithPrevHandle(i, points, point);
            }
          }
        });
      } else {
        const points = measurement.handles.points;
        for (let i = 0; i < points.length; i++) {
          const point = points[i];
          if (point.x < 0) {
            point.x = 0;
            synchWithPrevHandle(i, points, point);
          } else if (point.x > brhc.x) {
            point.x = brhc.x;
            synchWithPrevHandle(i, points, point);
          }

          if (point.y < 0) {
            point.y = 0;
            synchWithPrevHandle(i, points, point);
          } else if (point.y > brhc.y) {
            point.y = brhc.y;
            synchWithPrevHandle(i, points, point);
          }
        }
      }
    } else {
      const isChangePosition = !handles.start.active && !handles.end.active;

      if (isChangePosition) {
        if (measurement.toolType === 'CircleRoi') {
          const getDistance = cornerstoneMath.point.distance;
          const pixelToCanvas = cornerstone.pixelToCanvas;
          const canvasToPixel = cornerstone.canvasToPixel;

          const image = cornerstone.getImage(eventData.element);
          const centreOnImage = measurement.handles.start;
          const centerOnCanvas = pixelToCanvas(
            eventData.element,
            centreOnImage
          );

          const minXPoint = pixelToCanvas(eventData.element, {
            x: 0,
            y: centreOnImage.y,
          });
          const minYPoint = pixelToCanvas(eventData.element, {
            x: centreOnImage.x,
            y: 0,
          });
          const maxXPoint = pixelToCanvas(eventData.element, {
            x: image.width,
            y: centreOnImage.y,
          });
          const maxYPoint = pixelToCanvas(eventData.element, {
            x: centreOnImage.x,
            y: image.height,
          });

          const circumferencePointOnCanvas = pixelToCanvas(
            eventData.element,
            measurement.handles.end
          );

          const radiusOnCanvas = getDistance(
            centerOnCanvas,
            circumferencePointOnCanvas
          );

          let boundaryPoint;
          if (radiusOnCanvas > getDistance(centerOnCanvas, minXPoint)) {
            boundaryPoint = minXPoint;
          } else if (radiusOnCanvas > getDistance(centerOnCanvas, minYPoint)) {
            boundaryPoint = minYPoint;
          } else if (radiusOnCanvas > getDistance(centerOnCanvas, maxXPoint)) {
            boundaryPoint = maxXPoint;
          } else if (radiusOnCanvas > getDistance(centerOnCanvas, maxYPoint)) {
            boundaryPoint = maxYPoint;
          }

          if (boundaryPoint) {
            const dirCanvas = convertToVector3([
              centerOnCanvas.x - boundaryPoint.x,
              centerOnCanvas.y - boundaryPoint.y,
              0,
            ]).normalize();
            const radiusXDirCanvas = dirCanvas.multiplyScalar(radiusOnCanvas);
            const newCentreOnCanvas = {
              x: radiusXDirCanvas.x + boundaryPoint.x,
              y: radiusXDirCanvas.y + boundaryPoint.y,
            };
            circumferencePointOnCanvas.x +=
              newCentreOnCanvas.x - centerOnCanvas.x;
            circumferencePointOnCanvas.y +=
              newCentreOnCanvas.y - centerOnCanvas.y;

            const circumferenceOnImage = canvasToPixel(
              eventData.element,
              measurement.handles.end
            );

            const radiusOnCanvas = getDistance(
              centerOnCanvas,
              circumferencePointOnCanvas
            );

            let boundaryPoint;
            if (radiusOnCanvas > getDistance(centerOnCanvas, minXPoint)) {
              boundaryPoint = minXPoint;
            } else if (
              radiusOnCanvas > getDistance(centerOnCanvas, minYPoint)
            ) {
              boundaryPoint = minYPoint;
            } else if (
              radiusOnCanvas > getDistance(centerOnCanvas, maxXPoint)
            ) {
              boundaryPoint = maxXPoint;
            } else if (
              radiusOnCanvas > getDistance(centerOnCanvas, maxYPoint)
            ) {
              boundaryPoint = maxYPoint;
            }

            if (boundaryPoint) {
              const dirCanvas = convertToVector3([
                centerOnCanvas.x - boundaryPoint.x,
                centerOnCanvas.y - boundaryPoint.y,
                0,
              ]).normalize();
              const radiusXDirCanvas = dirCanvas.multiplyScalar(radiusOnCanvas);
              const newCentreOnCanvas = {
                x: radiusXDirCanvas.x + boundaryPoint.x,
                y: radiusXDirCanvas.y + boundaryPoint.y,
              };
              circumferencePointOnCanvas.x +=
                newCentreOnCanvas.x - centerOnCanvas.x;
              circumferencePointOnCanvas.y +=
                newCentreOnCanvas.y - centerOnCanvas.y;

              const circumferenceOnImage = canvasToPixel(
                eventData.element,
                circumferencePointOnCanvas
              );
              measurement.handles.end.x = circumferenceOnImage.x;
              measurement.handles.end.y = circumferenceOnImage.y;

              const centerOnImage = canvasToPixel(
                eventData.element,
                newCentreOnCanvas
              );
              measurement.handles.start.x = centerOnImage.x;
              measurement.handles.start.y = centerOnImage.y;
            }
          } else {
            const coords = getHandlePointForImage(
              handles.start.x,
              handles.start.y,
              handles.end.x,
              handles.end.y,
              brhc.x,
              brhc.y
            );

            measurement.handles.start.x = coords.startX;
            measurement.handles.start.y = coords.startY;
            measurement.handles.end.x = coords.endX;
            measurement.handles.end.y = coords.endY;
          }
        }
      }
    }

    if (
      !measurementData.active &&
      measurementData.location &&
      roiTools.hasOwnProperty(measurementData.toolType)
    ) {
      const state = store.getState();
      const projectConfig = state.flywheel.projectConfig;
      const labels = projectConfig ? projectConfig.labels || [] : [];
      const contourUnique = projectConfig?.contourUnique;

      let measurements = getAllMeasurementsForImage(
        measurementData.imagePath,
        state.timepointManager.measurements,
        roiTools
      );
      const imageId = getImageIdForImagePath(measurement.imagePath);

      let boundaryMeasurements = [];
      if (
        labels.some(item => item.boundary && item.boundary.length) &&
        contourUnique
      ) {
        // Boundary compared measurements will filter out from the image measurements list
        boundaryMeasurements = handleContourBoundary(
          measurementData,
          imageId,
          labels,
          measurements,
          eventData.element
        );
        if (boundaryMeasurements && boundaryMeasurements.length) {
          const boundaryMeasures = boundaryMeasurements.filter(
            x => !x.readonly
          );
          if (!measurementData.readonly) {
            callback(measurementData, boundaryMeasures);
          } else {
            if (boundaryMeasures.length) {
              callback(boundaryMeasures[0], boundaryMeasures);
            }
          }
        }
      }

      if (boundaryMeasurements.length === 0 && contourUnique) {
        const overlappedMeasurements = isOverlapWithOtherROI(
          toolType,
          measurement,
          imageId,
          eventData.element
        );
        const filteredMeasurements = [];
        // Check the overlapped measurements are there in the pending measurement list
        overlappedMeasurements.forEach(overlappedMeasure => {
          if (
            measurements.find(
              measurement => measurement._id === overlappedMeasure._id
            )
          ) {
            const labelConfig = labels.find(
              label => label.value === overlappedMeasure.location
            );
            if (labelConfig) {
              if (!labelConfig.boundary || labelConfig.boundary.length === 0) {
                filteredMeasurements.push(overlappedMeasure);
              } else if (
                labelConfig.boundary.length &&
                !labelConfig.boundary.find(
                  x => x.label === measurementData.location
                )
              ) {
                filteredMeasurements.push(overlappedMeasure);
              }
            }
          }
        });
        if (filteredMeasurements && filteredMeasurements.length) {
          const boundaryMeasures = filteredMeasurements.filter(
            x => !x.readonly
          );
          if (!measurementData.readonly) {
            callback(measurementData, boundaryMeasures);
          } else {
            if (boundaryMeasures.length) {
              callback(boundaryMeasures[0], boundaryMeasures);
            }
          }
        }
      }
    }
  }

  measurementApi.updateMeasurement(toolType, measurement);

  // TODO: Notify about the last activated measurement

  if (MeasurementApi.isToolIncluded(tool)) {
    // TODO: Notify that viewer suffered changes
  }
}
/**
 * Function replace previous handles lines array with modified coordinate.
 * @param {Integer} i
 * @param {Array} points
 * @param {Object} point
 */
function synchWithPrevHandle(i, points, point) {
  if (i !== 0) {
    points[i - 1].lines = [
      {
        x: point.x,
        y: point.y,
      },
    ];
  }
}

// Get measurement points while drag annotations
function getHandlePointForImage(
  startX,
  startY,
  endX,
  endY,
  imageWidth,
  imageHeight
) {
  const coords = { startX: startX, endX: endX, startY: startY, endY: endY };

  if (startX < 0) {
    coords.startX = 0;
    coords.endX += Math.abs(startX);
  } else if (endX > imageWidth) {
    const diff = endX - imageWidth;
    coords.endX = imageWidth;
    coords.startX -= diff;
  } else if (startX > imageWidth) {
    const diff = startX - imageWidth;
    coords.startX = imageWidth;
    coords.endX -= diff;
  } else if (endX < 0) {
    coords.endX = 0;
    coords.startX += Math.abs(endX);
  }

  if (startY < 0) {
    coords.startY = 0;
    coords.endY += Math.abs(startY);
  } else if (endY > imageHeight) {
    const diff = endY - imageHeight;
    coords.endY = imageHeight;
    coords.startY -= diff;
  } else if (startY > imageHeight) {
    const diff = startY - imageHeight;
    coords.startY = imageHeight;
    coords.endY -= diff;
  } else if (endY < 0) {
    coords.endY = 0;
    coords.startY += Math.abs(endY);
  }

  return coords;
}
