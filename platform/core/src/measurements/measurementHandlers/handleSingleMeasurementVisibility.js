import cornerstone from 'cornerstone-core';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import { MeasurementApi } from '../classes';
import log from '../../log';
import refreshCornerstoneViewports from '../lib/refreshCornerstoneViewports';
import getModality from '../lib/getModality';

const { commonToolUtils } = FlywheelCommonUtils;
const { saveActionState } = commonToolUtils;

export default function handleSingleMeasurementVisibility(
  { eventData, tool, toolGroupId, toolGroup },
  visibleStatus = null,
  notifyUpdateAndSync = true
) {
  log.info('CornerstoneToolsMeasurementVisibility');
  const { measurementData, toolType } = eventData;

  const measurementApi = MeasurementApi.Instance;
  if (!measurementApi) {
    log.warn('Measurement API is not initialized');
  }

  const collection = measurementApi.tools[toolType];

  // Stop here if the tool data shall not be persisted (e.g. temp tools)
  if (!collection) return;

  const measurementTypeId = measurementApi.toolsGroupsMap[toolType];
  const measurement = collection.find(t => t._id === measurementData._id);

  // Stop here if the measurement is already gone or never existed
  if (!measurement) return;

  saveActionState(measurement, 'measurements', 'Modify', true, false);
  const { lesionNamingNumber, timepointId, visible } = measurement;

  if (visible !== visibleStatus) {
    measurement.dirty = true;
  }
  measurementApi.setMeasurementVisibility(
    toolType,
    measurementTypeId,
    {
      lesionNamingNumber,
      timepointId,
    },
    visibleStatus,
    notifyUpdateAndSync
  );

  saveActionState(measurement, 'measurements', 'Modify', false, true);
  if (getModality(store.getState()) !== 'SM') {
    // TODO: This is very hacky, but will work for now
    refreshCornerstoneViewports();
  }

  if (MeasurementApi.isToolIncluded(tool)) {
    // TODO: Notify that viewer suffered changes
  }
}
