import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import { MeasurementApi } from '../classes';
import handleSingleMeasurementAdded from './handleSingleMeasurementAdded';
import handleChildMeasurementAdded from './handleChildMeasurementAdded';
import handleSingleMeasurementModified from './handleSingleMeasurementModified';
import handleChildMeasurementModified from './handleChildMeasurementModified';
import handleSingleMeasurementRemoved from './handleSingleMeasurementRemoved';
import handleSingleMeasurementVisibility from './handleSingleMeasurementVisibility';
import handleChildMeasurementRemoved from './handleChildMeasurementRemoved';
import handleContourBoundary from './handleContourBoundary';
import jumpToRowItem from './jumpToRowItem';
import Redux from '../../redux';

const scrollToIndex = cornerstoneTools.import('util/scrollToIndex');

const { setViewportSpecificData } = Redux.actions;
const getEventData = event => {
  const eventData = event.detail;
  if (eventData.toolName) {
    eventData.toolType = eventData.toolName;
  }

  return eventData;
};

const MeasurementHandlers = {
  handleSingleMeasurementAdded,
  handleChildMeasurementAdded,
  handleSingleMeasurementModified,
  handleChildMeasurementModified,
  handleSingleMeasurementRemoved,
  handleChildMeasurementRemoved,
  handleSingleMeasurementVisibility,
  handleContourBoundary,

  onAdded(event) {
    const eventData = getEventData(event);
    const { toolType } = eventData;
    const {
      toolGroupId,
      toolGroup,
      tool,
    } = MeasurementApi.getToolConfiguration(toolType);
    const params = {
      eventData,
      tool,
      toolGroupId,
      toolGroup,
    };

    if (!tool) return;

    if (tool.parentTool) {
      handleChildMeasurementAdded(params);
    } else {
      handleSingleMeasurementAdded(params);
    }
  },

  onModified(event, callback = null) {
    const eventData = getEventData(event);
    const { toolType } = eventData;
    const {
      toolGroupId,
      toolGroup,
      tool,
    } = MeasurementApi.getToolConfiguration(toolType);
    const params = {
      eventData,
      tool,
      toolGroupId,
      toolGroup,
    };

    if (!tool) return;

    if (tool.parentTool) {
      handleChildMeasurementModified(params);
    } else {
      handleSingleMeasurementModified(params, callback);
    }
  },

  onRemoved(event) {
    const eventData = getEventData(event);
    const { toolType } = eventData;
    const {
      toolGroupId,
      toolGroup,
      tool,
    } = MeasurementApi.getToolConfiguration(toolType);
    const params = {
      eventData,
      tool,
      toolGroupId,
      toolGroup,
    };

    if (!tool) return;

    if (tool.parentTool) {
      handleChildMeasurementRemoved(params);
    } else {
      handleSingleMeasurementRemoved(params);
    }
  },
  onVisible(event, visibleStatus = null, notifyUpdateAndSync = true) {
    const eventData = getEventData(event);
    const { toolType } = eventData;
    const {
      toolGroupId,
      toolGroup,
      tool,
    } = MeasurementApi.getToolConfiguration(toolType);
    const params = {
      eventData,
      tool,
      toolGroupId,
      toolGroup,
    };

    if (!tool) return;
    handleSingleMeasurementVisibility(
      params,
      visibleStatus,
      notifyUpdateAndSync
    );
  },
  onItemClick(measurementData) {
    const store = window.store.getState();
    const viewportsState = store.viewports;
    const timepointManagerState = store.timepointManager;

    // TODO: invertViewportTimepointsOrder should be stored in / read from user preferences
    // TODO: childToolKey should come from the measurement table when it supports child tools
    const options = {
      invertViewportTimepointsOrder: false,
      childToolKey: null,
    };

    let actionData = jumpToRowItem(
      measurementData,
      viewportsState,
      timepointManagerState,
      options
    );

    actionData.viewportSpecificData.forEach(viewportSpecificData => {
      const { viewportIndex, displaySet } = viewportSpecificData;

      window.store.dispatch(setViewportSpecificData(viewportIndex, displaySet));
    });

    const { toolType, measurementNumber } = measurementData;
    const measurementApi = MeasurementApi.Instance;

    Object.keys(measurementApi.tools).forEach(toolType => {
      const measurements = measurementApi.tools[toolType];

      measurements.forEach(measurement => {
        measurement.active = false;
      });
    });

    const measurementsToActive = measurementApi.tools[toolType].filter(
      measurement => {
        return measurement.measurementNumber === measurementNumber;
      }
    );

    measurementsToActive.forEach(measurementToActive => {
      measurementToActive.active = true;
    });

    measurementApi.syncMeasurementsAndToolData();

    cornerstone.getEnabledElements().forEach((enabledElement, index) => {
      const { displaySet } =
        actionData.viewportSpecificData.find(
          ({ viewportIndex }) => viewportIndex === index
        ) || {};
      if (displaySet && displaySet.images) {
        const scrollIndex = displaySet.images.findIndex(
          image => image.SOPInstanceUID === displaySet.SOPInstanceUID
        );
        if (scrollIndex >= 0) {
          scrollToIndex(enabledElement.element, scrollIndex);
        }
      }
      cornerstone.updateImage(enabledElement.element);
    });
  },
};

export default MeasurementHandlers;
