import cornerstone from 'cornerstone-core';
import { MODULE_TYPES, utils } from '@ohif/core';
import * as cornerstoneNIFTIImageLoader from '@cornerstonejs/nifti-image-loader';
import { Utils } from '@flywheel/extension-flywheel-common';
import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';
import store from '@ohif/viewer/src/store';
import getSourceDisplaySet from './getSourceDisplaySet';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
const { parseSliceIndex } = FlywheelCommonUtils;

const { segmentationUtils } = Utils;
const SOP_CLASS_UIDS = {
  MR: '1.2.840.10008.5.1.4.1.1.4',
  ENHANCED_MR: '1.2.840.10008.5.1.4.1.1.4.1',
  MR_SPECTROSCOPY: '1.2.840.10008.5.1.4.1.1.4.2',
  SEG: '1.2.840.10008.5.1.4.1.1.66.4',
};

async function loadImage(imageId, refSeriesOrientation = []) {
  const image = await cornerstoneNIFTIImageLoader.nifti.cornerstoneLoader(
    imageId
  );
  let pixelData = image.getPixelData();

  if (refSeriesOrientation?.length > 0) {
    const { imageOrientationPatient } = cornerstone.metaData.get(
      'imagePlaneModule',
      imageId
    );

    // Reverse the rows to flip left and right if rowcosines are opposite for
    // base image and overlay.
    if (
      isOppositeInDirection(
        refSeriesOrientation.slice(0, 3),
        imageOrientationPatient.slice(0, 3)
      )
    ) {
      for (let rowIndex = 0; rowIndex < image.rows; rowIndex++) {
        for (let colIndex = 0; colIndex < (image.columns - 1) / 2; colIndex++) {
          let temp = pixelData[colIndex + rowIndex * image.columns];
          pixelData[colIndex + rowIndex * image.columns] =
            pixelData[image.columns - 1 - colIndex + rowIndex * image.columns];
          pixelData[
            image.columns - 1 - colIndex + rowIndex * image.columns
          ] = temp;
        }
      }
    }

    // Reverse the columns to flip top and bottom if columncosines are opposite
    // for base image and overlay.
    if (
      isOppositeInDirection(
        refSeriesOrientation.slice(3, 6),
        imageOrientationPatient.slice(3, 6)
      )
    ) {
      for (let colIndex = 0; colIndex < image.columns; colIndex++) {
        for (let rowIndex = 0; rowIndex < (image.rows - 1) / 2; rowIndex++) {
          let temp = pixelData[colIndex + rowIndex * image.columns];
          pixelData[colIndex + rowIndex * image.columns] =
            pixelData[colIndex + image.columns * (image.rows - 1 - rowIndex)];
          pixelData[
            colIndex + image.columns * (image.rows - 1 - rowIndex)
          ] = temp;
        }
      }
    }
  }

  image.getPixelData = function getPixelData() {
    return pixelData;
  };
  return image;
}

const isOppositeInDirection = (direction1, direction2) => {
  const majorAxis1 = direction1.reduce((majorComponent, component) =>
    Math.abs(majorComponent) > Math.abs(component) ? majorComponent : component
  );
  const majorAxis2 = direction2[direction1.indexOf(majorAxis1)];
  return (
    majorAxis1 / Math.abs(majorAxis1) !== majorAxis2 / Math.abs(majorAxis2)
  );
};

const NiftiSopClassHandler = {
  id: 'NiftiSopClassHandlerPlugin',
  type: MODULE_TYPES.SOP_CLASS_HANDLER,
  sopClassUIDs: [
    SOP_CLASS_UIDS.MR,
    SOP_CLASS_UIDS.ENHANCED_MR,
    SOP_CLASS_UIDS.MR_SPECTROSCOPY,
    SOP_CLASS_UIDS.SEG,
  ],
  dataProtocol: 'nifti',
  getDisplaySetFromSeries(
    series,
    study,
    webClient,
    authorizationHeaders,
    displaySetDefaultBuilder
  ) {
    const defaultDisplaySet = displaySetDefaultBuilder(
      series,
      series._instances
    );

    const middleFrameIndex = Math.floor(
      (series.getInstanceCount() - 1 || 0) / 2
    );
    const seriesData = series.getData();
    const seriesMatches = [
      seriesData.SeriesInstanceUID,
      seriesData.SeriesDescription,
    ];
    const launchFrameIndex = parseSliceIndex(
      seriesMatches,
      middleFrameIndex,
      seriesData.instances
    );
    const instance = series.getInstanceByIndex(launchFrameIndex);

    const metadata = instance.getData().metadata;
    const { temporalSeries = false } = series._data || {};
    const { timeSlices = false } = study._data || {};
    const SOPInstanceUID =
      typeof instance.SOPInstanceUID === 'function'
        ? instance.SOPInstanceUID()
        : instance.SOPInstanceUID;

    const imageId = instance.getImageId();
    const isSeg = metadata.SOPClassUID === SOP_CLASS_UIDS.SEG;
    defaultDisplaySet.setAttributes({
      plugin: 'cornerstone::nifti',
      dataProtocol: NiftiSopClassHandler.dataProtocol,
      Modality: isSeg ? 'SEG' : 'MR',
      displaySetInstanceUID: utils.guid(),
      imageId,
      isDerived: isSeg,
      referencedDisplaySetUID: null, // Assigned when loaded.
      labelmapIndex: null, // Assigned when loaded.
      isLoaded: false,
      frameIndex: launchFrameIndex,
      metadata,
      SOPInstanceUID: SOPInstanceUID,
      SeriesInstanceUID: series.getSeriesInstanceUID(),
      StudyInstanceUID: study.getStudyInstanceUID(),
      authorizationHeaders: authorizationHeaders,
      temporalSet: temporalSeries,
      timeSlices: timeSlices,
      sopClassUIDs: NiftiSopClassHandler.sopClassUIDs,
    });

    if (isSeg) {
      defaultDisplaySet.getSourceDisplaySet = function(studies) {
        return getSourceDisplaySet(studies, defaultDisplaySet);
      };
      defaultDisplaySet.load = function(
        referencedDisplaySet,
        studies,
        segmentationIndex
      ) {
        const { dataProtocol } = referencedDisplaySet;
        // Convert nifti overlays over dicom to radiological convention(flip left and right),
        // based on configuration.
        const projectConfig = FlywheelRedux.selectors.selectProjectConfig(
          store.getState()
        );
        let loadImageById;
        if (
          dataProtocol !== 'nifti' &&
          projectConfig?.useRadiologyOrientationForNiftiOverDicomOverlay
        ) {
          loadImageById = loadImage;
        } else {
          loadImageById = cornerstoneNIFTIImageLoader.nifti.loadImage.bind(
            cornerstoneNIFTIImageLoader.nifti
          );
        }
        return segmentationUtils.loadSegmentation(
          defaultDisplaySet,
          referencedDisplaySet,
          studies,
          loadImageById,
          segmentationIndex
        );
      };
      defaultDisplaySet.setSegmentVisibility = function(isVisible) {
        return segmentationUtils.setSegmentVisibility(
          defaultDisplaySet.labelmapIndex,
          defaultDisplaySet.segmentationIndex,
          isVisible
        );
      };
      defaultDisplaySet.removeSegment = function(
        referencedDisplaySet,
        studies,
        currentSegIndex,
        newSegIndex = 0,
        newLabelmapIndex = -1
      ) {
        return segmentationUtils.removeSegment(
          defaultDisplaySet,
          referencedDisplaySet,
          studies,
          currentSegIndex,
          newSegIndex,
          newLabelmapIndex
        );
      };
      defaultDisplaySet.rePositionSegment = function(
        referencedDisplaySet,
        studies,
        newLabelmapIndex
      ) {
        return segmentationUtils.rePositionSegment(
          defaultDisplaySet,
          referencedDisplaySet,
          studies,
          newLabelmapIndex
        );
      };
    }

    return defaultDisplaySet;
  },
  loadImage: loadImage,
};

export default NiftiSopClassHandler;
