import OHIF, { redux } from '@ohif/core';
import cornerstone from 'cornerstone-core';
import _ from 'lodash';
import * as cornerstoneNIFTIImageLoader from '@cornerstonejs/nifti-image-loader';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';

const { setAllViewportsSpecificData } = redux.actions;
const { studyMetadataManager } = OHIF.utils;

/**
 * Navigate viewports to another time slices on given direction.
 * It updates all viewports at once.
 * @param {object} viewportSpecificData Current state of viewports
 * @param {number} direction Direction to navigate
 * @param {object} store app store
 */
function navigateTimeInViewports(viewportSpecificData, direction, store) {
  const displaySets = {};
  const viewportIndexes = Object.keys(viewportSpecificData);

  // iterate viewports and update each of them
  for (let keyIndex in viewportSpecificData) {
    const displaySet = viewportSpecificData[keyIndex];

    const nextDisplaySet = _getNextDisplaySet(keyIndex, direction, displaySet);
    if (nextDisplaySet) {
      displaySets[keyIndex] = {
        ...displaySet,
        ...nextDisplaySet,
      };
    }
  }

  // when navigating through slices we should not change number of display but only its content
  if (_.isEqual(viewportIndexes, Object.keys(displaySets))) {
    _updateViewports(displaySets, store);
  }
}

/**
 * It returns the cornerstone Id of image being displayed
 * in the viewport. It uses displaySet for a first inspection then get image Id from cornerstone api
 *
 * @param  {Number} viewportIndex  The HTMLElement of the viewport.
 * @param  {Object} displaySet     Current display set into viewport of index viewportIndex
 * @return {String}                The corresponding cornerstone imageId or null in case not found.
 */
function _getViewportImageId(viewportIndex, displaySet) {
  let enabledElement;

  const { imageId } = displaySet;

  if (imageId) {
    return imageId;
  }

  try {
    // get the current enabled element from the selected viewport
    const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
      viewportIndex
    );
    enabledElement = cornerstone.getEnabledElement(element);
  } catch (error) {
    OHIF.log.warn(`The element ${viewportIndex} does not contain a
        cornerstone enabled element.`);
  }

  return enabledElement && enabledElement.image && enabledElement.image.imageId;
}

/**
 * Method to get next display set based on given arguments.
 * It uses params to identify next display set UID and then find it on list of display sets.
 * @param {number} viewportIndex
 * @param {number} direction
 * @param {Object} currentDisplaySet current Display set for given viewportIndex
 */
const _getNextDisplaySet = (viewportIndex, direction, currentDisplaySet) => {
  const currentImageId = _getViewportImageId(viewportIndex, currentDisplaySet);

  if (!currentImageId) {
    return;
  }

  // parse the NIfTI imageId string to get the slice parameters
  const currentImageIdParts = _parseNiftiImageId(currentImageId);
  const currentTimePoint = currentImageIdParts.timePoint;

  // determine the next timepoint based on the current and the direction
  const nextTimePoint = _getNextTimePoint(currentImageIdParts, direction);

  if (nextTimePoint === currentTimePoint) {
    return;
  }

  const studyInstanceUID = _getStudyInstanceUID(currentImageId);
  if (!studyInstanceUID) {
    return;
  }
  const studyMetadata = studyMetadataManager.get(studyInstanceUID);
  // validate display sets
  if (!studyMetadata) {
    return;
  }

  return _findNextDisplaySet(nextTimePoint, currentDisplaySet, studyMetadata);
};

/**
 * It determines the next time point for the image represented
 * by the currentImageId, going on some direction (typically +1 or -1). If the
 * new time point falls outside the range of time slices for this file, it
 * revolves (eg, if -1 it becomes the last time point).
 *
 * @param  {Object} currentImageIdParts The parsed imageId (containing timePoint and slice data info)
 * @param  {Number} direction           The direction to navigate in time.
 * Typically this is either +1 (advance) or -1 (rewind).
 * @return {Number}                     The new time point value.
 */
function _getNextTimePoint(currentImageIdParts, direction) {
  if (direction === Number.POSITIVE_INFINITY) {
    return _getLastTimePoint(currentImageIdParts);
  }

  if (direction === Number.NEGATIVE_INFINITY) {
    return _getFirstTimePoint();
  }

  // determine current time point and slice dimension of the image
  const currentTimePoint = currentImageIdParts.timePoint;

  // get the next time point value, revolving if it falls outside the limits
  const numberOfTimePoints = cornerstone.metaData.get(
    'functional',
    currentImageIdParts.url
  ).timeSlices;
  const nextTimePoint =
    (currentTimePoint + direction + numberOfTimePoints) % numberOfTimePoints;

  return nextTimePoint;
}

function _getLastTimePoint(currentImageIdParts) {
  const numberOfTimePoints = cornerstone.metaData.get(
    'functional',
    currentImageIdParts.url
  ).timeSlices;

  return numberOfTimePoints - 1;
}

function _getFirstTimePoint() {
  return 0;
}
/**
 * It breaks down url into data info properties and return it.
 *
 * @param  {String} url Image id (url format) to be parsed
 * @return {Object}     Object containing timePoint and slice data info
 */
function _parseNiftiImageId(url) {
  const ImageId = cornerstoneNIFTIImageLoader.nifti.ImageId;

  return ImageId.fromURL(url);
}

/**
 * It returns StudyInstanceUID of given imageId
 *
 * @param  {String} imageId The imageId for which we're looking for the study.
 * @return {string}         Study instance uid
 */
function _getStudyInstanceUID(imageId) {
  const instance = cornerstone.metaData.get('instance', imageId);
  const StudyInstanceUID = instance && instance.StudyInstanceUID;

  if (!StudyInstanceUID) {
    OHIF.log.warn(
      `The image '${imageId}' does not have an associated StudyInstanceUID`
    );
    return null;
  }
  return StudyInstanceUID;
}

/**
 * It finds next display set based on params.
 *
 * @param  {Number} nextTimePoint     Next Time point value.
 * @param  {Object} currentDisplaySet Current display set into viewport of index viewportIndex
 * @param  {Object} studyMetadata     StudyMetada to look for next displaySet
 * @return  {Object}         Next Display set to be placed into viewport
 */
function _findNextDisplaySet(nextTimePoint, currentDisplaySet, studyMetadata) {
  const sliceName = currentDisplaySet.SeriesInstanceUID.split('-')[0];
  const nextSeriesInstanceUID = `${sliceName}-t${nextTimePoint}`;

  let nextDisplaySet = studyMetadata.findDisplaySet((displaySet, index) => {
    return displaySet.SeriesInstanceUID === nextSeriesInstanceUID;
  });

  return nextDisplaySet;
}

/**
 * Method to fire redux store to set new displaySet. Update all viewports at once
 *
 * @param  {Object} displaySets         Display sets to be placed into viewports
 */
function _updateViewports(displaySets, store) {
  if (store && displaySets) {
    store.dispatch(setAllViewportsSpecificData(displaySets));
  }
}

export default navigateTimeInViewports;
