let niftiProvider, niftiProviderPriority;

function registerMetaDataProvider(cornerstone, nifti) {
  captureNiftiMetaDataProvider(nifti);
  cornerstone.metaData.addProvider(
    metaDataProvider,
    niftiProviderPriority + 10
  );
}

const csSimulator = {
  registerImageLoader: () => {},
  metaData: {
    addProvider: (baseProvider, basePriority) => {
      niftiProvider = baseProvider;
      niftiProviderPriority = basePriority;
    },
  },
};

function captureNiftiMetaDataProvider(nifti) {
  nifti.register(csSimulator);
}

function metaDataProvider(type, imageId) {
  const metaData = niftiProvider(type, imageId);
  if (metaData) {
    // VTK currently only handle 16 bit allocated images, so the image pixel data should be in 16 bit format.
    // As per the image loader implementation(cornerstone-nifti-image-loader), it will by default provide the 16 bit data even the original data is
    // Float pixel array, so updating the metadata bitsAllocated attribute to 16.
    if (metaData.bitsAllocated) {
      metaData.bitsAllocated = 16;
    }
  }
  return metaData;
}

export default registerMetaDataProvider;
