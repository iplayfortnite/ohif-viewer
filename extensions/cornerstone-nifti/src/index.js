import NiftiSopClassHandler from './NiftiSopClassHandler.js';
import superExtension from '@ohif/extension-cornerstone/src/index';
import toolbarModule from './toolbarModule.js';
import extensionCommandsModule from './commandsModule.js';
import isTemporalCommandDisabled from './utils/isTemporalDisabled';

import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';

const adaptToolbarDefinitions = (toolbarDefinitions = []) => {
  return toolbarDefinitions.map(toolbarDefinition => {
    if (toolbarDefinition.id === 'Cine') {
      toolbarDefinition.getState = () => {
        const state = store.getState();
        const { viewports = {} } = state;
        let visible = true; // default (dicom file) is visible

        if (FlywheelCommonUtils.isCurrentWebImage()) {
          visible = false;
        } else if (FlywheelCommonUtils.isCurrentNifti(state)) {
          const isTemporalDisabled = isTemporalCommandDisabled(viewports);
          visible = isTemporalDisabled;
        }

        return {
          visible,
        };
      };
    }
    return toolbarDefinition;
  });
};
export default {
  ...superExtension,
  ...{
    id: 'cornerstone::nifti',
    getSopClassHandlerModule() {
      return NiftiSopClassHandler;
    },
    getToolbarModule() {
      const adaptedSuperDefinitions = adaptToolbarDefinitions(
        superExtension.getToolbarModule().definitions
      );
      return {
        definitions: [...adaptedSuperDefinitions, ...toolbarModule.definitions], // use cornerstone definition
        defaultContext: 'ACTIVE_VIEWPORT::CORNERSTONE::NIFTI',
      };
    },
    getCommandsModule({ servicesManager, commandsManager, appConfig }) {
      const superCommandsModule = superExtension.getCommandsModule({
        servicesManager,
      });

      const commandsModule = extensionCommandsModule({
        servicesManager,
        commandsManager,
        appConfig,
      });

      return {
        actions: { ...superCommandsModule.actions, ...commandsModule.actions },
        definitions: {
          ...superCommandsModule.definitions,
          ...commandsModule.definitions,
        },
        defaultContext: 'ACTIVE_VIEWPORT::CORNERSTONE::NIFTI',
      };
    },
    preRegistration() {
      // overwrite superextension but do nothing (case where superextension is added and allowed to register)
    },
  },
};
