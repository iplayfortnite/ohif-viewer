import isTemporalCommandDisabled from './utils/isTemporalDisabled';

const TOOLBAR_BUTTON_TYPES = {
  COMMAND: 'command',
  BUILT_IN: 'builtIn',
};

const TOOLBAR_BUTTON_BEHAVIORS = {
  CINE: 'CINE',
};

const definitions = [
  {
    id: 'PreviousTemporalSeries',
    label: 'Previous',
    icon: 'step-backward',
    type: TOOLBAR_BUTTON_TYPES.COMMAND,
    commandName: 'previousTemporalSeries',
  },
  {
    id: 'CineTemporal',
    label: 'CINE',
    icon: 'youtube',
    //
    type: TOOLBAR_BUTTON_TYPES.BUILT_IN,
    options: {
      behavior: TOOLBAR_BUTTON_BEHAVIORS.CINE,
      isCineTemporal: true,
    },
    getState: () => {
      const state = store.getState();
      const { viewports = {} } = state;
      return {
        visible: !isTemporalCommandDisabled(viewports),
      };
    },
  },
  {
    id: 'NextTemporalSeries',
    label: 'Next',
    icon: 'step-forward',
    type: TOOLBAR_BUTTON_TYPES.COMMAND,
    commandName: 'nextTemporalSeries',
  },
];

export default {
  definitions,
  defaultContext: 'ACTIVE_VIEWPORT::CORNERSTONE::NIFTI',
};
