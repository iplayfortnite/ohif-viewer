import Segmentation from '../components/toolbar/segmentation';
import withCommandsManager from '../components/withCommandsManager';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';

const TOOLBAR_BUTTON_TYPES = {
  COMMAND: 'command',
  SET_TOOL_ACTIVE: 'setToolActive',
};

const getDefinitions = (store, commandsManager, servicesManager) => {
  const toolbarDefinitions = [
    {
      id: 'toggleSegmentation',
      label: 'Segmentation',
      icon: 'layers',
      CustomComponent: withCommandsManager(
        Segmentation,
        commandsManager,
        servicesManager
      ),
      commandName: 'changeSegmentation',
      loadMaskButton: {
        id: 'loadMask',
        label: 'Load Mask',
        type: TOOLBAR_BUTTON_TYPES.COMMAND,
        commandName: 'toggleSegmentationFileChooser',
      },
      editLabel: {
        id: 'editLabel',
        label: '',
        type: TOOLBAR_BUTTON_TYPES.COMMAND,
        commandName: 'toggleChangeSegmentationLabel',
      },
      context: [
        'ACTIVE_VIEWPORT::CORNERSTONE',
        'ACTIVE_VIEWPORT::CORNERSTONE::NIFTI',
        'ACTIVE_VIEWPORT::CORNERSTONE::METAIMAGE',
      ],
      getState: () => {
        return {
          visible: !FlywheelCommonUtils.isCurrentWebImage(),
        };
      },
    },
  ];
  return toolbarDefinitions;
};

export default getDefinitions;
