import cornerstone from 'cornerstone-core';
import * as dcmjs from 'dcmjs';
import debounce from 'lodash/debounce';
import isPlainObject from 'lodash/isPlainObject';
import orderBy from 'lodash/orderBy';
import PropTypes from 'prop-types';
import React, { useEffect, useState, useRef } from 'react';
import isEqual from 'lodash/isEqual';
import { connect } from 'react-redux';
import { Icon } from '@ohif/ui';

import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';
import {
  Utils as FlywheelCommonUtils,
  HTTP as FlywheelCommonHTTP,
} from '@flywheel/extension-flywheel-common';
import { updateStudyFormAvailableLabels } from '@flywheel/extension-flywheel/src/components/StudyForm/utils';
import Redux from '@flywheel/extension-flywheel/src/redux';
import { getMeasurementLabels } from '@flywheel/extension-flywheel/src/components/StudyForm/validation/ValidationEngine.js';

import './TagBrowser.styl';

const { getInstanceTags } = FlywheelCommonHTTP.services;
const { addInstanceTags } = FlywheelRedux.actions;
const tagDictionary = dcmjs.data.DicomMetaDictionary.dictionary;
const { selectMeasurementsByStudyUid } = Redux.selectors;

const TagBrowser = props => {
  const {
    activeViewportIndex,
    instanceTags,
    project,
    measurementLabels,
  } = props;
  const [imageId, setImageId] = useState(undefined);
  const [[sortField, sortOrder], setFieldOrdering] = useState([
    'orderName',
    'asc',
  ]);
  const [fieldFilters, setFieldFilters] = useState({
    name: '',
    tag: '',
    vr: '',
    text: '',
  });

  const metaDataInstanceTag =
    (imageId && cornerstone.metaData.get('instance', imageId)) || {};
  const { SOPInstanceUID, SourceInstanceSequence } = metaDataInstanceTag;
  const tags = instanceTags[SOPInstanceUID];
  updateStudyFormAvailableLabels(measurementLabels);

  useEffect(() => {
    const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
      activeViewportIndex
    );
    if (!element) {
      return;
    }

    clearTimeout(TagBrowser.delayedRequestTimer);

    TagBrowser.delayedRequestTimer = setTimeout(
      () =>
        getTags(
          props,
          tags,
          SOPInstanceUID,
          SourceInstanceSequence,
          project,
          imageId
        ),
      500
    );

    const image = getImage(activeViewportIndex) || {};
    if (image.imageId !== imageId) {
      // when viewport props change
      setImageId(image.imageId);
    }
    const eventListener = debounce(event => {
      // when stack position changes
      setImageId(event.detail.image.imageId);
    });
    element.addEventListener(cornerstone.EVENTS.IMAGE_RENDERED, eventListener);
    return () => {
      element.removeEventListener(
        cornerstone.EVENTS.IMAGE_RENDERED,
        eventListener
      );
    };
  });

  const prevMeasurementLabels = usePrevious(measurementLabels);

  useEffect(() => {
    if (!isEqual(measurementLabels, prevMeasurementLabels)) {
      updateStudyFormAvailableLabels(measurementLabels);
    }
  }, [measurementLabels]);

  if (!tags) {
    return null;
  }

  let fields = Object.entries(tags).map(([key, value]) => {
    const tagId = formatTag(key);
    const tagData = tagDictionary[tagId] || {};
    const name = tagData.name || '';
    const content = buildContent(value.Value);
    const contentText = getContentText(content)
      .trim()
      .toLowerCase();
    return {
      tag: tagId,
      name: name,
      orderName: name.toUpperCase() || 'zzz' + tagId, // move unknowns to the bottom, ordered by tag
      vr: value.vr,
      value: content,
      text: contentText,
    };
  });
  const filterPairs = Object.entries(fieldFilters);
  fields = fields.filter(field => {
    for (const [filterName, filterValue] of filterPairs) {
      const fieldValue = String(field[filterName]).toLowerCase();
      if (!fieldValue.includes(filterValue.toLowerCase())) {
        return false;
      }
    }
    return true;
  });
  fields = orderBy(fields, [sortField], [sortOrder]);

  const headerProps = { sortField, sortOrder, setFieldOrdering };

  return (
    <table className="tag-browser">
      <thead>
        <tr>
          <TagHeader name="orderName" label="Field Name" {...headerProps} />
          <TagHeader name="tag" label="Tag" {...headerProps} />
          <TagHeader name="vr" label="VR" {...headerProps} />
          <TagHeader name="text" label="Content" {...headerProps} />
        </tr>
        <tr>
          {filterPairs.map(([filterName, filterValue]) => (
            <td key={filterName}>
              <input
                type="text"
                value={filterValue}
                onChange={event =>
                  setFieldFilters({
                    ...fieldFilters,
                    [filterName]: event.target.value,
                  })
                }
              />
            </td>
          ))}
        </tr>
      </thead>
      <tbody>
        {fields.map(field => (
          <tr key={field.tag}>
            <td>{field.name}</td>
            <td>{field.tag}</td>
            <td>{field.vr}</td>
            <td>
              <TagContent value={field.value} />
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

TagBrowser.delayedRequestTimer = 0;

TagBrowser.propTypes = {
  activeViewportIndex: PropTypes.number,
  instanceTags: PropTypes.object,
  measurementLabels: PropTypes.arrayOf(PropTypes.string),
};

function TagHeader({ name, label, sortField, sortOrder, setFieldOrdering }) {
  const onSortField = () => {
    let order = 'desc';
    if (name === sortField) {
      order = sortOrder === 'desc' ? 'asc' : 'desc';
    }
    setFieldOrdering([name, order]);
  };
  let sortIcon = 'sort';
  if (name === sortField) {
    sortIcon = sortOrder === 'desc' ? 'sort-down' : 'sort-up';
  }
  return (
    <th onClick={onSortField} style={{ whiteSpace: 'nowrap' }}>
      {label}
      <Icon name={sortIcon} style={{ fontSize: '12px' }} />
    </th>
  );
}

function TagContent({ value, indent }) {
  if (Array.isArray(value)) {
    return value.map((item, index) => (
      <TagContent key={item.key || index} value={item} indent />
    ));
  }
  const content = (
    <>
      {value.key ? `${value.key}: ` : null}
      {isPlainObject(value) ? <TagContent value={value.value} /> : value}
    </>
  );
  if (indent) {
    return <div className="tag-indent">{content}</div>;
  }
  return content;
}

function formatTag(tag) {
  return `(${tag.slice(0, 4)},${tag.slice(4)})`;
}

function buildContent(value) {
  if (Array.isArray(value)) {
    if (value.length <= 1) {
      return buildContent(value[0]);
    }
    return value.map(buildContent);
  }
  if (isPlainObject(value)) {
    if ('Alphabetic' in value) {
      return value['Alphabetic'];
    }
    if ('Value' in value) {
      return buildContent(value['Value']);
    }
    return Object.entries(value).map(([k, v]) => {
      const tagId = formatTag(k) || k;
      const tagData = tagDictionary[tagId] || {};
      return { key: tagData.name || tagId, value: buildContent(v) };
    });
  }
  return String(value === undefined ? '' : value);
}

function getContentText(value) {
  if (Array.isArray(value)) {
    return value.map(getContentText).join(' ');
  }
  return (value.key ? `${value.key}: ` : '') + (value.value || value);
}

function getImage(index) {
  try {
    const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
      index
    );
    const enabledElement = cornerstone.getEnabledElement(element);
    return enabledElement.image;
  } catch (error) {
    return undefined;
  }
}

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  }, [value]);
  return ref.current;
}
let cachedLabels = [];

const mapStateToProps = state => {
  const activeViewportIndex = state.viewports.activeViewportIndex;
  const StudyInstanceUID =
    state.viewports.viewportSpecificData?.[activeViewportIndex]
      ?.StudyInstanceUID;
  const projectConfig = FlywheelCommonUtils.getCondensedProjectConfig();
  const currentSlice = FlywheelCommonUtils.getActiveSliceInfo(
    activeViewportIndex
  );
  const sliceNo = currentSlice?.sliceNumber;

  const measurements =
    selectMeasurementsByStudyUid(state)[StudyInstanceUID] || {};

  const newLabels = getMeasurementLabels(measurements, projectConfig, sliceNo);

  if (!isEqual(newLabels, cachedLabels)) {
    cachedLabels = newLabels;
  }

  return {
    activeViewportIndex: state.viewports.activeViewportIndex,
    instanceTags: state.flywheel.instanceTags,
    project: state.flywheel.project,
    measurementLabels: cachedLabels,
  };
};
function getTags(
  props,
  tags,
  SOPInstanceUID,
  SourceInstanceSequence,
  project,
  imageId
) {
  if (!tags && SOPInstanceUID) {
    const seriesMetadata =
      (imageId && cornerstone.metaData.get('generalSeriesModule', imageId)) ||
      {};

    const referencedInstanceUid =
      (SourceInstanceSequence &&
        SourceInstanceSequence.ReferencedSOPInstanceUID) ||
      SOPInstanceUID;
    getInstanceTags(
      project._id,
      seriesMetadata.studyInstanceUID,
      seriesMetadata.seriesInstanceUID,
      referencedInstanceUid
    ).then(tags => {
      props.addInstanceTags(SOPInstanceUID, tags);
    });
  }
}

const mapDispatchToProps = {
  addInstanceTags,
};

export default connect(mapStateToProps, mapDispatchToProps)(TagBrowser);
