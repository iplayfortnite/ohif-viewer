import React, { useState, useEffect, useRef, useMemo } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import cornerstone from 'cornerstone-core';
import './colorMap.styl';
import { useSelector } from 'react-redux';

import {
  ToolbarButton,
  OverlayTrigger,
  Tooltip,
  ScrollableArea,
} from '@ohif/ui';

const HEIGHT_BAR = 20;
const WIDTH_BAR = 80;

const EXCLUDED_COLOR_IDS = {
  blues: true,
  coll: true,
  collwave: true,
  spring: true,
  summer: true,
  winter: true,
  bone: true,
  hotiron: true,
  hotmetalblue: true,
  copper: true,
};

// it ensures some colors are present prior to others.
const FIRST_COLOR_IDS_ORDER = {
  copper: 1,
  hot: 2,
  jet: 3,
  pet: 4,
  spectral: 5,
};
const DEFAULT_COLOR_ID = 'gray';

const _getValidColors = () => {
  let validColors = cornerstone.colors.getColormapsList(),
    firstColors = [];
  //filter which color opt must be displayed
  //find the first element
  validColors = validColors.filter(color => {
    const colorId = color.id.toLowerCase();

    if (colorId === DEFAULT_COLOR_ID || colorId in FIRST_COLOR_IDS_ORDER) {
      let idx = FIRST_COLOR_IDS_ORDER[colorId];
      if (colorId === DEFAULT_COLOR_ID) {
        idx = 0;
      }
      //for cases idx is greater than length push color to end.
      if (firstColors.length - 1 >= idx) {
        firstColors.splice(idx, 0, color);
      } else {
        firstColors.push(color);
      }

      return false;
    }
    return !(colorId in EXCLUDED_COLOR_IDS);
  });
  return [...firstColors, ...validColors];
};
let validColors = null;
const colorbarMap = {};

const _getInitialState = () => {
  if (!validColors) {
    validColors = _getValidColors();
  }

  return validColors;
};

const _getColorBar = (
  colormapId,
  canvas,
  canvasContext,
  widthBar,
  heightBar
) => {
  let colorbar;
  // create one or use current cached
  if (!colorbarMap[colormapId]) {
    // assuming height/width for colorbar wont change
    const colormap = cornerstone.colors.getColormap(colormapId);
    const lookupTable = colormap.createLookupTable();

    const height = canvas.height;
    const width = canvas.width;

    colorbar = canvasContext.createImageData(
      Number(widthBar),
      Number(heightBar)
    );
    // Set the min and max values then the lookup table
    // will be able to return the right color for this range
    lookupTable.setTableRange(0, width);
    // Update the colorbar pixel by pixel
    for (let col = 0; col < width; col++) {
      let color = lookupTable.mapValue(col);

      if (col >= lookupTable.NumberOfColors) {
        const complementCol = col % lookupTable.NumberOfColors;
        color = lookupTable.mapValue(complementCol);
      }
      for (let row = 0; row < height; row++) {
        const pixel = (col + row * width) * 4;
        colorbar.data[pixel] = color[0];
        colorbar.data[pixel + 1] = color[1];
        colorbar.data[pixel + 2] = color[2];
        colorbar.data[pixel + 3] = color[3];
      }
    }

    colorbarMap[colormapId] = colorbar;
  } else {
    colorbar = colorbarMap[colormapId];
  }

  return colorbar;
};
const _setColorBar = (canvasRef = {}, colormapId, widthBar, heightBar) => {
  const { current: canvas } = canvasRef;
  if (!canvas) {
    return;
  }
  const canvasContext = canvas.getContext('2d');

  let colorbar = _getColorBar(
    colormapId,
    canvas,
    canvasContext,
    widthBar,
    heightBar
  );

  canvasContext.putImageData(colorbar, 0, 0);
};

function _runColorChangeCommand(button, options, commandsManager) {
  if (button.commandName) {
    const _options = Object.assign({}, options, button.commandOptions);
    commandsManager.runCommand(button.commandName, _options);
  }
}

function _isIntoParentViewport(content) {
  try {
    const rect = content.getBoundingClientRect();
    const { top = 0 } = rect;
    const { offsetHeight } = content;
    const parentUl = content.parentNode;
    const parentWrapper = parentUl.parentNode;
    const parentRect = parentWrapper.getBoundingClientRect();
    const { top: parentTop = 0 } = parentRect;
    const { offsetHeight: parentOffsetHeight } = parentWrapper;

    const bottomPos = top + offsetHeight;
    const parentBottomPos = parentTop + parentOffsetHeight;

    return parentTop < top && bottomPos < parentBottomPos;
  } catch (e) {
    return true;
  }
}

function ColormapOption({ color, widthBar, heightBar, selected, onClick }) {
  const { name, id } = color;
  const canvasRef = useRef('canvas');
  const contentRef = useRef('content');

  const _widthBar = Number(widthBar);
  const _heightBar = Number(heightBar);

  useEffect(() => {
    _setColorBar(canvasRef, id, _widthBar, _heightBar);
  }, []);

  useEffect(() => {
    if (selected && !_isIntoParentViewport(contentRef.current)) {
      // ensure content is visible on select panel
      contentRef.current.scrollIntoView(true);
    }
  }, [selected]);

  return (
    <li
      ref={contentRef}
      id={`colormapDialog-opt-${id}`}
      className={classnames('colormapDialog-opt', { selected })}
      data-id={id}
      onClick={() => onClick(id)}
    >
      <div className="colormapDialog-name">{name}</div>
      <div className="colormapDialog-colorBar">
        <canvas ref={canvasRef} width={_widthBar} height={_heightBar}></canvas>
      </div>
    </li>
  );
}
function ColormapSelect({ colors, onChangeSelect, selectedColorId }) {
  const onClickCallback = selectedId => {
    if (typeof onChangeSelect === 'function') {
      onChangeSelect(selectedId);
    }
  };

  return (
    <div className="scrollableColormap">
      <ScrollableArea>
        <ul>
          {colors.map((color, index) => (
            <ColormapOption
              key={index}
              color={color}
              widthBar={WIDTH_BAR}
              heightBar={HEIGHT_BAR}
              selected={selectedColorId === color.id}
              onClick={onClickCallback}
            ></ColormapOption>
          ))}
        </ul>
      </ScrollableArea>
    </div>
  );
}
function Colormap({
  button,
  activeButtons,
  isActive = false,
  className = '',
  commandsManager,
}) {
  const colormapId = useSelector(state => {
    const viewports = state.viewports || {};
    return viewports.colormapId;
  });
  const [colors] = useState(_getInitialState(activeButtons));
  const [selectedColorId, setSelectedColorId] = useState(colormapId);
  const [isExpanded] = useState(false);
  const { label, icon, resetButton, commandName } = button;
  useEffect(() => {
    if (colormapId !== selectedColorId) {
      setSelectedColorId(colormapId);
    }
  }, [colormapId]);
  function onChangeSelect(selectedValue) {
    // find select value
    const operation = colors.find(button => button.id === selectedValue);

    if (!operation) {
      return;
    }

    _runColorChangeCommand(
      { commandName },
      { colorId: selectedValue, isReset: false },
      commandsManager
    );

    setSelectedColorId(selectedValue);
  }

  function onResetSelect() {
    _runColorChangeCommand(
      resetButton,
      {
        isReset: true,
      },
      commandsManager
    );

    setSelectedColorId(DEFAULT_COLOR_ID);
  }

  const getResetButtonComponent = button => {
    const { id, label, icon } = button;
    return (
      <ToolbarButton
        key={id}
        label={label}
        icon={icon}
        disabled={false}
        onClick={() => onResetSelect()}
      />
    );
  };

  const getToolBarButtonComponent = () => {
    return (
      <ToolbarButton
        key="menu-button"
        type="tool"
        label={label}
        icon={icon}
        className={'toolbar-button expandableToolMenu'}
        isActive={isActive}
        isActive={false}
        isExpandable={true}
        isExpanded={isExpanded}
      />
    );
  };

  const toolbarButtonComponent = getToolBarButtonComponent();
  const colormapSelect = useMemo(() => {
    return (
      <ColormapSelect
        colors={colors}
        onChangeSelect={onChangeSelect}
        selectedColorId={selectedColorId}
      ></ColormapSelect>
    );
  }, [colors, selectedColorId]);

  const toolbarMenuOverlay = () => (
    <Tooltip
      placement="bottom"
      className={classnames(
        'tooltip-toolbar-overlay colormapDialog',
        className
      )}
      id={`${Math.random()}_tooltip-toolbar-overlay}`}
    >
      {colormapSelect}
      <div>{getResetButtonComponent(resetButton)}</div>
    </Tooltip>
  );

  return (
    <OverlayTrigger
      key="menu-button"
      trigger="click"
      placement="bottom"
      rootClose={true}
      overlay={toolbarMenuOverlay()}
    >
      {toolbarButtonComponent}
    </OverlayTrigger>
  );
}

Colormap.propTypes = {
  button: PropTypes.object.isRequired,
  activeButtons: PropTypes.array.isRequired,
  isActive: PropTypes.bool,
  className: PropTypes.string,
};

export default Colormap;
