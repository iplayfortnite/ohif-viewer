import React, { useState, useEffect, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import {
  ToolbarButton,
  OverlayTrigger,
  Tooltip,
  ScrollableArea,
  Range,
  Icon,
  OverlayObserver,
} from '@ohif/ui';
import { utils } from '@ohif/core';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import './segmentation.styl';
import {
  Redux as FlywheelCommonRedux,
  Utils,
} from '@flywheel/extension-flywheel-common';
import {
  Redux as FlywheelRedux,
  Components as FlywheelComponents,
} from '@flywheel/extension-flywheel';
import {
  editSegmentationMask,
  exitEditSegmentationMode,
  isSelectedSegmentMaskInEditing,
  getSelectedSegmentationData,
  getActiveLabelmapIndex,
  setSegmentToolActive,
} from '@flywheel/extension-cornerstone-infusions';

const {
  addSegmentationData,
  setSegmentationData,
} = FlywheelCommonRedux.actions;
const { SaveSegmentationDialog } = FlywheelComponents;
const { selectCurrentVolumetricImage } = FlywheelCommonRedux.selectors;
const { studyMetadataManager } = utils;
const { selectAllAssociations } = FlywheelRedux.selectors;
const { isCurrentFile } = Utils;

function Segmentation({
  button,
  activeButtons,
  isActive = false,
  className = '',
  commandsManager,
  servicesManager,
}) {
  const confirmSaveSegmentationDialog = (
    selectedSegmentItem,
    segmentationIndex
  ) => {
    const { UIDialogService } = servicesManager.services;
    const popupFromLeft = 175;
    const popupFromTop = 220;
    const dialogId = UIDialogService.create({
      content: SaveSegmentationDialog,
      defaultPosition: {
        x: window.innerWidth / 2 - popupFromLeft,
        y: window.innerHeight / 2 - popupFromTop,
      },
      showOverlay: true,
      contentProps: {
        label:
          'Please save the segmentation. This will update the existing file with new version.',
        onClose: () => UIDialogService.dismiss({ id: dialogId }),
        onExitEdit: () => {
          onSaveOrExitEditSegmentation(
            selectedSegmentItem,
            segmentationIndex,
            false
          );
          UIDialogService.dismiss({ id: dialogId });
        },
        onSave: () => {
          onSaveOrExitEditSegmentation(
            selectedSegmentItem,
            segmentationIndex,
            true
          );
          UIDialogService.dismiss({ id: dialogId });
        },
      },
    });
  };

  const segmentationData =
    useSelector(store => store.segmentation.segmentationData) || {};
  const currentVolumetricImage = useSelector(selectCurrentVolumetricImage);
  // For keeping the active reference UID in which segmentations are added
  const activeReferenceUID = useSelector(store => {
    let referenceUID = null;
    const { viewports = {} } = store;
    if (viewports) {
      const { viewportSpecificData, activeViewportIndex } = viewports;
      if (viewportSpecificData[activeViewportIndex]) {
        referenceUID = currentVolumetricImage
          ? viewportSpecificData[activeViewportIndex].StudyInstanceUID
          : viewportSpecificData[activeViewportIndex].SeriesInstanceUID;
      }
    }
    return referenceUID;
  });
  const activeViewport = useSelector(store => {
    let viewport = null;
    const { viewports = {} } = store;
    if (viewports) {
      const { viewportSpecificData, activeViewportIndex } = viewports;
      if (viewportSpecificData[activeViewportIndex]) {
        viewport = viewportSpecificData[activeViewportIndex];
      }
    }
    return viewport;
  });
  const loadedSeriesCount = useSelector(store => {
    let count = 0;
    const studyData = store.studies?.studyData;
    if (activeViewport && studyData) {
      const studyInstanceUid = activeViewport.StudyInstanceUID;
      const studyMetadata = studyMetadataManager.get(studyInstanceUid);
      count = studyMetadata ? studyMetadata._derivedDisplaySets?.length : 0;
    }
    return count;
  });
  const allAssociations = useSelector(selectAllAssociations);
  const { label, icon, loadMaskButton, editLabel } = button;
  const [isExpanded] = useState(false);
  const dispatch = useDispatch();

  const [volumetricMaskArray, setVolumetricMaskArray] = useState([]);
  const [rtMaskArray, setRTMaskArray] = useState([]);

  useEffect(() => {
    let volumetricMaskItems = [];
    let rtMaskItems = [];
    if (activeViewport && activeReferenceUID) {
      if (segmentationData[activeReferenceUID]) {
        segmentationData[activeReferenceUID].forEach(segment => {
          if (segment.modality === 'SEG') {
            volumetricMaskItems.push(segment);
          }
        });
        segmentationData[activeReferenceUID].forEach(segment => {
          if (segment.modality === 'RTSTRUCT') {
            rtMaskItems.push(segment);
          }
        });
      }
    }
    setVolumetricMaskArray(volumetricMaskItems);
    setRTMaskArray(rtMaskItems);
  }, [segmentationData, activeReferenceUID]);

  useEffect(() => {
    addLoadedRTStructSegments();
  }, [loadedSeriesCount, activeReferenceUID]);

  function addLoadedRTStructSegments() {
    if (activeViewport && activeReferenceUID) {
      const studyMetadata = studyMetadataManager.get(
        activeViewport.StudyInstanceUID
      );
      const modality = 'RTSTRUCT';
      const derivedDatasets =
        studyMetadata?.getDerivedDatasets({
          Modality: modality,
        }) || [];
      derivedDatasets.forEach(derivedDataset => {
        if (
          !rtMaskArray.find(
            mask =>
              mask.studyInstanceUID === derivedDataset.StudyInstanceUID &&
              mask.seriesInstanceUID === derivedDataset.SeriesInstanceUID
          ) &&
          !(segmentationData[activeReferenceUID] || []).find(
            mask =>
              mask.studyInstanceUID === derivedDataset.StudyInstanceUID &&
              mask.seriesInstanceUID === derivedDataset.SeriesInstanceUID
          )
        ) {
          const associationKey = Object.keys(allAssociations).find(
            key =>
              allAssociations[key].study_uid ===
                derivedDataset.StudyInstanceUID &&
              allAssociations[key].series_uid ===
                derivedDataset.SeriesInstanceUID
          );
          const seriesDescription =
            'RT STruct ' + derivedDataset.SeriesDescription;
          const association = allAssociations[associationKey];

          if (association) {
            dispatch(
              addSegmentationData({
                referenceUID: activeReferenceUID,
                name: seriesDescription,
                parent_ref: association.acquisition_id,
                file_id: association.file_id,
                modality: modality,
                isLoadedInitially: true,
                studyInstanceUID: association.study_uid,
                seriesInstanceUID: association.series_uid,
              })
            );
          }
        }
      });
    }
  }

  function saveSegmentationData(maskArray, activeReferenceUID, modality) {
    let combinedArray = [];
    if (modality === 'RTSTRUCT') {
      combinedArray = volumetricMaskArray.concat(maskArray);
    } else {
      combinedArray = rtMaskArray.concat(maskArray);
    }
    dispatch(setSegmentationData([...combinedArray], activeReferenceUID));
  }

  function runSegmentationChangeCommand(button, options, commandsManager) {
    if (button.commandName) {
      const _options = Object.assign({}, options, button.commandOptions);
      commandsManager.runCommand(button.commandName, _options);
    }
  }

  const getItemStyle = (isDragging, draggableStyle) => {
    const { transform } = draggableStyle;
    let activeTransform = {};
    if (transform) {
      activeTransform = {
        transform: `translate(0, ${transform.substring(
          transform.indexOf(',') + 1,
          transform.indexOf(')')
        )})`,
      };
    }
    return {
      // some basic styles to make the items look a bit nicer
      userSelect: 'none',
      // styles we need to apply on draggables
      ...draggableStyle,
      ...activeTransform,
    };
  };

  const handleVolumetricFileOverlayOnDragEnd = result => {
    handleOnDragEnd(result, volumetricMaskArray, setVolumetricMaskArray);
  };

  const handleRTOverlayOnDragEnd = result => {
    handleOnDragEnd(result, rtMaskArray, setRTMaskArray);
  };

  const handleOnDragEnd = (dragData, dragMaskArray, setDragMaskArray) => {
    const items = Array.from(dragMaskArray);
    const [reOrderedItems] = items.splice(dragData.source.index, 1);
    if (dragData.destination != null) {
      items.splice(dragData.destination.index, 0, reOrderedItems);
      setDragMaskArray(items);
      saveSegmentationData(items, activeReferenceUID, items[0].modality);
    }
  };

  const getToolBarButtonComponent = () => {
    return (
      <ToolbarButton
        key="menu-button"
        type="tool"
        label={label}
        icon={icon}
        class={'material-icons'}
        className={'toolbar-button expandableToolMenu'}
        isActive={isActive}
        isExpandable={true}
        isExpanded={isExpanded}
      />
    );
  };

  function onClickEditSegmentation(selectedSegmentItem, segmentationIndex) {
    if (isSelectedSegmentMaskInEditing(selectedSegmentItem)) {
      OverlayObserver.setToggleState();
      confirmSaveSegmentationDialog(selectedSegmentItem, segmentationIndex);
    } else {
      updateMaskEditingStatusInSegmentationData(
        selectedSegmentItem,
        segmentationIndex
      );
      setSegmentToolActive();
      editSegmentationMask(selectedSegmentItem);
    }
  }

  const toolbarButtonComponent = getToolBarButtonComponent();

  const editSegmentationLabel = segmentationData => {
    OverlayObserver.setToggleState();
    runSegmentationChangeCommand(editLabel, segmentationData, commandsManager);
  };

  const SegmentationItemsList = ({ props }) => {
    const [valueInMaskArray, setValueInMaskArray] = useState(0);
    const { segmentationIndex, item, setMaskArray, maskArray } = props;
    const state = store.getState();
    const shouldDisplayEditOptions =
      item.modality !== 'RTSTRUCT' && !isCurrentFile(state);

    return (
      <Fragment>
        <div className={'drag-maindiv'}>
          <div title={item.name} className={'load-mask-title'}>
            {item.name}
          </div>
          <div className={'seg-pointer'}>
            <Icon
              className={`times ${setLabelChangeButton(item)}`}
              name={'times'}
              width="15px"
              height="13px"
              onClick={() => {
                let newMaskArray = maskArray.filter(elem => {
                  if (elem.id != item.id) return item;
                });
                let filteredMaskArray = [];
                newMaskArray.map((element, i) => {
                  filteredMaskArray.push({ ...element, id: i + 1 });
                });
                setMaskArray(filteredMaskArray);
                saveSegmentationData(
                  filteredMaskArray,
                  activeReferenceUID,
                  item.modality
                );
              }}
            />
          </div>
        </div>
        <div className={'drag-innerdiv'}>
          <div className={'seg-enable'}>
            <Icon
              className={`eye-icon ${item.isVisible && 'expanded'}`}
              name={item.isVisible ? 'eye' : 'eye-closed'}
              width="20px"
              height="20px"
              onClick={() => {
                let newVisibility = { ...item };
                newVisibility.isVisible = !newVisibility.isVisible;
                let newMaskArray = [...maskArray];
                newMaskArray[segmentationIndex] = newVisibility;
                setMaskArray(newMaskArray);
                saveSegmentationData(
                  newMaskArray,
                  activeReferenceUID,
                  item.modality
                );
              }}
            />
          </div>
          <div
            className={classnames('range seg-range', className)}
            style={{ pointerEvents: item.isVisible ? 'auto' : 'none' }}
          >
            <Range
              step={1}
              min={0}
              max={100}
              value={Number(item.opacity)}
              id={'seg-mask-' + segmentationIndex}
              onChange={event => {
                let inputValue = event.target.value;
                setValueInMaskArray(inputValue);
              }}
              onMouseUp={() => {
                let newItem = { ...item };
                let newMaskArray = [...maskArray];
                newItem.opacity = valueInMaskArray;
                newMaskArray[segmentationIndex] = newItem;
                setMaskArray(newMaskArray);
                setValueInMaskArray(0);
                saveSegmentationData(
                  newMaskArray,
                  activeReferenceUID,
                  newItem.modality
                );
              }}
            />
          </div>
          {shouldDisplayEditOptions && (
            <>
              <div className={`seg-edit`} title="Label change">
                <ToolbarButton
                  className={setLabelChangeButton(item)}
                  icon={'create'}
                  label=""
                  onClick={() => {
                    editSegmentationLabel(item);
                  }}
                />
              </div>
              <div className={`seg-edit`}>
                <ToolbarButton
                  className={setEditButton(item)}
                  icon={'brush'}
                  label=""
                  onClick={() => {
                    onClickEditSegmentation(item, segmentationIndex);
                  }}
                />
              </div>
            </>
          )}
        </div>
      </Fragment>
    );
  };

  const SegmentationLayerItem = props => {
    const { segmentationIndex } = props;
    const activeLabelmapIndex = getActiveLabelmapIndex();
    const shouldDisplayDrag = activeLabelmapIndex === 0;

    return (
      <Fragment>
        {shouldDisplayDrag ? (
          <Draggable
            key={segmentationIndex + 1}
            draggableId={'id' + segmentationIndex + 1}
            index={segmentationIndex}
            dra
          >
            {(provided, snapshot) => (
              <div
                className={'drag-segmentation'}
                id={'addEventListener'}
                ref={provided.innerRef}
                {...provided.draggableProps}
                {...provided.dragHandleProps}
                style={getItemStyle(
                  snapshot.isDragging,
                  provided.draggableProps.style
                )}
              >
                <SegmentationItemsList props={props} />
              </div>
            )}
          </Draggable>
        ) : (
          <SegmentationItemsList props={props} />
        )}
      </Fragment>
    );
  };

  const setLabelChangeButton = currentSegItem => {
    let className = 'seg-edit-button';

    if (currentSegItem.modality !== 'RTSTRUCT') {
      const activeLabelmapIndex = getActiveLabelmapIndex();
      if (activeLabelmapIndex && activeLabelmapIndex > 0) {
        className = 'seg-edit-disabled-button';
      }
    }

    return `popover__title material-icons edit-icon-size-toolbar icon-color ${className}`;
  };

  const setEditButton = currentSegItem => {
    let className = 'seg-edit-button';
    const {
      activeSegmentationDisplaySet,
      activeLabelmapIndex,
    } = getSelectedSegmentationData(currentSegItem);
    if (activeLabelmapIndex && activeSegmentationDisplaySet?.labelmapIndex) {
      if (activeLabelmapIndex === activeSegmentationDisplaySet.labelmapIndex) {
        className = 'seg-edit-inprogress-button';
      } else if (
        activeLabelmapIndex !== activeSegmentationDisplaySet.labelmapIndex &&
        activeLabelmapIndex > 0
      ) {
        className = 'seg-edit-disabled-button';
      }
    }
    return `popover__title material-icons edit-icon-size-toolbar icon-color ${className}`;
  };

  function onLoadMaskClick() {
    runSegmentationChangeCommand(loadMaskButton, {}, commandsManager);
  }

  const loadSegmentChooser = button => {
    const activeLabelmapIndex = getActiveLabelmapIndex();
    const { id, label, icon } = button;

    return (
      <ToolbarButton
        key={id}
        label={label}
        icon={icon}
        disabled={activeLabelmapIndex > 0}
        onClick={() => {
          OverlayObserver.setToggleState();
          onLoadMaskClick();
        }}
      />
    );
  };

  function getOverlayHeight() {
    let maskItemsLength = volumetricMaskArray.length + rtMaskArray.length;
    if (!maskItemsLength) return '200px';
    if (maskItemsLength < 4) return 100 * (maskItemsLength + 1) + 'px';
    return '400px';
  }

  const updateMaskEditingStatusInSegmentationData = (
    selectedSegmentItem,
    segmentationIndex
  ) => {
    let editingSegment = { ...selectedSegmentItem };
    editingSegment.isEditing = !editingSegment.isEditing;
    let newMaskArray = [...volumetricMaskArray];
    newMaskArray[segmentationIndex] = editingSegment;
    setVolumetricMaskArray(newMaskArray);
    saveSegmentationData(
      newMaskArray,
      activeReferenceUID,
      selectedSegmentItem.modality
    );
  };

  const onSaveOrExitEditSegmentation = (
    selectedSegmentItem,
    segmentationIndex,
    shouldSave
  ) => {
    if (shouldSave) {
      commandsManager.runCommand('saveSegments', { saveNewSegment: false });
    }
    exitEditSegmentationMode(selectedSegmentItem);
    updateMaskEditingStatusInSegmentationData(
      selectedSegmentItem,
      segmentationIndex
    );
  };

  const toolbarMenuOverlay = () => {
    let height = getOverlayHeight();
    return (
      <Tooltip
        placement="bottom"
        className={classnames(
          'tooltip-toolbar-overlay segmentationDialog',
          className
        )}
        id={`${Math.random()}_tooltip-toolbar-overlay}`}
        style={{ height }}
      >
        <div
          className={classnames(
            'seg-scrollable-outerdiv scrollableSegmentation',
            className
          )}
        >
          <ScrollableArea>
            <div className={'seg-scrollable-innerdiv'}>
              <div className={'load-mask-category'}>
                Volumetric Files (NIfTI, MHD, etc.)
              </div>
              <DragDropContext onDragEnd={handleVolumetricFileOverlayOnDragEnd}>
                <Droppable droppableId="volumetricSegmentations">
                  {provided => (
                    <div
                      className="segmentations"
                      {...provided.droppableProps}
                      ref={provided.innerRef}
                    >
                      {volumetricMaskArray.map((item, index) => {
                        return (
                          <SegmentationLayerItem
                            key={index}
                            value={100}
                            step={1}
                            min={0}
                            max={100}
                            segmentationIndex={index}
                            item={item}
                            maskArray={volumetricMaskArray}
                            setMaskArray={setVolumetricMaskArray}
                          />
                        );
                      })}
                      {provided.placeholder}
                    </div>
                  )}
                </Droppable>
              </DragDropContext>
              <div className={'load-mask-category'}>DICOM Files</div>
              <DragDropContext onDragEnd={handleRTOverlayOnDragEnd}>
                <Droppable droppableId="rtSegmentations">
                  {provided => (
                    <div
                      className="segmentations"
                      {...provided.droppableProps}
                      ref={provided.innerRef}
                    >
                      {rtMaskArray.map((item, index) => {
                        return (
                          <SegmentationLayerItem
                            key={index}
                            value={100}
                            step={1}
                            min={0}
                            max={100}
                            segmentationIndex={index}
                            item={item}
                            maskArray={rtMaskArray}
                            setMaskArray={setRTMaskArray}
                          />
                        );
                      })}
                      {provided.placeholder}
                    </div>
                  )}
                </Droppable>
              </DragDropContext>
            </div>
          </ScrollableArea>
        </div>
        <div>{loadSegmentChooser(loadMaskButton)}</div>
      </Tooltip>
    );
  };

  return (
    <OverlayTrigger
      key="menu-button"
      trigger="click"
      placement="bottom"
      rootClose={true}
      overlay={toolbarMenuOverlay()}
    >
      {toolbarButtonComponent}
    </OverlayTrigger>
  );
}

Segmentation.propTypes = {
  button: PropTypes.object.isRequired,
  activeButtons: PropTypes.array.isRequired,
  isActive: PropTypes.bool,
  className: PropTypes.string,
};

export default Segmentation;
