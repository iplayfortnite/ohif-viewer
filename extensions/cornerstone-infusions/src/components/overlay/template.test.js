import { evaluateTemplate, tokenize } from './template';

describe('template', () => {
  describe('tokenize', () => {
    test('tokenizes an empty string', () => {
      expect(tokenize('')).toEqual([]);
    });

    test('tokenizes a template with no placeholders', () => {
      expect(tokenize('here is a string')).toEqual(['here is a string']);
    });

    test('tokenizes a template with alternating placeholders', () => {
      expect(tokenize('here {verb} a {noun}')).toEqual([
        'here ',
        '{verb}',
        ' a ',
        '{noun}',
      ]);
    });

    test('tokenizes a template with repeating placeholders', () => {
      expect(tokenize('here {verb} {article} string {fin}')).toEqual([
        'here ',
        '{verb}',
        ' ',
        '{article}',
        ' string ',
        '{fin}',
      ]);
    });
  });

  describe('evaluateTemplate', () => {
    test('evaluates an empty template', () => {
      expect(evaluateTemplate('', {})).toEqual([]);
    });

    test('evaluates a literal template', () => {
      expect(evaluateTemplate('this goes right through', {})).toEqual([
        'this goes right through',
      ]);
    });

    test('places empty values for missing data points', () => {
      expect(evaluateTemplate('{width}px', {})).toEqual(['', 'px']);
    });

    test('places values for matching data points', () => {
      expect(
        evaluateTemplate('{width}px {height}px', { width: 10, height: 0 })
      ).toEqual([10, 'px ', 0, 'px']);
    });

    test('processes known values', () => {
      expect(
        evaluateTemplate('WL: {windowCenter} WW: {windowWidth}', {
          windowCenter: 25.123,
          windowWidth: 200.456,
        })
      ).toEqual(['WL: ', '25', ' WW: ', '200']);
    });

    test('skips templates with missing required values', () => {
      expect(
        evaluateTemplate('{modalityPixelValue:required} HU', {
          modalityPixelValue: undefined,
        })
      ).toEqual([]);
    });

    test('skips groups with missing required values', () => {
      expect(
        evaluateTemplate(
          'X: {imageX}px Y: {imageY}px | Value: {modalityPixelValue:required} HU',
          {
            imageX: 25,
            imageY: 50,
          }
        )
      ).toEqual(['X: ', 25, 'px Y: ', 50, 'px ']);
    });
  });
});
