import cornerstone from 'cornerstone-core';
import cornerstoneWADOImageLoader from 'cornerstone-wado-image-loader';
import pick from 'lodash/pick';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import './CustomizedViewportOverlay.styl';
import { formatDA, formatNumberPrecision, formatTM } from './helpers';

import { evaluateTemplate } from './template';
import ToolStateValue from './ToolStateValue';
import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';

const { selectProjectConfig } = FlywheelRedux.selectors;

const { getNumberValue, getValue } = cornerstoneWADOImageLoader.wadors.metaData;

class CustomizedViewportOverlay extends PureComponent {
  static propTypes = {
    imageId: PropTypes.string.isRequired,
    imageIndex: PropTypes.number.isRequired,
    overlayConfig: PropTypes.shape({
      color: PropTypes.string,
      topLeft: PropTypes.arrayOf(PropTypes.string),
      topRight: PropTypes.arrayOf(PropTypes.string),
      bottomRight: PropTypes.arrayOf(PropTypes.string),
      bottomLeft: PropTypes.arrayOf(PropTypes.string),
    }),
    scale: PropTypes.number.isRequired,
    stackSize: PropTypes.number.isRequired,
    windowCenter: PropTypes.number.isRequired,
    windowWidth: PropTypes.number.isRequired,
  };

  getData() {
    const { imageId } = this.props;
    const data = {};

    Object.assign(
      data,
      pick(this.props, [
        'imageIndex',
        'scale',
        'stackSize',
        'windowWidth',
        'windowCenter',
      ])
    );

    const studyMetadata =
      cornerstone.metaData.get('generalStudyModule', imageId) || {};
    Object.assign(
      data,
      pick(studyMetadata, ['studyDate', 'studyTime', 'studyDescription'])
    );

    const patientModule =
      cornerstone.metaData.get('patientModule', imageId) || {};
    Object.assign(data, pick(patientModule, ['patientId', 'patientName']));

    const patientStudyModule =
      cornerstone.metaData.get('patientStudyModule', imageId) || {};
    Object.assign(
      data,
      pick(patientStudyModule, ['patientAge', 'patientSize', 'patientWeight'])
    );

    const seriesMetadata =
      cornerstone.metaData.get('generalSeriesModule', imageId) || {};
    Object.assign(
      data,
      pick(seriesMetadata, [
        'modality',
        'seriesNumber',
        'seriesDescription',
        'studyInstanceUID',
        'seriesInstanceUID',
      ])
    );

    const generalImageModule =
      cornerstone.metaData.get('generalImageModule', imageId) || {};
    Object.assign(data, pick(generalImageModule, ['instanceNumber']));

    const cineModule = cornerstone.metaData.get('cineModule', imageId) || {};
    Object.assign(data, pick(cineModule, ['frameTime']));

    const imagePlaneModule =
      cornerstone.metaData.get('imagePlaneModule', imageId) || {};
    Object.assign(
      data,
      pick(imagePlaneModule, [
        'frameOfReferenceUID',
        'pixelSpacing',
        'rowPixelSpacing',
        'columnPixelSpacing',
        'rows',
        'columns',
        'sliceThickness',
        'sliceLocation',
        'imagePositionPatient',
        'imageOrientationPatient',
      ])
    );

    const imagePixelModule =
      cornerstone.metaData.get('imagePixelModule', imageId) || {};
    Object.assign(
      data,
      pick(imagePixelModule, [
        'samplesPerPixel',
        'photometricInterpretation',
        'bitsAllocated',
        'bitsStored',
        'highBit',
        'pixelRepresentation',
        'planarConfiguration',
        'pixelAspectRatio',
        'smallestPixelValue',
        'largestPixelValue',
        'redPaletteColorLookupTableDescriptor',
        'greenPaletteColorLookupTableDescriptor',
        'bluePaletteColorLookupTableDescriptor',
        'redPaletteColorLookupTableData',
        'greenPaletteColorLookupTableData',
        'bluePaletteColorLookupTableData',
      ])
    );
    const sopCommonModule =
      cornerstone.metaData.get('sopCommonModule', imageId) || {};
    Object.assign(
      data,
      pick(sopCommonModule, ['sopClassUID', 'sopInstanceUID'])
    );
    const modalityLutModule =
      cornerstone.metaData.get('modalityLutModule', imageId) || {};
    Object.assign(
      data,
      pick(modalityLutModule, ['rescaleIntercept', 'rescaleSlope'])
    );

    // instance tags
    const tags =
      cornerstoneWADOImageLoader.wadors.metaDataManager.get(imageId) || {};

    data.acquisitionDate = formatDA(getValue(tags['00080022']));
    data.acquisitionTime = formatTM(getValue(tags['00080032']));
    data.repetitionTime = getNumberValue(tags['00180080']);
    data.echoTime = getNumberValue(tags['00180081']);
    data.magneticFieldStrength = getNumberValue(tags['00180087']);
    data.protocolName = getValue(tags['00181030']);
    data.laterality = getValue(tags['00200060']);
    data.lossyImageCompression = getValue(tags['00282110']);
    data.lossyImageCompressionRatio = getNumberValue(tags['00282112)']);
    data.lossyImageCompressionMethod = getValue(tags['00282114']);
    data.accessionNumber = getNumberValue(tags['00281102']);

    // derived values
    data.zoomPercentage = formatNumberPrecision(data.scale * 100, 0);
    data.frameRate = formatNumberPrecision(1000 / data.frameTime, 1);
    data.compression = getCompression(imageId);
    data.wwwc = `W: ${formatNumberPrecision(
      data.windowWidth,
      2
    )} L: ${formatNumberPrecision(data.windowCenter, 2)}`;
    data.imageDimensions = `${data.columns} x ${data.rows}`;

    if (data.imagePositionPatient) {
      data.imagePositionPatient = `${formatNumberPrecision(
        data.imagePositionPatient?.[0],
        1
      )},  ${formatNumberPrecision(
        data.imagePositionPatient?.[1],
        1
      )},  ${formatNumberPrecision(data.imagePositionPatient?.[2], 1)}`;
    }

    if (data.imageOrientationPatient) {
      data.imageOrientationPatient = `${formatNumberPrecision(
        data.imageOrientationPatient?.[0],
        1
      )},  ${formatNumberPrecision(
        data.imageOrientationPatient?.[1],
        1
      )},  ${formatNumberPrecision(
        data.imageOrientationPatient?.[2],
        1
      )},  ${formatNumberPrecision(
        data.imageOrientationPatient?.[3],
        1
      )},${formatNumberPrecision(
        data.imageOrientationPatient?.[4],
        1
      )},${formatNumberPrecision(data.imageOrientationPatient?.[5], 1)}`;
    }

    data.imageX = (
      <ToolStateValue key="imageX" attribute="imageX" imageId={imageId} />
    );
    data.imageY = (
      <ToolStateValue key="imageY" attribute="imageY" imageId={imageId} />
    );
    data.pixelValue = (
      <ToolStateValue
        key="pixelValue"
        attribute="pixelValue"
        imageId={imageId}
      />
    );
    data.modalityPixelValue =
      data.modality === 'CT' ? (
        <ToolStateValue
          key="modalityPixelValue"
          attribute="modalityPixelValue"
          imageId={imageId}
        />
      ) : null;
    data.rotation = (
      <ToolStateValue key="rotation" attribute="rotation" imageId={imageId} />
    );

    return data;
  }

  render() {
    const { imageId, overlayConfig } = this.props;
    if (!imageId) {
      return null;
    }

    const config = { ...defaultOverlayConfig, ...overlayConfig };
    const data = this.getData();

    const style = {};
    if (config.color) {
      style.color = config.color;
    }

    return (
      <div className="CustomizedViewportOverlay" style={style}>
        <div className="top overlay-element">
          <div className="overlay-content">
            <div className="top-left">
              {config.topLeft.map(template => (
                <div key={template}>{evaluateTemplate(template, data)}</div>
              ))}
            </div>
            <div className="top-right">
              {config.topRight.map(template => (
                <div key={template}>{evaluateTemplate(template, data)}</div>
              ))}
            </div>
          </div>
        </div>
        <div className="bottom overlay-element">
          <div className="overlay-content">
            <div className="bottom-left">
              {config.bottomLeft.map(template => (
                <div key={template}>{evaluateTemplate(template, data)}</div>
              ))}
            </div>
            <div className="bottom-right">
              {config.bottomRight.map(template => (
                <div key={template}>{evaluateTemplate(template, data)}</div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const defaultOverlayConfig = {
  topLeft: [
    'Image Size: {rows:required} × {columns:required}',
    'WL: {windowCenter:required} WW: {windowWidth:required}',
    'X: {imageX}px Y: {imageY}px | Value: {modalityPixelValue:required} HU',
    'Thickness: {sliceThickness:required} mm Location: {sliceLocation:required} mm',
  ],
  topRight: [
    '{patientId}',
    '{patientName}',
    '{studyDescription}',
    '{seriesDescription}',
  ],
  bottomRight: [
    '{echoTime:required}:{repetitionTime:required}',
    '{magneticFieldStrength:required}',
    '{acquisitionDate} {acquisitionTime}',
    '{protocolName}',
  ],
  bottomLeft: [
    'Zoom: {zoomPercentage:required}%',
    'Im: {imageIndex:required}/{stackSize:required}| Series: {seriesNumber:required}',
  ],
};

function getCompression(imageId) {
  const generalImageModule =
    cornerstone.metaData.get('generalImageModule', imageId) || {};
  const {
    lossyImageCompression,
    lossyImageCompressionRatio,
    lossyImageCompressionMethod,
  } = generalImageModule;

  if (lossyImageCompression === '01' && lossyImageCompressionRatio !== '') {
    const compressionMethod = lossyImageCompressionMethod || 'Lossy: ';
    const compressionRatio = formatNumberPrecision(
      lossyImageCompressionRatio,
      2
    );
    return compressionMethod + compressionRatio + ' : 1';
  }

  return 'Lossless / Uncompressed';
}

const mapStateToProps = state => {
  const projectConfig = selectProjectConfig(state);
  return {
    overlayConfig: projectConfig && projectConfig.overlay,
  };
};

export default connect(mapStateToProps)(CustomizedViewportOverlay);
