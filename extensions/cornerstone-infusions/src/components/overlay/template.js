import isNil from 'lodash/isNil';

import {
  formatPN,
  formatDA,
  formatNumberPrecision,
  formatTM,
  isValidNumber,
} from './helpers';

const identity = value => value;

const templateMapping = {
  frameRate: frameRate =>
    frameRate >= 0 ? formatNumberPrecision(frameRate, 2) : '',
  patientName: formatPN,
  seriesNumber: seriesNumber => (seriesNumber >= 0 ? seriesNumber : ''),
  sliceLocation: sliceLocation =>
    isValidNumber(sliceLocation) ? formatNumberPrecision(sliceLocation, 2) : '',
  sliceThickness: sliceThickness =>
    sliceThickness ? formatNumberPrecision(sliceThickness, 2) : '',
  studyDate: formatDA,
  studyTime: formatTM,
  windowCenter: windowCenter => formatNumberPrecision(windowCenter, 2),
  windowWidth: windowWidth => formatNumberPrecision(windowWidth, 2),
};

export function evaluateTemplate(template, data) {
  return template.split('|').reduce((combined, group) => {
    let valid = true;
    const elements = tokenize(group).map(token => {
      if (!(token.startsWith('{') && token.endsWith('}'))) {
        return token;
      }
      const [key, required] = token.slice(1, -1).split(':');
      const fn = templateMapping[key] || identity;
      const raw = data[key];
      const value = isNil(raw) ? '' : fn(raw);
      if (required && !value) {
        valid = false;
      }
      return isNil(value) ? '' : value;
    });
    return valid ? combined.concat(elements) : combined;
  }, []);
}

export function tokenize(template) {
  const tokens = [];
  let word = [];
  for (const chr of Array.from(template)) {
    if (chr === '{') {
      tokens.push(word.join(''));
      word = [];
    }
    word.push(chr);
    if (chr === '}') {
      tokens.push(word.join(''));
      word = [];
    }
  }
  tokens.push(word.join(''));
  return tokens.filter(word => word);
}
