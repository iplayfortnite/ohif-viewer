import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import isNil from 'lodash/isNil';
import PropTypes from 'prop-types';
import React, { Component } from 'react';

class ToolStateValue extends Component {
  static propTypes = {
    attribute: PropTypes.oneOf([
      'imageX',
      'imageY',
      'pixelValue',
      'modalityPixelValue',
      'rotation',
    ]).isRequired,
    imageId: PropTypes.string.isRequired,
  };

  state = {
    value: 0,
  };

  constructor(props) {
    super(props);
    this.element = undefined;
    this.bindToElement();
  }

  componentDidUpdate(prevProps) {
    if (this.props.imageId !== prevProps.imageId) {
      this.bindToElement();
    }
  }

  componentWillUnmount() {
    this.unbindFromElement();
  }

  bindToElement() {
    this.unbindFromElement();
    const enabledElement = cornerstone
      .getEnabledElements()
      .find(
        element => element.image && element.image.imageId === this.props.imageId
      );
    if (enabledElement) {
      this.element = enabledElement.element;
      this.element.addEventListener(
        cornerstoneTools.EVENTS.MOUSE_MOVE,
        this.onMouseMove
      );
      this.element.addEventListener(
        cornerstone.EVENTS.IMAGE_RENDERED,
        this.onImageRendered
      );
    }
  }

  unbindFromElement() {
    if (this.element) {
      this.element.removeEventListener(
        cornerstoneTools.EVENTS.MOUSE_MOVE,
        this.onMouseMove
      );
      this.element.removeEventListener(
        cornerstone.EVENTS.IMAGE_RENDERED,
        this.onImageRendered
      );
    }
  }

  onMouseMove = event => {
    const { attribute } = this.props;
    const { image } = event.detail.currentPoints;
    let value;
    switch (attribute) {
      case 'pixelValue':
      case 'modalityPixelValue':
        value = cornerstone.getStoredPixels(
          this.element,
          image.x,
          image.y,
          1,
          1
        )[0];
        if (attribute === 'modalityPixelValue') {
          // https://github.com/cornerstonejs/cornerstoneTools/issues/278
          const { intercept, slope } = event.detail.image;
          value = value * slope + intercept;
        }
        break;
      case 'imageX':
      case 'imageY':
        value = image[attribute === 'imageY' ? 'y' : 'x'].toFixed(0);
        break;
    }
    if (!isNil(value)) {
      this.setState({ value: value || 0 });
    }
  };

  onImageRendered = event => {
    const { attribute } = this.props;
    const { viewport } = event.detail;
    let value;
    switch (attribute) {
      case 'rotation':
        value = viewport.rotation;
        break;
    }
    if (!isNil(value)) {
      this.setState({ value: value || 0 });
    }
  };

  render() {
    return this.state.value;
  }
}

export default ToolStateValue;
