import TagBrowser from './components/tags/TagBrowser';
import Components from './components';
import initExtension from './init';
import commandsModule from './commandsModule';
import toolbarModule from './toolbarModule';
import {
  getLinkButtonStatus,
  cornerstoneUtils,
  updateOtherViewports,
  scrollToCenter,
  editSegmentationMask,
  exitEditSegmentationMode,
  getActiveLabelmapIndex,
  isSelectedSegmentMaskInEditing,
  getSelectedSegmentationData,
  setSegmentToolActive,
  getActiveReferenceUID,
  getActiveSegmentationDisplaySet,
} from './utils';
import { CrosshairsTool } from './tools';
import store from '@ohif/viewer/src/store';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';

const { selectProjectConfig } = FlywheelRedux.selectors;

export default {
  id: 'cornerstone-infusions-extension',
  preRegistration({ commandsManager, servicesManager, appConfig }) {
    initExtension({ commandsManager, servicesManager, appConfig });
  },
  getCommandsModule({
    commandsManager,
    servicesManager,
    hotkeysManager,
    appConfig,
  }) {
    return commandsModule({
      commandsManager,
      servicesManager,
      hotkeysManager,
      appConfig,
    });
  },
  getToolbarModule({
    commandsManager,
    servicesManager,
    hotkeysManager,
    appConfig,
  }) {
    return toolbarModule({
      commandsManager,
      servicesManager,
      hotkeysManager,
      appConfig,
    });
  },
  getPanelModule() {
    return panelModule;
  },
};

const panelModule = {
  menuOptions: [
    {
      icon: 'exclamation-circle',
      label: 'Tags',
      target: 'tags-panel',
      context: ['ACTIVE_VIEWPORT::CORNERSTONE'],
      isDisabled: studies => {
        const config = selectProjectConfig(store.getState());
        if (!studies || config?.toolbar?.hideTagsPanel) {
          return true;
        }

        return FlywheelCommonUtils.isCurrentWebImage();
      },
    },
  ],
  components: [
    {
      id: 'tags-panel',
      component: TagBrowser,
    },
  ],
  defaultContext: ['VIEWER'],
};

const linkedToolUtils = { getLinkButtonStatus };

export {
  Components,
  linkedToolUtils,
  cornerstoneUtils,
  CrosshairsTool,
  scrollToCenter,
  updateOtherViewports,
  editSegmentationMask,
  exitEditSegmentationMode,
  getActiveLabelmapIndex,
  isSelectedSegmentMaskInEditing,
  getSelectedSegmentationData,
  setSegmentToolActive,
  getActiveReferenceUID,
  getActiveSegmentationDisplaySet,
};
