import cornerstoneTools from 'cornerstone-tools';
import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import { utils } from '@ohif/core';

const { setActiveTool, setSmartCTRange } = FlywheelCommonRedux.actions;
const {
  segmentationTools,
  selectAllowedActiveTools,
  selectActiveViewportIndex,
} = FlywheelCommonRedux.selectors;

const { setters, getters } = cornerstoneTools.getModule('segmentation');
const { studyMetadataManager } = utils;

const getSelectedSegmentationData = selectedSegmentData => {
  const state = store.getState();
  const activeSegmentationDisplaySet = getActiveSegmentationDisplaySet(
    selectedSegmentData
  );
  const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
    state.viewports.activeViewportIndex
  );
  let activeLabelmap = {};
  if (activeSegmentationDisplaySet?.labelmapIndex) {
    const { labelmaps3D } = getters.labelmaps3D(element);
    if (labelmaps3D) {
      activeLabelmap = getters.labelmap3D(
        element,
        activeSegmentationDisplaySet.labelmapIndex
      );
    }
  }
  const firstImageId = getFirstImageId();
  const activeLabelmapIndex = getters.activeLabelmapIndex(element);

  return {
    activeLabelmap,
    activeSegmentationDisplaySet,
    firstImageId,
    element,
    activeLabelmapIndex,
  };
};

const getActiveLabelmapIndex = () => {
  const state = store.getState();
  const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
    state.viewports.activeViewportIndex
  );
  const activeLabelmapIndex = getters.activeLabelmapIndex(element);

  return activeLabelmapIndex;
};

const editSegmentationMask = selectedSegmentData => {
  const {
    activeLabelmapIndex,
    element,
    activeSegmentationDisplaySet,
  } = getSelectedSegmentationData(selectedSegmentData);

  if (activeLabelmapIndex === 0) {
    setters.activeLabelmapIndex(
      element,
      activeSegmentationDisplaySet.labelmapIndex
    );
    const activeSegmentIndex = getters.activeSegmentIndex(
      element,
      activeSegmentationDisplaySet.labelmapIndex
    );
    const smartCTRangeList = FlywheelCommonUtils.getSmartCTRangeList();
    const selectedSegmentationColor =
      smartCTRangeList?.[activeSegmentIndex - 1];

    if (selectedSegmentationColor) {
      store.dispatch(setSmartCTRange(selectedSegmentationColor));
    }
  }
};

const exitEditSegmentationMode = selectedSegmentData => {
  const {
    activeLabelmapIndex,
    element,
    activeSegmentationDisplaySet,
  } = getSelectedSegmentationData(selectedSegmentData);

  if (activeLabelmapIndex === activeSegmentationDisplaySet.labelmapIndex) {
    setters.activeLabelmapIndex(element, 0);
  }
};

const getActiveSegmentationDisplaySet = selectedSegmentData => {
  const activeViewport = getActiveViewport();
  const studyInstanceUid = activeViewport.StudyInstanceUID;
  const studyMetadata = studyMetadataManager.get(studyInstanceUid);
  const derivedDisplaySets = studyMetadata.getDerivedDatasets({
    referencedSeriesInstanceUID: selectedSegmentData.referenceUID,
    Modality: selectedSegmentData.modality,
  });
  const segDisplayset = getDerivedDisplaySet(
    derivedDisplaySets,
    selectedSegmentData.displaySetInstanceUIDs
  );

  return segDisplayset;
};

const isSelectedSegmentMaskInEditing = selectedSegmentData => {
  const {
    activeLabelmapIndex,
    activeSegmentationDisplaySet,
  } = getSelectedSegmentationData(selectedSegmentData);

  const isSelectedMaskInEditing =
    activeLabelmapIndex > 0 &&
    activeSegmentationDisplaySet.labelmapIndex === activeLabelmapIndex;

  return isSelectedMaskInEditing;
};

const getActiveViewport = () => {
  const state = store.getState();
  let viewport = null;

  if (state.viewports) {
    const { viewportSpecificData, activeViewportIndex } = state.viewports;
    if (viewportSpecificData[activeViewportIndex]) {
      viewport = viewportSpecificData[activeViewportIndex];
    }
  }

  return viewport;
};

const getDerivedDisplaySet = (derivedDisplaySets, displaySetInstanceUIDs) => {
  let displaySet = {};
  if (!derivedDisplaySets.length) {
    return displaySet;
  }
  derivedDisplaySets.every(segDisplaySet => {
    if (
      !segDisplaySet.isDeleted &&
      displaySetInstanceUIDs?.includes(segDisplaySet.displaySetInstanceUID)
    ) {
      displaySet = segDisplaySet;
      return false;
    }
    return true;
  });
  return displaySet;
};

const setSegmentToolActive = () => {
  const state = store.getState();
  const activeTool = state.infusions?.activeTool;
  const isAnySegmentationToolActive = segmentationTools.includes(activeTool);

  if (!isAnySegmentationToolActive) {
    store.dispatch(setActiveTool(segmentationTools[0]));
  }
};

const getFirstImageId = () => {
  const { StudyInstanceUID, displaySetInstanceUID } = getActiveViewport();
  const studyMetadata = studyMetadataManager.get(StudyInstanceUID);
  return studyMetadata.getFirstImageId(displaySetInstanceUID);
};

function getActiveReferenceUID() {
  const state = store.getState();
  const currentVolumetricImage =
    state.flywheel?.currentNiftis[0] || state.flywheel?.currentMetaImage;
  let referenceUID = null;
  if (state.viewports) {
    const { viewportSpecificData, activeViewportIndex } = state.viewports;
    if (viewportSpecificData[activeViewportIndex]) {
      referenceUID = currentVolumetricImage
        ? viewportSpecificData[activeViewportIndex].StudyInstanceUID
        : viewportSpecificData[activeViewportIndex].SeriesInstanceUID;
    }
  }
  return referenceUID;
}

export {
  editSegmentationMask,
  exitEditSegmentationMode,
  getActiveLabelmapIndex,
  isSelectedSegmentMaskInEditing,
  getSelectedSegmentationData,
  setSegmentToolActive,
  getActiveReferenceUID,
  getActiveSegmentationDisplaySet,
};
