import cornerstone from 'cornerstone-core';
import cornerstoneMath from 'cornerstone-math';
import csTools from 'cornerstone-tools';
import groupBy from 'lodash/groupBy';
import _ from 'lodash';
import { vec3 } from 'gl-matrix';
import { utils } from '@ohif/core';
import store from '@ohif/viewer/src/store';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';

const imagePointToPatientPoint = csTools.import(
  'util/imagePointToPatientPoint'
);
const convertToVector3 = csTools.import('util/convertToVector3');
const { studyMetadataManager } = utils;
const { cornerstoneUtils } = FlywheelCommonUtils;

export function getStackData(element) {
  const enabledElement = cornerstone.getEnabledElement(element);
  const stackManager = enabledElement.toolStateManager.get(element, 'stack');
  return stackManager.data[0];
}

/**
 * Get ImageOrientationPatient with 2 decimals of precision for each component
 */
export function getOrientation(element) {
  const enabledElement = cornerstone.getEnabledElement(element);
  const imageId =
    enabledElement && enabledElement.image && enabledElement.image.imageId;
  if (!imageId) {
    return undefined;
  }
  const metadata = getPlaneMetadata(imageId);
  if (!metadata || !metadata.columnCosines || !metadata.rowCosines) {
    return undefined;
  }
  return [
    metadata.columnCosines.x.toFixed(2),
    metadata.columnCosines.y.toFixed(2),
    metadata.columnCosines.z.toFixed(2),
    metadata.rowCosines.x.toFixed(2),
    metadata.rowCosines.y.toFixed(2),
    metadata.rowCosines.z.toFixed(2),
  ].join('\\');
}

export function checkImagesHaveSamePosition(displaySet) {
  const imageSet = displaySet.images;
  const firstImageId = imageSet[0].getImageId();
  const {
    ImagePositionPatient,
    ImageOrientationPatient,
  } = cornerstone.metaData.get('instance', firstImageId);
  for (let i = 1; i < imageSet.length; i++) {
    const instance = cornerstone.metaData.get(
      'instance',
      imageSet[i].getImageId()
    );
    if (
      instance.ImagePositionPatient === ImagePositionPatient &&
      instance.ImageOrientationPatient === ImageOrientationPatient
    ) {
      return true;
    }
  }
  return false;
}

/**
 * Group elements by cosine values with 2 decimals of precision.
 */
export function bucketByOrientation(elements) {
  const state = store.getState();
  const projectConfig = state.flywheel.projectConfig;
  const linkUsingRounding = projectConfig?.allowSynchWithAngle === false;
  const thresholdAngleInDegree = projectConfig?.thresholdAngle || 5;
  const thresholdAngle = thresholdAngleInDegree * (Math.PI / 180);
  let bucketedElements = [];
  if (linkUsingRounding) {
    const metaElements = elements.map((element, index) => {
      return {
        element,
        bucket: getOrientation(element) || index,
      };
    });
    bucketedElements = groupBy(metaElements, 'bucket');
    return Object.values(bucketedElements).map(bucket =>
      bucket.map(el => el.element)
    );
  } else {
    // Check all image in series within threashold angle to be eligible to have a bucket.
    const studies = studyMetadataManager.all();
    elements.map(element => {
      const enabledElement = cornerstone.getEnabledElement(element);
      const imageId =
        enabledElement &&
        enabledElement.image &&
        (enabledElement.image.imageId || enabledElement.image.getImageId?.());
      const imagePlaneModule = cornerstone.metaData.get(
        'imagePlaneModule',
        imageId
      );
      const instance = cornerstone.metaData.get('instance', imageId);
      const study = studies.find(
        study => study.studyInstanceUID === instance.StudyInstanceUID
      );
      const displaySet = study._displaySets.find(
        ds => ds.SeriesInstanceUID === instance.SeriesInstanceUID
      );

      if (imagePlaneModule?.imageOrientationPatient) {
        // Check whether images in a single viewport has different orientations
        const haveDifferentOrientation = checkImagesHasDifferentOrientation(
          displaySet,
          thresholdAngle
        );
        const bucket = {
          element: [element],
          orientation: cornerstoneUtils.getNormalAxisOrientation(imageId),
          uniqueBucket: haveDifferentOrientation,
          syncFunctions: checkImagesHaveSamePosition(displaySet)
            ? { scroll: csTools.stackImageIndexSynchronizer }
            : {},
        };
        if (!bucketedElements.length) {
          bucketedElements.push(bucket);
        } else {
          let newBucket = true;
          bucketedElements.forEach(bucket => {
            if (!bucket.uniqueBucket) {
              const inThresholdAngle = checkThresholdAngle(
                bucket.element,
                element,
                thresholdAngle
              );
              if (
                inThresholdAngle &&
                cornerstoneUtils.getNormalAxisOrientation(imageId) ===
                  bucket.orientation
              ) {
                newBucket = false;
                bucket.element.push(element);
              }
            }
          });

          if (newBucket) {
            bucketedElements.push(bucket);
          }
        }
      } else {
        if (!bucketedElements.length) {
          bucketedElements.push({
            element: [element],
            orientation: cornerstoneUtils.getNormalAxisOrientation(imageId),
            syncFunctions: {
              scroll: csTools.stackImageIndexSynchronizer,
            },
          });
        } else {
          bucketedElements.forEach(bucket => {
            bucket.element.push(element);
          });
        }
      }
    });
    return bucketedElements;
  }
}

/**
 * Check whether images in a display set has different orientation.
 * @param {*} displaySet
 * @param {*} thresholdAngle
 * @returns
 */
function checkImagesHasDifferentOrientation(displaySet, thresholdAngle) {
  const imageSet = displaySet.images;
  if (imageSet.length < 2) {
    return false;
  }
  const firstImageInIMageSet = imageSet[0];
  const { columnCosineVector, rowCosineVector } = getCosineVectors(
    firstImageInIMageSet._imageId || firstImageInIMageSet.getImageId?.()
  );
  for (let i = 1; i < imageSet.length; i++) {
    const currentImageInIMageSet = imageSet[i];
    const cosineVectors2 = getCosineVectors(
      currentImageInIMageSet._imageId || currentImageInIMageSet.getImageId?.()
    );
    const columnCosineVector2 = cosineVectors2.columnCosineVector;
    const rowCosineVector2 = cosineVectors2.rowCosineVector;
    const inThresholdAngle = !checkInThresholdAngle(
      columnCosineVector,
      columnCosineVector2,
      thresholdAngle,
      rowCosineVector,
      rowCosineVector2
    );
    if (inThresholdAngle) {
      return true;
    }
  }
  return false;
}

/**
 * Check whether angle between image cosine vector keep a threshold angle with any elements in a bucket.
 * @param {*} bucket
 * @param {*} element
 * @param {*} thresholdAngle
 * @returns
 */
function checkThresholdAngle(bucket, element, thresholdAngle) {
  let inThresholdAngle = false;
  const enabledElement = cornerstone.getEnabledElement(element);
  const imageId = enabledElement?.image?.imageId;
  if (!imageId) {
    return undefined;
  }
  const { columnCosineVector, rowCosineVector } = getCosineVectors(imageId);
  bucket.map(e => {
    const enabledElement2 = cornerstone.getEnabledElement(e);
    const imageId2 = enabledElement2?.image?.imageId;
    if (!imageId2) {
      return undefined;
    }
    const cosineVectors2 = getCosineVectors(imageId2);
    const columnCosineVector2 = cosineVectors2.columnCosineVector;
    const rowCosineVector2 = cosineVectors2.rowCosineVector;
    inThresholdAngle = checkInThresholdAngle(
      columnCosineVector,
      columnCosineVector2,
      thresholdAngle,
      rowCosineVector,
      rowCosineVector2
    );
  });
  return inThresholdAngle;
}

/**
 * Check whether angle between coloumn cosine vectors and row cosine vectors less than threshold angle.
 * @param {*} columnCosineVector
 * @param {*} columnCosineVector2
 * @param {*} thresholdAngle
 * @param {*} rowCosineVector
 * @param {*} rowCosineVector2
 * @returns
 */
function checkInThresholdAngle(
  columnCosineVector,
  columnCosineVector2,
  thresholdAngle,
  rowCosineVector,
  rowCosineVector2
) {
  if (
    vec3.angle(columnCosineVector, columnCosineVector2) < thresholdAngle &&
    vec3.angle(rowCosineVector, rowCosineVector2) < thresholdAngle
  ) {
    return true;
  }
  return false;
}

function getCosineVectors(imageId) {
  const metadata = getPlaneMetadata(imageId);
  if (!metadata || !metadata.columnCosines || !metadata.rowCosines) {
    return undefined;
  }
  const columnCosineVector = [
    metadata.columnCosines.x,
    metadata.columnCosines.y,
    metadata.columnCosines.z,
  ];
  const rowCosineVector = [
    metadata.rowCosines.x,
    metadata.rowCosines.y,
    metadata.rowCosines.z,
  ];
  return {
    columnCosineVector,
    rowCosineVector,
  };
}

/**
 * Determine the axis based on cornerstone element passed.
 */
export function getNormalAxis(element) {
  const enabledElement = cornerstone.getEnabledElement(element);
  const imageId =
    enabledElement && enabledElement.image && enabledElement.image.imageId;
  if (!imageId) {
    return undefined;
  }

  return getNormalAxisFromImageId(imageId);
}

export function getPatientPosition(enabledElement, isActiveImageFromStack) {
  let imageId = enabledElement.image.imageId;
  if (isActiveImageFromStack) {
    const toolState = csTools.getToolState(enabledElement.element, 'stack');
    const stackData = toolState.data[0];
    const curIndex = stackData.currentImageIdIndex;
    imageId = stackData?.imageIds[curIndex];
  }
  const imagePlane = cornerstone.metaData.get('imagePlaneModule', imageId);
  if (imagePlane && imagePlane.imagePositionPatient) {
    return imagePointToPatientPoint(
      {
        x: Math.round(imagePlane.columns / 2),
        y: Math.round(imagePlane.rows / 2),
      },
      imagePlane
    );
  }
}

export function getNearestSliceIndex(enabledElement, patientPos) {
  const toolState = csTools.getToolState(enabledElement.element, 'stack');
  const stackData = toolState.data[0];

  return cornerstoneUtils.getNearestSliceFromImagesIds(
    stackData.imageIds,
    patientPos
  );
}
