import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';

const imagePointToPatientPoint = csTools.import(
  'util/imagePointToPatientPoint'
);

// Notify other plugin viewports with the properties in mixed viewports layout
export const updateOtherViewports = (
  commandsManager,
  toolNames,
  activeViewport,
  element,
  isActiveImageFromStack
) => {
  const enabledElement = cornerstone.getEnabledElement(element);
  let propertiesToSync = {};
  let plugin = activeViewport.plugin;
  propertiesToSync.seriesInstanceUID = activeViewport.SeriesInstanceUID;
  propertiesToSync.studyInstanceUID = activeViewport.StudyInstanceUID;
  toolNames.forEach(toolName => {
    if (toolName === 'Wwwc') {
      propertiesToSync.windowLevel = {
        windowCenter: enabledElement.viewport.voi.windowCenter,
        windowWidth: enabledElement.viewport.voi.windowWidth,
      };
    } else if (
      toolName === 'StackScroll' ||
      toolName === 'Crosshairs' ||
      toolName === 'StackScrollMouseWheel'
    ) {
      let imageId = enabledElement.image.imageId;
      if (isActiveImageFromStack) {
        const toolState = cornerstoneTools.getToolState(element, 'stack');
        const stackData = toolState.data[0];
        const curIndex = stackData.currentImageIdIndex;
        imageId = stackData?.imageIds[curIndex];
      }
      const imagePlane = cornerstone.metaData.get('imagePlaneModule', imageId);
      if (imagePlane && imagePlane.imagePositionPatient) {
        propertiesToSync.patientPos = imagePointToPatientPoint(
          {
            x: Math.round(imagePlane.columns / 2),
            y: Math.round(imagePlane.rows / 2),
          },
          imagePlane
        );
      }
    } else if (toolName === 'Zoom' || toolName === 'ZoomMouseWheel') {
      const defaultViewport = cornerstone.getDefaultViewport(
        enabledElement.canvas,
        enabledElement.image
      );
      propertiesToSync.zoom = {
        type: 'relative',
        scale: enabledElement.viewport.scale - defaultViewport.scale,
      };
      propertiesToSync.pan = enabledElement.viewport.translation;
    }
  });
  // Cross viewport sync triggering for cornerstone DICOM viewports only
  commandsManager?.runCommand('crossViewportSync', {
    plugin,
    propertiesToSync,
  });
};
