import { redux } from '@ohif/core';

const { setColormapId: storeAction } = redux.actions;

export default function setColormapId(colormapId, store) {
  const action = storeAction({ colormapId });

  store.dispatch(action);
}
