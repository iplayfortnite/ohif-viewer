import OHIF from '@ohif/core';
import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
import {
  Redux as FlywheelRedux,
  Utils as FlywheelUtils,
} from '@flywheel/extension-flywheel';
import { resetLabellingAndContextMenuAction } from '@ohif/viewer/src/appExtensions/MeasurementsPanel/actions';

const { selectActiveProtocolStore } = FlywheelRedux.selectors;

const { setLayout, setAllViewportsSpecificData } = OHIF.redux.actions;
const { setProtocolLayout, selectViewports } = FlywheelCommonRedux.selectors;
const { resetLayoutToHP } = FlywheelUtils;

function _setupViewports(selectedLayout, state, store) {
  // Get the largest number of columns
  const numColumns = selectedLayout.viewports.reduce(
    (biggest, viewport) =>
      viewport.length > biggest ? viewport.length : biggest,
    0
  );
  // Build viewport list for setLayout to work
  const currentViewports = selectViewports(state);
  let viewports = [];
  for (let i = 0; i < numColumns * selectedLayout.viewports.length; i++) {
    const viewport = currentViewports[i];
    let plugin = viewport && viewport.plugin;
    // NIFTI TO REVIEW
    if (viewport && viewport.vtk) {
      plugin = 'cornerstone';
    }
    viewports.push({ plugin });
  }
  // This sets the layout, which triggers clearing the selectedLayout in the cornerstone-infusions reducer.
  // We wait for rendering and the store to settle, then set the protocol layout
  store.dispatch(
    setLayout({
      numRows: selectedLayout.viewports.length,
      numColumns,
      viewports,
    })
  );
  setTimeout(() => {
    store.dispatch(setProtocolLayout(selectedLayout));
  }, 0);
}

const commandsModule = ({ servicesManager, commandsManager, appConfig }) => {
  const { store } = appConfig;

  // Retrieve the current cornerstone viewport properties from the element passed
  const getCurrentCornerstoneViewportProperties = (element, displaySet) => {
    let propertiesToSync = null;
    if (element) {
      const toolState = cornerstoneTools.getToolState(element, 'stack');
      const stackData = toolState.data[0];
      const curIndex = stackData.currentImageIdIndex;
      const imagePlane = cornerstone.metaData.get(
        'imagePlaneModule',
        stackData.imageIds[curIndex]
      );
      const enabledElement = cornerstone.getEnabledElement(element);
      const viewport = enabledElement.viewport;
      const oldCanvasWidth = enabledElement.canvas.width;
      const oldCanvasHeight = enabledElement.canvas.height;
      propertiesToSync = {
        seriesInstanceUID: displaySet.SeriesInstanceUID,
        studyInstanceUID: displaySet.StudyInstanceUID,
        patientPos: imagePlane?.imagePositionPatient,
        zoom: {
          type: 'rescale',
          viewportDim: { width: oldCanvasWidth, height: oldCanvasHeight },
          scale: viewport.scale,
        },
        pan: {
          type: 'relative',
          translation: {
            x: viewport.translation.x * viewport.scale,
            y: viewport.translation.y * viewport.scale,
          },
        },
        windowLevel: viewport.voi,
      };
    }
    return propertiesToSync;
  };

  const actions = {
    // TODO OHIF way is to pass necessary object from state instead the whole obj. Check command definition
    toggleProtocolList: state => {
      const { UIDialogService } = servicesManager.services;

      const closeDialog = () => {
        UIDialogService.dismiss({ id: dialogId });
        dialogId = null;
      };

      if (dialogId) {
        return closeDialog();
      }

      const selectCallback = selectedLayout => {
        _setupViewports(selectedLayout, state, store);
        closeDialog();
      };

      dialogId = UIDialogService.create({
        content: ProtocolLayoutOverlay,
        defaultPosition: { x: 600, y: 120 },
        isDraggable: false,
        showOverlay: true,
        contentProps: {
          selectCallback,
        },
      });
    },
    switchProtocolMode: state => {
      const protocolStore = selectActiveProtocolStore(state);
      const viewports = state.viewports || {};
      let activeViewport =
        viewports.layout.viewports[viewports.activeViewportIndex];
      let displaySet =
        viewports.viewportSpecificData[viewports.activeViewportIndex];
      let element = null;
      const plugins = [];
      // Find the cornerstone viewport details matching to active display set series
      if (!activeViewport.plugin?.includes('cornerstone')) {
        viewports.layout.viewports.every((viewport, index) => {
          const currentDisplaySet = viewports.viewportSpecificData[index];
          const plugin = viewport.plugin
            ? viewport.plugin
            : viewport.vtk
            ? 'vtk'
            : '';
          if (!plugins.includes(plugin)) {
            plugins.push(plugin);
          }
          if (
            plugin.includes('cornerstone') &&
            currentDisplaySet.SeriesInstanceUID === displaySet.SeriesInstanceUID
          ) {
            displaySet = currentDisplaySet;
            element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
              index
            );
            return false;
          }
          return true;
        });
      } else {
        element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
          viewports.activeViewportIndex
        );
      }

      const propertiesToSync = getCurrentCornerstoneViewportProperties(
        element,
        displaySet
      );
      // Reset to hanging protocol layout if a valid protocol already applied and if the current layout
      // or the viewport displaySet is different
      if (protocolStore) {
        resetLayoutToHP(
          commandsManager,
          store,
          protocolStore,
          isAlreadyMatching => {
            if (state.ui?.contextMenu) {
              store.dispatch(resetLabellingAndContextMenuAction());
            }
            if (isAlreadyMatching) {
              if (!element && plugins.includes('vtk')) {
                // If all are views other than cornerstone and include vtk js views,
                // then sync the properties from vtk viewport
                commandsManager?.runCommand('exit2dMprView');
                return;
              }
              // Switch to single view with active viewport
              // displaySet(this will be applicable to volumetric image types also)
              const currentDisplaySet = { 0: displaySet };
              const viewport = [
                {
                  height: '100%',
                  width: '100%',
                  plugin: displaySet.plugin || 'cornerstone',
                },
              ];
              store.dispatch(
                setLayout({
                  numRows: 1,
                  numColumns: 1,
                  viewports: viewport,
                })
              );
              setTimeout(() => {
                store.dispatch(setAllViewportsSpecificData(currentDisplaySet));
              }, 0);
            }
            if (propertiesToSync) {
              // Retain the scroll position in HP view after switching to single view.
              setTimeout(() => {
                commandsManager.runCommand('update2DViewport', {
                  viewports,
                  propertiesToSync,
                });
              }, 500);
            }
          }
        );
      }
    },
  };

  const definitions = {
    toggleProtocolList: {
      commandFn: actions.toggleProtocolList,
      storeContexts: ['flywheel', 'viewports'],
      options: {},
    },
    switchProtocolMode: {
      commandFn: actions.switchProtocolMode,
      storeContexts: ['flywheel', 'viewports', 'ui'],
      options: {},
    },
  };

  return {
    actions,
    definitions,
  };
};

export default commandsModule;
