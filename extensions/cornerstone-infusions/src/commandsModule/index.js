import colorMap from './colorMap';
import common from './common';
import crosshairs from './crosshairs';
import linkSeries from './linkSeries';
import protocols from './protocols';
import segmentationFileChooser from './segmentationFileChooser';
import editSegmentationLabel from './editSegmentationLabel';

const infusions = [
  colorMap,
  linkSeries,
  crosshairs,
  common,
  protocols,
  segmentationFileChooser,
  editSegmentationLabel,
];

export default function commandsModule({
  commandsManager,
  servicesManager,
  hotkeysManager,
  appConfig,
}) {
  const { store } = appConfig;
  const commandsModule = infusions.reduce(
    (accumulator, getCommandsModule) => {
      const infusionCommandsModule =
        getCommandsModule({
          commandsManager,
          servicesManager,
          hotkeysManager,
          appConfig,
        }) || [];

      return {
        definitions: {
          ...accumulator.definitions,
          ...infusionCommandsModule.definitions,
        },
        actions: {
          ...accumulator.actions,
          ...infusionCommandsModule.actions,
        },
      };
    },
    { definitions: {} }
  );

  return commandsModule;
}
