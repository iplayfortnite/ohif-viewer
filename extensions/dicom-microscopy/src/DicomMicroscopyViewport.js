import React, { Component } from 'react';
import ReactResizeDetector from 'react-resize-detector';
import debounce from 'lodash.debounce';
import { microscopyEventRegister } from './viewportInstance';
import { microscopyTagValidation } from './utils/utils';
import { AnnotationManager } from './utils/annotationManager';
import { addViewport, removeViewport } from './activeMicroscopyViewports';
import { restoreAnnotationsOnViewport } from './annotations/restoreAnnotation'
import { removeAllOpenLayerROIs } from './utils/roiOperations';
import { globalDicomMicroscopyToolStateManager } from './toolStateManager';

class DicomMicroscopyViewport extends Component {
  state = {
    error: null,
    imageViewers: {},
  };

  viewer = null;

  constructor(props) {
    super(props);

    this.container = React.createRef();

    this.debouncedResize = debounce(() => {
      if (this.viewer) this.viewer.resize();
    }, 100);
  }

  // install the microscopy renderer into the web page.
  // you should only do this once.
  installOpenLayersRenderer(container, displaySet, viewportIndex = 0) {
    const dicomWebClient = displaySet.dicomWebClient;

    const searchInstanceOptions = {
      studyInstanceUID: displaySet.StudyInstanceUID,
      seriesInstanceUID: displaySet.SeriesInstanceUID,
    };

    dicomWebClient
      .searchForInstances(searchInstanceOptions)
      .then(instances => {
        const promises = [];
        for (let i = 0; i < instances.length; i++) {
          const sopInstanceUID = instances[i]['00080018']['Value'][0];

          const retrieveInstanceOptions = {
            studyInstanceUID: displaySet.StudyInstanceUID,
            seriesInstanceUID: displaySet.SeriesInstanceUID,
            sopInstanceUID,
          };

          const promise = dicomWebClient
            .retrieveInstanceMetadata(retrieveInstanceOptions)
            .then(metadata => {
              const ImageType = metadata[0]['00080008']['Value'];
              if (ImageType[2] === 'VOLUME') {
                return metadata[0];
              }
            });
          promises.push(promise);
        }
        return Promise.all(promises);
      })
      .then(async metadata => {
        metadata = metadata.filter(m => m);

        microscopyTagValidation(metadata, this.props);

        const { api, events } = await import(
          /* webpackChunkName: "dicom-microscopy-viewer" */ 'dicom-microscopy-viewer'
        );

        const microscopyViewer = api.VLWholeSlideMicroscopyImageViewer;

        try {
          this.viewer = new microscopyViewer({
            client: dicomWebClient,
            metadata,
            retrieveRendered: false,
            annotationManager: AnnotationManager,
          });
        } catch (error) {
          console.error('[Microscopy Viewer] Failed to load:', error);
          const { UINotificationService } = this.props.servicesManager.services;
          if (UINotificationService) {
            UINotificationService.show({
              title: 'Microscopy Viewport',
              message:
                'Failed to load viewport. Please check that you have hardware acceleration enabled.',
              type: 'error',
            });
          }
        }

        this.viewer.render({ container });

        const imageViewers = { ...this.state.imageViewers };
        imageViewers[displaySet.SeriesInstanceUID] = this.viewer;
        this.setState({ imageViewers });

        addViewport(viewportIndex, this.viewer, container);

        microscopyEventRegister.microscopyEvents = events.EVENTS;
        microscopyEventRegister.bindEvents(viewportIndex);

        restoreAnnotationsOnViewport(viewportIndex);
      });
  }

  componentDidMount() {
    const { displaySet } = this.props.viewportData;

    this.installOpenLayersRenderer(
      this.container.current,
      displaySet,
      this.props.viewportIndex
    );
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.layout === prevProps.layout &&
      this.props.viewportData.displaySet.SeriesInstanceUID ===
        prevProps.viewportData.displaySet.SeriesInstanceUID
    ) {
      return;
    }
    const currentMap = this.viewer?.[
      Object.getOwnPropertySymbols(this.viewer).find(
        x => String(x) === 'Symbol(map)'
      )
    ];
    if (!currentMap) {
      return;
    }
    currentMap.setTarget(null);

    const { displaySet } = this.props.viewportData;
    const { viewportIndex } = this.props;

    if (this.state.imageViewers[displaySet.SeriesInstanceUID]) {
      const container = this.container.current;
      this.viewer = this.state.imageViewers[displaySet.SeriesInstanceUID];
      this.viewer.render({ container });
      addViewport(viewportIndex, this.viewer, container);

      // Remove all Rois already created on microscopy viewer and recreate them
      // from measurement data so that measurement panel operations are reflected
      // on the viewport on reloading.
      removeAllOpenLayerROIs(displaySet.imageId, this.viewer);
      restoreAnnotationsOnViewport(viewportIndex);
    } else {
      this.installOpenLayersRenderer(this.container.current, displaySet);
    }
  }

  componentWillUnmount() {
    const { viewportIndex } = this.props;
    microscopyEventRegister.unBindEvents(viewportIndex);
    removeViewport(viewportIndex);
  }

  render() {
    const style = { width: '100%', height: '100%' };
    return (
      <div
        className={'DicomMicroscopyViewer'}
        style={style}
        onPointerDown={this.setViewportActiveHandler}
      >
        {ReactResizeDetector && (
          <ReactResizeDetector
            handleWidth
            handleHeight
            onResize={this.onWindowResize}
          />
        )}
        {this.state.error ? (
          <h2>{JSON.stringify(this.state.error)}</h2>
        ) : (
          <div style={style} ref={this.container} />
        )}
      </div>
    );
  }

  onWindowResize = () => {
    this.debouncedResize();
  };

  setViewportActiveHandler = () => {
    const {
      setViewportActive,
      viewportIndex,
      activeViewportIndex,
    } = this.props;

    if (viewportIndex !== activeViewportIndex) {
      setViewportActive(viewportIndex);
    }
  };
}

export default DicomMicroscopyViewport;
