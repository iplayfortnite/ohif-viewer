import React from 'react';
import Redux from '../redux';
import store from '../../../../platform/viewer/src/store';
import { SimpleDialog } from '@ohif/ui';

const { addMicroscopyData } = Redux.actions;

const microscopyTagValidation = (metadata, props) => {
  let isOpticalPathSequenceTag = false;
  let isTotalPixelMatrixOriginSequenceTag = false;
  const OpticalPathSequenceTag = '00480105';
  const TotalPixelMatrixOriginSequenceTag = '00480008';
  metadata.map(m => {
    if (m.hasOwnProperty(OpticalPathSequenceTag)) {
      isOpticalPathSequenceTag = true;
    }
    if (m.hasOwnProperty(TotalPixelMatrixOriginSequenceTag)) {
      isTotalPixelMatrixOriginSequenceTag = true;
    }
  });
  if (!isOpticalPathSequenceTag || !isTotalPixelMatrixOriginSequenceTag) {
    dialogShow(
      props,
      isOpticalPathSequenceTag,
      isTotalPixelMatrixOriginSequenceTag,
      OpticalPathSequenceTag,
      TotalPixelMatrixOriginSequenceTag
    );
    store.dispatch(addMicroscopyData(true));
  }
};

const dialogShow = (
  props,
  isOpticalPathSequenceTag,
  isTotalPixelMatrixOriginSequenceTag,
  OpticalPathSequenceTag,
  TotalPixelMatrixOriginSequenceTag
) => {
  const { UIDialogService } = props.servicesManager.services;
  let missingTags = '';
  if (!isTotalPixelMatrixOriginSequenceTag) {
    missingTags = TotalPixelMatrixOriginSequenceTag;
  }
  if (!isOpticalPathSequenceTag) {
    if (missingTags) {
      missingTags += ', ';
    }
    missingTags += OpticalPathSequenceTag;
  }
  const returnContent = () => {
    return (
      <div>
        This dicom microscopy study does not contain the tag ({missingTags}) for
        activating annotation tools.
      </div>
    );
  };
  const callInputDialog = () => {
    let dialogId = UIDialogService.create({
      content: SimpleDialog,
      defaultPosition: {
        x: window.innerWidth / 2 - 300,
        y: window.innerHeight / 2 - 100,
      },
      showOverlay: true,
      contentProps: {
        showFooterButtons: false,
        headerTitle: 'Warning',
        onClose: () => UIDialogService.dismiss({ id: dialogId }),
        onConfirm: () => {
          UIDialogService.dismiss({ id: dialogId });
        },
        children: returnContent(),
        componentStyle: { width: '600px' },
      },
    });
  };
  callInputDialog();
};

export { microscopyTagValidation };
