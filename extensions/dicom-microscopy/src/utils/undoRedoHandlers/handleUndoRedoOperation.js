import store from '@ohif/viewer/src/store';
import handleMeasurementUndoOperation from './handleMeasurementUndoOperation';
import handleMeasurementRedoOperation from './handleMeasurementRedoOperation';

import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';

const {
  undoLastActionState,
  redoNextActionState,
} = FlywheelCommonRedux.actions;

/**
 * Handle the undo/redo operation
 * @param {boolean} isUndo - if true, then perform undo or else do redo
 * @param {Object} viewports - store viewport state
 */
export default function (isUndo, viewports) {
  const undoRedoActionState = store.getState().undoRedoActionState;
  const currentIndex = undoRedoActionState.currentIndex;

  if (isUndo) {
    const postUndoIndex = currentIndex - 1;
    if (0 <= postUndoIndex) {
      const actionState = undoRedoActionState.actionStates[postUndoIndex];
      const lastAction = actionState?.start;
      store.dispatch(undoLastActionState());
      if (lastAction.type === 'measurements') {
        handleMeasurementUndoOperation(lastAction);
      }
    }
  } else {
    if (undoRedoActionState.actionStates.length >= currentIndex + 1) {
      const actionState = undoRedoActionState.actionStates[currentIndex];
      const nextAction = actionState?.end || actionState?.start;
      if (nextAction.type === 'measurements') {
        handleMeasurementRedoOperation(nextAction);
      }
      store.dispatch(redoNextActionState());
    }
  }
}
