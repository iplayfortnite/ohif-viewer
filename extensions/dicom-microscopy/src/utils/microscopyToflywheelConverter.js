import store from '@ohif/viewer/src/store';
import { getFWToolType } from '../tools/utils';

const getFWMeasurement = (feature) => {
  const state = store.getState();
  const activeViewport = activeViewportIndex(state);
  const viewportData = state.viewports.viewportSpecificData[activeViewport];
  const imagePath = [
    viewportData.StudyInstanceUID,
    viewportData.SeriesInstanceUID,
    viewportData.SOPInstanceUID,
    0,
  ].join('$$$');

  return {
    visible: true,
    active: true,
    invalidated: true,
    imagePath,
    flywheelOrigin: {
      type: 'user',
      id: state.flywheel.user._id,
    },
    toolType: getFWToolType(feature),
    uuid: feature.id_,
    _measurementServiceId: undefined
  }
}

const activeViewportIndex = state => state.viewports.activeViewportIndex;

export { getFWMeasurement }
