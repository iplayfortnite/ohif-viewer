import _AnnotationManager from "dicom-microscopy-viewer/src/annotations/_AnnotationManager";
import { MarkupManager } from "./markupManager";

export class AnnotationManager extends _AnnotationManager {

  constructor(props = {}) {
    props.markupManagerConstructor = MarkupManager;
    super(props);
  }

}
