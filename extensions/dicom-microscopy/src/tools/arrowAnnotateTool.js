import { measurements } from '@ohif/core';
import { globalDicomMicroscopyToolStateManager } from '../toolStateManager';
import baseAnnotationTool from "./baseAnnotationTool";
import { getGeometryName } from "./utils";

export default class arrowAnnotateTool extends baseAnnotationTool {

  updateFWMeasurementHandles(measurementData, geometryData) {
    const graphicData = geometryData.scoord3d.graphicData;
    measurementData.handles = {
      start: {
        x: graphicData?.[0]?.[0],
        y: graphicData?.[0]?.[1],
        highlight: true,
        active: false,
      },
      end: {
        x: graphicData?.[1]?.[0],
        y: graphicData?.[1]?.[1],
        highlight: true,
        active: false,
      },
      textBox: this.createMeasurementTextBox(graphicData),
    }
  }

  createRoi(measurementData, viewerInstance) {
    const imageId = measurements.getImageIdForImagePath(measurementData.imagePath)
    const smMeasurementData = globalDicomMicroscopyToolStateManager.getToolStateForMeasurement(
      imageId, measurementData.toolType, measurementData.uuid);

    if (smMeasurementData?.geometryData) {
      super.createRoi(smMeasurementData, viewerInstance);
    } else {
      const roi = {};
      const styleOptions = {};

      roi.uid = measurementData.uuid;
      roi.properties = { marker: 'arrow' };
      roi.geometryName = getGeometryName(measurementData.toolType);
      roi.scoord3d = { graphicType: 'POLYLINE' };

      const { handles } = measurementData;
      const graphicData = [];

      if (handles?.start && handles?.end) {
        graphicData.push([handles.start.x, handles.start.y, 0]);
        graphicData.push([handles.end.x, handles.end.y, 0]);
      }
      roi.scoord3d.graphicData = graphicData;

      styleOptions.fill = { color: measurementData.color };
      styleOptions.stroke = {
        color:
          measurementData.color.substring(
            0,
            measurementData.color.lastIndexOf(',')
          ) + ', 1)',
        width: 2,
      };

      roi.properties.styleOptions = styleOptions;
      viewerInstance.addROI(roi, styleOptions);
    }
  }
}
