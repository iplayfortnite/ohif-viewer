import store from '@ohif/viewer/src/store';
import { getDefaultBoundingBox } from "../../../flywheel-common/src/tools/textBoxContent";
import { getFWToolType, getTextBoxCoords, getGeometryName, getSymbol } from "./utils";

export default class baseAnnotationTool {

  createMeasurementData(feature) {
    const state = store.getState();
    const activeViewport = activeViewportIndex(state);
    const viewportData = state.viewports.viewportSpecificData[activeViewport];
    const imagePath = [
      viewportData.StudyInstanceUID,
      viewportData.SeriesInstanceUID,
      viewportData.SOPInstanceUID,
      0,
    ].join('$$$');

    return {
      visible: true,
      active: true,
      invalidated: true,
      imagePath,
      flywheelOrigin: {
        type: 'user',
        id: state.flywheel.user._id,
      },
      toolType: getFWToolType(feature),
      uuid: feature.id_,
      _measurementServiceId: undefined
    }
  }

  createMeasurementTextBox(graphicData) {
    const { x, y } = getTextBoxCoords(graphicData);
    return {
      active: false,
      hasMoved: false,
      movesIndependently: false,
      drawnIndependently: true,
      allowedOutsideImage: true,
      hasBoundingBox: true,
      x,
      y,
      boundingBox: getDefaultBoundingBox({ x, y }),
    };
  }

  createRoi(smMeasurementData, viewerInstance) {
    if (!smMeasurementData.geometryData || !viewerInstance) {
      return;
    }
    const { styleOptions } = smMeasurementData.geometryData.properties || {};
    const roi = smMeasurementData.geometryData;
    roi.geometryName = getGeometryName(smMeasurementData.toolType);
    viewerInstance.addROI(roi, styleOptions);
  }
}

const activeViewportIndex = state => state.viewports.activeViewportIndex;
