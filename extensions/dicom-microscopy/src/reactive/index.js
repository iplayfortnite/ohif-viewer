import store from './store';

const reactive = {
  store,
};

export default reactive;
