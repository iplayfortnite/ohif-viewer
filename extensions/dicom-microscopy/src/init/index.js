import initStore from './initStore';

export default function initExtension({ appConfig }) {
  const { store } = appConfig;
  if (!store) {
    return;
  }
  initStore(store);
}
