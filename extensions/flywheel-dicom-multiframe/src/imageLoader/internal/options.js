import OHIF from '@ohif/core';

let options = {
  beforeFetch: function() {
    const headers = OHIF.DICOMWeb.getAuthorizationHeader();

    return headers;
  },
};

export function setOptions(newOptions) {
  Object.assign(options, newOptions);
}

export function getOptions() {
  return options;
}
