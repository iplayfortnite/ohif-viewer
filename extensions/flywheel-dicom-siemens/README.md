This extension is to display mosaic siemens study. It parses, load and create instances from a single mosaic study to multiple instances.

# Support
- Load of mosaic study series
- Load of instances of same siemens type (we group them before we do the split)
- OHIF tools
- Measurements and OHIF MeasurementAPI

# Not full support (might occurs some inconsistencies)
- Load of many studies
- Load of different instance types on a unique series

# The process
The whole process of spliting mosaic consists of:
- Register siemens image loader (done at load time, init extension)
- Fetch the study metadata (OHIF default)
- Create the instances (OHIF default)
- Split the instances. Only if Manufacture tag is `SIEMENS`
- Cornerstone selects Siemens loader to be used
- Siemens loader splits data into header and pixelData
- Siemens loader splits pixelData into blocks and cache them based on seriesInstanceUID
- Cornerstone displays the image

## The image loader
Siemens mosaic is based on wadors protocol from package https://github.com/cornerstonejs/cornerstoneWADOImageLoader. In summary, it uses same approach of loading an image as this last one. I.e we must register the image loader, define the loadImage method and at end the getPixelData method. To load the correct loader we established the imageId scheme as`siemensWadors`.


## Split instance
Prior to load process we have to produce _n_ slices instances from one unique instance. Server provides an entire mosaic only. Each new instance will be based on the source instance with some new tags. This is an important part of process where we can tell OHIF app the amount of instances and read private tags. Important tech decisions:
- Each instance will have its own SOPInstanceUID and the source SOPInstanceUID will be present at SourceInstanceSequence tag (**0042,0013**)
- Each instance imageId will contain siemens scheme (`siemensWadors`). It will be wadors ImageId based but each instance will refer the original source instance and not this new instance. Something like `...sourceSOPInstanceUID/frame/indexOfInstance`, where `sourceSOPInstanceUID` is the SOPInstanceUID from source image.
- Each instance contains unique `SOPInstanceUID` but `imageId` points to source image instead. We set frames to differentiate each instance. This is required because server only provides data for source imageId.
- Each instance `wadorsURI` and any other property (which is used to fetch data) will be referred to source image instead.
- TBD Each instance will contain data from parsed private tags

## Private tag
Siemens private tag goes under tags (**0029,xxxx**) and as per definition: The CSAImageHeaderInfo and the CSA Series Header Info fields are of the same format. The fields can be of two types, CSA1 and CSA2 (https://nipy.org/nibabel/dicom/siemens_csa.html).
So, CSA tags will require to be parsed by one of types CSA1 and CSA2, which can read the buffer and produce a readable format. This is extremely important to produce a more reasonable instance on process, it will improve app support such as: ordering, orientation...

## Split mosaic data
Based on nSlices, nBlocks and having a pixelData we can split pixelData into multiple blocks. The process has _**O(n)**_ complexity. It simulates original pixelData array being a 2D array, iterate over its pixels (using row major order method), then distribute them to equivalent blocks.
It runs only once per each SeriesInstanceUID. The remaining instance retrieve their blocks (pixelData) from the cached object.
