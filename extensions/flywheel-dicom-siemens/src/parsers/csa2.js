/*
hdr_id : S4 == ‘SV10’
unused1 : uint8, 4
n_tags, uint32, number of tags. Number of tags should apparently be between 1 and 128. If this is not true we just abort and move to csa_max_pos.
unused2, uint32, apparently has value 77

Each tag
name : S64, null terminated string 64 bytes
vm : int32
vr : S4, first 3 characters only
syngodt : int32
nitems : int32
xx : int32 - apparently either 77 or 205
nitems gives the number of items in the tag. The items follow directly after the tag.

Each item
xx : int32 * 4 . The first of these seems to be the length of the item in bytes, modified as below.
Now there’s a different length check from CSA1. item_len is given just by xx[1]. If item_len > csa_max_pos - csa_position (the remaining bytes in the header), then we just read the remaning bytes in the header (as above) into value below, as uint8, move the filepointer to the next 4 byte boundary, and give up reading.

value : uint8, item_len.
*/
import CSA from './csa';

const HDR_ID_LENGTH = 4;
const HDR_ID = 'SV10';

export default class CSA2 extends CSA {
  hrdIdValidation() {
    if (this.hdrId !== HDR_ID) {
      throw new Error('There is no valide hdr id');
    }

    return true;
  }

  readHeader() {
    this.hdrId = this.byteStream.readFixedString(HDR_ID_LENGTH);

    this.hrdIdValidation();
    this.skipBytes(8);
  }

  readItemLength() {
    return this.byteStream.readUint32();
  }
  itemLengthValidation(itemLength) {
    return false;
  }
}
