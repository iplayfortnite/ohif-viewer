import dicomParser from 'dicom-parser';
import cornerstoneWadoImageLoader from 'cornerstone-wado-image-loader';
import CSA2 from './csa2';

const { findIndexOfString } = cornerstoneWadoImageLoader.wadors;

function findBoundary(header) {
  for (let i = 0; i < header.length; i++) {
    if (header[i].substr(0, 2) === '--') {
      return header[i];
    }
  }
}

function uint8ArrayToString(data, offset, length) {
  offset = offset || 0;
  length = length || data.length - offset;
  let str = '';

  for (let i = offset; i < offset + length; i++) {
    str += String.fromCharCode(data[i]);
  }

  return str;
}

/**
 * It splits private tag on format array buffer. Tags (0029, 1010) and (0029, 1020)
 * @param {*} arrayBuffer
 */
export default function splitPrivateTagBuffer(arrayBuffer) {
  const response = new Uint8Array(arrayBuffer);

  // First look for the multipart mime header
  const tokenIndex = findIndexOfString(response, '\r\n\r\n');

  if (tokenIndex === -1) {
    reject(new Error('invalid response - no multipart mime header'));
  }
  const header = uint8ArrayToString(response, 0, tokenIndex);
  // Now find the boundary  marker
  const split = header.split('\r\n');
  const boundary = findBoundary(split);

  if (!boundary) {
    reject(new Error('invalid response - no boundary marker'));
  }
  const offset = tokenIndex + 4; // skip over the \r\n\r\n

  // find the terminal boundary marker
  const endIndex = findIndexOfString(response, boundary, offset);

  if (endIndex === -1) {
    reject(new Error('invalid response - terminating boundary not found'));
  }

  // Remove \r\n from the length
  const length = endIndex - offset - 2;

  const source = new Uint8Array(arrayBuffer, offset, length);
  const byteStream = new dicomParser.ByteStream(
    dicomParser.littleEndianByteArrayParser,
    source
  );

  // TODO MOSAIC to define which parser to use.
  const parser = new CSA2(byteStream);

  return parser.readPrivateTag();
}
