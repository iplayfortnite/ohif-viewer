import loadImage from './loadImage.js';

export default function(cornerstone) {
  // register siemens wadors scheme, using default OHIF metadata providers
  cornerstone.registerImageLoader('siemensWadors', loadImage);
}
