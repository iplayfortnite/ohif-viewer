import OHIF from '@ohif/core';
/**
 * Single object to store split blocks per mosaic image
 */
let MosaicManager = {};
class Block {
  constructor(TypedArray, blockLength) {
    this.pixelData = new TypedArray(blockLength);
    this.it = 0;
  }
  insertPixel(pixel) {
    this.pixelData[this.it] = pixel;
    this.it++;
  }
}

/**
 * is it last index of current block col?
 *
 * @param {number} index Pixel index
 * @param {number} blockLastIndex Current block last col
 * @return {boolean}
 */
const isAtLastBlockCol = (index, blockLastIndex) => index === blockLastIndex;

/**
 * is it last index of current block col and also last index current slab col?
 * Next index is the first index of first block of next slab row
 *
 *
 * @param {number} index Pixel index
 * @param {number} slabLastIndex Current slab last index
 * @return {boolean}
 */
const isAtLastSlabCol = (index, slabLastIndex) => index === slabLastIndex;

/**
 * is it last index of current block col and also last index of slice col?
 * Next index is the next row index (of first block) of current slab row
 *
 * @param {number} blockIndex Block index
 * @param {number} sliceLine Current line
 * @param {number} nRowBlocks Number of row blocks
 * @return {boolean}
 */
const isAtLastBlockIndexOfSlice = (blockIndex, sliceLine, nRowBlocks) =>
  blockIndex === sliceLine * nRowBlocks - 1;

/**
 * It returns the last index of given block col
 * @param {number} firstSlabIndex first index of current slab
 * @param {number} pixelsPerBlockCol number of pixels per block col
 * @param {number} correction parameter based on difference from source bits allocated and current instance bits allocated
 * @return {boolean}
 */
const getBlockLastIndex = (firstSlabIndex, pixelsPerBlockCol, correction = 1) =>
  firstSlabIndex + correction * pixelsPerBlockCol - 1;

/**
 * It returns the last index of given slab. I.e the last of the last index on current slab.
 * @param {number} firstSlabIndex first index of current slab
 * @param {number} nSlabRows number of slab rows
 * @param {number} nSlabCols number of slab columns
 * @param {number} pixelsPerBlockCol number of pixels per block col
 * @param {number} correction parameter based on difference from source bits allocated and current instance bits allocated
 */
const getSlabLastIndex = (
  firstSlabIndex,
  nSlabRows,
  nSlabCols,
  pixelsPerBlockCol,
  correction = 1
) => firstSlabIndex + correction * nSlabRows * pixelsPerBlockCol - 1;

/**
 * Get number of slices given instance has. Its based on SourceImageSequence data.
 * @param {Object} instance Object instance
 * @return {number} Number of slices
 */
const getNSlices = (instance = {}) => {
  const { SourceImageSequence = [] } = instance;
  return SourceImageSequence.length;
};

/**
 * It returns bits correction value. This is necessary because the initial parser differs from split method.
 * Initial parser uses by default uint8 and pixel data (info from metadata) can have a different value for bits allocated.
 * @param {Object} instance
 * @param {number} sourceBytesPerElement Bytes per element for source array element
 */
const getBitsCorrection = (instance, sourceBytesPerElement) => {
  let result = 1;
  const bits = 8;
  const { BitsAllocated } = instance;

  if (!BitsAllocated) {
    OHIF.log.warn('Failed to get bits correction');
    return result;
  }

  if (BitsAllocated % 2 !== 0) {
    OHIF.log.warn('Invalid bits allocated from instance');
    return result;
  }

  const sourceBitsAllocated = sourceBytesPerElement * bits;

  // TODO mosaic to check in case sourceBitsAllocated > BitsAllocated;
  return BitsAllocated / sourceBitsAllocated;
};

/**
 * Method to split given pixelData.
 * It splits siemens mosaic into multiple blocks (valid slices or not)
 * It caches each split mosaic per mosaic image to be consumer later.
 * It iterates over pixel data simulating row-major order method
 *
 * @param {TypedArray} sourcePixelData
 * @param {*} SourceTypedArray Constructor of source TypedArray
 * @param {Object} instance Object instance
 * @param {String} contentType Pixel data content type
 */
function splitAndCacheBlocks(
  sourcePixelData,
  SourceTypedArray,
  instance = {},
  contentType
) {
  const imageKey = getImageKey(instance);

  // create a new Mosaic instance
  clearCachedBlocks(imageKey);
  MosaicManager[imageKey] = {
    cachedBlocks: new Array(),
    contentType: contentType,
  };

  const nSlices = getNSlices(instance);
  const instanceRows = instance.Rows;

  if (!nSlices || !instanceRows) {
    throw Error(
      'Error while spliting mosaic. Missing Rows and/or SourceImageSequence tags.'
    );
  }
  // set params from https://nipy.org/nibabel/dicom/dicom_mosaic.html
  const nRowBlocks = Math.ceil(Math.sqrt(nSlices));
  const nColBlocks = nRowBlocks; // blocks are always squared, then nColBlocks === nRowBlocks;

  const nSlabRows = instanceRows * nRowBlocks;
  const nSlabCols = nSlabRows; // slabs are always squared, then nSlabRows === nSlabCols;

  const nBlocks = nRowBlocks * nColBlocks;
  const nSlab = sourcePixelData.length;

  const pixelsPerBlockCol = nSlabRows / nRowBlocks;
  // Get the value to correct a possible mismatching between source bits allocated and each instance.
  const correction = getBitsCorrection(
    instance,
    sourcePixelData.BYTES_PER_ELEMENT
  );
  const sliceLength = pixelsPerBlockCol * pixelsPerBlockCol * correction;

  // initialize vars of control
  let sliceLine = 1;
  let it = 0; // slab index
  let blockIndex = 0;
  let blockLastIndex = getBlockLastIndex(it, pixelsPerBlockCol, correction);
  let slabLastIndex = getSlabLastIndex(
    it,
    nSlabRows,
    nSlabCols,
    pixelsPerBlockCol,
    correction
  );

  // create first block (slice)
  MosaicManager[imageKey].cachedBlocks[0] = new Block(
    SourceTypedArray,
    sliceLength
  );

  try {
    // run over all slab indexes and populate blocks pixels (producing slices and non slices)
    while (it < nSlab) {
      const currentMosaic = MosaicManager[imageKey].cachedBlocks[blockIndex];
      // set last item and change mosaic
      if (isAtLastBlockCol(it, blockLastIndex)) {
        currentMosaic.insertPixel(sourcePixelData[it]);
        let changeSlice = false;
        // change mosaic index and check if we should change slice
        if (isAtLastSlabCol(it, slabLastIndex)) {
          blockIndex = MosaicManager[imageKey].cachedBlocks.length;
          changeSlice = true;
        } else {
          //next index is firstCol of same slice
          if (isAtLastBlockIndexOfSlice(blockIndex, sliceLine, nRowBlocks)) {
            blockIndex = blockIndex + 1 - nRowBlocks; // equivalent to circular increment
          } else {
            blockIndex++;
          }
        }

        it++;
        blockLastIndex = getBlockLastIndex(it, pixelsPerBlockCol, correction);
        // update current slice
        if (changeSlice) {
          slabLastIndex = getSlabLastIndex(
            it,
            nSlabRows,
            nSlabCols,
            pixelsPerBlockCol,
            correction
          );
          sliceLine++;
        }
        // create new block item
        if (!MosaicManager[imageKey].cachedBlocks[blockIndex]) {
          MosaicManager[imageKey].cachedBlocks[blockIndex] = new Block(
            SourceTypedArray,
            sliceLength
          );
        }
      } else {
        currentMosaic.insertPixel(sourcePixelData[it]);
        it++;
      }
    }
  } catch (e) {
    throw Error(
      'Error while spliting mosaic. Some slices might not be displayed properly'
    );
  }
}

/**
 * It returns the blocks for a given mosaic image.
 * @param {string} imageKey
 * @return {Array} Array of blocks (or in case of siemens slices)
 */
const getBlocks = imageKey => {
  return (MosaicManager[imageKey] || {}).cachedBlocks || [];
};

/**
 * It clears all cached (processed) blocks for given mosaic image
 * @param {number} imageKey
 * @return {boolean} True in case cached blocks exists, false otherwise
 */
const clearCachedBlocks = imageKey => {
  const cachedBlocks = (MosaicManager[imageKey] || {}).cachedBlocks;

  if (cachedBlocks) {
    cachedBlocks.splice(0, cachedBlocks.length);
    return true;
  }

  return false;
};

/**
 * Clear all existing data into MosaicManager
 */
const clearAllCachedBlocks = () => {
  Object.keys(MosaicManager).forEach(key => {
    delete MosaicManager[key];
  });
};
/**
 * It returns pixelData content type for given mosaic image
 * @param {number} imageKey
 * @return {string}
 */
const getContentType = imageKey => {
  return (MosaicManager[imageKey] || {}).contentTypeCached;
};

/**
 * Generate a key for the image containing series and SOP-instance UIDs to uniquely identify a mosaic image
 * @param {Object} instance Object instance
 * @return {string} cache key for a mosaic image
 */
const getImageKey = instance => {
  const seriesInstanceUID = instance.SeriesInstanceUID;
  const sopInstanceUID =
    instance.SourceInstanceSequence?.ReferencedSOPInstanceUID;
  return `${seriesInstanceUID}_${sopInstanceUID}`;
};

const mosaic = {
  splitAndCacheBlocks,
  clearCachedBlocks,
  clearAllCachedBlocks,
  getBlocks,
  getContentType,
  getImageKey,
};

export default mosaic;
