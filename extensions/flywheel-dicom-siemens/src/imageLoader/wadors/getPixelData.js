import cornerstoneWadoImageLoader from 'cornerstone-wado-image-loader';
import OHIF from '@ohif/core';
import Mosaic from './mosaic';
import getTypedArrayConstructor from '../../utils/getTypedArrayConstructor';

const { findIndexOfString } = cornerstoneWadoImageLoader.wadors;
const { xhrRequest } = cornerstoneWadoImageLoader.internal;
/**
 * Method to find header
 * Original from wadors image loader
 * @param {string} header
 */
function findBoundary(header) {
  for (let i = 0; i < header.length; i++) {
    if (header[i].substr(0, 2) === '--') {
      return header[i];
    }
  }
}

/**
 * Method to find content type
 * Original from wadors image loader
 * @param {string} header
 */
function findContentType(header) {
  for (let i = 0; i < header.length; i++) {
    if (header[i].substr(0, 13) === 'Content-Type:') {
      return header[i].substr(13).trim();
    }
  }
}

/**
 * Convert data to string
 * Original from wadors image loader
 * @param {*} data
 * @param {*} offset
 * @param {*} length
 */
function uint8ArrayToString(data, offset, length) {
  offset = offset || 0;
  length = length || data.length - offset;
  let str = '';

  for (let i = offset; i < offset + length; i++) {
    str += String.fromCharCode(data[i]);
  }

  return str;
}

/**
 * Method to which return promise resolve answer
 * It gets and return content type and pixelData from Mosaic
 * @param {number} blockIndex Index of given mosaic to return
 * @param {string} imageKey seriesInstanceUID_SOPInstanceUID of given mosaic image to return
 *
 */
function getPromiseResolve(blockIndex, imageKey) {
  try {
    const contentType = Mosaic.getContentType(imageKey);
    const blockPixelData = Mosaic.getBlocks(imageKey)[blockIndex].pixelData;
    return {
      contentType: contentType,
      imageFrame: {
        pixelData: blockPixelData,
      },
    };
  } catch (e) {
    throw Error(
      `Error while getting pixel data for given SeriesInstanceUID_SOPInstanceUID:${imageKey} and block index:${blockIndex}`
    );
  }
}

let fetchDataPromises = {};

/**
 * This is a copy from wadoImageLoader with some adjustments.
 * To keep original implementation it was added two strategies into it:
 * 1. One to store given series promise
 * 2. Split mosaic. For this case to prevent calling server many times we use strategy 1.
 * @param {*} uri
 * @param {*} imageId
 * @param {*} mediaType
 */

function getPixelData(
  uri,
  imageId,
  mediaType = 'application/octet-stream',
  options
) {
  const headers = {
    accept: mediaType,
  };

  const instance = cornerstone.metaData.get('instance', imageId) || {};

  // TODO mosaic to be reviewed
  const blockIndex = instance['00290001'];
  const mosaicImageKey = Mosaic.getImageKey(instance);

  // chain already existing promise to this new request
  const seriesFetchPromise = fetchDataPromises[mosaicImageKey];
  if (seriesFetchPromise && seriesFetchPromise.isFetching) {
    return seriesFetchPromise.Promise.then(() => {
      return getPromiseResolve(blockIndex, mosaicImageKey);
    });
  }
  // in case block already exists return it otherwise fetch and split mosaic
  if (Mosaic.getBlocks(mosaicImageKey)[blockIndex]) {
    return Promise.resolve(getPromiseResolve(blockIndex, mosaicImageKey));
  } else {
    fetchDataPromises[mosaicImageKey] = {};
    fetchDataPromises[mosaicImageKey].isFetching = true;
    fetchDataPromises[mosaicImageKey].Promise = new Promise(
      (resolve, reject) => {
        const loadPromise = xhrRequest(uri, imageId, headers);
        loadPromise.then(function(imageFrameAsArrayBuffer /* , xhr*/) {
          // request succeeded, Parse the multi-part mime response
          const response = new Uint8Array(imageFrameAsArrayBuffer);

          // First look for the multipart mime header
          const tokenIndex = findIndexOfString(response, '\r\n\r\n');

          if (tokenIndex === -1) {
            reject(new Error('invalid response - no multipart mime header'));
          }
          const header = uint8ArrayToString(response, 0, tokenIndex);
          // Now find the boundary  marker
          const split = header.split('\r\n');
          const boundary = findBoundary(split);

          if (!boundary) {
            reject(new Error('invalid response - no boundary marker'));
          }
          let transferSyntax = split[1].split('=')[1];
          if (!transferSyntax) {
            console.warn(
              `Could not find Transfer Syntax from '${header}'. ` +
                'Falling back to use DICOM Implicit VR Little Endian Transfer Syntax for decoding.'
            );
            // Try DICOM Implicit VR Little Endian Transfer Syntax
            transferSyntax = '1.2.840.10008.1.2';
          }
          const offset = tokenIndex + 4; // skip over the \r\n\r\n

          // find the terminal boundary marker
          const endIndex = findIndexOfString(response, boundary, offset);

          if (endIndex === -1) {
            reject(
              new Error('invalid response - terminating boundary not found')
            );
          }

          // Remove \r\n from the length
          const length = endIndex - offset - 2;

          const source = new Uint8Array(
            imageFrameAsArrayBuffer,
            offset,
            length
          );

          fetchDataPromises[mosaicImageKey].isFetching = false;
          fetchDataPromises[mosaicImageKey].isFetched = true;
          // Let Cornerstone decode supported compressed images
          cornerstoneWadoImageLoader
            .createImage(imageId, source, transferSyntax, options)
            .then(image => {
              let objectToResolve;
              try {
                const wholeMosaicPixelData = image.getPixelData();
                Mosaic.splitAndCacheBlocks(
                  wholeMosaicPixelData,
                  getTypedArrayConstructor(wholeMosaicPixelData),
                  instance,
                  findContentType(split)
                );

                objectToResolve = getPromiseResolve(blockIndex, mosaicImageKey);
                // return the info for this pixel data
                resolve(objectToResolve);
              } catch (e) {
                reject(e);
              }
            })
            .catch(reject);
        }, reject);
      }
    );

    return fetchDataPromises[mosaicImageKey].Promise;
  }
}

export default getPixelData;
