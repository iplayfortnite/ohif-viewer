export default function setTileSizeFromSiemensResolutionTag(sopInstance) {
  // Experimental! For a 128x128 mosaic tile,
  // the private tag (0051,100B) might contain value like 128p*128
  const sizeParts = sopInstance[
    dicomDict.dict.DICOMDescriptionsTag.InPlaneResolution
  ].split('*');
  // update col and row
  sopInstance.Rows = parseInt(sizeParts[0]);
  sopInstance.Columns = parseInt(sizeParts[1]);
}
