export default function setTileSizeFromAcquisitionMatrix(sopInstance) {
  const acquisitionMatrixValues = sopInstance.AcquisitionMatrix;
  // e.g. for a 256 square acquisition, AcquisitionMatrix would have:
  // [256, 0, 0, 256] if the image row axis was the frequency encoding axis
  // or
  // [0, 256, 256, 0] if the image row was the phase encoding axis
  sopInstance.Rows =
    parseInt(acquisitionMatrixValues[0]) || // frequency rows
    parseInt(acquisitionMatrixValues[2]); // phase rows
  sopInstance.Columns =
    parseInt(acquisitionMatrixValues[3]) || // phase columns
    parseInt(acquisitionMatrixValues[1]); // frequency columns
}
