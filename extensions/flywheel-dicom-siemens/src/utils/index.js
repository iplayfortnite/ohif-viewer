import DICOMDict from './DICOMDict';
import * as ImageIdUtils from './imageId';
import getTypedArrayConstructor from './getTypedArrayConstructor';

const utils = {
  DICOMDict,
  ImageIdUtils,
  getTypedArrayConstructor,
};

export default utils;
