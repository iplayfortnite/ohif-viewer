import * as dict from './dict';
import * as tag from './tag';

const dicomDict = {
  dict,
  tag,
};

export default dicomDict;
