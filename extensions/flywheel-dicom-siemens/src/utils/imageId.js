export const scheme = 'siemensWadors';

/**
 * It changes wadors to siemensWadors allowing to use Siemens image loader
 * @param {string} wadorsId Image id
 * @return {string}
 */
export const replaceToSiemensIdScheme = wadorsId => {
  return wadorsId.replace('wadors', scheme);
};

/**
 * Siemens wadors Image id is based on studyInstanceUID, seriesInstanceUID, source sopInstanceUID and current frame index.
 * It return uri equivalent to source imageID
 * @param {string} siemensImageId Image id
 * @return {string}
 */
export const getURI = siemensImageId => {
  let uri = siemensImageId.replace(`${scheme}:`, '');
  const anyFrameRegExp = /\/frames\/[\d]*/;
  const sourceFrame = '/frames/1';
  uri = uri.replace(anyFrameRegExp, sourceFrame);

  return uri;
};
