import OHIF from '@ohif/core';

const { getImageIdForImagePath } = OHIF.measurements;

export function getImageDataFromUrl(imageUrl) {
  const imagePath = imageUrl.replace(/^zip:/, '');
  const [
    StudyInstanceUID,
    SeriesInstanceUID,
    SOPInstanceUID,
    frameIndex,
  ] = imagePath.split('$$$');

  return {
    containerId: StudyInstanceUID,
    zipName: SeriesInstanceUID,
    path: SOPInstanceUID,
    id: getImageIdForImagePath(imagePath),
    frameIndex,
  };
}
