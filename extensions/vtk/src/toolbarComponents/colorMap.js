import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import vtkColorMaps from 'vtk.js/Sources/Rendering/Core/ColorTransferFunction/ColorMaps';
import { Select } from '@ohif/ui';
import './colorMap.styl';

export const DEFAULT_COLOR_ID = 'hsv';

let validColors = null;

const _getInitialState = () => {
  if (!validColors) {
    validColors = vtkColorMaps.rgbPresetNames.map((presetName, index) => {
      return {
        key: presetName,
        label: presetName,
        value: presetName,
      };
    });
  }
  return validColors;
};

const _getDefaultColorIndex = () => {
  return validColors.findIndex(color => color.key === DEFAULT_COLOR_ID);
};

function _runColorChangeCommand(button, options, commandsManager) {
  if (button.commandName) {
    //const _options = Object.assign({}, options, button.commandOptions);
    toolbarClickCallback(options);
  }
}

function Colormap({
  button,
  activeButtons,
  isActive = false,
  className = '',
  commandsManager,
  toolbarClickCallback,
}) {
  const { isFusionMode } = useSelector(state => {
    const { viewports = {} } = state;
    const { layout } = viewports;

    return {
      isFusionMode: (layout.viewports || []).find(vp => vp?.vtk?.isFusionMode),
    };
  });
  const [colors] = useState(_getInitialState());
  const [selectedColorId, setSelectedColorId] = useState(
    _getDefaultColorIndex()
  );
  function onChangeSelect(event) {
    const selectedValue = event.target.value;
    // find select value
    const index = colors.findIndex(color => color.value === selectedValue);

    if (index < 0) {
      return;
    }
    const generatedOperation = { ...button };
    generatedOperation.commandOptions = {
      ...button.commandOptions,
      presetName: colors[index].key,
    };
    toolbarClickCallback(generatedOperation, event);

    setSelectedColorId(index);
  }

  return (
    <>
      {isFusionMode && (
        <div>
          <div className="controller">
            <span>Color Map</span>
            <Select
              style={{
                fontSize: '0.8em',
                backgroundColor: 'black',
                color: 'var(--default-color)',
                fontWeight: '600',
                padding: '1px 5px',
                height: 'auto',
                width: '100%',
                position: 'relative',
                bottom: '-6px',
                padding: '0.6em 1.4em 0.5em 0.8em',
              }}
              className={'selectStyles'}
              value={colors[selectedColorId].key}
              onChange={onChangeSelect}
              options={colors}
            />
          </div>
        </div>
      )}
    </>
  );
}

Colormap.propTypes = {
  button: PropTypes.object.isRequired,
  toolbarClickCallback: PropTypes.func.isRequired,
  activeButtons: PropTypes.array.isRequired,
  isActive: PropTypes.bool,
  className: PropTypes.string,
};

export default Colormap;
