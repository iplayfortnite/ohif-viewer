import React, { useEffect, useCallback } from 'react';
import { View2D } from 'react-vtkjs-viewport';
import PropTypes from 'prop-types';
import ReactResizeDetector from 'react-resize-detector';

import './VTKViewport.css';

const VTKViewport = props => {
  const style = { width: '100%', height: '100%', position: 'relative' };

  const setViewportActiveHandler = useCallback(() => {
    const { setViewportActive, viewportIndex, activeViewportIndex } = props;

    if (viewportIndex !== activeViewportIndex) {
      // set in Connected
      setViewportActive();
    }
  });

  const setViewportChange = useCallback(event => {
    props.commandsManager.runCommand('panelView', {
      event: event,
      viewports: props.commandsManager._getAppState().viewports,
    });
  });

  // Notify the vtk viewports on window resize for repositioning the overlays
  const onWindowResize = useCallback(() => {
    const viewports = props.commandsManager._getAppState().viewports;
    const seriesInstanceUIDs = [];
    viewports.layout.viewports.forEach((viewport, index) => {
      const viewportData = viewports.viewportSpecificData[index] || {};
      if (viewport.vtk && !seriesInstanceUIDs.includes(viewportData.SeriesInstanceUID)) {
        seriesInstanceUIDs.push(viewportData.SeriesInstanceUID);
      }
    });
    seriesInstanceUIDs.forEach(seriesInstanceUID => {
      const propertiesToSync = { seriesInstanceUID: seriesInstanceUID, resize: true };
      props.commandsManager.runCommand('update2DMPRViewport', {
        propertiesToSync,
      });
    });
    setTimeout(() => {
      seriesInstanceUIDs.forEach(seriesInstanceUID => {
        const propertiesToSync = { seriesInstanceUID: seriesInstanceUID, resize: true };
        props.commandsManager.runCommand('update2DMPRViewport', {
          propertiesToSync,
        });
      });
    },0);
  });

  useEffect(() => {
    const handleScrollEvent = evt => {
      const vtkViewportApiReference = props.onScroll(props.viewportIndex) || {};
      const viewportUID = vtkViewportApiReference.uid;
      const viewportWasScrolled = viewportUID === evt.detail.uid;

      if (viewportWasScrolled) {
        setViewportActiveHandler();
      }
    };

    window.addEventListener('vtkscrollevent', handleScrollEvent);
    return () =>
      window.removeEventListener('vtkscrollevent', handleScrollEvent);
  }, [props, props.onScroll, props.viewportIndex, setViewportActiveHandler]);

  return (
    <div
      className="vtk-viewport-handler"
      style={style}
      onClick={setViewportActiveHandler}
      onDoubleClick={setViewportChange}
    >
      {ReactResizeDetector && (
        <ReactResizeDetector
          handleWidth
          handleHeight
          onResize={onWindowResize}
        />
      )}
      <View2D {...props} />
    </div>
  );
};

VTKViewport.propTypes = {
  setViewportActive: PropTypes.func.isRequired,
  viewportIndex: PropTypes.number.isRequired,
  activeViewportIndex: PropTypes.number.isRequired,
  /* Receives viewportIndex */
  onScroll: PropTypes.func,
};

VTKViewport.defaultProps = {
  onScroll: () => { },
};

export default VTKViewport;
