import macro from 'vtk.js/Sources/macro';
import { vtkSVGRotatableCrosshairsWidget } from 'react-vtkjs-viewport';

let instanceId = 1;

function getWidgetNode(svgContainer, widgetId) {
  let node = svgContainer.querySelector(`#${widgetId}`);
  if (!node) {
    node = document.createElement('g');
    node.setAttribute('id', widgetId);
    svgContainer.appendChild(node);
  }
  return node;
}

function getBorderStyle(border) {
  const projectConfig = window.store.getState()?.flywheel?.projectConfig;
  const dissociateCrosshairs =
    (projectConfig?.dissociateCrosshairs === undefined)
      ? true
      : projectConfig?.dissociateCrosshairs;
  let style = 'width: 100%; height: 100%;';
  if (dissociateCrosshairs) {
    if (border.left) {
      style += 'border-left: solid #00a4d9;';
    }
    if (border.right) {
      style += 'border-right: solid #00a4d9;';
    }
    if (border.top) {
      style += 'border-top: solid #00a4d9;';
    }
    if (border.bottom) {
      style += 'border-bottom: solid #00a4d9;';
    }
  }
  return style;
}

// ----------------------------------------------------------------------------

function vtkSVGDetachedRotatableCrosshairWidget(publicAPI, model) {
  model.classHierarchy.push('vtkSVGDetachedRotatableCrosshairWidget');
  model.widgetId = `vtkSVGDetachedRotatableCrosshairWidget-${instanceId++}`;


  publicAPI.render = (svgContainer, scale) => {
    const node = getWidgetNode(svgContainer, model.widgetId);
    let {
      point,
      strokeColors,
      strokeWidth,
      strokeDashArray,
      rotateHandleRadius,
      apis,
      apiIndex,
      selectedStrokeWidth,
    } = model;
    if (point[0] === null || point[1] === null) {
      return;
    }

    const thisApi = apis[apiIndex];
    const referenceLines = thisApi.svgWidgets.rotatableCrosshairsWidget.getReferenceLines();

    const width = parseInt(svgContainer.getAttribute('width'), 10);
    const height = parseInt(svgContainer.getAttribute('height'), 10);

    const left = 0;
    const bottom = height / scale;

    // split up lines.

    const p = point.slice();

    p[0] = point[0] * scale;
    p[1] = height - point[1] * scale;

    const [firstLine, secondLine] = referenceLines;

    const {
      points: firstLineRotateHandles,
      selected: firstLineRotateSelected,
    } = firstLine.rotateHandles;
    const {
      points: secondLineRotateHandles,
      selected: secondLineRotateSelected,
    } = secondLine.rotateHandles;

    const [firstLinePart1, firstLinePart2] = model.getSplitReferenceLine(
      firstLine,
      p
    );
    const [secondLinePart1, secondLinePart2] = model.getSplitReferenceLine(
      secondLine,
      p
    );

    const firstLineStrokeColor = strokeColors[firstLine.apiIndex];
    const secondLineStrokeColor = strokeColors[secondLine.apiIndex];

    const firstLineStrokeWidth =
      firstLine.selected || firstLine.active
        ? selectedStrokeWidth
        : strokeWidth;
    const secondLineStrokeWidth =
      secondLine.selected || secondLine.active
        ? selectedStrokeWidth
        : strokeWidth;

    const firstLineRotateWidth = firstLineRotateSelected
      ? selectedStrokeWidth
      : strokeWidth;
    const secondLineRotateWidth = secondLineRotateSelected
      ? selectedStrokeWidth
      : strokeWidth;

    const firstLineShowCrosshairs =
      firstLine.selected || firstLineRotateSelected;
    const secondLineShowCrosshairs =
      secondLine.selected || secondLineRotateSelected;

    const firstLineRotateHandleRadius = firstLineShowCrosshairs
      ? rotateHandleRadius
      : 0;

    const secondLineRotateHandleRadius = secondLineShowCrosshairs
      ? rotateHandleRadius
      : 0;

    const firstLineRotateHandleFill = firstLineRotateSelected
      ? referenceLines[0].color
      : 'none';

    const secondLineRotateHandleFill = secondLineRotateSelected
      ? referenceLines[1].color
      : 'none';

    const CIRCLE_RADIUS = 10;
    const DISTANCE_FROM_CENTER = 20;
    const widgetCircle = {
      radius: CIRCLE_RADIUS,
      center: { x: width - DISTANCE_FROM_CENTER, y: DISTANCE_FROM_CENTER }
    };
    model.widgetCircle = widgetCircle;

    const circleStyle = model.viewportActive
      ? `fill:${strokeColors[apiIndex]}`
      : `stroke: ${strokeColors[apiIndex]}; stroke-width: 2`;

    const style = getBorderStyle(model.border);

    if (model.display) {
      node.innerHTML = `
      <g id="container" fill-opacity="1" stroke-dasharray="none" stroke="none" stroke-opacity="1" fill="none">
       <g>
       <svg version="1.1" viewBox="0 0 ${width} ${height}" width=${width} height=${height} style="${style}">
        <g

          stroke="${firstLineStrokeColor}"
          stroke-dasharray="${strokeDashArray}"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-width="${firstLineStrokeWidth}"
        >
        <!-- First Line part 1!-->
          <line
            x1="${firstLinePart1[0].x}"
            y1="${firstLinePart1[0].y}"
            x2="${firstLinePart1[1].x}"
            y2="${firstLinePart1[1].y}"
          ></line>
          <!-- First Line part 2!-->
          <line
            x1="${firstLinePart2[0].x}"
            y1="${firstLinePart2[0].y}"
            x2="${firstLinePart2[1].x}"
            y2="${firstLinePart2[1].y}"
          ></line>
        </g>
        <g
          stroke="${referenceLines[0].color}"
          stroke-dasharray="${strokeDashArray}"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-width="${firstLineRotateWidth}"
        >
            <!--First line rotateHandle 0 -->
            <circle cx="${firstLineRotateHandles[0].x}" cy="${firstLineRotateHandles[0].y
        }" r="${firstLineRotateHandleRadius}" fill="${firstLineRotateHandleFill}" />
            <!--First line rotateHandle 1 -->
            <circle cx="${firstLineRotateHandles[1].x}" cy="${firstLineRotateHandles[1].y
        }" r="${firstLineRotateHandleRadius}" fill="${firstLineRotateHandleFill}" />
        </g>
        <g
          stroke-dasharray="${strokeDashArray}"
          stroke="${secondLineStrokeColor}"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-width=${secondLineStrokeWidth}
        >
          <!-- Second Line part 1 !-->
          <line
            x1="${secondLinePart1[0].x}"
            y1="${secondLinePart1[0].y}"
            x2="${secondLinePart1[1].x}"
            y2="${secondLinePart1[1].y}"
          ></line>
          <!-- Second Line part 2 !-->
          <line
            x1="${secondLinePart2[0].x}"
            y1="${secondLinePart2[0].y}"
            x2="${secondLinePart2[1].x}"
            y2="${secondLinePart2[1].y}"
          ></line>
      </g>
      <g
        stroke-dasharray="${strokeDashArray}"
        stroke="${referenceLines[1].color}"
        stroke-linecap="round"
        stroke-linejoin="round"
        stroke-width=${secondLineRotateWidth}
      >
        <!--Second line rotateHandle 0 -->
        <circle cx="${secondLineRotateHandles[0].x}" cy="${secondLineRotateHandles[0].y
        }" r="${secondLineRotateHandleRadius}" fill="${secondLineRotateHandleFill}" />
        <!--Second line rotateHandle 1 -->
        <circle cx="${secondLineRotateHandles[1].x}" cy="${secondLineRotateHandles[1].y
        }" r="${secondLineRotateHandleRadius}" fill="${secondLineRotateHandleFill}" />
      </g>
      <circle cx="${widgetCircle.center.x}" cy="${widgetCircle.center.y}" r="${widgetCircle.radius}" style="${circleStyle}"/>
      </g>

      `;
    } else {
      node.innerHTML = '';
    }
  };

}

// ----------------------------------------------------------------------------

const DEFAULT_VALUES = {
  point: [null, null],
  apis: [null, null, null],
  referenceLines: [null, null],
  strokeColors: ['#e83a0e', '#ede90c', '#07e345'],
  strokeWidth: 1,
  rotateHandleRadius: 5,
  selectedStrokeWidth: 3,
  centerRadius: 20,
  strokeDashArray: '',
  display: true,
  viewportActive: false,
  widgetCircle: {},
  border: { left: false, right: false, top: false, bottom: false }
};

// ----------------------------------------------------------------------------

export function extend(publicAPI, model, initialValues = {}) {
  Object.assign(model, DEFAULT_VALUES, initialValues);

  vtkSVGRotatableCrosshairsWidget.extend(publicAPI, model, initialValues);

  macro.obj(publicAPI, model);
  macro.get(publicAPI, model, ['widgetId']);
  macro.setGet(publicAPI, model, [
    'viewportActive',
    'widgetCircle',
    'border'
  ]);

  macro.setGetArray(publicAPI, model, ['point', 'referenceLines'], 2);
  macro.setGetArray(publicAPI, model, ['apis', 'strokeColors'], 3);

  vtkSVGDetachedRotatableCrosshairWidget(publicAPI, model);
}

// ----------------------------------------------------------------------------

export const newInstance = macro.newInstance(
  extend,
  'vtkSVGDetachedRotatableCrosshairWidget'
);

// ----------------------------------------------------------------------------

export default Object.assign({ newInstance, extend });
