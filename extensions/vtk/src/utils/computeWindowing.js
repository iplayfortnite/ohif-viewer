export function getWindowingDifference(srcWindowingDiff, srcApi, tgtApi) {
  const sourceIstyle = srcApi.genericRenderWindow.getInteractor().getInteractorStyle();
  const sourceRange = sourceIstyle.getVolumeActor()
    .getMapper()
    .getInputData()
    .getPointData()
    .getScalars()
    .getRange();
  const sourceImageDynamicRange = sourceRange[1] - sourceRange[0];
  const sourceMultiplier = Math.round(sourceImageDynamicRange / 1024) * sourceIstyle.getLevelScale();

  const targetIstyle = tgtApi.genericRenderWindow.getInteractor().getInteractorStyle();
  const targetRange = targetIstyle.getVolumeActor()
    .getMapper()
    .getInputData()
    .getPointData()
    .getScalars()
    .getRange();
  const targetImageDynamicRange = targetRange[1] - targetRange[0];
  const targetMultiplier = (targetImageDynamicRange / 1024) * targetIstyle.getLevelScale();

  const windowWidth = Math.round((srcWindowingDiff.windowWidth / sourceMultiplier) * targetMultiplier);
  const windowCenter = Math.round((srcWindowingDiff.windowCenter / sourceMultiplier) * targetMultiplier);
  return { windowWidth, windowCenter };
}
