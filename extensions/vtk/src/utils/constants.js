
export const INTERACTOR_NAMES = {
  rotatableCrosshair: 'vtkInteractorStyleDetachedRotatableMPRCrosshairs',
  crosshair: 'vtkInteractorStyleMPRFovIndicators'
};

export const INDICATOR_TYPES = {
  PLANE: "plane",
  FOV: "fov"
};

export const FOV_COLOR = "rgb(88, 245, 9)";
