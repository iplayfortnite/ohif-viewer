import { vec3 } from 'gl-matrix';
import { INDICATOR_TYPES, FOV_COLOR } from '../constants.js';
import { FovIndicatorData } from './IndicatorData.js';
import { getIndicatorHitStatus } from './IndicatorUtils.js';

/**
 * Move the position in the direction and by scaling
 * @param {Object} position - current position
 * @param {Object} direction - direction to move
 * @param {number} scaleDistance - scale the direction
 * @returns {Object} moved position
 */
const getMovedPosition = (position, direction, scale) => {
  if (position && direction && scale) {
    const resultPos = vec3.create();
    vec3.scaleAndAdd(resultPos, position, direction, scale);

    return [resultPos[0], resultPos[1], resultPos[2]];
  }
  return null;
};

/**
 * Retrieves the fov indicators data in world(patient) position with the given center and diameters
 * @param {Object} fovCenter - fov center
 * @param {Array} fovDiameters - diameters for creating the fov circles
 * @param {boolean} hideFovGrid - skip fov grid indicator creation if true, else create
 * @param {Object} api - api
 * @returns {Array} fov indicator collection in world co-ordinate
 */
export const getFovIndicatorDataInWorld = (
  fovCenter,
  fovDiameters,
  hideFovGrid,
  api,
  shouldSelectCenter = false
) => {
  const fovIndicatorsDataInWorld = [];

  const fovCenterVec = vec3.fromValues(...fovCenter);

  // Find the row and column cosine of the current rendered plane from normal and up direction
  const rowCos = vec3.create();
  const colCos = vec3.create();
  const sliceNormalForApi = api.getSliceNormal();
  const viewUpForApi = api.getViewUp();
  const normal = vec3.fromValues(...sliceNormalForApi);
  const upDir = vec3.fromValues(...viewUpForApi);
  vec3.cross(rowCos, normal, upDir);
  vec3.scale(colCos, upDir, -1);

  const fovRadiuses = fovDiameters.map(diameter => diameter / 2);
  fovRadiuses.sort((a, b) => a - b);

  const direction = rowCos;

  const fovIndicator = new FovIndicatorData(
    'crosshair',
    shouldSelectCenter ? 'yellow' : FOV_COLOR
  );
  fovIndicator.CenterPos = fovCenter;
  fovIndicator.isSelected = shouldSelectCenter;
  fovIndicatorsDataInWorld.push(fovIndicator);

  if (!hideFovGrid) {
    let smallestFovRadiusPt = null;
    let highestFovRadiusPt = null;
    fovRadiuses.forEach((radius, index) => {
      const radiusPt = getMovedPosition(fovCenterVec, direction, radius);
      if (radiusPt) {
        const fovIndicator = new FovIndicatorData('circle', FOV_COLOR);
        fovIndicator.CenterPos = fovCenter;
        fovIndicator.RadiusPoint = radiusPt;
        fovIndicatorsDataInWorld.push(fovIndicator);
        // Keep the smallest and largest radius fov's for diagonal indicator
        if (index === 0) {
          smallestFovRadiusPt = radiusPt;
        } else if (index === fovRadiuses.length - 1) {
          highestFovRadiusPt = radiusPt;
        }
      }
    });

    if (smallestFovRadiusPt?.length && highestFovRadiusPt?.length) {
      const fovIndicator = new FovIndicatorData('diagonal', FOV_COLOR);
      fovIndicator.CenterPos = fovCenter;
      fovIndicator.StartPoint = smallestFovRadiusPt;
      fovIndicator.EndPoint = highestFovRadiusPt;
      fovIndicatorsDataInWorld.push(fovIndicator);
    }
  }

  return fovIndicatorsDataInWorld;
};

/**
 * Get the fov indicator points(merging all positions of indicator) in world co-ordinate
 * Diagonal indicator is not considered as it have no interactive behavior
 * @param {Object} indicatorData - indicator data
 * @param {number} selectedIndex - selected fov index
 * @param {Object} api - vtk js api object
 * @returns {Object} fov indicator info with positions in world co-ordinate
 */
export const getFovIndicatorPointsInWorld = (
  indicatorsDataInWorld,
  selectedIndex,
  api
) => {
  const fovIndicatorInfo = { isCenterEnabled: false, isPatientPosSync: false };
  const sliceNormal = api.getSliceNormal();
  indicatorsDataInWorld.forEach((indicatorData, index) => {
    if (indicatorData.Type === INDICATOR_TYPES.FOV) {
      if (indicatorData.Shape === 'circle' && indicatorData.isSelected) {
        fovIndicatorInfo.center = indicatorData.CenterPos;
        fovIndicatorInfo.isPatientPosSync =
          selectedIndex === index ? false : fovIndicatorInfo.isPatientPosSync;
        fovIndicatorInfo.referenceLineDir = sliceNormal;
        fovIndicatorInfo.circumferencePts = [];

        // Find the fov circle circumference points for syncing and displaying reference
        // line in 2D
        const renderer = api.genericRenderWindow.getRenderer();
        const camera = renderer.getActiveCamera();
        const directionOfProjection = camera.getDirectionOfProjection();
        const axisIndex = directionOfProjection.findIndex(val => val !== 0);
        const DEG_TO_RAD = Math.PI / 180;

        for (let angle = 0; angle < 360; angle += 0.5) {
          const radian = angle * DEG_TO_RAD;
          const circumferencePt = vec3.create();
          if (axisIndex === 0) {
            vec3.rotateX(
              circumferencePt,
              indicatorData.RadiusPoint,
              indicatorData.CenterPos,
              radian
            );
          } else if (axisIndex === 1) {
            vec3.rotateY(
              circumferencePt,
              indicatorData.RadiusPoint,
              indicatorData.CenterPos,
              radian
            );
          } else {
            vec3.rotateZ(
              circumferencePt,
              indicatorData.RadiusPoint,
              indicatorData.CenterPos,
              radian
            );
          }
          fovIndicatorInfo.circumferencePts.push(circumferencePt);
        }
      } else if (indicatorData.Shape === 'crosshair') {
        fovIndicatorInfo.center = indicatorData.CenterPos;
        fovIndicatorInfo.isPatientPosSync =
          selectedIndex === index ? true : fovIndicatorInfo.isPatientPosSync;
        fovIndicatorInfo.isCenterEnabled = indicatorData.isSelected;
        fovIndicatorInfo.referenceLineDir = sliceNormal;
      }
    }
  });
  return fovIndicatorInfo;
};

/**
 * Finds fov indicator from the clicked mouse position
 * @param {Array} indicatorsData - indicator data collection in display co-ordinate
 * @param {Object} callData - click event data from vtk js
 * @returns {number} hit fov indicator index
 */
export const getClickedFovIndex = (indicatorsData, callData, shape = null) => {
  let indicatorIndex = -1;
  if (indicatorsData && indicatorsData.length > 0) {
    indicatorsData.forEach((indicator, index) => {
      if (indicator.Type === INDICATOR_TYPES.FOV) {
        if (shape && indicator.Shape !== shape) {
          return;
        }
        // Finds the clicked indicator index(taking the last matching item)
        if (getIndicatorHitStatus(indicator, callData)) {
          indicatorIndex = index;
        }
      }
    });
  }
  return indicatorIndex;
};

/**
 * Finds fov indicator from the clicked mouse position
 * @param {Array} indicatorsData - indicator data collection in display co-ordinate
 * @param {Object} callData - click event data from vtk js
 * @returns {number} hit fov indicator index
 */
export const getClickedFovRingIndex = (indicatorsData, callData) => {
  return getClickedFovIndex(indicatorsData, callData, 'circle');
};

/**
 * Is the center indicator enabled or not
 * @param {Array} indicatorsDataInWorld - indicator collection with world co-ordinate points
 * @returns {boolean} true if enabled
 */
export const isCenterEnabled = indicatorsDataInWorld => {
  let isCenterEnabled = false;
  if (indicatorsDataInWorld && indicatorsDataInWorld.length > 0) {
    indicatorsDataInWorld.forEach((indicator, index) => {
      if (indicator.Shape === 'crosshair') {
        isCenterEnabled = indicator.isSelected;
      }
    });
  }
  return isCenterEnabled;
};

/**
 * Is the center indicator enabled or not
 * @param {Array} indicatorsDataInWorld - indicator collection with world co-ordinate points
 * @returns {boolean} true if enabled
 */
export const isClickedOnCenter = (indicatorsData, callData) => {
  const fovIndex = getClickedFovIndex(indicatorsData, callData);
  return fovIndex !== -1 && indicatorsData[fovIndex]?.Shape === 'crosshair';
};

/**
 * Update the indicator center and adjust the other related points in the indicator
 * @param {Object} fovCenter - fov center
 * @param {Array} indicatorsDataInWorld - indicator collection with world co-ordinate points
 * @returns {Array} indicatorsDataInWorld - updated indicator collection with world co-ordinate points
 */
export const updateFovIndicatorCenter = (fovCenter, indicatorsDataInWorld) => {
  if (indicatorsDataInWorld && indicatorsDataInWorld.length > 0) {
    indicatorsDataInWorld.forEach((indicator, index) => {
      if (
        indicator.Shape !== 'crosshair' &&
        indicator.Shape !== 'circle' &&
        indicator.Shape !== 'diagonal'
      ) {
        return;
      }
      // Find the direction between the current center and new center and update other points by moving in that
      // direction
      const fovCenterVec = vec3.fromValues(...fovCenter);
      const curCenterVec = vec3.fromValues(...indicator.CenterPos);
      const direction = vec3.create();
      vec3.sub(direction, fovCenterVec, curCenterVec);

      indicator.CenterPos = fovCenter;

      if (indicator.Shape === 'circle') {
        const curRadiusPt = indicator.RadiusPoint;
        const curRadiusPtVec = vec3.fromValues(...curRadiusPt);
        indicator.RadiusPoint = getMovedPosition(curRadiusPtVec, direction, 1);
      } else if (indicator.Shape === 'diagonal') {
        const curStartPtVec = vec3.fromValues(...indicator.StartPoint);
        const curEndPtVec = vec3.fromValues(...indicator.EndPoint);
        indicator.StartPoint = getMovedPosition(curStartPtVec, direction, 1);
        indicator.EndPoint = getMovedPosition(curEndPtVec, direction, 1);
      }
    });
  }
  return indicatorsDataInWorld;
};

/**
 * Update the select/unselect property of all the fov circle and crosshair indicators
 * @param {Array} indicatorsDataInWorld - indicator collection with world co-ordinate points
 * @param {number} selectionIndex - selected indicator index, if -1 then unselect the previously selected
 * @returns {Array} indicatorsDataInWorld - updated indicator collection
 */
export const updateFovIndicatorSelection = (
  indicatorsDataInWorld,
  selectionIndex
) => {
  if (indicatorsDataInWorld && indicatorsDataInWorld.length > 0) {
    const selectedShape =
      selectionIndex >= 0 ? indicatorsDataInWorld[selectionIndex]?.Shape : null;
    indicatorsDataInWorld.forEach((indicator, index) => {
      const isClicked = index === selectionIndex;
      if (indicator.Shape === 'circle' && selectedShape === indicator.Shape) {
        indicator.isSelected = isClicked ? !indicator.isSelected : false;
        indicator.Color = indicator.isSelected ? 'yellow' : FOV_COLOR;
      } else if (indicator.Shape === 'crosshair') {
        indicator.isSelected = isClicked
          ? !indicator.isSelected
          : indicator.isSelected;
        indicator.Color = indicator.isSelected ? 'yellow' : FOV_COLOR;
      }
    });
  }
  return indicatorsDataInWorld;
};
