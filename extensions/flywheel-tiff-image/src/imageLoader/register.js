import loadImage from './loadImage.js';

export default function(cornerstone) {
  cornerstone.registerImageLoader('tiff', loadImage);
}
