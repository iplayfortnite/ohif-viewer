import loadImage from './loadImage.js';
import register from './register.js';
import { toTiffSchema } from './getImageId.js';

export default {
  loadImage,
  register,
  toTiffSchema,
};
