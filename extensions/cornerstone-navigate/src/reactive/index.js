import viewport from './viewport';

const reactive = {
  viewport,
};

export default reactive;
