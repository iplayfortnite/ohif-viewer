/*
Copyright (c) MONAI Consortium
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React, { Component, useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { UIModalService, UINotificationService } from '@ohif/core';
import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import CornerstoneNifti from '@flywheel/extension-cornerstone-nifti';
import * as cornerstoneNIFTIImageLoader from '@cornerstonejs/nifti-image-loader';
import cloneDeep from 'lodash.clonedeep';
import classnames from 'classnames';

import './SegmentationList.styl';

import {
  createSegment,
  clearSegment,
  deleteSegment,
  getFirstSegmentId,
  getSegmentInfo,
  updateSegment,
  updateSegmentMeta,
  createSegmentItem,
  setSegmentToolActive,
} from '../utils/SegmentationUtils';
import SegmentationLabelForm from './SegmentationLabelForm';
import {
  hexToRgb,
  randomName,
  randomRGB,
  rgbToHex,
  getLabelColor,
} from '../utils/GenericUtils';
import SegmentationReader from '../utils/SegmentationReader';
import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import { connect } from 'react-redux';
import csTools from 'cornerstone-tools';
import { getActiveLabelmapIndex } from '@flywheel/extension-cornerstone-infusions';

const {
  setSmartCTRange,
  setAvailableSmartRanges,
} = FlywheelCommonRedux.actions;
const { getSmartCTRanges } = FlywheelCommonRedux.smartCTRanges;
const { isSliceOrderSame } = FlywheelCommonUtils;
const segmentationModule = csTools.getModule('segmentation');

const nifti = cornerstoneNIFTIImageLoader.nifti;
const ImageId = nifti.ImageId;
const defaultColor = 'rgba(255, 255, 0, 0.2)';
const ColorPicker = require('a-color-picker');

const ColoredCircle = props => {
  const { color, segmentIndex, label, isSegmentationInEditMode } = props;
  const [segmentColor, setSegmentColor] = useState(color);
  const [showColorPicker, setShowColorPicker] = useState(false);
  const [colorPickCoordinates, setColorPickCoordinates] = useState();
  const [selectedColor, setSelectedColor] = useState();
  const ref = useRef(null);

  useEffect(() => {
    onColorChangeHandler();
  }, [showColorPicker, colorPickCoordinates]);

  useEffect(() => {
    document.addEventListener('click', handleClickOutside, true);
    return () => {
      document.removeEventListener('click', handleClickOutside, true);
    };
  }, [ref]);

  const closeColorPicker = () => {
    setShowColorPicker(false);
  };

  function handleClickOutside(event) {
    if (ref.current && !ref.current.contains(event.target)) {
      closeColorPicker();
    }
  }

  const refreshCornerstoneViewports = () => {
    cornerstone.getEnabledElements().forEach(enabledElement => {
      if (enabledElement.image) {
        cornerstone.updateImage(enabledElement.element);
      }
    });
  };

  const getPalette = () => {
    const state = store.getState();
    const palette = state.colorPalette?.palette;
    return palette;
  };

  const onColorChangeHandler = () => {
    const state = store.getState();
    const smartCTRanges = state.smartCT?.smartCTRanges;
    ColorPicker.from('.picker').on('change', picker => {
      const colorSelected = `rgba(${picker.rgba.join(',')})`;
      let rgbaValues = colorSelected.match(/\d+\.\d+|\d+/g);
      rgbaValues = rgbaValues.map(x => parseFloat(x));
      if (smartCTRanges) {
        const ranges = cloneDeep(smartCTRanges);
        const index = ranges.findIndex(x => x.label === label);
        ranges[index].color = rgbaValues;
        store.dispatch(setAvailableSmartRanges(ranges));
      }
      rgbaValues[3] = rgbaValues[3] * 255;
      csTools.store.modules.segmentation.state.colorLutTables[0][
        segmentIndex
      ] = rgbaValues;
      setSegmentColor(rgbaValues);
      refreshCornerstoneViewports();
    });
  };

  const getColorSelectorDialog = () => {
    if (isSegmentationInEditMode) {
      return;
    }
    let palette = getPalette();
    return (
      <div
        ref={ref}
        tabIndex={1}
        className="color-picker"
        style={{
          left: colorPickCoordinates.left,
          top: colorPickCoordinates.top,
        }}
        onClick={e => e.stopPropagation()}
        onMouseDown={e => {
          e.preventDefault();
        }}
      >
        <div
          className="picker"
          acp-color={selectedColor}
          acp-palette={palette.join('|')}
          acp-show-rgb="no"
          acp-show-hsl="no"
          acp-show-hex="no"
          acp-show-alpha="yes"
          acp-palette-editable="no"
          acp-use-alpha-in-palette="no"
        ></div>
      </div>
    );
  };

  const onClickColorPicker = event => {
    const screenHeight = window.innerHeight;

    // Align dialog 20px above or below from clicked point to not overlap the picker dialog over the palette and other buttons.
    const offset = 20;

    const pickerSize = {
      height: 297, // Color selector dialog height
      width: 232, // Color selector dialog width
    };

    const colorPickCoordinates = {
      left: event.clientX - pickerSize.width / 2,
      top:
        screenHeight / 2 > event.clientY
          ? event.clientY + offset
          : event.clientY - offset - pickerSize.height,
    };
    setColorPickCoordinates(colorPickCoordinates);
    setSelectedColor(defaultColor);
    setShowColorPicker(true);
  };

  return (
    <div tabIndex={1}>
      <span
        onClick={onClickColorPicker}
        className="segColor"
        style={{
          backgroundColor: `rgba(${segmentColor?.[0]}, ${segmentColor?.[1]}, ${segmentColor?.[2]}, ${segmentColor?.[3]} )`,
        }}
      />
      {showColorPicker && getColorSelectorDialog()}
    </div>
  );
};

ColoredCircle.propTypes = {
  color: PropTypes.array.isRequired,
};

class SegmentationList extends Component {
  static propTypes = {
    viewConstants: PropTypes.any,
    onSegmentCreated: PropTypes.func,
    onSegmentUpdated: PropTypes.func,
    onSegmentDeleted: PropTypes.func,
    onSegmentSelected: PropTypes.func,
    selectedRange: PropTypes.object,
    smartCTRanges: PropTypes.any,
    smartCTRangesFromConfig: PropTypes.any,
    segmentationData: PropTypes.any,
  };

  constructor(props) {
    super(props);

    this.notification = UINotificationService.create({});
    this.uiModelService = UIModalService.create({});
    const { element } = this.props.viewConstants;
    if (this.props.smartCTRangesFromConfig) {
      this.updateColorLUT();
    }
    const segments = [];
    const smartCTRangeList = this.props.smartCTRanges;
    for (let k = 0; k < smartCTRangeList.length; k++) {
      const segmentItem = createSegmentItem(smartCTRangeList[k], k + 1);
      segments.push(segmentItem);
    }
    const segmentItem = this.setSelectedSegmentId(
      this.props.selectedRange,
      segments
    );
    this.state = {
      element: element,
      segments: segments,
      selectedSegmentId: segmentItem?.id,
      header: null,
      disableColorSelection: false,
      activeLabelmapIndex: 0,
    };
  }
  setSelectedSegmentId(range, segments = this.state.segments) {
    const selectedRange = range;
    let segmentIndex = this.props.smartCTRanges.findIndex(
      r => r.label === selectedRange?.label
    );
    if (segmentIndex < 0) {
      segmentIndex = 0;
    }
    const segmentItem = segments?.[segmentIndex];
    return segmentItem;
  }

  componentDidMount() {
    const activeLabelmapIndex = getActiveLabelmapIndex();
    if (this.state.activeLabelmapIndex !== activeLabelmapIndex) {
      this.setState({
        activeLabelmapIndex: activeLabelmapIndex,
        disableColorSelection: activeLabelmapIndex > 0,
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps?.selectedRange) {
      const segmentItem = this.setSelectedSegmentId(nextProps.selectedRange);
      this.setState({ selectedSegmentId: segmentItem?.id });
    }
  }
  getSelectedActiveIndex = () => {
    const id = this.state.selectedSegmentId;
    if (id) {
      let index = id.split('+').map(Number);
      return { id, labelmapIndex: index[0], segmentIndex: index[1] };
    }
    return { labelmapIndex: 0, segmentIndex: 1 };
  };

  getIndexByName = name => {
    for (let i = 0; i < this.state.segments.length; i++) {
      if (this.state.segments[i].meta.SegmentLabel == name) {
        let id = this.state.segments[i].id;
        let index = id.split('+').map(Number);
        return { id, labelmapIndex: index[0], segmentIndex: index[1] };
      }
    }
    return null;
  };

  getNameByIndex = index => {
    const id = index.id;
    for (let i = 0; i < this.state.segments.length; i++) {
      if (this.state.segments[i].id == id) {
        return this.state.segments[i].meta.SegmentLabel;
      }
    }
    return null;
  };

  onAddSegment = (
    name,
    description,
    color,
    selectActive = true,
    newLabelMap = false
  ) => {
    this.uiModelService.hide();
    const { element } = this.props.viewConstants;
    const { id } = createSegment(
      element,
      name,
      description,
      hexToRgb(color),
      newLabelMap
    );
    this.refreshSegTable(id, selectActive);
    if (this.props.onSegmentCreated) {
      this.props.onSegmentCreated(id);
    }
  };

  onUpdateSegment = (name, description, color) => {
    this.uiModelService.hide();

    const { element } = this.props.viewConstants;
    const activeIndex = this.getSelectedActiveIndex();
    updateSegmentMeta(
      element,
      activeIndex.labelmapIndex,
      activeIndex.segmentIndex,
      name,
      description,
      hexToRgb(color)
    );
    this.refreshSegTable(activeIndex.id);

    if (this.props.onSegmentUpdated) {
      this.props.onSegmentUpdated(activeIndex.id);
    }
  };

  onSelectSegment = segment => {
    const smartCTRangeList = this.props.smartCTRanges;
    this.setState({ selectedSegmentId: segment.id });
    store.dispatch(setSmartCTRange(smartCTRangeList[segment.segmentIndex - 1]));
    setSegmentToolActive();
  };

  onClickAddSegment = () => {
    this.uiModelService.show({
      content: SegmentationLabelForm,
      title: 'Add New Label',
      contentProps: {
        name: randomName(),
        description: '',
        color: randomRGB(),
        onSubmit: this.onAddSegment,
      },
      customClassName: 'segmentationLabelForm',
      shouldCloseOnEsc: true,
    });
  };

  onClickEditSegment = () => {
    const { element } = this.props.viewConstants;
    const activeIndex = this.getSelectedActiveIndex();
    const { name, description, color } = getSegmentInfo(
      element,
      activeIndex.labelmapIndex,
      activeIndex.segmentIndex
    );

    this.uiModelService.show({
      content: SegmentationLabelForm,
      title: 'Edit Label',
      contentProps: {
        name: name,
        description: description,
        color: rgbToHex(
          Math.floor(color[0]),
          Math.floor(color[1]),
          Math.floor(color[2])
        ),
        onSubmit: this.onUpdateSegment,
      },
      customClassName: 'segmentationLabelForm',
      shouldCloseOnEsc: true,
    });
  };

  onUpdateLabelOrDesc = (id, evt, label) => {
    const { element } = this.props.viewConstants;
    let index = id.split('+').map(Number);
    const labelmapIndex = index[0];
    const segmentIndex = index[1];

    updateSegmentMeta(
      element,
      labelmapIndex,
      segmentIndex,
      label ? evt.currentTarget.textContent : undefined,
      label ? undefined : evt.currentTarget.textContent
    );
  };

  onClickDeleteSegment = () => {
    const { element } = this.props.viewConstants;
    const activeIndex = this.getSelectedActiveIndex();

    deleteSegment(element, activeIndex.labelmapIndex, activeIndex.segmentIndex);
    this.setState({ selectedSegmentId: null });
    this.refreshSegTable(null);

    if (this.props.onSegmentDeleted) {
      this.props.onSegmentDeleted(activeIndex.id);
    }
  };

  onDeleteSegmentByName = name => {
    this.onDeleteSegmentByIndex(this.getIndexByName(name));
  };

  onDeleteSegmentByIndex = selectedIndex => {
    const { element } = this.props.viewConstants;

    if (selectedIndex) {
      deleteSegment(
        element,
        selectedIndex.labelmapIndex,
        selectedIndex.segmentIndex,
        this.state.header
      );
      this.setState({ selectedSegmentId: null });
      this.refreshSegTable(null);

      if (this.props.onSegmentDeleted) {
        this.props.onSegmentDeleted(selectedIndex.id);
      }
    }
  };

  onClearSegmentByName = name => {
    this.onClearSegmentByIndex(this.getIndexByName(name));
  };

  onClearSegmentByIndex = selectedIndex => {
    const { element } = this.props.viewConstants;

    if (selectedIndex) {
      clearSegment(
        element,
        selectedIndex.labelmapIndex,
        selectedIndex.segmentIndex,
        this.state.header
      );
    }
  };

  onClickExportSegments = () => {
    const { getters } = cornerstoneTools.getModule('segmentation');
    const { labelmaps3D } = getters.labelmaps3D(
      this.props.viewConstants.element
    );
    if (!labelmaps3D) {
      return;
    }

    this.notification.show({
      title: 'MONAI Label',
      message: 'Preparing the labelmap to download as .nrrd',
      type: 'info',
      duration: 5000,
    });

    for (let i = 0; i < labelmaps3D.length; i++) {
      const labelmap3D = labelmaps3D[i];
      if (!labelmap3D) {
        continue;
      }

      const metadata = labelmap3D.metadata.data
        ? labelmap3D.metadata.data
        : labelmap3D.metadata;
      if (!metadata || !metadata.length) {
        continue;
      }

      SegmentationReader.serializeNrrdCompressed(
        this.state.header,
        labelmap3D.buffer,
        'segment-' + i + '.nrrd'
      );
    }
  };

  refreshSegTable = (id, selectActive = true) => {
    const segments = [];
    const smartCTRangeList = this.props.smartCTRanges;
    for (let k = 0; k < smartCTRangeList.length; k++) {
      const segmentItem = createSegmentItem(smartCTRangeList[k], k + 1);
      segments.push(segmentItem);
    }

    for (let i = 0; i < segments.length; i++) {
      if (
        segments[i].meta.SegmentLabel.includes('scribbles') &&
        segments[i].id === id
      ) {
        id = null;
        break;
      }
    }
    if (id && selectActive) {
      this.setState({ segments: segments, selectedSegmentId: id });
    } else {
      this.setState({ segments: segments });
    }
  };

  setActiveSegment = () => {
    const { element } = this.props.viewConstants;
    const activeIndex = this.getSelectedActiveIndex();

    const { setters, getters } = cornerstoneTools.getModule('segmentation');
    const { labelmapIndex, segmentIndex } = activeIndex;

    getters.labelmap2D(element);
    setters.activeSegmentIndex(element, segmentIndex);

    // Refresh
    cornerstone.getEnabledElements().forEach(enabledElement => {
      if (enabledElement.image) {
        cornerstone.updateImage(enabledElement.element);
      }
    });
    if (this.props.onSegmentSelected) {
      this.props.onSegmentSelected(activeIndex.id);
    }
    const smartCTRangeList = this.props.smartCTRanges;
    store.dispatch(setSmartCTRange(smartCTRangeList[segmentIndex - 1]));
  };

  createImageId(url) {
    return new ImageId.fromURL(url);
  }

  updateView = async (
    response,
    labels,
    operation,
    slice,
    overlap,
    selectedIndex
  ) => {
    const { element, numberOfFrames } = this.props.viewConstants;
    if (!selectedIndex) {
      selectedIndex = this.getSelectedActiveIndex();
    }
    const isNIfTI = true;
    const { header, image } = isNIfTI
      ? await SegmentationReader.parseNIfTIData(response.data)
      : SegmentationReader.parseNrrdData(response.data);
    this.setState({ header: header });

    const imageIdFormat = this.createImageId(image.imageId);
    const dataType = header.dataType.TypedArrayConstructor;
    const buffer = new dataType(
      (image.sizeInBytes * header.numberOfFrames) / 2
    );

    const toolState = cornerstoneTools.getToolState(element, 'stack');
    const stackData = toolState.data[0];
    const { imageOrientationPatient } = cornerstone.metaData.get(
      'imagePlaneModule',
      stackData.imageIds[0]
    );
    const loadImage = CornerstoneNifti.getSopClassHandlerModule().loadImage;
    let isSameSliceOrder = true;

    for (let i = 0; i < header.numberOfFrames; i++) {
      const frameImageId = new ImageId(
        imageIdFormat.filePath,
        {
          dimension: imageIdFormat.slice.dimension,
          index: isSameSliceOrder ? i : numberOfFrames - 1 - i,
        },
        imageIdFormat.timePoint
      );
      const frameImage = await loadImage(
        frameImageId.url,
        imageOrientationPatient
      );

      // If slice ordering is different in segmentation data when compared
      // with base dicom, reverse the segmentation slices.
      if (i == 1 && isSameSliceOrder) {
        const segImageIds = [];
        imageIdFormat.slice.index = 0;
        segImageIds.push(imageIdFormat.url);
        segImageIds.push(frameImageId.url);
        isSameSliceOrder = isSliceOrderSame(stackData.imageIds, segImageIds);

        if (!isSameSliceOrder) {
          i = -1;
          continue;
        }
      }
      let pixelData = new dataType(frameImage.getPixelData().buffer);
      buffer.set(pixelData, i * pixelData.length);
    }

    if (labels) {
      let i = 0;
      for (var label in labels) {
        if (Array.isArray(labels)) {
          label = labels[label];
        }

        if (label === 'background') {
          continue;
        }

        const resp = createSegment(
          element,
          label,
          '',
          getLabelColor(label),
          false
        );
        if (i === 0) {
          selectedIndex = resp;
        }
        i++;
      }
    }

    if (!operation && overlap) {
      operation = 'overlap';
    }

    const refreshSegmentationTableOnUpdate = () => {
      if (this.state.selectedSegmentId) {
        this.refreshSegTable();
      } else {
        this.refreshSegTable(selectedIndex.id);
      }
    };

    updateSegment(
      element,
      selectedIndex.labelmapIndex,
      selectedIndex.segmentIndex,
      buffer.buffer,
      numberOfFrames,
      operation,
      slice
    ).then(() => {
      refreshSegmentationTableOnUpdate();
    });
    refreshSegmentationTableOnUpdate();
    setSegmentToolActive();
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.selectedSegmentId !== this.state.selectedSegmentId) {
      if (this.state.selectedSegmentId) {
        this.setActiveSegment();
      }
    }
    const activeLabelmapIndex = getActiveLabelmapIndex();
    if (this.state.activeLabelmapIndex !== activeLabelmapIndex) {
      this.setState({
        activeLabelmapIndex: activeLabelmapIndex,
        disableColorSelection: activeLabelmapIndex > 0,
      });
    }
  }

  updateColorLUT() {
    let colorLUT = [];
    this.props.smartCTRangesFromConfig.forEach(ranges => {
      colorLUT.push([...ranges.color.slice(0, 3), ranges.color[3] * 255]);
    });
    segmentationModule.setters.colorLUT(0, colorLUT);
  }

  displaySegmentationColorRanges = range => {
    let colorRange = '';
    if (isFinite(range?.min) && isFinite(range?.max)) {
      colorRange = `${range?.min} - ${range?.max}`;
    }
    return colorRange;
  };

  render() {
    const disabled = this.state.disableColorSelection;
    const segmentId = this.state.selectedSegmentId
      ? this.state.selectedSegmentId
      : getFirstSegmentId(this.props.viewConstants.element);

    return (
      <div className={classnames('segmentationList', { disabled })}>
        {/* <table width="100%">
          <tbody>
            <tr>
              <td>
                <button
                  className="segButton"
                  onClick={this.onClickAddSegment}
                  title="Add Segment"
                >
                  <Icon name="plus" width="12px" height="12px" />
                </button>
                &nbsp;
                <button
                  className="segButton"
                  onClick={this.onClickEditSegment}
                  title="Edit Selected Segment"
                  disabled={!segmentId}
                >
                  <Icon name="edit" width="12px" height="12px" />
                </button>
                &nbsp;
                <button
                  className="segButton"
                  onClick={this.onClickDeleteSegment}
                  title="Delete Selected Segment"
                  disabled={!segmentId}
                >
                  <Icon name="trash" width="12px" height="12px" />
                </button>
              </td>
              <td align="right">
                <button
                  className="segButton"
                  onClick={this.onClickExportSegments}
                  title={'Download Segments'}
                  disabled={!this.state.header}
                >
                  <Icon name="save" width="12px" height="12px" />
                </button>
              </td>

            </tr>
          </tbody>
        </table> */}

        <div className="segSection">
          <table className="segTable">
            <thead>
              <tr>
                <th width="5%">#</th>
                <th width="10%">Color</th>
                <th width="60%">Name</th>
                <th width="40%">Range</th>
              </tr>
            </thead>
            <tbody>
              {this.state.segments
                .filter(
                  // do not display anything related to scribbles
                  function(seg) {
                    return !seg?.meta?.SegmentLabel?.includes('scribbles');
                  }
                )
                .map(seg => (
                  <tr key={seg.id}>
                    <td>
                      <input
                        type="radio"
                        name="segitem"
                        value={seg.id}
                        disabled={
                          this.state.activeLabelmapIndex > 0 &&
                          this.state.selectedSegmentId !== seg.id
                        }
                        checked={seg.id === this.state.selectedSegmentId}
                        onChange={() => this.onSelectSegment(seg)}
                      />
                    </td>
                    <td className="text-center">
                      <ColoredCircle
                        color={seg.color}
                        segmentIndex={seg.segmentIndex}
                        label={seg.name}
                        isSegmentationInEditMode={
                          this.state.activeLabelmapIndex > 0
                        }
                      />
                    </td>
                    <td
                      title={
                        seg.meta.SegmentDescription || seg.meta.SegmentLabel
                      }
                    >
                      {seg.meta.SegmentLabel}
                    </td>
                    <td style={{ fontSize: '12px' }}>
                      {this.displaySegmentationColorRanges(seg?.range)}
                    </td>
                    {/* <td
                      contentEditable="true"
                      suppressContentEditableWarning="true"
                      onKeyUp={evt =>
                        this.onUpdateLabelOrDesc(seg.id, evt, false)
                      }
                    >
                      {seg.meta.SegmentDescription}
                    </td> */}
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state, ownProps = {}) => ({
  selectedRange: state?.smartCT?.selectedRange,
  smartCTRanges: state?.smartCT?.smartCTRanges || getSmartCTRanges(),
  smartCTRangesFromConfig: state?.smartCT?.smartCTRanges || null,
  segmentationData: state?.segmentation?.segmentationData,
});

export default connect(mapStateToProps, null, null, {
  forwardRef: true,
})(SegmentationList);
