import React, { useState, useEffect, Fragment, useCallback } from 'react';
import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
  HTTP as FlywheelCommonHTTP,
} from '@flywheel/extension-flywheel-common';
import { Components as FlywheelComponents } from '@flywheel/extension-flywheel';
import { useDispatch, useSelector } from 'react-redux';
import {
  ToolbarButton,
  Range,
  Icon,
  OverlayObserver,
  RadioButtonList,
} from '@ohif/ui';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import './SegmentationPanel.styl';
import {
  getSelectedSegmentationData,
  getActiveLabelmapIndex,
  isSelectedSegmentMaskInEditing,
  editSegmentationMask,
  exitEditSegmentationMode,
  setSegmentToolActive,
} from '@flywheel/extension-cornerstone-infusions';
import classNames from 'classnames';
import { connect } from 'react-redux';
import store from '@ohif/viewer/src/store';

const { SaveSegmentationDialog } = FlywheelComponents;
const { measurementToolUtils } = FlywheelCommonUtils;
const { DeleteSegmentationDialog } = FlywheelComponents;
const { removeSegmentationData } = FlywheelCommonRedux.actions;
const { deleteFile } = FlywheelCommonHTTP.services;
const {
  setScissorsStrategy,
  addSegmentationData,
  setSegmentationData,
  setThresholdToleranceValue,
} = FlywheelCommonRedux.actions;
const { getActiveTool } = measurementToolUtils;
const { selectCurrentVolumetricImage } = FlywheelCommonRedux.selectors;
const { segmentationRangeManager } = FlywheelCommonUtils.commonToolUtils;

const SegmentationPanel = props => {
  const {
    activeTool,
    viewports,
    activeViewportIndex,
    servicesManager,
    commandsManager,
    segmentationDefinitions,
  } = props;
  const [
    shouldEnableSegmentationColorDropDown,
    enableSegmentationColorDropDown,
  ] = useState(false);
  const [
    shouldEnableScissorsStrategyButtons,
    enableScissorsStrategyButtons,
  ] = useState(false);
  const dispatch = useDispatch();
  const commandsData = segmentationDefinitions;
  const segmentationData =
    useSelector(store => store.segmentation.segmentationData) || {};
  const [shouldEnableSliderTolerance, enableSliderTolerance] = useState(true);
  const [sliderToleranceValue, setSliderTolerance] = useState();
  const [volumetricMaskArray, setVolumetricMaskArray] = useState([]);
  const [rtMaskArray, setRTMaskArray] = useState([]);
  const state = store.getState();
  const projectConfig = state.flywheel.projectConfig;
  const currentVolumetricImage = useSelector(selectCurrentVolumetricImage);
  const { isCurrentFile } = FlywheelCommonUtils;

  useEffect(() => {
    shouldEnableSegmentationPanelItems();
  }, [projectConfig?.toolbar]);

  const activeReferenceUID = useSelector(store => {
    let referenceUID = null;
    const { viewports = {} } = store;
    if (viewports) {
      const { viewportSpecificData, activeViewportIndex } = viewports;
      if (viewportSpecificData[activeViewportIndex]) {
        referenceUID = currentVolumetricImage
          ? viewportSpecificData[activeViewportIndex].StudyInstanceUID
          : viewportSpecificData[activeViewportIndex].SeriesInstanceUID;
      }
    }
    return referenceUID;
  });

  useEffect(() => {
    let volumetricMaskItems = [];
    let rtMaskItems = [];
    if (activeViewport && activeReferenceUID) {
      if (segmentationData[activeReferenceUID]) {
        segmentationData[activeReferenceUID].forEach(segment => {
          if (segment.modality === 'SEG') {
            volumetricMaskItems.push(segment);
          }
        });
        segmentationData[activeReferenceUID].forEach(segment => {
          if (segment.modality === 'RTSTRUCT') {
            rtMaskItems.push(segment);
          }
        });
      }
    }
    setVolumetricMaskArray(volumetricMaskItems);
    setRTMaskArray(rtMaskItems);
    const activeLabelmapIndex = getActiveLabelmapIndex();
    if (activeLabelmapIndex === undefined) {
      segmentationRangeManager.activateDefaultSegmentation();
    }
  }, [segmentationData, activeReferenceUID]);

  useEffect(() => {
    setSliderTolerance(defaultRange);
    segmentationRangeManager.activateDefaultSegmentation();
  }, []);

  const strategyOptions = [
    {
      label: 'Fill Inside',
      id: 'FILL_INSIDE',
      checked: true,
    },
    {
      label: 'Fill Outside',
      id: 'FILL_OUTSIDE',
      checked: false,
    },
    {
      label: 'Erase Inside',
      id: 'ERASE_INSIDE',
      checked: false,
    },
    {
      label: 'Erase Outside',
      id: 'ERASE_OUTSIDE',
      checked: false,
    },
  ];

  const DEFAULTSLIDER = {
    MIN: 0,
    MAX: 250,
    STEP: 1,
  };

  const isAnyScissorsToolActive =
    activeTool === 'FreehandScissors' ||
    activeTool === 'CircleScissors' ||
    activeTool === 'RectangleScissors';

  const activeViewport = useSelector(store => {
    let viewport = null;
    const { viewports = {} } = store;
    if (viewports) {
      const { viewportSpecificData, activeViewportIndex } = viewports;
      if (viewportSpecificData[activeViewportIndex]) {
        viewport = viewportSpecificData[activeViewportIndex];
      }
    }
    return viewport;
  });

  function saveSegmentationData(maskArray, activeReferenceUID, modality) {
    let combinedArray = [];
    if (modality === 'RTSTRUCT') {
      combinedArray = volumetricMaskArray.concat(maskArray);
    } else {
      combinedArray = rtMaskArray.concat(maskArray);
    }
    dispatch(setSegmentationData([...combinedArray], activeReferenceUID));
  }

  function onClickEditSegmentation(selectedSegmentItem, segmentationIndex) {
    if (isSelectedSegmentMaskInEditing(selectedSegmentItem)) {
      OverlayObserver.setToggleState();
      confirmSaveSegmentationDialog(selectedSegmentItem, segmentationIndex);
    } else {
      updateMaskEditingStatusInSegmentationData(
        selectedSegmentItem,
        segmentationIndex
      );
      editSegmentationMask(selectedSegmentItem);
      setSegmentToolActive();
    }
  }

  const editSegmentationLabel = segmentationData => {
    const editLabel = commandsData.editLabel;
    OverlayObserver.setToggleState();
    runSegmentationChangeCommand(editLabel, segmentationData, commandsManager);
  };

  const updateMaskEditingStatusInSegmentationData = (
    selectedSegmentItem,
    segmentationIndex
  ) => {
    let editingSegment = { ...selectedSegmentItem };
    editingSegment.isEditing = !editingSegment.isEditing;
    let newMaskArray = [...volumetricMaskArray];
    newMaskArray[segmentationIndex] = editingSegment;
    setVolumetricMaskArray(newMaskArray);
    saveSegmentationData(
      newMaskArray,
      activeReferenceUID,
      selectedSegmentItem.modality
    );
  };
  const onSaveOrExitEditSegmentation = (
    selectedSegmentItem,
    segmentationIndex,
    shouldSave
  ) => {
    if (shouldSave) {
      commandsManager.runCommand('saveSegments', { saveNewSegment: false });
    }
    exitEditSegmentationMode(selectedSegmentItem);
    updateMaskEditingStatusInSegmentationData(
      selectedSegmentItem,
      segmentationIndex
    );
  };

  const confirmSaveSegmentationDialog = (
    selectedSegmentItem,
    segmentationIndex
  ) => {
    const { UIDialogService } = servicesManager.services;
    const popupFromLeft = 175;
    const popupFromTop = 220;
    const dialogId = UIDialogService.create({
      content: SaveSegmentationDialog,
      defaultPosition: {
        x: window.innerWidth / 2 - popupFromLeft,
        y: window.innerHeight / 2 - popupFromTop,
      },
      showOverlay: true,
      contentProps: {
        label:
          'Please save the segmentation. This will update the existing file with new version.',
        onClose: () => UIDialogService.dismiss({ id: dialogId }),
        onExitEdit: () => {
          onSaveOrExitEditSegmentation(
            selectedSegmentItem,
            segmentationIndex,
            false
          );
          UIDialogService.dismiss({ id: dialogId });
        },
        onSave: () => {
          onSaveOrExitEditSegmentation(
            selectedSegmentItem,
            segmentationIndex,
            true
          );
          UIDialogService.dismiss({ id: dialogId });
        },
      },
    });
  };

  function runSegmentationChangeCommand(button, options, commandsManager) {
    if (button.commandName) {
      const _options = Object.assign({}, options, button.commandOptions);
      commandsManager.runCommand(button.commandName, _options);
    }
  }

  function shouldEnableSegmentationPanelItems() {
    const toolbarConfig = projectConfig?.toolbar;
    const state = store.getState();
    const segmentTools = [
      'Manual',
      'Smart CT',
      'Auto CT',
      'Circle',
      'Rectangle',
      'Freehand',
      'ThresholdTool',
    ];
    if (
      projectConfig?.enableSegmentationTools === false &&
      segmentTools.includes(state.infusions.activeTool)
    ) {
      // Set default active tool as WWWc ,
      // if last used tool was Segmentation tool
      store.dispatch(setActiveTool('Wwwc'));
      return;
    }
    if (toolbarConfig?.hasOwnProperty('Segment')) {
      const segmentConfig = toolbarConfig['Segment'];
      if (
        segmentConfig?.hasOwnProperty('except') ||
        segmentConfig?.hasOwnProperty('only')
      ) {
        const paintTools = ['Manual', 'Smart CT', 'Auto CT', 'Save Segments'];
        const scissorsTools = ['Circle', 'Rectangle', 'Freehand'];
        let index = paintTools.findIndex(tool =>
          segmentConfig['except']?.includes(tool)
        );

        if (
          paintTools.some(tool => segmentConfig['only']?.includes(tool)) ||
          index !== -1 ||
          (index === -1 && segmentConfig['except']?.length)
        ) {
          enableSegmentationColorDropDown(true);
        } else {
          enableSegmentationColorDropDown(false);
        }
        if (paintTools.every(tool => segmentConfig['except']?.includes(tool))) {
          enableSegmentationColorDropDown(false);
        }

        index = scissorsTools.findIndex(tool =>
          segmentConfig['except']?.includes(tool)
        );
        if (
          scissorsTools.some(tool => segmentConfig['only']?.includes(tool)) ||
          index !== -1 ||
          (index === -1 && segmentConfig['except']?.length)
        ) {
          enableScissorsStrategyButtons(true);
        } else {
          enableScissorsStrategyButtons(false);
        }
        if (
          scissorsTools.every(tool => segmentConfig['except']?.includes(tool))
        ) {
          enableScissorsStrategyButtons(false);
        }
        if (
          segmentConfig['only']?.includes('Threshold Tool') ||
          !segmentConfig['except']?.includes('Threshold Tool')
        ) {
          enableSliderTolerance(true);
        }
      }
    } else {
      enableSegmentationColorDropDown(true);
      enableScissorsStrategyButtons(true);
      enableSliderTolerance(true);
    }
  }

  const handleStrategy = event => {
    if (isAnyScissorsToolActive) {
      const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
        activeViewportIndex
      );
      const activeTool = getActiveTool(element, 1 /*left mouse button*/);
      const activeStrategy = event.target.value;
      store.dispatch(setScissorsStrategy(activeStrategy));
      activeTool.setActiveStrategy(activeStrategy);
    }
  };

  const customSliderRanges = projectConfig?.sliderRanges;
  const defaultRange = useSelector(store => {
    return customSliderRanges?.min || store.segmentation?.sliderRange;
  });

  const handleRangeSlider = event => {
    let val = parseInt(event.target.value, 10);
    setSliderTolerance(val);
    store.dispatch(setThresholdToleranceValue(val));
  };

  const getItemStyle = (isDragging, draggableStyle) => {
    const { transform } = draggableStyle;
    let activeTransform = {};
    if (transform) {
      activeTransform = {
        transform: `translate(0, ${transform.substring(
          transform.indexOf(',') + 1,
          transform.indexOf(')')
        )})`,
      };
    }
    return {
      // some basic styles to make the items look a bit nicer
      userSelect: 'none',
      // styles we need to apply on draggables
      ...draggableStyle,
      ...activeTransform,
    };
  };

  const handleVolumetricFileOverlayOnDragEnd = result => {
    handleOnDragEnd(result, volumetricMaskArray, setVolumetricMaskArray);
  };

  const handleRTOverlayOnDragEnd = result => {
    handleOnDragEnd(result, rtMaskArray, setRTMaskArray);
  };

  const handleOnDragEnd = (dragData, dragMaskArray, setDragMaskArray) => {
    const items = Array.from(dragMaskArray);
    const [reOrderedItems] = items.splice(dragData.source.index, 1);
    if (dragData.destination != null) {
      items.splice(dragData.destination.index, 0, reOrderedItems);
      setDragMaskArray(items);
      saveSegmentationData(items, activeReferenceUID, items[0].modality);
    }
  };

  const setLabelChangeButton = currentSegItem => {
    let className = 'seg-edit-button';

    if (currentSegItem.modality !== 'RTSTRUCT') {
      const activeLabelmapIndex = getActiveLabelmapIndex();
      if (activeLabelmapIndex && activeLabelmapIndex > 0) {
        className = 'seg-edit-disabled-button';
      }
    }

    return `popover__title material-icons edit-icon-size-toolbar icon-color ${className}`;
  };

  const setEditButton = currentSegItem => {
    let className = 'seg-edit-button';
    const {
      activeSegmentationDisplaySet,
      activeLabelmapIndex,
    } = getSelectedSegmentationData(currentSegItem);
    if (activeLabelmapIndex && activeSegmentationDisplaySet?.labelmapIndex) {
      if (activeLabelmapIndex === activeSegmentationDisplaySet.labelmapIndex) {
        className = 'seg-edit-inprogress-button';
      } else if (
        activeLabelmapIndex !== activeSegmentationDisplaySet.labelmapIndex &&
        activeLabelmapIndex > 0
      ) {
        className = 'seg-edit-disabled-button';
      }
    }
    return `popover__title material-icons edit-icon-size-toolbar icon-color ${className}`;
  };

  const deleteSegmentationFile = data => {
    if (data.containerId) {
      deleteFile(data.containerId, data.name).then(response => {
        if (response.ok) {
          dispatch(removeSegmentationData(data));
        }
      });
    }
  };

  const confirmDeleteSegmentationDialog = data => {
    const { UIDialogService } = servicesManager.services;
    const popupFromLeft = 175;
    const popupFromTop = 220;
    const dialogId = UIDialogService.create({
      content: DeleteSegmentationDialog,
      defaultPosition: {
        x: window.innerWidth / 2 - popupFromLeft,
        y: window.innerHeight / 2 - popupFromTop,
      },
      showOverlay: true,
      contentProps: {
        label:
          'This will delete the saved segmentation file. Do you want to delete the file from the system?',
        onClose: () => UIDialogService.dismiss({ id: dialogId }),
        onDelete: () => {
          deleteSegmentationFile(data);
          UIDialogService.dismiss({ id: dialogId });
        },
      },
    });
  };

  const onClickDeleteSegmentation = data => {
    confirmDeleteSegmentationDialog(data);
  };

  const loadMask = commandsData.loadMaskButton;
  const activeLabelmapIndex = getActiveLabelmapIndex();
  const { id } = loadMask;
  const shouldDisplayNewSegOptions = !isCurrentFile(state);

  function onLoadMaskClick() {
    runSegmentationChangeCommand(loadMask, {}, commandsManager);
  }

  const [segmentationVisibility, setSegmentationVisibility] = useState([
    { type: 'VOLUMETRIC', isHidden: false },
    { type: 'DICOM', isHidden: false },
  ]);

  const onClickItem = useCallback(e => {
    setSegmentationVisibility([
      ...segmentationVisibility,
      (segmentationVisibility[e].isHidden = !segmentationVisibility[e]
        .isHidden),
    ]);
  }, []);

  const onClickAddSegmentation = () => {
    const fileExtension = '.nii.gz';
    const newSegmentFile = {
      name: fileExtension,
      parent_ref: { id: '' },
      file_id: '',
    };
    const modality =
      fileExtension.includes('.nii') ||
      fileExtension.includes('.nii.gz') ||
      fileExtension.includes('.mhd')
        ? 'SEG'
        : 'RTSTRUCT';

    store.dispatch(
      addSegmentationData({
        ...newSegmentFile,
        modality: modality,
        referenceUID: activeReferenceUID,
        studyInstanceUID: newSegmentFile.name,
        seriesInstanceUID: '',
      })
    );
  };

  const SegmentationItemsList = ({ props }) => {
    const [valueInMaskArray, setValueInMaskArray] = useState(0);
    const { ids, item, setMaskArray, maskArray } = props;
    const shouldDisplayEditOptions =
      item.modality !== 'RTSTRUCT' && !isCurrentFile(state);
    return (
      <Fragment>
        <div className={'drag-maindiv'}>
          <div title={item.name} className={'load-mask-title'}>
            {item.name}
          </div>
          <div className={'seg-pointer'} title="Close">
            <ToolbarButton
              label=""
              width="15px"
              height="13px"
              className={`close-icon ${setLabelChangeButton(item)}`}
              icon="close_icon"
              onClick={() => {
                let newMaskArray = maskArray.filter(elem => {
                  if (elem.id != item.id) return item;
                });
                let filteredMaskArray = [];
                newMaskArray.map((element, i) => {
                  filteredMaskArray.push({ ...element, id: i + 1 });
                });
                setMaskArray(filteredMaskArray);
                saveSegmentationData(
                  filteredMaskArray,
                  activeReferenceUID,
                  item.modality
                );
              }}
            />
          </div>
        </div>
        <div className={'drag-innerdiv'}>
          <div className={'seg-enable'}>
            <Icon
              className={`eye-icon ${item.isVisible && 'expanded'}`}
              name={item.isVisible ? 'eye' : 'eye-closed'}
              width="20px"
              height="20px"
              onClick={() => {
                let newVisibility = { ...item };
                newVisibility.isVisible = !newVisibility.isVisible;
                let newMaskArray = [...maskArray];
                newMaskArray[ids] = newVisibility;
                setMaskArray(newMaskArray);
                saveSegmentationData(
                  newMaskArray,
                  activeReferenceUID,
                  item.modality
                );
              }}
            />
          </div>
          <div
            className={classNames('range seg-range', '')}
            style={{ pointerEvents: item.isVisible ? 'auto' : 'none' }}
          >
            <Range
              step={1}
              min={0}
              max={100}
              value={Number(item.opacity)}
              id={'seg-mask-' + ids}
              onChange={event => {
                let inputValue = event.target.value;
                setValueInMaskArray(inputValue);
              }}
              onMouseUp={() => {
                let newItem = { ...item };
                let newMaskArray = [...maskArray];
                newItem.opacity = valueInMaskArray;
                newMaskArray[ids] = newItem;
                setMaskArray(newMaskArray);
                setValueInMaskArray(0);
                saveSegmentationData(
                  newMaskArray,
                  activeReferenceUID,
                  newItem.modality
                );
              }}
            />
          </div>
          {shouldDisplayEditOptions && (
            <>
              <div className={`seg-edit`} title="Label change">
                <ToolbarButton
                  className={setLabelChangeButton(item)}
                  icon={'create'}
                  label=""
                  onClick={() => {
                    editSegmentationLabel(item);
                  }}
                />
              </div>
              <div className={`seg-edit`} title="Brush">
                <ToolbarButton
                  className={setEditButton(item)}
                  icon={'brush'}
                  label=""
                  onClick={() => {
                    onClickEditSegmentation(item, ids);
                  }}
                />
              </div>
              <div className={`seg-edit`} title="Delete">
                <ToolbarButton
                  disabled={item.containerId === ''}
                  className={setLabelChangeButton(item)}
                  icon={'trash'}
                  label=""
                  onClick={() => {
                    onClickDeleteSegmentation(item);
                  }}
                />
              </div>
            </>
          )}
        </div>
      </Fragment>
    );
  };

  const SegmentationLayerItem = props => {
    const { ids } = props;
    const activeLabelmapIndex = getActiveLabelmapIndex();
    const shouldDisplayDrag = activeLabelmapIndex === 0;
    return (
      <Fragment>
        {shouldDisplayDrag ? (
          <Draggable key={ids + 1} draggableId={'id' + ids + 1} index={ids} dra>
            {(provided, snapshot) => (
              <div
                className={'drag-segmentation'}
                id={'addEventListener'}
                ref={provided.innerRef}
                {...provided.draggableProps}
                {...provided.dragHandleProps}
                style={getItemStyle(
                  snapshot.isDragging,
                  provided.draggableProps.style
                )}
              >
                <SegmentationItemsList props={props} />
              </div>
            )}
          </Draggable>
        ) : (
          <SegmentationItemsList props={props} />
        )}
      </Fragment>
    );
  };

  return (
    <div className="segmentation-panel">
      {shouldEnableSliderTolerance && (
        <div
          className="slider-container margin-top"
          title="Sensitivity Tolerance"
        >
          <Range
            value={Number(sliderToleranceValue)}
            min={customSliderRanges?.min || DEFAULTSLIDER.MIN}
            max={customSliderRanges?.max || DEFAULTSLIDER.MAX}
            step={customSliderRanges?.step || DEFAULTSLIDER.STEP}
            onChange={handleRangeSlider}
            id="slider"
          />
          <span className="sliderLabel"> ± {sliderToleranceValue}</span>
        </div>
      )}
      {shouldEnableScissorsStrategyButtons && (
        <div
          className={classNames('radio-button-wrapper', {
            'is-scissorsTool-active': isAnyScissorsToolActive,
          })}
        >
          <RadioButtonList
            description={strategyOptions}
            onChange={e => {
              handleStrategy(e);
            }}
            disabled={isAnyScissorsToolActive}
          />
        </div>
      )}
      <div className="header-style header-title-style segmentation-title">
        <span>Segmentations</span>
        <span className="LoadMaskIcon">
          {shouldDisplayNewSegOptions && (
            <div className={`seg-edit`} title="New Segmentation">
              <ToolbarButton
                disabled={activeLabelmapIndex > 0}
                className={
                  'popover__title material-icons icon-size-toolbar icon-color seg-edit-button'
                }
                icon={'note_add'}
                label=""
                onClick={() => {
                  onClickAddSegmentation();
                }}
              />
            </div>
          )}
          <div className={`seg-edit`} title="Load Segmentation">
            <ToolbarButton
              key={id}
              disabled={activeLabelmapIndex > 0}
              label=""
              className="material-icons"
              icon="file_open"
              onClick={() => {
                OverlayObserver.setToggleState();
                onLoadMaskClick();
              }}
            />
          </div>
        </span>
      </div>

      <div className="display-flex-column enable-cursor-pointer">
        <div
          className="container-tile padding-left-5 margin-bottom-5"
          onClick={() => onClickItem(0)}
        >
          <span className="display-flex">
            <ToolbarButton
              label=""
              className="material-icons"
              icon={
                segmentationVisibility[0].isHidden
                  ? 'arrow_drop_up'
                  : 'arrow_drop_down'
              }
            />
          </span>
          <span className=""> Volumetric Files (NIfTI, MHD, etc.)</span>
          <span />
        </div>
        {!segmentationVisibility[0].isHidden && (
          <div>
            {' '}
            <DragDropContext onDragEnd={handleVolumetricFileOverlayOnDragEnd}>
              <Droppable droppableId="volumetricSegmentations">
                {provided => (
                  <div
                    className="segmentations"
                    {...provided.droppableProps}
                    ref={provided.innerRef}
                  >
                    {volumetricMaskArray.map((item, index) => {
                      return (
                        <div
                          className="SegmentationLayerItem-style"
                          key={index}
                        >
                          <SegmentationLayerItem
                            key={index}
                            value={100}
                            step={1}
                            min={0}
                            max={100}
                            ids={index}
                            item={item}
                            maskArray={volumetricMaskArray}
                            setMaskArray={setVolumetricMaskArray}
                          />
                        </div>
                      );
                    })}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </DragDropContext>
          </div>
        )}
      </div>

      <div className="display-flex-column enable-cursor-pointer">
        <div
          className="container-tile padding-left-5 margin-bottom-5"
          onClick={() => onClickItem(1)}
        >
          <span className="display-flex">
            <ToolbarButton
              label=""
              className="material-icons"
              icon={
                segmentationVisibility[1].isHidden
                  ? 'arrow_drop_up'
                  : 'arrow_drop_down'
              }
            />
          </span>
          <span className="">DICOM Files</span>
          <span />
        </div>
        {!segmentationVisibility[1].isHidden && (
          <div>
            <DragDropContext onDragEnd={handleRTOverlayOnDragEnd}>
              <Droppable droppableId="rtSegmentations">
                {provided => (
                  <div
                    className="segmentations"
                    {...provided.droppableProps}
                    ref={provided.innerRef}
                  >
                    {rtMaskArray.map((item, index) => {
                      return (
                        <SegmentationLayerItem
                          key={index}
                          value={100}
                          step={1}
                          min={0}
                          max={100}
                          segmentationIndex={index}
                          item={item}
                          maskArray={rtMaskArray}
                          setMaskArray={setRTMaskArray}
                        />
                      );
                    })}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </DragDropContext>
          </div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  activeTool: state.infusions?.activeTool,
  viewports: state.viewports || {},
  activeViewportIndex: state.viewports?.activeViewportIndex,
});

export default connect(mapStateToProps)(SegmentationPanel);
