/*
Copyright (c) MONAI Consortium
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
import { HTTP, Utils } from '@flywheel/extension-flywheel-common';
import axios from 'axios';

const { getRouteParams } = Utils;
export default class FlywheelMonaiLabelClient {
  constructor(server_url = '') {
    this.server_url = server_url ? new URL(server_url) : '';
  }

  async info() {
    let url = HTTP.urls.GET_MONAI_LABEL_INFO;
    return await FlywheelMonaiLabelClient.api_get(url);
  }

  async segmentation(model, image, params = {}, label = null) {
    // label is used to send label volumes, e.g. scribbles,
    // that are to be used during segmentation
    return this.infer(model, image, params, label);
  }

  async deepgrow(model, image, foreground, background, params = {}) {
    params['foreground'] = foreground;
    params['background'] = background;
    return this.infer(model, image, params);
  }

  async infer(model, image, params, label = null, result_extension = '') {
    const param = getRouteParams(window.history.location);
    const project_id = param.projectId;

    const urlOptions = {
      url: HTTP.urls.UPLOAD_MONAI_LABEL_SEGMENTATION,
      urlParams: {
        pathParams: {
          monaiLabelModel: encodeURIComponent(model),
        },
        queryParams: {
          image,
          output: 'image',
          project_id,
        },
      },
    };
    if (result_extension) {
      params.result_extension = result_extension;
      params.result_dtype = 'uint16';
      params.result_compress = false;
    }
    const config = {
      headersMap: {
        responseType: 'arraybuffer',
      },
    };
    return await FlywheelMonaiLabelClient.api_post(
      urlOptions,
      params,
      label,
      true,
      config
    );
  }

  async next_sample(stategy = 'random', params = {}) {
    const url = new URL(
      'activelearning/' + encodeURIComponent(stategy),
      this.server_url
    ).toString();

    return await FlywheelMonaiLabelClient.api_post(
      url,
      params,
      null,
      false,
      'json'
    );
  }

  async save_label(image, label, params) {
    let url = new URL('datastore/label', this.server_url);
    url.searchParams.append('image', image);
    url = url.toString();

    const data = FlywheelMonaiLabelClient.constructFormDataFromArray(
      params,
      label,
      'label',
      'label.bin'
    );

    return await FlywheelMonaiLabelClient.api_put_data(url, data, 'json');
  }

  async is_train_running() {
    let url = new URL('train', this.server_url);
    url.searchParams.append('check_if_running', 'true');
    url = url.toString();

    const response = await FlywheelMonaiLabelClient.api_get(url);
    return (
      response && response.status === 200 && response.data.status === 'RUNNING'
    );
  }

  async run_train(params) {
    const url = new URL('train', this.server_url).toString();
    return await FlywheelMonaiLabelClient.api_post(
      url,
      params,
      null,
      false,
      'json'
    );
  }

  async stop_train() {
    const url = new URL('train', this.server_url).toString();
    return await FlywheelMonaiLabelClient.api_delete(url);
  }

  static constructFormDataFromArray(params, data, name, fileName) {
    let formData = new FormData();
    formData.append('params', JSON.stringify(params));
    formData.append(name, data, fileName);
    return formData;
  }

  static constructFormData(params, files) {
    let formData = new FormData();
    formData.append('params', JSON.stringify(params));

    if (files) {
      if (!Array.isArray(files)) {
        files = [files];
      }
      for (let i = 0; i < files.length; i++) {
        formData.append(files[i].name, files[i].data, files[i].fileName);
      }
    }
    return formData;
  }

  static constructFormOrJsonData(params, files) {
    return files
      ? FlywheelMonaiLabelClient.constructFormData(params, files)
      : params;
  }

  static api_get(url) {
    const urlOptions = {
      url,
    };
    const config = {
      parseToJson: true,
    };
    return HTTP.ApiClient.get(urlOptions, config)
      .then(function(response) {
        return {
          status: 200,
          data: response,
        };
      })
      .catch(function(error) {
        return error;
      })
      .finally(function() {});
  }

  static api_delete(url) {
    console.debug('DELETE:: ' + url);
    return axios
      .delete(url)
      .then(function(response) {
        console.debug(response);
        return response;
      })
      .catch(function(error) {
        return error;
      })
      .finally(function() {});
  }

  static api_post(url, params, files, form = true, config) {
    const data = form
      ? FlywheelMonaiLabelClient.constructFormData(params, files)
      : FlywheelMonaiLabelClient.constructFormOrJsonData(params, files);

    return FlywheelMonaiLabelClient.api_post_data(url, data, config);
  }

  static api_post_data(url, data, config) {
    return HTTP.ApiClient.post(url, data, config)
      .then(function(response) {
        console.debug(response);
        return {
          status: response.status,
          data: response,
        };
      })
      .catch(function(error) {
        return error;
      });
  }

  static api_put(url, params, files, form = false, responseType = 'json') {
    const data = form
      ? FlywheelMonaiLabelClient.constructFormData(params, files)
      : FlywheelMonaiLabelClient.constructFormOrJsonData(params, files);
    return FlywheelMonaiLabelClient.api_put_data(url, data, responseType);
  }

  static api_put_data(url, data) {
    return HTTP.ApiClient.put(url, data)
      .then(function(response) {
        console.debug(response);
        return response;
      })
      .catch(function(error) {
        return error;
      });
  }
}
