/*
Copyright (c) MONAI Consortium
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import nrrd from 'nrrd-js';
import pako from 'pako';
import readImageArrayBuffer from 'itk/readImageArrayBuffer';
import writeArrayBuffer from 'itk/writeArrayBuffer';
import config from 'itk/itkConfig';
import * as cornerstoneNIFTIImageLoader from '@cornerstonejs/nifti-image-loader';

const pkgJSON = require('../../package.json');
const itkVersion = pkgJSON.dependencies.itk.substring(1);
config.itkModulesPath = 'https://unpkg.com/itk@' + itkVersion; // HACK to use ITK from CDN
const URL_SCHEMA = 'nifti:';

export default class SegmentationReader {
  static parseNrrdData(data) {
    let nrrdfile = nrrd.parse(data);

    // Currently gzip is not supported in nrrd.js
    if (nrrdfile.encoding === 'gzip') {
      const buffer = pako.inflate(nrrdfile.buffer).buffer;

      nrrdfile.encoding = 'raw';
      nrrdfile.data = new Uint16Array(buffer);
      nrrdfile.buffer = buffer;
    }

    const image = nrrdfile.buffer;
    const header = nrrdfile;
    delete header.data;
    delete header.buffer;

    return {
      header,
      image,
    };
  }

  static async parseNIfTIData(data) {
    const bufferPromise = data.arrayBuffer();
    const headerImagePromise = await bufferPromise.then(function(buffer) {
      const nifti = cornerstoneNIFTIImageLoader.nifti;
      const blob = new Blob([buffer], { type: 'application/octet-stream' });
      const blobURL = URL.createObjectURL(blob);
      const requestURL = `${URL_SCHEMA}${blobURL}`;
      const headerPromise = nifti.loadHeader(requestURL, true);
      const imagePromise = nifti.loadImage(requestURL);
      return Promise.all([headerPromise, imagePromise]).then(
        async ([header, image]) => {
          URL.revokeObjectURL(blobURL);
          const convertHeader = false;
          header = convertHeader
            ? SegmentationReader.convertHeader(header)
            : header;
          return {
            header,
            image,
          };
        }
      );
    });
    return await headerImagePromise;
  }

  static getType(dataType) {
    let nativeType;
    if (dataType.TypedArrayConstructor === Int8Array) {
      nativeType = 'int8';
    } else if (dataType.TypedArrayConstructor === Uint8Array) {
      nativeType = 'uint8';
    } else if (dataType.TypedArrayConstructor === Int16Array) {
      nativeType = 'int16';
    } else if (dataType.TypedArrayConstructor === Uint16Array) {
      nativeType = 'uint16';
    } else if (dataType.TypedArrayConstructor === Int32Array) {
      nativeType = 'int32';
    } else if (dataType.TypedArrayConstructor === Uint32Array) {
      nativeType = 'uint32';
    } else if (dataType.TypedArrayConstructor === Float32Array) {
      nativeType = 'float';
    } else if (dataType.TypedArrayConstructor === Float64Array) {
      nativeType = 'double';
    }
    if (nativeType !== undefined) {
      return nativeType;
    } else {
      throw new Error('Type of data is not given and cannot be inferred!');
    }
  }
  static convertHeader(header) {
    const outputHeader = [];
    const pixDim = header.header.pixDims;
    const coordinates = `(${pixDim[0]}, 0.0, 0.0)(0.0,${pixDim[1]}, 0.0)(0.0, 0.0,${pixDim[2]})`;
    const orientationMatrix = header.orientationMatrix;
    outputHeader.push('NIFTY0001'); // TODO: Adjust version based on features that are actually used and/or the version specified by the user (if any).
    outputHeader.push('# Generated from Flywheel viewer');
    const dataType = SegmentationReader.getType(header.dataType);
    outputHeader.push(`type: ${dataType}`);
    outputHeader.push(`dimension: ${header.header.dims[0]}`);
    outputHeader.push('space: RAS');
    outputHeader.push(`sizes: ${header.voxelLength.join(' ')}`);
    outputHeader.push(`space directions: ${coordinates}`);
    outputHeader.push(`centerings: cell cell cell ???`);
    outputHeader.push(`kinds: space space space list`);
    outputHeader.push(`endian: little`);
    outputHeader.push('encoding: raw');
    outputHeader.push(`space units: "mm" "mm" "mm"`);
    outputHeader.push(
      `space origin: [${orientationMatrix[0][3]}${orientationMatrix[1][3]}${orientationMatrix[2][3]}]`
    );
    outputHeader.push(`measurement frame: (1,0,0) (0,1,0) (0,0,1)`);
    outputHeader.join('\n');
    return outputHeader;
  }

  static saveFile(blob, filename) {
    if (window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob, filename);
    } else {
      const a = document.createElement('a');
      document.body.appendChild(a);

      const url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = filename;
      a.click();

      setTimeout(() => {
        window.URL.revokeObjectURL(url);
        document.body.removeChild(a);
      }, 0);
    }
  }

  // GZIP write not supported by nrrd-js (so use ITK save with compressed = true)
  static serializeNrrdCompressed(header, image, filename) {
    const nrrdBuffer = SegmentationReader.serializeNrrd(header, image);

    const reader = readImageArrayBuffer(null, nrrdBuffer, 'temp.nrrd');
    reader.then(function(response) {
      const writer = writeArrayBuffer(
        response.webWorker,
        true,
        response.image,
        filename
      );
      writer.then(function(response) {
        SegmentationReader.saveFile(new Blob([response.arrayBuffer]), filename);
      });
    });
  }

  static serializeNrrd(header, image, filename) {
    let nrrdOrg = Object.assign({}, header);
    nrrdOrg.buffer = image;
    nrrdOrg.data = new Uint16Array(image);

    const nrrdBuffer = nrrd.serialize(nrrdOrg);
    if (filename) {
      SegmentationReader.saveFile(new Blob([nrrdBuffer]), filename);
    }
    return nrrdBuffer;
  }
}
