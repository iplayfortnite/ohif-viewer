import cornerstone from 'cornerstone-core';
import flattenNDarray from '../shared/flattenNDarray.js';
import arrayRotateRight from '../shared/arrayRotateRight.js';

/**
 * A slice of a volume that is orthogonal to its i,j,k values (not to x,y,z).
 * The main property is .cornersoneImage, which exposes a
 * Cornerstone Image Object.
 */
export default class Slice {
  constructor(volume, imageIdObject, isSingleTimepoint = false) {
    this.volume = volume;
    this.imageIdObject = imageIdObject;
    this.dimension = imageIdObject.slice.dimension;
    const { rowsIndex, columnsIndex, framesIndex } = this.getDimensionsIndices(this.dimension);
    if (!this.volume.metaData.rightHanded()) {
      this.index = this.volume.metaData.numberOfFrames(framesIndex) - 1 - imageIdObject.slice.index;
    } else {
      this.index = imageIdObject.slice.index;
    }
    this.timePoint = isSingleTimepoint ? 0 : imageIdObject.timePoint;
    this.metaData = {};

    this.determineMetaData();
    if (this.volume.hasImageData) {
      this.determinePixelData();
    }
  }

  determineMetaData() {
    const metaImageMetaData = this.volume.metaData.header;
    const { rowsIndex, columnsIndex, framesIndex } = this.getDimensionsIndices(this.dimension);
    const dimensions = metaImageMetaData.updatedDimSize || metaImageMetaData.DimSize;
    const rows = parseInt(dimensions[rowsIndex], 10);
    const columns = parseInt(dimensions[columnsIndex], 10);
    const numberOfFrames = parseInt(dimensions[framesIndex], 10);
    const pixelSpacing = this.volume.metaData.pixelSpacing();
    const rowPixelSpacing = pixelSpacing[rowsIndex];
    const columnPixelSpacing = pixelSpacing[columnsIndex];
    const slicePixelSpacing = pixelSpacing[framesIndex];
    const orientations = this.volume.metaData.imageOrientation(this.dimension);
    const bitsAllocated = this.volume.metaData.bitsAllocated();
    const numBitsPerVoxel = bitsAllocated;
    const rowCosines = [
      parseFloat(orientations[0]),
      parseFloat(orientations[1]),
      parseFloat(orientations[2])
    ];
    const columnCosines = [
      parseFloat(orientations[3]),
      parseFloat(orientations[4]),
      parseFloat(orientations[5])
    ];
    const patientPosition = this.volume.metaData.imagePosition(this.index, framesIndex);

    Object.assign(this.metaData, {
      columns,
      rows,
      numberOfFrames,
      columnsIndex,
      rowsIndex,
      framesIndex,
      columnPixelSpacing,
      rowPixelSpacing,
      slicePixelSpacing,
      columnCosines,
      rowCosines,
      imagePositionPatient: patientPosition,
      imageOrientationPatient: [...rowCosines, ...columnCosines],
      numBitsPerVoxel,
      frameOfReferenceUID: this.imageIdObject.filePath
    });
  }

  determinePixelData() {
    this.volume.imageDataNDarray.set(0, 0, 0, 255);
    // Pick a slice (sliceIndex) according to the wanted dimension (sliceDimension)
    // const dimensionIndex = 'xyz'.indexOf(slice.dimension);
    const slicePick = arrayRotateRight([this.index, null, null], this.metaData.framesIndex);
    const TypeArrayConstructor = this.volume.metaData.dataType.TypedArrayConstructor;
    const imageDataView = this.volume.imageDataNDarray.pick(...slicePick, this.timePoint);

    this.pixelData = flattenNDarray(imageDataView, TypeArrayConstructor);

    // If the data was originally in float values, we also slice the
    // original float ndarray
    const isDataInFloat = this.volume.metaData.dataType.isDataInFloat;

    if (isDataInFloat) {
      const floatImageDataView = this.volume.imageDataNDarray.pick(...slicePick, this.timePoint);

      this.floatPixelData = flattenNDarray(floatImageDataView, this.volume.metaData.dataType.OriginalTypedArrayConstructor);
    }
  }

  getDimensionsIndices(sliceDimension) {
    let rowsIndex, columnsIndex, framesIndex;

    switch (sliceDimension) {
      case 'x':
        rowsIndex = 'xyz'.indexOf('z');
        columnsIndex = 'xyz'.indexOf('y');
        framesIndex = 'xyz'.indexOf('x');
        break;

      case 'y':
        rowsIndex = 'xyz'.indexOf('z');
        columnsIndex = 'xyz'.indexOf('x');
        framesIndex = 'xyz'.indexOf('y');
        break;

      case 'z':
        rowsIndex = 'xyz'.indexOf('y');
        columnsIndex = 'xyz'.indexOf('x');
        framesIndex = 'xyz'.indexOf('z');
        break;
    }

    return {
      rowsIndex,
      columnsIndex,
      framesIndex
    };
  }

  get cornerstoneImageObject() {
    const volumeMetaData = this.volume.metaData;
    const sliceMetaData = this.metaData;
    const render = volumeMetaData.dataType?.isDataInColors
      ? cornerstone.renderColorImage
      : cornerstone.renderGrayscaleImage;


    return {
      imageId: this.imageIdObject.url,
      color: volumeMetaData.dataType.isDataInColors,
      columnPixelSpacing: sliceMetaData.columnPixelSpacing,
      columns: sliceMetaData.columns,
      height: sliceMetaData.rows,
      intercept: 0,//volumeMetaData.intercept,
      invert: false,
      minPixelValue: volumeMetaData.minPixelValue,
      maxPixelValue: volumeMetaData.maxPixelValue,
      rowPixelSpacing: sliceMetaData.rowPixelSpacing,
      rows: sliceMetaData.rows,
      sizeInBytes: this.pixelData.byteLength,
      slope: 1,//volumeMetaData.slope,
      width: sliceMetaData.columns,
      windowCenter: volumeMetaData.windowCenter,
      windowWidth: volumeMetaData.windowWidth,
      floatPixelData: this.floatPixelData,
      decodeTimeInMS: 0,
      getPixelData: () => this.pixelData,
      render
    };
  }

  get compoundMetaData() {
    return Object.assign({}, this.volume.metaData, this.metaData);
  }
}
