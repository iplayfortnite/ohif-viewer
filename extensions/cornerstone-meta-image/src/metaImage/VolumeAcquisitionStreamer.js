import Volume from './Volume.js';
import VolumeCache from './VolumeCache.js';
import convertFloatDataToInteger from './convertFloatDataToInteger.js';
import ImageId from './ImageId.js';
import minMaxNDarray from '../shared/minMaxNDarray.js';
import unInt8ArrayConcat from '../shared/unInt8ArrayConcat.js';
import VolumeTimepointFileFetcher from './VolumeTimepointFileFetcher';
import normalizeInvalid from '../shared/normalizeInvalid.js';

import ndarray from 'ndarray';

/**
 * A singleton that is responsible for getting a Volume for a meta-image imageId.
 * It can either get it from its cache, or load a file with an asynchronous
 * request and process it to return the volume. Main method is acquire(imageId).
 */
export default class VolumeAcquisitionStreamer {
  constructor(httpHeaders = {}) {
    this.volumeCache = new VolumeCache();
    this.volumeFetchers = {};
    this.rawFileNameMap = {};
    this.httpHeaders = httpHeaders;
  }

  static getInstance(httpHeaders) {
    if (!VolumeAcquisitionStreamer.instance) {
      VolumeAcquisitionStreamer.instance = new VolumeAcquisitionStreamer(
        httpHeaders
      );
    }

    return VolumeAcquisitionStreamer.instance;
  }

  acquireMetaHeader(imageIdObject) {
    const fetcherData = this.createFetcherData(imageIdObject);

    if (fetcherData.metaHeaderPromise) {
      return fetcherData.metaHeaderPromise;
    }

    let volumeHeaderAcquiredPromise = null;
    try {
      // If no one has requested the meta header for this file yet, we create a
      // promise to acquire it
      volumeHeaderAcquiredPromise = fetcherData.volumeFetcher
        .getHeaderPromise()
        // Retrieves the meta header data
        .then(data => {
          this.rawFileNameMap[imageIdObject.filePath] =
            data.metaData.header.ElementDataFile;
          return data;
        });
    } catch (error) {
      throw error;
    }
    // Save this promise to the promise cache
    this.volumeFetchers[
      imageIdObject.filePath
    ].metaHeaderPromise = volumeHeaderAcquiredPromise;

    return volumeHeaderAcquiredPromise;
  }

  acquireHeaderOnly(imageIdObject) {
    let fetcherData = this.createFetcherData(imageIdObject);

    if (fetcherData.headerPromise) {
      return fetcherData.headerPromise;
    }

    // If no one has requested the header of this volume yet, we create a
    // promise to acquire it and save this promise to the promise cache
    fetcherData.headerPromise = new Promise((resolve, reject) => {
      const cachedVolume = this.volumeCache.get(imageIdObject);

      if (cachedVolume) {
        resolve(cachedVolume);

        return;
      }

      const fetcher = fetcherData.volumeFetcher;
      const fileFetchedPromise = fetcher.getHeaderPromise();

      fileFetchedPromise
        // Creates the volume: metadata + image data
        .then(data => this.createVolume(data, imageIdObject))
        .then(data => this.cacheVolume(data, imageIdObject))
        .then(data => resolve(data))
        .catch(reject);
    });

    return fetcherData.headerPromise;
  }

  acquireTimepoint(imageIdObject) {
    // If we have the timepoint already generated, return it immediately.
    const cachedVolume = this.volumeCache.get(imageIdObject);

    if (cachedVolume && cachedVolume.hasImageData) {
      return new Promise((resolve, reject) => {
        resolve(cachedVolume);
      });
    }

    const fetcherData = this.createFetcherData(imageIdObject);

    if (fetcherData.timePointPromises[imageIdObject.timePoint]) {
      return fetcherData.timePointPromises[imageIdObject.timePoint];
    }

    // If no one has requested this volume yet, we create a promise to acquire it
    const volumeAcquiredPromise = new Promise((resolve, reject) => {
      const fileFetchedPromise = fetcherData.volumeFetcher.fetchTimepoint(
        imageIdObject.timePoint,
        imageIdObject
      );

      fileFetchedPromise
        // Reads the image data and puts it in an ndarray (to be sliced)
        .then(data => this.readImageData(data))
        // Transforms the image data (eg float to int, in case)
        .then(data => this.transformImageData(data))
        // Gather meta data that depends on the image data (eg min/max, wc/ww)
        .then(data => this.determineImageDependentMetaData(data))
        // Creates the volume: metadata + image data
        .then(data => this.createVolume(data, imageIdObject))
        .then(data => this.cacheVolume(data, imageIdObject))
        .then(data => resolve(data))
        .catch(reject);
    });

    fetcherData.timePointPromises[
      imageIdObject.timePoint
    ] = volumeAcquiredPromise;

    return volumeAcquiredPromise;
  }

  readImageData({ headerData, metaData, volumeData }) {
    const decompressedFileData = unInt8ArrayConcat(headerData, volumeData);

    const imageData = metaData.parseRawImage(
      decompressedFileData.buffer,
      headerData
    );

    const dimensions = metaData.header.DimSize;
    // Only 1 timeSlice
    const timeSlices = 1;
    const strides = [
      1,
      dimensions[0],
      dimensions[0] * dimensions[1],
      // Each time slice has a stride of x*y*z
      dimensions[0] * dimensions[1] * dimensions[2],
    ];

    // Create an ndarray of the whole data
    let imageDataNDarray = ndarray(
      imageData,
      [...dimensions, timeSlices],
      strides
    );

    // Finds the smallest and largest voxel value
    const minMax = minMaxNDarray(imageDataNDarray);

    metaData.minPixelValue = minMax.min;
    metaData.maxPixelValue = minMax.max;

    // Special handling for reading MHD layered elevation map data
    // The dimension in header DimSize => [width of each layer, number of slices, number of layers,...]
    // The raw pixel data arranged in order=>
    // First layer information for all slices(size of width * number of slices pixels), Second layer information for all slices
    // like in this order
    // From this representation, we are creating each slice data which represent all layers(size width * number of layers pixels)
    // Here the vertical positions of each layer will be identified from the pixel value getting from the original raw data.
    // The height of the slice will be the maximum pixel value after iterating through the entire original raw data array.
    // So, the new reconstructed raw data represents layer information in the order=>
    // First slice data with all layer paths in first slice(size of width * max pixel value pixels), then next slice data etc..
    // Each slice pixels have the layer index(1 based) as pixel values depicts the layer path, and other pixels have 0 pixel values..

    if (metaData.header.LayerNames?.length) {
      // New dimension based on the maximum pixel value as the slice rows value.
      const rows = metaData.maxPixelValue;
      const columns = dimensions[0];
      const numOfSlices = dimensions[1];
      const numOfLayers = dimensions[2];
      const pixelsPerFrame = columns * rows;
      metaData.updateDimensionSizeForLayeredFormatData([
        columns,
        rows,
        numOfSlices,
      ]);
      const volumeTimepointLength = metaData.getRawDataLength();
      const buffer = new Uint16Array(volumeTimepointLength / 2);

      for (let i = 0; i < numOfSlices * numOfLayers; i++) {
        const srcOffset = i * columns;
        const tgtSliceOffset = (i % numOfSlices) * pixelsPerFrame;
        for (let j = 0; j < columns; j++) {
          const rowOffset = imageData[srcOffset + j] - 1;
          const tgtIndex = tgtSliceOffset + rowOffset * columns + j;
          buffer[tgtIndex] = Math.floor(i / numOfSlices) + 1;
        }
      }

      const TypedArrayConstructor = metaData.dataType.TypedArrayConstructor;
      let newImageData = normalizeInvalid(
        TypedArrayConstructor,
        new TypedArrayConstructor(buffer)
      );

      const newDimensions = metaData.header.updatedDimSize;
      const newStrides = [
        1,
        newDimensions[0],
        newDimensions[0] * newDimensions[1],
        // Each time slice has a stride of x*y*z
        newDimensions[0] * newDimensions[1] * newDimensions[2],
      ];

      imageDataNDarray = ndarray(
        newImageData,
        [...newDimensions, timeSlices],
        newStrides
      );

      metaData.minPixelValue = 0;
      metaData.maxPixelValue = numOfLayers;
    }

    return {
      metaData,
      imageDataNDarray,
    };
  }

  transformImageData({ metaData, imageDataNDarray }) {
    if (metaData.dataType.isDataInFloat) {
      const conversion = convertFloatDataToInteger(imageDataNDarray, metaData);

      imageDataNDarray = conversion.convertedImageDataView;
      Object.assign(metaData, conversion.metaData, {
        floatImageDataNDarray: conversion.floatImageDataView,
      });
    }

    return {
      metaData,
      imageDataNDarray,
    };
  }

  determineImageDependentMetaData({ metaData, imageDataNDarray }) {
    Object.assign(
      metaData,
      determineWindowValues(
        1,
        0,
        metaData.minPixelValue,
        metaData.maxPixelValue
      )
    );

    return {
      metaData,
      imageDataNDarray,
    };
  }

  createVolume({ metaData, imageDataNDarray }, imageIdObject) {
    const timePointImageIdObject = new ImageId(
      imageIdObject.filePath,
      imageIdObject.slice,
      0
    );

    return new Volume(
      timePointImageIdObject,
      metaData,
      imageDataNDarray,
      metaData.floatImageDataNDarray,
      true
    );
  }

  cacheVolume(volume, imageIdObject) {
    this.volumeCache.add(imageIdObject, volume);

    return volume;
  }

  createFetcherData(imageIdObject) {
    let fileUrl = imageIdObject.filePath;
    // For the raw file, find the corresponding meta-image file url which was used to cache the volume fetcher
    if (!imageIdObject.isHeaderFile) {
      const keys = Object.keys(this.rawFileNameMap);
      fileUrl =
        keys.find(key =>
          imageIdObject.filePath.includes(this.rawFileNameMap[key])
        ) || imageIdObject.filePath;
    }

    this.volumeFetchers[fileUrl] = this.volumeFetchers[fileUrl] || {
      metaHeaderPromise: null,
      headerPromise: null,
      timePointPromises: [],
      volumeFetcher: new VolumeTimepointFileFetcher(imageIdObject, {
        headers: this.httpHeaders,
      }),
    };

    return this.volumeFetchers[fileUrl];
  }
}

function determineWindowValues(slope, intercept, minValue, maxValue) {
  const maxVoi = maxValue * slope + intercept;
  const minVoi = minValue * slope + intercept;

  return {
    windowCenter: (maxVoi + minVoi) / 2,
    windowWidth: maxVoi - minVoi,
  };
}
