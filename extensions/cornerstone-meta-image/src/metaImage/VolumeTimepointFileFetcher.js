import pako from 'pako';
import FileStreamer from './FileStreamer.js';
import unInt8ArrayConcat from '../shared/unInt8ArrayConcat.js';
import MHDDataReader from './MHDDataReader'

/**
 * Stream and returns a volume timepoint from a file
 */
export default class VolumeTimepointFileFetcher {
  constructor(imageIdObject, {
    method = 'GET',
    responseType = 'arraybuffer',
    beforeSend = noop,
    headers = {},
    onHeadersReceived = noop
  } = {}) {

    this.imageId = imageIdObject;
    this.imageDataFileId = "";
    this.options = {
      method,
      responseType,
      beforeSend,
      headers,
      onHeadersReceived
    };
    this.volumeData = VolumeTimepointFileFetcher.createDefaultVolumeData();
  }

  fetchTimepoint(timepoint = 0, imageId) {
    if ((this.imageDataFileId.length === 0) && !imageId?.isHeaderFile) {
      this.imageDataFileId = imageId;
    }
    this.ensureStreaming(this.imageId);

    const fileFetchPromise = new Promise((resolve, reject) => {
      this.getHeaderPromise().then((header) => {
        const volumeTimepointLength = header.metaData.getRawDataLength();
        const offset = this.volumeData.headerOffset + (timepoint * volumeTimepointLength);

        this.getBufferPromise(offset, volumeTimepointLength).then((volumeData) => {
          resolve({
            headerData: header.headerData,
            volumeData,
            metaData: header.metaData
          });
        }).catch(err => {
          reject(err);
        });
      }).catch(err => {
        reject(err);
      });
    });

    return fileFetchPromise;
  }

  ensureStreaming(imageIdObject) {

    const imageData = this.volumeData;

    if (imageData.streamer) {
      if (this.volumeData.headerParsed && (!imageIdObject.isHeaderFile)) {
        VolumeTimepointFileFetcher.runStreamer(imageIdObject, imageData);
      }
      return;
    }

    const streamer = new FileStreamer(this.options);

    imageData.streamer = streamer;

    VolumeTimepointFileFetcher.runStreamer(imageIdObject, imageData);
  }

  static runStreamer(imageIdObject, imageData) {
    imageData.streamer.stream(
      imageIdObject,
      (chunk, imageIdObject, contentLength, err) => {
        if (!chunk) {
          const errorDescription = `Could not fetch the file '${imageIdObject.filePath}'.`;
          const error = err || new Error(errorDescription);

          const promiseEntry = imageData.bufferPromiseEntries[0];
          if (!promiseEntry) {
            imageData.headerPromiseMethods.reject(error);
          } else {
            imageData.bufferPromiseEntries.splice(0, 1);

            promiseEntry.promise.reject(error);
          }
          return;
        }
        if (!imageData.headerParsed) {
          imageData.headerOffset = parseInt(contentLength);
          if (!VolumeTimepointFileFetcher.tryParseHeader(imageData, chunk)) {
            return;
          }

          // Now we have the header parsed
          imageData.headerPromiseMethods.resolve(imageData.header);
          VolumeTimepointFileFetcher.evaluatePromises(imageData);

          imageData.headerParsed = true;

          return;
        }

        if (imageData.isCompressed) {
          chunk = VolumeTimepointFileFetcher.uncompress(imageData, chunk);
        }

        VolumeTimepointFileFetcher.addToImageDataBuffer(imageData, chunk);

        VolumeTimepointFileFetcher.evaluatePromises(imageData);
      }
    );
  }

  getHeaderPromise() {
    try {
      this.ensureStreaming(this.imageId);

      if (this.volumeData.header) {
        return Promise.resolve(this.volumeData.header);
      }
    } catch (error) {
      const errorDescription = `Could not fetch the file '${this.imageId.filePath}'.`;

      throw new Error(errorDescription);
    }
    return this.volumeData.headerPromise;
  }

  getBufferPromise(offset, length) {
    let imageId = this.imageDataFileId || this.imageId;

    let fileStreamPromise = null;
    try {
      this.ensureStreaming(imageId);
      fileStreamPromise = new Promise((resolve, reject) => {
        if (this.volumeData.dataStreamedLength >= length) {
          resolve(this.volumeData.buffer.slice(offset, offset + length));
        } else {
          const bufferPromiseEntry = {
            offset,
            length,
            promise: {
              resolve,
              reject
            }
          };

          this.volumeData.bufferPromiseEntries.push(bufferPromiseEntry);
        }
      });
    } catch (error) {
      const errorDescription = `Could not fetch the file '${imageId?.filePath}'.`;

      throw new Error(errorDescription);
    }

    return fileStreamPromise;
  }

  static evaluatePromises(imageData) {
    let index = imageData.bufferPromiseEntries.length - 1;

    while (index >= 0) {
      const entry = imageData.bufferPromiseEntries[index];
      const totalLength = entry.offset + entry.length;

      if (imageData.dataStreamedLength >= totalLength) {

        imageData.bufferPromiseEntries.splice(index, 1);

        entry.promise.resolve(imageData.buffer.slice(entry.offset, entry.offset + entry.length));
      }

      index--;
    }
  }

  static createDefaultVolumeData() {
    const headerPromiseMethods = {};
    const headerPromise = new Promise((resolve, reject) => {
      headerPromiseMethods.resolve = resolve;
      headerPromiseMethods.reject = reject;
    });

    return {
      streamer: null,
      buffer: new Uint8Array(0),
      bufferPromiseEntries: [],
      dataStreamedLength: 0,
      headerDataStreamedLength: 0,
      isCompressed: false,
      tmpBuffer: new Uint8Array(0),
      headerParsed: false,
      inflator: new pako.Inflate(),
      headerPromise,
      headerPromiseMethods,
      headerOffset: 0
    };
  }

  static parseHeaderData(headerData) {

    const metaData = new MHDDataReader({ mhdBuffer: headerData.buffer, rawBuffer: null, url: "" });
    return {
      headerData,
      metaData
    };
  }

  static uncompress(imageData, chunk) {
    if (chunk.length === 0) {
      return chunk;
    }

    imageData.inflator.push(chunk, pako.Z_SYNC_FLUSH);

    return imageData.inflator.result;
  }

  static addToImageDataBuffer(imageData, chunk) {
    if (imageData.dataStreamedLength + chunk.length > imageData.buffer.length) {
      imageData.buffer = unInt8ArrayConcat(imageData.buffer, chunk);
    } else {
      imageData.buffer.set(chunk, imageData.dataStreamedLength);
    }

    imageData.dataStreamedLength += chunk.length;
  }

  static tryParseHeader(imageData, chunk) {
    if (imageData.isHeaderTypeKnown) {

      VolumeTimepointFileFetcher.addToImageDataBuffer(imageData, chunk);

      const canParse = imageData.dataStreamedLength >= imageData.headerOffset;

      if (canParse) {
        imageData.header = VolumeTimepointFileFetcher.parseHeaderData(imageData.buffer.slice(0, imageData.headerOffset));

        imageData.isCompressed = imageData.header.metaData.header.CompressedData === "True" ? true : false;
        const volumeBuffer = VolumeTimepointFileFetcher.createVolumeBuffer(imageData);

        volumeBuffer.set(imageData.buffer);
        imageData.buffer = volumeBuffer;
      }

      return canParse;
    }

    if (imageData.tmpBuffer.length + chunk.length < imageData.headerOffset) {
      // We don't have enough data, lets add it and wait for next chunk
      imageData.tmpBuffer = unInt8ArrayConcat(imageData.tmpBuffer, chunk);

      return false;
    }


    // We don't want to add the chunk now to tmpBuffer/buffer. This will be handlded by recursion
    let tempBuffer = unInt8ArrayConcat(imageData.tmpBuffer, chunk);

    imageData.isCompressed = imageData.header?.CompressedData === "True" ? true : false;

    if (imageData.isCompressed) {
      const inflator = new pako.Inflate();

      inflator.push(tempBuffer, pako.Z_SYNC_FLUSH);

      tempBuffer = inflator.result;
    }

    imageData.isHeaderTypeKnown = true;
    // We know what type of data we have, we shouldn't be using the temp buffer anymore
    imageData.buffer = (imageData.isCompressed) ? VolumeTimepointFileFetcher.uncompress(imageData, imageData.tmpBuffer) : imageData.tmpBuffer;
    imageData.dataStreamedLength = imageData.buffer.length;
    imageData.tmpBuffer = undefined;

    // Recursion, now we should satisfy the "isHeaderTypeKnown"
    return VolumeTimepointFileFetcher.tryParseHeader(imageData, chunk);
  }

  static createVolumeBuffer(imageData) {
    const volumeTimepointLength = imageData.header.metaData.getRawDataLength();
    const offset = imageData.headerOffset;

    // We want to update the buffer to have the full capacity of the volume
    const buffer = new ArrayBuffer(offset + (volumeTimepointLength * imageData.header.metaData.header.timeSlices));

    return new Uint8Array(buffer);
  }
}

function noop() {
}
