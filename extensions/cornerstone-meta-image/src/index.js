import MetaImageSopClassHandler from './MetaImageSopClassHandler.js';
import superExtension from '@ohif/extension-cornerstone/src/index';
import toolbarModule from './toolbarModule.js';
import extensionCommandsModule from './commandsModule.js';
import metaImage from './metaImageLoader';
import initExtension from './init';

export default {
  ...superExtension,
  ...{
    id: 'cornerstone::metaimage',
    getSopClassHandlerModule() {
      return MetaImageSopClassHandler;
    },
    getToolbarModule() {
      return {
        definitions: [
          ...superExtension.getToolbarModule().definitions,
          ...toolbarModule.definitions,
        ], // use cornerstone definition
        defaultContext: 'ACTIVE_VIEWPORT::CORNERSTONE::METAIMAGE',
        shouldDisplayLayoutButton: true,
      };
    },
    getViewportModule({ commandsManager }) {
      return superExtension.getViewportModule({ commandsManager });
    },
    getCommandsModule({ servicesManager, commandsManager, appConfig }) {
      const superCommandsModule = superExtension.getCommandsModule({
        servicesManager,
      });

      const commandsModule = extensionCommandsModule({
        servicesManager,
        commandsManager,
        appConfig,
      });

      return {
        actions: { ...superCommandsModule.actions, ...commandsModule.actions },
        definitions: {
          ...superCommandsModule.definitions,
          ...commandsModule.definitions,
        },
        defaultContext: 'ACTIVE_VIEWPORT::CORNERSTONE::METAIMAGE',
      };
    },
    preRegistration({ commandsManager, servicesManager, appConfig }) {
      initExtension({ commandsManager, servicesManager, appConfig });
    },
  },
};

export { metaImage };
