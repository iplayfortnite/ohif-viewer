import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import OHIF from '@ohif/core';
import applyFullDynamicWWWC from './utils/applyFullDynamicWWWC';
import store from '@ohif/viewer/src/store';

import {
  Utils as FlywheelCommonUtils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import { clearRequestedIdCache } from './components/StudyForm/utils/CustomInfo';
import { clearAcquisitionPromises } from '../../flywheel-common/src/http/services/session';

const {
  clearSegmentationData,
  clearMultiSessionData,
  setHighlightOverlappedStatus,
  setProtocolLayout,
  clearAllActionStates,
  setFovGridRestoreInfo,
} = FlywheelCommonRedux.actions;

const commandsModule = () => {
  const actions = {
    clearAllAnnotations: ({ syncWithMeasurementAPI = true }) => {
      // force toolstate to restore to be empty value
      cornerstoneTools.globalImageIdSpecificToolStateManager.restoreToolState(
        {}
      );

      const toolsName = FlywheelCommonUtils.cornerstoneUtils
        .getCurrentAnnotationTools()
        .map(tool => tool.name);

      // clear local tool state cache and cs tool change history
      FlywheelCommonUtils.cornerstoneUtils.clearCacheToolMode();
      FlywheelCommonUtils.cornerstoneUtils.clearToolChangeHistory(toolsName);

      if (syncWithMeasurementAPI) {
        const measurementApi = OHIF.measurements.MeasurementApi.Instance;
        measurementApi.syncMeasurementsAndToolData();
      }

      cornerstone.getEnabledElements().forEach(enabledElement => {
        if (enabledElement.image) {
          cornerstone.updateImage(enabledElement.element);
        }
      });
    },
    setAllAnnotationsState: ({ toolState = 'passive' }) => {
      FlywheelCommonUtils.cornerstoneUtils
        .getCurrentAnnotationTools()
        .forEach(tool => {
          FlywheelCommonUtils.cornerstoneUtils.setToolMode(
            tool.name,
            toolState
          );
        });
    },
    /**
     * Set full dynamic for given element or current element in active viewport (in case given element is not passed)
     */
    setFullDynamicWWWC: ({ element, viewports }) => {
      let _element = element;

      if (!_element) {
        _element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
          viewports.activeViewportIndex
        );
        if (!_element) {
          return;
        }
      }

      const csEnabledElement = cornerstone.getEnabledElement(_element);
      if (!csEnabledElement || !csEnabledElement.image) {
        return;
      }
      applyFullDynamicWWWC(csEnabledElement);
    },
    clearSegmentationData: ({}) => {
      store.dispatch(clearSegmentationData());
    },
    clearMultiSessionViewData: ({}) => {
      store.dispatch(clearMultiSessionData());
    },
    clearContourProperties: ({}) => {
      // This can be extended to all properties in a single action
      store.dispatch(setHighlightOverlappedStatus(false));
    },
    clearProtocolLayout: ({}) => {
      store.dispatch(setProtocolLayout(null));
    },
    clearAllActionStates: ({}) => {
      store.dispatch(clearAllActionStates());
    },
    clearAllFovGridRestoreStates: ({}) => {
      store.dispatch(setFovGridRestoreInfo(null));
    },
    clearAllRequestedIdCache: ({}) => {
      clearRequestedIdCache();
    },
    clearAcquisitionCache: ({}) => {
      clearAcquisitionPromises();
    },
  };

  const definitions = {
    clearAllAnnotations: {
      commandFn: actions.clearAllAnnotations,
      storeContexts: [],
      options: {},
    },
    setAllAnnotationsState: {
      commandFn: actions.setAllAnnotationsState,
      storeContexts: [],
      options: {},
    },
    setFullDynamicWWWC: {
      commandFn: actions.setFullDynamicWWWC,
      storeContexts: ['viewports'],
      options: {},
    },
    clearSegmentationData: {
      commandFn: actions.clearSegmentationData,
      storeContexts: [],
      options: {},
    },
    clearMultiSessionViewData: {
      commandFn: actions.clearMultiSessionViewData,
      storeContexts: [],
      options: {},
    },
    clearContourProperties: {
      commandFn: actions.clearContourProperties,
      storeContexts: [],
      options: {},
    },
    clearProtocolLayout: {
      commandFn: actions.clearProtocolLayout,
      storeContexts: [],
      options: {},
    },
    clearAllActionStates: {
      commandFn: actions.clearAllActionStates,
      storeContexts: [],
      options: {},
    },
    clearAllFovGridRestoreStates: {
      commandFn: actions.clearAllFovGridRestoreStates,
      storeContexts: [],
      options: {},
    },
    clearAllRequestedIdCache: {
      commandFn: actions.clearAllRequestedIdCache,
      storeContexts: [],
      options: {},
    },
    clearAcquisitionCache: {
      commandFn: actions.clearAcquisitionCache,
      storeContexts: [],
      options: {},
    },
  };

  return {
    actions,
    definitions,
    defaultContext: 'VIEWER',
  };
};

export default commandsModule;
