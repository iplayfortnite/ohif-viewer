import { log, utils, redux } from '@ohif/core';
import cornerstoneWADOImageLoader from 'cornerstone-wado-image-loader';
import cornerstoneWebImageLoader from 'cornerstone-web-image-loader';
import cornerstone from 'cornerstone-core';
import * as cornerstoneNIFTIImageLoader from '@cornerstonejs/nifti-image-loader';
import _ from 'lodash';
import { setupFullscreen } from '../viewport';
import initOriginalStore from './initStore';
import initCornerstone from './initCornerstone';
import initReactive from './initReactive';
import initMeasurements from './initMeasurements';
import Redux from '../redux';
import {
  HTTP as FlywheelCommonHTTP,
  Utils as FlywheelCommonUtils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import registerNiftiMetaDataProvider from '@flywheel/extension-cornerstone-nifti/src/metaData/metaDataProvider.js';
import { isHpForWebImages } from '@flywheel/extension-flywheel-common/src/utils';
import mergeDeep from '../utils/mergeDeep';
import {
  getFileDetailsFromAcquisition,
  getFilteredAcquisitionMap,
} from '../utils/index';

const {
  setPaletteColorsFromConfig,
  setStudyFormAvailableStatus,
  setAvailableSmartRanges,
  changeRightHandPanelMode,
} = FlywheelCommonRedux.actions;

const {
  getProject,
  getProjectFileJson,
  getFileJson,
  getSession,
  getUser,
  getUsers,
  listSessions,
  getAcquisitionAssociation,
  getProjectAssociations,
  getColorMapsJson,
  getFile,
  getReaderTask,
  getReaderTaskProtocol,
  getViewerConfig,
  getConfig,
  getViewerForm,
  getAllViewerForm,
  getViewerTaskContext,
  getRoles,
  getAcquisition,
  listAcquisitions,
} = FlywheelCommonHTTP.services;

const { customInfoKey, getRequestURL } = FlywheelCommonHTTP.utils.REQUEST_UTILS;
const { getAuthToken } = FlywheelCommonHTTP.utils.AUTH_UTILS;

const {
  clearAssociations,
  setActiveProject,
  setAllSessions,
  setAssociations,
  setFileAssociations,
  setProjectConfig,
  setUser,
  setAllUsers,
  setSeriesOrder,
  setReaderTask,
  setFeatureConfig,
  setRoles,
  setPermissions,
  setAcquisitions,
  setMultipleReaderTask,
  setMultipleReaderTaskResponse,
} = Redux.actions;

const {
  selectAllAssociations,
  selectAllSessions,
  selectUser,
  selectReaderTask,
  hasPermission,
} = Redux.selectors;

const { performanceTracking, addServers } = utils;
const { performanceStart, performanceEnd } = performanceTracking.setPerformance;
const { getRouteParams, showErrorPage } = FlywheelCommonUtils;

const OHIF_CONFIG_FILENAME = 'ohif_config.json';

const { INITIAL_IMAGE_LOAD } = redux.performanceTrackerKeys;

export default async function initExtension({
  commandsManager,
  servicesManager,
  appConfig,
  hotkeysManager,
}) {
  setNifiLoaderConfig();
  const { history } = appConfig;
  const store = initOriginalStore(appConfig);
  initReactive(store);
  initCornerstone({ commandsManager, appConfig });

  const { projectId, taskId, auth0Connection } = getRouteParams(
    history.location
  );

  let projectConfigPromise;
  let multipleTask = false;
  const taskIds = taskId?.split(',');
  if (taskIds?.length > 1) {
    multipleTask = true;
    const taskData = { taskIds: taskIds };
    store.dispatch(setMultipleReaderTask(taskData));
  }

  const baseServer = { ...appConfig.servers.dicomWeb[0] };

  logInitialLoadingPerformance(store, history.location);

  if (projectId) {
    setLoadersConfig(projectId);
  }

  const featureConfig = await getConfig();
  store.dispatch(setFeatureConfig(featureConfig));

  try {
    await getUser().then(user => {
      store.dispatch(setUser(user));
      if (projectId && !multipleTask) {
        setProject(projectId, taskId);
      } else if (projectId && taskIds.length) {
        taskIds.forEach(taskid => {
          setProject(projectId, taskid);
        });
      }
    });
  } catch (error) {
    const appBase = (appConfig.routerBasename || '').replace(/\/$/, '');
    const apiBase = (appConfig.apiRoot || '').replace(/\/$/, '');
    if (appBase !== apiBase) {
      // redirect to Flywheel UI to log in
      let params = `redirect_url=${encodeURIComponent(window.location.href)}`;
      if (auth0Connection) {
        params += `&auth0_direct_connection_name=${encodeURIComponent(
          auth0Connection
        )}`;
      }
      window.location.href = `${apiBase}/?${params}`;
    } else {
      showErrorPage(
        history,
        'Could not retrieve Flywheel user data. Log into Flywheel and try again.'
      );
    }
    return;
  }

  if (taskId && !projectId && !multipleTask) {
    const taskContext = await getViewerTaskContext(taskId).catch(error =>
      handleCustomError('task', error)
    );
    if (taskContext) {
      const taskUrl = generateTaskUrl(taskContext);

      // if viewer is launched with only task id, then get task context from server and redirect to the url with context
      if (taskUrl) {
        window.location.href = `.${taskUrl}`;
      }
    }
  } else if (taskId && !projectId && multipleTask) {
    processMultitaskUrl();
  }

  initMeasurements(store, appConfig);

  try {
    // handle route changes
    history.listen(handleLocation);

    // expand viewport on double click
    setupFullscreen();
    // load full user list
    getUsers().then(users => {
      const usersMap = {};
      for (const user of users) {
        usersMap[user._id] = user;
      }
      store.dispatch(setAllUsers(usersMap));
    });
  } catch (error) {
    handleError(error);
  }

  const { RemoteService: remoteService } = servicesManager.services;
  if (remoteService.enabled) {
    remoteService
      .listenForMethod(remoteService.methods.OpenStudy)
      .subscribe(({ studyInstanceUid }) => {
        if (!remoteService.projectId) {
          log.error('Project context not set');
          showCustomizeError('Remote:OpenStudy', 'Project id');
          return;
        }
        history.push({
          pathname: `/project/${remoteService.projectId}/viewer/${studyInstanceUid}`,
        });
      });
    remoteService
      .listenForMethod(remoteService.methods.OpenTask)
      .subscribe(async ({ taskId }) => {
        const taskContext = await getViewerTaskContext(taskId).catch(error =>
          handleCustomError('task', error)
        );
        if (taskContext) {
          history.push(generateTaskUrl(taskContext));
        }
      });
    remoteService
      .listenForMethod(remoteService.methods.OpenFile)
      .subscribe(async ({ fileId }) => {
        if (!remoteService.projectId) {
          log.error('Project context not set');
          showCustomizeError('Remote:OpenFile', 'Project id');
          return;
        }
        getFileJson(fileId).then(fileInfo => {
          if (!fileInfo) {
            log.error('Failed to fetch file info with the file id');
            showCustomizeError(
              'Remote:OpenFile',
              `file info with the file id ${fileId}`
            );
            return;
          }
          if (fileInfo.type === 'dicom') {
            history.push(
              `/project/${remoteService.projectId}/viewer?fileIds=${fileInfo._id}`
            );
          } else {
            const fileType =
              fileInfo.type === 'ITK MetaIO Header' ? 'metaio' : fileInfo.type;
            const containerId = fileInfo.parent_ref?.id;
            history.push({
              pathname: `/project/${remoteService.projectId}/${fileType}/${containerId}/${fileInfo.name}`,
            });
          }
        });
      });
    remoteService
      .listenForMethod(remoteService.methods.OpenSession)
      .subscribe(async ({ sessionId }) => {
        if (!remoteService.projectId) {
          log.error('Project context not set');
          showCustomizeError('Remote:OpenSession', 'Project id');
          return;
        }
        history.push(
          `/project/${remoteService.projectId}/viewer?sessionId=${sessionId}`
        );
      });
    remoteService.signalInitialization();
  }

  function getMessage(allTaskDetails, result, errorMessage, isFirstTask) {
    if (allTaskDetails[0].parent_type !== result.parent_type) {
      const parentType = isFirstTask
        ? allTaskDetails[0].parent_type
        : result.parent_type;
      errorMessage += 'parentType: ' + parentType + '. \n ';
    } else if (allTaskDetails[0].project_id !== result.project_id) {
      const projectId = isFirstTask
        ? allTaskDetails[0].project_id
        : result.project_id;
      errorMessage += 'projectId: ' + projectId + '. \n ';
    } else if (allTaskDetails[0].session_id !== result.session_id) {
      const sessionId = isFirstTask
        ? allTaskDetails[0].session_id
        : result.session_id;
      errorMessage += 'sessionId: ' + sessionId + '. \n ';
    } else if (allTaskDetails[0].acquisition_id !== result.acquisition_id) {
      const acquisitionId = isFirstTask
        ? allTaskDetails[0].acquisition_id
        : result.acquisition_id;
      errorMessage += 'acquisitionId: ' + acquisitionId + '. \n ';
    }
    return errorMessage;
  }

  function processMultitaskUrl() {
    const allTaskDetails = [];
    let isSameTypeForTask = true;
    let errorMessage = `Could not open viewer for multiple tasks. Tasks are created for different parent containers. \n `;
    let isFirstTask = true;
    multipleTask &&
      taskIds?.forEach(taskid => {
        const taskContext = getViewerTaskContext(taskid)
          .then(result => {
            if (result) {
              allTaskDetails.push(result);
              if (allTaskDetails.length > 1) {
                if (allTaskDetails.length === 2) {
                  isFirstTask = true;
                  errorMessage +=
                    'Task(TaskId: ' +
                    allTaskDetails[0].task_id +
                    ') created for ' +
                    allTaskDetails[0].parent_type +
                    '. ';

                  if (
                    allTaskDetails[0].parent_type === 'project' ||
                    allTaskDetails[0].parent_type === 'session' ||
                    allTaskDetails[0].parent_type === 'acquisition'
                  ) {
                    errorMessage = getMessage(
                      allTaskDetails,
                      result,
                      errorMessage,
                      isFirstTask
                    );
                  } else if (allTaskDetails[0].parent_type === 'file') {
                    errorMessage += allTaskDetails[0].file_uuid
                      ? 'fileId: ' + allTaskDetails[0].file_uuid + '. '
                      : ' ';
                    if (!allTaskDetails[0].file_uuid) {
                      if (allTaskDetails[0].project_id !== result.project_id) {
                        errorMessage +=
                          'projectId: ' +
                          allTaskDetails[0].project_id +
                          '. \n ';
                      } else if (
                        allTaskDetails[0].session_id !== result.session_id
                      ) {
                        errorMessage +=
                          'sessionId: ' +
                          allTaskDetails[0].session_id +
                          '. \n ';
                      } else if (
                        allTaskDetails[0].acquisition_id !==
                        result.acquisition_id
                      ) {
                        errorMessage +=
                          'acquisitionId: ' +
                          allTaskDetails[0].acquisition_id +
                          '. \n ';
                      } else if (
                        allTaskDetails[0].file_name !== result.file_name
                      ) {
                        errorMessage = errorMessage.replace(
                          'parent containers.',
                          'files.'
                        );
                        errorMessage +=
                          'fileName: ' + allTaskDetails[0].file_name + '. \n ';
                      } else {
                        errorMessage += ' \n ';
                      }
                    } else {
                      errorMessage += ' \n ';
                    }
                  }
                }

                if (
                  allTaskDetails[0].project_id !== result.project_id ||
                  allTaskDetails[0].session_id !== result.session_id ||
                  allTaskDetails[0].acquisition_id !== result.acquisition_id ||
                  allTaskDetails[0].parent_type !== result.parent_type ||
                  allTaskDetails[0].file_uuid !== result.file_uuid ||
                  allTaskDetails[0].file_name !== result.file_name
                ) {
                  isSameTypeForTask = false;
                  errorMessage +=
                    'Task(TaskId: ' +
                    result.task_id +
                    ') created for ' +
                    result.parent_type +
                    '. ';
                  isFirstTask = false;

                  if (
                    result.parent_type === 'project' ||
                    result.parent_type === 'session' ||
                    result.parent_type === 'acquisition'
                  ) {
                    errorMessage = getMessage(
                      allTaskDetails,
                      result,
                      errorMessage,
                      isFirstTask
                    );
                  } else if (result.parent_type === 'file') {
                    errorMessage += result.file_uuid
                      ? 'fileId: ' + result.file_uuid + '. '
                      : ' ';
                    if (!result.file_uuid) {
                      if (allTaskDetails[0].project_id !== result.project_id) {
                        errorMessage +=
                          'projectId: ' + result.project_id + '. \n ';
                      } else if (
                        allTaskDetails[0].session_id !== result.session_id
                      ) {
                        errorMessage +=
                          'sessionId: ' + result.session_id + '. \n ';
                      } else if (
                        allTaskDetails[0].acquisition_id !==
                        result.acquisition_id
                      ) {
                        errorMessage +=
                          'acquisitionId: ' + result.acquisition_id + '. \n ';
                      } else if (
                        allTaskDetails[0].file_name !== result.file_name
                      ) {
                        errorMessage +=
                          'fileName: ' + result.file_name + '. \n ';
                      } else {
                        errorMessage += ' \n ';
                      }
                    } else {
                      errorMessage += ' \n ';
                    }
                  }
                }

                if (!isSameTypeForTask) {
                  showErrorPage(history, errorMessage, projectId);
                }
              }

              if (
                taskIds.length === allTaskDetails.length &&
                isSameTypeForTask
              ) {
                const multiTaskUrl = generateTaskUrl(result, taskIds.join(','));
                // if viewer is launched with only task id, then get task context from server and redirect to the url with context
                if (multiTaskUrl) {
                  window.location.href = `.${multiTaskUrl}`;
                }
              }
            }
          })
          .catch(error => handleCustomError('task', error));
      });
  }

  function showCustomizeError(methodName, resourceInfo) {
    const message = `${methodName} failed due to missing the ${resourceInfo}.`;
    showErrorPage(history, message, projectId);
  }

  function handleError(error) {
    showErrorPage(history, error.message);
    log.error(error);
  }

  function appendParams(path, isFirstParam = false) {
    const { slice, labels, ignoreUserSettings } = getRouteParams(location);
    if (slice !== undefined) {
      path += (isFirstParam ? '?' : '&') + `slice=${slice}`;
      isFirstParam = false;
    }
    if (labels !== undefined) {
      path += (isFirstParam ? '?' : '&') + `labels=${labels}`;
      isFirstParam = false;
    }
    if (ignoreUserSettings !== undefined) {
      path +=
        (isFirstParam ? '?' : '&') + `ignoreUserSettings=${ignoreUserSettings}`;
      isFirstParam = false;
    }
    return path;
  }

  function generateTaskUrl(taskContext, tasks = null) {
    const {
      acquisition_id,
      file_name,
      file_type,
      file_uuid,
      parent_type,
      project_id,
      session_id,
      task_id,
    } = taskContext;
    let path = `/project/${project_id}/`;

    tasks = tasks ? tasks : task_id;
    switch (parent_type) {
      case 'project': {
        path += `?taskId=${tasks}`;
        break;
      }
      case 'session': {
        path += `viewer?sessionId=${session_id}&taskId=${tasks}`;
        path = appendParams(path);
        break;
      }
      case 'acquisition': {
        path += `viewer?sessionId=${session_id}&acquisitionIds=${acquisition_id}&taskId=${tasks}`;
        path = appendParams(path);
        break;
      }
      case 'file': {
        switch (file_type) {
          case 'dicom': {
            path += `viewer?sessionId=${session_id}&acquisitionIds=${acquisition_id}&fileIds=${file_uuid}&taskId=${tasks}`;
            path = appendParams(path);
            break;
          }
          case 'image':
            path += `${file_type}/${acquisition_id}/${file_name}?taskId=${tasks}`;
            break;
          case 'tiff':
            path += `${file_type}/${acquisition_id}/${file_name}?taskId=${tasks}`;
            break;
          case 'nifti': {
            path += `${file_type}/${acquisition_id}/${file_name}?taskId=${tasks}`;
            path = appendParams(path);
            break;
          }
          case 'ITK MetaIO Header': {
            path += `metaio/${acquisition_id}/${file_name}?taskId=${tasks}`;
            path = appendParams(path);
            break;
          }
          default: {
            throw new Error(
              `Unknown file type(${file_type}) specified for reader task: ${tasks}`
            );
          }
        }
        break;
      }
      default: {
        throw new Error(
          `Viewer can not identify context for reader task: ${tasks}`
        );
      }
    }

    return path;
  }

  async function handleLocation(location) {
    try {
      const state = store.getState();
      if (location.pathname.indexOf('?') > -1) {
        const tempURL = new URL(window.location.origin + location.pathname);
        location.pathname = tempURL.pathname;
        location.search = tempURL.search;
      }
      let { acquisitionIds, fileIds } = getRouteParams(location);
      const {
        projectId,
        sessionId,
        StudyInstanceUID,
        taskId,
        slice,
      } = getRouteParams(location);

      const project = state.flywheel.project || {};

      if (projectId && projectId !== project._id && !taskIds?.length) {
        await setProject(projectId, taskId);
        return;
      } else if (projectId && projectId !== project._id && taskIds?.length) {
        await taskIds?.forEach(taskid => {
          setProject(projectId, taskid);
        });
        return;
      }

      let hpLayouts;

      if (!taskIds?.length && !taskId) {
        const state = store.getState();
        let projectConfig = state.flywheel.projectConfig;

        if (!projectConfig) {
          projectConfig = (await getConfigPromise(project, projectId)) || {};
          store.dispatch(setProjectConfig(projectConfig));
        }
        hpLayouts = projectConfig.layouts;
      }

      if (hpLayouts && !fileIds && isHpForWebImages(hpLayouts)) {
        if (sessionId) {
          const pathname = `/project/${projectId}/image/${sessionId}`;
          setTimeout(() => {
            history.replace({ pathname });
          });
          return;
        }
      } else if (
        !StudyInstanceUID &&
        (fileIds || acquisitionIds || sessionId)
      ) {
        let studyUid;
        // resolve study for DICOM (ignore nifti/image)
        fileIds = fileIds ? fileIds.split(',') : [];
        acquisitionIds = acquisitionIds ? acquisitionIds.split(',') : [];
        const acquisitions = state.flywheel.acquisitions;
        const allAssociations = state.flywheel.allAssociations;
        const firstAssociation = allAssociations.find(
          association => association.acquisition_id !== 'None'
        );
        let acquisitionData =
          acquisitions && allAssociations.length
            ? acquisitions[firstAssociation.acquisition_id]
            : null;

        const readerTask = await state.flywheel.readerTask;
        let acquisitionId;
        let dataProtocolName;
        let seriesUids;
        let filesDetails = [];
        if (
          readerTask?.filters?.length &&
          readerTask.parent.type !== 'project'
        ) {
          let filteredAcquisitionsMap;
          let allAcquisition = [];
          let firstAcqId = null;
          if (readerTask.parent.type === 'session') {
            allAcquisition = await listAcquisitions(sessionId);
          }

          // Acquisition scope reader task(readerTask.parent.type === 'acquisition') handled here
          if (acquisitionIds.length) {
            allAcquisition = [];
            const acquisitionPromises = [];
            acquisitionIds.forEach(async acqId => {
              acquisitionPromises.push(getAcquisition(acqId));
            });
            allAcquisition = await Promise.all(acquisitionPromises);
          }

          // Take all acquisitions in the reader task scope and filter
          // it and filter the files in it and create a custom filtered map
          if (allAcquisition.length) {
            // Filtering out the analyses container acquisitions
            allAcquisition = allAcquisition.filter(
              acq => acq.acquisition_id !== 'None'
            );
            // Perform the reader task filtering
            filteredAcquisitionsMap = getFilteredAcquisitionMap(
              readerTask?.filters || [],
              allAcquisition
            );
            firstAcqId = (Object.keys(filteredAcquisitionsMap) || [])[0];
            dataProtocolName = (filteredAcquisitionsMap || {})[firstAcqId]
              ?.files?.[0]?.type;
            acquisitionData = (filteredAcquisitionsMap || {})[firstAcqId]
              ?.acquisition;
          }

          filesDetails = filteredAcquisitionsMap
            ? getFileDetailsFromAcquisition(filteredAcquisitionsMap)
            : [];

          if (dataProtocolName && dataProtocolName !== 'dicom') {
            acquisitionId =
              firstAcqId || Object.keys(filteredAcquisitionsMap || {})[0];
          } else if (firstAcqId) {
            if (dataProtocolName === 'dicom') {
              let studyAssociations = selectAllAssociations(store.getState());
              studyAssociations = studyAssociations.filter(
                data =>
                  Object.keys(filteredAcquisitionsMap).find(
                    key => key === data.acquisition_id
                  ) &&
                  filteredAcquisitionsMap[data.acquisition_id]?.files.find(
                    file => file._id === data.file_id
                  )
              );
              studyUid = studyAssociations?.length
                ? studyAssociations[0]?.study_uid
                : studyUid;
              if (studyUid) {
                seriesUids = studyAssociations
                  .filter(data => data.study_uid === studyUid)
                  .map(association => association.series_uid)
                  .join(',');
              }
            }
            if (!studyUid) {
              // general study_uid
              studyUid = getStudyUid({
                fileId: filesDetails[0]?.fileId,
                acquisitionId: firstAcqId,
                sessionId,
                acquisition: acquisitionData,
              });
            }
          }

          if (!studyUid && !dataProtocolName) {
            showErrorPage(
              history,
              'Could not find a supporting file with the reader task filter applied',
              project
            );
          }
        } else {
          // general study_uid
          studyUid = getStudyUid({
            fileId: fileIds[0],
            acquisitionId: acquisitionIds[0],
            sessionId,
            acquisition: acquisitionData,
          });
        }

        if (!studyUid && (!dataProtocolName || dataProtocolName === 'dicom')) {
          // fetch associations for query params and circle back
          const params = sessionId
            ? { session_id: sessionId }
            : fileIds.length > 0
            ? { file_id: fileIds[0] }
            : acquisitionIds.length > 0
            ? { acquisition_id: acquisitionIds[0] }
            : {};
          getProjectAssociations(projectId, params)
            .then(associations => {
              store.dispatch(setAssociations(associations));
              let isFileIdMatched = false;
              const firstAssociation = associations.find(
                association => association.acquisition_id !== 'None'
              );
              const acquisitionPromise = getAcquisition(
                firstAssociation.acquisition_id
              );
              acquisitionPromise.then(acquisition => {
                let acquisitionsList = state.flywheel.acquisitions || {};
                acquisitionsList[firstAssociation.acquisition_id] = acquisition;
                store.dispatch(setAcquisitions(acquisitionsList));
                if (fileIds?.[0]) {
                  associations.forEach(projectAssociations => {
                    acquisition.files.length &&
                      acquisition.files.forEach(file => {
                        if (projectAssociations.file_id === file._id) {
                          isFileIdMatched = true;
                        }
                      });
                  });
                  if (!isFileIdMatched) {
                    showErrorPage(
                      history,
                      'Could not open requested study. Unable to fetch associated acquisition file.',
                      firstAssociation.project_id
                    );
                  }
                }
                // general study_uid
                studyUid = getStudyUid({
                  fileId: fileIds[0],
                  acquisitionId: acquisitionIds[0],
                  sessionId,
                  acquisition,
                });
                if (studyUid) {
                  handleLocation(history.location);
                } else {
                  throw new Error(
                    'Missing association to study uid from file id ' +
                      fileIds[0]
                  );
                }
              });
            })
            .catch(error => {
              log.error(error);
              const message = sessionId
                ? `Session "${sessionId}" contains no valid DICOM studies, or its studies have not been indexed yet.`
                : 'No studies are associated to the requested DICOM file, or it has not been indexed yet.';
              showErrorPage(history, message, projectId);
            });
          return;
        }
        /**
         * If fileIds are provided, try to get series order via file_id,
         * then try by acquisition_id.
         * fileIds are file UUIDs on the storage provider, and io-proxy
         * may rename this field to file_uuid in the associations response
         * to distinguish it from file_id for the file record in core.
         * Remove acquisition_id fallback when FLYW-5850 is stable.
         */
        let seriesOrderedUids = [];

        if (fileIds.length > 0) {
          seriesOrderedUids = (
            await Promise.all(fileIds.map(fileId => getFileUids(fileId)))
          )
            .map(series => series && series.series_uid)
            .filter(uid => uid);
        }

        const fileId = (filesDetails || [])[0]?.fileId;
        if (fileId) {
          const layouts = store.getState().flywheel.projectConfig?.layouts;
          if (dataProtocolName && dataProtocolName !== 'dicom') {
            const fileName = (filesDetails || [])[0]?.fileName;
            const protocolName =
              dataProtocolName === 'ITK MetaIO Header'
                ? 'metaio'
                : dataProtocolName;
            let pathname = `/project/${projectId}/`;
            pathname += `${protocolName}/${acquisitionId}/${fileName}?taskId=${taskId}`;

            if (slice !== undefined) {
              pathname += `&slice=${slice}`;
            }

            setTimeout(() => {
              history.replace({ pathname });
            });
          } else if (!layouts) {
            const fileData = await Promise.resolve(getFileUids(fileId));
            seriesOrderedUids =
              fileData && fileData.series_uid ? [fileData.series_uid] : [];
          }
        } else if (
          seriesOrderedUids.length === 0 &&
          acquisitionIds.length > 0
        ) {
          seriesOrderedUids = (
            await Promise.all(
              acquisitionIds.map(acqId => getAcquisitionUids(projectId, acqId))
            )
          )
            .map(series => series && series.series_uid)
            .filter(uid => uid);
        }

        if (seriesOrderedUids.length > 0) {
          store.dispatch(setSeriesOrder(seriesOrderedUids));
        }

        if (!dataProtocolName || dataProtocolName === 'dicom') {
          let pathname = `/project/${projectId}/viewer/${studyUid}`;

          if (seriesUids?.length) {
            pathname += `/` + seriesUids;
          } else if (seriesOrderedUids.length === 1) {
            pathname += `/${seriesOrderedUids[0]}`;
          }

          if (taskId) {
            pathname += `?taskId=${taskId}`;
          }

          pathname = appendParams(pathname, taskId === undefined);

          setTimeout(() => {
            history.replace({ pathname });
          });
        }
      }

      if (sessionId || StudyInstanceUID) {
        const resolvedSessionId =
          sessionId ||
          (StudyInstanceUID ? getSessionId(StudyInstanceUID) : null);
        const allSessions = selectAllSessions(store.getState());
        if (resolvedSessionId && !(resolvedSessionId in allSessions)) {
          // set the initial session to load quickly
          // other project sessions will arrive later
          const session = await getSession(resolvedSessionId);
          const { user } = store.getState().flywheel;
          const readData = _.get(
            session,
            `info.ohifViewer.read.${customInfoKey(user._id)}`
          );
          store.dispatch(
            setAllSessions({
              [session._id]: {
                ...session,
                isRead: Boolean(readData),
                readOnly: readData?.readOnly || false,
              },
            })
          );
        }
        // ensure association for study_uid -> session_id
        const viewerAssociations = selectAllAssociations(
          store.getState()
        ).filter(
          assoc =>
            assoc.study_uid === StudyInstanceUID ||
            assoc.session_id === sessionId
        );
        if (viewerAssociations.length === 0) {
          const queryParams = StudyInstanceUID
            ? { study_uid: StudyInstanceUID }
            : { session_id: sessionId };
          getProjectAssociations(projectId, queryParams).then(associations => {
            store.dispatch(setAssociations(associations));
          });
        }
      }
    } catch (error) {
      handleError(error);
    }
  }

  async function setProject(projectId, taskId = '') {
    try {
      setLoadersConfig(projectId);
      const independentPromises = [];
      const state = store.getState();

      independentPromises.push(
        getProject(projectId).then(async project => {
          store.dispatch(setActiveProject(project));
          const featureConfig = store.getState().flywheel.featureConfig;
          if (featureConfig?.features?.reader_tasks) {
            const rolesPromise = getRoles();
            rolesPromise.catch(handleCustomError.bind(this, 'roles'));
            const user = selectUser(store.getState());

            let viewerConfigPromise = null;
            let viewerFormPromise = null;
            let protocolPromise = null;
            if (taskId) {
              const readerTaskPromise = getReaderTask(taskId);
              store.dispatch(setReaderTask(readerTaskPromise));

              readerTaskPromise.catch(handleCustomError.bind(this, 'task'));
              const readerTask = await readerTaskPromise;
              remoteService.signalTaskOpened(readerTask);

              const multipleReaderTask = store.getState().multipleReaderTask;
              if (
                multipleReaderTask.multipleTaskResponse.length &&
                multipleReaderTask.multipleTaskResponse[0].viewer_config_id !==
                  readerTask?.viewer_config_id
              ) {
                let errorMessageViewerConfig =
                  'Could not open viewer for multiple tasks. Viewer config is different. \n ';
                errorMessageViewerConfig +=
                  'Task: ' +
                  multipleReaderTask.multipleTaskResponse[0]._id +
                  ', ViewerConfigId: ' +
                  multipleReaderTask.multipleTaskResponse[0]?.viewer_config_id +
                  '. \n ';
                errorMessageViewerConfig +=
                  'Task: ' +
                  taskId +
                  ', ViewerConfigId: ' +
                  readerTask?.viewer_config_id +
                  '. \n ';
                showErrorPage(history, errorMessageViewerConfig, projectId);
              } else if (
                multipleReaderTask.multipleTaskResponse.length &&
                multipleReaderTask.multipleTaskResponse[0].form_id !==
                  readerTask?.form_id
              ) {
                let errorMessageForm =
                  'Could not open viewer for multiple tasks. Form is different. \n ';
                errorMessageForm +=
                  'Task: ' +
                  multipleReaderTask.multipleTaskResponse[0]._id +
                  ', FormId: ' +
                  multipleReaderTask.multipleTaskResponse[0]?.form_id +
                  '. \n ';
                errorMessageForm +=
                  'Task: ' +
                  taskId +
                  ', FormId: ' +
                  readerTask?.form_id +
                  '. \n ';
                showErrorPage(history, errorMessageForm, projectId);
              }

              if (taskIds.length > 1) {
                store.dispatch(setMultipleReaderTaskResponse(readerTask));
              }

              protocolPromise = new Promise((resolve, reject) => {
                let viewerConfigId = readerTask?.viewer_config_id;
                let formId = readerTask?.form_id;
                const protocolId = readerTask?.protocol_id;
                let requestPromise = null;
                if (protocolId && (!viewerConfigId || !formId)) {
                  requestPromise = getReaderTaskProtocol(protocolId);
                } else {
                  requestPromise = Promise.resolve({
                    viewer_config_id: viewerConfigId,
                    form_id: formId,
                  });
                }
                requestPromise
                  .then(({ viewer_config_id, form_id }) => {
                    // Preference for the viewer config/form defined in the reader task(if exist)
                    viewerConfigId = viewerConfigId || viewer_config_id;
                    formId = formId || form_id;
                    let readerTaskModified = {
                      ...readerTask,
                      ...{ form_id: formId, viewer_config_id: viewerConfigId },
                    };
                    if (viewerConfigId) {
                      viewerConfigPromise = getViewerConfig(viewerConfigId);
                      viewerConfigPromise.catch(
                        handleCustomError.bind(this, 'viewer config')
                      );
                    }

                    if (formId) {
                      if (readerTask.assignee === user._id) {
                        viewerFormPromise = getViewerForm(formId);
                      } else {
                        viewerFormPromise = getAllViewerForm(formId).then(
                          res => res.results[0]
                        );
                      }
                      viewerFormPromise.catch(
                        handleCustomError.bind(this, 'form')
                      );
                    }
                    store.dispatch(
                      setReaderTask(Promise.resolve(readerTaskModified))
                    );
                    resolve();
                  })
                  .catch(handleCustomError.bind(this, 'protocol'));
              });
            } else {
              store.dispatch(setReaderTask(Promise.resolve(null)));
            }

            Promise.all([protocolPromise])
              .then(() => {
                Promise.all([
                  viewerConfigPromise,
                  viewerFormPromise,
                  rolesPromise,
                ])
                  .then(async ([viewerConfig, viewerForm, roles]) => {
                    let projectConfig = {};
                    const readerTask = await selectReaderTask(
                      window.store.getState()
                    );
                    if (taskId && readerTask) {
                      if (viewerConfig) {
                        projectConfig = {
                          ...projectConfig,
                          ...viewerConfig,
                        };
                      }
                      if (viewerForm && viewerForm.form) {
                        if (viewerForm.form?.studyForm) {
                          store.dispatch(setStudyFormAvailableStatus(true));
                        }
                        projectConfig = {
                          ...projectConfig,
                          ...viewerForm.form,
                        };
                      }
                      if (readerTask.config_overrides) {
                        if (readerTask.config_overrides?.studyForm) {
                          store.dispatch(setStudyFormAvailableStatus(true));
                        }
                        projectConfig = mergeDeep(
                          projectConfig,
                          readerTask.config_overrides
                        );
                      }
                      if (state.multipleReaderTask.TaskIds.length > 1) {
                        projectConfig.contourUnique = false;
                      }
                    } else {
                      const configPromise = getConfigPromise(
                        project,
                        projectId
                      );
                      projectConfig = (await configPromise) || {};
                      if (
                        projectConfig?.studyForm ||
                        projectConfig?.questions
                      ) {
                        store.dispatch(setStudyFormAvailableStatus(true));
                      }
                    }

                    if (roles && roles.length) {
                      store.dispatch(setRoles(roles));
                      const currentUserPermission = project.permissions.find(
                        permission => permission._id === user._id
                      );
                      if (currentUserPermission) {
                        const roleIds = currentUserPermission.role_ids;
                        const userRoles = roles.filter(role =>
                          roleIds.find(roleId => roleId === role._id)
                        );
                        const permissions = _.uniq(
                          [].concat.apply(
                            [],
                            userRoles.map(role => role.actions)
                          )
                        );
                        store.dispatch(setPermissions(permissions));
                      } else if (user.roles.includes('site_admin')) {
                        store.dispatch(setPermissions(Promise.resolve([])));
                      } else {
                        handleCustomError('project', new Error(''));
                        return;
                      }
                    } else {
                      store.dispatch(setPermissions(Promise.resolve([])));
                    }
                    if (projectConfig?.paletteColors) {
                      store.dispatch(
                        setPaletteColorsFromConfig(projectConfig.paletteColors)
                      );
                    }
                    if (projectConfig?.smartCTRanges) {
                      store.dispatch(
                        setAvailableSmartRanges(projectConfig.smartCTRanges)
                      );
                    }
                    store.dispatch(setProjectConfig(projectConfig));
                    setRightPanelExpansionMode(projectConfig, store);
                    const hotkeys = projectConfig?.hotkeys;
                    if (projectConfig && hotkeys) {
                      hotkeysManager?.setHotkeys(hotkeys);
                    }
                    performanceTracking.enablePerformanceTracker(
                      projectConfig?.evaluatePerformance
                    );
                    if (multipleTask && user._id !== readerTask?.assignee) {
                      const isSiteAdmin = user.roles.includes('site_admin');
                      const hasViewOthersAnnotationPermission = await hasPermission(
                        state,
                        'annotations_view_others'
                      );
                      const hasEditAnnotationsOthersPermission = await hasPermission(
                        state,
                        'annotations_edit_others'
                      );
                      const hasViewOthersFormPermission = await hasPermission(
                        state,
                        'form_responses_view_others'
                      );
                      const hasEditFormOthersPermission = await hasPermission(
                        state,
                        'form_responses_edit_others'
                      );
                      if (
                        !isSiteAdmin &&
                        (!hasViewOthersAnnotationPermission ||
                          !hasViewOthersFormPermission) &&
                        (!hasEditAnnotationsOthersPermission ||
                          !hasEditFormOthersPermission)
                      ) {
                        const errorMessagePermission =
                          'Could not open viewer for multiple tasks. This user have no privilege to view other task.';
                        showErrorPage(
                          history,
                          errorMessagePermission,
                          projectId
                        );
                      }
                    }
                  })
                  // The error's are already handled in corresponding request
                  // exceptions, hence only logged here.
                  .catch(err => log.error(err));
              })
              .catch(err => log.error(err));
          } else if (
            project.files.some(file => file.name === OHIF_CONFIG_FILENAME)
          ) {
            const configPromise = getConfigPromise(project, projectId);
            const config = (await configPromise) || {};
            store.dispatch(setProjectConfig(config));
            setRightPanelExpansionMode(config);
            if (config?.studyForm || config?.questions) {
              store.dispatch(setStudyFormAvailableStatus(true));
            }
            performanceTracking.enablePerformanceTracker(
              config?.evaluatePerformance
            );
            const hotkeys = config?.hotkeys;
            if (config && hotkeys) {
              hotkeysManager?.setHotkeys(hotkeys);
            }
            if (config?.paletteColors) {
              store.dispatch(setPaletteColorsFromConfig(config.paletteColors));
            }
            if (config?.smartCTRanges) {
              store.dispatch(setAvailableSmartRanges(config.smartCTRanges));
            }
            store.dispatch(setReaderTask(Promise.resolve(null)));
            store.dispatch(setPermissions(Promise.resolve([])));
          } else {
            // set empty project config to let other parts in viewer aware that
            // the config retrieval is now completed
            store.dispatch(setProjectConfig({}));
            store.dispatch(setReaderTask(Promise.resolve(null)));
            store.dispatch(setPermissions(Promise.resolve([])));
          }
        })
      );

      store.dispatch(clearAssociations());
      // load all project associations async FLYW-8550
      getProjectAssociations(projectId).then(associations => {
        store.dispatch(setAssociations(associations));
      });

      //load file associations for nifti files
      if (FlywheelCommonUtils.isCurrentNifti(store.getState())) {
        const {
          fileContainerId,
          filename,
        } = FlywheelCommonUtils.getRouteParams(window.location);
        store.dispatch(
          setFileAssociations({
            containerId: fileContainerId,
            filename: filename,
          })
        );
      }

      // load all project sessions async with no await
      // this one can be really slow FLYW-7794
      const { user } = store.getState().flywheel;
      Promise.all([
        listSessions(projectId, [
          `info.ohifViewer.read.${customInfoKey(user._id)}=null`,
        ]),
        listSessions(projectId, [
          `info.ohifViewer.read.${customInfoKey(user._id)}!=null`,
        ]),
        listSessions(projectId, [
          `info.ohifViewer.read.${customInfoKey(user._id)}.readOnly=true`,
        ]),
      ]).then(([unreadSessions, readSessions, readOnlySessions]) => {
        const readOnly = new Set(readOnlySessions.map(session => session._id));
        store.dispatch(
          setAllSessions({
            ...Object.assign(
              {},
              ...unreadSessions.map(session => ({
                [session._id]: {
                  ...session,
                  isRead: false,
                  readOnly: readOnly.has(session._id),
                },
              }))
            ),
            ...Object.assign(
              {},
              ...readSessions.map(session => ({
                [session._id]: {
                  ...session,
                  isRead: true,
                  readOnly: readOnly.has(session._id),
                },
              }))
            ),
          })
        );
      });

      // wait for data
      await Promise.all(independentPromises);

      handleLocation(history.location); // route for study/session
    } catch (error) {
      handleError(error);
    }
  }

  function handleCustomError(resource, err) {
    if (err.message === 'Forbidden' || err.code === 401 || err.code === 403) {
      err = {
        ...err,
        message: `Forbidden: You do not have access to this ${resource}`,
      };
    } else if (
      err.message === 'Not Found' ||
      err.message === 'Unprocessable Entity' ||
      err.code === 404 ||
      err.code === 422
    ) {
      err = {
        ...err,
        message: `We could not provide details of the requested ${resource}, Please confirm the URL is valid`,
      };
    } else {
      err = {
        ...err,
        message: err.message || `Error retrieving the resource ${resource}`,
      };
    }
    handleError(err);
  }

  function setLoadersConfig(projectId) {
    const server = appConfig.servers.dicomWeb[0];
    if (server.wadoRoot.includes(projectId)) {
      return; // this module has been hot-loaded and the configuration is already set
    }
    const apiRoot = appConfig.apiRoot || '';
    Object.assign(server, {
      // Prefix WADO requests with the project ID
      wadoUriRoot: `${apiRoot}${baseServer.wadoUriRoot}/projects/${projectId}`,
      qidoRoot: `${apiRoot}${baseServer.qidoRoot}/projects/${projectId}`,
      wadoRoot: `${apiRoot}${baseServer.wadoRoot}/projects/${projectId}`,
      requestOptions: {
        // Send the auth token for all WADO requests
        auth: () => getAuthToken(),
      },
    });
    addServers(appConfig.servers, store);
    cornerstoneWADOImageLoader.configure({
      // Send the auth token for image loader requests
      beforeSend: xhr => {
        xhr.setRequestHeader('Authorization', getAuthToken());
      },
    });

    cornerstoneWebImageLoader.external.cornerstone = cornerstone;
    cornerstoneWebImageLoader.configure({
      // Send the auth token for image loader requests
      beforeSend: xhr => {
        xhr.setRequestHeader('Authorization', getAuthToken());
      },
    });
  }

  function setNifiLoaderConfig() {
    cornerstoneNIFTIImageLoader.nifti.streamingMode = true;
    registerNiftiMetaDataProvider(
      cornerstone,
      cornerstoneNIFTIImageLoader.nifti
    );
    cornerstoneNIFTIImageLoader.nifti.configure({
      headers: {
        Authorization: getAuthToken(),
      },
    });
    cornerstoneNIFTIImageLoader.external.cornerstone = cornerstone;
  }

  function getAssociationByFileId(associations, acquisition) {
    if (acquisition) {
      associations = associations.filter(projectAssociations => {
        const id =
          acquisition.files.length &&
          acquisition.files.find(
            file => projectAssociations.file_id === file._id
          )?._id;
        return projectAssociations.file_id === id;
      });
    }
    return associations;
  }

  function getStudyUid({ fileId, acquisitionId, sessionId, acquisition }) {
    // Get associations
    let associations = selectAllAssociations(store.getState());
    // Filter by file_id or acquisition_id or session_id
    if (fileId && associations.some(assoc => assoc.file_id)) {
      associations = associations.filter(assoc => assoc.file_id === fileId);
    } else if (
      acquisitionId &&
      associations.some(assoc => assoc.acquisition_id)
    ) {
      associations = associations.filter(
        assoc => assoc.acquisition_id === acquisitionId
      );
      // make sure if file exists in server for selected series instance uid
      associations = getAssociationByFileId(associations, acquisition);
    } else {
      associations = associations.filter(
        assoc => assoc.session_id === sessionId
      );
      // make sure if file exists in server for selected series instance uid
      associations = getAssociationByFileId(associations, acquisition);
    }
    const uids = associations[0] || {};
    return uids?.study_uid;
  }

  function getSessionId(studyUid) {
    const associations = selectAllAssociations(store.getState()).filter(
      assoc => assoc.study_uid === studyUid
    );
    const uids = associations[0] || {};
    return uids.session_id;
  }

  function getAcquisitionUids(projectId, acquisitionId) {
    const associations = selectAllAssociations(store.getState());
    if (associations.some(assoc => assoc.acquisition_id)) {
      return associations.find(assoc => assoc.acquisition_id === acquisitionId);
    }
    return getAcquisitionAssociation(projectId, acquisitionId).catch(error => {
      console.warn('Error fetching acquisition association', error);
      return undefined;
    });
  }

  function getFileUids(fileId) {
    return selectAllAssociations(store.getState()).find(
      assoc => assoc.file_id === fileId
    );
  }

  async function getConfigPromise(project, projectId) {
    if (project.files?.some(file => file.name === OHIF_CONFIG_FILENAME)) {
      if (!projectConfigPromise) {
        projectConfigPromise = getProjectFileJson(
          projectId,
          OHIF_CONFIG_FILENAME
        );
      }
      return await projectConfigPromise.catch(error =>
        handleCustomError('project config', error)
      );
    } else {
      return {};
    }
  }
}

function logInitialLoadingPerformance(store, location) {
  const startEntry = {
    key: INITIAL_IMAGE_LOAD,
    id: location,
    startTime: Date.now(),
  };
  performanceStart(startEntry);

  let handler = undefined;
  handler = event => {
    const endEntry = {
      key: INITIAL_IMAGE_LOAD,
      id: location,
      endTime: Date.now(),
    };
    performanceEnd(endEntry);

    cornerstone.events.removeEventListener(
      cornerstone.EVENTS.IMAGE_LOADED,
      handler
    );
  };
  cornerstone.events.addEventListener(cornerstone.EVENTS.IMAGE_LOADED, handler);
}

function loadCustomColormaps() {
  const baseRoot = window.PUBLIC_URL || '/';
  return new Promise((resolve, reject) => {
    getColorMapsJson(baseRoot)
      .then(jsonResponse => {
        const promises = jsonResponse.map(colormap =>
          loadCustomColormapFromJson(colormap)
        );
        Promise.all(promises).then(() => {
          resolve();
        });
      })
      .catch(reason => {
        reject(reason);
      });
  });
}

function loadCustomColormapFromJson(colormapFileData) {
  const rootPath = window.PUBLIC_URL || '/';
  return new Promise((resolve, reject) => {
    const url = getRequestURL(
      `${rootPath}${colormapFileData.path}`,
      undefined,
      { prefixRoot: false, normalize: true }
    );
    getFile(url, 'text')
      .then(textResponse => {
        const colormapData = textResponse;
        let colorLines = colormapData.split('\n');
        const colormap = cornerstone.colors.getColormap(colormapFileData.id);

        colormap.setColorSchemeName(colormapFileData.name);
        colormap.setNumberOfColors(colorLines.length);

        colorLines.forEach((colorLine, index) => {
          let colors = colorLine.split(/\s+/);
          let red = parseFloat(colors[0]) * 255;
          let green = parseFloat(colors[1]) * 255;
          let blue = parseFloat(colors[2]) * 255;

          if (!isNaN(red) && !isNaN(green) && !isNaN(blue)) {
            colormap.insertColor(index, [red, green, blue, 255]);
          }
        });

        resolve();
      })
      .catch(error => {
        reject(error);
      });
  });
}

function setRightPanelExpansionMode(config) {
  if (config?.rightPanelExpansionMode) {
    const panelMode = config.rightPanelExpansionMode.toUpperCase();
    store.dispatch(changeRightHandPanelMode(panelMode));
  }
}
