import cornerstone from 'cornerstone-core';
import Reactive from '../reactive';

export default function init({ commandsManager, appConfig }) {
  const { enableAutoFullDynamicWWWC } = appConfig;
  function elementEnabledHandler(event) {
    if (enableAutoFullDynamicWWWC) {
      Reactive.fullDynamicWWWC.subscribe(event, commandsManager);
    }
  }

  function elementDisabledHandler(event) {}

  cornerstone.events.addEventListener(
    cornerstone.EVENTS.ELEMENT_ENABLED,
    elementEnabledHandler
  );
  cornerstone.events.addEventListener(
    cornerstone.EVENTS.ELEMENT_DISABLED,
    elementDisabledHandler
  );
}
