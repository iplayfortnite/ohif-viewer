import redux from '../redux';
import {
  Redux as FlywheelCommonRedux,
  HTTP as FlywheelCommonHTTP,
} from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import merge from 'lodash.merge';
import { redux as coreRedux } from '@ohif/core';
import OHIF from '@ohif/core';

const measurementApi = OHIF.measurements.MeasurementApi;
const {
  setMeasurementColorIntensity,
  setSingleAvatar,
  setViewerAvatar,
  setActiveTool,
} = FlywheelCommonRedux.actions;
const { measurementTools } = FlywheelCommonRedux.selectors;
const {
  setReaderTaskWithFormResponse,
  setReaderTask,
  setFormResponse,
} = redux.actions;
const { getReaderTask } = FlywheelCommonHTTP.services;
const { selectUser, hasPermission } = redux.selectors;
const { setViewportLayoutAndData } = coreRedux.actions;

const avatarClick = async (
  userAvatarClickedDetails,
  slice,
  initials,
  taskId,
  e,
  dispatchActions,
  measurements
) => {
  e.stopPropagation();
  e.preventDefault();
  const state = store.getState();
  const user = selectUser(state);
  const isSiteAdmin = user.roles.includes('site_admin');
  const hasEditAnnotationsOthersPermission = await hasPermission(
    state,
    'annotations_edit_others'
  );
  const hasEditFormOthersPermission = await hasPermission(
    state,
    'form_responses_edit_others'
  );

  if (
    isSiteAdmin ||
    (hasEditAnnotationsOthersPermission && hasEditFormOthersPermission)
  ) {
    const viewerAvatar = state.viewerAvatar.viewerAvatar;

    if (viewerAvatar.length) {
      userAvatarClickedDetails = viewerAvatar;
    }

    let isAllTrue = false;
    isAllTrue = userAvatarClickedDetails.every(data => data.visible === true);
    if (isAllTrue) {
      userAvatarClickedDetails.forEach(user => {
        if (user.visible === true) {
          user.visible = false;
        }
        if (user[user.initials + '-' + user.taskId] === true) {
          user[user.initials + '-' + user.taskId] = false;
        }
      });
    }

    let userClickedCount = 0;
    let singleTaskId;
    userAvatarClickedDetails.forEach(user => {
      if (
        user[initials + '-' + taskId] === false ||
        user[initials + '-' + taskId] === true
      ) {
        user[initials + '-' + taskId] = !user[initials + '-' + taskId];
        user.visible = !user.visible;
      }
      if (user.visible) {
        userClickedCount += 1;
        singleTaskId = user.taskId;
      }
    });

    let isAll = false;
    isAll = userAvatarClickedDetails.every(data => data.visible === false);
    if (isAll) {
      userAvatarClickedDetails.forEach(user => {
        if (user.visible === false) {
          user.visible = true;
        }
        if (user[user.initials + '-' + user.taskId] === false) {
          user[user.initials + '-' + user.taskId] = true;
        }
      });
    }

    store.dispatch(setViewerAvatar(userAvatarClickedDetails));
    const avatarClickedInfo = { status: false, taskId: taskId };
    const multipleTaskNotes = { ...state.viewerAvatar.multipleTaskNotes };

    if (userClickedCount === 1) {
      if (multipleTaskNotes[singleTaskId]) {
        const readerTaskPromise = getReaderTask(singleTaskId);
        store.dispatch(setReaderTask(readerTaskPromise));

        const multipleTaskFormResponses = [
          ...state.viewerAvatar.multipleTaskFormResponses,
        ];
        const formResponse = multipleTaskFormResponses.find(
          response => response.task_id === singleTaskId
        );
        store.dispatch(setFormResponse(formResponse));

        const multipleTaskResponse = [
          ...state.multipleReaderTask.multipleTaskResponse,
        ];
        const readerTask = multipleTaskResponse.find(
          response => response._id === singleTaskId
        );

        const readerTaskNotes = multipleTaskNotes[singleTaskId].notes[0];
        readerTask.info.notes = readerTaskNotes;
        store.dispatch(setReaderTaskWithFormResponse(readerTask));

        avatarClickedInfo.status = true;
        avatarClickedInfo.taskId = singleTaskId;
        store.dispatch(setSingleAvatar(avatarClickedInfo));

        const numRows = state.viewports.numRows;
        const numColumns = state.viewports.numColumns;
        const viewportSpecificData = state.viewports.viewportSpecificData;
        const viewports = state.viewports.layout.viewports;

        if (numRows > 1 || numColumns > 1) {
          store.dispatch(
            setViewportLayoutAndData(
              {
                numRows,
                numColumns,
                viewports,
              },
              viewportSpecificData
            )
          );
        }
      }
    } else {
      if (multipleTaskNotes[taskId]) {
        const readerTaskWithFormResponse = {
          ...state.flywheel?.readerTaskWithFormResponse,
        };
        Object.keys(multipleTaskNotes).forEach(taskId => {
          const readerTaskNotes = multipleTaskNotes[taskId].notes[0];
          readerTaskWithFormResponse.info.notes = merge(
            {},
            readerTaskWithFormResponse.info.notes,
            readerTaskNotes
          );
        });
        store.dispatch(
          setReaderTaskWithFormResponse(readerTaskWithFormResponse)
        );
        store.dispatch(setSingleAvatar(avatarClickedInfo));
      }
    }

    store.dispatch(setActiveTool('Wwwc'));

    userWiseMeasurementEnable(
      dispatchActions,
      userAvatarClickedDetails,
      multipleTaskNotes,
      measurements,
      e
    );
  }
};

const userWiseMeasurementEnable = async (
  dispatchActions,
  userAvatarClickedDetails,
  multipleTaskNotes,
  measurements,
  e
) => {
  if (!dispatchActions) {
    dispatchActions = require('@ohif/viewer/src/appExtensions/MeasurementsPanel/dispatchActions.js')
      .default;
  }

  const visibleUser = [];
  userAvatarClickedDetails.forEach(user => {
    Object.keys(user).forEach(data => {
      if (user[data] === true) {
        const taskId = user.taskId;
        if (multipleTaskNotes?.[taskId]?.userId) {
          visibleUser.push(multipleTaskNotes?.[taskId]?.userId);
        }
      }
    });
  });

  Object.keys(measurements).forEach(key => {
    measurements[key]?.forEach(measurement => {
      if (visibleUser.includes(measurement.flywheelOrigin.id)) {
        measurement.visible = false;
        dispatchActions.onVisibleClick(e, measurement, false);
      } else {
        measurement.visible = true;
        dispatchActions.onVisibleClick(e, measurement, false);
      }
    });
  });

  measurementApi.Instance.syncMeasurementsAndToolData();
  measurementApi.Instance.onMeasurementsUpdated();
};

const onAvatarMouseEnter = (
  e,
  dispatchActions,
  allMeasurements,
  user,
  questionKey,
  taskId,
  slice,
  initials
) => {
  e.target.style.color = 'white';
  e.target.style.borderWidth = '1px';
  e.target.style.borderStyle = 'solid';

  const data = initials + '-' + taskId;
  const avatarElements = document.getElementsByClassName(data).length;
  if (avatarElements > 0) {
    for (let n = 0; n < avatarElements; n++) {
      document.getElementsByClassName(data).item(n).style.color = 'white';
      document.getElementsByClassName(data).item(n).style.borderWidth = '1px';
      document.getElementsByClassName(data).item(n).style.borderStyle = 'solid';
    }
  }

  if (!dispatchActions) {
    dispatchActions = require('@ohif/viewer/src/appExtensions/MeasurementsPanel/dispatchActions.js')
      .default;
  }

  const measurements = [];
  measurementTools.forEach(tool => {
    allMeasurements?.[tool]?.forEach(annotation => {
      const isShow =
        questionKey !== null
          ? annotation?.flywheelOrigin?.id === user?._id &&
            annotation?.task_id === taskId &&
            annotation?.questionKey === questionKey
          : annotation?.flywheelOrigin?.id === user?._id &&
            annotation?.task_id === user?.task_id;
      if (isShow) {
        annotation.measurementId = annotation._id;
        measurements.push(annotation);
      }
    });
  });
  store.dispatch(setMeasurementColorIntensity(true));
  dispatchActions?.dispatchActiveHoveredMeasurements(
    measurements,
    store.dispatch
  );
};

const onAvatarMouseLeave = (
  e,
  dispatchActions,
  allMeasurements,
  taskId,
  slice,
  initials
) => {
  e.target.style.color = 'black';
  e.target.style.borderWidth = '1px';
  e.target.style.borderStyle = 'solid';

  const data = initials + '-' + taskId;
  const avatarElements = document.getElementsByClassName(data).length;
  if (avatarElements > 0) {
    for (let n = 0; n < avatarElements; n++) {
      document.getElementsByClassName(data).item(n).style.color = 'black';
      document.getElementsByClassName(data).item(n).style.borderWidth = '1px';
      document.getElementsByClassName(data).item(n).style.borderStyle = 'solid';
    }
  }

  if (!dispatchActions) {
    dispatchActions = require('@ohif/viewer/src/appExtensions/MeasurementsPanel/dispatchActions.js')
      .default;
  }

  const measurements = [];
  measurementTools.forEach(tool => {
    allMeasurements?.[tool]?.forEach(annotation => {
      annotation.measurementId = annotation._id;
      measurements.push(annotation);
    });
  });
  store.dispatch(setMeasurementColorIntensity(false));
  dispatchActions?.dispatchActiveHoveredMeasurements(
    measurements,
    store.dispatch
  );
};

export { avatarClick, onAvatarMouseEnter, onAvatarMouseLeave };
