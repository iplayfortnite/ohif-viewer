import store from '@ohif/viewer/src/store';

export default function check2dmprViewportPresent() {
  let twoDMPR = false;
  const viewportSpecificData = store.getState().viewports.viewportSpecificData;
  const checkVtkViewport = key => viewportSpecificData[key].plugin === 'vtk';
  twoDMPR = Object.keys(viewportSpecificData).some(checkVtkViewport);
  return twoDMPR;
}
