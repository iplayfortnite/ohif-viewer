import Redux from '../../../redux';
import cloneDeep from 'lodash.clonedeep';
import {
  HTTP as FlywheelCommonHTTP,
  Utils as FlywheelCommonUtils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import OHIF from '@ohif/core';
import FlywheelRedux from '../../../redux';

import { adaptToApi } from './dataAdapters';
import store from '@ohif/viewer/src/store';
import { filterFormResponse } from '../../../components/StudyForm/utils/filterFormResponse';

const {
  setActiveSessions,
  setCurrentNifti,
  setCurrentNiftis,
  setCurrentMetaImage,
  setFormResponse,
  setCurrentWebImage,
  setReaderTaskWithFormResponse,
} = Redux.actions;
const {
  selectActiveSessions,
  selectUser,
  selectStudySessions,
  selectAllAssociations,
  selectReaderTask,
  hasPermission,
} = Redux.selectors;
const {
  addDeletedMeasurementId,
  removeDeletedMeasurementId,
  setSaveStatus,
} = OHIF.redux.actions;
const { log } = OHIF;
const { setReaderTask } = FlywheelRedux.actions;

const {
  setFileInfo,
  setSessionInfo,
  createAnnotation,
  modifyAnnotation,
  deleteAnnotation,
  setViewerFormResponse,
  updateViewerFormResponse,
  updateReaderTask,
} = FlywheelCommonHTTP.services;
const { customInfoKey } = FlywheelCommonHTTP.utils.REQUEST_UTILS;
const {
  isCurrentNifti,
  isCurrentMetaImage,
  isCurrentFile,
  getRouteParams,
  isCurrentWebImage,
  isCurrentZipFile,
  shouldIgnoreUserSettings,
} = FlywheelCommonUtils;
const {
  selectCurrentNiftis,
  selectCurrentMetaImage,
  selectCurrentWebImage,
  selectCurrentWebImages,
} = FlywheelCommonRedux.selectors;

const cachedFormResponse = {};

/**
 * Service to store measurementData
 * It acts as an interceptor and it should be used in conjunction to OHIF MeasurementAPI, which will provide measurementData
 * It can store data for nifti or dicom files
 * @param {Object} store app store. It comes from initialized process where store is exposed to current package
 * @param {Object} measurementData OHIF MeasurementAPI data representing current measurements. Default first param from OHIF MeasurementAPI storeFn method
 * @param {Object} filter OHIF MeasurementAPI data representing filters to be applied. Default second param from OHIF MeasurementAPI storeFn method
 * @param {Object} server OHIF MeasurementAPI data representing current server. Default third param from OHIF MeasurementAPI storeFn method
 * @param {Any} restParams OHIF MeasurementAPI data representing rest of params. Used to allow any customization from directly consumer passing through OHIF MeasurementAPI
 *
 * @return { Promise } Promise for service request
 */
const storeMeasurements = async (
  store,
  measurementData,
  filter,
  server,
  ...restParams
) => {
  const state = store.getState();
  const { taskId } = getRouteParams();
  let service = () => {};
  let storeAction;
  let currentDataInfo;
  // define which service, storeAction and currentDataInfo to use

  const featureConfig = state.flywheel.featureConfig;
  const user = selectUser(state);
  const isSiteAdmin = user.roles.includes('site_admin');

  if (featureConfig?.features?.reader_tasks) {
    let ownAnnotationPermission =
      (await hasPermission(state, 'annotations_own')) ||
      (await hasPermission(state, 'annotations_manage'));

    let hasEditAnnotationsOthersPermission = await hasPermission(
      state,
      'annotations_edit_others'
    );

    let hasEditFormOthersPermission = await hasPermission(
      state,
      'form_responses_edit_others'
    );

    let ownFormResponsesPermission =
      (await hasPermission(state, 'form_responses_own')) ||
      (await hasPermission(state, 'form_responses_manage'));
    if (isSiteAdmin) {
      ownAnnotationPermission = true;
      hasEditAnnotationsOthersPermission = true;
      ownFormResponsesPermission = true;
      hasEditFormOthersPermission = true;
    }

    store.dispatch(setSaveStatus(true));

    let allCreateReqData = [];
    let allModifiedReqData = [];
    measurementData['allTools'].forEach(measurement => {
      if (measurement.hasOwnProperty('id')) {
        const createdBy = measurement.flywheelOrigin.id;

        if (
          (createdBy !== user._id && !hasEditAnnotationsOthersPermission) ||
          (createdBy === user._id && !ownAnnotationPermission)
        ) {
          return;
        }

        if (!measurement.readonly && measurement.dirty) {
          delete measurement.dirty;
          delete measurement.handles?.textBox?.isVisible;
          const singleReqData = {
            data: measurement,
          };
          allModifiedReqData.push(singleReqData);
        }
      } else {
        if (
          !ownAnnotationPermission ||
          !measurement.dirty ||
          measurement.isCreationInProgress
        ) {
          return; // User does not have permission to create annotation or skip duplicate creation.
        }
        let fileId = '';
        if (isCurrentFile(state)) {
          const currentFile = selectCurrentFile(measurement.imagePath);
          fileId = currentFile.file_id;
        } else {
          const associations = selectAllAssociations(state).filter(
            association =>
              association.study_uid === measurement.StudyInstanceUID &&
              association.series_uid === measurement.SeriesInstanceUID &&
              association.acquisition_id !== 'None'
          );
          const currentFileAssociation = associations[0]; // Todo need api support to get file id corresponding to sop instance id.
          const acquisition =
            state.flywheel.acquisitions[currentFileAssociation.acquisition_id];

          if (acquisition) {
            const file = acquisition.files.find(
              file => file._id === currentFileAssociation.file_id
            );
            fileId = file?.file_id;
          }
        }
        delete measurement.dirty;
        delete measurement.handles?.textBox?.isVisible;
        const singleReqData = {
          file_id: fileId,
          task_id: taskId,
          data: measurement,
        };
        allCreateReqData.push(singleReqData);
      }
    });

    const deletedMeasurementIds = state.timepointManager.deletedMeasurementIds;
    const allDeleteReqData = deletedMeasurementIds.map(measurementId => {
      return { id: measurementId.id };
    });

    const promises = [];
    const resource = taskId ? 'Reader Task' : 'Project';
    if (allCreateReqData.length) {
      allCreateReqData.forEach(createReqData => {
        const createPromise = createAnnotations(createReqData);
        createPromise.catch(err => {
          createReqData.data.dirty = true;
          delete measurement.isCreationInProgress;
          handleCustomError(resource, err, 'create annotation');
        });
        createReqData.data.isCreationInProgress = true;
        promises.push(createPromise);
      });
    }

    if (allModifiedReqData.length) {
      allModifiedReqData.forEach(modifiedReqData => {
        const modifyPromise = modifyAnnotations(modifiedReqData);
        modifyPromise.catch(err => {
          modifiedReqData.data.dirty = true;
          handleCustomError(resource, err, 'modify annotation');
        });
        promises.push(modifyPromise);
      });
    }

    if (allDeleteReqData.length) {
      allDeleteReqData.forEach(deleteReqData => {
        const deletePromise = deleteAnnotations(deleteReqData.id);
        deletePromise.catch(err =>
          handleCustomError(resource, err, 'delete annotation')
        );
        promises.push(deletePromise);
      });
    }

    await Promise.all(promises);
    const readerTask =
      _.cloneDeep(state.flywheel.readerTaskWithFormResponse) ||
      _.cloneDeep(await selectReaderTask(state));

    if (
      readerTask &&
      ((readerTask.assignee !== user._id && !hasEditFormOthersPermission) ||
        (readerTask.assignee === user._id && !ownFormResponsesPermission))
    ) {
      return Promise.resolve();
    }

    if (isCurrentFile(state)) {
      // Store form response for file only when launched with task
      if (readerTask) {
        let responseNotes = {};

        const paramNotes =
          _.find(
            restParams,
            param => typeof param === 'object' && 'notes' in param
          ) || {};
        if (paramNotes.notes) {
          responseNotes = paramNotes.notes;
        }
        readerTask.info = {
          orientations: readerTask.info?.orientations || [],
          date: new Date().toISOString(),
          readTime: paramNotes.readTime,
          readStatus: paramNotes.readStatus,
          displayProperties: readerTask.info?.displayProperties || null,
        };
        if (restParams[0].taskDisplayProperties) {
          readerTask.info.displayProperties =
            restParams[0].taskDisplayProperties;
        }

        return saveOrUpdateFormResponse(
          responseNotes,
          readerTask.info,
          restParams[0].readStatus
        ).then(res => {
          readerTask.info.notes = responseNotes;
          store.dispatch(setSaveStatus(false));
          if (!shouldIgnoreUserSettings(state)) {
            delete readerTask.info.displayProperties;
          }
          store.dispatch(setReaderTaskWithFormResponse(readerTask));
          store.dispatch(
            setFormResponse({
              ...res,
            })
          );
        });
      } else {
        const routeParams = getRouteParams();
        let currentFiles = [
          {
            containerId: routeParams.fileContainerId,
            fileName: routeParams.filename,
          },
        ];

        service = (fileContainerId, filename, requestData) =>
          setFileInfo(fileContainerId, filename, requestData);

        if (state.multiSessionData?.isMultiSessionViewEnabled) {
          currentFiles = [];
          state.multiSessionData.acquisitionData.forEach(acquisition => {
            currentFiles.push({
              containerId: acquisition.containerId,
              fileName: acquisition.studyUid,
            });
          });
        }

        const currentDataInfos = [];
        const fileInfos = [];
        if (isCurrentNifti(state)) {
          const currentNiftis = selectCurrentNiftis(store.getState());
          currentNiftis.forEach(currentNifti => {
            fileInfos.push(currentNifti);
            currentDataInfos.push(currentNifti.info);
          });
        } else if (isCurrentMetaImage()) {
          const currentMetaImage = selectCurrentMetaImage(state);
          fileInfos.push(currentMetaImage);
          currentDataInfos.push(currentMetaImage.info);
        } else if (isCurrentWebImage()) {
          const currentWebImages = selectCurrentWebImages(state);
          if (!(routeParams.fileContainerId && routeParams.filename)) {
            currentFiles = [];
            measurementData.allTools.forEach(m => {
              const file = selectCurrentFile(m.imagePath);
              currentFiles.push({
                containerId: file.containerId,
                fileName: file.filename,
              });
            });
            if (currentFiles.length < 1) {
              currentFiles.push({
                containerId: currentWebImages[0]?.containerId,
                fileName: currentWebImages[0]?.filename,
              });
            }
          }
          const currentImage = selectCurrentWebImage(state);
          fileInfos.push(currentImage);
          currentImage?.info && currentDataInfos.push(currentImage.info);
        }

        if (currentDataInfos.length < 1) {
          return;
        }

        storeAction = (currentFile, currentDataInfo, ohifInfo) =>
          performStoreActionForFile(
            state,
            currentFile,
            currentDataInfo,
            ohifInfo
          );

        let requestDataCollection = [];
        currentDataInfos.forEach(currentDataInfo => {
          requestDataCollection.push({ ...currentDataInfo.ohifViewer });
        });

        // get object containing notes if existing
        const paramNotes =
          _.find(
            restParams,
            param => typeof param === 'object' && 'notes' in param
          ) || {};

        if (paramNotes.notes) {
          const user = selectUser(state);
          if (!user) {
            return Promise.reject();
          }
          requestDataCollection.forEach(requestData => {
            requestData.read = {
              ...requestData.read,
              [customInfoKey(user._id)]: {
                date: new Date().toISOString(),
                readTime: paramNotes.readTime,
                notes: paramNotes.notes,
                readStatus: paramNotes.readStatus,
              },
            };
          });
        }

        store.dispatch(setSaveStatus(false));
        // contact service then store on redux store

        const promises = [];
        requestDataCollection.forEach((requestData, index) => {
          const promise = service(
            currentFiles[index].containerId,
            currentFiles[index].fileName,
            requestData
          );
          promises.push(promise);
          promise.then(() => {
            if (storeAction) {
              storeAction(
                fileInfos[index],
                currentDataInfos[index],
                requestData
              );
            }
          });
        });
        return Promise.all(promises);
      }
    } else {
      if (readerTask) {
        let responseNotes = {};
        const paramNotes =
          _.find(
            restParams,
            param => typeof param === 'object' && 'notes' in param
          ) || {};
        if (paramNotes.notes) {
          responseNotes = paramNotes.notes;
        }
        readerTask.info = {
          orientations: readerTask.info?.orientations || [],
          date: new Date().toISOString(),
          readTime: paramNotes.readTime,
          readStatus: paramNotes.readStatus,
          displayProperties: readerTask.info?.displayProperties || null,
        };
        if (restParams[0].taskDisplayProperties) {
          readerTask.info.displayProperties =
            restParams[0].taskDisplayProperties;
        }

        return saveOrUpdateFormResponse(
          responseNotes,
          readerTask.info,
          restParams[0].readStatus
        ).then(res => {
          store.dispatch(setSaveStatus(false));
          readerTask.info.notes = responseNotes;
          if (!shouldIgnoreUserSettings(state)) {
            delete readerTask.info.displayProperties;
          }
          store.dispatch(setReaderTaskWithFormResponse(readerTask));
          store.dispatch(
            setFormResponse({
              ...res,
            })
          );
        });
      } else {
        const sessions = selectActiveSessions(state);
        service = requestData => {
          let count = 0;
          return new Promise((resolve, reject) => {
            sessions.forEach((session, index) => {
              let sessionDtls = cloneDeep(requestData[index]);
              let formSaveService = null;
              formSaveService = setSessionInfo(session._id, sessionDtls);
              formSaveService.then(
                () => {
                  count++;
                  if (count === sessions.length) {
                    resolve();
                  }
                },
                error => {
                  count++;
                  if (count === sessions.length) {
                    reject(error);
                  }
                }
              );
            });
          });
        };
        storeAction = sessionDatas => setActiveSessions(sessionDatas);
        const saveRequestDatas = [];
        sessions.forEach(session => {
          currentDataInfo = session.info;
          if (!currentDataInfo) {
            return;
          }
          const requestData = {
            ...currentDataInfo.ohifViewer,
          };

          const paramNotes =
            _.find(
              restParams,
              param => typeof param === 'object' && 'notes' in param
            ) || {};
          if (paramNotes.notes) {
            const user = selectUser(state);
            if (!user) {
              return Promise.reject();
            }
            requestData.read = {
              ...requestData.read,
              [customInfoKey(user._id)]: {
                date: new Date().toISOString(),
                readTime: paramNotes.readTime,
                notes: paramNotes.notes,
                readStatus: paramNotes.readStatus,
              },
            };
          }
          saveRequestDatas.push(requestData);
        });
        store.dispatch(setSaveStatus(false));
        return service(saveRequestDatas).then(() => {
          if (storeAction) {
            const updatedSessions = [];
            saveRequestDatas.forEach((requestData, index) => {
              updatedSessions.push({
                ...sessions[index],
                info: { ...currentDataInfo, ohifViewer: requestData },
              });
            });
            store.dispatch(storeAction(updatedSessions));
          }
        });
      }
    }
  } else if (isCurrentFile(state)) {
    currentDataInfo = { ohifViewer: {} };

    const routeParams = getRouteParams();
    let currentFiles = [
      {
        containerId: routeParams.fileContainerId,
        fileName: routeParams.filename,
      },
    ];

    service = (fileContainerId, filename, requestData) =>
      setFileInfo(fileContainerId, filename, requestData);

    if (state.multiSessionData?.isMultiSessionViewEnabled) {
      currentFiles = [];
      state.multiSessionData.acquisitionData.forEach(acquisition => {
        currentFiles.push({
          containerId: acquisition.containerId,
          fileName: acquisition.studyUid,
        });
      });
    }
    const currentDataInfos = [];
    const fileInfos = [];
    if (isCurrentNifti(state)) {
      const currentNiftis = selectCurrentNiftis(store.getState());
      currentNiftis.forEach(currentNifti => {
        fileInfos.push(currentNifti);
        currentDataInfos.push(currentNifti.info);
      });
    } else if (isCurrentMetaImage()) {
      const currentMetaImage = selectCurrentMetaImage(state);
      fileInfos.push(currentMetaImage);
      currentDataInfos.push(currentMetaImage.info);
    } else if (isCurrentWebImage()) {
      const currentWebImage = selectCurrentWebImage(state);
      fileInfos.push(currentWebImage);
      currentDataInfos.push(currentWebImage.info);
    }

    if (currentDataInfos.length < 0) {
      return;
    }

    storeAction = (currentFile, currentDataInfo, ohifInfo) =>
      performStoreActionForFile(state, currentFile, currentDataInfo, ohifInfo);

    let requestDataCollection = [];
    currentDataInfos.forEach((currentDataInfo, index) => {
      const requestData = {
        ...currentDataInfo.ohifViewer,
        measurements: adaptToApi(measurementData),
      };

      const fileMeasurements = {};
      Object.keys(requestData.measurements).forEach(key => {
        requestData.measurements[key].forEach(measurement => {
          const studyInstanceUid =
            measurement.StudyInstanceUID || measurement.studyInstanceUid;
          const { fileName, containerId } = currentFiles[index];
          const measurementStudyInstanceUID = isCurrentZipFile(fileName)
            ? containerId
            : fileName;
          if (studyInstanceUid === measurementStudyInstanceUID) {
            if (!fileMeasurements[key]) fileMeasurements[key] = [];
            fileMeasurements[key].push(measurement);
          }
        });
      });
      requestData.measurements = fileMeasurements;
      requestDataCollection.push(requestData);
    });
    // get object containing notes if existing
    const paramNotes =
      _.find(
        restParams,
        param => typeof param === 'object' && 'notes' in param
      ) || {};

    if (paramNotes.notes) {
      const user = selectUser(state);
      if (!user) {
        return Promise.reject();
      }

      requestDataCollection.forEach(requestData => {
        requestData.read = {
          ...requestData.read,
          [customInfoKey(user._id)]: {
            date: new Date().toISOString(),
            readTime: paramNotes.readTime,
            notes: paramNotes.notes,
            readStatus: paramNotes.readStatus,
          },
        };
      });
    }
    store.dispatch(setSaveStatus(false));
    // contact service then store on redux store

    const promises = [];
    requestDataCollection.forEach((requestData, index) => {
      const promise = service(
        currentFiles[index].containerId,
        currentFiles[index].fileName,
        requestData
      );
      promises.push(promise);
      promise.then(() => {
        if (storeAction) {
          storeAction(fileInfos[index], currentDataInfos[index], requestData);
        }
      });
    });
    return Promise.all(promises);
  } else {
    const sessions = selectActiveSessions(state);
    const studySessions = selectStudySessions(state);
    service = requestData => {
      let count = 0;
      return new Promise((resolve, reject) => {
        sessions.forEach((session, index) => {
          let sessionDtls = cloneDeep(requestData[index]);
          setSessionInfo(session._id, sessionDtls).then(
            () => {
              count++;
              if (count === sessions.length) {
                resolve();
              }
            },
            error => {
              count++;
              if (count === sessions.length) {
                reject(error);
              }
            }
          );
        });
      });
    };
    storeAction = sessionDatas => setActiveSessions(sessionDatas);
    const saveRequestDatas = [];
    const currentDataInfos = [];
    sessions.forEach(session => {
      currentDataInfo = session.info;
      if (!currentDataInfo) {
        return;
      }
      const requestData = {
        ...currentDataInfo.ohifViewer,
        measurements: adaptToApi(measurementData),
      };
      const sessionMeasurements = {};
      Object.keys(requestData.measurements).forEach(key => {
        requestData.measurements[key].forEach(measurement => {
          const studyInstanceUid =
            measurement.StudyInstanceUID || measurement.studyInstanceUid;
          const studySession = studySessions[studyInstanceUid];
          if (studySession._id === session._id) {
            if (!sessionMeasurements[key]) sessionMeasurements[key] = [];
            sessionMeasurements[key].push(measurement);
          }
          delete measurement.dirty;
          delete measurement.handles?.textBox?.isVisible;
        });
      });
      requestData.measurements = sessionMeasurements;
      const paramNotes =
        _.find(
          restParams,
          param => typeof param === 'object' && 'notes' in param
        ) || {};
      if (paramNotes.notes) {
        const user = selectUser(state);
        if (!user) {
          return Promise.reject();
        }
        requestData.read = {
          ...requestData.read,
          [customInfoKey(user._id)]: {
            date: new Date().toISOString(),
            readTime: paramNotes.readTime,
            notes: paramNotes.notes,
            readStatus: paramNotes.readStatus,
          },
        };
      }
      saveRequestDatas.push(requestData);
      currentDataInfos.push(currentDataInfo);
    });

    store.dispatch(setSaveStatus(false));
    return service(saveRequestDatas).then(() => {
      if (storeAction) {
        const updatedSessions = [];
        saveRequestDatas.forEach((requestData, index) => {
          updatedSessions.push({
            ...sessions[index],
            info: { ...currentDataInfo, ohifViewer: requestData },
          });
        });
        store.dispatch(storeAction(updatedSessions));
      }
    });
  }
};

const performStoreActionForFile = (state, file, dataInfo, ohifInfo) => {
  if (isCurrentNifti(state)) {
    store.dispatch(
      setCurrentNifti(
        file.containerId,
        file.filename,
        file.file_id,
        {
          ...dataInfo,
          ohifViewer: ohifInfo,
        },
        file.parents
      )
    );
  } else if (isCurrentMetaImage(state)) {
    store.dispatch(
      setCurrentMetaImage(
        file.containerId,
        file.filename,
        file.file_id,
        {
          ...dataInfo,
          ohifViewer: ohifInfo,
        },
        file.parents
      )
    );
  } else if (isCurrentWebImage()) {
    store.dispatch(
      setCurrentWebImage(
        file.containerId,
        file.filename,
        file.file_id,
        {
          ...dataInfo,
          ohifViewer: ohifInfo,
        },
        file.parents
      )
    );
  }
};

const createAnnotations = data => {
  return createAnnotation(data).then(response => {
    if (response && response.data) {
      let measurement = response.data;
      const measurementApi = OHIF.measurements.MeasurementApi.Instance;
      const allMeasurements = store.getState().timepointManager.measurements;
      const annotation = allMeasurements[measurement.toolType];
      measurement = annotation.find(
        x => x._id === measurement._id && x.uuid === measurement.uuid
      );
      if (measurement) {
        measurement.id = response._id;
        delete measurement.isCreationInProgress;
        measurementApi.updateMeasurement(measurement.toolType, measurement);
      } else {
        store.dispatch(
          addDeletedMeasurementId(response._id, response.data._id)
        );
      }
    }
  });
};

const modifyAnnotations = reqData => {
  return modifyAnnotation(reqData.data.id, reqData);
};

const deleteAnnotations = id => {
  return deleteAnnotation(id).then(response => {
    if (response.deleted) {
      store.dispatch(removeDeletedMeasurementId(id));
    } else {
      // Annotation not found error
    }
  });
};

const saveOrUpdateFormResponse = async (formResponse, info, readStatus) => {
  const readerTask = await selectReaderTask(store.getState());
  if (readerTask._id) {
    let response = store.getState().flywheel.formResponse; // Existing saved response
    let formResponsePromise = null;
    const filteredForm = filterFormResponse(formResponse);
    if (response) {
      // Modify
      formResponsePromise = updateViewerFormResponse(response._id, {
        response_data: {
          ...filteredForm,
        },
      });
    } else if (formResponse && Object.keys(formResponse).length) {
      // Create
      if (!cachedFormResponse.hasOwnProperty(readerTask._id)) {
        let requestBody = {
          form_id: readerTask.form_id,
          parent: {
            id: readerTask.parent.id,
            type: readerTask.parent.type,
          },
          task_id: readerTask._id,
        };
        requestBody.response_data = {
          ...filteredForm,
        };
        formResponsePromise = setViewerFormResponse(requestBody);
        cachedFormResponse[readerTask._id] = formResponsePromise;
        formResponsePromise.catch(() => {
          delete cachedFormResponse[readerTask._id];
        });
      }
    } else {
      if (info?.displayProperties) {
        const promise = updateReaderTask(
          readerTask._id,
          readStatus || READER_TASK_STATUS.TODO,
          info
        );
        promise.then(res => {
          if (
            res?.info?.displayProperties &&
            !shouldIgnoreUserSettings(store.getState())
          ) {
            delete res.info.displayProperties;
            store.dispatch(setReaderTask(Promise.resolve(res)));
          } else {
            store.dispatch(setReaderTask(promise));
          }
        });
        return promise;
      }
      return Promise.resolve();
    }
    formResponsePromise.then(res => {
      const promise = updateReaderTask(
        readerTask._id,
        readStatus || READER_TASK_STATUS.TODO,
        info
      );
      promise.then(res => {
        if (
          res.info?.displayProperties &&
          !shouldIgnoreUserSettings(store.getState())
        ) {
          delete res.info.displayProperties;
          store.dispatch(setReaderTask(Promise.resolve(res)));
        } else {
          store.dispatch(setReaderTask(promise));
        }
      });
    });
    return formResponsePromise;
  }
};

const handleCustomError = (resource, err, operation) => {
  if (err.message === 'Forbidden') {
    err = {
      ...err,
      message: operation
        ? `You do not have permission to ${operation} in this ${resource}`
        : `Forbidden: You do not have access to this ${resource}`,
    };
  } else {
    err = err.message
      ? err
      : {
          ...err,
          message: operation
            ? `You do not have permission to ${operation} in this ${resource}`
            : `You do not have permission to view this ${resource}`,
        };
  }
  log.error(err);
};

function selectCurrentFile(imagePath) {
  let file = null;
  let state = store.getState();
  if (isCurrentNifti(state)) {
    const currentNiftis = selectCurrentNiftis(state);
    file = findImage(currentNiftis, imagePath);
  } else if (isCurrentMetaImage(state)) {
    file = selectCurrentMetaImage(state);
  } else if (isCurrentWebImage()) {
    const files = selectCurrentWebImages(state);
    file = findImage(files, imagePath);
  }
  return file;
}

function findImage(files, imagePath) {
  return files.find(image => {
    const imageDetails = imagePath.split('$$$');
    const imageNameFromPath = isCurrentZipFile(image.filename)
      ? imageDetails[1]
      : imageDetails[0];
    return (
      imagePath.includes(image.containerId) &&
      imageNameFromPath === image.filename
    );
  });
}

const READER_TASK_STATUS = {
  TODO: 'Todo',
  INPROGRESS: 'In_progress',
  COMPLETE: 'Complete',
};

export default storeMeasurements;
