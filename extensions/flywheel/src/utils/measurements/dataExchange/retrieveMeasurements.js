import Redux from '../../../redux';
import OHIF, { log, utils } from '@ohif/core';
import cloneDeep from 'lodash.clonedeep';
import {
  HTTP as FlywheelCommonHTTP,
  Utils as FlywheelCommonUtils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import cornerstone from 'cornerstone-core';
import { adaptToApp } from './dataAdapters';
import _ from 'lodash';
import store from '@ohif/viewer/src/store';
import merge from 'lodash.merge';

const {
  setActiveSession,
  setActiveSessions,
  setCurrentNiftis,
  setCurrentMetaImage,
  setAcquisitions,
  setSessions,
  setFormResponse,
  setCurrentWebImages,
  setReaderTaskWithFormResponse,
} = Redux.actions;

const {
  setPreviousSessionReadTime,
  setMultipleTaskNotesUserAndFormResponse,
} = FlywheelCommonRedux.actions;

const {
  selectAssociations,
  selectFileAssociations,
  selectAllAssociations,
  selectReaderTask,
  selectUser,
  hasPermission,
} = Redux.selectors;

const { setOrientation } = OHIF.redux.actions;

const {
  getFileMetadata,
  getSession,
  getMeasurementsByTaskId,
  getMeasurementsByFileId,
  getAcquisition,
  getViewerFormResponse,
} = FlywheelCommonHTTP.services;
const { customInfoKey } = FlywheelCommonHTTP.utils.REQUEST_UTILS;
const {
  isCurrentNifti,
  isCurrentMetaImage,
  isCurrentFile,
  getRouteParams,
  isCurrentWebImage,
  showErrorPage,
} = FlywheelCommonUtils;

const multipleTaskDetails = {};

/**
 * It returns a nested function to be used as filterMethod. FilterObj is passed to inner function closure to be used later.
 * @param {Object} filterObj Object to compare against
 * @return {Function} Function which defines filterObj on its scope and then its used to filter data
 */
const _filterBySeries = filterObj => (dataToCompare = []) => {
  return _.partition(dataToCompare, dataObj => _.isMatch(dataObj, filterObj));
};
/**
 * Service to get measurements
 * It acts as an interceptor and to get and return measurements.
 * It should be used in conjunction to OHIF MeasurementAPI
 * It can retrieve data for nifti or dicom files
 * @return {Object} measurements for given study
 */
const retrieveMeasurements = async (
  store,
  appConfig,
  server,
  ...restParams
) => {
  // getting data from url but its expected this method to be called by OHIF MeasurementAPI and
  // after study is loaded and viewer session is ready.
  const location = window.location;
  const { history } = appConfig;
  const routeParams = FlywheelCommonUtils.getRouteParams(location);
  const state = store.getState();
  let service = () => {};
  let filterResponse;
  let responseAdapter = (response = {}, filterResponse) => {
    const measurements =
      (response.info &&
        response.info.ohifViewer &&
        response.info.ohifViewer.measurements) ||
      null;

    return adaptToApp(measurements, filterResponse);
  };
  let storeAction;
  let storeOrientation = null;
  let measurements = {};
  let measurementKey = '';
  let measurementsFromMultipleTask = {};
  // define which service, storeAction to use

  if (isCurrentFile(state)) {
    let currentFiles = [
      {
        containerId: routeParams.fileContainerId,
        fileName: routeParams.filename,
      },
    ];
    const responseCollection = [];

    const { taskId } = routeParams;
    service = (fileContainerId, filename) =>
      getFileMetadata(fileContainerId, filename);

    if (state.multiSessionData?.isMultiSessionViewEnabled) {
      currentFiles = [];
      state.multiSessionData.acquisitionData.forEach(acquisition => {
        currentFiles.push({
          containerId: acquisition.containerId,
          fileName: acquisition.studyUid,
        });
      });
    }

    if (isCurrentNifti(state)) {
      storeAction = fileDataCollection => {
        setTimer(store, fileDataCollection[0]);
        return setCurrentNiftis(fileDataCollection);
      };
    } else if (isCurrentMetaImage(state)) {
      storeAction = responseData =>
        setCurrentMetaImage(
          responseData[0].containerId,
          responseData[0].filename,
          responseData[0].file_id,
          responseData[0].info,
          responseData[0].parents
        );
    } else if (isCurrentWebImage()) {
      // TODO: containerId and fileName for webimages are temporarily stored in studies in store
      // Need to retrieve these data from some other place.
      const studyData = store.getState().studies?.studyData || {};
      if (!(routeParams.fileContainerId && routeParams.filename)) {
        currentFiles = [];
        for (const key in studyData) {
          const data = studyData[key];
          const { ZIP_IMAGES } = utils.regularExpressions;
          if (ZIP_IMAGES.test(data.fileName) && data.series.length > 1) {
            data.series.forEach(series => {
              currentFiles.push({
                containerId: data.containerId,
                fileName: series.SeriesInstanceUID,
              });
            });
          } else {
            currentFiles.push({
              containerId: data.containerId,
              fileName: data.fileName,
            });
          }
        }
      }
      storeAction = responseData => {
        setTimer(store, responseData[0]);
        return setCurrentWebImages(responseData);
      };
    }

    for (let i = 0; i < currentFiles.length; i++) {
      const fileData = currentFiles[i];
      const fileContainerId = fileData.containerId;
      const fileName = fileData.fileName;
      let response = await service(fileContainerId, fileName);
      const featureConfig = state.flywheel.featureConfig;
      if (featureConfig?.features?.reader_tasks) {
        let measurementsFromTaskOrFile = null;
        let measurementKey = '';
        let readerTask = await selectReaderTask(window.store.getState());

        if (readerTask) {
          if (state.multipleReaderTask.TaskIds.length > 1) {
            await getMeasurementsFromMultipleTasks(taskId);
            measurementsFromTaskOrFile = measurementsFromMultipleTask;
          } else {
            measurementKey = taskId;
            measurementsFromTaskOrFile = await getMeasurementForReaderTaskOrFile(
              getMeasurementsByTaskId.bind(this, taskId)
            ).catch(err => handleCustomError('measurements', err));
          }
        } else {
          measurementKey = response.file_id;
          measurementsFromTaskOrFile = await getMeasurementForReaderTaskOrFile(
            getMeasurementsByFileId.bind(this, response.file_id)
          ).catch(err => handleCustomError('measurements', err));
          // Skip any task specific saved annotations in task independent scenarios
          Object.keys(measurementsFromTaskOrFile).forEach(key => {
            measurementsFromTaskOrFile[key] = measurementsFromTaskOrFile[
              key
            ].filter(m => !m.task_id);
          });
        }

        if (measurementsFromTaskOrFile) {
          const taskResponse = {
            info: { ohifViewer: { measurements: measurementsFromTaskOrFile } },
          };
          response.info = taskResponse.info;
          formatMeasurements(taskResponse);
          const allTools = responseAdapter(taskResponse, undefined);
          setUrlBasedLabelsVisibility(routeParams, allTools);
          measurements[measurementKey] = allTools;
        }
        if (readerTask) {
          const readerTaskWithFormResponse = await getFormResponse(
            _.cloneDeep(readerTask)
          ).catch(err => handleCustomError('form response', err));
          store.dispatch(
            setReaderTaskWithFormResponse({ ...readerTaskWithFormResponse })
          );

          if (state.multipleReaderTask.TaskIds.length > 1) {
            store.dispatch(
              setMultipleTaskNotesUserAndFormResponse(multipleTaskDetails)
            );
          }
        }
      } else {
        formatMeasurements(response);
        const allTools = responseAdapter(response, filterResponse);
        setUrlBasedLabelsVisibility(routeParams, allTools);
        measurements[i] = allTools;
      }
      responseCollection.push(response);
    }

    if (responseCollection.length === currentFiles.length && storeAction) {
      const responseData = [];
      responseCollection.forEach((response, index) => {
        const data = {
          containerId: currentFiles[index].containerId,
          filename: currentFiles[index].fileName,
          file_id: response.file_id,
          parents: response.parents,
          info: response.info,
        };
        responseData.push(data);
      });
      setTimer(store, responseData[0]);
      store.dispatch(storeAction(responseData));
    }
  } else {
    const { StudyInstanceUID, SeriesInstanceUID } = routeParams;
    const studyInstaUIDs = [StudyInstanceUID];
    const responseCollection = [];
    const responseDataCollection = [];
    if (
      !!state.multiSessionData &&
      state.multiSessionData.isMultiSessionViewEnabled
    ) {
      state.multiSessionData.acquisitionData.forEach(acquisition => {
        if (!studyInstaUIDs.includes(acquisition.studyUid)) {
          studyInstaUIDs.push(acquisition.studyUid);
        }
      });
      storeAction = responses => setActiveSessions(responses);
    } else {
      storeAction = responses => setActiveSession(responses[0]);
    }
    // to store orientations
    storeOrientation = responses => {
      let orientationFromResponses = [];
      responses.forEach(response => {
        if (
          response &&
          response.info &&
          response.info.ohifViewer &&
          response.info.ohifViewer.orientations
        ) {
          orientationFromResponses = orientationFromResponses.concat(
            response.info.ohifViewer.orientations
          );
        }
      });
      if (orientationFromResponses.length > 0) {
        store.dispatch(setOrientation(orientationFromResponses, true));

        if (orientationFromResponses && !orientationFromResponses.length) {
          return;
        }

        let viewportSpecificData = state.viewports.viewportSpecificData;

        const getOrientation = viewPortData => {
          return orientationFromResponses.find(item => {
            if (
              viewPortData.StudyInstanceUID == item.StudyInstanceUID &&
              viewPortData.SeriesInstanceUID == item.SeriesInstanceUID
            ) {
              return item;
            }
          });
        };

        let enabledElements = cornerstone.getEnabledElements();
        Object.keys(viewportSpecificData).forEach((key, index) => {
          let viewPortData = viewportSpecificData[key];
          let viewPortOrientationData = getOrientation(viewPortData);
          if (viewPortOrientationData) {
            if (enabledElements[index] && enabledElements[index].element) {
              let enabledElement = enabledElements[index].element;
              if (enabledElement) {
                let viewport = cornerstone.getViewport(enabledElement);
                if (viewport) {
                  viewport.rotation = viewPortOrientationData.rotation;
                  cornerstone.setViewport(enabledElement, viewport);
                }
              }
            }
          }
        });
      }
    };
    for (let i = 0; i < studyInstaUIDs.length; i++) {
      const associations = selectAssociations(state);
      const assoc = associations[studyInstaUIDs[i]] || {};
      const sessionId = assoc.session_id;

      if (sessionId) {
        service = () => getSession(sessionId);
      }
      let sessionResponsePromise = service();

      const { taskId } = getRouteParams();
      const featureConfig = state.flywheel.featureConfig;
      let measurementsFromTaskOrFile = null;

      if (featureConfig?.features?.reader_tasks) {
        const studyAssociations = selectAllAssociations(state).filter(
          association => association.study_uid === studyInstaUIDs[i]
        );
        let uniqueFileIds = [];
        let uniqueAcquisitionIds = [];
        const acquisitionPromises = [];
        const filePromises = [];
        studyAssociations.forEach(association => {
          const acquisitionId = association.acquisition_id;
          if (uniqueAcquisitionIds.includes(acquisitionId)) {
            // Skip duplicate api calls for same acquisition for task mode, for non-task mode
            // skip if the file in the acquisition is duplicate.
            if (
              taskId ||
              (!taskId && uniqueFileIds.includes(association.file_id))
            ) {
              return;
            }
          }
          uniqueAcquisitionIds.push(acquisitionId);
          if (acquisitionId !== 'None') {
            const acquisitionPromise = getAcquisition(acquisitionId);
            acquisitionPromise.then(acquisition => {
              let acquisitions = state.flywheel.acquisitions || {};
              acquisitions[acquisitionId] = acquisition;
              store.dispatch(setAcquisitions(acquisitions));
              if (!taskId) {
                // Get annotation based on files if launched without task.
                const promises = acquisitions[acquisitionId].files.map(file => {
                  if (association.file_id !== file._id) {
                    // Skip downloading file info of other studies in same acquisition.
                    return;
                  }
                  if (uniqueFileIds.includes(file.file_id)) {
                    // Skip duplicate api calls for same file.
                    return;
                  }
                  uniqueFileIds.push(file.file_id);
                  measurementKey = file.file_id; // Todo return annotations for specific file id as key in measurements without overwriting
                  return getMeasurementForReaderTaskOrFile(
                    getMeasurementsByFileId.bind(this, measurementKey)
                  )
                    .then(measurementsForFile => {
                      if (measurementsFromTaskOrFile) {
                        Object.keys(measurementsForFile).forEach(toolType => {
                          if (measurementsFromTaskOrFile[toolType]) {
                            measurementsFromTaskOrFile[toolType] = [
                              ...measurementsFromTaskOrFile[toolType],
                              ...measurementsForFile[toolType],
                            ];
                          } else {
                            measurementsFromTaskOrFile = {
                              ...measurementsFromTaskOrFile,
                              [toolType]: measurementsForFile[toolType],
                            };
                          }
                        });
                      } else {
                        measurementsFromTaskOrFile = { ...measurementsForFile };
                      }
                      // Skip any task specific saved annotations in task independent scenarios
                      Object.keys(measurementsFromTaskOrFile).forEach(key => {
                        measurementsFromTaskOrFile[
                          key
                        ] = measurementsFromTaskOrFile[key].filter(
                          m => !m.task_id
                        );
                      });
                      setUrlBasedLabelsVisibility(
                        routeParams,
                        measurementsFromTaskOrFile
                      );
                    })
                    .catch(err => handleCustomError('measurements', err));
                });
                [].push.apply(filePromises, promises);
              }
            });
            acquisitionPromises.push(acquisitionPromise);
          }
        });
        await Promise.all(acquisitionPromises);
        await Promise.all(filePromises);

        if (taskId) {
          if (state.multipleReaderTask.TaskIds.length > 1) {
            await getMeasurementsFromMultipleTasks(taskId);
            measurementsFromTaskOrFile = measurementsFromMultipleTask;
          } else {
            measurementKey = taskId;
            measurementsFromTaskOrFile = await getMeasurementForReaderTaskOrFile(
              getMeasurementsByTaskId.bind(this, taskId)
            ).catch(err => handleCustomError('measurements', err));
          }
        }
      }

      if (sessionId) {
        if (
          !state.multiSessionData ||
          !state.multiSessionData.isMultiSessionViewEnabled
        ) {
          // get object containing singleFileMode if existing from params
          const singleFileModeParam =
            _.find(
              restParams,
              param => typeof param === 'object' && 'singleFileMode' in param
            ) || {};

          if (singleFileModeParam.singleFileMode && SeriesInstanceUID) {
            const filterObj = {
              StudyInstanceUID,
              SeriesInstanceUID,
            };
            filterResponse = _filterBySeries(filterObj);
          }
        }
      }
      // contact service, process response and store on redux store
      let responseData = await sessionResponsePromise;
      formatMeasurements(responseData);
      let response = responseData ? cloneDeep(responseData) : null;

      let readerTask = await selectReaderTask(window.store.getState());
      if (readerTask) {
        const readerTaskWithFormResponse = await getFormResponse(
          _.cloneDeep(readerTask)
        ).catch(err => handleCustomError('form response', err));
        store.dispatch(
          setReaderTaskWithFormResponse({ ...readerTaskWithFormResponse })
        );

        if (state.multipleReaderTask.TaskIds.length > 1) {
          store.dispatch(
            setMultipleTaskNotesUserAndFormResponse(multipleTaskDetails)
          );
        }

        if (storeOrientation && readerTask.info?.orientations) {
          const readerOrientations = {
            info: {
              ohifViewer: { orientations: readerTask.info?.orientations },
            },
          };
          storeOrientation([readerOrientations]);
        }
      }

      if (measurementsFromTaskOrFile) {
        const taskResponse = {
          info: { ohifViewer: { measurements: measurementsFromTaskOrFile } },
        };
        formatMeasurements(taskResponse);
        const allTools = responseAdapter(taskResponse, undefined);
        setUrlBasedLabelsVisibility(routeParams, allTools);
        measurements[measurementKey] = allTools;
      } else {
        const allTools = responseAdapter(response, filterResponse);
        setUrlBasedLabelsVisibility(routeParams, allTools);
        measurements[sessionId] = allTools;
      }
      if (response && storeAction) {
        responseCollection.push(response);
        responseDataCollection.push(responseData);
        if (responseCollection.length === studyInstaUIDs.length) {
          store.dispatch(storeAction(responseCollection));
          store.dispatch(setSessions(responseDataCollection));
          if (storeOrientation && !readerTask) {
            storeOrientation(responseCollection);
          }
        }
        setTimer(store, responseCollection[0]);
      }
    }
  }

  async function getMeasurementsFromTaskOrFile(allMeasurements) {
    const measurementsFromTaskOrFile = await getMeasurementForReaderTaskOrFile(
      () => Promise.resolve({ results: allMeasurements })
    ).catch(err => handleCustomError('measurements', err));
    return measurementsFromTaskOrFile;
  }

  async function getMeasurementsFromMultipleTasks(taskId) {
    measurementKey = taskId;
    const allMeasurements = [];
    const promises = [];
    const measurementsPromise = [];
    const taskIds = state.multipleReaderTask.TaskIds;
    await taskIds.forEach(task => {
      promises.push(
        getMeasurementsByTaskId
          .bind(this, task)()
          .then(result => {
            allMeasurements.push(...result.results);
          })
      );
    });
    await Promise.all(promises).then(() => {
      measurementsFromMultipleTask = getMeasurementsFromTaskOrFile(
        allMeasurements
      );
      measurementsPromise.push(
        measurementsFromMultipleTask.then(result => {
          measurementsFromMultipleTask = result;
        })
      );
    });
    await Promise.all(measurementsPromise).then(() => {
      return measurementsFromMultipleTask;
    });
  }

  function handleCustomError(resource, err) {
    if (err.message === 'Forbidden') {
      err = {
        ...err,
        message: `Forbidden: You do not have access to this ${resource}`,
      };
    } else if (
      err.message === 'Unprocessable Entity' ||
      err.message === 'Not Found'
    ) {
      err = {
        ...err,
        message: `We could not provide details of the requested ${resource}, Please confirm the URL is valid`,
      };
    } else {
      err = {
        ...err,
        message: `You do not have permission to view this ${resource}`,
      };
    }
    handleError(err);
  }

  function handleError(error) {
    showErrorPage(history, error.message);
    log.error(error);
  }

  return measurements;
};

async function getMeasurementForReaderTaskOrFile(getMeasurement) {
  const state = store.getState();

  const user = selectUser(state);
  const isSiteAdmin = user.roles.includes('site_admin');
  let ownAnnotationPermission =
    (await hasPermission(state, 'annotations_own')) ||
    (await hasPermission(state, 'annotations_manage'));
  let hasViewOthersPermission = await hasPermission(
    state,
    'annotations_view_others'
  );
  let hasEditOthersPermission = await hasPermission(
    state,
    'annotations_edit_others'
  );
  if (isSiteAdmin) {
    ownAnnotationPermission = true;
    hasViewOthersPermission = true;
    hasEditOthersPermission = true;
  }

  const readerTask = await selectReaderTask(state);
  // If user don't have permission to view annotation's, then skip requesting it to server
  const res =
    (readerTask &&
      readerTask.assignee === user._id &&
      ownAnnotationPermission) ||
    (!readerTask && ownAnnotationPermission) ||
    hasViewOthersPermission
      ? await getMeasurement()
      : null;
  let measurementsFromTaskOrFile = {};
  let userBasedMeasurements = {};
  res?.results.forEach(taskMeasurement => {
    if (taskMeasurement.origin.id !== user._id) {
      if (!hasEditOthersPermission && !hasViewOthersPermission) {
        return; // Do not add annotation if user don't have permission on others annotation.
      }
    } else {
      if (!ownAnnotationPermission) {
        return; // User does not have permission to view own annotation.
      }
    }

    if (!userBasedMeasurements[taskMeasurement.origin.id]) {
      userBasedMeasurements[taskMeasurement.origin.id] = {
        minMeasurementNo: Infinity,
        maxMeasurementNo: -Infinity,
        minLesionNo: Infinity,
        maxLesionNo: -Infinity,
        annotations: [],
      };
    }
    const prevUserData = userBasedMeasurements[taskMeasurement.origin.id];
    userBasedMeasurements[taskMeasurement.origin.id] = {
      minMeasurementNo: Math.min(
        prevUserData.minMeasurementNo,
        taskMeasurement.data.measurementNumber
      ),
      maxMeasurementNo: Math.max(
        prevUserData.maxMeasurementNo,
        taskMeasurement.data.measurementNumber
      ),
      minLesionNo: Math.min(
        prevUserData.minLesionNo,
        taskMeasurement.data.lesionNamingNumber
      ),
      maxLesionNo: Math.max(
        prevUserData.maxLesionNo,
        taskMeasurement.data.lesionNamingNumber
      ),
      annotations: [...prevUserData.annotations, taskMeasurement],
    };
  });
  // make unique measurement and lesion numbers when multiple user's annotation's are loaded for admin.
  let prevMaxMeasurementNo = 0;
  let prevMaxLesionNo = 0;
  Object.keys(userBasedMeasurements).forEach(userId => {
    const measurementOffset =
      prevMaxMeasurementNo - userBasedMeasurements[userId].minMeasurementNo + 1;
    const lesionOffset =
      prevMaxLesionNo - userBasedMeasurements[userId].minLesionNo + 1;

    userBasedMeasurements[userId].annotations.forEach(taskMeasurement => {
      const toolType = taskMeasurement.data.toolType;
      if (!measurementsFromTaskOrFile[toolType]) {
        measurementsFromTaskOrFile[toolType] = [];
      }
      let measurementNumber =
        taskMeasurement.data.measurementNumber + measurementOffset;
      let lesionNamingNumber =
        taskMeasurement.data.lesionNamingNumber + lesionOffset;
      prevMaxMeasurementNo = Math.max(prevMaxMeasurementNo, measurementNumber);
      prevMaxLesionNo = Math.max(prevMaxLesionNo, lesionNamingNumber);
      let readonly = false;
      if (taskMeasurement.origin.id !== user._id) {
        if (!hasEditOthersPermission && hasViewOthersPermission) {
          readonly = true; // User only has view permission on other user's annotations.
        }
      }
      const points = getFormatedMeasurement(taskMeasurement.data);
      let handles = { ...taskMeasurement.data.handles };
      if (points.length) {
        handles = { ...taskMeasurement.data.handles, points: points };
      }
      measurementsFromTaskOrFile[toolType].push({
        ...taskMeasurement.data,
        handles: { ...handles },
        measurementNumber,
        lesionNamingNumber,
        readonly,
        id: taskMeasurement._id,
        file_id: taskMeasurement.file_ref.file_id,
        task_id: taskMeasurement.task_id,
      });
    });
  });

  return measurementsFromTaskOrFile;
}

/**
 * For backward compatibility to support already saved older version by converting to the upgraded
 * format(Ex: Currently the points with Array format of Open Freehand ROIs to Array of Array format)
 * This can be extended to support other measurements version upgrade
 * @param {Object} data
 */
function getFormatedMeasurement(data) {
  const measurements = [];
  if (data.toolType === 'OpenFreehandRoi') {
    if (
      data?.handles?.points?.length &&
      !Array.isArray(data.handles.points[0])
    ) {
      // Converting to Array of Array format by pushing it to the first index
      const points = [...data.handles.points];
      measurements.push(points);
    }
  }
  return measurements;
}

async function getFormResponse(readerTask) {
  const state = store.getState();
  const user = selectUser(state);
  const isSiteAdmin = user.roles.includes('site_admin');
  let ownFormResponsePermission =
    (await hasPermission(state, 'form_responses_own')) ||
    (await hasPermission(state, 'form_responses_manage'));
  let hasViewOthersPermission = await hasPermission(
    state,
    'form_responses_view_others'
  );
  let hasEditOthersPermission = await hasPermission(
    state,
    'form_responses_edit_others'
  );
  if (isSiteAdmin) {
    ownFormResponsePermission = true;
    hasViewOthersPermission = true;
    hasEditOthersPermission = true;
  }
  if (readerTask.assignee === user._id) {
    if (!ownFormResponsePermission) {
      readerTask.info = {}; // User does not have permission to view own form response.
      return readerTask;
    }
  } else if (!hasEditOthersPermission && !hasViewOthersPermission) {
    readerTask.info = {};
    return readerTask; // User does not have permission to view and edit others form response.
  }

  if (state.multipleReaderTask.TaskIds.length > 1) {
    let multipleTaskFormResponse;
    let mergedMultipleTaskFormResponse;
    const promises = [];
    let multipleTaskData = {};
    await state.multipleReaderTask.multipleTaskResponse.forEach(
      taskResponse => {
        const response = getMultiTaskFormResponse(taskResponse);
        promises.push(
          response.then(result => {
            if (result.results.length) {
              multipleTaskFormResponse =
                result.results[result.results.length - 1];
              if (!mergedMultipleTaskFormResponse) {
                mergedMultipleTaskFormResponse = {
                  ...result.results[result.results.length - 1],
                };
              } else {
                mergedMultipleTaskFormResponse = merge(
                  {},
                  { ...mergedMultipleTaskFormResponse },
                  { ...result.results[result.results.length - 1] }
                );
              }
            }

            const multipleTaskResponse =
              state.multipleReaderTask.multipleTaskResponse;
            const hasTask = multipleTaskResponse.find(
              data => data._id === result.results[0]?.task_id
            );
            if (hasTask) {
              if (multipleTaskFormResponse) {
                store.dispatch(
                  setFormResponse({
                    ...multipleTaskFormResponse,
                  })
                );
              }

              if (!multipleTaskData.multipleTaskFormResponses) {
                multipleTaskData.multipleTaskFormResponses = [];
              }

              multipleTaskData.multipleTaskFormResponses.push({
                ...result.results[result.results.length - 1],
              });

              if (multipleTaskFormResponse) {
                if (!readerTask.info) {
                  readerTask.info = {};
                }

                readerTask.info.notes = {
                  ...mergedMultipleTaskFormResponse['response_data'],
                };

                const userId =
                  result.results[result.results.length - 1].origin.id;
                const taskId =
                  result.results[result.results.length - 1].task_id;

                if (!multipleTaskData.multipleTaskNotes) {
                  multipleTaskData.multipleTaskNotes = {};
                }

                if (!multipleTaskData.multipleTaskNotes[taskId]) {
                  multipleTaskData.multipleTaskNotes[taskId] = {};
                }

                if (!multipleTaskData.multipleTaskNotes[taskId].notes) {
                  multipleTaskData.multipleTaskNotes[taskId].notes = [];
                }

                if (!multipleTaskData.multipleTaskNotes[taskId].userId) {
                  multipleTaskData.multipleTaskNotes[taskId].userId = userId;
                }

                multipleTaskData.multipleTaskNotes[taskId].notes.push({
                  ...result.results[result.results.length - 1]['response_data'],
                });

                if (!multipleTaskData.multipleTaskUser) {
                  multipleTaskData.multipleTaskUser = [];
                }

                multipleTaskData.multipleTaskUser.push({
                  ...result.results[result.results.length - 1].origin,
                });
              }
            }
          })
        );
      }
    );
    await Promise.all(promises).then(result => {});
    multipleTaskDetails.multipleTaskNotes = multipleTaskData.multipleTaskNotes;
    multipleTaskDetails.multipleTaskUser = multipleTaskData.multipleTaskUser;
    multipleTaskDetails.multipleTaskFormResponses =
      multipleTaskData.multipleTaskFormResponses;
    return readerTask;
  } else {
    const formResponse = await getViewerFormResponse(
      readerTask.form_id,
      readerTask._id
    );

    if (formResponse.results.length) {
      store.dispatch(
        setFormResponse({
          ...formResponse.results[formResponse.results.length - 1],
        })
      );
    }

    if (formResponse) {
      if (!readerTask.info) {
        readerTask.info = {};
      }
      if (formResponse && formResponse.results.length) {
        readerTask.info.notes = {
          ...formResponse.results[formResponse.results.length - 1][
            'response_data'
          ],
        };
      }
    }
    return readerTask;
  }
}

async function getMultiTaskFormResponse(taskResponse) {
  const formResponse = await getViewerFormResponse(
    taskResponse.form_id,
    taskResponse._id
  );
  return formResponse;
}

/**
 * For backward compatibility to correct already saved older version of Open Freehand ROIs.
 * Update and correct poly bounding box for saved open freehand measurements
 * @param {*} data Measurement data
 * @returns
 */
function updatePolyBoundingBox(data) {
  const lines = data.handles.points;

  let polyBoundingBox;

  // Retrieve the bounds of the ROI in image coordinates
  const bounds = {
    left: lines[0][0].x,
    right: lines[0][0].x,
    bottom: lines[0][0].y,
    top: lines[0][0].y,
  };

  for (let l = 0; l < lines.length; l++) {
    const points = lines[l];
    if (!points.length) {
      return;
    }

    for (let i = 0; i < points.length; i++) {
      bounds.left = Math.min(bounds.left, points[i].x);
      bounds.right = Math.max(bounds.right, points[i].x);
      bounds.bottom = Math.min(bounds.bottom, points[i].y);
      bounds.top = Math.max(bounds.top, points[i].y);
    }

    polyBoundingBox = {
      left: bounds.left,
      top: bounds.bottom,
      width: Math.abs(bounds.right - bounds.left),
      height: Math.abs(bounds.top - bounds.bottom),
    };
  }

  // Store the bounding box information for the text box
  data.polyBoundingBox = polyBoundingBox;
}

/**
 * For backward compatibility to support already saved older version by converting to the upgraded
 * format(Ex: Currently the points with Array format of Open Freehand ROIs to Array of Array format)
 * This can be extended to support other measurements version upgrade
 * @param {*} response
 */
function formatMeasurements(response) {
  if (response?.info?.ohifViewer?.measurements) {
    let measures = _.cloneDeep(response.info.ohifViewer.measurements);
    const toolType = 'OpenFreehandRoi';
    const measurements = [];
    if (measures[toolType]?.length) {
      (measures[toolType] || []).forEach(measure => {
        if (
          measure?.handles?.points?.length &&
          !Array.isArray(measure.handles.points[0])
        ) {
          // Converting to Array of Array format by pushing it to the first index
          const points = measure.handles.points;
          measure.handles.points = [];
          measure.handles.points.push(points);
          measurements.push(measure);
        } else {
          measurements.push(measure);
        }
        updatePolyBoundingBox(measure);
      });
      response.info.ohifViewer.measurements[toolType] = measurements;
    }
  }
}

function setUrlBasedLabelsVisibility(routeParams, allTools) {
  const labelsFromUrl = routeParams?.labels;
  if (labelsFromUrl) {
    const labels = labelsFromUrl.split(',');
    Object.keys(allTools).forEach(key => {
      allTools[key].forEach(m => {
        m.visible = labels.includes(m.location);
      });
    });
  }
}

function setTimer(store, response = null) {
  let readTime = Date.now();
  if (response) {
    const readInfo = getUserReadInfo(response.info);
    if (readInfo) {
      readTime = readInfo.readTime;
    }
  }
  store.dispatch(setPreviousSessionReadTime(readTime));
}

function getUserReadInfo(info) {
  const state = store.getState();
  const user = selectUser(state);
  const ohifViewer = info.ohifViewer;
  return (
    ohifViewer && ohifViewer.read && ohifViewer.read[customInfoKey(user._id)]
  );
}

export default retrieveMeasurements;
