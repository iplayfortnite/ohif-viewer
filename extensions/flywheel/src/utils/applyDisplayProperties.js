import OHIF from '@ohif/core';
import store from '@ohif/viewer/src/store';
import resetLayoutToHP from './resetLayoutToHP';
import redux from '../redux';
import {
  Utils as FlywheelCommonUtils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import { updateOtherViewports } from '@flywheel/extension-cornerstone-infusions';
import cloneDeep from 'lodash.clonedeep';

const { setResizedFormHeight } = FlywheelCommonRedux.actions;
const { getEnabledElement } = FlywheelCommonUtils.cornerstoneUtils;
const { getRouteParams } = FlywheelCommonUtils;
const {
  setViewportLayoutAndData,
  setViewportActive,
  setViewportSpecificData,
} = OHIF.redux.actions;
const { selectActiveProject } = redux.selectors;

/**
 * Apply the viewer display properties and settings and update the viewer
 * @param {Object} commandsManager
 * @param {Object} state
 * @param {Object} viewerProperties
 */
const applyDisplayProperties = (commandsManager, state, viewerProperties) => {
  const restoreDisplaySets = {};
  const { slice } = getRouteParams();
  const viewportChangeState = {
    // This is using whether need to wait for the change to restoring layout
    // or continue by clearing the observers listening for viewport change
    // Note: currently using only for HP layout not matching scenarios handling and
    // can be used in other scenarios.
    waitForViewportChange: true,
  };
  viewerProperties.viewportsData.forEach((viewport, index) => {
    const ds = getDisplaySet(viewport);
    if (ds) {
      restoreDisplaySets[index] = ds;
    }
    if (slice !== undefined) {
      // Clearing the restoring slice details if explicitly mentioned a slice in the url
      delete viewport.scrollIndex;
      delete viewport.patientPos;
    }
  });
  if (!viewerProperties.protocolLayoutConfig) {
    commandsManager.runCommand('clearProtocolLayout', {});
    const unsubscribeFn = store.subscribe(() => {
      const state = store.getState();
      if (
        !state.infusions.protocolLayout &&
        cornerstone.getEnabledElements().length ===
          state.viewports.layout.viewports.length
      ) {
        unsubscribeFn();
        if (viewerProperties.is2DMPRMode) {
          const firstViewportProperties = viewerProperties.viewportsData[0];
          const { viewportSpecificData, activeViewportIndex } = state.viewports;
          if (viewerProperties.dataProtocol) {
            const activeDs = viewportSpecificData[activeViewportIndex];
            firstViewportProperties.seriesInstanceUID =
              activeDs.SeriesInstanceUID;
          }
          if (viewerProperties.viewportsData?.length === 1) {
            const orientations = firstViewportProperties.orientations;
            const displaySets = {};
            viewerProperties.viewportsData.forEach((vpData, index) => {
              vpData.orientation = orientations[index].orientation;
              delete vpData.orientations;
              const matchingDisplaySetKey =
                Object.keys(viewportSpecificData).find(
                  key =>
                    viewportSpecificData[key].SeriesInstanceUID ===
                    vpData.seriesInstanceUID
                ) || activeViewportIndex;
              displaySets[index] = cloneDeep(
                viewportSpecificData[matchingDisplaySetKey]
              );
              displaySets[index].plugin = 'vtk';
            });

            commandsManager.runCommand('protocolView', {
              viewportSpecificData: displaySets,
              viewportProps: viewerProperties.viewportsData,
              layout: { numRows: 1, numColumns: 1 },
              hideIndicators: true,
            });
          } else {
            commandsManager.runCommand('mpr2d', {
              viewports: state.viewports,
              viewerProperties: firstViewportProperties,
            });
          }
        } else {
          const viewports = [];
          viewerProperties.viewportsData.forEach(viewport => {
            viewports.push({
              height: '100%',
              width: '100%',
              plugin: viewport.plugin,
            });
          });
          store.dispatch(
            setViewportLayoutAndData(
              {
                numRows: viewerProperties.layout.numRows,
                numColumns: viewerProperties.layout.numColumns,
                viewports,
              },
              restoreDisplaySets
            )
          );
        }
      }
    });
  } else {
    const protocolStore = OHIF.hangingProtocols.loadProtocols({
      layouts: viewerProperties.protocolLayoutConfig,
    });
    const project = selectActiveProject(state);
    const protocol = {
      waitForProtocol: true,
      projectId: project._id,
      protocolStore,
    };
    resetLayoutToHP(
      commandsManager,
      store,
      protocol,
      isMatching => (viewportChangeState.waitForViewportChange = isMatching)
    );
  }
  if (
    !viewerProperties.is2DMPRMode ||
    viewerProperties.viewportsData?.length === 1
  ) {
    setTimeout(() => {
      updateViewportProperties(
        commandsManager,
        viewerProperties,
        restoreDisplaySets,
        viewportChangeState
      );
      store.dispatch(
        setViewportActive(viewerProperties.activeViewportIndex || 0)
      );
    }, 1000);
  }
  if (viewerProperties.formHeight > 0) {
    store.dispatch(setResizedFormHeight(viewerProperties.formHeight));
  }
};

/**
 * Update the image display properties in all viewport
 * @param {Object} commandsManager
 * @param {Object} viewerProperties
 * @param {Object} restoreDisplaySets
 * @param {Object} viewportChangeState
 */
const updateViewportProperties = (
  commandsManager,
  viewerProperties,
  restoreDisplaySets,
  viewportChangeState
) => {
  const unsubscribeFn = store.subscribe(() => {
    const state = store.getState();
    const viewports = state.viewports;
    if (
      viewports.layout.viewports.length ===
      viewerProperties.viewportsData.length
    ) {
      // Ensure the layout updated to the expected saved display layout before updating the
      // image properties.
      const isLayoutUpdated = !viewports.layout.viewports.find(
        (vp, index) =>
          viewports.viewportSpecificData[index].plugin !==
          viewerProperties.viewportsData[index].plugin
      );
      const enabledElements = cornerstone.getEnabledElements();
      if (!enabledElements.find(element => !element.image) && isLayoutUpdated) {
        unsubscribeFn();
        enabledElements.forEach(enabledElement =>
          cornerstone.resize(enabledElement.element)
        );
        for (
          let index = 0;
          index < viewports.layout.viewports.length;
          index++
        ) {
          const displaySet = viewports.viewportSpecificData[index] || {};
          const restoreViewportData = viewerProperties.viewportsData[index];
          const element = getEnabledElement(index);
          let commandName = 'update2DMPRViewport';
          if (element) {
            commandName = 'update2DViewport';
            if (
              displaySet.SeriesInstanceUID !==
              restoreViewportData.seriesInstanceUID
            ) {
              store.dispatch(
                setViewportSpecificData(index, restoreDisplaySets[index])
              );
              // If the displayed series is different from the one showing
              // then set it and on store update, update viewport properties also.
              setTimeout(() => {
                updateViewportProperties(
                  commandsManager,
                  viewerProperties,
                  restoreDisplaySets,
                  { waitForViewportChange: false }
                );
              }, 1);
              return;
            }
          }
          commandsManager.runCommand(commandName, {
            viewports,
            propertiesToSync: {
              ...restoreViewportData,
              viewportIndex: index,
            },
          });
          if (element) {
            updateOtherViewports(
              commandsManager,
              ['Wwwc', 'StackScroll', 'Zoom'],
              restoreDisplaySets[index],
              element,
              true
            );
          }
        }

        if (viewerProperties.colorMapId) {
          commandsManager.runCommand('changeColormap', {
            viewports,
            colorId: viewerProperties.colorMapId,
            reset: false,
          });
        }
        // A patch to adjust the viewport image to fit to the viewport on loading
        setTimeout(() => {
          store.dispatch(
            setViewportActive(viewerProperties.activeViewportIndex || 0)
          );
        }, 10);
      }
    } else if (!viewportChangeState.waitForViewportChange) {
      unsubscribeFn();
    }
  });
};

/**
 * Get the display set corresponding to the viewport data
 *
 * @param {Object} viewportData
 */
const getDisplaySet = viewportData => {
  const study = OHIF.utils.studyMetadataManager.get(
    viewportData.studyInstanceUID
  );

  return study.findDisplaySet(ds => {
    return ds.SeriesInstanceUID === viewportData.seriesInstanceUID;
  });
};

export default applyDisplayProperties;
