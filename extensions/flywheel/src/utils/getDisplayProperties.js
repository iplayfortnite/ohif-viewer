import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import OHIF from '@ohif/core';
import redux from '../redux';
const { selectActiveProtocolStore, selectProjectConfig } = redux.selectors;

const { getEnabledElement } = FlywheelCommonUtils.cornerstoneUtils;

/**
 * Get the viewer display properties and settings
 * @param {Object} commandsManager
 * @param {Object} state
 * @returns viewer display properties for restoring later
 */
const getDisplayProperties = (commandsManager, state) => {
  const viewports = [];
  let is2DMPRViewportExist = false;
  state.viewports.layout.viewports.forEach((vp, index) => {
    const displaySet = state.viewports.viewportSpecificData[index] || {};
    const element = getEnabledElement(index);
    let propertiesToSync = null;
    if (element) {
      const enabledElement = cornerstone.getEnabledElement(element);
      const toolState = cornerstoneTools.getToolState(element, 'stack');
      const stackData = toolState.data[0];
      const curIndex = stackData.currentImageIdIndex;
      const imagePlane = cornerstone.metaData.get(
        'imagePlaneModule',
        stackData.imageIds[curIndex]
      );
      const viewport = enabledElement.viewport;
      const oldCanvasWidth = enabledElement.canvas.width;
      const oldCanvasHeight = enabledElement.canvas.height;

      propertiesToSync = {
        patientPos: imagePlane?.imagePositionPatient,
        scrollIndex: curIndex,
        zoom: {
          type: 'rescale',
          viewportDim: { width: oldCanvasWidth, height: oldCanvasHeight },
          scale: viewport.scale,
        },
        pan: {
          type: 'relative',
          translation: {
            x: viewport.translation.x * viewport.scale,
            y: viewport.translation.y * viewport.scale,
          },
        },
        windowLevel: viewport.voi,
        rotation: viewport.rotation,
        hflip: viewport.hflip,
        vflip: viewport.vflip,
        invert: viewport.invert,
      };
    } else {
      is2DMPRViewportExist = true;
      propertiesToSync = commandsManager.runCommand('getVtkPropertiesForView', {
        displaySet,
      });
    }

    viewports[index] = {
      seriesInstanceUID: displaySet.SeriesInstanceUID,
      studyInstanceUID: displaySet.StudyInstanceUID,
      plugin: displaySet.plugin,
    };
    viewports[index] = {
      ...viewports[index],
      ...propertiesToSync,
    };
  });
  const projectConfig = selectProjectConfig(state);
  const viewerProperties = {
    protocolLayoutConfig: null,
    viewportsData: viewports,
    layout: null,
    is2DMPRMode: false,
    formHeight: state.measurementPanelProperties.formHeight,
    dataProtocol: state.viewports.viewportSpecificData[0].dataProtocol,
    activeViewportIndex: state.viewports.activeViewportIndex || 0,
    colorMapId: state.viewports.colormapId,
  };

  const { ProtocolEngineUtils } = OHIF.hangingProtocols;
  const protocolStore = selectActiveProtocolStore(state);
  if (protocolStore) {
    ProtocolEngineUtils.isCurrentLayoutMatchingWithHP(
      state,
      protocolStore.protocolStore,
      isMatching => {
        if (isMatching) {
          viewerProperties.protocolLayoutConfig = projectConfig.layouts;
        } else {
          // If not in HP layout, then one 2D MPR viewport assumes the viewer in 2D MPR mode;
          viewerProperties.is2DMPRMode = is2DMPRViewportExist;
          viewerProperties.layout = {
            numColumns: state.viewports.numColumns,
            numRows: state.viewports.numRows,
          };
        }
      },
      true
    );
  } else {
    // If not in HP layout, then one 2D MPR viewport assumes the viewer in 2D MPR mode;
    viewerProperties.is2DMPRMode = is2DMPRViewportExist;
    viewerProperties.layout = {
      numColumns: state.viewports.numColumns,
      numRows: state.viewports.numRows,
    };
  }
  return viewerProperties;
};

export default getDisplayProperties;
