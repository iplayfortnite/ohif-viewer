import cloneDeep from 'lodash.clonedeep';
import {
  ADD_INSTANCE_TAGS,
  CLEAR_ASSOCIATIONS,
  CLEAR_FILE_ASSOCIATIONS,
  SET_ACTIVE_PROJECT,
  SET_ACTIVE_SESSION,
  SET_ACTIVE_SESSIONS,
  SET_ALL_PROJECTS,
  SET_ALL_SESSIONS,
  SET_ASSOCIATIONS,
  SET_FILE_ASSOCIATIONS,
  SET_CURRENT_NIFTI,
  SET_CURRENT_NIFTIS,
  SET_CURRENT_META_IMAGE,
  SET_PROJECT_CONFIG,
  SET_SERIES_ORDER,
  SET_USER,
  SET_ALL_USERS,
  SET_ACTIVE_PROTOCOL_STORE,
  SET_READER_TASK,
  SET_FEATURE_CONFIG,
  SET_SESSION_ACQUISITIONS,
  SET_SESSIONS,
  SET_FORM_RESPONSE,
  SET_CURRENT_WEB_IMAGE,
  SET_CURRENT_WEB_IMAGES,
  SET_ROLES,
  SET_PERMISSIONS,
  SET_READER_TASK_WITH_FORM_RESPONSE,
} from '../constants/ActionTypes';

import selectors from '../selectors';
const { getSessionReadData } = selectors;
const deferreds = [];

const initialState = {
  allAssociations: [],
  allProjects: {}, // project._id -> project
  allSessions: {}, // session._id -> session
  associations: {}, // StudyInstanceUID -> { session_id }
  currentNiftis: [],
  currentMetaImage: null,
  currentWebImage: [],
  fileAssociations: {},
  instanceTags: {}, // SOPInstanceUID -> { tag -> { vr, Value } }
  project: null,
  projectConfig: null,
  sessions: [],
  user: null,
  protocolStore: null,
  readerTask: null,
  readerTaskWithFormResponse: null,
  featureConfig: {},
  acquisitions: {},
  sessionList: [],
  formResponse: null,
  roles: [],
  allUsers: {},
  permissions: new Promise((resolve, reject) => {
    deferreds.push({ resolve: resolve, reject: reject });
  }),
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ADD_INSTANCE_TAGS:
      return {
        ...state,
        instanceTags: {
          ...state.instanceTags,
          [action.SOPInstanceUID]: action.tags,
        },
      };
    case CLEAR_ASSOCIATIONS:
      return {
        ...state,
        allAssociations: [],
        associations: {},
      };
    case CLEAR_FILE_ASSOCIATIONS:
      return {
        ...state,
        allfileAssociations: [],
        fileAssociations: {},
      };
    case SET_ACTIVE_SESSION:
      const sessions = [];
      sessions.push(action.session);
      return {
        ...state,
        sessions: sessions,
        allSessions: {
          ...state.allSessions,
          [action.session._id]: {
            ...action.session,
            isRead: Boolean(getSessionReadData(action.session, state.user)),
          },
        },
        currentNiftis: action.clearExistingInfos?.clearNifti
          ? []
          : state.currentNiftis,
        currentMetaImage: action.clearExistingInfos?.clearMeta
          ? null
          : state.currentMetaImage,
      };
    case SET_ACTIVE_SESSIONS:
      const allSessions = Object.assign(
        {},
        state.allSessions,
        ...Object.values(action.sessions).map(session => ({
          [session._id]: {
            ...session,
            isRead: Boolean(getSessionReadData(session, state.user)),
          },
        }))
      );
      return {
        ...state,
        sessions: action.sessions,
        allSessions: allSessions,
        currentNiftis: action.clearExistingInfos?.clearNifti
          ? []
          : state.currentNiftis,
        currentMetaImage: action.clearExistingInfos?.clearMeta
          ? null
          : state.currentMetaImage,
      };
    case SET_ACTIVE_PROJECT:
      return { ...state, project: action.project };
    case SET_ALL_PROJECTS:
      return { ...state, allProjects: action.projects };
    case SET_ALL_SESSIONS:
      return {
        ...state,
        allSessions: {
          ...state.allSessions,
          ...action.sessions,
        },
      };
    case SET_SESSIONS:
      return { ...state, sessionList: action.sessions };
    case SET_ASSOCIATIONS:
      return {
        ...state,
        allAssociations: [...state.allAssociations, ...action.associations],
        associations: {
          ...state.associations,
          ...Object.assign(
            {},
            ...action.associations.map(
              ({ project_id, session_id, study_uid }) => ({
                [study_uid]: { project_id, session_id, study_uid },
              })
            )
          ),
        },
      };
    case SET_FILE_ASSOCIATIONS:
      return {
        ...state,
        fileAssociations: {
          ...state.fileAssociations,
          ...Object.assign({
            containerId: action.fileAssociations.containerId,
            filename: action.fileAssociations.filename,
          }),
        },
      };

    case SET_CURRENT_NIFTI:
      const currentNiftis = action.clear ? [] : cloneDeep(state.currentNiftis);
      const index = currentNiftis.findIndex(
        nifti =>
          action.containerId === nifti.containerId &&
          action.filename === nifti.filename
      );
      if (index > -1) {
        currentNiftis[index] = {
          containerId: action.containerId,
          filename: action.filename,
          file_id: action.file_id,
          info: action.info,
          parents: action.parents,
        };
      } else {
        currentNiftis.push({
          containerId: action.containerId,
          filename: action.filename,
          file_id: action.file_id,
          info: action.info,
          parents: action.parents,
        });
      }
      return {
        ...state,
        ...{ currentNiftis: currentNiftis },
        sessions: action.clearExistingInfos?.clearSession
          ? null
          : state.sessions,
      };

    case SET_CURRENT_NIFTIS:
      return {
        ...state,
        ...{ currentNiftis: action.niftis },
        sessions: action.clearExistingInfos?.clearSession
          ? null
          : state.sessions,
      };

    case SET_CURRENT_META_IMAGE:
      return {
        ...state,
        currentMetaImage: {
          containerId: action.containerId,
          filename: action.filename,
          file_id: action.fileId,
          info: action.info,
          parents: action.parents,
        },
        sessions: action.clearExistingInfos?.clearSession
          ? null
          : state.sessions,
      };
    case SET_PROJECT_CONFIG:
      return { ...state, projectConfig: action.config };
    case SET_SERIES_ORDER:
      return { ...state, seriesOrder: action.seriesOrder };
    case SET_USER:
      return { ...state, user: action.user };
    case SET_ALL_USERS:
      return { ...state, allUsers: action.users };
    case SET_ACTIVE_PROTOCOL_STORE:
      const protocolStr = {
        projectId: action.projectId,
        protocolStore: action.protocolStore,
        waitForProtocol: action.waitForProtocol,
      };
      return { ...state, protocolStore: protocolStr };
    case SET_READER_TASK:
      return { ...state, readerTask: action.task };
    case SET_READER_TASK_WITH_FORM_RESPONSE:
      return { ...state, readerTaskWithFormResponse: action.task };
    case SET_FEATURE_CONFIG:
      return { ...state, featureConfig: action.featureConfig };
    case SET_SESSION_ACQUISITIONS:
      return { ...state, acquisitions: action.acquisitions };
    case SET_FORM_RESPONSE:
      return { ...state, formResponse: action.response };
    case SET_CURRENT_WEB_IMAGE:
      const currentWebImage = cloneDeep(state.currentWebImage);
      const webImageIndex = currentWebImage.findIndex(
        image =>
          action.containerId === image.containerId &&
          action.filename === image.filename
      );
      if (webImageIndex > -1) {
        currentWebImage[webImageIndex] = {
          containerId: action.containerId,
          filename: action.filename,
          file_id: action.fileId,
          info: action.info,
          parents: action.parents,
        };
      } else {
        currentWebImage.push({
          containerId: action.containerId,
          filename: action.filename,
          file_id: action.fileId,
          info: action.info,
          parents: action.parents,
        });
      }
      return {
        ...state,
        ...{ currentWebImage },
      };

    case SET_CURRENT_WEB_IMAGES:
      return {
        ...state,
        ...{ currentWebImage: action.webImages },
        sessions: action.clearExistingInfos?.clearSession
          ? null
          : state.sessions,
      };

    case SET_PERMISSIONS: {
      deferreds[0].resolve(action.permissions);
      return { ...state };
    }

    case SET_ROLES:
      return { ...state, roles: action.roles };
    default:
      return state;
  }
}
