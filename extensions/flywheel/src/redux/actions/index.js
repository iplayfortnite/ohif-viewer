import * as associations from './associations';
import * as fileAssociations from './fileAssociations';
import * as project from './project';
import * as session from './session';
import * as study from './study';
import * as studyList from './studyList';
import * as user from './user';
import * as protocolStore from './protocolStore';
import * as multipleReaderTask from './multipleReaderTask';

const actions = {
  ...associations,
  ...fileAssociations,
  ...project,
  ...session,
  ...study,
  ...studyList,
  ...user,
  ...protocolStore,
  ...multipleReaderTask,
};

export default actions;
