import {
  SET_ACTIVE_PROJECT,
  SET_ALL_PROJECTS,
  SET_PROJECT_CONFIG,
  SET_READER_TASK,
  SET_FEATURE_CONFIG,
  SET_FORM_RESPONSE,
  SET_ROLES,
  SET_PERMISSIONS,
  SET_READER_TASK_WITH_FORM_RESPONSE,
} from '../constants/ActionTypes';

export function setActiveProject(project) {
  return {
    type: SET_ACTIVE_PROJECT,
    project,
  };
}

export function setAllProjects(projects) {
  return {
    type: SET_ALL_PROJECTS,
    projects,
  };
}

export function setProjectConfig(config) {
  return {
    type: SET_PROJECT_CONFIG,
    config,
  };
}

export function setPermissions(permissions) {
  return {
    type: SET_PERMISSIONS,
    permissions,
  };
}

export function setReaderTask(task) {
  return {
    type: SET_READER_TASK,
    task,
  };
}

export function setReaderTaskWithFormResponse(task) {
  return {
    type: SET_READER_TASK_WITH_FORM_RESPONSE,
    task,
  };
}

export function setFeatureConfig(featureConfig) {
  return {
    type: SET_FEATURE_CONFIG,
    featureConfig,
  };
}

export function setFormResponse(response) {
  return {
    type: SET_FORM_RESPONSE,
    response,
  };
}

export function setRoles(roles) {
  return {
    type: SET_ROLES,
    roles: roles,
  };
}
