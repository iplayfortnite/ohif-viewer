import {
  ADD_INSTANCE_TAGS,
  SET_SERIES_ORDER,
  SET_CURRENT_NIFTIS,
  SET_CURRENT_NIFTI,
  SET_CURRENT_META_IMAGE,
  SET_CURRENT_WEB_IMAGE,
  SET_CURRENT_WEB_IMAGES,
} from '../constants/ActionTypes';

export function addInstanceTags(SOPInstanceUID, tags) {
  return {
    type: ADD_INSTANCE_TAGS,
    SOPInstanceUID,
    tags,
  };
}

export function setSeriesOrder(seriesOrder) {
  return {
    type: SET_SERIES_ORDER,
    seriesOrder,
  };
}

export function setCurrentNifti(
  containerId,
  filename,
  fileId,
  info,
  parents,
  clearExistingInfos = {
    clearSession: true,
  }
) {
  return {
    type: SET_CURRENT_NIFTI,
    containerId,
    filename,
    file_id: fileId,
    info,
    parents,
    clearExistingInfos,
  };
}

export function setCurrentNiftis(
  niftis,
  clearExistingInfos = {
    clearSession: true,
  }
) {
  return {
    type: SET_CURRENT_NIFTIS,
    niftis,
    clearExistingInfos,
  };
}

export function setCurrentMetaImage(
  containerId,
  filename,
  fileId,
  info,
  parents,
  clearExistingInfos = {
    clearSession: true,
  }
) {
  return {
    type: SET_CURRENT_META_IMAGE,
    containerId,
    filename,
    fileId,
    info,
    parents,
    clearExistingInfos,
  };
}

export function setCurrentWebImage(
  containerId,
  filename,
  fileId,
  info,
  parents
) {
  return {
    type: SET_CURRENT_WEB_IMAGE,
    containerId,
    filename,
    fileId,
    info,
    parents,
  };
}

export function setCurrentWebImages(
  webImages,
  clearExistingInfos = {
    clearSession: true,
  }
) {
  return {
    type: SET_CURRENT_WEB_IMAGES,
    webImages,
    clearExistingInfos,
  };
}
