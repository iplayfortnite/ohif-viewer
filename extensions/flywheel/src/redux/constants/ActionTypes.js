export const ADD_INSTANCE_TAGS = 'ohif/flywheel/ADD_INSTANCE_TAGS';
export const CLEAR_ASSOCIATIONS = 'ohif/flywheel/CLEAR_ASSOCIATIONS';
export const CLEAR_FILE_ASSOCIATIONS = 'ohif/flywheel/CLEAR_FILE_ASSOCIATIONS';
export const SET_ACTIVE_PROJECT = 'ohif/flywheel/SET_ACTIVE_PROJECT';
export const SET_ACTIVE_SESSION = 'ohif/flywheel/SET_ACTIVE_SESSION';
export const SET_ACTIVE_SESSIONS = 'ohif/flywheel/SET_ACTIVE_SESSIONS';
export const SET_ALL_PROJECTS = 'ohif/flywheel/SET_ALL_PROJECTS';
export const SET_ALL_SESSIONS = 'ohif/flywheel/SET_ALL_SESSIONS';
export const SET_SESSIONS = 'ohif/flywheel/SET_SESSIONS';
export const SET_ASSOCIATIONS = 'ohif/flywheel/SET_ASSOCIATIONS';
export const SET_FILE_ASSOCIATIONS = 'ohif/flywheel/SET_FILE_ASSOCIATIONS';
export const SET_CURRENT_NIFTIS = 'ohif/flywheel/SET_CURRENT_NIFTIS';
export const SET_CURRENT_NIFTI = 'ohif/flywheel/SET_CURRENT_NIFTI';
export const SET_CURRENT_META_IMAGE = 'ohif/flywheel/SET_CURRENT_META_IMAGE';
export const SET_CURRENT_WEB_IMAGE = 'ohif/flywheel/SET_CURRENT_WEB_IMAGE';
export const SET_CURRENT_WEB_IMAGES = 'ohif/flywheel/SET_CURRENT_WEB_IMAGES';
export const SET_PROJECT_CONFIG = 'ohif/flywheel/SET_PROJECT_CONFIG';
export const SET_SERIES_ORDER = 'ohif/flywheel/SET_SERIES_ORDER';
export const SET_USER = 'ohif/flywheel/SET_USER';
export const SET_ALL_USERS = 'ohif/flywheel/SET_ALL_USERS';
// study list
export const SET_STUDY_SORT = 'ohif/flywheel/SET_STUDY_SORT';
export const SET_STUDY_FILTERS = 'ohif/flywheel/SET_STUDY_FILTERS';
export const SET_STUDY_ORDERING = 'ohif/flywheel/SET_STUDY_ORDERING';
// Active protocol store
export const SET_ACTIVE_PROTOCOL_STORE =
  'ohif/flywheel/SET_ACTIVE_PROTOCOL_STORE';
export const SET_READER_TASK = 'ohif/flywheel/SET_READER_TASK';
export const SET_READER_TASK_WITH_FORM_RESPONSE =
  'ohif/flywheel/SET_READER_TASK_WITH_FORM_RESPONSE';
export const SET_FEATURE_CONFIG = 'ohif/flywheel/SET_FEATURE_CONFIG';

export const SET_SESSION_ACQUISITIONS =
  'ohif/flywheel/SET_SESSION_ACQUISITIONS';
export const SET_FORM_RESPONSE = 'ohif/flywheel/SET_FORM_RESPONSE';
export const SET_ROLES = 'ohif/flywheel/SET_ROLES';
export const SET_PERMISSIONS = 'ohif/flywheel/SET_PERMISSIONS';
export const SET_MULTIPLE_READER_TASK =
  'ohif/flywheel/SET_MULTIPLE_READER_TASK';
export const SET_MULTIPLE_READER_TASK_RESPONSE =
  'ohif/flywheel/SET_MULTIPLE_READER_TASK_RESPONSE';
