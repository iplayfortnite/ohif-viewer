export function selectAssociations(state) {
  return state.flywheel.associations;
}

export function selectAllAssociations(state) {
  return state.flywheel.allAssociations;
}
