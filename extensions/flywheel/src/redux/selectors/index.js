import * as associations from './associations';
import * as fileAssociations from './fileAssociations';
import * as measurements from './measurements';
import * as project from './project';
import * as session from './session';
import * as studyList from './studyList';
import * as user from './user';
import * as protocolStore from './protocolStore';

const selectors = {
  ...associations,
  ...fileAssociations,
  ...measurements,
  ...project,
  ...session,
  ...studyList,
  ...user,
  ...protocolStore,
};

export default selectors;
