import { createSelector } from 'reselect';
import { selectUser } from './user';
import { selectAssociations } from './associations';
import { selectFileAssociations } from './fileAssociations';
import {
  HTTP as FlywheelCommonHTTP,
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';

const { customInfoKey } = FlywheelCommonHTTP.utils.REQUEST_UTILS;
const {
  selectCurrentNifti,
  selectCurrentVolumetricImage,
  selectCurrentWebImage,
} = FlywheelCommonRedux.selectors;
const {
  isCurrentNifti,
  isCurrentVolumetricImage,
  isCurrentWebImage,
} = FlywheelCommonUtils;

export function selectActiveSession(state, index = 0) {
  return state.flywheel.sessions !== null
    ? state.flywheel.sessions.length > index
      ? state.flywheel.sessions[index]
      : state.flywheel.sessions[0]
    : null;
}
export function selectActiveSessions(state) {
  return state.flywheel.sessions;
}

export function selectSessions(state) {
  return state.flywheel.sessionList;
}

export function selectSessionData(state, index = 0) {
  const session = selectActiveSession(state, index);
  return (session && session.info && session.info.ohifViewer) || {};
}

export function selectSessionFileData(state, index = 0) {
  let currentFile = null;
  if (isCurrentNifti(state)) {
    currentFile = selectCurrentNifti(state, index);
  } else if (isCurrentVolumetricImage(state)) {
    currentFile = selectCurrentVolumetricImage(state, index);
  } else if (isCurrentWebImage()) {
    currentFile = selectCurrentWebImage(state);
  }
  return (currentFile && currentFile.info && currentFile.info.ohifViewer) || {};
}

export function selectSessionRead(state, index = 0) {
  const user = selectUser(state);
  if (!user) {
    return undefined;
  }
  const data = selectSessionData(state, index);
  if (!data) {
    return undefined;
  }
  return data.read && data.read[customInfoKey(user._id)];
}

export function selectSessionFileRead(state, index = 0) {
  const user = selectUser(state);
  if (!user) {
    return undefined;
  }
  const data = selectSessionFileData(state, index);
  if (!data) {
    return undefined;
  }
  return data.read && data.read[customInfoKey(user._id)];
}

export function selectAllSessions(state) {
  return state.flywheel.allSessions;
}

export const selectCompletedSessions = createSelector(
  selectUser,
  selectAllSessions,
  (user, allSessions) => {
    if (!user) {
      return [];
    }
    const readSessions = Object.values(allSessions).filter(session =>
      getSessionReadData(session, user)
    );
    readSessions.sort((a, b) => {
      a = getSessionReadData(a, user).date;
      b = getSessionReadData(b, user).date;
      return a < b ? -1 : a > b ? 1 : 0;
    });
    return readSessions;
  }
);

export const selectStudySessions = createSelector(
  selectAllSessions,
  selectAssociations,
  selectFileAssociations,
  (allSessions, associations) => {
    const studySessions = {};
    for (const { session_id, study_uid } of Object.values(associations)) {
      if (session_id in allSessions) {
        studySessions[study_uid] = allSessions[session_id];
      }
    }
    return studySessions;
  }
);

export function getSessionReadData(session, user) {
  const data = session.info && session.info.ohifViewer;
  return (data && data.read && data.read[customInfoKey(user._id)]) || undefined;
}
