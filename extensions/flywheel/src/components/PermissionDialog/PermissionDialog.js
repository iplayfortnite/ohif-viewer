import { Component } from 'react';
import React from 'react';
import PropTypes from 'prop-types';
import { SimpleDialog } from '@ohif/ui';

import bounding from '@ohif/viewer/src/lib/utils/bounding.js';

import './PermissionDialog.styl';

export default class PermissionDialog extends Component {
  static defaultProps = {
    componentRef: React.createRef(),
    componentStyle: {},
  };

  static propTypes = {
    measurements: PropTypes.array.isRequired,
    onClose: PropTypes.func.isRequired,
    onContinue: PropTypes.func,
    deleteSingleMeasurement: PropTypes.func,
    componentRef: PropTypes.object,
    componentStyle: PropTypes.object,
    toolActivationContext: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {};
    this.mainElement = React.createRef();
  }

  componentDidMount = () => {
    bounding(this.mainElement);
  };

  render() {
    return (
      <SimpleDialog
        headerTitle="Delete Measurements"
        onClose={() => {}}
        onConfirm={() => {}}
        componentRef={this.mainElement}
        showFooterButtons={false}
        hideCloseButton={true}
      >
        <div className="label-style">{this.props.label}</div>
        <div className="button-container">
          <div
            className="button-style"
            onClick={() => {
              const { toolActivationContext } = this.props.contentProps;
              this.props.onDelete(
                this.props.measurements,
                toolActivationContext
              );
              this.props.onClose();
            }}
          >
            Yes
          </div>

          <div onClick={this.props.onClose} className="button-style">
            No
          </div>
        </div>
      </SimpleDialog>
    );
  }
}
