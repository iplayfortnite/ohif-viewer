import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import { SimpleDialog } from '@ohif/ui';
import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
import {
  getSelectedSegmentationData,
  getActiveSegmentationDisplaySet,
} from '@flywheel/extension-cornerstone-infusions';
import './EditSegmentationLabel.styl';

const { getSmartCTRangeList } = FlywheelCommonUtils;
const { setSmartCTRange } = FlywheelCommonRedux.actions;
const { setters, state } = cornerstoneTools.getModule('segmentation');

export default function EditSegmentationLabelDialog(props) {
  const { onClose, segmentationData } = props.contentProps;
  const dispatch = useDispatch();
  const smartCTRangeList = getSmartCTRangeList();
  const selectedRange = useSelector(state => state.smartCT.selectedRange);

  const [selectedItem, setSelectedItem] = useState(selectedRange);

  function updateSegmentPixels(segmentIndex) {
    const {
      activeSegmentationDisplaySet,
      activeLabelmap,
      firstImageId,
      element,
    } = getSelectedSegmentationData(segmentationData);

    const labelMaps2D = activeLabelmap.labelmaps2D;
    const prevSegmentIndex = activeLabelmap.activeSegmentIndex;

    if (!labelMaps2D) {
      return;
    }

    Object.keys(labelMaps2D).forEach(imageIndex => {
      if (labelMaps2D[imageIndex].segmentsOnLabelmap) {
        const labelMap2D = labelMaps2D[imageIndex];
        const indexOnSegment = labelMap2D.segmentsOnLabelmap.findIndex(
          index => index === prevSegmentIndex
        );
        if (indexOnSegment > -1) {
          labelMap2D.segmentsOnLabelmap[indexOnSegment] = segmentIndex;
          labelMap2D.pixelData.forEach((segmentationIndex, index) => {
            if (segmentationIndex === prevSegmentIndex) {
              labelMap2D.pixelData[index] = segmentIndex;
            }
          });
        }
      }
    });
    const brushStackState = state.series[firstImageId];

    brushStackState.labelmaps3D[
      activeSegmentationDisplaySet.labelmapIndex
    ].activeSegmentIndex = segmentIndex;

    setters.activeSegmentIndex(element, segmentIndex);
    const segDisplaySet = getActiveSegmentationDisplaySet(segmentationData);
    if (segDisplaySet) {
      segDisplaySet.segmentationIndex = segmentIndex;
    }
    const colorLutTable = state.colorLutTables[activeLabelmap.colorLUTIndex];
    colorLutTable[segmentIndex] = selectedItem?.color;
    onClose();
    cornerstone.updateImage(element);
  }

  const setSelectedLabel = () => {
    dispatch(setSmartCTRange(selectedItem));

    const segmentIndex = smartCTRangeList.findIndex(
      segmentation => segmentation.label === selectedItem.label
    );

    if (segmentIndex > -1) {
      updateSegmentPixels(segmentIndex + 1);
    }
    onClose();
  };

  const listItems = smartCTRangeList.map(item => (
    <li
      key={item.label}
      onClick={() => setSelectedItem(item)}
      className={`${selectedItem.label === item.label && 'active-li'}`}
    >
      <div
        className="color-picker-container"
        style={{
          backgroundColor: `rgba(${item.color[0]}, ${item.color[1]}, ${item.color[2]}, 1.0 )`,
        }}
      ></div>
      <div style={{ color: 'white' }}>{item.label}</div>
    </li>
  ));

  return (
    <SimpleDialog
      headerTitle={'Change Segmentation Label'}
      rootClass="segment-label-chooser-dialog"
      onClose={onClose}
      onConfirm={setSelectedLabel}
      showFooterButtons={true}
      confirmButtonTitle={'Save'}
      enableConfirm={true}
    >
      <div className="selectLabel-container">
        <ul>{listItems}</ul>
      </div>
    </SimpleDialog>
  );
}
