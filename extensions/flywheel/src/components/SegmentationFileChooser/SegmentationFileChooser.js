import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { ServicesManager, utils, redux } from '@ohif/core';
import { SimpleDialog, useSnackbarContext } from '@ohif/ui';
import {
  HTTP as FlywheelCommonHTTP,
  Utils as FlywheelCommonUtils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import flywheelSelectors from '../../redux/selectors';
const {
  listAcquisitions,
  listSubjects,
  listSessionsBySubjectId,
  listSessionAnalyses,
  getProject,
  getContainer,
  getAcquisitionAssociation,
  listSubjectAnalyses,
  getProjectJobs,
} = FlywheelCommonHTTP.services;
import './SegmentationFileChooser.css';

const { addSegmentationData } = FlywheelCommonRedux.actions;
const {
  selectCurrentNifti,
  selectCurrentMetaImage,
  selectCurrentVolumetricImage,
} = FlywheelCommonRedux.selectors;
const { getRouteParams } = FlywheelCommonUtils;
const servicesManager = new ServicesManager();
const { UINotificationService } = servicesManager.services;
const { performanceTracking } = utils;
const { performanceStart } = performanceTracking.setPerformance;
const { SEGMENTATION_LOAD } = redux.performanceTrackerKeys;

const PROJECT = 'PROJECT';
const PROJECT_ANALYSES = 'PROJECT_ANALYSES';
const SUBJECTS = 'SUBJECTS';
const SUBJECTS_ANALYSES = 'SUBJECTS_ANALYSES';
const SESSION = 'SESSION';
const SESSION_ANALYSES = 'SESSION_ANALYSES';
const ACQUISITION = 'ACQUISITION';
const ACQUISITION_ANALYSES = 'ACQUISITION_ANALYSES';
const FILES = 'FILES';

export default function SegmentationFileChooser(props) {
  const dispatch = useDispatch();
  const history = useHistory();
  const snackbarContext = useSnackbarContext();

  const params = getRouteParams(history.location);

  const session = useSelector(flywheelSelectors.selectActiveSession);
  const currentNifti = useSelector(selectCurrentNifti);
  const currentMetaImage = useSelector(selectCurrentMetaImage);
  const currentVolumetricImage = useSelector(selectCurrentVolumetricImage);
  // For keeping the active reference UID in which segmentations are added
  const activeReferenceUID = useSelector(store => {
    let referenceUID = null;
    if (store.viewports) {
      const { viewportSpecificData, activeViewportIndex } = store.viewports;
      if (viewportSpecificData[activeViewportIndex]) {
        referenceUID = currentVolumetricImage
          ? viewportSpecificData[activeViewportIndex].StudyInstanceUID
          : viewportSpecificData[activeViewportIndex].SeriesInstanceUID;
      }
    }
    return referenceUID;
  });

  const segmentationData = useSelector(store => {
    return store?.segmentation?.segmentationData;
  });
  const allAssociations = useSelector(store => {
    return store?.flywheel?.allAssociations;
  });

  const evaluatePerformance = useSelector(store => {
    return store?.flywheel?.projectConfig?.evaluatePerformance;
  });
  // to set project
  const [project, setProject] = useState([]);
  // to set Subjects list based on project
  const [subjects, setSubjects] = useState([]);
  // to set Subjects analyses
  const [subjectsAnalyses, setSubjectsAnalyses] = useState([]);
  // to set session list based on subject
  const [sessions, setSessions] = useState([]);
  // to set session analyses
  const [sessionAnalyses, setSessionAnalyses] = useState([]);
  // to set acquisition list based on session
  const [acquisitions, setAcquisitions] = useState([]);
  // to set acquisition analyses
  const [acquisitionsAnalyses, setAcquisitionsAnalyses] = useState([]);

  const [files, setFiles] = useState([]);
  const [title, setTitle] = useState('');
  const [stage, setStage] = useState(initFileChooserLevel());
  const [selectedFile, setSelectedFile] = useState(null);
  const [manageItemClick, setManageItemClick] = useState({});
  const [sessionAnalysisResponse, setSessionAnalysesResponse] = useState([]);
  const [subjectAnalysisResponse, setSubjectAnalysesResponse] = useState([]);
  const [
    acquisitionAnalysisResponse,
    setAcquisitionAnalysesResponse,
  ] = useState([]);
  const [acquisitionAnalysisData, setAcquisitionAnalysisData] = useState([]);
  const [inputFiles, setInputFiles] = useState([]);
  const [outputFiles, setOutputFiles] = useState([]);
  const [headerItem] = useState([{}]);

  /**
   * To initialize folder level based on project type
   */
  function initFileChooserLevel() {
    const projectType = getProjectType();
    switch (projectType) {
      case 'DICOM':
        return ACQUISITION;
      case 'NIFTI':
      case 'META_IMAGE':
        return FILES;
      default:
        return SUBJECTS;
    }
  }

  function filterFiles(files) {
    return (files || []).filter(data => {
      return (
        data.type === 'dicom' ||
        data.type === 'image' ||
        data.type === 'nifti' ||
        data.type === 'ITK MetaIO Header'
      );
    });
  }

  useEffect(() => {
    const projectId = params.projectId;
    getProject(projectId).then(res => {
      if (res) {
        let data = res;
        data.analyses = data.analyses.filter(item => {
          item.files = filterFiles(item.files);
          item.inputs = filterFiles(item.inputs);
          if (item.files.length > 0 || item.inputs.length > 0) {
            return item;
          }
        });
        setProject(data);
      }
    });

    getAcquisitionAnalyses(projectId);

    // To get subjects
    listSubjects(projectId).then(res => {
      if (res) {
        setSubjects(res);
      }
    });

    // To initialize volumetric image(nifti, meta-image like mhd etc) files
    if (currentVolumetricImage) {
      initializeVolumetricImageFiles(currentVolumetricImage.containerId);
    }

    // To initialize dicom files
    if (session) {
      initializeDicomFiles();
    }
  }, []);

  useEffect(() => {
    if (
      sessionAnalysisResponse.length &&
      !sessionAnalyses.some(x => x._id === sessionAnalysisResponse[0]._id)
    ) {
      const data = sessionAnalyses.concat(sessionAnalysisResponse);
      setSessionAnalyses([...data]);
    }
  }, [sessionAnalysisResponse]);

  useEffect(() => {
    if (
      subjectAnalysisResponse.length &&
      !subjectsAnalyses.some(x => x._id === subjectAnalysisResponse[0]._id)
    ) {
      const data = subjectsAnalyses.concat(subjectAnalysisResponse);
      setSubjectsAnalyses([...data]);
    }
  }, [subjectAnalysisResponse]);

  useEffect(() => {
    if (
      acquisitionAnalysisResponse.length &&
      !acquisitionsAnalyses.some(
        x => x._id === acquisitionAnalysisResponse[0]._id
      )
    ) {
      let data = acquisitionsAnalyses.concat(acquisitionAnalysisResponse);
      data = Array.prototype.concat.apply([], data);
      setAcquisitionsAnalyses([...data]);
    }
  }, [acquisitionAnalysisResponse]);

  /**
   * to initialize dicom files
   */
  function initializeDicomFiles() {
    if (session.parents && session.parents.subject) {
      const subjectId = session.parents.subject;
      getSessionBySubjectId(subjectId, false);
      getSubjectsAnalyses(subjectId);
    }
    if (session._id) {
      getAcquisitions(session._id);
    }
  }

  /**
   * To initialize volumetric image(nifti, meta-image like mhd etc) files
   */
  function initializeVolumetricImageFiles() {
    try {
      getContainer(currentVolumetricImage.containerId).then(res => {
        if (res) {
          setFiles(res.files);
          setStage(FILES);
          if (res.label) {
            setTitle(res.label);
          }
          if (res.session) {
            getAcquisitions(res.session, false);
          }
          if (res.parents && res.parents.subject) {
            let subjectId = res.parents.subject;
            getSessionBySubjectId(subjectId, false);
            getSubjectsAnalyses(subjectId);
          }
        }
      });
    } catch (error) {
      handleError(error);
    }
  }

  // to handle multiple click
  useEffect(() => {
    let obj = { ...manageItemClick };
    obj[SESSION] = false;
    obj[PROJECT] = false;
    setManageItemClick({});
  }, [files]);

  /**
   * to get project type
   */
  function getProjectType() {
    if (currentNifti) {
      return 'NIFTI';
    } else if (currentMetaImage) {
      return 'META_IMAGE';
    }
    return 'DICOM';
  }

  /**
   * to handle multiple click in folder level
   * @param {string} folderLevel -  current folder level
   */
  function handleMultipleClick(folderLevel, denyClick = true) {
    let folderStage = { ...manageItemClick };
    folderStage[folderLevel] = denyClick;
    setManageItemClick(folderStage);
  }

  /**
   * to check current folder level item is clicked
   * @param {string} folderLevel
   */
  function isClickable(folderLevel) {
    if (manageItemClick && manageItemClick[folderLevel]) {
      return true;
    }
    return false;
  }

  /**
   * to get session list
   * @param {string} subjectId
   */
  function getSessionBySubjectId(subjectId, skipStage = true) {
    try {
      listSessionsBySubjectId(subjectId).then(res => {
        if (res) {
          setSessions(res);
          handleMultipleClick(SUBJECTS, false);
          if (skipStage) {
            setStage(SESSION);
          }
          res.forEach(item => {
            getSessionAnalyses(item._id);
          });
        }
      });
    } catch (error) {
      handleError(error);
    }
  }

  function handleError(error) {
    UINotificationService.show({
      title: 'Flywheel Error',
      message: error.message,
      duration: 30 * 1000,
      position: 'topCenter',
      type: 'error',
    });
    log.error(error);
  }

  /**
   * to get session analyses list
   * @param {string} sessionId
   */
  function getSessionAnalyses(sessionId) {
    try {
      listSessionAnalyses(sessionId).then(res => {
        if (res) {
          let data = [...res];
          data = data.filter(item => {
            item.files = filterFiles(item.files);
            item.inputs = filterFiles(item.inputs);
            if (item.files.length > 0 || item.inputs.length > 0) {
              return item;
            }
          });
          setSessionAnalysesResponse(data);
        }
      });
    } catch (error) {
      handleError(error);
    }
  }

  /**
   *  to get subject list by project id
   * @param {string} projectId
   */
  function getSubjects(projectId) {
    try {
      listSubjects(projectId).then(res => {
        if (res) {
          setSubjects(res);
          setStage(SUBJECTS);
          handleMultipleClick(PROJECT, false);
          res.forEach(item => {
            getSubjectsAnalyses(item._id);
          });
        }
      });
    } catch (error) {
      handleError(error);
    }
  }

  /**
   * to get subject analyses list
   * @param {string} subjectId
   */
  function getSubjectsAnalyses(subjectId) {
    try {
      listSubjectAnalyses(subjectId).then(res => {
        if (res) {
          let data = [...res];
          data = data.filter(item => {
            item.files = filterFiles(item.files);
            item.inputs = filterFiles(item.inputs);
            if (item.files.length > 0 || item.inputs.length > 0) {
              return item;
            }
          });
          setSubjectAnalysesResponse(data);
        }
      });
    } catch (error) {
      handleError(error);
    }
  }

  /**
   * to get acquisition analyses list
   * @param {string} projectId
   */
  function getAcquisitionAnalyses(projectId) {
    try {
      getProjectJobs(projectId).then(res => {
        if (res) {
          const data = [...res];
          setAcquisitionAnalysisData(data);
        }
      });
    } catch (error) {
      handleError(error);
    }
  }

  function setAcquisitionAnalyses(acquisitionId) {
    const res = acquisitionAnalysisData.filter(item => {
      item.outputs = filterFiles(item.outputs);
      if (item.outputs.length > 0) {
        return (
          item.state === 'complete' &&
          item.parents.acquisition === acquisitionId
        );
      }
    });
    const data = [...res];
    setAcquisitionAnalysesResponse(data);
  }

  /**
   * to get acquisition list
   * @param {string} sessionId
   */
  function getAcquisitions(sessionId, skipStage = true) {
    try {
      listAcquisitions(sessionId).then(res => {
        if (res) {
          handleMultipleClick(SESSION, false);
          setAcquisitions(res);
          if (skipStage) {
            setStage(ACQUISITION);
          }
        }
      });
    } catch (error) {
      handleError(error);
    }
  }

  /**
   * to change the folder level and also clear the selected file info
   * - when click on back button
   * @param {string} stageType - folder level
   */
  function changeStage(stageType) {
    setStage(stageType);
    setSelectedFile(null);
    setTitle('');
    setManageItemClick({});
  }

  function backButtonClick(stage) {
    let stageType = PROJECT;
    if (stage === SUBJECTS_ANALYSES) {
      stageType = SESSION;
    } else if (stage === SESSION_ANALYSES) {
      stageType = ACQUISITION;
    } else if (stage === PROJECT_ANALYSES) {
      stageType = SUBJECTS;
    } else if (stage === ACQUISITION_ANALYSES) {
      stageType = FILES;
    } else {
      stageType = ACQUISITION;
    }
    changeStage(stageType);
  }

  /**
   * to get the title when click on back
   */
  function getTitle() {
    if (stage === SUBJECTS) {
      return project && project.label;
    } else if (stage === SESSION) {
      return getTitleObject(subjects, sessions, 'subject');
    } else if (stage === ACQUISITION) {
      return getTitleObject(sessions, acquisitions, 'session');
    }
    return '';
  }

  /**
   * to get title when click on previous(back button) folder level
   * @param {Array} parent
   * @param {Array} child
   * @param {string} key
   */
  function getTitleObject(parent, child, key) {
    const obj = parent.find(item => {
      return child.find(x => x.parents[key] === item._id);
    });
    if (obj) {
      return obj.label;
    }
    return '';
  }

  /**
   * to change current folder level to parent level (to go back)
   */
  const BackButton = () => {
    switch (stage) {
      case PROJECT:
        return <div></div>;
      case SUBJECTS:
        return (
          <div className="folder-back" onClick={() => changeStage(PROJECT)}>
            ..
          </div>
        );
      case SESSION:
        return (
          <div className="folder-back" onClick={() => changeStage(SUBJECTS)}>
            ..
          </div>
        );
      case ACQUISITION:
        return (
          <div onClick={() => changeStage(SESSION)} className="folder-back">
            ..
          </div>
        );
      case FILES:
        return (
          <div
            onClick={() => {
              backButtonClick(stage);
            }}
            className="folder-back"
          >
            ..
          </div>
        );
      case PROJECT_ANALYSES:
        return (
          <div
            onClick={() => {
              backButtonClick(stage);
              setInputFiles([]);
              setOutputFiles([]);
            }}
            className="folder-back"
          >
            ..
          </div>
        );
      case SESSION_ANALYSES:
        return (
          <div
            onClick={() => {
              backButtonClick(stage);
              setInputFiles([]);
              setOutputFiles([]);
            }}
            className="folder-back"
          >
            ..
          </div>
        );
      case SUBJECTS_ANALYSES:
        return (
          <div
            onClick={() => {
              backButtonClick(stage);
              setInputFiles([]);
              setOutputFiles([]);
            }}
            className="folder-back"
          >
            ..
          </div>
        );
      default:
        return null;
    }
  };

  /**
   * to set selected file
   * @param {object} fileDetails
   */
  function onSelectFile(fileDetails) {
    // On selecting the 'input'/'output' folder name, the file details will be missing
    const fileName = fileDetails?.name || '';
    if (
      fileName.includes('.nii') ||
      fileName.includes('.nii.gz') ||
      fileName.includes('.mhd') ||
      fileName.includes('.zip') ||
      fileName.includes('.dcm') ||
      fileName.includes('.dicom') ||
      fileDetails?.type?.includes('dicom')
    ) {
      setSelectedFile(fileDetails);
    }
  }

  const showMessage = (message, dialog = {}) => {
    const { show: showUserMessage = () => {} } = dialog;
    showUserMessage({
      message,
    });
  };

  /**
   * To add selected file as segmentation data in the store
   */
  async function onConfirm() {
    if (selectedFile) {
      const matchingFile = (segmentationData[activeReferenceUID] || []).find(
        segmentation =>
          segmentation.fileId === selectedFile._id ||
          segmentation.fileId === selectedFile.file_id
      );
      if (matchingFile) {
        showMessage(
          'The overlay file already applied, choose a different one',
          snackbarContext
        );
        return;
      }

      let fileName = selectedFile.name;
      let fileId = selectedFile.file_id;
      const modality =
        fileName.includes('.nii') ||
        fileName.includes('.nii.gz') ||
        fileName.includes('.mhd')
          ? 'SEG'
          : 'RTSTRUCT';
      if (modality === 'RTSTRUCT') {
        fileId = selectedFile._id;
        let rtAssociation = allAssociations.find(
          association => association.file_id === selectedFile._id
        );
        rtAssociation =
          rtAssociation ||
          (await getAcquisitionAssociation(
            selectedFile.parents.project,
            selectedFile.parents.acquisition
          ));
        if (rtAssociation) {
          dispatch(
            addSegmentationData({
              ...selectedFile,
              file_id: selectedFile._id,
              modality: modality,
              referenceUID: activeReferenceUID,
              studyInstanceUID: rtAssociation.study_uid,
              seriesInstanceUID: rtAssociation.series_uid,
            })
          );
        }
      } else {
        dispatch(
          addSegmentationData({
            ...selectedFile,
            modality: modality,
            referenceUID: activeReferenceUID,
            studyInstanceUID: fileName,
            seriesInstanceUID: '',
          })
        );
      }
      // For tracking performance
      if (evaluatePerformance) {
        const startEntry = {
          key: SEGMENTATION_LOAD,
          id: fileId,
          startTime: Date.now(),
        };
        performanceStart(startEntry);
      }
      setStage(SUBJECTS);
      setSelectedFile(null);
      if (props.contentProps.onClose) {
        props.contentProps.onClose();
      }
    }
  }

  const returnElement = (index, classList, name, item) => {
    return (
      <div
        key={index}
        className={classList}
        style={{
          background:
            selectedFile && item && selectedFile.name === item.name
              ? '#0089bb'
              : '',
        }}
        onClick={() => {
          onSelectFile(item);
          setTitle(name);
        }}
      >
        {name}
      </div>
    );
  };

  return (
    <SimpleDialog
      headerTitle={title ? title : getTitle()}
      rootClass="segment-file-chooser-dialog"
      onClose={props.contentProps.onClose}
      onConfirm={onConfirm}
      showFooterButtons={true}
      confirmButtonTitle={'Choose File'}
      enableConfirm={selectedFile ? true : false}
    >
      <div className="file-viewer">
        {BackButton()}
        {stage === PROJECT && project && project.label && (
          <div
            className="item-menu"
            onClick={() => {
              if (isClickable(PROJECT)) {
                return;
              }
              handleMultipleClick(PROJECT);
              getSubjects(project._id);
              setTitle(project.label);
            }}
          >
            {project.label}
          </div>
        )}
        {stage === SUBJECTS &&
          project &&
          project.analyses &&
          project.analyses.map((item, index) => {
            return (
              <div
                key={index}
                className="item-menu"
                onClick={() => {
                  if (isClickable(SUBJECTS)) {
                    return;
                  }
                  handleMultipleClick(SUBJECTS);
                  setFiles([]);
                  setInputFiles([...item.inputs]);
                  setOutputFiles([...item.files]);
                  setStage(PROJECT_ANALYSES);
                  setTitle(item.label);
                }}
              >
                {item.label}{' '}
              </div>
            );
          })}
        {stage === SUBJECTS &&
          subjects.map((item, index) => {
            return (
              <div
                key={index}
                className="item-menu"
                onClick={() => {
                  if (isClickable(SUBJECTS)) {
                    return;
                  }
                  setSessions([]);
                  setSubjectsAnalyses([]);
                  handleMultipleClick(SUBJECTS);
                  getSessionBySubjectId(item._id, true);
                  getSubjectsAnalyses(item._id);
                  setTitle(item.label);
                }}
              >
                {item.label}
              </div>
            );
          })}
        {stage === SESSION &&
          subjectsAnalyses.map((item, index) => {
            return (
              <div
                key={index}
                className="item-menu"
                onClick={() => {
                  if (isClickable(SESSION)) {
                    return;
                  }
                  handleMultipleClick(SESSION);
                  setFiles([]);
                  setInputFiles([...item.inputs]);
                  setOutputFiles([...item.files]);
                  setStage(SUBJECTS_ANALYSES);
                  setTitle(item.label);
                }}
              >
                {item.label}
              </div>
            );
          })}
        {stage === SESSION &&
          sessions.map((item, index) => {
            return (
              <div
                key={index}
                className="item-menu"
                onClick={() => {
                  if (isClickable(SESSION)) {
                    return;
                  }
                  handleMultipleClick(SESSION);
                  setAcquisitions([]);
                  setSessionAnalyses([]);
                  getAcquisitions(item._id, item.label);
                  getSessionAnalyses(item._id);
                  setTitle(item.label);
                }}
              >
                {item.label}
              </div>
            );
          })}
        {stage === ACQUISITION &&
          sessionAnalyses.map((item, index) => {
            return (
              <div
                key={index}
                className="item-menu"
                onClick={() => {
                  if (isClickable(ACQUISITION)) {
                    return;
                  }
                  handleMultipleClick(ACQUISITION);
                  setFiles([]);
                  setInputFiles([...item.inputs]);
                  setOutputFiles([...item.files]);
                  setStage(SESSION_ANALYSES);
                  setTitle(item.label);
                }}
              >
                {item.label}
              </div>
            );
          })}
        {stage === ACQUISITION &&
          acquisitions.map((item, index) => {
            return (
              <div
                key={index}
                className="item-menu"
                onClick={() => {
                  const newFiles = filterFiles(item.files);
                  setFiles(newFiles);
                  setStage(FILES);
                  setTitle(item.label);
                  setAcquisitionsAnalyses([]);
                  setAcquisitionAnalyses(item._id);
                }}
              >
                {item.label}
              </div>
            );
          })}
        {(stage === FILES ||
          stage === PROJECT_ANALYSES ||
          stage === SESSION_ANALYSES ||
          stage === SUBJECTS_ANALYSES ||
          stage === ACQUISITION_ANALYSES) &&
          files.map((item, index) => {
            return (
              <div
                style={{
                  background:
                    selectedFile && selectedFile.name == item.name
                      ? '#0089bb'
                      : '',
                }}
                key={index}
                className="item-menu"
                onClick={() => {
                  onSelectFile(item);
                  setTitle(item.name);
                }}
              >
                {item.name}
              </div>
            );
          })}

        {stage === FILES &&
          acquisitionsAnalyses.length > 0 &&
          acquisitionsAnalyses.inputs.length > 0 &&
          headerItem.map(() => {
            return returnElement(0, 'analysis-item-header', 'Input', null);
          })}
        {stage === FILES &&
          acquisitionsAnalyses.length > 0 &&
          acquisitionsAnalyses.inputs &&
          acquisitionsAnalyses.inputs.map((item, index) => {
            return returnElement(index, 'analysis-item', item.name, item);
          })}
        {stage === FILES &&
          acquisitionsAnalyses.length > 0 &&
          acquisitionsAnalyses.outputs.length > 0 &&
          headerItem.map(() => {
            return returnElement(0, 'analysis-item-header', 'Output', null);
          })}
        {stage === FILES &&
          acquisitionsAnalyses.length > 0 &&
          acquisitionsAnalyses.outputs &&
          acquisitionsAnalyses.outputs.map((item, index) => {
            return returnElement(index, 'analysis-item', item.name, item);
          })}

        {(stage === PROJECT_ANALYSES ||
          stage === SUBJECTS_ANALYSES ||
          stage === SESSION_ANALYSES) &&
          inputFiles.length > 0 &&
          headerItem.map(() => {
            return returnElement(0, 'analysis-item-header', 'Input', null);
          })}
        {(stage === PROJECT_ANALYSES ||
          stage === SUBJECTS_ANALYSES ||
          stage === SESSION_ANALYSES) &&
          inputFiles &&
          inputFiles.map((item, index) => {
            return returnElement(index, 'analysis-item', item.name, item);
          })}
        {(stage === PROJECT_ANALYSES ||
          stage === SUBJECTS_ANALYSES ||
          stage === SESSION_ANALYSES) &&
          outputFiles.length > 0 &&
          headerItem.map(() => {
            return returnElement(0, 'analysis-item-header', 'Output', null);
          })}
        {(stage === PROJECT_ANALYSES ||
          stage === SUBJECTS_ANALYSES ||
          stage === SESSION_ANALYSES) &&
          outputFiles &&
          outputFiles.map((item, index) => {
            return returnElement(index, 'analysis-item', item.name, item);
          })}
      </div>
    </SimpleDialog>
  );
}
