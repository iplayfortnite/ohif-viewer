import _ from 'lodash';

import store from '@ohif/viewer/src/store';
import {
  Utils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
const {
  selectActiveTool,
  selectAvailableLabels,
  selectedQuestion,
} = FlywheelCommonRedux.selectors;
const {
  getBSliceTools,
  getBSliceSettings,
  getActiveSliceInfo,
  hasLabelsLimitReached,
} = Utils;

function filterLabelsForCurrentAnswer(state) {
  let availableLabels = selectAvailableLabels(state);
  const bSliceSettings = getBSliceSettings();
  const sliceInfo = getActiveSliceInfo(state.viewports.activeViewportIndex);
  const question = selectedQuestion(state);
  const bSliceTools = getBSliceTools(
    question,
    bSliceSettings,
    sliceInfo?.sliceNumber
  );
  const filteredLabels = [];
  const currentQuestionAnswer = getCurrentQuestionAnswer();
  if (currentQuestionAnswer?.instructionSet?.length > 1) {
    // Tools should be enabled for multiple instructions of selected question,
    // However label popup should only contain labels available for the instructions that has selected active tool.
    const activeTool = selectActiveTool(store.getState());
    currentQuestionAnswer.instructionSet.forEach(instruction => {
      if ((bSliceTools || instruction.measurementTools).includes(activeTool)) {
        filteredLabels.push(
          ..._.intersection(
            availableLabels,
            instruction.requireMeasurements || []
          )
        );
      }
    });
    availableLabels = filteredLabels;
  }
  availableLabels = availableLabels?.filter?.(
    x =>
      !hasLabelsLimitReached(
        [x],
        question,
        currentQuestionAnswer,
        sliceInfo?.sliceNumber,
        bSliceSettings,
        question?.subFormAnnotationId
      )
  ) || [''];

  return availableLabels;
}

function getCurrentQuestionAnswer() {
  const state = store.getState();
  const currentQuestion = state.infusions?.currentSelectedQuestion;
  if (currentQuestion?.answer) {
    const answer = currentQuestion?.answer?.value || currentQuestion?.answer;
    return currentQuestion?.values?.find(x => x.value === answer);
  }
  return null;
}

export default filterLabelsForCurrentAnswer;
