import store from '@ohif/viewer/src/store';
import {
  Utils as FlywheelCommonUtils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import Redux from '../../../redux';
import { getAvailableLabels } from '../utils/StudyFormLabel.js';

const { selectSessionRead, selectSessionFileRead } = Redux.selectors;

const {
  isBSliceApplicable,
  getActiveSliceInfo,
  isCurrentFile,
} = FlywheelCommonUtils;
const { setAvailableLabels, setActiveTool } = FlywheelCommonRedux.actions;
const { allowedActiveTools } = FlywheelCommonRedux.selectors;

const updateStudyFormAvailableLabels = measurementLabels => {
  const state = store.getState();
  const projectConfig = state.flywheel.projectConfig;
  const bSlices = projectConfig?.bSlices;

  if (isBSliceApplicable(state)) {
    const activeViewportIndex = state.viewports.activeViewportIndex;
    const currentSlice = getActiveSliceInfo(activeViewportIndex);
    const sliceNo = currentSlice?.sliceNumber;
    if (bSlices) {
      const isBSlice = Object.keys(bSlices?.settings).includes(`${sliceNo}`);
      if (!isBSlice || !isStudyFormAnsweringBSlice(sliceNo)) {
        store.dispatch(setAvailableLabels(['']));
      } else {
        setAvailableToolsAndAvailableLabels(
          projectConfig,
          measurementLabels,
          sliceNo
        );
      }
    } else {
      setAvailableToolsAndAvailableLabels(
        projectConfig,
        measurementLabels,
        sliceNo
      );
    }
  }
};

const setAvailableToolsAndAvailableLabels = (
  projectConfig,
  measurementLabels,
  sliceNo
) => {
  const infusions = store.getState().infusions;
  const currentSelectedQuestion = infusions.currentSelectedQuestion;
  const answer = currentSelectedQuestion?.answer;
  const currentQuestionAnswer = answer?.value || answer;
  const values = currentSelectedQuestion?.values;
  let currentTools = [];
  const requiredLabels = new Set();
  let availableLabels = [];
  let availableTools = [];

  if (values?.length > 0) {
    const currentAnswer = values.find(v => v.value === currentQuestionAnswer);
    if (currentAnswer?.instructionSet?.length) {
      currentAnswer?.instructionSet.forEach(instruction => {
        currentTools.push(...instruction.measurementTools);
      });
      currentAnswer?.instructionSet?.forEach(instruction => {
        const labels = instruction.requireMeasurements || [];
        labels.forEach(requiredLabels.add, requiredLabels);
      });
    } else {
      currentTools.push(...(currentAnswer?.measurementTools || []));
      for (const measure of currentAnswer?.requireMeasurements || []) {
        requiredLabels.add(measure);
      }
    }
    availableLabels = getAvailableLabels(
      projectConfig,
      requiredLabels,
      measurementLabels,
      sliceNo,
      currentSelectedQuestion.question,
      currentSelectedQuestion.subFormName,
      currentSelectedQuestion.subFormAnnotationId,
      currentSelectedQuestion.isSubForm
    );

    availableTools = [...new Set([...currentTools, ...allowedActiveTools])];
    if (!availableLabels.length) {
      availableLabels.push('');
    }
    store.dispatch(setAvailableLabels(availableLabels, availableTools));
    store.dispatch(setActiveTool(availableTools[0]));
  }
};

const getSessionRead = () => {
  const state = store.getState();
  let sessionRead;

  if (isCurrentFile(state)) {
    if (state.flywheel.readerTaskWithFormResponse) {
      sessionRead = state.flywheel.readerTaskWithFormResponse.info;
    } else {
      sessionRead = selectSessionFileRead(state);
    }
  } else {
    if (state.flywheel.readerTaskWithFormResponse) {
      sessionRead = state.flywheel.readerTaskWithFormResponse.info;
    } else {
      sessionRead = selectSessionRead(state);
    }
  }
  return sessionRead;
};

const isStudyFormAnsweringBSlice = sliceNo => {
  const sessionRead = getSessionRead();
  const notes = sessionRead?.notes;
  const slice = notes?.slices?.[sliceNo];

  if (!slice) {
    return false;
  }
  const infusions = store.getState().infusions;
  const currentSelectedQuestion = infusions.currentSelectedQuestion;
  const key = currentSelectedQuestion?.key;
  const currentQuestionAnswer =
    currentSelectedQuestion?.answer?.value || currentSelectedQuestion?.answer;

  let currentSliceValue = currentSelectedQuestion?.isSubForm
    ? slice?.[currentSelectedQuestion?.question]?.value
    : slice?.[key]?.value || slice?.[key];

  if (currentSelectedQuestion?.isSubForm) {
    currentSliceValue = slice?.[currentSelectedQuestion?.question][
      currentSelectedQuestion?.subFormName
    ].find(
      item => item.annotationId === currentSelectedQuestion?.subFormAnnotationId
    )?.[currentSelectedQuestion?.questionKey];
  }

  return currentQuestionAnswer === currentSliceValue;
};

export { updateStudyFormAvailableLabels };
