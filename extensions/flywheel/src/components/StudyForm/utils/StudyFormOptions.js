const formatOptions = (options, DROPDOWN_DEFAULT_VALUE) => {
  let modifiedOptions = [
    { key: DROPDOWN_DEFAULT_VALUE, value: DROPDOWN_DEFAULT_VALUE },
  ].concat(options);
  modifiedOptions = modifiedOptions.reduce((previousValue, currentValue) => {
    let prevVal = currentValue;
    if (currentValue.label) {
      // Change the value format to key-value pair if not already
      prevVal = {
        key: currentValue.label,
        value: currentValue.value,
      };
    }
    previousValue.push(prevVal);
    return previousValue;
  }, []);
  return modifiedOptions;
};

export { formatOptions };
