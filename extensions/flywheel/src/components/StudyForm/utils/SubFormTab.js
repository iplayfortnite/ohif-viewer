import { Utils } from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import Redux from '../../../redux';

const { isCurrentFile } = Utils;
const { selectUser } = Redux.selectors;

const getSubFormTabDetails = (
  componentKeyList,
  projectConfig,
  measurementTools,
  measurements,
  studyFormState
) => {
  let tab = {
    annotationIdForQuestionKey: {},
    subFormTabList: {},
    subFormPanel: {},
    subFormTab: {},
    subFormTabWithAnnotationId: {},
  };
  const state = store.getState();
  const isFile = isCurrentFile(state);
  const user = selectUser(state);
  measurementTools.forEach(tool => {
    measurements?.[tool]?.forEach(annotation => {
      if (componentKeyList.includes(annotation?.questionKey)) {
        const answer = annotation?.answer?.value || annotation?.answer;
        const questionValue =
          !isFile && projectConfig.bSlices
            ? typeof studyFormState.notes.slices?.[annotation.sliceNumber]?.[
                annotation?.questionKey
              ] === 'object'
              ? studyFormState.notes.slices?.[annotation.sliceNumber]?.[
                  annotation?.questionKey
                ].value
              : studyFormState.notes.slices?.[annotation.sliceNumber]?.[
                  annotation?.questionKey
                ]
            : typeof studyFormState.notes[annotation?.questionKey] === 'object'
            ? studyFormState.notes[annotation?.questionKey].value
            : studyFormState.notes[annotation?.questionKey];
        const subFormName = projectConfig.studyForm?.components
          ?.find(item => item.key === annotation?.questionKey)
          ?.values?.find(value => value.value === questionValue)?.subForm;
        if (!isFile && projectConfig.bSlices) {
          if (
            (questionValue === answer &&
              annotation.flywheelOrigin?.id === user._id) ||
            (subFormName && annotation.flywheelOrigin?.id === user._id)
          ) {
            if (!tab.subFormTabList[annotation.sliceNumber]) {
              tab.subFormTabList[annotation.sliceNumber] = {};
            }
            if (
              tab.subFormTabList[annotation.sliceNumber]?.[
                annotation?.questionKey
              ]
            ) {
              tab.subFormTabList[annotation.sliceNumber][
                annotation?.questionKey
              ] += 1;
            } else {
              tab.subFormTabList[annotation.sliceNumber][
                annotation?.questionKey
              ] = 1;
            }

            if (!tab.subFormPanel[annotation.sliceNumber]) {
              tab.subFormPanel[annotation.sliceNumber] = {};
            }
            if (
              tab.subFormPanel[annotation.sliceNumber][
                annotation?.questionKey
              ] === undefined
            ) {
              if (
                studyFormState.subFormPanel[annotation.sliceNumber]?.[
                  annotation.questionKey
                ] === false ||
                studyFormState.subFormPanel[annotation.sliceNumber]?.[
                  annotation.questionKey
                ] === true
              ) {
                tab.subFormPanel[annotation.sliceNumber][
                  annotation.questionKey
                ] =
                  studyFormState.subFormPanel[annotation.sliceNumber]?.[
                    annotation.questionKey
                  ];
              } else {
                tab.subFormPanel[annotation.sliceNumber][
                  annotation.questionKey
                ] = true;
              }
            }

            if (!tab.subFormTab[annotation.sliceNumber]) {
              tab.subFormTab[annotation.sliceNumber] = {};
            }
            if (
              tab.subFormTab[annotation.sliceNumber][
                annotation?.questionKey
              ] === undefined
            ) {
              if (
                studyFormState.subFormTab[annotation.sliceNumber]?.[
                  annotation.questionKey
                ]
              ) {
                tab.subFormTab[annotation.sliceNumber][annotation.questionKey] =
                  studyFormState.subFormTab[annotation.sliceNumber][
                    annotation.questionKey
                  ];
              } else {
                tab.subFormTab[annotation.sliceNumber][annotation.questionKey] =
                  annotation.questionKey + '0';
              }
            }

            if (!tab.subFormTabWithAnnotationId[annotation.sliceNumber]) {
              tab.subFormTabWithAnnotationId[annotation.sliceNumber] = {};
            }
            if (
              tab.subFormTabWithAnnotationId[annotation.sliceNumber][
                annotation?.questionKey
              ] === undefined
            ) {
              if (
                studyFormState.subFormTabWithAnnotationId[
                  annotation.sliceNumber
                ]?.[annotation.questionKey]
              ) {
                tab.subFormTabWithAnnotationId[annotation.sliceNumber][
                  annotation.questionKey
                ] =
                  studyFormState.subFormTabWithAnnotationId[
                    annotation.sliceNumber
                  ][annotation.questionKey];
              } else {
                tab.subFormTabWithAnnotationId[annotation.sliceNumber][
                  annotation.questionKey
                ] = annotation?.uuid;
              }
            }

            if (!tab.annotationIdForQuestionKey[annotation.sliceNumber]) {
              tab.annotationIdForQuestionKey[annotation.sliceNumber] = {};
            }
            if (
              tab.annotationIdForQuestionKey[annotation.sliceNumber][
                annotation?.questionKey
              ] === undefined
            ) {
              tab.annotationIdForQuestionKey[annotation.sliceNumber][
                annotation?.questionKey
              ] = [];
            }
            tab.annotationIdForQuestionKey[annotation.sliceNumber][
              annotation?.questionKey
            ].push(annotation.uuid);
          }
        } else {
          if (
            (questionValue === answer &&
              annotation.flywheelOrigin?.id === user._id) ||
            (subFormName && annotation.flywheelOrigin?.id === user._id)
          ) {
            if (tab.subFormTabList?.[annotation?.questionKey]) {
              tab.subFormTabList[annotation?.questionKey] += 1;
            } else {
              tab.subFormTabList[annotation?.questionKey] = 1;
            }

            if (tab.subFormPanel[annotation?.questionKey] === undefined) {
              if (
                studyFormState.subFormPanel[annotation.questionKey] === false ||
                studyFormState.subFormPanel[annotation.questionKey] === true
              ) {
                tab.subFormPanel[annotation.questionKey] =
                  studyFormState.subFormPanel[annotation.questionKey];
              } else {
                tab.subFormPanel[annotation.questionKey] = true;
              }
            }

            if (tab.subFormTab[annotation?.questionKey] === undefined) {
              if (studyFormState.subFormTab[annotation.questionKey]) {
                tab.subFormTab[annotation.questionKey] =
                  studyFormState.subFormTab[annotation.questionKey];
              } else {
                tab.subFormTab[annotation.questionKey] =
                  annotation.questionKey + '0';
              }
            }

            if (
              tab.subFormTabWithAnnotationId[annotation?.questionKey] ===
              undefined
            ) {
              if (
                studyFormState.subFormTabWithAnnotationId[
                  annotation.questionKey
                ]
              ) {
                tab.subFormTabWithAnnotationId[annotation.questionKey] =
                  studyFormState.subFormTabWithAnnotationId[
                    annotation.questionKey
                  ];
              } else {
                tab.subFormTabWithAnnotationId[annotation.questionKey] =
                  annotation?.uuid;
              }
            }

            if (
              tab.annotationIdForQuestionKey[annotation?.questionKey] ===
              undefined
            ) {
              tab.annotationIdForQuestionKey[annotation?.questionKey] = [];
            }
            tab.annotationIdForQuestionKey[annotation?.questionKey].push(
              annotation.uuid
            );
          }
        }
      }
    });
  });
  return tab;
};

export { getSubFormTabDetails };
