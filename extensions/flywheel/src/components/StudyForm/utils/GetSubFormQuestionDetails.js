import { getDeletedMeasurementDetail } from './MeasurementDetails';

const getSubFormQuestionDetails = (state, visibleQuestions) => {
  const timePointManager = store.getState().timepointManager;
  let components,
    location,
    subFormName,
    subFormAnnotationId,
    question,
    isSubForm = false;

  if (!timePointManager.isMeasurementDeletedForStudyFormQuestion) {
    if (state.isSubFormQuestion) {
      components = visibleQuestions.find(
        component => component.subFormName === state.subFormName
      )?.components;
      isSubForm = true;
      question = state.subFormQuestion;
      subFormName = state.subFormName;
      subFormAnnotationId = state.subFormAnnotationId;
    }
  } else {
    const lastDeletedMeasurementId = timePointManager.lastDeletedMeasurementId;
    const deletedMeasurement = getDeletedMeasurementDetail(
      timePointManager,
      lastDeletedMeasurementId
    );
    if (deletedMeasurement.isSubForm) {
      components = visibleQuestions.find(
        component => component.subFormName === deletedMeasurement?.subFormName
      )?.components;
      isSubForm = true;
      question = deletedMeasurement?.question;
      subFormName = deletedMeasurement?.subFormName;
      subFormAnnotationId = deletedMeasurement?.subFormAnnotationId;
    } else {
      isSubForm = false;
      question = '';
      subFormName = '';
      subFormAnnotationId = '';
    }
    location = deletedMeasurement.location;
  }

  return {
    isSubForm,
    question,
    subFormName,
    subFormAnnotationId,
    location,
    components,
  };
};

export { getSubFormQuestionDetails };
