import store from '@ohif/viewer/src/store';
import cloneDeep from 'lodash.clonedeep';
import { Utils } from '@flywheel/extension-flywheel-common';

const filterFormResponse = formResponse => {
  const state = store.getState();
  const measurements = state.timepointManager.measurements;
  let subFormAnnotations = {};
  let subFormQuestions = [];
  const filteredFormResponse = cloneDeep(formResponse);
  if (Utils.isCurrentFile(state)) {
    Object.keys(measurements).forEach(toolType => {
      measurements[toolType].forEach(annotation => {
        if (
          annotation.isSubForm &&
          !subFormAnnotations[annotation.subFormAnnotationId]
        ) {
          subFormQuestions.push(annotation.question);
          subFormAnnotations[annotation.subFormAnnotationId] = true;
        }
      });
    });

    subFormQuestions.forEach(question => {
      Object.keys(formResponse[question]).forEach(item => {
        if (Array.isArray(formResponse[question][item])) {
          const filtered = formResponse[question][item].filter(x => {
            return subFormAnnotations[x.annotationId];
          });
          filteredFormResponse[question][item] = filtered;
        }
      });
    });

    Object.keys(formResponse).forEach(questionKey => {
      if (typeof formResponse[questionKey] === 'object') {
        if (!subFormQuestions.includes(questionKey)) {
          filteredFormResponse[questionKey] =
            filteredFormResponse[questionKey].value;
        }
      }
    });
  }

  return filteredFormResponse;
};

export { filterFormResponse };
