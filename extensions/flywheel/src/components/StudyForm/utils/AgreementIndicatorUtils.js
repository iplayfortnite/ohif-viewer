import { percentColorsConfig } from '../../CircleChart/CircleChartUtils.js';

function getPercentColor(options, index, total, value, colorConfig = []) {
  const percentColors = colorConfig?.length ? colorConfig : percentColorsConfig;
  const optionData = options.find(option => option.value === value);
  if (!optionData?.count) {
    return '';
  } else {
    const percent = getPercentage(optionData, total);
    const percentColor = percentColors.find(
      x => percent >= x.from && percent <= x.to
    );
    if (percentColor?.color) {
      return percentColor.color;
    }
  }
  return '';
}

const _getHighestVector = newPercentColors => {
  const options = getOptions(newPercentColors);
  if (options.length) {
    return options[0];
  }
  return null;
};

const getHighestPercentage = (total, newPercentColors) => {
  const option = _getHighestVector(newPercentColors);
  if (option) {
    const percentage = getPercentage(option, total);
    return percentage || 0;
  }
  return 0;
};

const getPercentage = (option, total) => {
  return option.count <= 1 ? 0 : Math.round((option.count / total) * 100);
};

const getOptions = newPercentColors => {
  return newPercentColors?.sort((a, b) => b.count - a.count);
};

const getInverted = (
  multipleTaskNotes,
  slice,
  question,
  taskCount,
  newPercentColors
) => {
  const allValue = [];
  let inverted = false;

  Object.keys(multipleTaskNotes)?.forEach(taskId => {
    let value = multipleTaskNotes[taskId].notes[0]?.slices
      ? multipleTaskNotes[taskId].notes[0]?.slices?.[slice]?.[question.key]
      : multipleTaskNotes[taskId].notes[0]?.[question.key];
    value = typeof value === 'object' ? value.value : value;

    if (value !== '' && value) {
      allValue.push(value);
    }
  });

  if (question?.tieBreaker?.type) {
    if (question.tieBreaker.type === 'simple') {
      const progress = getHighestPercentage(taskCount, newPercentColors);
      inverted = progress !== 100;
    } else if (question.tieBreaker.type === 'advanced') {
      const groups = question?.tieBreaker?.groups;
      const isTieBreaker = groups?.some(group => {
        const diff = _.xor(group, allValue);
        return !diff.length;
      });
      const advancedProgress = getHighestPercentage(
        taskCount,
        newPercentColors
      );

      inverted = advancedProgress !== 100 && !isTieBreaker;
    }
  }
  return inverted;
};

export {
  getPercentColor,
  getHighestPercentage,
  getPercentage,
  getOptions,
  getInverted,
};
