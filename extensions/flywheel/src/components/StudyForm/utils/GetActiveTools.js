import { Utils } from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
const { isCurrentFile } = Utils;

const getStudyFormActiveToolsForEnable = (
  projectConfig,
  props,
  subFormName = null,
  toolsWithLabel = null
) => {
  const state = store.getState();
  let studyFormComponents = [];
  if (subFormName) {
    studyFormComponents =
      projectConfig?.studyForm?.subForms?.[subFormName]?.components;
  } else {
    studyFormComponents = projectConfig?.studyForm?.components;
  }
  let studyFormQuestionTools = [];
  const labels = props.projectConfig?.labels;
  const slice = props.sliceInfo?.sliceNumber;
  const allMeasurements = props.measurements;
  studyFormComponents?.forEach(item => {
    if (item?.values?.length) {
      return item?.values?.forEach(data => {
        if (
          data?.requireMeasurements?.length &&
          data?.measurementTools?.length
        ) {
          data?.measurementTools?.forEach(tool => {
            data?.requireMeasurements?.forEach(label => {
              const selectedLabel = labels.find(data => data.value === label);
              let filteredMeasurements;
              if (!isCurrentFile(state) && projectConfig?.bSlices) {
                filteredMeasurements = allMeasurements[tool]?.filter(
                  measurement =>
                    measurement.sliceNumber === slice &&
                    measurement.location === label &&
                    measurement.toolType === tool
                );
              } else {
                filteredMeasurements = allMeasurements[tool]?.filter(
                  measurement =>
                    measurement.location === label &&
                    measurement.toolType === tool
                );
              }
              if (
                !filteredMeasurements?.length ||
                filteredMeasurements?.length < selectedLabel.limit
              ) {
                studyFormQuestionTools.push(tool);
                if (toolsWithLabel && Object.keys(toolsWithLabel).length) {
                  if (!toolsWithLabel[tool]) {
                    toolsWithLabel[tool] = [];
                    data?.requireMeasurements.forEach(label => {
                      toolsWithLabel[tool].push(label);
                    });
                  } else {
                    data?.requireMeasurements.forEach(label => {
                      if (!toolsWithLabel[tool].includes(label)) {
                        toolsWithLabel[tool].push(label);
                      }
                    });
                  }
                }
              }
            });
          });
        }
      });
    }
  });
  studyFormQuestionTools = [...new Set(studyFormQuestionTools)];
  const dragProbe = 'DragProbe';
  studyFormQuestionTools.push(dragProbe);
  return studyFormQuestionTools;
};

export { getStudyFormActiveToolsForEnable };
