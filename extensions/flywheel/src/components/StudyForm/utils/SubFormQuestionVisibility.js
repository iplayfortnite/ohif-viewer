import jsonLogic from 'json-logic-js';

const isQuestionDisplay = (
  subFormQuestion,
  annotationId,
  slice,
  question,
  projectConfig,
  stateNotes
) => {
  if (!subFormQuestion.conditional) {
    return true;
  } else {
    const notes = stateNotes;
    const value = notes.slices
      ? notes.slices?.[slice]?.[question.key]?.value
      : notes?.[question.key]?.value;
    const subFormName = projectConfig.studyForm.components
      .find(qs => qs.key === question.key)
      .values?.find(val => val.value === value)?.subForm;
    let notesForSubForm = notes.slices
      ? notes.slices?.[slice]?.[question.key]?.[subFormName]?.find(
          SubFormTabNotes => SubFormTabNotes.annotationId === annotationId
        )
      : notes?.[question.key]?.[subFormName]?.find(
          SubFormTabNotes => SubFormTabNotes.annotationId === annotationId
        );
    if (notesForSubForm === undefined) {
      notesForSubForm = {};
      return false;
    }
    const showQuestion = jsonLogic.apply(
      subFormQuestion.conditional.json,
      notesForSubForm
    );
    return showQuestion;
  }
};

export { isQuestionDisplay };
