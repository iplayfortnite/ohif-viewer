import {
  Utils as FlywheelCommonUtils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';

const { getBSliceSettings, hasLabelsLimitReached } = FlywheelCommonUtils;
const { selectedQuestion } = FlywheelCommonRedux.selectors;

const getAvailableLabels = (
  projectConfig,
  currentRequired,
  measurementLabels,
  slice,
  subFormQuestion,
  subFormName,
  subFormAnnotationId,
  isSubForm
) => {
  const state = store.getState();
  const question = selectedQuestion(state);
  const selectedOption = getSelectedOption(question);
  const bSliceSettings = getBSliceSettings();
  const availableLabels = projectConfig.labels
    .filter(label => {
      if (!currentRequired.has(label.value)) {
        return false;
      }
      return !hasLabelsLimitReached(
        [label.value],
        question,
        selectedOption,
        slice,
        bSliceSettings,
        subFormAnnotationId
      );
    })
    .map(label => label.value);
  return availableLabels;
};

function getSelectedOption(question) {
  if (question) {
    return question?.values?.find(
      x => x.value === (question?.answer?.value || question?.answer)
    );
  }
  return null;
}

export { getAvailableLabels };
