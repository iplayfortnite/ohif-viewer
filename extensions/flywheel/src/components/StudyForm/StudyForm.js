import { SimpleDialog, OverlayTrigger, Tooltip } from '@ohif/ui';
import OHIF from '@ohif/core';
import classnames from 'classnames';
import isEqual from 'lodash/isEqual';
import noop from 'lodash/noop';
import omit from 'lodash/omit';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import cornerstoneTools from 'cornerstone-tools';
import { withTranslation } from '@ohif/ui';
import { updateComponentByPromise } from '../utils';
import _ from 'lodash';
import Redux from '../../redux';
import {
  getDisplayProperties,
  applyDisplayProperties,
  check2dmprViewportPresent,
} from '../../utils';
import {
  Utils,
  Redux as FlywheelCommonRedux,
  HTTP as FlywheelCommonHTTP,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import ConnectedInstructionContainer from './ui/InstructionSet/ConnectedInstructionContainer.js';
import {
  validateWhileQuestionSwitches,
  validateForm,
  draftStudyForm,
  bSliceRequiredValidation,
  errorSetting,
  getQuestionsInNotes,
  getBSlices,
  sendTaskSignals,
  isFormViewOnly,
  isAnnotationsViewOnly,
  getMeasurementLabels,
  isOptionAvailable,
  bSliceQuestionValidation,
  getVisibleQuestions,
  isMeasurementTools,
} from './validation/ValidationEngine.js';
import {
  updateStudyFormNoteSubFormQuestionDeleted,
  setNotes,
  getValue,
  getValueSubForm,
} from './utils/StudyFormNotes.js';
import { getSubFormTabDetails } from './utils/SubFormTab.js';
import { getDeletedMeasurementDetail } from './utils/MeasurementDetails.js';
import { getSubFormQuestionDetails } from './utils/GetSubFormQuestionDetails';
import { getAvailableLabels } from './utils/StudyFormLabel.js';
import { isQuestionDisplay } from './utils/SubFormQuestionVisibility.js';
import {
  subFormEdit,
  updateAnnotationKeyAnswerAndNotes,
  updateAnnotationKeyAnswer,
  updateNoteInMixedOrROI,
  getQuestionAnswerAndTabDetails,
  getValueFromNoteAndSubFormName,
} from './utils/subFormEdit.js';
import { formatOptions } from './utils/StudyFormOptions.js';
import { clickColorPicker } from './utils/ColorPicker.js';
import { selectQuestionValues } from './utils/StudyFormQuestion.js';
import { getStudyFormActiveToolsForEnable } from './utils/GetActiveTools.js';
import { getAndSetBSliceValidatedInfo } from './utils/BSliceValidation.js';
import { getAvailableTools } from './utils/GetAvailableTools.js';
import { getToolsWithLabels } from './utils/GetToolsWithLabels.js';
import './StudyForm.styl';
import './SubForm.styl';
import ColorPickerDialog from '../ColorPickerDialog/ColorPickerDialog';
import { Header } from './ui/StudyFormComponents';
import ui from './ui/index.js';
import StudyFormQuestionConfirmationDialog from './dialog/StudyFormQuestionConfirmationDialog.js';
import SubFormDialog from './dialog/SubFormDialog.js';

const { customInfoKey } = FlywheelCommonHTTP.utils.REQUEST_UTILS;
const { shouldIgnoreUserSettings } = FlywheelCommonUtils;
const {
  StudyFormContent,
  SubFormPanel,
  SubFormTabHeader,
  SubFormTabContent,
} = ui;
const scrollToIndex = cornerstoneTools.import('util/scrollToIndex');

const {
  selectMeasurementsByStudyUid,
  selectSessionRead,
  selectProjectConfig,
  selectStudyList,
  selectSessionFileRead,
  selectUser,
  selectReaderTask,
  hasPermission,
  selectActiveSessions,
} = Redux.selectors;
const {
  setReaderTaskWithFormResponse,
  setReaderTask,
  setActiveSessions,
} = Redux.actions;
const {
  setActiveTool,
  setAvailableLabels,
  setActiveQuestion,
  updateRoiColors,
  updatePaletteColors,
  setMeasurementColorIntensity,
  subFormPopupVisible,
  setLabelForQuestion,
  subFormQuestionAnswered,
  studyFormAnswerInMixedMode,
  studyFormBSliceValidatedInfo,
  setStudyStateNote,
  setMultipleTaskNotes,
  setSubFormPopupClose,
  setWorkFlowTools,
  setPanelSwitched,
  setToolWhilePanelSwitch,
} = FlywheelCommonRedux.actions;
const {
  selectActiveTool,
  measurementTools,
  allowedActiveTools,
  segmentationTools,
} = FlywheelCommonRedux.selectors;
const measurementApi = OHIF.measurements.MeasurementApi;
const { setMeasurementDeleted } = OHIF.redux.actions;
const {
  getRouteParams,
  isCurrentFile,
  isCurrentZipFile,
  isBSliceApplicable,
  getCondensedProjectConfig,
  hasActiveToolLimitReached,
  getMeasurementToolsForAnswer,
  getBSliceSettings,
  getLabelsForTool,
  getBSliceTools,
  getToolsAndLabelsForQuestionAnswer,
  validateAllBSlice,
  validateBSlice,
  cornerstoneUtils,
} = Utils;
const readStatus = {
  Todo: 'Todo',
  Inprogress: 'In_progress',
  Complete: 'Complete',
};
const DROPDOWN_DEFAULT_VALUE = '--- SELECT ---';
const DEFAULT_COLOR = 'rgba(255, 255, 0, 0.2)';

class StudyForm extends Component {
  static propTypes = {
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    measurementLabels: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.string),
      PropTypes.object,
    ]),
    showStudyFormButtonsOnly: PropTypes.bool,
    projectConfig: PropTypes.shape({
      allowDraft: PropTypes.bool,
      resetTool: PropTypes.bool,
      showStudyList: PropTypes.bool,
      questions: PropTypes.arrayOf(
        PropTypes.shape({
          key: PropTypes.string.isRequired,
          options: PropTypes.arrayOf(
            PropTypes.shape({
              label: PropTypes.string,
              value: PropTypes.string.isRequired,
              excludeMeasurements: PropTypes.arrayOf(PropTypes.string),
              requireMeasurements: PropTypes.arrayOf(PropTypes.string),
              require: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
            })
          ),
          prompt: PropTypes.string.isRequired,
          required: PropTypes.bool,
          type: PropTypes.oneOf(['radio', 'text']).isRequired,
          when: PropTypes.object,
        })
      ),
      studyFormWorkflow: PropTypes.string,
      studyForm: PropTypes.shape({
        // https://github.com/formio/formio.js/wiki/Components-JSON-Schema
        components: PropTypes.arrayOf(
          PropTypes.shape({
            key: PropTypes.string.isRequired,
            label: PropTypes.string,
            defaultValue: PropTypes.string,
            values: PropTypes.arrayOf(
              PropTypes.shape({
                directive: PropTypes.string,
                instructionSet: PropTypes.array,
                label: PropTypes.string,
                value: PropTypes.string,
                excludeMeasurements: PropTypes.arrayOf(PropTypes.string),
                requireMeasurements: PropTypes.arrayOf(PropTypes.string),
              })
            ),
            validate: PropTypes.shape({
              required: PropTypes.bool,
              custom: PropTypes.object,
              minSelectedCount: PropTypes.number,
              maxSelectedCount: PropTypes.number,
            }),
            type: PropTypes.oneOf([
              'content',
              'radio',
              'text',
              'selectboxes',
              'textarea',
              'textfield',
              'dropdown',
            ]).isRequired,
            // This is for conditionally rendering this question.
            conditional: PropTypes.shape({
              // https://github.com/jwadhams/json-logic-js/
              json: PropTypes.object,
            }),
          })
        ),
        subForms: PropTypes.shape({
          subFormName: PropTypes.shape({
            label: PropTypes.string,
            components: PropTypes.arrayOf(
              PropTypes.shape({
                key: PropTypes.string.isRequired,
                label: PropTypes.string,
                defaultValue: PropTypes.string,
                values: PropTypes.arrayOf(
                  PropTypes.shape({
                    directive: PropTypes.string,
                    instructionSet: PropTypes.array,
                    label: PropTypes.string,
                    value: PropTypes.string,
                    excludeMeasurements: PropTypes.arrayOf(PropTypes.string),
                    requireMeasurements: PropTypes.arrayOf(PropTypes.string),
                  })
                ),
                validate: PropTypes.shape({
                  required: PropTypes.bool,
                  custom: PropTypes.object,
                  minSelectedCount: PropTypes.number,
                  maxSelectedCount: PropTypes.number,
                }),
                type: PropTypes.oneOf([
                  'content',
                  'radio',
                  'text',
                  'selectboxes',
                  'textarea',
                  'textfield',
                  'dropdown',
                ]).isRequired,
                // This is for conditionally rendering this question.
                conditional: PropTypes.shape({
                  // https://github.com/jwadhams/json-logic-js/
                  json: PropTypes.object,
                }),
              })
            ),
          }),
        }),
      }),
      bSlices: PropTypes.shape({
        settings: PropTypes.objectOf(
          PropTypes.shape({
            hiddenQuestions: PropTypes.arrayOf(PropTypes.string),
            measurementTools: PropTypes.objectOf(
              PropTypes.arrayOf(PropTypes.string)
            ),
          })
        ),
        required: PropTypes.shape({
          all: PropTypes.arrayOf(PropTypes.string),
          any: PropTypes.arrayOf(PropTypes.string),
          one: PropTypes.arrayOf(PropTypes.string),
          specific: PropTypes.objectOf(PropTypes.arrayOf(PropTypes.string)),
        }),
      }),
    }).isRequired,
    saveStudy: PropTypes.func.isRequired,
    showUserErrorMessage: PropTypes.func.isRequired,
    getReadTime: PropTypes.func.isRequired,
    sessionRead: PropTypes.shape({
      date: PropTypes.string, //isRequired, Todo enable when reader task supports saving date in info field
      notes: PropTypes.object,
      readOnly: PropTypes.bool,
      readStatus: PropTypes.string,
    }),
    setAvailableLabels: PropTypes.func.isRequired,
    StudyInstanceUID: PropTypes.string.isRequired,
    t: PropTypes.func.isRequired,
    servicesManager: PropTypes.object,
    commandsManager: PropTypes.object,
    sliceInfo: PropTypes.shape({
      imageId: PropTypes.string,
      sliceNumber: PropTypes.number,
      viewportIndex: PropTypes.number,
    }),
    setActiveTool: PropTypes.func,
  };

  state = {
    confirmCallback: null,
    errors: {},
    notes: {},
    readOnly: false,
    save: null,
    currentQuestion: null,
    previousQuestion: null,
    previousTool: 'Wwwc',
    isComplete: false,
    questionValidation: true,
    draftSave: null,
    submit: null,
    draftMessage: null,
    readerTaskStatus: null,
    readerTask: null,
    ownAnnotationPermission: false,
    hasEditAnnotationsOthersPermission: false,
    ownFormResponsesPermission: false,
    hasEditFormOthersPermission: false,
    formReadonly: false,
    currentSliceInfo: null,
    isStateUpdate: false,
    showInstructionColorPicker: {},
    selectedColor: DEFAULT_COLOR,
    showColorPicker: false,
    colorPickCoordinates: { left: 0, top: 0 },
    measurementData: null,
    currentType: null,
    currentKey: null,
    subFormPanel: {},
    subFormTab: {},
    subFormTabWithAnnotationId: {},
    subFormTabList: {},
    annotationIdForQuestionKey: {},
    isSubFormQuestion: false,
    subFormName: '',
    subFormQuestion: null,
    subFormAnnotationId: null,
    currentAvailableLabels: null,
    currentAvailableTools: null,
    currentTool: null,
    confirmationDialogRoiAnswer: [],
    answer: '',
    confirmationDialogMeasurementList: [],
    lastAnsweredValue: {},
    confirmationDialogCurrentQuestion: {},
    subFormDialogCurrentQuestion: {},
    subFormDialogMeasurementList: [],
    subFormDialogRoiAnswer: [],
    studyFormQuestionToolForSubForm: '',
  };

  previousVisibleQuestion = null;
  scrollableRef = null;
  questionRefs = {};
  roiQuestionDialogId = null;
  roiSubFormDialogId = null;
  toolsWithLabels = {};

  async getReaderTask() {
    const readerTask = await selectReaderTask(store.getState());
    if (readerTask) {
      this.setState({
        readerTaskStatus: readerTask.status,
      });
    }
  }

  componentDidMount() {
    this.getReaderTask();
    const { projectConfig, sessionRead } = this.props;
    const state = store.getState();
    const isPanelSwitched = state.infusions?.isPanelSwitched;
    if (sessionRead) {
      const notes = sessionRead.notes;
      const currentSelectedQuestion = state.infusions?.currentSelectedQuestion;
      if (isPanelSwitched && currentSelectedQuestion) {
        this.setState(prevState => ({
          ...prevState,
          notes: { ...notes } || {},
          readOnly: sessionRead.readOnly || false,
          isSubFormQuestion: currentSelectedQuestion.isSubForm,
          subFormName: currentSelectedQuestion.subFormName,
          subFormQuestion: currentSelectedQuestion.question,
          subFormAnnotationId: currentSelectedQuestion.subFormAnnotationId,
        }));
      } else {
        this.setState(prevState => ({
          ...prevState,
          notes: { ...notes } || {},
          readOnly: sessionRead.readOnly || false,
        }));
      }

      const confirmationDialogData = state.infusions?.confirmationDialogData;
      const subFormDialogData = state.infusions?.subFormDialogData;
      this.setState(prevState => ({
        ...prevState,
        confirmationDialogRoiAnswer:
          confirmationDialogData.confirmationDialogRoiAnswer,
        confirmationDialogMeasurementList:
          confirmationDialogData.confirmationDialogMeasurementList,
        confirmationDialogCurrentQuestion:
          confirmationDialogData.currentQuestion,
        subFormDialogCurrentQuestion:
          subFormDialogData.subFormDialogCurrentQuestion,
        subFormDialogRoiAnswer: subFormDialogData.subFormDialogRoiAnswer,
        subFormDialogMeasurementList:
          subFormDialogData.subFormDialogMeasurementList,
      }));

      if (projectConfig.studyFormWorkflow === 'Form') {
        const componentKeyList = projectConfig.studyForm?.components?.map(
          component => component.key
        );
        const tab = getSubFormTabDetails(
          componentKeyList,
          projectConfig,
          measurementTools,
          this.props.measurements,
          this.state
        );
        this.setState(prevState => ({
          ...prevState,
          subFormTabList: { ...tab.subFormTabList },
          subFormPanel: { ...tab.subFormPanel },
          subFormTab: { ...tab.subFormTab },
          annotationIdForQuestionKey: { ...tab.annotationIdForQuestionKey },
        }));
      }
    }
    const currentSelectedQuestion = state.infusions.currentSelectedQuestion;
    if (currentSelectedQuestion && !isPanelSwitched) {
      const option = this.getCurrentOption(currentSelectedQuestion);
      this.clickType(
        option,
        currentSelectedQuestion.questionKey,
        currentSelectedQuestion.question,
        currentSelectedQuestion.subFormName,
        currentSelectedQuestion.subFormAnnotationId,
        false
      );
    }

    if (!isPanelSwitched) {
      this.setStudyLabels();
    }

    if (projectConfig.resetTool && !isPanelSwitched) {
      this.props.setActiveTool('Wwwc');
    }

    this.setReaderTaskAndPermissions();

    if (
      !isCurrentFile(state) &&
      projectConfig.bSlices?.settings &&
      !isPanelSwitched
    ) {
      this.props.setAvailableLabels([], []);
      this.props.setActiveTool();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      measurementLabels,
      projectConfig,
      sessionRead,
      StudyInstanceUID,
      sliceInfo,
    } = this.props;
    let slice = 1;
    const state = store.getState();
    const isPanelSwitched = state.infusions?.isPanelSwitched;
    const isFile = isCurrentFile(state);
    const timePointManager = state.timepointManager;
    const { UIDialogService } = this.props.servicesManager.services;
    const isStudyFormContainsMeasurementTools = projectConfig?.studyForm?.components?.some(
      isMeasurementTools
    );
    const sliceSettings = projectConfig.bSlices?.settings;
    const availableToolsFromStore = state.infusions.availableTools;
    const diff = _.xor(allowedActiveTools, availableToolsFromStore);
    const isSubFormEnabled =
      this.roiQuestionDialogId || this.roiSubFormDialogId ? true : false;
    const toolsWithLabel = getToolsWithLabels(
      projectConfig.studyForm.components,
      isSubFormEnabled
    );

    if (!Object.keys(this.toolsWithLabels).length) {
      this.toolsWithLabels = toolsWithLabel;
    }

    const isSwitchedToDifferentSlice = () => {
      return (
        this.props.sliceInfo?.sliceNumber !== prevProps.sliceInfo?.sliceNumber
      );
    };
    if (
      sliceSettings &&
      state.infusions.currentSelectedQuestion?.subFormName &&
      !isFile &&
      isSwitchedToDifferentSlice()
    ) {
      store.dispatch(setActiveQuestion(null));
    }

    if (isPanelSwitched) {
      const currentSelectedQuestion = state.infusions?.currentSelectedQuestion;

      if (
        currentSelectedQuestion?.isSubForm &&
        state.infusions?.subFormDialog
      ) {
        const qKey = currentSelectedQuestion.question;
        const question = projectConfig.studyForm?.components?.find(
          component => component.key === qKey
        );
        const answerData =
          currentSelectedQuestion.answer?.value ||
          currentSelectedQuestion.answer;
        const lastAnsweredValue = question?.values?.find(
          value => value.value === answerData
        );
        const allMeasurements = timePointManager.measurements;
        const measurementList = [];

        if (lastAnsweredValue) {
          Object.keys(allMeasurements).forEach(key => {
            const annotation = allMeasurements[key]?.find(
              keyAnnotation =>
                keyAnnotation.uuid ===
                currentSelectedQuestion?.subFormAnnotationId
            );
            if (annotation) {
              measurementList.push(annotation);
            }
          });
        }

        if (measurementList.length > 0 && currentSelectedQuestion) {
          this.addOREditSubForm(
            projectConfig,
            question,
            measurementList,
            lastAnsweredValue,
            this.state.notes
          );
        }
      } else if (!currentSelectedQuestion && state.infusions?.subFormDialog) {
        const subFormDialogData = state.infusions?.subFormDialogData;
        this.addOREditSubForm(
          projectConfig,
          subFormDialogData.subFormDialogCurrentQuestion,
          subFormDialogData.subFormDialogMeasurementList,
          subFormDialogData.subFormDialogRoiAnswer,
          this.state.notes
        );
      }

      if (state.infusions?.confirmationDialog) {
        const confirmationDialogData = state.infusions?.confirmationDialogData;
        const studyFormNotes = this.state.notes;
        this.confirmationDialogShow(
          confirmationDialogData.currentQuestion,
          confirmationDialogData.confirmationDialogMeasurementList,
          this.props.projectConfig,
          confirmationDialogData.lastAnsweredValue,
          confirmationDialogData.sliceInfo.sliceNumber,
          confirmationDialogData.confirmationDialogRoiAnswer,
          studyFormNotes,
          confirmationDialogData.sliceInfo,
          confirmationDialogData.answer
        );
      }

      const panel = {
        isPanelSwitched: false,
        confirmationDialog: false,
        subFormDialog: false,
        confirmationDialogData: state.infusions?.confirmationDialogData,
        subFormDialogData: state.infusions?.subFormDialogData,
      };
      store.dispatch(setPanelSwitched(panel));
    }

    if (timePointManager.isMeasurementDeletedForStudyFormQuestion) {
      const lastDeletedMeasurementId =
        timePointManager.lastDeletedMeasurementId;
      const deletedMeasurement = getDeletedMeasurementDetail(
        timePointManager,
        lastDeletedMeasurementId
      );

      if (this.roiSubFormDialogId) {
        UIDialogService.dismiss({ id: this.roiSubFormDialogId });
        this.roiSubFormDialogId = null;
        store.dispatch(subFormPopupVisible(false));

        if (
          projectConfig?.studyFormWorkflow === 'ROI' &&
          isStudyFormContainsMeasurementTools
        ) {
          const studyFormQuestionTools = getStudyFormActiveToolsForEnable(
            projectConfig,
            this.props,
            null,
            this.toolsWithLabels
          );
          const label = studyFormQuestionTools?.length ? [''] : null;
          let tools = studyFormQuestionTools?.length
            ? studyFormQuestionTools
            : null;
          tools = getAvailableTools(
            tools,
            projectConfig,
            this.toolsWithLabels,
            this.props.measurementLabels,
            slice
          );
          this.props.setAvailableLabels(label, tools);
        }
      }

      if (
        !deletedMeasurement.isSubForm && projectConfig.bSlices && !isFile
          ? this.state.annotationIdForQuestionKey[
              deletedMeasurement?.sliceNumber
            ]?.[deletedMeasurement?.questionKey]?.includes(
              lastDeletedMeasurementId
            )
          : this.state.annotationIdForQuestionKey[
              deletedMeasurement?.questionKey
            ]?.includes(lastDeletedMeasurementId)
      ) {
        if (
          (projectConfig.studyFormWorkflow === 'ROI' ||
            projectConfig.studyFormWorkflow === 'Mixed') &&
          isStudyFormContainsMeasurementTools
        ) {
          let questionTools = getStudyFormActiveToolsForEnable(
            projectConfig,
            this.props,
            null,
            this.toolsWithLabels
          );
          questionTools = getAvailableTools(
            questionTools,
            projectConfig,
            this.toolsWithLabels,
            this.props.measurementLabels,
            slice
          );
          this.props.setAvailableLabels([''], questionTools);
        }

        let studyNotes = JSON.parse(JSON.stringify(this.state.notes));
        studyNotes = updateStudyFormNoteSubFormQuestionDeleted(
          projectConfig,
          deletedMeasurement,
          lastDeletedMeasurementId,
          studyNotes
        );
        const componentKeyListDelete = projectConfig.studyForm?.components?.map(
          component => component.key
        );
        const tabDelete = getSubFormTabDetails(
          componentKeyListDelete,
          projectConfig,
          measurementTools,
          this.props.measurements,
          this.state
        );
        if (this.props !== prevProps) {
          this.setState(prevState => ({
            ...prevState,
            notes: { ...studyNotes },
            subFormTabList: { ...tabDelete.subFormTabList },
            subFormPanel: { ...tabDelete.subFormPanel },
            subFormTab: { ...tabDelete.subFormTab },
            subFormTabWithAnnotationId: {
              ...tabDelete.subFormTabWithAnnotationId,
            },
            annotationIdForQuestionKey: {
              ...tabDelete.annotationIdForQuestionKey,
            },
          }));
        }
      }
    }
    if (
      sessionRead?.displayProperties &&
      !prevProps.sessionRead?.displayProperties
    ) {
      const shouldIgnore = shouldIgnoreUserSettings(state);
      if (!shouldIgnore) {
        applyDisplayProperties(
          this.props.commandsManager,
          state,
          sessionRead?.displayProperties
        );
      }
      if (state.flywheel.readerTaskWithFormResponse) {
        const readerTaskWithFormResponse = _.cloneDeep(
          state.flywheel.readerTaskWithFormResponse
        );
        if (!shouldIgnore) {
          delete readerTaskWithFormResponse.info.displayProperties;
        }
        store.dispatch(
          setReaderTaskWithFormResponse(readerTaskWithFormResponse)
        );
      }

      if (this.state.readerTask) {
        const readerTask = _.cloneDeep(this.state.readerTask);
        if (readerTask?.info?.displayProperties) {
          if (!shouldIgnore) {
            delete readerTask.info.displayProperties;
          }
          store.dispatch(setReaderTask(Promise.resolve(readerTask)));
        }
      }
    }

    if (
      !isFile &&
      this.state.currentSliceInfo &&
      this.state.currentSliceInfo?.imageId !== sliceInfo?.imageId
    ) {
      if (this.roiQuestionDialogId) {
        UIDialogService.dismiss({ id: this.roiQuestionDialogId });
        this.roiQuestionDialogId = null;
      } else if (this.roiSubFormDialogId) {
        UIDialogService.dismiss({ id: this.roiSubFormDialogId });
        this.roiSubFormDialogId = null;
      }
      store.dispatch(subFormPopupVisible(false));
    }

    if (
      !this.state.currentSliceInfo ||
      this.state.currentSliceInfo.imageId !== sliceInfo.imageId
    ) {
      const sliceNumber = sliceInfo?.sliceNumber;
      if (sliceNumber > 0) {
        this.setState(
          {
            currentSliceInfo: {
              imageId: sliceInfo.imageId,
              sliceNumber,
              viewportIndex: sliceInfo.viewportIndex,
            },
          },
          this.setStudyLabels
        );
      }
      slice = sliceNumber;
    } else {
      slice = this.state.currentSliceInfo.sliceNumber;
    }

    if (
      !this.state.currentSliceInfo ||
      this.state.currentSliceInfo.imageId !== sliceInfo.imageId ||
      (projectConfig.bSlices &&
        !sliceSettings[sliceInfo?.sliceNumber] &&
        diff.length !== 0) ||
      (this.state.currentTool != prevState.currentTool &&
        !state.subForm.isSubFormPopupEnabled)
    ) {
      const sliceNumber = sliceInfo?.sliceNumber;

      if (sliceNumber > 0) {
        if (
          ((!this.state.currentSliceInfo ||
            this.state.currentSliceInfo.imageId !== sliceInfo.imageId ||
            this.state.currentSliceInfo.viewportIndex ===
              sliceInfo.viewportIndex) &&
            !state.flywheel.currentNiftis?.length &&
            !state.flywheel.currentMetaImage &&
            isStudyFormContainsMeasurementTools) ||
          ((projectConfig?.studyFormWorkflow === 'ROI' ||
            projectConfig?.studyFormWorkflow === 'Mixed') &&
            isStudyFormContainsMeasurementTools)
        ) {
          const availableTools = this.state.currentAvailableTools;
          const availableLabels = this.state.currentAvailableLabels;
          let activeTool = this.state.previousTool;
          const infusions = state.infusions;
          const infusionActiveTool = infusions.activeTool;
          const { readOnly } = this.state;
          const measurementTools = [
            'Length',
            'Parallel',
            'ArrowAnnotate',
            'Angle',
            'RectangleRoi',
            'EllipticalRoi',
            'CircleRoi',
            'FreehandRoi',
            'OpenFreehandRoi',
            'Bidirectional',
            'ContourRoi',
          ];
          const isToolContain = infusions.availableTools?.some(tool =>
            measurementTools.includes(tool)
          );
          if (readOnly) {
            if (isToolContain) {
              this.props.setAvailableLabels([''], ['DragProbe']);
            }
          } else if (
            projectConfig?.studyFormWorkflow === 'ROI' ||
            projectConfig?.studyFormWorkflow === 'Mixed'
          ) {
            let questionToolsList = getStudyFormActiveToolsForEnable(
              projectConfig,
              this.props,
              null,
              this.toolsWithLabels
            );

            if (
              (!projectConfig?.bSlices &&
                !projectConfig?.studyForm?.components) ||
              (projectConfig?.bSlices?.settings?.[sliceNumber] &&
                !questionToolsList?.length &&
                !isFile) ||
              (isFile &&
                projectConfig?.studyForm?.components?.length &&
                !questionToolsList?.length)
            ) {
              this.props.setAvailableLabels(null, null);
            } else if (
              (!projectConfig?.bSlices &&
                projectConfig?.studyForm?.components) ||
              (projectConfig?.bSlices?.settings?.[sliceNumber] &&
                questionToolsList?.length &&
                !isFile) ||
              (isFile &&
                projectConfig?.studyForm?.components?.length &&
                questionToolsList?.length)
            ) {
              questionToolsList = getAvailableTools(
                questionToolsList,
                projectConfig,
                this.toolsWithLabels,
                this.props.measurementLabels,
                slice
              );
              this.props.setAvailableLabels([''], questionToolsList);
            } else if (!infusions.availableTools?.includes('DragProbe')) {
              this.props.setAvailableLabels([''], ['DragProbe']);
            }
          } else if (!segmentationTools?.includes(infusionActiveTool)) {
            if (infusions.currentSelectedQuestion === null) {
              this.props.setAvailableLabels([''], allowedActiveTools);
            } else if (
              !projectConfig.bSlices &&
              (!availableTools || !availableLabels)
            ) {
              this.props.setAvailableLabels([''], allowedActiveTools);
              this.props.setActiveTool(this.state.previousTool);
            } else if (!projectConfig.bSlices && availableLabels) {
              this.props.setAvailableLabels(
                availableLabels,
                availableTools.size > 0 ? Array.from(availableTools) : null
              );
              const activeTool = this.getActiveTool();
              this.props.setActiveTool(activeTool);
            } else if (
              projectConfig.bSlices &&
              infusions.currentSelectedQuestion !== null
            ) {
              this.setToolsForBSliceConfig(projectConfig, sliceInfo);
            } else {
              this.props.setAvailableLabels([''], allowedActiveTools);
              this.props.setActiveTool(this.state.previousTool);
            }
          } else if (projectConfig?.studyFormWorkflow === 'Form') {
            this.props.setAvailableLabels([''], ['DragProbe']);
            activeTool = infusionActiveTool;
          } else {
            if (
              this.state.currentSliceInfo?.sliceNumber !=
              this.props.sliceInfo?.sliceNumber
            ) {
              this.props.setAvailableLabels([''], infusions.availableTools);
            }
            this.props.setActiveTool(infusionActiveTool);
          }
        }
      }
    }

    const singleAvatar = state.viewerAvatar.singleAvatar;
    const isStudyStateNoteUpdate = state.viewerAvatar.isStudyStateNoteUpdate;
    const singleTaskId = state.viewerAvatar.taskId;
    const multipleTaskNotes = { ...state.viewerAvatar.multipleTaskNotes };
    const isMultipleReaderTask = state.multipleReaderTask?.TaskIds?.length > 1;

    if (
      !this.state.isStateUpdate &&
      singleAvatar &&
      isStudyStateNoteUpdate &&
      isMultipleReaderTask
    ) {
      const note = { ...state.flywheel.formResponse?.response_data };
      this.setState({ notes: { ...note } });
      store.dispatch(setStudyStateNote(false));
    }

    if (
      !this.state.isStateUpdate &&
      !singleAvatar &&
      isStudyStateNoteUpdate &&
      isMultipleReaderTask
    ) {
      const note = {
        ...state.flywheel.readerTaskWithFormResponse?.info?.notes,
      };
      this.setState({ notes: { ...note } });
      store.dispatch(setStudyStateNote(false));
    }

    if (
      !this.state.isStateUpdate &&
      singleAvatar &&
      isMultipleReaderTask &&
      !isEqual(multipleTaskNotes[singleTaskId].notes[0], this.state.notes)
    ) {
      let noteForAvatar;
      if (isStudyStateNoteUpdate) {
        noteForAvatar = { ...state.flywheel.formResponse?.response_data };
      } else {
        noteForAvatar = this.state.notes;
      }
      multipleTaskNotes[singleTaskId].notes[0] = noteForAvatar;
      store.dispatch(setMultipleTaskNotes(multipleTaskNotes));
    }

    if (
      (StudyInstanceUID !== prevProps.StudyInstanceUID ||
        !isEqual(sessionRead, prevProps.sessionRead)) &&
      !this.state.isStateUpdate
    ) {
      let sessionReadNotes = (sessionRead && sessionRead.notes) || {};
      const readOnly =
        (sessionRead && sessionRead.readOnly) || this.state.readOnly;
      if (!isFile && projectConfig.bSlices && this.state.notes.slices) {
        const bSlices = getBSlices(projectConfig);
        for (const sliceNo of bSlices) {
          if (sessionReadNotes.slices) {
            sessionReadNotes.slices[sliceNo] = {
              ...sessionReadNotes.slices?.[sliceNo],
              ...this.state.notes.slices?.[sliceNo],
            };
          }
        }
      } else {
        sessionReadNotes = {
          ...sessionReadNotes,
          ...this.state.notes,
        };
      }
      this.setState({ notes: { ...sessionReadNotes }, readOnly, errors: {} });
      if (
        this.scrollableRef &&
        StudyInstanceUID !== prevProps.StudyInstanceUID
      ) {
        this.scrollableRef.scrollTop = 0;
      }
    }
    if (measurementLabels !== prevProps.measurementLabels) {
      let notes = {};
      const props = {
        ...this.props,
        previousVisibleQuestion: this.state.previousQuestion,
        sliceInfo: this.props.sliceInfo,
      };
      const visibleQuestions = getVisibleQuestions(this.state, props);
      for (const question of visibleQuestions) {
        if (question.values) {
          const currentValue = this.state.notes.slices
            ? this.state.notes.slices[slice]
              ? this.state.notes.slices[slice][question.key]
              : undefined
            : this.state.notes[question.key];
          const validOptions = question.values.filter(opt =>
            isOptionAvailable(
              opt,
              measurementLabels,
              projectConfig,
              slice,
              false,
              this.state
            )
          );

          if (
            typeof currentValue === 'object' &&
            !validOptions.some(opt => {
              if (Array.isArray(currentValue.value)) {
                return currentValue.value.includes(opt.value);
              }
              return opt.value === currentValue.value;
            })
          ) {
            notes = setNotes(
              validOptions,
              state,
              projectConfig,
              notes,
              slice,
              question,
              currentValue
            );
          } else if (
            typeof currentValue !== 'object' &&
            !validOptions.some(opt => {
              if (Array.isArray(currentValue)) {
                return currentValue.includes(opt.value);
              }
              return opt.value === currentValue;
            })
          ) {
            notes = setNotes(
              validOptions,
              state,
              projectConfig,
              notes,
              slice,
              question,
              currentValue
            );
          }
        }
      }
      if (Object.keys(notes).length > 0) {
        if (!isFile && projectConfig.bSlices) {
          const stateNotes =
            this.state.notes.slices && notes.slices
              ? {
                  ...this.state.notes.slices[slice],
                  ...notes.slices[slice],
                }
              : this.state.notes.slices
              ? {
                  ...this.state.notes.slices[slice],
                }
              : notes.slices
              ? {
                  ...notes.slices[slice],
                }
              : {};
          let allNotes = { ...this.state.notes };
          if (!allNotes.slices) {
            allNotes.slices = {};
          }
          allNotes.slices[slice] = stateNotes;
          this.setState({ notes: { ...allNotes } });
        } else {
          this.setState(state => ({ notes: { ...state.notes, ...notes } }));
        }
      }
    }
    if (
      !isEqual(this.state.notes, prevState.notes) ||
      measurementLabels !== prevProps.measurementLabels
    ) {
      const componentKeyList = projectConfig.studyForm?.components?.map(
        component => component.key
      );
      const tab = getSubFormTabDetails(
        componentKeyList,
        projectConfig,
        measurementTools,
        this.props.measurements,
        this.state
      );
      this.setState(prevState => ({
        ...prevState,
        subFormTabList: { ...tab.subFormTabList },
        subFormPanel: { ...tab.subFormPanel },
        subFormTab: { ...tab.subFormTab },
        subFormTabWithAnnotationId: { ...tab.subFormTabWithAnnotationId },
        annotationIdForQuestionKey: { ...tab.annotationIdForQuestionKey },
      }));
    }

    if (state.infusions.currentSelectedQuestion) {
      this.restoreCurrentQuestion(state);
    }

    if (
      !isEqual(this.state.notes, prevState.notes) ||
      measurementLabels !== prevProps.measurementLabels
    ) {
      this.setStudyLabels();
    }

    if (
      StudyInstanceUID !== prevProps.StudyInstanceUID &&
      projectConfig.resetTool
    ) {
      this.props.setActiveTool('Wwwc');
    }

    if (
      state.subForm?.isSubFormPopupCloseFromOverlappingDelete ||
      state.subForm?.isSubFormPopupCloseFromOverlappingAutoAdjust
    ) {
      const data = { delete: false, autoAdjust: false };
      store.dispatch(setSubFormPopupClose(data));
      if (state.subForm?.isSubFormPopupCloseFromOverlappingDelete) {
        if (this.roiQuestionDialogId) {
          UIDialogService.dismiss({ id: this.roiQuestionDialogId });
          this.roiQuestionDialogId = null;
        } else if (this.roiSubFormDialogId) {
          UIDialogService.dismiss({ id: this.roiSubFormDialogId });
          this.roiSubFormDialogId = null;
        }
        const formQuestionTools = getStudyFormActiveToolsForEnable(
          projectConfig,
          this.props,
          null,
          this.toolsWithLabels
        );
        const label = formQuestionTools?.length ? [''] : null;
        let toolsList = formQuestionTools?.length ? formQuestionTools : null;
        toolsList = getAvailableTools(
          toolsList,
          projectConfig,
          this.toolsWithLabels,
          measurementLabels,
          slice
        );
        this.props.setAvailableLabels(label, toolsList);
      }
    }

    if (
      (projectConfig?.studyFormWorkflow === 'ROI' ||
        projectConfig?.studyFormWorkflow === 'Mixed') &&
      state.subForm?.isROIEdit
    ) {
      const edit = subFormEdit(
        state,
        projectConfig,
        this.props,
        state.subForm?.measurement?.uuid
      );
      this.addOREditSubForm(
        projectConfig,
        edit.roiSelectedQuestion,
        [edit.measurementEdit],
        edit.lastAnsweredValue
      );
    }
    if (
      (projectConfig?.studyFormWorkflow === 'ROI' ||
        projectConfig?.studyFormWorkflow === 'Mixed') &&
      state.ui?.labelling?.measurementData &&
      !state.ui?.labelling?.editLabelDescriptionOnDialog &&
      !timePointManager.isMeasurementDeletedForStudyFormQuestion
    ) {
      const measurementData = state.ui.labelling?.measurementData;
      const isInTimePointManager = state.timepointManager?.measurements?.[
        measurementData.toolType
      ]?.find(data => data._id === measurementData._id);
      if (
        measurementData.location &&
        measurementData.toolType &&
        isInTimePointManager
      ) {
        const roiQuestionList = projectConfig.studyForm?.components?.filter(
          item => {
            return item?.values?.some(
              value =>
                value.measurementTools?.includes(measurementData.toolType) &&
                value.requireMeasurements?.includes(measurementData.location)
            )
              ? item
              : null;
          }
        );
        let roiQuestion = [];
        if (roiQuestionList.length) {
          roiQuestion = roiQuestionList[0];
          const roiAnswer = roiQuestion.values.filter(
            value =>
              value.measurementTools?.includes(measurementData.toolType) &&
              value.requireMeasurements?.includes(measurementData.location)
          );
          let studyFormNotes = JSON.parse(JSON.stringify(this.state.notes));
          const stateAnswer = this.state.notes.slices
            ? this.state.notes.slices?.[slice]?.[roiQuestion.key]
            : this.state.notes?.[roiQuestion.key];
          const formQuestionAnswer =
            typeof stateAnswer === 'object' ? stateAnswer.value : stateAnswer;
          let allLabelApplied = true;
          roiAnswer.forEach(item => {
            item.measurementTools.forEach(tool => {
              item.requireMeasurements.forEach(label => {
                if (allLabelApplied) {
                  allLabelApplied =
                    !isFile && projectConfig.bSlices
                      ? this.props.measurements[tool]?.some(
                          measurement =>
                            measurement.location === label &&
                            measurement.questionKey === roiQuestion.key &&
                            measurement.sliceNumber === slice &&
                            measurement.toolType === tool
                        )
                      : this.props.measurements[tool]?.some(
                          measurement =>
                            measurement.location === label &&
                            measurement.questionKey === roiQuestion.key &&
                            measurement.toolType === tool
                        );
                }
              });
            });
          });
          if (
            roiQuestion.values &&
            roiAnswer.length > 1 &&
            (!roiQuestion?.positiveAnswers?.includes(formQuestionAnswer) ||
              !allLabelApplied)
          ) {
            if (this.roiQuestionDialogId) {
              return;
            }
            // Update all measurements by measurement number
            const measurementList = measurementApi.Instance.tools[
              measurementData.toolType
            ]?.filter(
              m => m.measurementNumber === measurementData.measurementNumber
            );
            let lastAnsweredValue;
            if (
              state.subForm?.isLabelApplied &&
              (projectConfig?.studyFormWorkflow === 'ROI' ||
                projectConfig?.studyFormWorkflow === 'Mixed')
            ) {
              store.dispatch(setLabelForQuestion(false));
              let annotationAnswer = roiAnswer[0]?.value;
              const answerHasSubForm = roiAnswer?.some(
                answer => answer.value === formQuestionAnswer
              );
              if (formQuestionAnswer && answerHasSubForm) {
                annotationAnswer = roiAnswer.find(
                  data => data.value === formQuestionAnswer
                )?.value;
                updateAnnotationKeyAnswer(
                  measurementList,
                  roiQuestion,
                  annotationAnswer
                );
                let annotationAnswerData = roiAnswer.filter(
                  data => data.value === formQuestionAnswer
                );
                const note = updateNoteInMixedOrROI(
                  projectConfig,
                  studyFormNotes,
                  sliceInfo,
                  roiQuestion,
                  annotationAnswerData
                );
                studyFormNotes = note;
              }
              if (
                projectConfig?.studyFormWorkflow === 'ROI' ||
                projectConfig?.studyFormWorkflow === 'Mixed'
              ) {
                this.setSubFormTabDetails(projectConfig, studyFormNotes, true);
              } else {
                this.setSubFormTabDetails(projectConfig, studyFormNotes, false);
              }
            }
            if (!roiQuestion?.positiveAnswers?.includes(formQuestionAnswer)) {
              this.setState({
                confirmationDialogCurrentQuestion: roiQuestion,
                confirmationDialogRoiAnswer: roiAnswer,
                confirmationDialogMeasurementList: measurementList,
              });
              this.confirmationDialogShow(
                roiQuestion,
                measurementList,
                projectConfig,
                lastAnsweredValue,
                slice,
                roiAnswer,
                studyFormNotes,
                sliceInfo
              );
            } else if (
              roiQuestion?.positiveAnswers?.includes(formQuestionAnswer) &&
              !this.roiSubFormDialogId
            ) {
              let annotationAnswer = roiAnswer[0];
              if (formQuestionAnswer) {
                annotationAnswer = roiAnswer.find(
                  data => data.value === formQuestionAnswer
                );
              }
              lastAnsweredValue = annotationAnswer;
              this.addOREditSubForm(
                projectConfig,
                roiQuestion,
                measurementList,
                lastAnsweredValue,
                studyFormNotes
              );
            }
          } else if (
            roiQuestion.values &&
            roiAnswer.length === 1 &&
            (!roiQuestion?.positiveAnswers?.includes(formQuestionAnswer) ||
              !allLabelApplied)
          ) {
            if (this.roiSubFormDialogId) {
              return;
            }
            let lastAnsweredValue;
            let measurementsList = [];
            if (state.subForm?.isLabelApplied) {
              store.dispatch(setLabelForQuestion(false));
              // Update all measurements by measurement number
              measurementsList = measurementApi.Instance.tools[
                measurementData.toolType
              ]?.filter(
                m => m.measurementNumber === measurementData.measurementNumber
              );
              updateAnnotationKeyAnswer(
                measurementsList,
                roiQuestion,
                roiAnswer[0].value
              );
              this.setSubFormTabDetails(projectConfig, studyFormNotes, false);
              const note = updateNoteInMixedOrROI(
                projectConfig,
                studyFormNotes,
                sliceInfo,
                roiQuestion,
                roiAnswer
              );
              studyFormNotes = note;
              if (projectConfig?.studyFormWorkflow === 'Mixed') {
                this.setSubFormTabDetails(projectConfig, studyFormNotes, true);
              } else {
                this.setState(prevState => ({
                  ...prevState,
                  notes: studyFormNotes,
                }));
              }
              lastAnsweredValue = roiAnswer[0];
              setTimeout(() => {
                this.saveWhileQuestionSwitches();
              }, 1);
            }
            if (
              roiAnswer[0].subForm &&
              projectConfig.studyForm?.subForms?.[roiAnswer[0].subForm]
                ?.components
            ) {
              this.addOREditSubForm(
                projectConfig,
                roiQuestion,
                measurementsList,
                lastAnsweredValue,
                studyFormNotes
              );
            }
          } else if (
            roiQuestion?.positiveAnswers?.includes(formQuestionAnswer) &&
            (projectConfig?.studyFormWorkflow === 'ROI' ||
              projectConfig?.studyFormWorkflow === 'Mixed')
          ) {
            if (
              roiAnswer[0].subForm &&
              projectConfig.studyForm?.subForms?.[roiAnswer[0].subForm]
                ?.components &&
              state.subForm?.isLabelApplied
            ) {
              store.dispatch(setLabelForQuestion(false));
              const lastAnsweredValue = roiQuestion.values?.find(
                value => value.value === formQuestionAnswer
              );
              const measurementList = measurementApi.Instance.tools[
                measurementData.toolType
              ]?.filter(
                m => m.measurementNumber === measurementData.measurementNumber
              );
              let annotationAnswer = roiAnswer[0]?.value;
              if (formQuestionAnswer && roiAnswer.length > 1) {
                annotationAnswer = roiAnswer.find(
                  data => data.value === formQuestionAnswer
                )?.value;
              }
              updateAnnotationKeyAnswer(
                measurementList,
                roiQuestion,
                annotationAnswer
              );
              this.addOREditSubForm(
                projectConfig,
                roiQuestion,
                measurementList,
                lastAnsweredValue,
                studyFormNotes
              );
              this.setSubFormTabDetails(projectConfig, studyFormNotes, false);
            }
          }
        }
      }
    }
    if (
      sessionRead?.notes &&
      !isFile &&
      projectConfig.bSlices &&
      (measurementLabels !== prevProps.measurementLabels ||
        sessionRead !== prevProps.sessionRead)
    ) {
      getAndSetBSliceValidatedInfo(this.props, this.state.notes);
      this.setColorIndicationForViewport(sliceInfo);
    } else if (
      !sessionRead?.notes &&
      !isFile &&
      projectConfig.bSlices &&
      (measurementLabels === prevProps.measurementLabels ||
        sessionRead === prevProps.sessionRead) &&
      !Object.keys(state.studyFormBSliceValidatedInfo?.bSliceValidation)
        .length &&
      sliceInfo?.sliceNumber &&
      projectConfig.bSlices.settings?.[sliceInfo.sliceNumber]
    ) {
      getAndSetBSliceValidatedInfo(this.props, this.state.notes);
      this.setColorIndicationForViewport(sliceInfo);
    }
    if (this.props.currentSelectedQuestion !== this.state.currentQuestion) {
      this.setState({ currentQuestion: this.props.currentSelectedQuestion });
    }
  }

  confirmationDialogShow = (
    roiQuestion,
    measurementList,
    projectConfig,
    lastAnsweredValue,
    slice,
    roiAnswer,
    studyFormNotes,
    sliceInfo,
    answer = null
  ) => {
    const { UIDialogService } = this.props.servicesManager.services;
    const popupFromLeft = 175;
    const popupFromTop = 220;
    const measurementTools = [
      'Length',
      'Parallel',
      'ArrowAnnotate',
      'Angle',
      'RectangleRoi',
      'EllipticalRoi',
      'CircleRoi',
      'FreehandRoi',
      'OpenFreehandRoi',
      'Bidirectional',
      'ContourRoi',
    ];
    this.props.setAvailableLabels([''], ['DragProbe']);
    const activeTool = store.getState().infusions?.activeTool;
    if (
      measurementTools.includes(activeTool) &&
      activeTool !== this.state.studyFormQuestionToolForSubForm
    ) {
      this.setState({ studyFormQuestionToolForSubForm: activeTool });
    }
    this.props.setActiveTool('Wwwc');
    this.roiQuestionDialogId = UIDialogService.create({
      content: StudyFormQuestionConfirmationDialog,
      defaultPosition: {
        x: window.innerWidth / 2 - popupFromLeft,
        y: window.innerHeight / 2 - popupFromTop,
      },
      showOverlay: false,
      contentProps: {
        question: roiQuestion,
        value: {
          value: answer ? answer : '',
        },
        formatOptions: () => {
          formatOptions(roiQuestion.values, DROPDOWN_DEFAULT_VALUE);
        },
        getValue: (slice, question) => {
          getValue(slice, question, this.state);
        },
        slice: measurementList[0]?.sliceNumber,
        selectBoxOnChange: (option, question) => {
          this.clickType(option, question.key);
          this.onClickQuestionValues(question);
        },
        onClickQuestionValues: question => {
          this.onClickQuestionValues(question);
        },
        onConfirm: () => {
          UIDialogService.dismiss({ id: this.roiQuestionDialogId });
          this.roiQuestionDialogId = null;
          if (
            lastAnsweredValue?.subForm &&
            projectConfig.studyForm.subForms[lastAnsweredValue?.subForm]
              .components
          ) {
            this.addOREditSubForm(
              projectConfig,
              roiQuestion,
              measurementList,
              lastAnsweredValue
            );
            this.props.setActiveTool(
              this.state.studyFormQuestionToolForSubForm
            );
            setTimeout(() => {
              this.props.setActiveTool('Wwwc');
            }, 0);
          } else {
            const studyFormQuestionTools = getStudyFormActiveToolsForEnable(
              projectConfig,
              this.props,
              null,
              this.toolsWithLabels
            );
            const label = studyFormQuestionTools?.length ? [''] : null;
            let tools = studyFormQuestionTools?.length
              ? studyFormQuestionTools
              : null;
            tools = getAvailableTools(
              tools,
              projectConfig,
              this.toolsWithLabels,
              this.props.measurementLabels,
              slice
            );
            this.props.setAvailableLabels(label, tools);
            this.props.setActiveTool(
              this.state.studyFormQuestionToolForSubForm
            );
            setTimeout(() => {
              this.props.setActiveTool('Wwwc');
            }, 0);
          }
        },
        onClose: () => {
          UIDialogService.dismiss({ id: this.roiQuestionDialogId });
          this.roiQuestionDialogId = null;
        },
        clickQuestion: answer => {
          const roiData = updateAnnotationKeyAnswerAndNotes(
            measurementList,
            roiQuestion,
            answer,
            roiAnswer,
            projectConfig,
            studyFormNotes,
            sliceInfo
          );
          lastAnsweredValue = roiData.data;
          this.setSubFormTabDetails(
            projectConfig,
            roiData.studyFormNotes,
            true
          );
          this.setState({
            confirmationDialogCurrentQuestion: roiQuestion,
            confirmationDialogRoiAnswer: roiAnswer,
            answer: answer,
            confirmationDialogMeasurementList: measurementList,
            lastAnsweredValue: lastAnsweredValue,
          });
          setTimeout(() => {
            this.saveWhileQuestionSwitches();
          }, 1);
        },
      },
    });
  };

  setSubFormTabDetails = (projectConfig, studyFormNotes, isNoteUpdate) => {
    const componentKeyList = projectConfig.studyForm?.components?.map(
      component => component.key
    );
    const tab = getSubFormTabDetails(
      componentKeyList,
      projectConfig,
      measurementTools,
      this.props.measurements,
      this.state
    );
    if (isNoteUpdate) {
      this.setState(prevState => ({
        ...prevState,
        notes: studyFormNotes,
        subFormTabList: { ...tab.subFormTabList },
        subFormPanel: { ...tab.subFormPanel },
        subFormTab: { ...tab.subFormTab },
        subFormTabWithAnnotationId: {
          ...tab.subFormTabWithAnnotationId,
        },
        annotationIdForQuestionKey: {
          ...tab.annotationIdForQuestionKey,
        },
      }));
    } else {
      this.setState(prevState => ({
        ...prevState,
        subFormTabList: { ...tab.subFormTabList },
        subFormPanel: { ...tab.subFormPanel },
        subFormTab: { ...tab.subFormTab },
        subFormTabWithAnnotationId: {
          ...tab.subFormTabWithAnnotationId,
        },
        annotationIdForQuestionKey: {
          ...tab.annotationIdForQuestionKey,
        },
      }));
    }
  };

  getCurrentOption(currentSelectedQuestion) {
    const currentQuestionAnswer =
      currentSelectedQuestion?.answer?.value || currentSelectedQuestion?.answer;
    const values = currentSelectedQuestion?.values;
    if (values?.length > 0) {
      return values.find(v => v.value === currentQuestionAnswer);
    }
  }

  restoreCurrentQuestion = state => {
    const {
      conditional,
      contourVisibility,
      key,
      label,
      required,
      type,
      values,
    } = state.infusions.currentSelectedQuestion;

    if (isBSliceApplicable(state) && this.state.currentQuestion === null) {
      this.setState({
        currentQuestion: {
          conditional: conditional,
          contourVisibility: contourVisibility,
          key: key,
          label: label,
          required: required,
          type: type,
          values: values,
        },
      });
    }
  };

  getAndSetBSliceValidatedInfo = () => {
    const { projectConfig } = this.props;
    const allBSliceValidatedData = validateAllBSlice(
      projectConfig,
      this.state.notes
    );
    if (Object.keys(allBSliceValidatedData).length) {
      store.dispatch(studyFormBSliceValidatedInfo(allBSliceValidatedData));
    }
  };

  setColorIndicationForViewport = sliceInfo => {
    if (check2dmprViewportPresent()) {
      this.props.commandsManager.runCommand('setColorIndicationFor2DMPR');
      if (sliceInfo) {
        this.scrollCornerstoneViewport(sliceInfo);
      }
    }
  };

  addOREditSubForm = (
    projectConfig,
    roiQuestion,
    measurements,
    lastAnsweredValue,
    studyFormNotes = null
  ) => {
    const { UIDialogService } = this.props.servicesManager.services;
    if (this.roiSubFormDialogId) {
      UIDialogService.dismiss({ id: this.roiSubFormDialogId });
      this.roiSubFormDialogId = null;
    }

    if (
      this.roiQuestionDialogId ||
      !projectConfig.studyForm?.subForms?.[lastAnsweredValue?.subForm]
        ?.components
    ) {
      return;
    }

    this.setState({
      subFormDialogCurrentQuestion: roiQuestion,
      subFormDialogRoiAnswer: lastAnsweredValue,
      subFormDialogMeasurementList: measurements,
    });

    this.props.setAvailableLabels([''], ['DragProbe']);

    const formStyles = {
      pointerEvents: isFormViewOnly(this.state) ? 'none' : 'auto',
      opacity: isFormViewOnly(this.state) ? 0.5 : 1,
    };
    store.dispatch(subFormPopupVisible(true));
    let studyState = JSON.parse(JSON.stringify(this.state));
    if (studyFormNotes) {
      studyState.notes = studyFormNotes;
    }
    const popupFromLeft = 175;
    const popupFromTop = 220;
    this.roiSubFormDialogId = UIDialogService.create({
      content: SubFormDialog,
      defaultPosition: {
        x: window.innerWidth / 2 - popupFromLeft,
        y: window.innerHeight / 2 - popupFromTop,
      },
      showOverlay: false,
      contentProps: {
        subFormComponent:
          projectConfig.studyForm?.subForms?.[lastAnsweredValue.subForm]
            ?.components,
        annotationId: measurements[0]?.uuid,
        slice: measurements[0]?.sliceNumber,
        question: roiQuestion,
        studyFormState: studyState,
        clickType: (option, key, questionKey, subFormName, annotationId) => {
          this.clickType(option, key, questionKey, subFormName, annotationId);
        },
        formStyles: formStyles,
        onClickQuestionValues: (subFormQuestion, isSubForm, subFormName) => {
          this.onClickQuestionValues(subFormQuestion, isSubForm, subFormName);
        },
        getValueSubForm: (
          slice,
          subFormQuestion,
          key,
          subFormName,
          annotationId
        ) => {
          getValueSubForm(
            slice,
            subFormQuestion,
            key,
            subFormName,
            annotationId,
            this.state
          );
        },
        formatOptions: values => {
          formatOptions(values, DROPDOWN_DEFAULT_VALUE);
        },
        readOnly: false,
        measurementLabels: this.props.measurementLabels,
        questionRefs: this.questionRefs,
        onClose: () => {
          UIDialogService.dismiss({ id: this.roiSubFormDialogId });
          this.roiSubFormDialogId = null;
          store.dispatch(subFormPopupVisible(false));
        },
        onConfirm: () => {
          UIDialogService.dismiss({ id: this.roiSubFormDialogId });
          this.roiSubFormDialogId = null;
          store.dispatch(subFormPopupVisible(false));
          if (
            projectConfig?.studyFormWorkflow === 'ROI' ||
            projectConfig?.studyFormWorkflow === 'Mixed'
          ) {
            const studyFormQuestionTools = getStudyFormActiveToolsForEnable(
              projectConfig,
              this.props,
              null,
              this.toolsWithLabels
            );
            const label = studyFormQuestionTools?.length ? [''] : null;
            let tools = studyFormQuestionTools?.length
              ? studyFormQuestionTools
              : null;
            tools = getAvailableTools(
              tools,
              projectConfig,
              this.toolsWithLabels,
              this.props.measurementLabels,
              measurements[0]?.sliceNumber
            );
            this.props.setAvailableLabels(label, tools);
            store.dispatch(setActiveQuestion(null));
            this.props.setActiveTool(
              this.state.studyFormQuestionToolForSubForm
            );
            setTimeout(() => {
              this.props.setActiveTool('Wwwc');
            }, 0);
          }
        },
      },
    });
  };

  async setReaderTaskAndPermissions() {
    const state = store.getState();
    const user = selectUser(state);
    const isSiteAdmin = user.roles.includes('site_admin');
    const readerTask = await selectReaderTask(state);
    let ownAnnotationPermission =
      (await hasPermission(state, 'annotations_own')) ||
      (await hasPermission(state, 'annotations_manage'));
    let hasEditAnnotationsOthersPermission = await hasPermission(
      state,
      'annotations_edit_others'
    );
    let ownFormResponsesPermission =
      (await hasPermission(state, 'form_responses_own')) ||
      (await hasPermission(state, 'form_responses_manage'));
    let hasEditFormOthersPermission = await hasPermission(
      state,
      'form_responses_edit_others'
    );
    const featureConfig = state.flywheel.featureConfig;
    this.setState({
      readerTask: readerTask || null,
      readOnly:
        !ownFormResponsesPermission &&
        !hasEditFormOthersPermission &&
        !isSiteAdmin
          ? featureConfig?.features?.reader_tasks || false
          : false,
      ownAnnotationPermission: ownAnnotationPermission || isSiteAdmin,
      hasEditAnnotationsOthersPermission:
        hasEditAnnotationsOthersPermission || isSiteAdmin,
      ownFormResponsesPermission: ownFormResponsesPermission || isSiteAdmin,
      hasEditFormOthersPermission: hasEditFormOthersPermission || isSiteAdmin,
    });
  }

  onValueChangeStudyNoteUpdation = (
    question,
    subFormQuestion,
    annotationId,
    subFormName,
    value,
    key,
    type,
    projectConfig
  ) => {
    const slice = this.state.currentSliceInfo?.sliceNumber || 1;
    let subFormPanel = { ...this.state.subFormPanel };
    const state = store.getState();
    const isFile = isCurrentFile(state);
    if (
      projectConfig.studyFormWorkflow === 'Form' ||
      projectConfig?.studyFormWorkflow === 'Mixed'
    ) {
      if (!isFile && projectConfig.bSlices && subFormPanel[slice]) {
        Object.keys(subFormPanel[slice]).forEach(questionKey => {
          if (!subFormQuestion) {
            subFormPanel[slice][questionKey] = false;
          } else {
            if (questionKey !== subFormQuestion.key) {
              subFormPanel[slice][questionKey] = false;
            } else {
              subFormPanel[slice][questionKey] = true;
            }
          }
        });
      } else {
        Object.keys(subFormPanel).forEach(questionKey => {
          if (!subFormQuestion) {
            subFormPanel[questionKey] = false;
          } else {
            if (questionKey !== subFormQuestion.key) {
              subFormPanel[questionKey] = false;
            } else {
              subFormPanel[questionKey] = true;
            }
          }
        });
      }
    }
    let stateNotes = JSON.parse(JSON.stringify(this.state.notes));
    let updatedNotes =
      !isFile && projectConfig.bSlices && stateNotes.slices
        ? stateNotes.slices[slice] || {}
        : stateNotes;
    if (subFormQuestion !== null) {
      if (type === 'selectboxes') {
        // multi-select
        if (!updatedNotes[subFormQuestion.key][subFormName]) {
          updatedNotes[subFormQuestion.key][subFormName] = [];
        }
        const isSubFormNotes = updatedNotes[subFormQuestion.key][
          subFormName
        ].find(item => item.annotationId === annotationId);
        if (!isSubFormNotes[key] || !Array.isArray(isSubFormNotes[key])) {
          const subFormNotes = {
            annotationId: annotationId,
            [key]: [value],
          };
          updatedNotes[subFormQuestion.key][subFormName].push(subFormNotes);
        } else if (!isSubFormNotes[key].includes(value)) {
          isSubFormNotes[key].push(value);
        } else {
          updatedNotes[subFormQuestion.key][subFormName].find(
            item => item.annotationId === annotationId
          )[key] = isSubFormNotes[key].filter(item => item !== value);
        }
      } else {
        if (question.values) {
          question.values.forEach(item => {
            if (item.value === value) {
              if (
                subFormName &&
                typeof updatedNotes[subFormQuestion.key] === 'string'
              ) {
                updatedNotes[subFormQuestion.key] = {
                  value: item.value,
                };
              }
              if (!updatedNotes[subFormQuestion.key]) {
                updatedNotes[subFormQuestion.key] = {
                  value: item.value,
                };
              }
              if (!updatedNotes[subFormQuestion.key][subFormName]) {
                updatedNotes[subFormQuestion.key][subFormName] = [];
              }
              const isSubFormNotes = updatedNotes[subFormQuestion.key][
                subFormName
              ].find(item => item.annotationId === annotationId);
              if (!isSubFormNotes) {
                const subFormNotes = {
                  annotationId: annotationId,
                  [key]: value === DROPDOWN_DEFAULT_VALUE ? '' : value,
                };
                updatedNotes[subFormQuestion.key][subFormName].push(
                  subFormNotes
                );
              } else {
                updatedNotes[subFormQuestion.key][subFormName].find(
                  item => item.annotationId === annotationId
                )[key] = value === DROPDOWN_DEFAULT_VALUE ? '' : value;
              }
            }
          });
        } else {
          updatedNotes[subFormQuestion.key][subFormName].find(
            item => item.annotationId === annotationId
          )[key] = value === DROPDOWN_DEFAULT_VALUE ? '' : value;
        }
      }
    } else {
      if (type === 'selectboxes') {
        // multi-select
        if (!updatedNotes[key] || !Array.isArray(updatedNotes[key])) {
          updatedNotes[key] = [value];
        } else if (!updatedNotes[key].includes(value)) {
          updatedNotes[key].push(value);
        } else {
          updatedNotes[key] = updatedNotes[key].filter(item => item !== value);
        }
      } else {
        if (question.values) {
          question.values.forEach(item => {
            if (item.value === value) {
              if (item.subForm) {
                const subFormName = item.subForm;
                if (typeof updatedNotes[key] === 'object') {
                  updatedNotes[key].value =
                    value === DROPDOWN_DEFAULT_VALUE ? '' : value;
                } else {
                  updatedNotes[key] =
                    value === DROPDOWN_DEFAULT_VALUE
                      ? {
                          value: '',
                        }
                      : {
                          value: value,
                        };
                  updatedNotes[key][subFormName] = [];
                }
              } else {
                updatedNotes[key] =
                  value === DROPDOWN_DEFAULT_VALUE ? '' : value;
              }
            }
          });
        } else {
          updatedNotes[key] = value === DROPDOWN_DEFAULT_VALUE ? '' : value;
        }
      }
    }
    const activeTool = selectActiveTool(store.getState());
    const previousTool = measurementTools.includes(activeTool)
      ? this.state.previousTool
      : !activeTool
      ? this.state.previousTool
      : activeTool;
    if (!isFile && projectConfig.bSlices) {
      if (!stateNotes.slices) {
        stateNotes = { slices: {} };
      }
      stateNotes.slices[slice] = updatedNotes;
    } else {
      stateNotes = updatedNotes;
    }
    if (subFormQuestion !== null) {
      store.dispatch(
        setActiveQuestion({
          ...question,
          answer: updatedNotes[subFormQuestion.key][subFormName].find(
            item => item.annotationId === annotationId
          )[key],
          questionKey: key,
          isSubForm: true,
          subFormName: subFormName,
          subFormAnnotationId: annotationId,
          question: subFormQuestion.key,
        })
      );
    } else {
      store.dispatch(
        setActiveQuestion({
          ...question,
          answer: updatedNotes[key],
          questionKey: key,
          isSubForm: false,
          subFormName: '',
          subFormAnnotationId: '',
          question: '',
        })
      );
    }

    if (!isFile && projectConfig.bSlices) {
      const allBSliceValidatedData =
        state.studyFormBSliceValidatedInfo.bSliceValidation;
      const bSliceValidatedData = validateBSlice(
        projectConfig,
        slice,
        stateNotes
      );
      if (
        Object.keys(allBSliceValidatedData).length &&
        allBSliceValidatedData[slice] !== !bSliceValidatedData
      ) {
        allBSliceValidatedData[slice] = !bSliceValidatedData;
        store.dispatch(studyFormBSliceValidatedInfo(allBSliceValidatedData));
        this.props.commandsManager.runCommand('setColorIndicationFor2DMPR');
        this.scrollCornerstoneViewport(this.state.currentSliceInfo);
      }
    }

    this.setState(prevState => ({
      ...prevState,
      errors: omit(prevState.errors, key),
      notes: { ...stateNotes },
      currentQuestion: { ...question },
      previousTool: previousTool,
      previousQuestion: this.state.currentQuestion,
      subFormPanel: subFormPanel,
    }));
  };

  onValueChange = (
    question,
    subFormQuestion = null,
    annotationId = null
  ) => event => {
    const { value } = event.target;
    const { key, type } = question;
    const { projectConfig } = this.props;
    let subFormName = null;
    if (
      (projectConfig?.studyFormWorkflow === 'ROI' ||
        projectConfig?.studyFormWorkflow === 'Mixed') &&
      !subFormQuestion
    ) {
      subFormName = question.values?.find(value => value.value === value)
        ?.subForm;
    } else if (subFormQuestion) {
      subFormName = subFormQuestion?.subFormName;
    }
    this.onValueChangeStudyNoteUpdation(
      question,
      subFormQuestion,
      annotationId,
      subFormName,
      value,
      key,
      type,
      projectConfig
    );
  };

  scrollCornerstoneViewport = sliceInfo => {
    if (sliceInfo?.sliceNumber) {
      const element = cornerstoneUtils.getEnabledElement(
        sliceInfo.viewportIndex
      );
      if (element) {
        const enabledElement = cornerstone.getEnabledElement(element);
        if (enabledElement?.image) {
          const scrollIndex = sliceInfo.sliceNumber - 1;
          scrollToIndex(enabledElement.element, scrollIndex);
        }
      }
    }
  };

  setStudyLabels() {
    const { projectConfig } = this.props;
    if (!projectConfig.labels) {
      return;
    }
    const state = store.getState();
    let toolsWithLabel = {};
    const timePointManager = state.timepointManager;
    const props = {
      ...this.props,
      previousVisibleQuestion: this.state.previousQuestion,
      sliceInfo: this.props.sliceInfo,
    };

    const visibleQuestions = getVisibleQuestions(this.state, props);
    if (!visibleQuestions.length) {
      return;
    }

    const isSubFormEnabled =
      this.roiQuestionDialogId || this.roiSubFormDialogId ? true : false;
    toolsWithLabel = getToolsWithLabels(visibleQuestions, isSubFormEnabled);

    let {
      requiredLabels,
      availableLabels,
      availableTools,
      subFormInfo,
    } = this.getToolsAndLabelsForCurrentAnswer(
      visibleQuestions,
      toolsWithLabel
    );

    // if there are no required measurements, allow all labels
    this.setAvailableLabelsAndToolsBasedOnCurrentAnswer(
      requiredLabels,
      timePointManager,
      subFormInfo,
      visibleQuestions,
      availableTools,
      availableLabels
    );
  }

  getToolsAndLabelsForCurrentAnswer = (visibleQuestions, toolsWithLabel) => {
    const { measurementLabels, projectConfig } = this.props;
    const { currentSliceInfo, currentQuestion } = this.state;
    const slice = currentSliceInfo?.sliceNumber || 1;
    const state = store.getState();
    const timePointManager = state.timepointManager;

    let components = visibleQuestions;

    const subFormInfo = getSubFormQuestionDetails(this.state, visibleQuestions);
    if (subFormInfo?.components?.some(x => x.key === currentQuestion?.key)) {
      components = subFormInfo.components;
    }

    // get required measurements across all question options and selected options
    const {
      requiredLabels,
      availableTools,
    } = this.getRequiredLabelsAndAvailableToolsForCurrentAnswer(
      components,
      subFormInfo
    );

    if (toolsWithLabel && Object.keys(toolsWithLabel)?.length) {
      this.toolsWithLabels = toolsWithLabel;
    }

    // filter labels down to those required by selected options
    let subFormInfoForLabels = { ...subFormInfo };

    if (timePointManager.isMeasurementDeletedForStudyFormQuestion) {
      const lastDeletedMeasurementId =
        timePointManager.lastDeletedMeasurementId;
      subFormInfoForLabels = getDeletedMeasurementDetail(
        timePointManager,
        lastDeletedMeasurementId
      );
    }

    let availableLabels = getAvailableLabels(
      projectConfig,
      requiredLabels,
      measurementLabels,
      slice,
      subFormInfoForLabels.question,
      subFormInfoForLabels.subFormName,
      subFormInfoForLabels.subFormAnnotationId,
      subFormInfoForLabels.isSubForm
    );

    return { requiredLabels, availableLabels, availableTools, subFormInfo };
  };

  setToolsForBSliceConfig = (projectConfig, sliceInfo) => {
    const infusions = store.getState().infusions;
    const infusionActiveTool = infusions.activeTool;
    const currentQuestionKey = infusions.currentSelectedQuestion?.key;
    const currentQuestionAnswer =
      infusions.currentSelectedQuestion?.answer?.value ||
      infusions.currentSelectedQuestion?.answer;
    const values = infusions.currentSelectedQuestion?.values;
    let currentTool = this.state.currentTool;
    let isToolAvailableForQuestion = true;

    if (values?.length > 0) {
      const currentAnswer = values.find(v => v.value === currentQuestionAnswer);
      if (this.state.isSubFormQuestion) {
        currentTool = currentAnswer?.measurementTools?.[0];
      } else {
        if (currentAnswer?.instructionSet?.length) {
          currentTool = currentAnswer?.instructionSet[0].measurementTools[0];
        } else {
          currentTool = currentAnswer?.measurementTools?.[0];
        }
      }
    }

    if (
      measurementTools.includes(infusionActiveTool) &&
      currentTool !== infusionActiveTool &&
      infusionActiveTool !== 'DragProbe'
    ) {
      currentTool = infusionActiveTool;
    }

    if (projectConfig?.bSlices?.settings?.[sliceInfo?.sliceNumber]) {
      const slices = this.state.notes.slices;
      const currentSlice = slices?.[sliceInfo?.sliceNumber];
      if (
        this.state.isSubFormQuestion &&
        currentSlice?.hasOwnProperty(this.state.subFormQuestion)
      ) {
        const subFormLabel = currentSlice?.[this.state.subFormQuestion]?.[
          this.state.subFormName
        ].find(item => item.annotationId === this.state.subFormAnnotationId);
        if (subFormLabel?.hasOwnProperty(currentQuestionKey)) {
          const answer =
            subFormLabel[currentQuestionKey].value ||
            subFormLabel[currentQuestionKey];
          const value = values?.find(v => v.value === answer);
          if (!value?.measurementTools?.includes(currentTool)) {
            isToolAvailableForQuestion = false;
            Utils.cornerstoneUtils.setToolMode(currentTool, 'passive');
          }
        }
      } else if (currentSlice?.hasOwnProperty(currentQuestionKey)) {
        const answer =
          currentSlice[currentQuestionKey]?.value ||
          currentSlice[currentQuestionKey];
        const value = values?.find(v => v.value === answer);
        if (
          !value?.measurementTools?.includes(currentTool) &&
          !value?.instructionSet?.filter(i =>
            i.measurementTools.includes(currentTool)
          )
        ) {
          isToolAvailableForQuestion = false;
          Utils.cornerstoneUtils.setToolMode(currentTool, 'passive');
        }
      } else {
        isToolAvailableForQuestion = false;
        Utils.cornerstoneUtils.setToolMode(currentTool, 'passive');
      }
    } else {
      if (currentTool === infusionActiveTool) {
        isToolAvailableForQuestion = false;
        Utils.cornerstoneUtils.setToolMode(infusionActiveTool, 'passive');
      }
    }
    setTimeout(() => {
      this.setAvailableLabelsForActiveModeTools(
        currentTool,
        sliceInfo,
        isToolAvailableForQuestion
      );
    }, 100);
  };

  // Set tools and labels enabled/disabled based on the tool mode of selected tool.
  // This will not change currently selected tool
  setAvailableLabelsForActiveModeTools = (
    currentTool,
    sliceInfo,
    isToolAvailableForQuestion
  ) => {
    const element = cornerstoneUtils.getEnabledElement(sliceInfo.viewportIndex);
    const tool = cornerstoneTools.getToolForElement(element, currentTool);
    const availableTools = store.getState().infusions.availableTools;
    if (
      tool?.mode !== 'active' &&
      (availableTools.includes(currentTool) ||
        (!availableTools.includes(currentTool) && !isToolAvailableForQuestion))
    ) {
      this.props.setAvailableLabels([''], ['DragProbe']);
    } else {
      const props = {
        ...this.props,
        previousVisibleQuestion: this.state.previousQuestion,
        sliceInfo: this.props.sliceInfo,
      };

      const visibleQuestions = getVisibleQuestions(this.state, props);
      if (!visibleQuestions.length) {
        return;
      }
      const {
        availableLabels,
        availableTools,
      } = this.getToolsAndLabelsForCurrentAnswer(visibleQuestions);
      availableTools.add('DragProbe');
      const availableToolsList =
        availableTools?.size > 0 ? Array.from(availableTools) : availableTools;
      if (availableLabels.length === 0) {
        if (!isToolAvailableForQuestion) {
          availableTools.clear();
          allowedActiveTools.forEach(tool => availableTools.add(tool));
          this.props.setAvailableLabels(
            [''],
            availableTools.size > 0 ? availableToolsList : null
          );
          this.props.setActiveTool(availableToolsList[0]);
        } else {
          this.props.setAvailableLabels([''], ['DragProbe']);
        }
      } else {
        allowedActiveTools.forEach(tool => availableTools.add(tool));
        this.props.setAvailableLabels(
          availableLabels,
          availableTools.size > 0 ? availableToolsList : null
        );
        this.props.setActiveTool(availableToolsList[0]);
      }
    }
  };

  getRequiredLabelsAndAvailableToolsForCurrentAnswer(components, subFormInfo) {
    const { projectConfig } = this.props;
    const { notes, currentQuestion, currentSliceInfo } = this.state;
    const slice = currentSliceInfo?.sliceNumber || 1;
    const state = store.getState();
    const timePointManager = state.timepointManager;

    const requiredLabels = new Set();
    const availableTools = new Set();

    const isStudyFormContainsMeasurementTools = projectConfig.studyForm?.components?.some(
      isMeasurementTools
    );
    const bSliceSettings = getBSliceSettings();
    if (
      state.infusions.currentSelectedQuestion === null &&
      isStudyFormContainsMeasurementTools &&
      !timePointManager.isMeasurementDeletedForStudyFormQuestion
    ) {
      // Allow DragProbe to be active always
      availableTools.add(...allowedActiveTools);
      return { requiredLabels, availableTools };
    }

    for (const question of components) {
      if (
        (currentQuestion && currentQuestion.key !== question.key) ||
        timePointManager.isMeasurementDeletedForStudyFormQuestion
      ) {
        continue;
      }
      for (const option of question.values || []) {
        let isSelected = false;
        let answer = '';
        if (subFormInfo.isSubForm) {
          let subFormNotes = null;
          if (notes.slices) {
            subFormNotes = notes?.slices?.[slice];
          } else {
            subFormNotes = notes;
          }

          answer = subFormNotes?.[subFormInfo.question]?.[
            subFormInfo.subFormName
          ].find(item => item.annotationId === subFormInfo.subFormAnnotationId);
          isSelected = option.value === answer?.[question.key];
          if (!isSelected && currentQuestion?.key == question.key) {
            isSelected = true;
          }
        } else {
          if (typeof notes.slices?.[slice]?.[question.key] === 'object') {
            isSelected =
              option.value === notes.slices[slice][question.key].value;
          } else if (typeof notes[question.key] === 'object') {
            isSelected = option.value === notes[question.key].value;
          } else {
            isSelected = notes.slices
              ? notes.slices[slice]
                ? option.value === notes.slices[slice][question.key]
                : false
              : Array.isArray(notes[question.key])
              ? notes[question.key].includes(option.value)
              : option.value === notes[question.key];
          }
        }
        let requireMeasurements = option.requireMeasurements;
        if (
          !subFormInfo.isSubForm &&
          (!requireMeasurements || !requireMeasurements.length)
        ) {
          if (option?.instructionSet?.length) {
            requireMeasurements = option.instructionSet[0].requireMeasurements;
          }
        }
        if (isSelected) {
          const measurementToolsForAnswer = getMeasurementToolsForAnswer(
            option,
            null,
            question,
            bSliceSettings,
            slice
          );
          measurementToolsForAnswer.forEach(x => availableTools.add(x));
          const bSliceTools = getBSliceTools(question, bSliceSettings, slice);
          for (const tool of availableTools) {
            const labelsForTool = getLabelsForTool(tool, option, bSliceTools);
            labelsForTool.forEach(x => requiredLabels.add(x));
          }
        }
      }
    }

    return { requiredLabels, availableTools };
  }

  setAvailableLabelsAndToolsBasedOnCurrentAnswer(
    currentRequired,
    timePointManager,
    subFormInfo,
    visibleQuestions,
    availableTools,
    availableLabels
  ) {
    const { measurementLabels, projectConfig } = this.props;
    const { notes, currentQuestion, currentSliceInfo, readOnly } = this.state;
    const slice = currentSliceInfo?.sliceNumber || 1;
    const state = store.getState();

    let deletedMeasurementLabels;
    let deletedMeasurementTools;
    if (currentRequired.size === 0) {
      if (!this.state.isComplete) {
        if (timePointManager.isMeasurementDeletedForStudyFormQuestion) {
          let {
            deletedMeasurementLabel,
            deletedMeasurementTool,
          } = this.setAvailableLabels_ActiveTools_ValidateWhileSwitch(
            projectConfig,
            timePointManager,
            notes,
            slice,
            measurementLabels,
            subFormInfo.isSubForm,
            subFormInfo.question,
            subFormInfo.subFormName,
            subFormInfo.subFormAnnotationId,
            subFormInfo.location,
            visibleQuestions,
            availableTools,
            currentRequired
          );
          deletedMeasurementLabels = deletedMeasurementLabel;
          deletedMeasurementTools = deletedMeasurementTool;
        } else {
          if (this.state.currentQuestion) {
            this.props.setActiveTool(state.infusions.activeTool);
          }
          this.validateWhileQuestionSwitch();
        }
      } else {
        this.setState({ isComplete: false });
        this.props.setAvailableLabels(null, null);
        this.props.setActiveTool();
        this.validateWhileQuestionSwitch();
      }
    }

    availableTools.add('DragProbe');
    if (deletedMeasurementLabels?.length && deletedMeasurementTools?.length) {
      availableLabels = deletedMeasurementLabels;
      let newAvailableTools = new Set();
      deletedMeasurementTools.forEach(tool => {
        newAvailableTools.add(tool);
      });
      availableTools = newAvailableTools;
    }

    let availableToolsList = [];
    if (
      (projectConfig?.studyFormWorkflow === 'ROI' ||
        projectConfig?.studyFormWorkflow === 'Mixed') &&
      availableLabels.length
    ) {
      availableToolsList = getAvailableTools(
        availableTools,
        projectConfig,
        this.toolsWithLabels,
        measurementLabels,
        slice,
        subFormInfo.isSubForm
      );
    } else if (
      !projectConfig?.studyFormWorkflow ||
      projectConfig?.studyFormWorkflow === 'Form'
    ) {
      availableToolsList =
        availableTools?.size > 0 ? Array.from(availableTools) : availableTools;
    } else if (!availableToolsList.length) {
      availableToolsList.push('DragProbe');
    }

    const isFile = isCurrentFile(state);
    let isSetActiveTool =
      isFile && state.infusions.activeTool === 'OpenFreehandRoi';

    if (
      (projectConfig.studyFormWorkflow === 'ROI' ||
        projectConfig.studyFormWorkflow === 'Mixed') &&
      state.infusions?.toolWhilePanelSwitch
    ) {
      store.dispatch(setToolWhilePanelSwitch(false));
      isSetActiveTool = availableToolsList[0] !== state.infusions.activeTool;
    }

    if (availableLabels.length === 0) {
      availableTools.clear();
      allowedActiveTools.forEach(tool => availableTools.add(tool));
      this.props.setAvailableLabels(
        [''],
        availableTools.size > 0 ? Array.from(availableTools) : null
      );

      if (
        !state.infusions?.currentSelectedQuestion &&
        availableToolsList[0] !== state.infusions.activeTool
      ) {
        isSetActiveTool = true;
      }

      if (!isSetActiveTool) {
        let tool = availableToolsList[0];
        if (
          projectConfig?.studyFormWorkflow === 'Mixed' ||
          projectConfig?.studyFormWorkflow === 'Form'
        ) {
          const questionAnswer =
            currentQuestion?.answer?.value || currentQuestion?.answer;
          const selectedOption = currentQuestion?.values?.find(
            x => x.value === questionAnswer
          );
          if (
            selectedOption?.measurementTools?.includes(
              state.infusions.activeTool
            )
          ) {
            tool = state.infusions.activeTool;
          }
        }
        this.props.setActiveTool(tool);
      }
    } else {
      allowedActiveTools.forEach(tool => availableTools.add(tool));
      this.props.setAvailableLabels(
        availableLabels,
        availableTools.size > 0 ? availableToolsList : null
      );
      if (!isSetActiveTool) {
        this.props.setActiveTool(availableToolsList[0]);
      }
    }

    if (!projectConfig?.bSlices || isCurrentFile(state)) {
      this.setState(prevState => ({
        ...prevState,
        currentAvailableLabels: availableLabels,
        currentAvailableTools: availableTools,
        currentTool: [...availableToolsList][0],
      }));
    }
    let availableToolList = [...availableToolsList];
    availableToolList.splice(availableToolList.indexOf('DragProbe'), 1);
    const activeTool = this.getActiveTool(availableToolList);
    // re-evaluate current tool state
    if (availableTools.size > 0 && availableLabels.length > 0) {
      if (
        projectConfig?.studyFormWorkflow === 'ROI' ||
        projectConfig?.studyFormWorkflow === 'Mixed'
      ) {
        const questionTools = getStudyFormActiveToolsForEnable(
          projectConfig,
          this.props,
          null,
          this.toolsWithLabels
        );
        const qLabels = questionTools?.length ? [''] : null;
        let qTools = questionTools?.length ? questionTools : null;
        Array.from(availableTools)?.forEach(tool => {
          if (!qTools.find(questionTool => questionTool === tool)) {
            qTools.push(tool);
          }
        });
        qTools = getAvailableTools(
          qTools,
          projectConfig,
          this.toolsWithLabels,
          measurementLabels,
          slice,
          subFormInfo.isSubForm
        );
        this.props.setAvailableLabels(qLabels, qTools);
      }
      const TaskIds = state.multipleReaderTask.TaskIds;
      const toolMode =
        TaskIds.length > 1 || hasActiveToolLimitReached(activeTool)
          ? 'passive'
          : 'active';
      Utils.cornerstoneUtils.setToolMode(activeTool, toolMode);
      if (!(TaskIds.length > 1)) {
        this.props.setActiveTool(activeTool);
      }
    } else {
      if (!this.state.isComplete) {
        const isStudyFormContainsMeasurementTools = projectConfig.studyForm?.components?.some(
          isMeasurementTools
        );

        if (!isStudyFormContainsMeasurementTools) {
          this.props.setAvailableLabels([''], measurementTools);
        }

        if (
          !projectConfig?.studyFormWorkflow ||
          (!availableLabels.length && !state.subForm?.isSubFormPopupEnabled)
        ) {
          if (
            projectConfig?.studyFormWorkflow === 'ROI' ||
            projectConfig?.studyFormWorkflow === 'Mixed'
          ) {
            const studyFormQuestionToolsList = getStudyFormActiveToolsForEnable(
              projectConfig,
              this.props,
              null,
              this.toolsWithLabels
            );
            const labelList =
              studyFormQuestionToolsList?.length &&
              state.subForm?.isSubFormPopupEnabled
                ? []
                : studyFormQuestionToolsList?.length &&
                  !state.subForm?.isSubFormPopupEnabled
                ? ['']
                : null;
            let toolList = studyFormQuestionToolsList?.length
              ? studyFormQuestionToolsList
              : null;
            toolList = getAvailableTools(
              toolList,
              projectConfig,
              this.toolsWithLabels,
              measurementLabels,
              slice
            );
            if (!readOnly) {
              this.props.setAvailableLabels(labelList, toolList);
            }
          } else {
            this.props.setAvailableLabels([], []);
          }

          if (availableLabels.length) {
            this.props.setActiveTool();
          }
        } else {
          Utils.cornerstoneUtils.setToolMode(availableToolsList[0], 'passive');
        }
      } else if (this.state.isComplete) {
        this.setState({ isComplete: false });
        this.props.setAvailableLabels(null, null);
      }
      this.props.setActiveTool(activeTool);
    }

    if (
      (projectConfig?.studyFormWorkflow === 'ROI' ||
        projectConfig?.studyFormWorkflow === 'Mixed') &&
      !state.subForm?.isSubFormPopupEnabled &&
      !subFormInfo.isSubForm
    ) {
      const currentActiveTool = selectActiveTool(store.getState());
      const studyFormQuestionTools = getStudyFormActiveToolsForEnable(
        projectConfig,
        this.props,
        null,
        this.toolsWithLabels
      );
      const label = studyFormQuestionTools?.length ? [''] : null;
      let tools = studyFormQuestionTools?.length
        ? studyFormQuestionTools
        : null;
      store.dispatch(setWorkFlowTools(tools));
      if (!tools.includes(currentActiveTool)) {
        tools = getAvailableTools(
          tools,
          projectConfig,
          this.toolsWithLabels,
          measurementLabels,
          slice
        );
        if (!readOnly) {
          this.props.setAvailableLabels(label, tools);
        }
        this.props.setActiveTool('Wwwc');
      }

      if (this.roiQuestionDialogId) {
        this.props.setAvailableLabels([''], ['DragProbe']);
        this.props.setActiveTool('Wwwc');
      }
    }
  }

  setAvailableLabels_ActiveTools_ValidateWhileSwitch = (
    projectConfig,
    timePointManager,
    notes,
    slice,
    measurementLabels,
    isSubForm,
    subFormQuestion,
    subFormName,
    subFormAnnotationId,
    measurementLabel,
    visibleQuestions,
    availableTools,
    currentRequired
  ) => {
    const state = store.getState();
    const isFile = isCurrentFile(state);
    let deletedMeasurementIsSubForm = isSubForm;
    let deletedMeasurementSubFormAnnotationId = subFormAnnotationId;
    let deletedMeasurementSubFormName = subFormName;
    let deletedMeasurementQuestion = subFormQuestion;
    let deletedMeasurementLabel = measurementLabel;
    let deletedMeasurementKey;
    let deletedMeasurementToolType;
    const lastDeletedMeasurementId = timePointManager.lastDeletedMeasurementId;
    const deletedMeasurement = getDeletedMeasurementDetail(
      timePointManager,
      lastDeletedMeasurementId
    );
    if (isSubForm) {
      deletedMeasurementIsSubForm = deletedMeasurement?.isSubForm;
      deletedMeasurementSubFormAnnotationId =
        deletedMeasurement?.subFormAnnotationId;
      deletedMeasurementSubFormName = deletedMeasurement?.subFormName;
      deletedMeasurementQuestion = deletedMeasurement?.question;
    }
    deletedMeasurementLabel = deletedMeasurement?.location;
    deletedMeasurementKey = deletedMeasurement?.questionKey;
    deletedMeasurementToolType = deletedMeasurement?.toolType;

    let deletedQuestion = {};
    if (deletedMeasurementIsSubForm) {
      const subFormComponents = visibleQuestions?.find(
        component => component.subFormName === deletedMeasurementSubFormName
      )?.components;
      deletedQuestion = subFormComponents?.find(
        question => question.key === deletedMeasurementKey
      );
    } else {
      if (deletedMeasurementQuestion) {
        deletedQuestion = visibleQuestions?.find(
          question => question.key === deletedMeasurementQuestion
        );
      } else {
        deletedQuestion = visibleQuestions?.find(question => {
          return question?.values?.some(v => {
            if (v.instructionSet?.length) {
              return v.instructionSet?.some(i =>
                i.requireMeasurements?.includes(deletedMeasurementLabel)
              );
            } else {
              return v.requireMeasurements?.includes(deletedMeasurementLabel);
            }
          });
        });
      }
    }

    if (deletedQuestion) {
      this.setState({ currentQuestion: deletedQuestion });
    }
    let deletedMeasurementTool = [];
    deletedMeasurementTool.push(deletedMeasurementToolType);
    store.dispatch(setMeasurementDeleted(false));
    const labelList = projectConfig?.labels?.map(label => label.value);
    let answerValue = '';
    if (deletedMeasurementIsSubForm) {
      answerValue =
        !isFile && projectConfig.bSlices
          ? notes.slices[slice]?.[deletedMeasurementQuestion]?.[
              deletedMeasurementSubFormName
            ]?.find(
              item =>
                item.annotationId === deletedMeasurementSubFormAnnotationId
            )?.[deletedMeasurementKey]
          : notes[deletedMeasurementQuestion]?.[
              deletedMeasurementSubFormName
            ]?.find(
              item =>
                item.annotationId === deletedMeasurementSubFormAnnotationId
            )?.[deletedMeasurementKey];
      store.dispatch(
        setActiveQuestion({
          ...deletedQuestion,
          answer: answerValue,
          questionKey: deletedMeasurementKey,
          isSubForm: true,
          subFormName: deletedMeasurementSubFormName,
          subFormAnnotationId: deletedMeasurementSubFormAnnotationId,
          question: deletedMeasurementQuestion,
        })
      );
    } else {
      answerValue =
        !isFile && projectConfig.bSlices
          ? notes.slices[slice]?.[deletedMeasurementKey]
          : notes[deletedMeasurementKey];
      store.dispatch(
        setActiveQuestion({
          ...deletedQuestion,
          answer: answerValue,
          questionKey: deletedMeasurementKey,
          isSubForm: false,
          subFormName: '',
          subFormAnnotationId: '',
          question: '',
        })
      );
    }
    const selectedOption = deletedQuestion?.values?.find(
      x => x.value === answerValue
    );
    const {
      questionTools,
      questionLabels,
    } = getToolsAndLabelsForQuestionAnswer(
      deletedQuestion,
      selectedOption,
      slice
    ); // Should 'slice' be deleted measurement.sliceNumber?

    if (!labelList.includes(deletedMeasurementLabel)) {
      deletedMeasurementTool.push('DragProbe');
      this.props.setActiveTool(this.state.previousTool);
      this.validateWhileQuestionSwitch();
    }
    questionLabels.forEach(x => currentRequired.add(x));
    questionTools.forEach(x => availableTools.add(x));
    this.props.setActiveTool(deletedMeasurementToolType);
    return {
      deletedMeasurementLabel: questionLabels,
      deletedMeasurementTool: questionTools,
    };
  };

  validateWhileQuestionSwitch = () => {
    const props = {
      ...this.props,
      previousVisibleQuestion: this.state.previousQuestion,
      sliceInfo: this.props.sliceInfo,
    };
    const visibleQuestions = getVisibleQuestions(this.state, props);
    const qValidation = validateWhileQuestionSwitches(
      this.props,
      visibleQuestions,
      this.state.notes
    );
    if (qValidation !== null) {
      this.setState({ questionValidation: qValidation });
    }
  };

  getActiveTool(tools) {
    let activeTool = selectActiveTool(store.getState());
    const state = this.state;
    if (
      (state.currentQuestion?.key !== state.previousQuestion?.key &&
        state.previousQuestion?.values?.some(x => x?.measurementTools)) ||
      (state.currentType && !tools?.includes(activeTool))
    ) {
      activeTool = tools?.length ? tools[0] : activeTool;
    }
    return activeTool;
  }

  saveWhileQuestionSwitches = () => {
    const { projectConfig } = this.props;
    const allowDraft = projectConfig?.allowDraft !== false;
    const state = store.getState();
    const slice = this.state.currentSliceInfo?.sliceNumber || 1;
    if (allowDraft) {
      let questions = {};
      if (!isCurrentFile(state) && projectConfig?.bSlices) {
        questions.slices = {};
        const bSlices = getBSlices(projectConfig);
        const formState = _.cloneDeep(this.state);
        for (const sliceNo of bSlices) {
          formState.currentSliceInfo.sliceNumber = sliceNo;
          questions.slices[sliceNo] = getVisibleQuestions(
            formState,
            this.props
          );
        }
      } else {
        const props = {
          ...this.props,
          previousVisibleQuestion: this.state.previousQuestion,
          sliceInfo: this.props.sliceInfo,
        };
        questions = getVisibleQuestions(this.state, props);
      }
      const { notes } = draftStudyForm(questions, this.state, this.props);
      const readTime = this.props.getReadTime();
      const prevStatus = this.state.readerTaskStatus;
      const { taskId } = getRouteParams();
      this.updateSessionNotes(notes);
      updateComponentByPromise(
        {
          init: {
            errors: {},
            draftSave: 'Saving...',
            draftMessage: 'Please complete required items.',
          },
          success: { draftSave: 'Saved' },
          error: {
            draftSave: 'Problems saving',
          },
          default: { draftSave: null },
        },
        this.props.saveStudy.bind(
          undefined,
          this.state.readOnly ? null : notes,
          readTime,
          readStatus.Inprogress,
          slice
        ),
        (state, error) => {
          if (error && error.code !== 422) {
            this.props.showUserErrorMessage(error);
          }
          if (state.draftSave === 'Saving...') {
            state.isStateUpdate = true;
          } else {
            if (state.draftSave === 'Saved' && taskId) {
              sendTaskSignals(
                taskId,
                readStatus.Inprogress,
                prevStatus,
                this.props,
                readStatus
              );
            }
            state.isStateUpdate = false;
          }
        }
      );
      this.setState(prevState => ({
        ...prevState,
        isComplete: false,
        submit: 'Submit',
        readerTaskStatus: readStatus.Inprogress,
        notes: notes,
      }));
    }
  };

  noteUpdateForROI = (
    question,
    subFormQuestion = null,
    annotationId = null,
    subFormName = null,
    currentType = null
  ) => {
    const { key, type } = question;
    const { projectConfig } = this.props;
    const value = currentType?.value;
    this.onValueChangeStudyNoteUpdation(
      question,
      subFormQuestion,
      annotationId,
      subFormName,
      value,
      key,
      type,
      projectConfig
    );
  };

  updateSessionNotes(notes) {
    const state = store.getState();
    const isFile = isCurrentFile(state);
    const readerTaskResponse = _.cloneDeep(
      state.flywheel.readerTaskWithFormResponse
    );
    const featureConfig = state.flywheel.featureConfig;
    const { taskId } = getRouteParams();

    if (featureConfig?.features?.reader_tasks && taskId) {
      this.updateReaderTaskResponse(readerTaskResponse, notes);
    } else if (!isFile) {
      this.updateSession(notes);
    }
  }

  updateReaderTaskResponse(readerTaskResponse, notes) {
    const readTime = this.props.getReadTime();
    const shouldIgnore = shouldIgnoreUserSettings(store.getState());
    readerTaskResponse.info = {
      orientations: readerTaskResponse.info?.orientations || [],
      date: new Date().toISOString(),
      readTime: readTime,
      readStatus: readStatus.Inprogress,
      displayProperties: shouldIgnore
        ? readerTaskResponse.info.displayProperties
        : null,
    };
    readerTaskResponse.info.notes = notes;
    store.dispatch(setReaderTaskWithFormResponse(readerTaskResponse));
  }

  updateSession(notes) {
    const state = store.getState();
    const sessions = selectActiveSessions(state);
    let currentDataInfo;
    let isUpdateRequired = false;

    if (sessions) {
      for (let key in notes) {
        if (notes[key] === undefined) {
          delete notes[key];
        }
      }
      const updatedSessions = [];
      sessions.forEach((session, index) => {
        // Pushing all sessions to avoid missing session while updating to store
        updatedSessions.push({ ...session });
        currentDataInfo = session.info;
        if (!currentDataInfo) {
          return;
        }
        const requestData = {
          ...currentDataInfo.ohifViewer,
        };
        const user = selectUser(state);
        if (!user) {
          return;
        }
        if (requestData.read) {
          const userInfo = requestData.read[customInfoKey(user._id)];
          const readTime = this.props.getReadTime();
          requestData.read = {
            ...requestData.read,
            [customInfoKey(user._id)]: {
              date: new Date().toISOString(),
              readTime: userInfo ? userInfo.readTime : readTime,
              notes: notes,
              readStatus: userInfo
                ? userInfo.Inprogress
                : readStatus.Inprogress,
            },
          };
          updatedSessions[index] = {
            ...session,
            info: { ...currentDataInfo, ohifViewer: requestData },
          };
          // Update if modification in at least one session.
          isUpdateRequired = true;
        }
      });
      if (isUpdateRequired) {
        store.dispatch(setActiveSessions(updatedSessions));
      }
    }
  }

  clickType = (
    option,
    key,
    questionKey = null,
    subFormName = null,
    annotationId = null,
    shouldTriggerAutoSave = true
  ) => {
    let qOption = { ...option };
    const { projectConfig } = this.props;
    const slice = this.state.currentSliceInfo?.sliceNumber || 1;
    const state = store.getState();
    if (
      (projectConfig?.studyFormWorkflow === 'Mixed' ||
        projectConfig?.studyFormWorkflow === 'ROI') &&
      ((this.state.subFormPanel?.[slice] &&
        Object.keys(this.state.subFormPanel?.[slice]).length) ||
        (!projectConfig?.bSlices &&
          this.state.subFormPanel &&
          Object.keys(this.state.subFormPanel).length))
    ) {
      const isInSubForm = questionKey !== null;

      store.dispatch(subFormQuestionAnswered(isInSubForm));

      if (projectConfig?.studyFormWorkflow === 'Mixed') {
        const questionKeyValue = {
          key: key,
          value: qOption.value,
        };
        store.dispatch(studyFormAnswerInMixedMode(questionKeyValue));
      }
    }
    const isFile = isCurrentFile(state);

    if (questionKey === null) {
      if (
        !isFile &&
        projectConfig?.bSlices?.settings[slice]?.measurementTools &&
        !(qOption?.directive || qOption?.instructionSet)
      ) {
        if (projectConfig?.bSlices?.settings[slice]?.measurementTools[key]) {
          qOption.measurementTools =
            projectConfig?.bSlices?.settings[slice]?.measurementTools[key];
        }
      }

      if (qOption?.instructionSet) {
        qOption.instructionSet.forEach(x => {
          if (!qOption.measurementTools) {
            qOption.measurementTools = [];
          }
          if (!qOption.requireMeasurements) {
            qOption.requireMeasurements = [];
          }
          qOption.measurementTools = qOption.measurementTools.concat(
            x.measurementTools
          );
          qOption.requireMeasurements = qOption.requireMeasurements.concat(
            x.requireMeasurements
          );
        });
      }
    }

    this.setState(prevState => ({
      ...prevState,
      currentType: qOption,
      currentKey: key,
      isSubFormQuestion: !_.isEmpty(questionKey),
      subFormName: questionKey === null ? '' : subFormName,
      subFormQuestion: questionKey === null ? '' : questionKey,
      subFormAnnotationId: questionKey === null ? '' : annotationId,
      currentAvailableLabels: null,
      currentAvailableTools: null,
      currentTool: null,
    }));
    if (
      (projectConfig?.studyFormWorkflow === 'ROI' ||
        projectConfig?.studyFormWorkflow === 'Mixed') &&
      subFormName &&
      (state.subForm?.isROIEdit || store.getState().subForm?.isSubFormQuestion)
    ) {
      const subFormQuestion = projectConfig.studyForm?.subForms?.[
        subFormName
      ]?.components?.find(x => x.key === key);
      const question = projectConfig.studyForm?.components?.find(
        x => x.key === questionKey
      );
      const data = {
        question: question,
        subFormQuestion: subFormQuestion,
        annotationId: annotationId,
      };
      this.noteUpdateForROI(
        data.subFormQuestion,
        data.question,
        data.annotationId,
        subFormName,
        qOption
      );
    }

    if (shouldTriggerAutoSave) {
      setTimeout(() => {
        this.saveWhileQuestionSwitches();
      }, 100);
    }
  };

  onDraftStudy = () => {
    const { UIDialogService } = this.props.servicesManager.services;
    const dialogWidth = 500;
    const dialogId = UIDialogService.create({
      content: SimpleDialog,
      defaultPosition: {
        x: window.innerWidth / 2 - dialogWidth / 2,
        y: window.innerHeight / 2 - 220,
      },
      showOverlay: true,
      contentProps: {
        componentStyle: { width: dialogWidth + 'px' },
        headerTitle: 'Confirm Draft?',
        showFooterButtons: true,
        confirmButtonTitle: 'Proceed',
        enableConfirm: true,
        children:
          'Saving form progress does not submit your completed study for review. You will be able to open the study in its current state and continue your evaluation at any time. Would you like to proceed?',
        onClose: () => UIDialogService.dismiss({ id: dialogId }),
        onConfirm: () => {
          UIDialogService.dismiss({ id: dialogId });
          const { projectConfig } = this.props;
          let questions = {};
          const state = store.getState();
          const isFile = isCurrentFile(state);
          if (!isFile && projectConfig.bSlices) {
            questions.slices = {};
            const bSlices = getBSlices(projectConfig);
            const formState = _.cloneDeep(this.state);
            for (const sliceNo of bSlices) {
              formState.currentSliceInfo.sliceNumber = sliceNo;
              questions.slices[sliceNo] = getVisibleQuestions(
                formState,
                this.props
              );
            }
          } else {
            const props = {
              ...this.props,
              previousVisibleQuestion: this.state.previousQuestion,
              sliceInfo: this.props.sliceInfo,
            };
            questions = getVisibleQuestions(this.state, props);
          }
          const { errors, notes } = validateForm(
            questions,
            this.state.notes,
            this.props.measurementLabels,
            projectConfig,
            this.props,
            this.state
          );
          if (!isFile && projectConfig.bSlices) {
            const bSlices = getBSlices(projectConfig);
            for (const sliceNo of bSlices) {
              if (
                errors.slices[sliceNo] &&
                Object.keys(errors.slices[sliceNo]).length > 0
              ) {
                this.setState({
                  isComplete: false,
                  submit: 'Submit',
                  readerTaskStatus: readStatus.Inprogress,
                  questionValidation: true,
                });
              }
            }
          } else {
            if (Object.keys(errors).length > 0) {
              this.setState({
                isComplete: false,
                submit: 'Submit',
                readerTaskStatus: readStatus.Inprogress,
                questionValidation: true,
              });
            }
          }
          const readTime = this.props.getReadTime();
          const slice = this.state.currentSliceInfo?.sliceNumber || 1;
          const prevStatus = this.state.readerTaskStatus;
          const { taskId } = getRouteParams();
          const displayProperties =
            taskId && !shouldIgnoreUserSettings(state)
              ? getDisplayProperties(this.props.commandsManager, state)
              : null;
          updateComponentByPromise(
            {
              init: {
                errors: {},
                draftSave: 'Saving...',
                draftMessage: 'Please complete required items.',
              },
              success: { draftSave: 'Saved', draftMessage: '' },
              error: {
                draftSave: 'Problems saving',
              },
              default: { draftSave: null, draftMessage: '' },
            },
            this.props.saveStudy.bind(
              undefined,
              this.state.readOnly ? null : notes,
              readTime,
              readStatus.Inprogress,
              slice,
              displayProperties
            ),
            (state, error) => {
              if (error) {
                this.props.showUserErrorMessage(error);
              }
              if (state.draftSave === 'Saved' && taskId) {
                sendTaskSignals(
                  taskId,
                  readStatus.Inprogress,
                  prevStatus,
                  this.props,
                  readStatus
                );
              }
              this.setState(state);
            }
          );
          this.setState({
            notes: notes,
          });
        },
      },
    });
  };

  onCompleteStudy = () => {
    const { projectConfig } = this.props;
    let questions = {};
    const state = store.getState();
    const isFile = isCurrentFile(state);
    const props = {
      ...this.props,
      previousVisibleQuestion: this.state.previousQuestion,
      sliceInfo: this.props.sliceInfo,
    };
    if (!isFile && projectConfig.bSlices) {
      questions.slices = {};
      const bSlices = getBSlices(projectConfig);
      const formState = _.cloneDeep(this.state);
      for (const sliceNo of bSlices) {
        formState.currentSliceInfo.sliceNumber = sliceNo;
        questions.slices[sliceNo] = getVisibleQuestions(formState, props);
      }
    } else {
      questions = getVisibleQuestions(this.state, props);
    }
    const { errors, notes, allLabels, allTools } = validateForm(
      questions,
      this.state.notes,
      this.props.measurementLabels,
      projectConfig,
      this.props,
      this.state
    );
    if (!isFile && projectConfig.bSlices) {
      const bSlices = getBSlices(projectConfig);
      const requiredError = bSliceRequiredValidation(
        projectConfig,
        bSlices,
        this.state.notes
      );
      const data = bSliceQuestionValidation(requiredError, projectConfig);
      if (data !== '') {
        this.setState({
          errors,
          save: 'Validation Failed',
          draftMessage: data,
          questionValidation: false,
        });
        setTimeout(() => {
          this.setState({ save: null });
        }, 2000);

        this.props.setAvailableLabels(allLabels, allTools);
        this.props.setActiveTool();
        return;
      }
      const visibleQuestions = getVisibleQuestions(this.state, props);
      for (const sliceNo of bSlices) {
        if (
          errors.slices?.[sliceNo] &&
          Object.keys(errors.slices?.[sliceNo]).length > 0
        ) {
          this.errorCreation(
            true,
            errors,
            sliceNo,
            allLabels,
            allTools,
            visibleQuestions,
            this.questionRefs
          );
          return;
        }
      }
    } else {
      if (Object.keys(errors).length > 0) {
        const visibleQuestions = getVisibleQuestions(this.state, props);
        this.errorCreation(
          false,
          errors,
          0,
          allLabels,
          allTools,
          visibleQuestions,
          this.questionRefs
        );
        return;
      }
    }
    const readTime = this.props.getReadTime();
    const slice = this.state.currentSliceInfo?.sliceNumber || 1;
    const prevStatus = this.state.readerTaskStatus;
    const { taskId } = getRouteParams();
    const displayProperties =
      taskId && !shouldIgnoreUserSettings(state)
        ? getDisplayProperties(this.props.commandsManager, state)
        : null;
    updateComponentByPromise(
      {
        init: { errors: {}, save: 'Saving...' },
        success: { save: 'Saved' },
        error: { save: 'Problems saving' },
        default: { save: null },
      },
      this.props.saveStudy.bind(
        undefined,
        this.state.readOnly ? null : notes,
        readTime,
        readStatus.Complete,
        slice,
        displayProperties
      ),
      (state, error) => {
        if (error) {
          this.props.showUserErrorMessage(error);
        }
        if (state.save === 'Saved' && taskId) {
          sendTaskSignals(
            taskId,
            readStatus.Complete,
            prevStatus,
            this.props,
            readStatus
          );
        }
        this.setState(state);
      }
    );
    this.setState({
      isComplete: true,
      submit: 'Resubmit',
      readerTaskStatus: readStatus.Complete,
      questionValidation: true,
      notes: notes,
    });
  };

  errorCreation = (
    isbSlice,
    errors,
    sliceNo,
    allLabels,
    allTools,
    visibleQuestions,
    questionRefs
  ) => {
    const errorData = errorSetting(
      isbSlice,
      errors,
      sliceNo,
      visibleQuestions,
      questionRefs,
      this.props,
      this.state.notes
    );

    if (errorData?.isbSlice) {
      this.setState({
        errors,
        save: 'Validation Failed',
        draftMessage: errorData?.message,
        questionValidation: false,
      });
    } else {
      this.setState({
        errors,
        save: 'Validation Failed',
        draftMessage: errorData?.message,
        questionValidation: false,
      });
    }

    setTimeout(() => {
      this.setState({ save: null });
    }, 2000);

    let availableTools = allTools;
    if (this.props.projectConfig.studyFormWorkflow) {
      const studyFormQuestionTools = getStudyFormActiveToolsForEnable(
        this.props.projectConfig,
        this.props,
        null,
        this.toolsWithLabels
      );
      let tools = studyFormQuestionTools?.length
        ? studyFormQuestionTools
        : null;
      availableTools = tools;
    }

    this.props.setAvailableLabels(allLabels, availableTools);
    this.props.setActiveTool();
  };

  confirmNavigation = callback => {
    const { projectConfig } = this.props;
    const savedNotes =
      (this.props.sessionRead && this.props.sessionRead.notes) || {};
    let questions = {};
    const state = store.getState();
    const isFile = isCurrentFile(state);
    if (!isFile && projectConfig.bSlices) {
      questions.slices = {};
      const bSlices = getBSlices(projectConfig);
      const formState = _.cloneDeep(this.state);
      for (const sliceNo of bSlices) {
        formState.currentSliceInfo.sliceNumber = sliceNo;
        questions.slices[sliceNo] = getVisibleQuestions(formState, this.props);
      }
    } else {
      const props = {
        ...this.props,
        previousVisibleQuestion: this.state.previousQuestion,
        sliceInfo: this.props.sliceInfo,
      };
      questions = getVisibleQuestions(this.state, props);
    }
    const { errors } = validateForm(
      questions,
      this.state.notes,
      this.props.measurementLabels,
      this.props.projectConfig
    );
    if (!isFile && projectConfig.bSlices) {
      const bSlices = getBSlices(projectConfig);
      for (const sliceNo of bSlices) {
        if (
          errors.slices[sliceNo] &&
          this.state.notes.slices[sliceNo] &&
          ((Object.keys(this.state.notes.slices[sliceNo]).length &&
            Object.keys(errors.slices[sliceNo]).length) ||
            !isEqual(savedNotes, this.state.notes))
        ) {
          this.setState({
            confirmCallback: () => {
              callback();
              this.setState({ confirmCallback: null });
            },
          });
        } else {
          callback();
        }
      }
    } else {
      if (
        (Object.keys(this.state.notes).length && Object.keys(errors).length) ||
        !isEqual(savedNotes, this.state.notes)
      ) {
        this.setState({
          confirmCallback: () => {
            callback();
            this.setState({ confirmCallback: null });
          },
        });
      } else {
        callback();
      }
    }
  };

  navigateStudy = offset => {
    const { StudyInstanceUID, studyList } = this.props;
    const { projectId } = this.props.match.params;
    const index = studyList.ordering.findIndex(uid => uid === StudyInstanceUID);
    const nextUid = studyList.ordering[index + offset];
    if (nextUid) {
      this.props.history.push({
        pathname: `/project/${projectId}/viewer/${nextUid}`,
      });
    } else {
      this.props.history.push({ pathname: `/project/${projectId}` });
    }
  };

  cleanHTML = html => {
    return html.replace(/<script[^>]*>[^<]*<\/script>/gm, '');
  };

  onAdvanceStudy = () => this.navigateStudy(1);
  onPreviousStudy = () => this.navigateStudy(-1);

  onClickQuestionValues(question, isSubForm = false, subFormName = null) {
    this.previousVisibleQuestion = question;
    const props = {
      ...this.props,
      previousVisibleQuestion: this.state.previousQuestion,
      sliceInfo: this.props.sliceInfo,
    };
    selectQuestionValues(question, props, subFormName, isSubForm, this.state);
  }

  // To Show the color selector dialog
  onClickColorPicker = (event, measurementData) => {
    const color = clickColorPicker(
      window,
      event,
      measurementData,
      DEFAULT_COLOR,
      this.props
    );

    this.setState({
      showColorPicker: !this.state.showColorPicker,
      colorPickCoordinates: color.colorPickCoordinates,
      measurementData,
      selectedColor: color.selectedColor,
    });
  };

  closeColorPicker = () => {
    this.setState({ showColorPicker: false });
  };

  isErrorMessage = message => {
    return message === 'Problems saving';
  };

  render() {
    const { measurementLabels, projectConfig, sessionRead } = this.props;
    const { confirmCallback, readOnly, readerTaskStatus } = this.state;
    const { taskId } = getRouteParams();
    let saveButtonText = this.state.submit ? this.state.submit : 'Submit';
    let draftButtonText = 'Draft';
    if (this.state.save) {
      saveButtonText = this.state.save;
    } else if (this.state.draftSave) {
      draftButtonText = this.state.draftSave;
    } else if (
      sessionRead &&
      sessionRead.notes &&
      Object.keys(sessionRead.notes).length &&
      !readOnly
    ) {
      if (
        (!taskId && sessionRead?.readStatus === readStatus.Inprogress) ||
        (taskId && readerTaskStatus === readStatus.Inprogress)
      ) {
        saveButtonText = 'Submit';
      } else if (
        (!taskId && sessionRead?.readStatus === readStatus.Complete) ||
        (taskId && readerTaskStatus === readStatus.Complete)
      ) {
        saveButtonText = 'Resubmit';
      }
      draftButtonText = 'Draft';
    }
    const slice = this.state.currentSliceInfo?.sliceNumber || 1;
    const draftMessage = this.state.draftMessage;
    const allowDraft = projectConfig?.allowDraft !== false;
    const showNavButtons = projectConfig.showStudyList !== false; // show if true or undefined
    const formStyles = {
      pointerEvents: isFormViewOnly(this.state) ? 'none' : 'auto',
      opacity: isFormViewOnly(this.state) ? 0.5 : 1,
    };
    let isSaveDisabled =
      this.state.save ||
      (isFormViewOnly(this.state) && isAnnotationsViewOnly(this.state));
    const props = {
      ...this.props,
      previousVisibleQuestion: this.state.previousQuestion,
      sliceInfo: this.props.sliceInfo,
    };
    const state = store.getState();
    const multipleTaskNotes = { ...state.viewerAvatar.multipleTaskNotes };
    const taskIds = state.multipleReaderTask?.TaskIds;
    const multipleTaskUser = !state.viewerAvatar?.multipleTaskUser
      ? []
      : [...state.viewerAvatar.multipleTaskUser];
    const visibleQuestions = getVisibleQuestions(this.state, props);
    const displayStyle = this.props.showStudyFormButtonsOnly ? 'none' : '';
    const isMultipleReaderTask = taskIds.length > 1;
    const singleAvatar = state.viewerAvatar.singleAvatar;
    if (isMultipleReaderTask && !singleAvatar) {
      isSaveDisabled = true;
    }
    return (
      <>
        <div
          key={`${projectConfig.studyFormWorkflow}`}
          className="study-completion-form"
          // Special handling to clear the focus from StudyForm on leaving mouse
          onMouseLeave={() => document.activeElement?.blur()}
        >
          <div
            style={{
              display: displayStyle,
            }}
          >
            <Header
              title="Viewer Form"
              measurements={this.props.measurements}
              slice={slice}
            />
          </div>
          <div
            style={{
              display: displayStyle,
            }}
            className="study-form-scrollable"
            ref={element => (this.scrollableRef = element)}
          >
            {visibleQuestions.map((question, index) =>
              this.studyQuestionAndAnswer(
                question,
                index,
                measurementLabels,
                readOnly,
                slice,
                formStyles,
                projectConfig,
                multipleTaskNotes,
                taskIds,
                multipleTaskUser
              )
            )}
          </div>
          <div className="study-completion-buttons">
            {(showNavButtons && !isMultipleReaderTask) ||
            (showNavButtons && isMultipleReaderTask && singleAvatar) ? (
              <button
                type="button"
                onClick={() => this.confirmNavigation(this.onPreviousStudy)}
                className="study-complete-button"
                style={{ flex: 1 }}
              >
                &lt;&lt; {this.props.t('Back')}
              </button>
            ) : null}
            {allowDraft ? (
              <React.Fragment>
                <button
                  type="button"
                  onClick={this.onDraftStudy}
                  disabled={isSaveDisabled}
                  className="study-complete-button"
                  style={{
                    flex: 1,
                    opacity: isSaveDisabled ? '0.5' : '1',
                    pointerEvents: isSaveDisabled ? 'none' : 'auto',
                    fontWeight: this.isErrorMessage(draftButtonText)
                      ? 'bold'
                      : 'normal',
                  }}
                >
                  {this.props.t(draftButtonText)}
                </button>
                {!this.state.questionValidation && draftMessage ? (
                  <OverlayTrigger
                    key="menu-button"
                    placement="top"
                    rootClose={true}
                    overlay={
                      <Tooltip
                        placement="left"
                        className="in tooltip-warning"
                        id="tooltip-left"
                      >
                        <>
                          <div className="warningTitle">Validation Failed</div>
                          <div className="warningContent">
                            {this.props.t(draftMessage)}
                          </div>{' '}
                        </>
                      </Tooltip>
                    }
                  >
                    <button
                      type="button"
                      onClick={this.onCompleteStudy}
                      disabled={isSaveDisabled}
                      className="study-complete-button"
                      style={{
                        flex: 1,
                        opacity: '0.5',
                        pointerEvents: isSaveDisabled ? 'none' : 'auto',
                      }}
                    >
                      {this.props.t(saveButtonText)}
                    </button>
                  </OverlayTrigger>
                ) : (
                  <button
                    type="button"
                    onClick={this.onCompleteStudy}
                    disabled={isSaveDisabled}
                    className="study-complete-button"
                    style={{
                      flex: 1,
                      opacity: isSaveDisabled ? '0.5' : '1',
                      pointerEvents: isSaveDisabled ? 'none' : 'auto',
                    }}
                  >
                    {this.props.t(saveButtonText)}
                  </button>
                )}
              </React.Fragment>
            ) : null}
            {!allowDraft ? (
              <button
                type="button"
                onClick={this.onCompleteStudy}
                disabled={isSaveDisabled}
                className="study-complete-button"
                style={{ flex: 2 }}
              >
                {this.props.t(saveButtonText)}
              </button>
            ) : null}
            {(showNavButtons && !isMultipleReaderTask) ||
            (showNavButtons && isMultipleReaderTask && singleAvatar) ? (
              <button
                type="button"
                onClick={() => this.confirmNavigation(this.onAdvanceStudy)}
                className="study-complete-button"
                style={{ flex: 1 }}
              >
                {this.props.t('Next')} &gt;&gt;
              </button>
            ) : null}
          </div>
        </div>
        {confirmCallback && (
          <SimpleDialog
            componentStyle={{ bottom: 0, right: 0, top: 'auto', left: 'auto' }}
            headerTitle="Unsaved Changes"
            onClose={() => this.setState({ confirmCallback: null })}
            onConfirm={confirmCallback}
            showFooterButtons={true}
          >
            Unsaved changes from the form will be lost. Would you like to
            proceed?
          </SimpleDialog>
        )}
        <ColorPickerDialog
          closeColorPicker={this.closeColorPicker}
          showColorPicker={this.state.showColorPicker}
          measurementData={this.state.measurementData}
          colorPickCoordinates={this.state.colorPickCoordinates}
          selectedColor={this.state.selectedColor}
          {...this.props}
        />
      </>
    );
  }

  getInstructionSet(question, slice, sliceInfo) {
    if (
      !question.values ||
      (question &&
        question.values &&
        !question.values.some(x => x.instructionSet || x.directive))
    ) {
      return null;
    }
    if (!question.values.some(x => x.instructionSet || x.directive)) {
      return null;
    }
    const questionAnswer = this.state.notes?.slices
      ? this.state.notes.slices[slice]?.[question.key] || ''
      : this.state.notes[question.key] || '';

    let qn;
    if (typeof questionAnswer === 'object') {
      qn = question.values.find(x =>
        Object.keys(x).find(
          key =>
            (key === 'directive' || key === 'instructionSet') &&
            (questionAnswer.value === x.label ||
              questionAnswer.value === x.value)
        )
      );
    } else {
      qn = question.values.find(x =>
        Object.keys(x).find(
          key =>
            (key === 'directive' || key === 'instructionSet') &&
            (questionAnswer === x.label || questionAnswer === x.value)
        )
      );
    }

    if (qn?.directive) {
      return (
        <ConnectedInstructionContainer
          instruction={qn}
          question={question}
          notes={this.state.notes}
          sliceInfo={sliceInfo}
          servicesManager={this.props.servicesManager}
          groupClickStatus={this.state.showInstructionColorPicker}
          onGroupClick={state =>
            this.setState({ showInstructionColorPicker: { ...state } })
          }
          onClickColorPicker={this.onClickColorPicker}
          commandsManager={this.props.commandsManager}
          errors={
            this.state.errors.slices &&
            this.state.errors.slices[slice] &&
            this.state.errors.slices[slice][question.key]
              ? this.state.errors.slices[slice][question.key]
              : this.state.errors[question.key]
          }
          instructionSet={[]}
          measurements={this.props.measurements}
        />
      );
    }
    if (qn?.instructionSet) {
      return qn.instructionSet.map((x, index) => (
        <ConnectedInstructionContainer
          key={index}
          notes={this.state.notes}
          instructionIndex={index}
          instruction={x}
          question={question}
          sliceInfo={sliceInfo}
          servicesManager={this.props.servicesManager}
          commandsManager={this.props.commandsManager}
          groupClickStatus={this.state.showInstructionColorPicker}
          onGroupClick={state =>
            this.setState({ showInstructionColorPicker: { ...state } })
          }
          onClickColorPicker={this.onClickColorPicker}
          errors={
            this.state.errors.slices &&
            this.state.errors.slices[slice] &&
            this.state.errors.slices[slice][question.key]
              ? this.state.errors.slices[slice][question.key]
              : this.state.errors[question.key]
          }
          instructionSet={qn.instructionSet}
          measurements={this.props.measurements}
        />
      ));
    }
    return null;
  }

  subFormPanelSwitch = (questionKey, slice, projectConfig) => {
    const state = store.getState();
    let subFormPanel = { ...this.state.subFormPanel };
    if (!isCurrentFile(state) && projectConfig.bSlices) {
      if (!subFormPanel[slice]) {
        subFormPanel[slice] = {};
      }
      subFormPanel[slice][questionKey] = !subFormPanel[slice]?.[questionKey];
    } else {
      subFormPanel[questionKey] = !subFormPanel?.[questionKey];
    }
    this.setState({ subFormPanel });
  };

  subFormTabSwitch = (keyIndex, key, slice, projectConfig, tabAnnotationId) => {
    const state = store.getState();
    let subFormTab = { ...this.state.subFormTab };
    let subFormTabWithAnnotationId = {
      ...this.state.subFormTabWithAnnotationId,
    };
    if (!isCurrentFile(state) && projectConfig.bSlices) {
      if (!subFormTab[slice]) {
        subFormTab[slice] = {};
      }
      subFormTab[slice][key] = keyIndex;

      if (!subFormTabWithAnnotationId[slice]) {
        subFormTabWithAnnotationId[slice] = {};
      }
      subFormTabWithAnnotationId[slice][key] = tabAnnotationId;
    } else {
      subFormTab[key] = keyIndex;

      subFormTabWithAnnotationId[key] = tabAnnotationId;
    }
    this.setState({ subFormTab, subFormTabWithAnnotationId });
  };

  studyQuestionAndAnswer(
    question,
    index,
    measurementLabels,
    readOnly,
    slice,
    formStyles,
    projectConfig,
    multipleTaskNotes,
    taskIds,
    multipleTaskUser
  ) {
    const answerAndTabData = getQuestionAnswerAndTabDetails(
      projectConfig,
      this.state,
      slice,
      question
    );
    const subFormTabs = answerAndTabData.subFormTabs;
    const questionAnswer = answerAndTabData.questionAnswer;
    const state = store.getState();
    const isFile = isCurrentFile(state);
    if (
      (projectConfig?.studyFormWorkflow === 'Form' ||
        projectConfig?.studyFormWorkflow === 'Mixed') &&
      question.components?.length > 0 &&
      subFormTabs?.[question.key] &&
      question.positiveAnswers?.includes(questionAnswer)
    ) {
      let tabs = [];
      const subFormTabList =
        !isFile && projectConfig.bSlices
          ? question[question.key][slice]
          : question[question.key];
      for (let i = 0; i < subFormTabList.length; i++) {
        let tab = {
          name: i + 1,
          annotationId: subFormTabList[i].annotationId,
        };
        tabs.push(tab);
      }
      const subFormPanel =
        !isFile && projectConfig.bSlices
          ? this.state.subFormPanel[slice]
          : this.state.subFormPanel;
      return (
        <div key={`${index}_${question.key}`} className="subFormHeader">
          <SubFormPanel
            subFormPanelSwitch={() =>
              this.subFormPanelSwitch(question.key, slice, projectConfig)
            }
            slice={slice}
            question={question}
            studyFormState={this.state}
            t={this.props.t}
            projectConfig={projectConfig}
            measurements={this.props.measurements}
            readOnly={readOnly}
          />
          {subFormPanel?.[question.key] && (
            <div className="TabComponents">
              <div className="TabComponents_tabHeader">
                <div className="TabComponents_tabHeader_selector">
                  <div className="dialog-separator-after">
                    <ul className="nav nav-tabs">
                      {tabs.map((tab, tabPanelIndex) => {
                        const tabAnnotationId = tab.annotationId;
                        return (
                          <SubFormTabHeader
                            key={`${question.key}_${tabPanelIndex}_${tabAnnotationId}`}
                            slice={slice}
                            index={tabPanelIndex}
                            question={question}
                            tab={tab}
                            tabMouseEnter={e => {
                              if (!dispatchActions) {
                                dispatchActions = require('@ohif/viewer/src/appExtensions/MeasurementsPanel/dispatchActions.js')
                                  .default;
                              }
                              const measurements = [];
                              measurementTools.forEach(tool => {
                                this.props.measurements?.[tool]?.forEach(
                                  annotation => {
                                    if (annotation?.uuid === tabAnnotationId) {
                                      annotation.measurementId = annotation._id;
                                      measurements.push(annotation);
                                    }
                                  }
                                );
                              });
                              store.dispatch(
                                setMeasurementColorIntensity(true)
                              );
                              dispatchActions?.dispatchActiveHoveredMeasurements(
                                measurements,
                                store.dispatch
                              );
                            }}
                            tabMouseLeave={e => {
                              store.dispatch(
                                setMeasurementColorIntensity(false)
                              );
                              dispatchActions?.dispatchActiveHoveredMeasurements(
                                [],
                                store.dispatch
                              );
                            }}
                            tabClick={() =>
                              this.subFormTabSwitch(
                                question.key + tabPanelIndex,
                                question.key,
                                slice,
                                projectConfig,
                                tabAnnotationId
                              )
                            }
                            studyFormState={this.state}
                            projectConfig={projectConfig}
                            readOnly={readOnly}
                          />
                        );
                      })}
                    </ul>
                  </div>
                </div>
              </div>
              {tabs.map((tab, tabIndex) => {
                const { annotationId } = tab;
                const subFormTab =
                  !isFile && projectConfig.bSlices
                    ? this.state.subFormTab[slice]
                    : this.state.subFormTab;
                const subFormComponent =
                  !isFile && projectConfig.bSlices
                    ? question[question.key][slice]
                    : question[question.key];
                return (
                  <div
                    key={`${tabIndex}_${annotationId}_${index}_${question.key}`}
                    className={classnames(
                      'TabComponents_content',
                      question.key + tabIndex === subFormTab?.[question.key] &&
                        'active'
                    )}
                  >
                    {subFormComponent[tabIndex].annotationId &&
                      question.components.map(
                        (subFormQuestion, subFormIndex) => {
                          const questionDisplay = isQuestionDisplay(
                            subFormQuestion,
                            annotationId,
                            slice,
                            question,
                            projectConfig,
                            this.state.notes
                          );
                          const noteData = getValueFromNoteAndSubFormName(
                            this.state,
                            slice,
                            question,
                            projectConfig,
                            annotationId
                          );
                          let notesForSubForm = noteData.notesForSubForm;
                          let subFormName = noteData.subFormName;
                          if (questionDisplay) {
                            return (
                              <SubFormTabContent
                                key={`${subFormQuestion.key}_${tabIndex}_${question.key}_${subFormIndex}`}
                                subFormQuestion={subFormQuestion}
                                index={tabIndex}
                                slice={slice}
                                question={question}
                                annotationId={annotationId}
                                projectConfig={projectConfig}
                                studyFormState={this.state}
                                questionRefs={this.questionRefs}
                                onValueChangeForSubForm={() =>
                                  this.onValueChange(
                                    subFormQuestion,
                                    question,
                                    annotationId
                                  )
                                }
                                formStyles={formStyles}
                                cleanHTML={() =>
                                  this.cleanHTML(subFormQuestion.html)
                                }
                                measurementLabels={measurementLabels}
                                radioOnClick={option => {
                                  this.clickType(
                                    option,
                                    subFormQuestion.key,
                                    question.key,
                                    subFormName,
                                    annotationId
                                  );
                                  this.onClickQuestionValues(
                                    subFormQuestion,
                                    true,
                                    subFormName
                                  );
                                }}
                                selectBoxOnClick={e => {
                                  this.clickType(
                                    subFormQuestion.values.find(
                                      x =>
                                        x.value === e.target.value ||
                                        x.label === e.target.value
                                    ),
                                    subFormQuestion.key,
                                    question.key,
                                    subFormName,
                                    annotationId
                                  );
                                  this.onClickQuestionValues(
                                    subFormQuestion,
                                    true,
                                    subFormName
                                  );
                                }}
                                onClickQuestionValues={(question, option) => {
                                  this.clickType(option, question.key);
                                  this.onClickQuestionValues(
                                    subFormQuestion,
                                    true,
                                    subFormName
                                  );
                                }}
                                readOnly={readOnly}
                                getValueSubForm={() =>
                                  getValueSubForm(
                                    slice,
                                    subFormQuestion,
                                    question.key,
                                    subFormName,
                                    annotationId,
                                    this.state
                                  )
                                }
                                formatOptions={() =>
                                  formatOptions(
                                    subFormQuestion.values,
                                    DROPDOWN_DEFAULT_VALUE
                                  )
                                }
                                noop={noop}
                                notesForSubForm={notesForSubForm}
                                multipleTaskNotes={multipleTaskNotes}
                                taskIds={taskIds}
                                multipleTaskUser={multipleTaskUser}
                                measurements={this.props.measurements}
                              />
                            );
                          }
                        }
                      )}
                  </div>
                );
              })}
            </div>
          )}
        </div>
      );
    } else {
      if (!question.components) {
        return (
          <StudyFormContent
            key={`${question.key}_${index}`}
            question={question}
            index={index}
            questionRefs={this.questionRefs}
            onValueChange={question => this.onValueChange(question)}
            studyFormState={this.state}
            slice={slice}
            readOnly={readOnly}
            measurementLabels={measurementLabels}
            projectConfig={projectConfig}
            cleanHTML={question => this.cleanHTML(question.html)}
            radioOnClick={(option, question) => {
              this.clickType(option, question.key);
              this.onClickQuestionValues(question);
            }}
            getValue={(slice, question) =>
              getValue(slice, question, this.state)
            }
            selectBoxOnChange={(option, question) => {
              this.clickType(option, question.key);
              this.onClickQuestionValues(question);
            }}
            formatOptions={question =>
              formatOptions(question.values, DROPDOWN_DEFAULT_VALUE)
            }
            onClickQuestionValues={(question, option) => {
              this.clickType(option, question.key);
              this.onClickQuestionValues(question);
            }}
            getInstructionSet={(question, slice, studyFormState) =>
              this.getInstructionSet(
                question,
                slice,
                studyFormState.currentSliceInfo
              )
            }
            noop={noop}
            formStyles={formStyles}
            multipleTaskNotes={multipleTaskNotes}
            taskIds={taskIds}
            multipleTaskUser={multipleTaskUser}
            measurements={this.props.measurements}
          />
        );
      }
    }
  }

  componentWillUnmount() {
    let confirmationDialogData = {};
    let subFormDialogData = {};
    if (this.roiQuestionDialogId) {
      confirmationDialogData = {
        sliceInfo: this.props.sliceInfo,
        currentQuestion: this.state.confirmationDialogCurrentQuestion,
        confirmationDialogRoiAnswer: this.state.confirmationDialogRoiAnswer,
        answer: this.state.answer,
        confirmationDialogMeasurementList: this.state
          .confirmationDialogMeasurementList,
        lastAnsweredValue: this.state.lastAnsweredValue,
      };
    }
    if (this.roiSubFormDialogId) {
      subFormDialogData = {
        subFormDialogCurrentQuestion: this.state.subFormDialogCurrentQuestion,
        subFormDialogRoiAnswer: this.state.subFormDialogRoiAnswer,
        subFormDialogMeasurementList: this.state.subFormDialogMeasurementList,
      };
    }
    const panel = {
      isPanelSwitched: true,
      confirmationDialog: this.roiQuestionDialogId ? true : false,
      subFormDialog: this.roiSubFormDialogId ? true : false,
      confirmationDialogData: confirmationDialogData,
      subFormDialogData: subFormDialogData,
    };
    store.dispatch(setPanelSwitched(panel));
    const state = store.getState();
    const isPanelSwitched = state.infusions?.isPanelSwitched;
    const { UIDialogService } = this.props.servicesManager.services;
    if (isPanelSwitched && this.roiSubFormDialogId) {
      UIDialogService.dismiss({ id: this.roiSubFormDialogId });
      this.roiSubFormDialogId = null;
    }
    if (isPanelSwitched && this.roiQuestionDialogId) {
      UIDialogService.dismiss({ id: this.roiQuestionDialogId });
      this.roiQuestionDialogId = null;
    }
  }
}

// measurements are mutated when the location/label is set
// so we do our own deep equality check
let cachedLabels = [];

const mapStateToProps = (state, props) => {
  let StudyInstanceUID = '';
  let sessionRead = null;
  const { colorPalette } = state;
  const measurements = state.timepointManager.measurements;
  const isFile = isCurrentFile(state);
  const currentSelectedQuestion = state.infusions.currentSelectedQuestion;
  const featureConfig = state.flywheel.featureConfig;
  const { taskId } = getRouteParams();
  const reader_tasks = featureConfig?.features?.reader_tasks;
  const isReaderTask = taskId && reader_tasks;

  if (isFile) {
    const { fileName, containerId } = props.match.params;
    StudyInstanceUID = isCurrentZipFile(fileName) ? containerId : fileName;
    if (isReaderTask) {
      sessionRead = state.flywheel.readerTaskWithFormResponse
        ? state.flywheel.readerTaskWithFormResponse.info
        : null;
    } else {
      sessionRead = selectSessionFileRead(state);
    }
  } else {
    const uids = props.match.params.studyInstanceUIDs || '';
    StudyInstanceUID = uids.split(',')[0];
    if (isReaderTask) {
      sessionRead = state.flywheel.readerTaskWithFormResponse
        ? state.flywheel.readerTaskWithFormResponse.info
        : null;
    } else {
      sessionRead = selectSessionRead(state);
    }
  }
  const slice = props.sliceInfo?.sliceNumber || 1;

  const projectConfig = getCondensedProjectConfig();

  const newLabels = getMeasurementLabels(
    selectMeasurementsByStudyUid(state)[StudyInstanceUID] || {},
    projectConfig,
    slice
  );

  if (!isEqual(newLabels, cachedLabels)) {
    if (!isFile && projectConfig.bSlices) {
      cachedLabels = {};
    }
    cachedLabels = newLabels;
  }

  return {
    measurementLabels: cachedLabels,
    projectConfig: projectConfig,
    sessionRead: sessionRead,
    StudyInstanceUID: StudyInstanceUID,
    studyList: selectStudyList(state),
    paletteIndex: colorPalette.paletteIndex,
    palette: colorPalette.palette,
    measurements,
    currentSelectedQuestion,
  };
};

let dispatchActions = null;

const mapDispatchToActions = {
  setActiveTool,
  setAvailableLabels,
  dispatchUpdateRoiColor: roiColor => {
    return updateRoiColors(roiColor);
  },
  dispatchUpdatePaletteColors: measurements => {
    return updatePaletteColors(measurements);
  },
};

const mergeProps = (propsFromState, propsFromDispatch, ownProps) => {
  return {
    ...propsFromState,
    ...ownProps,
    ...propsFromDispatch,
    onChangeColor: (color, measurementData) => {
      if (!dispatchActions) {
        dispatchActions = require('@ohif/viewer/src/appExtensions/MeasurementsPanel/dispatchActions.js')
          .default;
      }
      return dispatchActions?.onChangeColor(color, measurementData);
    },
  };
};

const ConnectedStudyForm = withTranslation('Flywheel')(
  withRouter(
    connect(mapStateToProps, mapDispatchToActions, mergeProps)(StudyForm)
  )
);

export default ConnectedStudyForm;
