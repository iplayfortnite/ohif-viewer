import React from 'react';

function Radio(props) {
  const {
    option,
    question,
    onClick,
    radioChecked,
    radioDisabled,
    radioReadOnly,
    subFormIndex,
  } = props;

  return (
    <input
      type="radio"
      onClick={onClick}
      name={question.key + subFormIndex}
      value={option.value}
      onChange={e => {}}
      checked={radioChecked}
      disabled={radioDisabled}
      readOnly={radioReadOnly}
      className={'cursorPointer'}
    />
  );
}

export default Radio;
