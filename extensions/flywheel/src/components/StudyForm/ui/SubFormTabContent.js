import React from 'react';
import classnames from 'classnames';
import { isOptionAvailable } from '../validation/ValidationEngine.js';
import Radio from './Radio';
import SelectBox from './SelectBox';
import Checkbox from './Checkbox';
import TextBox from './TextBox';
import TextArea from './TextArea';
import Label from './Label';
import '../StudyForm.styl';
import '../SubForm.styl';
import AgreementIndicator from './AgreementIndicator/AgreementIndicator.js';

function SubFormTabContent(props) {
  const {
    subFormQuestion,
    index,
    slice,
    question,
    annotationId,
    projectConfig,
    studyFormState,
    questionRefs,
    onValueChangeForSubForm,
    formStyles,
    cleanHTML,
    measurementLabels,
    radioOnClick,
    selectBoxOnClick,
    onClickQuestionValues,
    readOnly,
    getValueSubForm,
    formatOptions,
    noop,
    notesForSubForm,
    multipleTaskNotes,
    taskIds,
    multipleTaskUser,
    measurements,
  } = props;
  let hideSeparator =
    projectConfig.studyForm.hideSeparator === undefined
      ? false
      : projectConfig.studyForm.hideSeparator;
  if (subFormQuestion.hideSeparator !== undefined) {
    hideSeparator = subFormQuestion.hideSeparator;
  }
  return (
    <div
      ref={element => (questionRefs[subFormQuestion.key] = element)}
      onChange={onValueChangeForSubForm()}
      className={classnames([
        'study-form-question',
        {
          'has-error':
            studyFormState.errors.slices &&
            studyFormState.errors.slices[slice] &&
            studyFormState.errors.slices[slice][question.key]?.[
              question.subFormName
            ]?.find(item => item.annotationId === annotationId)?.[
              subFormQuestion.key
            ]
              ? studyFormState.errors.slices[slice][question.key][
                  question.subFormName
                ].find(item => item.annotationId === annotationId)?.[
                  subFormQuestion.key
                ]
              : studyFormState.errors[question.key]?.[
                  question.subFormName
                ]?.find(item => item.annotationId === annotationId)?.[
                  subFormQuestion.key
                ],
        },
      ])}
      style={formStyles}
    >
      <div className="study-form-label">{subFormQuestion.label}</div>
      {(() => {
        if (taskIds.length > 1) {
          return (
            <div className={!hideSeparator ? 'study-form-seperator' : ''}>
              {subFormQuestion.type === 'radio' && (
                <AgreementIndicator
                  percentColors={[
                    { count: 1, label: '' },
                    { count: 9, label: '' },
                  ]}
                  isOptionAvailable={isOptionAvailable}
                  question={subFormQuestion}
                  notes={studyFormState.notes}
                  slice={slice}
                  readOnly={readOnly}
                  measurementLabels={measurementLabels}
                  projectConfig={projectConfig}
                  onClick={option => radioOnClick(option, subFormQuestion)}
                  studyFormState={studyFormState}
                  multipleTaskNotes={multipleTaskNotes}
                  taskIds={taskIds}
                  multipleTaskUser={multipleTaskUser}
                  index={index}
                  measurements={measurements}
                  notesForSubForm={notesForSubForm}
                  subFormName={question.subFormName}
                />
              )}

              {['text', 'textarea'].includes(subFormQuestion.type) && (
                <TextArea
                  question={subFormQuestion}
                  value={getValueSubForm()}
                  onChange={noop}
                  disabled={readOnly}
                />
              )}
              {subFormQuestion.type === 'textfield' && (
                <TextBox
                  question={subFormQuestion}
                  value={getValueSubForm()}
                  onChange={noop}
                  disabled={readOnly}
                />
              )}
            </div>
          );
        } else {
          return (
            <div className={!hideSeparator ? 'study-form-seperator' : ''}>
              {subFormQuestion.type === 'content' && (
                <div
                  dangerouslySetInnerHTML={{
                    __html: cleanHTML(),
                  }}
                />
              )}
              {subFormQuestion.type === 'radio' &&
                subFormQuestion.values.map((option, i) => {
                  let checked = false;
                  if (notesForSubForm === undefined) {
                    checked = false;
                  } else {
                    checked =
                      notesForSubForm[subFormQuestion.key] === option.value;
                  }
                  const disabled =
                    readOnly ||
                    !isOptionAvailable(
                      option,
                      measurementLabels,
                      projectConfig,
                      slice,
                      true,
                      studyFormState
                    );
                  return (
                    <Label
                      key={`${option.value}_${i}_${index}_${subFormQuestion.key}_${question.key}`}
                      className={classnames(
                        subFormQuestion.style === 'buttons'
                          ? 'study-form-radio-button'
                          : 'study-form-radio',
                        { checked, disabled }
                      )}
                    >
                      <Radio
                        option={option}
                        question={subFormQuestion}
                        onClick={() => radioOnClick(option)}
                        radioChecked={checked}
                        radioDisabled={disabled}
                        radioReadOnly={readOnly}
                        subFormIndex={index}
                      />
                      {option.label || option.value}
                    </Label>
                  );
                })}
              {subFormQuestion.type === 'dropdown' && (
                <div className="drop-down-container">
                  <SelectBox
                    value={getValueSubForm()}
                    onChange={e => selectBoxOnClick()}
                    options={formatOptions()}
                  />
                </div>
              )}
              {subFormQuestion.type === 'selectboxes' &&
                subFormQuestion.values.map((option, i) => {
                  let stateValue = [];
                  if (notesForSubForm === undefined) {
                    stateValue = [];
                  } else {
                    stateValue = notesForSubForm[subFormQuestion.key];
                  }
                  const checked =
                    (stateValue && stateValue.includes(option.value)) || false;
                  const disabled =
                    readOnly ||
                    !isOptionAvailable(
                      option,
                      measurementLabels,
                      projectConfig,
                      slice,
                      true,
                      studyFormState
                    );
                  return (
                    <Label
                      key={`${option.value}_${i}_${index}_${subFormQuestion.key}_${question.key}`}
                      className={classnames(
                        subFormQuestion.style === 'buttons'
                          ? 'study-form-radio-button'
                          : 'study-form-radio',
                        { checked, disabled }
                      )}
                    >
                      <Checkbox
                        onClick={() => onClickQuestionValues(question, option)}
                        question={subFormQuestion}
                        option={option}
                        checked={checked}
                        disabled={disabled}
                        readOnly={readOnly}
                      />
                      {option.label || option.value}
                    </Label>
                  );
                })}
              {['text', 'textarea'].includes(subFormQuestion.type) && (
                <TextArea
                  question={subFormQuestion}
                  value={getValueSubForm()}
                  onChange={noop}
                  disabled={readOnly}
                />
              )}
              {subFormQuestion.type === 'textfield' && (
                <TextBox
                  question={subFormQuestion}
                  value={getValueSubForm()}
                  onChange={noop}
                  disabled={readOnly}
                />
              )}
            </div>
          );
        }
      })()}
    </div>
  );
}

export default SubFormTabContent;
