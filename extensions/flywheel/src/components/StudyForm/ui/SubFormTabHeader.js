import React from 'react';
import classnames from 'classnames';
import { Utils } from '@flywheel/extension-flywheel-common';
import '../StudyForm.styl';
import '../SubForm.styl';
import store from '@ohif/viewer/src/store';

const { isCurrentFile } = Utils;

function SubFormTabHeader(props) {
  const {
    slice,
    index,
    question,
    tab,
    tabMouseEnter,
    tabMouseLeave,
    tabClick,
    studyFormState,
    projectConfig,
    readOnly,
  } = props;
  const { name, annotationId } = tab;
  const state = store.getState();
  const subFormTab =
    !isCurrentFile(state) && projectConfig.bSlices
      ? studyFormState.subFormTab[slice]
      : studyFormState.subFormTab;
  const subFormTabHeightAdjustment = 8;
  return (
    <li
      onMouseEnter={() => tabMouseEnter()}
      onMouseLeave={() => tabMouseLeave()}
      onClick={() => tabClick()}
      className={classnames(
        'nav-link',
        question.key + index === subFormTab?.[question.key] && 'active',
        index > subFormTabHeightAdjustment && 'height25'
      )}
      data-cy={name}
      disabled={readOnly}
    >
      <button>{name}</button>
    </li>
  );
}

export default SubFormTabHeader;
