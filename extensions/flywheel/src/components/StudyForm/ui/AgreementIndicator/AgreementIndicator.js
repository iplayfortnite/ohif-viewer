import React, { memo } from 'react';
import classnames from 'classnames';
import _ from 'lodash';
import store from '@ohif/viewer/src/store';
import CircleChart from '../../../CircleChart/CircleChart.js';
import { getPercentColor } from '../../utils/index.js';
import GroupedRadioButton from '../GroupedRadioButton/GroupedRadioButton.js';
import { getInverted } from '../../utils/AgreementIndicatorUtils.js';
import './AgreementIndicator.styl';

function AgreementIndicator(props) {
  const {
    slice,
    notes,
    onClick,
    readOnly,
    question,
    projectConfig,
    isOptionAvailable,
    measurementLabels,
    percentColors = [],
    studyFormState,
    multipleTaskNotes,
    taskIds,
    multipleTaskUser,
    index,
    measurements,
    notesForSubForm,
    subFormName,
  } = props;

  const state = store.getState();

  return (
    <div className="agreement-container">
      <div className="component-container">
        {Object.keys(multipleTaskNotes).length >= 1 &&
          taskIds.length > 1 &&
          question.values?.map((option, i) => {
            let checked = false;
            const viewerAvatar = state.viewerAvatar;
            let isSingleUser = viewerAvatar.singleAvatar;
            if (isSingleUser) {
              if (notesForSubForm === undefined) {
                let note = getQuestionNoteFromStudyFormState(
                  notes,
                  slice,
                  question
                );
                checked = note === option.value;
              } else {
                checked = notesForSubForm?.[question.key] === option.value;
              }
            } else {
              taskIds.map(taskId => {
                let note;
                if (notesForSubForm === undefined) {
                  note = getNoteFromMultipleTask(
                    multipleTaskNotes,
                    taskId,
                    slice,
                    question
                  );
                } else {
                  note = notesForSubForm?.[question.key];
                }
                if (!checked) {
                  checked = note === option.value;
                }
              });
            }

            let disabled =
              readOnly ||
              !isOptionAvailable(
                option,
                measurementLabels,
                projectConfig,
                slice,
                false,
                studyFormState
              );

            const taskCount = taskIds.length;

            let newPercentColors = [];
            question.values?.forEach((option, i) => {
              let obj = { count: 0, value: '' };
              taskIds.forEach(taskId => {
                if (multipleTaskNotes?.[taskId]) {
                  let note;
                  if (isSingleUser) {
                    note = getQuestionNoteFromStudyFormState(
                      notes,
                      slice,
                      question
                    );
                  } else {
                    note = getNoteFromMultipleTask(
                      multipleTaskNotes,
                      taskId,
                      slice,
                      question
                    );
                  }
                  obj.count = note === option.value ? obj.count + 1 : obj.count;
                  obj.value =
                    note === option.value ? option.value : option.value;
                }
              });
              newPercentColors.push(obj);
            });
            const total = taskCount;
            const percentColor = newPercentColors?.[i]
              ? getPercentColor(newPercentColors, i, total, option.value)
              : '';

            if (taskCount > 1) {
              disabled = !isSingleUser;
              if (
                (notes.slices && !notes.slices?.[slice]?.[question.key]) ||
                (!notes.slices && !notes[question.key])
              ) {
                disabled = true;
              }
            }

            return (
              <div
                key={option.value + i}
                className="radio-button-wrapper-multi-task"
              >
                <GroupedRadioButton
                  className={classnames(
                    question.style === 'buttons'
                      ? 'study-form-radio-button'
                      : 'study-form-radio',
                    { checked, disabled }
                  )}
                  percentColor={percentColor}
                  option={option}
                  question={question}
                  onClick={e => {
                    onClick(option, question);
                  }}
                  radioChecked={checked}
                  radioDisabled={disabled}
                  radioReadOnly={readOnly}
                  label={option.label || option.value}
                  multipleTaskNotes={multipleTaskNotes}
                  taskIds={taskIds}
                  multipleTaskUser={multipleTaskUser}
                  index={index}
                  indexValue={i}
                  measurements={measurements}
                  slice={slice}
                  isSingleUser={isSingleUser}
                  notes={notes}
                />
              </div>
            );
          })}
      </div>
      {(() => {
        const taskCount = taskIds.length;
        let answerCount = 0;
        let answeredUserCount = 0;
        let newPercentColors = [];
        const viewerAvatar = state.viewerAvatar;
        const isSingleUser = viewerAvatar.singleAvatar;
        const singleTaskId = viewerAvatar.taskId;
        Object.keys(multipleTaskNotes).length &&
          question.values?.forEach((option, i) => {
            let obj = { count: 0, value: '' };
            taskIds.forEach(taskId => {
              let note;

              if (isSingleUser && singleTaskId === taskId) {
                note = getQuestionNoteFromStudyFormState(
                  notes,
                  slice,
                  question
                );
              } else {
                note = getNoteFromMultipleTask(
                  multipleTaskNotes,
                  taskId,
                  slice,
                  question
                );
              }

              answeredUserCount = !note
                ? answeredUserCount
                : answeredUserCount + 1;
              answerCount =
                note === option.value ? answerCount + 1 : answerCount;
              obj.count = note === option.value ? obj.count + 1 : obj.count;
              obj.value = note === option.value ? option.value : option.value;
            });
            newPercentColors.push(obj);
          });

        const inverted = getInverted(
          multipleTaskNotes,
          slice,
          question,
          taskCount,
          newPercentColors
        );

        if (Object.keys(multipleTaskNotes).length && question.values?.length) {
          return (
            <div className="chart-container">
              <CircleChart
                options={newPercentColors}
                taskCount={taskCount}
                invert={inverted}
              />
            </div>
          );
        }
      })()}
    </div>
  );
}

const getQuestionNoteFromStudyFormState = (notes, slice, question) => {
  let note = notes.slices
    ? typeof notes.slices[slice]?.[question.key] === 'object'
      ? notes.slices[slice]?.[question.key].value
      : notes.slices[slice]?.[question.key]
    : typeof notes[question.key] === 'object'
    ? notes[question.key].value
    : notes[question.key];
  return note;
};

const getNoteFromMultipleTask = (
  multipleTaskNotes,
  taskId,
  slice,
  question
) => {
  let note = multipleTaskNotes?.[taskId]
    ? multipleTaskNotes?.[taskId]?.notes?.[0]?.slices
      ? typeof multipleTaskNotes[taskId].notes?.[0]?.slices[slice]?.[
          question.key
        ] === 'object'
        ? multipleTaskNotes[taskId].notes?.[0]?.slices[slice]?.[question.key]
            .value
        : multipleTaskNotes[taskId].notes?.[0]?.slices[slice]?.[question.key]
      : typeof multipleTaskNotes?.[taskId]?.notes?.[0]?.[question.key] ===
        'object'
      ? multipleTaskNotes[taskId].notes?.[0]?.[question.key].value
      : multipleTaskNotes[taskId].notes?.[0]?.[question.key]
    : '';
  return note;
};

export default memo(AgreementIndicator);
