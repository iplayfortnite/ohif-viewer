import React from 'react';

function Checkbox(props) {
  const { onClick, question, option, checked, disabled, readOnly } = props;

  return (
    <input
      type="checkbox"
      onClick={onClick}
      name={question.key}
      value={option.value}
      checked={checked}
      disabled={disabled}
      readOnly={readOnly}
      onChange={e => {}}
    />
  );
}

export default Checkbox;
