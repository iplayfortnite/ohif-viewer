import React, { useEffect, useState } from 'react';
import cloneDeep from 'lodash.clonedeep';

import store from '@ohif/viewer/src/store';
import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
import { MeasurementTableComponents, Select } from '@ohif/ui';
import * as MeasurementTableUtils from '@ohif/ui/src/components/measurementTable/AnnotationTableComponents/Utils.js';
import OHIFLabellingData from '@ohif/viewer/src/components/Labelling/OHIFLabellingData';
import InstructionDialog from '../../../InstructionDialog/InstructionDialog.js';
import {
  avatarClick,
  onAvatarMouseEnter,
  onAvatarMouseLeave,
} from './../../../../utils';

import './InstructionContainer.styl';

let helpDialogId = null;

const {
  SingleMeasurementBox,
  GroupedMeasurementBox,
  GroupedMeasurementContainer,
  NewAnnotationBox,
} = MeasurementTableComponents;

const {
  isCurrentFile,
  getBSliceTools,
  getBSliceSettings,
  getActiveSliceInfo,
  hasInstructionRange,
  hasLabelsLimitReached,
  getSelectedOptionForQuestion,
  getCountForLabel,
  getLabelLimits,
} = FlywheelCommonUtils;

const { RightHandPanelModes } = FlywheelCommonRedux.constants;

const DEFAULT_LABEL = '--- Select Label ---';
const DEFAULT_COLOR = 'rgba(255, 255, 0, 0.2)';
let timeInterval = null;

function InstructionContainer(props) {
  const {
    instruction,
    question,
    sliceInfo,
    dispatchActions,
    servicesManager,
    labels,
    commandsManager,
    instructionSet,
    notes,
    measurements,
  } = props;

  const [newPlaceholder, setNewPlaceholder] = useState({});
  const [labelBasedMeasurements, setLabelBasedMeasurements] = useState({});
  const [placeHolderInstruction, setPlaceholderInstruction] = useState(
    instruction
  );

  useEffect(() => {
    if (hasInstructionRange(instruction)) {
      const newPlaceholderInstruction = cloneDeep(placeHolderInstruction);
      newPlaceholderInstruction.requireMeasurements = [];
      const totalMeasuresCount = totalAnnotationCount();
      const defaultPlaceholderCount = getDefaultPlaceholderCount();
      for (let i = 0; i < defaultPlaceholderCount - totalMeasuresCount; i++) {
        newPlaceholderInstruction.requireMeasurements.push('');
      }
      setPlaceholderInstruction(newPlaceholderInstruction);
    }
  }, []);

  useEffect(() => {
    if (hasInstructionRange(instruction)) {
      const measures = getMeasurements();
      const bSliceSettings = getBSliceSettings();
      const newPlaceholderInstruction = cloneDeep(placeHolderInstruction);
      const defaultPlaceholderLimit = getDefaultPlaceholderLimit();
      let isUpdated = false;
      if (labelBasedMeasurements !== measures) {
        newPlaceholderInstruction.requireMeasurements = [];
        instruction.requireMeasurements.forEach(label => {
          const sliceNumber = sliceInfo?.sliceNumber;
          const labelCount = getCountForLabel(
            label,
            question,
            sliceNumber,
            bSliceSettings,
            '' // TODO : subFormAnnotationId
          );
          const selectedOption = getSelectedOptionForQuestion(
            question,
            sliceNumber,
            '' // TODO : subFormAnnotationId
          );
          const limit = getLabelLimits(label, selectedOption);
          const placeholder = Math.max(0, defaultPlaceholderLimit - labelCount);
          for (let i = 0; i < placeholder; i++) {
            newPlaceholderInstruction.requireMeasurements.push(label);
          }
          if (placeholder >= 0 || labelCount === limit) {
            isUpdated = true;
          }
        });
      }
      if (isUpdated) {
        setPlaceholderInstruction(newPlaceholderInstruction);
        setLabelBasedMeasurements(measures);
      }
    }
  }, [measurements]);

  function getMeasurementTools(currentInstruction) {
    const state = store.getState();
    const bSliceSettings = getBSliceSettings();
    const sliceInfo = getActiveSliceInfo(state.viewports.activeViewportIndex);
    const bSliceTools = getBSliceTools(
      question,
      bSliceSettings,
      sliceInfo?.sliceNumber
    );
    return bSliceTools || currentInstruction?.measurementTools;
  }

  function getLabelsForInstruction() {
    return instruction.requireMeasurements || [];
  }

  function getMeasurements() {
    const state = store.getState();
    const hasBSlice = isAvailableBSlice(state);
    const measurements = state.timepointManager.measurements;
    let measuresList = {};
    const bSliceSettings = getBSliceSettings();
    const sliceInfo = getActiveSliceInfo(state.viewports.activeViewportIndex);
    const bSliceTools = getBSliceTools(
      question,
      bSliceSettings,
      sliceInfo?.sliceNumber
    );
    const measurementTools = getMeasurementTools(instruction);
    measurementTools?.map(toolName => {
      measurements[toolName]?.map(measurement => {
        const sliceDetails = props.getSliceInfo(measurement);
        const isSameQuestion = measurement.questionKey === question.key;
        const isLabelAvailableInInstruction = instruction?.requireMeasurements?.includes(
          measurement.location
        );
        const isSameSlice =
          sliceDetails?.sliceNumber === sliceInfo?.sliceNumber;
        if (isSameQuestion) {
          if (isCurrentFile(state)) {
            addMeasurement(measuresList, measurement);
          } else if (
            hasBSlice &&
            isSameSlice &&
            isLabelAvailableInInstruction
          ) {
            addMeasurement(measuresList, measurement);
          } else if (!hasBSlice && isLabelAvailableInInstruction) {
            addMeasurement(measuresList, measurement);
          }
        }
      });
    });

    return measuresList;
  }

  function addMeasurement(measuresList, measurement) {
    if (!measuresList[measurement.location]) {
      measuresList[measurement.location] = [];
    }
    measuresList[measurement.location].push(measurement);
  }

  function isAvailableBSlice(state) {
    const projectConfig = state?.flywheel?.projectConfig;
    return !isCurrentFile(state) && projectConfig?.bSlices;
  }

  function loadSelectBoxOptions(measurement) {
    let initialItems = labels || OHIFLabellingData;
    const requireMeasurements = getLabelsForInstruction();
    const bSliceSettings = getBSliceSettings();
    const selectedOption = getSelectedOptionForQuestion(
      question,
      sliceInfo?.sliceNumber,
      measurement?.subFormAnnotationId
    );
    initialItems = initialItems.filter(
      x =>
        requireMeasurements.includes(x.value) &&
        (measurement?.location === x.value ||
          measurement?.location === x.label ||
          !hasLabelsLimitReached(
            [x.value],
            question,
            selectedOption,
            sliceInfo?.sliceNumber,
            bSliceSettings,
            measurement?.subFormAnnotationId // TODO Check subFormAnnotationId,
          ))
    );
    const selectOptions = initialItems.map(x => ({
      ...x,
      key: `${x.value} - ${x.label}`,
      item: x.label,
      value: x.value,
      limit: x.limit,
    }));
    selectOptions.unshift({
      key: DEFAULT_LABEL,
      label: DEFAULT_LABEL,
      value: DEFAULT_LABEL,
    });
    return selectOptions;
  }

  function checkLabelLimit(label) {
    let initialItems = getLabels();
    let measures = [];
    const state = store.getState();
    const measurements = state.timepointManager.measurements;

    Object.keys(measurements).forEach(key => {
      measurements[key].forEach(x => {
        if (x.questionKey === question.key) {
          measures.push(x);
        }
      });
    });
    let count = 0;
    measures.forEach(x => {
      if (x.location === label) {
        count += 1;
      }
    });

    const labelObj = initialItems.find(
      x => x.value === label || x.label === label
    );
    if (labelObj?.limit) {
      if (labelObj.limit <= count) {
        return false;
      }
    }
    return true;
  }

  function getSubComponents(measures) {
    const state = store.getState();
    return measures.map((x, index) => {
      const sliceDetails = props.getSliceInfo(x);
      const measurement = { ...x, measurementId: x._id, ...sliceDetails };
      const toolIconComponent = MeasurementTableUtils.getToolIcon(measurement, {
        t: noop,
      });
      const isVisible = MeasurementTableUtils.getItemVisibilityStatus(
        measurement
      );
      return (
        <GroupedMeasurementBox
          key={index}
          onMouseEnter={noop}
          onMouseLeave={noop}
          isOverlapped={false}
          onItemClick={noop}
          onClick={noop}
          dim={''}
          measurementColor={measurement.color}
          measurement={measurement}
          onEditLabelDescriptionClick={noop}
          onEditSubFormClick={noop}
          parentProps={{ t: noop }}
          showEditIcon={false}
          toolObj={{}}
          orientation={dispatchActions?.getImageOrientation(measurement, {
            timepointManager: state.timepointManager,
          })}
          toolIconComponent={toolIconComponent}
          visibleButtonObj={{
            onClick: e => {
              preventPropagation(e);
              dispatchActions.onVisibleClick(e, measurement);
            },
            isVisible: isVisible,
          }}
          deleteButtonObj={{
            onClick: e => {
              preventPropagation(e);
              if (x.readonly) {
                return;
              }
              dispatchActions.onDeleteClick(e, measurement, {
                servicesManager,
              });
            },
          }}
          rootClass="measurement-item"
          subComponent={getSelectBox(measurement, {
            measures: [],
            hasMultipleSet: true,
            hasTitle: true,
          })}
          detailsPaneObj={{
            showDetailsPane: false,
          }}
          sliceInfo={sliceInfo}
          avatarClick={avatarClick}
          onAvatarMouseEnter={onAvatarMouseEnter}
          onAvatarMouseLeave={onAvatarMouseLeave}
          measurements={measurements}
        />
      );
    });
  }

  function getMeasurementCountByLabel(labelObj) {
    const state = store.getState();
    const measurements = state.timepointManager.measurements;
    let measurementList = [];
    Object.keys(measurements).forEach(toolType => {
      measurements[toolType].forEach(measure => {
        const data = { ...measure, measurementId: measure._id };
        const sliceDetails = props.getSliceInfo(data);
        if (
          measure.location === labelObj.value &&
          measure.questionKey === question.key &&
          sliceInfo?.sliceNumber === sliceDetails?.sliceNumber
        ) {
          measurementList.push(measure);
        }
      });
    });
    return measurementList;
  }

  function getSelectBox(
    measurement,
    propsObj = {
      measures: [],
      showDefaultPlainText: false,
      hasMultipleSet: false,
      hasMainHeader: false,
      hasTitle: false,
    },
    labelIndex = -1,
    hasPlaceholder = false
  ) {
    const {
      measures,
      showDefaultPlainText,
      hasMultipleSet,
      hasMainHeader,
      hasTitle,
    } = propsObj;
    let options = loadSelectBoxOptions(measurement);
    if (hasMultipleSet) {
      let requireMeasurements = [];
      instructionSet.forEach(x => {
        if (getMeasurementTools(x).includes(measurement.toolType)) {
          requireMeasurements = requireMeasurements.concat(
            x.requireMeasurements
          );
        }
      });
      let label_list = [];
      const currentLabel = options.filter(x => x.value == measurement.location);
      if (hasMainHeader) {
        label_list = options.filter(
          (x, index) => x.value !== measurement.location && index !== 0
        );

        const currentMeasurementList = getMeasurementCountByLabel(
          currentLabel[0]
        );
        if (label_list.length) {
          label_list.forEach(x => {
            const measurementList = getMeasurementCountByLabel(x);
            if (x?.limit) {
              const difference = x.limit - measurementList.length;
              if (difference <= 0) {
                options = options.filter(option => option.value !== x.value);
              }
              if (
                difference > 0 &&
                currentMeasurementList.length > difference
              ) {
                options = options.filter(option => option.value !== x.value);
              }
            }
          });
        }
      } else {
        label_list = options.filter((x, index) => index !== 0);
        if (label_list.length) {
          label_list.forEach(x => {
            if (x.value !== currentLabel[0].value) {
              const measurementList = getMeasurementCountByLabel(x);
              if (x?.limit) {
                const difference = x.limit - measurementList.length;
                if (difference <= 0) {
                  options = options.filter(option => option.value !== x.value);
                }
              }
            }
          });
        }
      }
      if (instructionSet.length) {
        options = options.filter((x, index) =>
          index === 0 ? true : requireMeasurements.includes(x.value)
        );
      }
    }

    if (showDefaultPlainText) {
      options[0].value = '';
      options[0].label = '';
      options[0].key = '';
    }

    const option = options.find(x => x.value === measurement.location);

    if (!hasMultipleSet && measurement?.description) {
      options.find(x => {
        if (x.value === measurement.location) {
          x.value = measurement.location + ` (${measurement.description})`;
        }
      });
      option.value = measurement.location + ` (${measurement.description})`;
    }
    const state = store.getState();
    const singleAvatar = state.viewerAvatar.singleAvatar;
    const isDisabled =
      state.multipleReaderTask.TaskIds.length > 1 && !singleAvatar;

    const selectBoxValue = hasPlaceholder
      ? getPlaceHolderValue(labelIndex)
      : showDefaultPlainText
      ? ''
      : option?.value || '';
    return (
      <div
        className="width-100 select-box-in-instruction"
        onClick={e => {
          preventPropagation(e);
        }}
      >
        <Select
          rootClass={hasPlaceholder ? 'new-select-box' : 'select-box-style'}
          disabled={isDisabled}
          value={selectBoxValue}
          title={
            hasTitle
              ? measurement.description
                ? measurement.description
                : `# ${measurement.measurementNumber}`
              : undefined
          }
          onClick={preventPropagation}
          onChange={e => {
            e.stopPropagation();
            e.preventDefault();
            const { value } = e.target;
            const option = options.find(x => x.value === value);

            if (option.value === DEFAULT_LABEL) {
              return;
            }
            if (hasPlaceholder) {
              onChangePlaceHolderSelectBox(value, labelIndex);
            } else {
              onChangeMeasurementSelectBox(
                value,
                measures,
                option,
                measurement
              );
            }
          }}
          options={options}
        />
      </div>
    );
  }

  function onChangePlaceHolderSelectBox(value, labelIndex) {
    const newInstruction = cloneDeep(placeHolderInstruction);
    newInstruction.requireMeasurements.splice(labelIndex, 1, value);
    setPlaceholderInstruction(newInstruction);
  }

  function onChangeMeasurementSelectBox(value, measures, option, measurement) {
    if (measures.length) {
      measures.forEach(x =>
        _updateLabellingHandler(
          {
            location: option.value,
            color: option?.color || DEFAULT_COLOR,
          },
          x
        )
      );
    } else {
      _updateLabellingHandler(
        {
          location: option.value,
          color: option?.color || DEFAULT_COLOR,
        },
        measurement
      );
    }
  }

  const isExpandedRightHandPanel = () => {
    const state = store.getState();
    return state?.rightHandPanel?.panelMode === RightHandPanelModes.EXPAND;
  };

  const openInstructionDialog = event => {
    const { UIDialogService } = servicesManager.services;

    if (helpDialogId) {
      UIDialogService.dismiss({ id: helpDialogId });
      helpDialogId = null;
      return;
    }
    const isExpanded = isExpandedRightHandPanel();
    const width = isExpanded ? 580 : 260;
    const height = 250;
    helpDialogId = UIDialogService.create({
      content: InstructionDialog,
      isDraggable: false,
      preservePosition: false,
      defaultPosition: {
        x: (event && event.clientX - width) || 0,
        y: (event && event.clientY - height) || 0,
      },
      showOverlay: false,
      contentProps: {
        instruction: instruction?.instruction,
        clearTimeOut: () => clearTimeout(timeInterval),
        onClose: () => {
          UIDialogService.dismiss({ id: helpDialogId });
          helpDialogId = null;
        },
      },
    });
  };

  const totalAnnotationCount = () => {
    const measures = getMeasurements();
    let totalCount = 0;
    Object.keys(measures).forEach(x => {
      totalCount += measures[x].length;
    });
    return totalCount;
  };

  const hasPlaceholderLimitReached = () => {
    const annotationCount = totalAnnotationCount();
    const { exact = null, max = null, requireMeasurements = [] } = instruction;
    const total =
      placeHolderInstruction.requireMeasurements.length + annotationCount;

    if (exact) {
      const exactCount = exact * requireMeasurements.length;
      return hasLimitReached(exactCount, total, annotationCount);
    } else if (max) {
      const exactCount = max * requireMeasurements.length;
      return hasLimitReached(exactCount, total, annotationCount);
    } else if (requireMeasurements.length) {
      const sliceNumber = sliceInfo?.sliceNumber;
      const selectedOption = getSelectedOptionForQuestion(
        question,
        sliceNumber,
        '' // TODO : subFormAnnotationId
      );
      let limit = null;
      requireMeasurements.forEach(x => {
        limit += getLabelLimits(x, selectedOption);
      });
      if (limit === total) {
        return true;
      }
    }
    return false;
  };

  const hasLimitReached = (exactCount, total, annotationCount) => {
    if (annotationCount === exactCount) {
      return true;
    }
    if (total === exactCount) {
      return true;
    }
    if (placeHolderInstruction.requireMeasurements.length === exactCount) {
      return true;
    }
    return false;
  };

  const getDefaultPlaceholderCount = () => {
    if (instruction.exact) {
      return (
        instruction.exact * (instruction?.requireMeasurements?.length || 1)
      );
    }
    if (instruction.min !== 0) {
      return (
        (instruction.min || 1) * (instruction?.requireMeasurements?.length || 1)
      );
    }
    return 0;
  };

  const getDefaultPlaceholderLimit = () => {
    if (instruction.exact) {
      return instruction.exact;
    }
    if (instruction.min !== 0) {
      return instruction.min;
    }
    return 0;
  };

  function getLabels() {
    const { labels = [] } = props;
    if (labels.length) {
      return labels;
    }
    return OHIFLabellingData;
  }

  function getPlaceHolderValue(labelIndex) {
    const { requireMeasurements = [] } = placeHolderInstruction;
    if (labelIndex > -1 && requireMeasurements[labelIndex]) {
      return requireMeasurements[labelIndex];
    }
    return '';
  }

  const addNewPlaceholder = e => {
    const placeholder = cloneDeep(newPlaceholder);
    const newInstruction = cloneDeep(placeHolderInstruction);
    newInstruction.requireMeasurements.push('');
    setPlaceholderInstruction(newInstruction);
  };

  const getPlaceholder = () => {
    return placeHolderInstruction;
  };

  const _updateLabellingHandler = (labellingData, measurementData) => {
    const { location, color } = labellingData;
    if (location) {
      measurementData.location = location;
    }
    if (color) {
      measurementData.color = color;
    }
    commandsManager.runCommand(
      'updateTableWithNewMeasurementData',
      measurementData
    );
  };

  const hasRange = hasInstructionRange(instruction);
  const shouldEnableAddButton = hasPlaceholderLimitReached();
  const measuresList = getMeasurements();
  const infoIcon = MeasurementTableUtils.getActionButton('exclamation-circle');

  return (
    <div className="instruction-container">
      <div className="instruction-box">
        <div style={{ color: props.errors ? 'red' : '' }} className="free-text">
          {instruction.directive}
        </div>
        <div className="instruction-icon-container">
          <span
            onMouseEnter={event => {
              event.persist();
              openInstructionDialog(event);
            }}
            onMouseLeave={event => {
              event.persist();
              timeInterval = setTimeout(() => {
                openInstructionDialog(event);
              }, 300);
            }}
          >
            <span className="pointer-events-none info-icon-container">
              {infoIcon}
            </span>
          </span>
        </div>
      </div>
      <div className="measurements-list">
        {instruction?.requireMeasurements?.map((label, index) => {
          if (measuresList[label]) {
            const measurement = measuresList[label][0];
            const measurementCount = measuresList[label].length;
            if (measuresList[label].length !== 1) {
              const isVisibleGroupButton = !measuresList[label].some(x => {
                const data = { ...x, measurementId: x._id };
                return MeasurementTableUtils.getItemVisibilityStatus(data);
              });
              const hasContainReadOnly = measuresList[label].some(
                x => x.readonly
              );
              return (
                <GroupedMeasurementContainer
                  component={getSelectBox(
                    { ...measurement },
                    {
                      measures: measuresList[label],
                      hasMultipleSet: true,
                      hasMainHeader: true,
                    }
                  )}
                  key={label}
                  onGroupClick={e => {
                    let newState = { ...props.groupClickStatus };
                    newState[question.key + props.instructionIndex] = {
                      ...newState[question.key + props.instructionIndex],
                    };
                    newState[question.key + props.instructionIndex][
                      label
                    ] = !newState[question.key + props.instructionIndex][label];
                    props.onGroupClick(newState);
                  }}
                  activeBorder={''}
                  activeBorderBottom={''}
                  measurementColor={measurement.color}
                  measurementCount={measurementCount}
                  visibleButtonObj={{
                    onClick: e => {
                      preventPropagation(e);
                      measuresList[label].forEach(x => {
                        const data = { ...x, measurementId: x._id };
                        const isVisible = MeasurementTableUtils.getItemVisibilityStatus(
                          data
                        );
                        if (isVisibleGroupButton) {
                          if (!isVisible) {
                            dispatchActions.onVisibleClick(e, data);
                          }
                        }
                        if (!isVisibleGroupButton) {
                          if (isVisible) {
                            dispatchActions.onVisibleClick(e, data);
                          }
                        }
                      });
                    },
                    isVisible: isVisibleGroupButton,
                  }}
                  deleteButtonObj={{
                    onClick: e => {
                      preventPropagation(e);
                      if (hasContainReadOnly) {
                        return;
                      }
                      const new_measures = measuresList[label].reduce(
                        (accumulator, x) => {
                          accumulator.push({ ...x, measurementId: x._id });
                          return accumulator;
                        },
                        []
                      );
                      dispatchActions.onDeleteGroupMeasurementClick(
                        e,
                        new_measures,
                        { servicesManager }
                      );
                    },
                  }}
                  onClickColorPicker={e => {
                    preventPropagation(e);
                    const measures = measuresList[label].reduce(
                      (accumulator, measure) => {
                        accumulator.push({
                          ...measure,
                          measurementId: measure._id,
                        });
                        return accumulator;
                      },
                      []
                    );
                    props.onClickColorPicker(e, measures);
                  }}
                  dim={''}
                  groupTitle={measurement.location}
                  onMouseEnter={noop}
                  onMouseLeave={noop}
                  rootClass="measurement-item"
                  subComponent={
                    props.groupClickStatus?.[
                      question.key + props.instructionIndex
                    ]?.[label]
                      ? getSubComponents(measuresList[label])
                      : null
                  }
                />
              );
            }

            return measuresList[label].map((x, index) => {
              const data = { ...x, measurementId: x._id };
              const isVisible = MeasurementTableUtils.getItemVisibilityStatus(
                data
              );
              const toolIconComponent = MeasurementTableUtils.getToolIcon(
                data,
                {
                  t: noop,
                }
              );
              const state = store.getState();
              const sliceDetails = props.getSliceInfo(data);
              const measurement = { ...x, ...sliceDetails };
              return (
                <SingleMeasurementBox
                  component={getSelectBox(data)}
                  measurement={measurement}
                  key={index}
                  onMouseEnter={noop}
                  onMouseLeave={noop}
                  onItemClick={noop}
                  dim={''}
                  label={label}
                  onClickColorPicker={e => props.onClickColorPicker(e, data)}
                  measurementColor={x.color}
                  onEditLabelDescriptionClick={noop}
                  showEditIcon={false}
                  parentProps={{ t: noop }}
                  toolObj={{}}
                  orientation={dispatchActions.getImageOrientation(data, state)}
                  toolIconComponent={toolIconComponent}
                  visibleButtonObj={{
                    isVisible,
                    onClick: e => dispatchActions.onVisibleClick(e, data),
                  }}
                  deleteButtonObj={{
                    onClick: e => {
                      dispatchActions.onDeleteClick(e, data, {
                        servicesManager,
                      });
                    },
                  }}
                  detailsPaneObj={{ isVisible: false }}
                  isOverlapped={false}
                  rootClass="measurement-item"
                />
              );
            });
          }
          if (!hasRange) {
            const view = defaultView({
              getSelectBox,
              index,
              instruction,
              getLabels,
            });
            return <div key={index}>{view}</div>;
          }
          return null;
        })}
        {hasRange &&
          placeholderView({
            getSelectBox,
            instruction: getPlaceholder(),
            getAllLabels: () => getLabels(),
            hasPlaceholder: true,
            onDelete: labelIndex => {
              const {
                exact = null,
                min = null,
                requireMeasurements = [],
              } = placeHolderInstruction;
              if (exact || min === requireMeasurements.length) {
                return;
              }
              const newInstruction = cloneDeep(placeHolderInstruction);
              newInstruction.requireMeasurements.splice(labelIndex, 1);
              setPlaceholderInstruction(newInstruction);
            },
          })}
        <NewAnnotationBox
          disabled={shouldEnableAddButton || !hasRange}
          onClick={e => {
            if (shouldEnableAddButton || !hasRange) {
              return;
            }
            addNewPlaceholder(e);
          }}
        />
      </div>
    </div>
  );
}

function defaultView(props) {
  const { index, instruction, getLabels } = props;
  let label = '';
  if (instruction?.requireMeasurements?.length) {
    label = instruction.requireMeasurements[index]
      ? instruction.requireMeasurements[index]
      : instruction.requireMeasurements[0];
  }
  const labelItem = getLabels().find(
    x => x.label === label || x.value === label
  );
  return (
    <SingleMeasurementBox
      component={
        <div className="default-label-view width-100 " title={label}>
          {label}
        </div>
      }
      measurement={{ sliceNumber: null, totalSlice: 0 }}
      key={index}
      onMouseEnter={noop}
      onMouseLeave={noop}
      onItemClick={noop}
      dim={''}
      label={''}
      onClickColorPicker={noop}
      measurementColor={labelItem?.color || ''}
      onEditLabelDescriptionClick={noop}
      onEditSubFormClick={noop}
      showEditIcon={false}
      parentProps={{ t: noop }}
      toolObj={{}}
      orientation={''}
      toolIconComponent={null}
      visibleButtonObj={{
        isVisible: true,
        onClick: noop,
      }}
      deleteButtonObj={{
        onClick: noop,
      }}
      detailsPaneObj={{ isVisible: false }}
      isOverlapped={false}
      rootClass="default-measurement-item"
    />
  );
}

function placeholderView(props) {
  const { instruction } = props;
  let requireMeasurements = [];
  if (instruction?.requireMeasurements) {
    requireMeasurements = cloneDeep(instruction.requireMeasurements);
  }
  return getDefaultAnnotationView(requireMeasurements, props);
}

function getDefaultAnnotationView(requireMeasurements, props) {
  const { getAllLabels, getSelectBox, hasPlaceholder, onDelete } = props;
  return requireMeasurements?.map((requireLabel, labelIndex) => {
    const labelItem = getAllLabels().find(
      x => x.label === requireLabel || x.value === requireLabel
    );
    return (
      <SingleMeasurementBox
        component={
          hasPlaceholder ? (
            getSelectBox(
              {},
              { showDefaultPlainText: true },
              labelIndex,
              hasPlaceholder
            )
          ) : (
            <div className="default-label-view">{requireLabel}</div>
          )
        }
        measurement={{ sliceNumber: null, totalSlice: 0 }}
        key={labelIndex}
        onMouseEnter={noop}
        onMouseLeave={noop}
        onItemClick={noop}
        dim={''}
        label={''}
        onClickColorPicker={noop}
        measurementColor={labelItem?.color || DEFAULT_COLOR}
        onEditLabelDescriptionClick={noop}
        showEditIcon={false}
        parentProps={{ t: noop }}
        toolObj={{}}
        orientation={''}
        toolIconComponent={null}
        visibleButtonObj={{
          isVisible: true,
          onClick: noop,
        }}
        deleteButtonObj={{
          onClick: hasPlaceholder ? () => onDelete(labelIndex) : noop,
        }}
        detailsPaneObj={{ isVisible: false }}
        isOverlapped={false}
        rootClass={
          hasPlaceholder ? 'default-placeholder' : 'default-measurement-item'
        }
      />
    );
  });
}

function preventPropagation(evt) {
  evt.stopPropagation();
  evt.preventDefault();
}

function noop(e) {}

export default React.memo(InstructionContainer);
