import Radio from './Radio';
import SelectBox from './SelectBox';
import Checkbox from './Checkbox';
import TextBox from './TextBox';
import TextArea from './TextArea';
import Label from './Label';
import StudyFormContent from './StudyFormContent';
import SubFormPanel from './SubFormPanel';
import SubFormTabHeader from './SubFormTabHeader';
import SubFormTabContent from './SubFormTabContent';

const ui = {
  Radio,
  SelectBox,
  Checkbox,
  TextBox,
  TextArea,
  Label,
  StudyFormContent,
  SubFormPanel,
  SubFormTabHeader,
  SubFormTabContent,
};

export default ui;
