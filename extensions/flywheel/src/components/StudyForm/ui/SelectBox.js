import React from 'react';
import { Select } from '@ohif/ui';

function SelectBox(props) {
  const { value, onChange, options } = props;

  return (
    <Select
      rootClass="drop-down-style"
      showIcon={true}
      value={value}
      onChange={onChange}
      options={options}
    />
  );
}

export default SelectBox;
