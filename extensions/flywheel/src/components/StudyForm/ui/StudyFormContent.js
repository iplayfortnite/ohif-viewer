import React from 'react';
import classnames from 'classnames';
import { getAvatarStyle } from '@ohif/ui/src/components/measurementTable/Utils.js';
import { Icon } from '@ohif/ui';
import { isOptionAvailable } from '../validation/ValidationEngine.js';
import Radio from './Radio';
import SelectBox from './SelectBox';
import Checkbox from './Checkbox';
import TextBox from './TextBox';
import TextArea from './TextArea';
import Label from './Label';
import AgreementIndicator from './AgreementIndicator/AgreementIndicator.js';
import AgreementIndicatorForSelectBox from './AgreementIndicator/AgreementIndicatorForSelectBox.js';
import {
  avatarClick,
  onAvatarMouseEnter,
  onAvatarMouseLeave,
} from '../../../utils';
import ViewerFormAvatar from '../ui/ViewerFormAvatar.js';
import './StudyFormContent.styl';
import CustomInfoTable from './CustomInfoTable.js';

function StudyFormContent(props) {
  const {
    question,
    index,
    questionRefs,
    onValueChange,
    studyFormState,
    slice,
    readOnly,
    measurementLabels,
    projectConfig,
    cleanHTML,
    radioOnClick,
    getValue,
    selectBoxOnChange,
    formatOptions,
    onClickQuestionValues,
    getInstructionSet,
    noop,
    formStyles,
    multipleTaskNotes,
    taskIds,
    multipleTaskUser,
    measurements,
  } = props;

  const state = store.getState();
  const users = state.flywheel.allUsers;
  let dispatchActions = null;
  let userAvatarClickedDetails = [];
  let hideSeparator =
    projectConfig.studyForm.hideSeparator === undefined
      ? false
      : projectConfig.studyForm.hideSeparator;
  if (question.hideSeparator !== undefined) {
    hideSeparator = question.hideSeparator;
  }
  Object.keys(multipleTaskNotes).length &&
    taskIds.map(taskId => {
      const userId = multipleTaskNotes[taskId]?.userId;
      if (userId) {
        const user = users[userId];
        const { avatarStyle, initials, avatar } = getAvatarStyle(user);

        if (
          !userAvatarClickedDetails.find(
            task =>
              task[initials + '-' + taskId] === true ||
              task[initials + '-' + taskId] === false
          )
        ) {
          userAvatarClickedDetails.push({
            [initials + '-' + taskId]: false,
            initials: initials,
            taskId: taskId,
            slice: slice,
            visible: false,
          });
        }
      }
    });

  return (
    <div
      key={`${question.key}_${index}_${question.label}`}
      ref={element => (questionRefs[question.key] = element)}
      onChange={onValueChange(question)}
      className={classnames([
        'study-form-question',
        {
          'has-error': studyFormState.errors.slices
            ? typeof studyFormState.errors.slices?.[slice]?.[question.key] ===
              'object'
              ? studyFormState.errors.slices[slice][question.key].value
              : typeof studyFormState.errors.slices?.[slice]?.[question.key] !==
                'object'
              ? studyFormState.errors.slices?.[slice]?.[question.key]
              : ''
            : typeof studyFormState.errors[question.key] === 'object'
            ? studyFormState.errors[question.key].value
            : studyFormState.errors[question.key],
        },
      ])}
      style={formStyles}
    >
      <div className="study-form-label">
        {taskIds.length > 1 && question.type === 'dropdown' && (
          <span className="warning-status">
            <span className="warning-border">
              <Icon
                name="exclamation-triangle"
                width="14px"
                height="14px"
                className="material-icons pointerEvents"
              />
            </span>
          </span>
        )}
        {question.label}
      </div>
      {(() => {
        const isSingleUser = state.viewerAvatar.singleAvatar;
        const singleTaskId = state.viewerAvatar.taskId;
        const isMultipleTask = taskIds.length > 1;
        const viewerAvatar = state.viewerAvatar;
        if (taskIds.length > 1) {
          return (
            <div className={!hideSeparator ? 'study-form-seperator' : ''}>
              {(question.type === 'radio' || question.type === 'dropdown') && (
                <AgreementIndicator
                  percentColors={[
                    { count: 1, label: '' },
                    { count: 9, label: '' },
                  ]}
                  isOptionAvailable={isOptionAvailable}
                  question={question}
                  notes={studyFormState.notes}
                  slice={slice}
                  readOnly={readOnly}
                  measurementLabels={measurementLabels}
                  projectConfig={projectConfig}
                  onClick={option => radioOnClick(option, question)}
                  studyFormState={studyFormState}
                  multipleTaskNotes={multipleTaskNotes}
                  taskIds={taskIds}
                  multipleTaskUser={multipleTaskUser}
                  index={index}
                  measurements={measurements}
                />
              )}

              {question.type === 'selectboxes' && (
                <AgreementIndicatorForSelectBox
                  percentColors={[
                    { count: 1, label: '' },
                    { count: 9, label: '' },
                  ]}
                  isOptionAvailable={isOptionAvailable}
                  question={question}
                  notes={studyFormState.notes}
                  slice={slice}
                  readOnly={readOnly}
                  measurementLabels={measurementLabels}
                  projectConfig={projectConfig}
                  onClick={option => radioOnClick(option, question)}
                  studyFormState={studyFormState}
                  multipleTaskNotes={multipleTaskNotes}
                  taskIds={taskIds}
                  multipleTaskUser={multipleTaskUser}
                  index={index}
                  measurements={measurements}
                />
              )}

              {['text', 'textarea'].includes(question.type) &&
                Object.keys(multipleTaskNotes).length &&
                taskIds.map(taskId => {
                  if (
                    (isMultipleTask &&
                      isSingleUser &&
                      taskId === singleTaskId) ||
                    (isMultipleTask && !isSingleUser)
                  ) {
                    const userId = multipleTaskNotes[taskId]?.userId;
                    if (userId) {
                      const user = users[userId];
                      const { avatarStyle, initials, avatar } = getAvatarStyle(
                        user
                      );

                      avatarStyle.color = 'black';
                      avatarStyle.borderWidth = '1px';
                      avatarStyle.borderStyle = 'solid';

                      if (viewerAvatar.length) {
                        viewerAvatar.forEach(avatar => {
                          const data =
                            avatar.initials + avatar.slice + avatar.taskId;
                          const avatarElements = document.getElementsByClassName(
                            avatar.initials + avatar.slice + avatar.taskId
                          ).length;
                          if (!avatar[data] && avatarElements > 0) {
                            for (let n = 0; n < avatarElements; n++) {
                              document
                                .getElementsByClassName(data)
                                .item(n).style.opacity = 0.3;
                            }
                          } else if (avatarElements > 0 && avatar[data]) {
                            for (let n = 0; n < avatarElements; n++) {
                              document
                                .getElementsByClassName(data)
                                .item(n).style.opacity = 1;
                            }
                          }
                        });
                      }

                      return (
                        <div className="marginBottom displayFlex">
                          {['text', 'textarea'].includes(question.type) && (
                            <TextArea
                              question={question}
                              value={
                                isMultipleTask && !isSingleUser
                                  ? multipleTaskNotes[taskId].notes?.[0]?.slices
                                    ? multipleTaskNotes[taskId].notes?.[0]
                                        ?.slices?.[slice]?.[question.key] || ''
                                    : multipleTaskNotes[taskId].notes?.[0]?.[
                                        question.key
                                      ] || ''
                                  : studyFormState.notes?.slices
                                  ? studyFormState.notes.slices?.[slice]?.[
                                      question.key
                                    ]
                                    ? studyFormState.notes.slices[slice][
                                        question.key
                                      ]
                                    : ''
                                  : studyFormState.notes[question.key] || ''
                              }
                              onChange={noop}
                              disabled={readOnly}
                            />
                          )}
                          <ViewerFormAvatar
                            initials={initials}
                            slice={slice}
                            taskId={taskId}
                            avatar={avatar}
                            avatarStyle={avatarStyle}
                            avatarClick={avatarClick}
                            userAvatarClickedDetails={userAvatarClickedDetails}
                            onAvatarMouseEnter={onAvatarMouseEnter}
                            dispatchActions={dispatchActions}
                            measurements={measurements}
                            user={user}
                            questionKey={question.key}
                            onAvatarMouseLeave={onAvatarMouseLeave}
                          />
                        </div>
                      );
                    }
                  }
                })}

              {question.type === 'textfield' &&
                Object.keys(multipleTaskNotes).length &&
                taskIds.map(taskId => {
                  if (
                    (isMultipleTask &&
                      isSingleUser &&
                      taskId === singleTaskId) ||
                    (isMultipleTask && !isSingleUser)
                  ) {
                    const userId = multipleTaskNotes[taskId]?.userId;
                    if (userId) {
                      const user = users[userId];
                      const { avatarStyle, initials, avatar } = getAvatarStyle(
                        user
                      );

                      avatarStyle.color = 'black';
                      avatarStyle.borderWidth = '1px';
                      avatarStyle.borderStyle = 'solid';

                      if (viewerAvatar.length) {
                        viewerAvatar.forEach(avatar => {
                          const data =
                            avatar.initials + avatar.slice + avatar.taskId;
                          const avatarElements = document.getElementsByClassName(
                            avatar.initials + avatar.slice + avatar.taskId
                          ).length;
                          if (!avatar[data] && avatarElements > 0) {
                            for (let n = 0; n < avatarElements; n++) {
                              document
                                .getElementsByClassName(data)
                                .item(n).style.opacity = 0.3;
                            }
                          } else if (avatarElements > 0 && avatar[data]) {
                            for (let n = 0; n < avatarElements; n++) {
                              document
                                .getElementsByClassName(data)
                                .item(n).style.opacity = 1;
                            }
                          }
                        });
                      }

                      return (
                        <div className="marginBottom displayFlex">
                          {question.type === 'textfield' && (
                            <TextBox
                              question={question}
                              value={
                                isMultipleTask && !isSingleUser
                                  ? multipleTaskNotes[taskId].notes?.[0]?.slices
                                    ? multipleTaskNotes[taskId].notes?.[0]
                                        ?.slices?.[slice]?.[question.key] || ''
                                    : multipleTaskNotes[taskId].notes?.[0]?.[
                                        question.key
                                      ] || ''
                                  : studyFormState.notes.slices
                                  ? studyFormState.notes.slices?.[slice]?.[
                                      question.key
                                    ]
                                    ? studyFormState.notes.slices[slice][
                                        question.key
                                      ]
                                    : ''
                                  : studyFormState.notes[question.key] || ''
                              }
                              onChange={noop}
                              disabled={readOnly}
                            />
                          )}
                          <ViewerFormAvatar
                            initials={initials}
                            slice={slice}
                            taskId={taskId}
                            avatar={avatar}
                            avatarStyle={avatarStyle}
                            avatarClick={avatarClick}
                            userAvatarClickedDetails={userAvatarClickedDetails}
                            onAvatarMouseEnter={onAvatarMouseEnter}
                            dispatchActions={dispatchActions}
                            measurements={measurements}
                            user={user}
                            questionKey={question.key}
                            onAvatarMouseLeave={onAvatarMouseLeave}
                          />
                        </div>
                      );
                    }
                  }
                })}

              {getInstructionSet(question, slice, studyFormState)}
            </div>
          );
        } else {
          return (
            <div className={!hideSeparator ? 'study-form-seperator' : ''}>
              {question.type === 'content' && (
                <div
                  dangerouslySetInnerHTML={{
                    __html: cleanHTML(question),
                  }}
                />
              )}
              {question.type === 'radio' &&
                question.values.map((option, i) => {
                  let checked = false;
                  if (
                    typeof studyFormState.notes.slices?.[slice]?.[
                      question.key
                    ] === 'object'
                  ) {
                    checked =
                      studyFormState.notes.slices[slice][question.key].value ===
                      option.value;
                  } else if (
                    typeof studyFormState.notes[question.key] === 'object'
                  ) {
                    checked =
                      studyFormState.notes[question.key].value === option.value;
                  } else {
                    checked = studyFormState.notes.slices
                      ? studyFormState.notes.slices[slice]
                        ? studyFormState.notes.slices[slice][question.key] ===
                          option.value
                        : false
                      : studyFormState.notes[question.key] === option.value;
                  }
                  const disabled =
                    readOnly ||
                    !isOptionAvailable(
                      option,
                      measurementLabels,
                      projectConfig,
                      slice,
                      false,
                      studyFormState
                    );
                  return (
                    <Label
                      key={`${option.value}_${i}_${index}_${question.key}`}
                      className={classnames(
                        question.style === 'buttons'
                          ? 'study-form-radio-button'
                          : 'study-form-radio',
                        { checked, disabled }
                      )}
                    >
                      <Radio
                        option={option}
                        question={question}
                        onClick={() => radioOnClick(option, question)}
                        radioChecked={checked}
                        radioDisabled={disabled}
                        radioReadOnly={readOnly}
                        subFormIndex={index}
                      />
                      {option.label || option.value}
                    </Label>
                  );
                })}
              {question.type === 'dropdown' && (
                <div className="drop-down-container">
                  <SelectBox
                    value={getValue(slice, question)}
                    onChange={e =>
                      selectBoxOnChange(
                        question.values.find(
                          x =>
                            x.value === e.target.value ||
                            x.label === e.target.value
                        ),
                        question
                      )
                    }
                    options={formatOptions(question)}
                  />
                </div>
              )}
              {question.type === 'selectboxes' &&
                question.values.map((option, i) => {
                  const stateValue = studyFormState.notes.slices
                    ? studyFormState.notes.slices[slice][question.key]
                    : studyFormState.notes[question.key];
                  const checked =
                    (stateValue && stateValue.includes(option.value)) || false;
                  const disabled =
                    readOnly ||
                    !isOptionAvailable(
                      option,
                      measurementLabels,
                      projectConfig,
                      slice,
                      false,
                      studyFormState
                    );
                  return (
                    <Label
                      key={`${option.value}_${i}_${index}_${question.key}`}
                      className={classnames(
                        question.style === 'buttons'
                          ? 'study-form-radio-button'
                          : 'study-form-radio',
                        { checked, disabled }
                      )}
                    >
                      <Checkbox
                        onClick={() => onClickQuestionValues(question, option)}
                        question={question}
                        option={option}
                        checked={checked}
                        disabled={disabled}
                        readOnly={readOnly}
                      />
                      {option.label || option.value}
                    </Label>
                  );
                })}
              {['text', 'textarea'].includes(question.type) && (
                <TextArea
                  question={question}
                  value={
                    studyFormState.notes?.slices
                      ? studyFormState.notes.slices?.[slice]?.[question.key]
                        ? studyFormState.notes.slices[slice][question.key]
                        : ''
                      : studyFormState.notes[question.key] || ''
                  }
                  onChange={noop}
                  disabled={readOnly}
                />
              )}
              {question.type === 'textfield' && (
                <TextBox
                  question={question}
                  value={
                    studyFormState.notes.slices
                      ? studyFormState.notes.slices?.[slice]?.[question.key]
                        ? studyFormState.notes.slices[slice][question.key]
                        : ''
                      : studyFormState.notes[question.key] || ''
                  }
                  onChange={noop}
                  disabled={readOnly}
                />
              )}
              {question.type === 'info' && (
                <CustomInfoTable question={question} />
              )}
              {getInstructionSet(question, slice, studyFormState)}
            </div>
          );
        }
      })()}
    </div>
  );
}

export default StudyFormContent;
