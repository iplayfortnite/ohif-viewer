import React from 'react';
import { ToolbarButton } from '@ohif/ui';
import { Utils } from '@flywheel/extension-flywheel-common';
import '../StudyForm.styl';
import '../SubForm.styl';
import store from '@ohif/viewer/src/store';

const { isCurrentFile } = Utils;

function SubFormPanel(props) {
  const {
    subFormPanelSwitch,
    slice,
    question,
    studyFormState,
    t,
    projectConfig,
    measurements,
    readOnly,
  } = props;
  const state = store.getState();
  const subFormPanel =
    !isCurrentFile(state) && projectConfig.bSlices
      ? studyFormState.subFormPanel[slice]
      : studyFormState.subFormPanel;

  const studyForm = state.flywheel.projectConfig?.studyForm?.components;
  const studyFormWorkflow = state.flywheel.projectConfig?.studyFormWorkflow;
  let showSubForm = false;
  let measurementData = {};
  if (studyForm?.length && studyFormWorkflow === 'Mixed') {
    const subFormTabWithAnnotationId =
      !isCurrentFile(state) && projectConfig.bSlices
        ? studyFormState.subFormTabWithAnnotationId[slice]
        : studyFormState.subFormTabWithAnnotationId;
    const annotationId = subFormTabWithAnnotationId[question.key];
    const note = studyFormState.notes.slices
      ? studyFormState.notes.slices?.[slice]?.[question.key]
      : studyFormState.notes?.[question.key];
    const answer = typeof note === 'object' ? note.value : note;
    const data = studyForm.find(item => item.key === question.key);
    const dataValue =
      data && data.values?.find(value => value.value === answer);
    dataValue?.measurementTools?.forEach(tool => {
      const measurement = measurements[tool].find(
        measurement =>
          measurement.questionKey === question.key &&
          measurement.sliceNumber === slice &&
          measurement.uuid === annotationId
      );
      if (measurement) {
        measurementData = measurement;
      }
    });
    if (
      question?.subFormName &&
      dataValue?.subForm &&
      Object.keys(measurementData).length
    ) {
      showSubForm = true;
    }
  }

  return (
    <div className="display-flex-column enable-cursor-pointer">
      <div className="container-tile padding-left-5 margin-bottom-5">
        <div className="display-flex">
          <ToolbarButton
            disabled={readOnly}
            onClick={() => subFormPanelSwitch()}
            isActive={true}
            t={t}
            label=""
            className="material-icons display-flex"
            icon={
              subFormPanel?.[question.key] ? 'arrow_drop_down' : 'arrow_drop_up'
            }
          />
          <div className="subFormTitle" style={{ marginRight: '15px' }}>
            {question.label}
          </div>
          {showSubForm && (
            <div
              className="margin-left-5 flex-align-center"
              onClick={() => onEditSubFormClick(measurementData)}
              disabled={readOnly}
            >
              <ToolbarButton
                disabled={readOnly}
                className={'material-icons icon-size icon-color'}
                icon={'list_alt'}
                label=""
              />
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

const onEditSubFormClick = measurementData => {
  const dispatchActions = require('@ohif/viewer/src/appExtensions/MeasurementsPanel/dispatchActions.js')
    .default;
  dispatchActions.dispatchEditSubForm(measurementData);
};

export default SubFormPanel;
