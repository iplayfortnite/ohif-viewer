import React from 'react';

function TextArea(props) {
  const { question, value, onChange, disabled } = props;

  return (
    <textarea
      type="text"
      name={question.key}
      value={value}
      onChange={onChange}
      disabled={disabled}
      className="study-form-text study-form-textarea"
    ></textarea>
  );
}

export default TextArea;
