import React, { memo } from 'react';
import { getAvatarStyle } from '@ohif/ui/src/components/measurementTable/Utils.js';
import classnames from 'classnames';
import store from '@ohif/viewer/src/store';
import Checkbox from '../Checkbox';
import Label from '../Label';
import './GroupedCheckBox.styl';
import ViewerFormAvatar from '../ViewerFormAvatar.js';
import {
  avatarClick,
  onAvatarMouseEnter,
  onAvatarMouseLeave,
} from '../../../../utils';

function GroupedCheckBox(props) {
  const {
    label,
    option,
    question,
    onClick,
    className,
    percentColor,
    checked,
    disabled,
    readOnly,
    multipleTaskNotes,
    taskIds,
    multipleTaskUser,
    index,
    indexValue,
    measurements,
    slice,
    isSingleUser,
    notes,
  } = props;

  const state = store.getState();
  const users = state.flywheel.allUsers;
  const viewerAvatar = state.viewerAvatar.viewerAvatar;
  const singleUserTaskId = state.viewerAvatar.taskId;

  let dispatchActions = null;
  let userAvatarClickedDetails = [];

  return (
    <div style={{ display: 'flex' }}>
      {(() => {
        if (!isSingleUser) {
          return (
            <label className={`group-check-container cursorunset ${className}`}>
              {label}
              <input
                type="checkbox"
                onClick={onClick}
                name={question.key}
                value={option.value}
                onChange={e => {}}
                checked={checked}
                disabled={disabled}
                readOnly={readOnly}
              />
              <span
                className="checkbox-mark"
                style={{
                  backgroundColor: percentColor || 'white',
                }}
              ></span>
            </label>
          );
        } else {
          return (
            <label className={`${className}`} style={{ width: '100%' }}>
              <Label
                key={`${option.value}_${indexValue}_${index}_${question.key}`}
                className={classnames(
                  question.style === 'buttons'
                    ? 'study-form-radio-button'
                    : 'study-form-radio',
                  { checked, disabled }
                )}
              >
                <Checkbox
                  onClick={() => onClick(option, question)}
                  question={question}
                  option={option}
                  checked={checked}
                  disabled={disabled}
                  readOnly={readOnly}
                />
                {option.label || option.value}
              </Label>
            </label>
          );
        }
      })()}

      {taskIds.map(taskId => {
        const userId = multipleTaskNotes[taskId]?.userId;
        if (userId) {
          const user = users[userId];
          const { avatarStyle, initials, avatar } = getAvatarStyle(user);

          if (
            !userAvatarClickedDetails.find(
              task =>
                task[initials + '-' + taskId] === true ||
                task[initials + '-' + taskId] === false
            )
          ) {
            userAvatarClickedDetails.push({
              [initials + '-' + taskId]: false,
              initials: initials,
              taskId: taskId,
              slice: slice,
              visible: false,
            });
          }

          let questionValue;

          if ((checked && !isSingleUser) || isSingleUser) {
            if (singleUserTaskId === taskId && isSingleUser) {
              questionValue = notes.slices
                ? notes.slices[slice]?.[question.key]
                : notes[question.key];
            } else {
              questionValue = multipleTaskNotes?.[taskId]?.notes?.[0]?.slices
                ? multipleTaskNotes[taskId].notes?.[0]?.slices[slice]?.[
                    question.key
                  ]
                : multipleTaskNotes?.[taskId]?.notes?.[0]?.[question.key];
            }

            questionValue = !questionValue ? [] : questionValue;

            if (questionValue.includes(option.value)) {
              const userId = multipleTaskNotes[taskId].userId;
              const user = users[userId];
              const { avatarStyle, initials, avatar } = getAvatarStyle(user);

              avatarStyle.color = 'black';
              avatarStyle.borderWidth = '1px';
              avatarStyle.borderStyle = 'solid';

              if (viewerAvatar.length) {
                viewerAvatar.forEach(avatar => {
                  const data = avatar.initials + '-' + avatar.taskId;
                  const avatarElements = document.getElementsByClassName(data)
                    .length;
                  if (!avatar[data] && avatarElements > 0) {
                    for (let n = 0; n < avatarElements; n++) {
                      document
                        .getElementsByClassName(data)
                        .item(n).style.opacity = 0.3;
                    }
                  } else if (avatarElements > 0 && avatar[data]) {
                    for (let n = 0; n < avatarElements; n++) {
                      document
                        .getElementsByClassName(data)
                        .item(n).style.opacity = 1;
                    }
                  }
                });
              }

              return (
                <ViewerFormAvatar
                  initials={initials}
                  slice={slice}
                  taskId={taskId}
                  avatar={avatar}
                  avatarStyle={avatarStyle}
                  avatarClick={avatarClick}
                  userAvatarClickedDetails={userAvatarClickedDetails}
                  onAvatarMouseEnter={onAvatarMouseEnter}
                  dispatchActions={dispatchActions}
                  measurements={measurements}
                  user={user}
                  questionKey={question.key}
                  onAvatarMouseLeave={onAvatarMouseLeave}
                />
              );
            }
          }
        }
      })}
    </div>
  );
}

export default memo(GroupedCheckBox);
