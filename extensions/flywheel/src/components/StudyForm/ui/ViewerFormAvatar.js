import React from 'react';

export default function ViewerFormAvatar(props) {
  const {
    initials,
    slice,
    taskId,
    avatar,
    avatarStyle,
    avatarClick,
    userAvatarClickedDetails,
    onAvatarMouseEnter,
    dispatchActions,
    measurements,
    user,
    questionKey,
    onAvatarMouseLeave,
  } = props;

  return (
    <div className="avatar-wrapper">
      <div
        className={`avatar viewerFormAvatar ${initials + '-' + taskId}`}
        style={avatarStyle}
        onClick={e =>
          avatarClick(
            userAvatarClickedDetails,
            slice,
            initials,
            taskId,
            e,
            dispatchActions,
            measurements
          )
        }
        onMouseEnter={e =>
          onAvatarMouseEnter(
            e,
            dispatchActions,
            measurements,
            user,
            questionKey,
            taskId,
            slice,
            initials
          )
        }
        onMouseLeave={e =>
          onAvatarMouseLeave(
            e,
            dispatchActions,
            measurements,
            taskId,
            slice,
            initials
          )
        }
      >
        {avatar ? '' : initials}
      </div>
    </div>
  );
}
