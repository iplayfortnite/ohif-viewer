import React from 'react';

function TextBox(props) {
  const { question, value, onChange, disabled } = props;

  return (
    <input
      type="text"
      name={question.key}
      value={value}
      onChange={onChange}
      disabled={disabled}
      className="study-form-text"
    ></input>
  );
}

export default TextBox;
