import { Component } from 'react';
import React from 'react';
import PropTypes from 'prop-types';
import noop from 'lodash/noop';
import classnames from 'classnames';
import cloneDeep from 'lodash.clonedeep';
import { SimpleDialog } from '@ohif/ui';
import bounding from '@ohif/viewer/src/lib/utils/bounding.js';
import store from '@ohif/viewer/src/store';
import { isQuestionDisplay } from '../utils/SubFormQuestionVisibility.js';
import ui from '../ui/index.js';
import { isOptionAvailable } from '../validation/ValidationEngine.js';

import '../StudyForm.styl';
import '../SubForm.styl';
import './SubFormDialog.styl';

const { Radio, SelectBox, Checkbox, TextBox, TextArea, Label } = ui;
const DROPDOWN_DEFAULT_VALUE = '--- SELECT ---';

export default class SubFormDialog extends Component {
  static defaultProps = {
    componentRef: React.createRef(),
    componentStyle: {},
  };

  static propTypes = {
    onClose: PropTypes.func.isRequired,
    onConfirm: PropTypes.func.isRequired,
    componentRef: PropTypes.object,
    componentStyle: PropTypes.object,
    subFormComponent: PropTypes.arrayOf(
      PropTypes.shape({
        key: PropTypes.string.isRequired,
        label: PropTypes.string,
        defaultValue: PropTypes.string,
        values: PropTypes.arrayOf(
          PropTypes.shape({
            directive: PropTypes.string,
            instructionSet: PropTypes.array,
            label: PropTypes.string,
            value: PropTypes.string,
            excludeMeasurements: PropTypes.arrayOf(PropTypes.string),
            requireMeasurements: PropTypes.arrayOf(PropTypes.string),
          })
        ),
        validate: PropTypes.shape({
          required: PropTypes.bool,
          custom: PropTypes.object,
          minSelectedCount: PropTypes.number,
          maxSelectedCount: PropTypes.number,
        }),
        type: PropTypes.oneOf([
          'content',
          'radio',
          'text',
          'selectboxes',
          'textarea',
          'textfield',
          'dropdown',
        ]).isRequired,
        // This is for conditionally rendering this question.
        conditional: PropTypes.shape({
          // https://github.com/jwadhams/json-logic-js/
          json: PropTypes.object,
        }),
      })
    ),
    annotationId: PropTypes.string,
    slice: PropTypes.number,
    question: PropTypes.shape({
      key: PropTypes.string.isRequired,
      label: PropTypes.string,
      defaultValue: PropTypes.string,
      values: PropTypes.arrayOf(
        PropTypes.shape({
          directive: PropTypes.string,
          instructionSet: PropTypes.array,
          label: PropTypes.string,
          value: PropTypes.string,
          excludeMeasurements: PropTypes.arrayOf(PropTypes.string),
          requireMeasurements: PropTypes.arrayOf(PropTypes.string),
        })
      ),
      validate: PropTypes.shape({
        required: PropTypes.bool,
        custom: PropTypes.object,
        minSelectedCount: PropTypes.number,
        maxSelectedCount: PropTypes.number,
      }),
      type: PropTypes.oneOf([
        'content',
        'radio',
        'text',
        'selectboxes',
        'textarea',
        'textfield',
        'dropdown',
      ]).isRequired,
      // This is for conditionally rendering this question.
      conditional: PropTypes.shape({
        // https://github.com/jwadhams/json-logic-js/
        json: PropTypes.object,
      }),
    }),
    studyFormState: PropTypes.object,
    clickType: PropTypes.func,
    formStyles: PropTypes.object,
    onClickQuestionValues: PropTypes.func,
    getValueSubForm: PropTypes.func,
    formatOptions: PropTypes.func,
    readOnly: PropTypes.bool,
    measurementLabels: PropTypes.object,
    questionRefs: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {
      subFormNotes: {},
    };
    this.mainElement = React.createRef();
  }

  componentDidMount() {
    bounding(this.mainElement);
    const { studyFormState } = this.props;
    if (!Object.keys(this.state.subFormNotes).length) {
      const studyStateNotes = cloneDeep(studyFormState.notes);
      this.setState({ subFormNotes: studyStateNotes });
    }
  }

  render() {
    const {
      subFormComponent,
      annotationId,
      slice,
      question,
      studyFormState,
      clickType,
      formStyles,
      onClickQuestionValues,
      getValueSubForm,
      formatOptions,
      readOnly,
      measurementLabels,
      questionRefs,
      onClose,
      onConfirm,
    } = this.props;
    const projectConfig = store.getState().flywheel.projectConfig;
    const studyState = cloneDeep(studyFormState);
    studyState.notes = cloneDeep(this.state.subFormNotes);
    return (
      <div className={'subFormOpacity'}>
        <SimpleDialog
          headerTitle="SubForm Question"
          onClose={() => {}}
          onConfirm={() => {}}
          componentRef={this.mainElement}
          showFooterButtons={false}
          hideCloseButton={true}
        >
          {subFormComponent.length &&
            subFormComponent.map((subFormQuestion, subFormIndex) => {
              const questionDisplay = isQuestionDisplay(
                subFormQuestion,
                annotationId,
                slice,
                question,
                projectConfig,
                this.state.subFormNotes
              );
              let notesForSubForm = {};
              let value = '';
              let subFormName = '';
              if (this.state.subFormNotes?.slices) {
                value =
                  this.state.subFormNotes.slices?.[slice]?.[question.key]
                    ?.value ||
                  this.state.subFormNotes.slices?.[slice]?.[question.key];
                subFormName = projectConfig?.studyForm?.components
                  .find(qs => qs.key === question.key)
                  .values?.find(val => val.value === value)?.subForm;
                notesForSubForm = this.state.subFormNotes.slices?.[slice]?.[
                  question.key
                ]?.[subFormName]?.find(
                  SubFormTabNotes =>
                    SubFormTabNotes.annotationId === annotationId
                );
              } else {
                value =
                  this.state.subFormNotes[question.key]?.value ||
                  this.state.subFormNotes[question.key];
                subFormName = projectConfig?.studyForm?.components
                  .find(qs => qs.key === question.key)
                  .values?.find(val => val.value === value)?.subForm;
                notesForSubForm = this.state.subFormNotes[question.key]?.[
                  subFormName
                ]?.find(
                  SubFormTabNotes =>
                    SubFormTabNotes.annotationId === annotationId
                );
              }
              if (questionDisplay) {
                return (
                  <div
                    key={`${annotationId}_${slice}_${subFormIndex}_${subFormQuestion.key}_${question.key}`}
                    ref={element =>
                      (questionRefs[subFormQuestion.key] = element)
                    }
                    className={classnames([
                      'study-form-question',
                      {
                        'has-error': studyFormState?.errors?.slices?.[slice]?.[
                          question.key
                        ]?.[subFormName]?.find(
                          item => item.annotationId === annotationId
                        )?.[subFormQuestion.key]
                          ? studyFormState.errors.slices[slice][question.key][
                              subFormName
                            ].find(
                              item => item.annotationId === annotationId
                            )?.[subFormQuestion.key]
                          : studyFormState?.errors?.[question.key]?.[
                              subFormName
                            ]?.find(
                              item => item.annotationId === annotationId
                            )?.[subFormQuestion.key],
                      },
                    ])}
                    style={formStyles}
                  >
                    <div className="study-form-label">
                      {subFormQuestion.label}
                    </div>
                    <div>
                      {subFormQuestion.type === 'content' && (
                        <div
                          dangerouslySetInnerHTML={{
                            __html: this.cleanHTML(),
                          }}
                        />
                      )}
                      {subFormQuestion.type === 'radio' &&
                        subFormQuestion.values.map((option, i) => {
                          let checked = false;
                          if (notesForSubForm === undefined) {
                            checked = false;
                          } else {
                            checked =
                              notesForSubForm[subFormQuestion.key] ===
                              option.value;
                          }
                          const disabled =
                            readOnly ||
                            !isOptionAvailable(
                              option,
                              measurementLabels,
                              projectConfig,
                              slice,
                              true,
                              studyState
                            );
                          return (
                            <Label
                              key={`${option.value}_${i}_${subFormQuestion.key}_${question.key}`}
                              className={classnames(
                                subFormQuestion.style === 'buttons'
                                  ? 'study-form-radio-button'
                                  : 'study-form-radio',
                                { checked, disabled }
                              )}
                            >
                              <Radio
                                option={option}
                                question={subFormQuestion}
                                onClick={() => {
                                  let updatedNotes = this.state.subFormNotes
                                    .slices
                                    ? this.state.subFormNotes.slices[slice]
                                    : this.state.subFormNotes;
                                  if (!updatedNotes) {
                                    updatedNotes = {};
                                  }
                                  const questionValue =
                                    updatedNotes[question.key]?.value ||
                                    updatedNotes[question.key];
                                  const questionSubFormName = projectConfig?.studyForm?.components
                                    .find(qs => qs.key === question.key)
                                    .values?.find(
                                      val => val.value === questionValue
                                    )?.subForm;
                                  if (question.values) {
                                    question.values.forEach(item => {
                                      if (item.value === option.value) {
                                        if (
                                          questionSubFormName &&
                                          typeof updatedNotes[question.key] ===
                                            'string'
                                        ) {
                                          updatedNotes[question.key] = {};
                                        }
                                        if (!updatedNotes[question.key]) {
                                          updatedNotes[question.key] = {};
                                        }
                                        if (
                                          !updatedNotes[question.key]?.[
                                            questionSubFormName
                                          ]
                                        ) {
                                          updatedNotes[question.key][
                                            questionSubFormName
                                          ] = [];
                                        }
                                        const isSubFormNotes = updatedNotes[
                                          question.key
                                        ][questionSubFormName].find(
                                          item =>
                                            item.annotationId === annotationId
                                        );
                                        if (!isSubFormNotes) {
                                          const subFormNotes = {
                                            annotationId: annotationId,
                                            [subFormQuestion.key]:
                                              option.value ===
                                              DROPDOWN_DEFAULT_VALUE
                                                ? ''
                                                : option.value,
                                          };
                                          updatedNotes[question.key][
                                            questionSubFormName
                                          ].push(subFormNotes);
                                        } else {
                                          updatedNotes[question.key][
                                            questionSubFormName
                                          ].find(
                                            item =>
                                              item.annotationId === annotationId
                                          )[subFormQuestion.key] =
                                            option.value ===
                                            DROPDOWN_DEFAULT_VALUE
                                              ? ''
                                              : option.value;
                                        }
                                      }
                                    });
                                  } else {
                                    updatedNotes[question.key][
                                      questionSubFormName
                                    ].find(
                                      item => item.annotationId === annotationId
                                    )[subFormQuestion.key] =
                                      option.value === DROPDOWN_DEFAULT_VALUE
                                        ? ''
                                        : option.value;
                                  }
                                  if (
                                    question.key &&
                                    subFormName &&
                                    annotationId
                                  ) {
                                    clickType(
                                      option,
                                      subFormQuestion.key,
                                      question.key,
                                      subFormName,
                                      annotationId
                                    );
                                    onClickQuestionValues(
                                      subFormQuestion,
                                      true,
                                      subFormName
                                    );
                                  }
                                }}
                                radioChecked={checked}
                                radioDisabled={disabled}
                                radioReadOnly={readOnly}
                                subFormIndex={i}
                              />
                              {option.label || option.value}
                            </Label>
                          );
                        })}
                      {subFormQuestion.type === 'dropdown' && (
                        <div className="drop-down-container">
                          <SelectBox
                            value={getValueSubForm()}
                            onChange={e => {
                              clickType(
                                subFormQuestion.values.find(
                                  x =>
                                    x.value === e.target.value ||
                                    x.label === e.target.value
                                ),
                                subFormQuestion.key,
                                question.key,
                                subFormName,
                                annotationId
                              );
                              onClickQuestionValues(
                                subFormQuestion,
                                true,
                                subFormName
                              );
                            }}
                            options={formatOptions()}
                          />
                        </div>
                      )}
                      {subFormQuestion.type === 'selectboxes' &&
                        subFormQuestion.values.map((option, i) => {
                          let stateValue = [];
                          if (notesForSubForm === undefined) {
                            stateValue = [];
                          } else {
                            stateValue = notesForSubForm[subFormQuestion.key];
                          }
                          const checked =
                            (stateValue && stateValue.includes(option.value)) ||
                            false;
                          const disabled =
                            readOnly ||
                            !isOptionAvailable(
                              option,
                              measurementLabels,
                              projectConfig,
                              slice,
                              true,
                              studyState
                            );
                          return (
                            <Label
                              key={`${option.value}_${i}_${subFormQuestion.key}_${question.key}`}
                              className={classnames(
                                subFormQuestion.style === 'buttons'
                                  ? 'study-form-radio-button'
                                  : 'study-form-radio',
                                { checked, disabled }
                              )}
                            >
                              <Checkbox
                                onClick={() => onClickQuestionValues()}
                                question={subFormQuestion}
                                option={option}
                                checked={checked}
                                disabled={disabled}
                                readOnly={readOnly}
                              />
                              {option.label || option.value}
                            </Label>
                          );
                        })}
                      {['text', 'textarea'].includes(subFormQuestion.type) && (
                        <TextArea
                          question={subFormQuestion}
                          value={getValueSubForm()}
                          onChange={noop}
                          disabled={readOnly}
                        />
                      )}
                      {subFormQuestion.type === 'textfield' && (
                        <TextBox
                          question={subFormQuestion}
                          value={getValueSubForm()}
                          onChange={noop}
                          disabled={readOnly}
                        />
                      )}
                    </div>
                  </div>
                );
              }
            })}

          <div className="button-container">
            <div className="button-style-close" onClick={onConfirm}>
              Close
            </div>
          </div>
        </SimpleDialog>
      </div>
    );
  }

  cleanHTML = html => {
    return html.replace(/<script[^>]*>[^<]*<\/script>/gm, '');
  };
}
