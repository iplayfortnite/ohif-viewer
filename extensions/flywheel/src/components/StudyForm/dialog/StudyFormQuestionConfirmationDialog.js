import { Component } from 'react';
import React from 'react';
import PropTypes from 'prop-types';
import { SimpleDialog } from '@ohif/ui';
import bounding from '@ohif/viewer/src/lib/utils/bounding.js';
import classnames from 'classnames';
import Radio from '../ui/Radio';
import SelectBox from '../ui/SelectBox';
import Checkbox from '../ui/Checkbox';
import Label from '../ui/Label';
import './StudyFormQuestionConfirmationDialog.styl';

export default class StudyFormQuestionConfirmationDialog extends Component {
  static defaultProps = {
    componentRef: React.createRef(),
    componentStyle: {},
  };

  static propTypes = {
    onClose: PropTypes.func.isRequired,
    onConfirm: PropTypes.func.isRequired,
    componentRef: PropTypes.object,
    componentStyle: PropTypes.object,
    question: PropTypes.object,
    value: PropTypes.object,
    clickQuestion: PropTypes.func,
    formatOptions: PropTypes.func,
    getValue: PropTypes.func,
    slice: PropTypes.number,
    selectBoxOnChange: PropTypes.func,
    onClickQuestionValues: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.mainElement = React.createRef();
  }

  componentDidMount() {
    bounding(this.mainElement);
  }

  render() {
    const {
      question,
      value,
      clickQuestion,
      formatOptions,
      getValue,
      slice,
      selectBoxOnChange,
      onClickQuestionValues,
    } = this.props;
    return (
      <SimpleDialog
        headerTitle="Confirm Study Form Question Answer"
        onClose={() => {}}
        onConfirm={() => {}}
        componentRef={this.mainElement}
        showFooterButtons={false}
        hideCloseButton={true}
      >
        <div className="study-form-question">
          <div className="study-form-label">{question.label}</div>
          <div>
            {question.type === 'radio' &&
              question.values &&
              question.values.map((option, index) => {
                if (option.requireMeasurements?.length) {
                  let checked = option.value === value.value;
                  const disabled = false;
                  const readOnly = false;
                  return (
                    <Label
                      key={`${option.value}_${index}_${question.key}`}
                      className={classnames(
                        question.style === 'buttons'
                          ? 'study-form-radio-button'
                          : 'study-form-radio',
                        { checked, disabled }
                      )}
                    >
                      <Radio
                        option={option}
                        question={question}
                        onClick={() => {
                          value.value = option.value;
                          clickQuestion(option.value);
                        }}
                        radioChecked={checked}
                        radioDisabled={disabled}
                        radioReadOnly={readOnly}
                        subFormIndex={index}
                      />
                      {option.label || option.value}
                    </Label>
                  );
                }
              })}

            {question.type === 'dropdown' && (
              <div className="drop-down-container">
                <SelectBox
                  value={getValue(slice, question)}
                  onChange={e =>
                    selectBoxOnChange(
                      question.values.find(
                        x =>
                          x.value === e.target.value ||
                          x.label === e.target.value
                      ),
                      question
                    )
                  }
                  options={formatOptions()}
                />
              </div>
            )}
            {question.type === 'selectboxes' &&
              question.values.map((option, i) => {
                const readOnly = false;
                const checked = option.value === value;
                const disabled = false;
                return (
                  <Label
                    key={`${option.value}_${i}_${question.key}`}
                    className={classnames(
                      question.style === 'buttons'
                        ? 'study-form-radio-button'
                        : 'study-form-radio',
                      { checked, disabled }
                    )}
                  >
                    <Checkbox
                      onClick={() => onClickQuestionValues(question)}
                      question={question}
                      option={option}
                      checked={checked}
                      disabled={disabled}
                      readOnly={readOnly}
                    />
                    {option.label || option.value}
                  </Label>
                );
              })}
          </div>

          <div className="button-container">
            <div className="button-style-close" onClick={this.props.onConfirm}>
              Close
            </div>
          </div>
        </div>
      </SimpleDialog>
    );
  }
}
