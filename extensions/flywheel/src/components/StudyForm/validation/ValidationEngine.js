import jsonLogic from 'json-logic-js';
import pick from 'lodash/pick';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import OHIF from '@ohif/core';
import store from '@ohif/viewer/src/store';
import { Utils } from '@flywheel/extension-flywheel-common';
import Redux from '../../../redux';
import { getSliceInfo } from '@ohif/viewer/src/appExtensions/MeasurementsPanel/Utils.js';
import difference from 'lodash/difference';
import cloneDeep from 'lodash.clonedeep';
import { containersUtils } from '../../../utils';

const { selectUser } = Redux.selectors;
const measurementApi = OHIF.measurements.MeasurementApi;
const { getImageIdForImagePath } = OHIF.measurements;
const {
  isCurrentFile,
  hasInstructionRange,
  hasLabelsLimitReached,
  getLabelCount,
  getLabelLimits,
} = Utils;
const { getContainersIds, getContainers } = containersUtils;

/**
 * Check if question should be visible in form UI
 * @param {Object} question
 * @param {Number} sliceNo
 * @param {Object} notes
 * @param {Array<string>} allProcessableHiddenQns
 * @returns {Boolean} True, if question is visible. False, otherwise
 */
const _checkQuestionVisibility = (
  question,
  sliceNo,
  notes,
  allProcessableHiddenQns,
  props,
  stateNotes,
  previousVisibleQuestion
) => {
  if (allProcessableHiddenQns.includes(question.key)) {
    return false;
  }
  const { projectConfig } = props;
  const canShow = _canShowQuestion(
    question.key,
    sliceNo,
    projectConfig.bSlices,
    stateNotes,
    projectConfig
  );
  if (canShow) {
    const isConditionValidated = _evaluateQuestionCondition(
      question,
      sliceNo,
      notes,
      allProcessableHiddenQns,
      props,
      stateNotes,
      previousVisibleQuestion
    );
    return (
      isConditionValidated && !allProcessableHiddenQns.includes(question.key)
    );
  }
  return false;
};

const _canShowQuestion = (
  questionKey,
  sliceNo,
  bSlice,
  notes,
  projectConfig
) => {
  const state = store.getState();
  if (!isCurrentFile(state) && bSlice?.settings) {
    if (!Object.keys(bSlice?.settings).includes(`${sliceNo}`)) return false; // not a bSlice, so no questions are shown

    if (bSlice.required?.all?.includes(questionKey)) return true;

    if (bSlice.required?.specific?.[sliceNo]?.includes(questionKey))
      return true;

    if (bSlice.settings?.[sliceNo]?.hiddenQuestions?.includes(questionKey))
      return false;

    if (bSlice.required?.any?.includes(questionKey)) return true;

    if (bSlice.required?.one?.includes(questionKey))
      return !_checkAnswerExistInAnySlice(
        questionKey,
        bSlice,
        sliceNo,
        notes,
        projectConfig
      );
  }
  return true;
};

let previousQuestion = null;
let questionsVisibleStatus = {};

const _isDrawnInstructedMeasurements = (
  instruction,
  labels,
  measuresBasedOnQuestion
) => {
  return instruction.requireMeasurements.some(x => {
    const labelItem = labels.find(
      label => label.label == x || label.value === x
    );
    const count = measuresBasedOnQuestion.filter(
      m => m.location === labelItem?.label || m.location === labelItem?.value
    ).length;

    if (count) {
      return !labelItem?.limit ? true : count <= labelItem?.limit;
    }
  });
};

const _hasInstructedMeasurements = ({
  previousVisibleQuestion,
  questionAnswer,
  question,
  sliceInfo,
}) => {
  const questionOption = previousVisibleQuestion?.values.find(
    x => x.label === questionAnswer || x.value === questionAnswer
  );
  if (!questionOption) {
    return null;
  }
  const state = store.getState();
  const measurements = state.timepointManager.measurements;
  const projectConfig = state.flywheel?.projectConfig;
  const labels = projectConfig?.labels || [];
  const isCurrentFileType = isCurrentFile(state);
  if (questionOption.directive || questionOption.instructionSet) {
    const measuresBasedOnQuestion = Object.keys(measurements).reduce(
      (accumulator, currentItem) => {
        const filteredMeasurements = measurements[currentItem].filter(x => {
          if (isCurrentFileType) {
            return x.questionKey === previousVisibleQuestion?.key;
          } else {
            const annotationSliceInfo = getSliceInfo(x);
            return (
              annotationSliceInfo?.sliceNumber === sliceInfo?.sliceNumber &&
              x.questionKey === previousVisibleQuestion?.key
            );
          }
        });
        if (filteredMeasurements.length) {
          accumulator = accumulator.concat(filteredMeasurements);
        }
        return accumulator;
      },
      []
    );
    previousQuestion = question;
    if (questionOption.directive && labels.length) {
      const hasMeasurements = _isDrawnInstructedMeasurements(
        questionOption,
        labels,
        measuresBasedOnQuestion
      );
      questionsVisibleStatus[question.key] = hasMeasurements;
      return hasMeasurements;
    }
    if (questionOption.instructionSet) {
      const hasMeasurements = questionOption.instructionSet.some(x =>
        _isDrawnInstructedMeasurements(x, labels, measuresBasedOnQuestion)
      );
      questionsVisibleStatus[question.key] = hasMeasurements;
      return hasMeasurements;
    }
  }
  return null;
};

const hasInstruction = (showQuestion, question, previousVisibleQuestion) => {
  return (
    showQuestion &&
    question?.values &&
    previousVisibleQuestion &&
    question.key !== previousVisibleQuestion?.key &&
    previousVisibleQuestion?.values?.some(x => x.directive || x.instructionSet)
  );
};

/**
 * Evaluate currently answered questions and any considerable hidden questions
 * satisfies given question's conditionals
 * @param {Object} question
 * @param {Number} sliceNo
 * @param {Object} notes
 * @param {Array<string>} allProcessableHiddenQns
 * @returns {Boolean} True, if question criteria is met(also supports hidden question).
 */
const _evaluateQuestionCondition = (
  question,
  sliceNo,
  notes,
  allProcessableHiddenQns,
  props,
  stateNotes,
  previousVisibleQuestion
) => {
  if (!question.conditional?.json) {
    previousQuestion = question;
    return true;
  }

  const applicableHiddenQns = _getHiddenQuestionsInCondition(
    [question.conditional?.json],
    sliceNo,
    notes,
    allProcessableHiddenQns,
    props,
    stateNotes,
    previousVisibleQuestion
  );
  let extraAnswers = {};
  for (let qIndex = 0; qIndex < applicableHiddenQns.length; qIndex++) {
    const questionKey = applicableHiddenQns[qIndex];
    extraAnswers[questionKey] = _getHiddenQuestionEligibleAnswers(
      questionKey,
      props
    );
  }
  if (applicableHiddenQns.length) {
    // Generate answer combinations for applicable hidden questions
    // to check any one combination with current answers passes jsonLogic
    const combinations = _generateCombinations(extraAnswers, 0, []);
    for (const combination of combinations) {
      if (
        jsonLogic.apply(question.conditional.json, {
          ...notes,
          ...combination,
        })
      ) {
        return true;
      }
    }
  }

  const ids = getContainersIds();
  let customInfoContainers = {};
  if (ids) {
    customInfoContainers = getContainers(ids);
  }
  const studyFormNotes = { ...customInfoContainers };

  Object.keys(notes).forEach(key => {
    if (typeof notes[key] === 'object') {
      studyFormNotes[key] = notes[key].value;
    } else {
      studyFormNotes[key] = notes[key];
    }
  });

  const TaskIds = store.getState().multipleReaderTask?.TaskIds;
  let showQuestion = jsonLogic.apply(question.conditional.json, studyFormNotes);

  if (
    (!showQuestion &&
      (props.projectConfig?.studyFormWorkflow === 'ROI' ||
        props.projectConfig?.studyFormWorkflow === 'Mixed')) ||
    (TaskIds?.length > 1 && !showQuestion)
  ) {
    showQuestion = question.positiveAnswers?.includes(
      studyFormNotes?.[question.key]
    );
  }
  const questionAnswer = notes[previousQuestion?.key];
  const previousQuestionKey = previousQuestion?.key || '';
  const currentUsedData =
    question && question?.conditional?.json
      ? jsonLogic.uses_data(question.conditional.json, studyFormNotes)
      : [];
  const hasInstructionSet =
    Object.keys(notes).length &&
    previousQuestion?.values?.some(
      x =>
        (questionAnswer === x.label || questionAnswer === x.value) &&
        (x?.directive || x?.instructionSet)
    );
  const hasCommonUseData =
    hasInstructionSet &&
    currentUsedData.length &&
    !!currentUsedData.find(x => previousQuestionKey === x) &&
    question.key !== previousQuestion?.key;

  const { sliceInfo } = props;

  if (hasInstructionSet && hasCommonUseData) {
    if (!notes[previousQuestion.key]) {
      previousQuestion = question;
      questionsVisibleStatus[question.key] = false;
      return false;
    }
    const questionAnswer = notes[previousQuestion.key];
    const hasInstructedMeasurements = _hasInstructedMeasurements({
      questionAnswer,
      previousVisibleQuestion: previousQuestion,
      question,
      sliceInfo,
    });
    if (hasInstructedMeasurements !== null) {
      previousQuestion = question;
      return hasInstructedMeasurements;
    }
  }

  if (
    hasInstruction(showQuestion, question, previousQuestion) &&
    hasCommonUseData
  ) {
    const questionAnswer = notes[previousQuestion.key];
    const hasInstructedMeasurements = _hasInstructedMeasurements({
      questionAnswer,
      previousVisibleQuestion: previousQuestion,
      question,
      sliceInfo,
    });
    if (hasInstructedMeasurements !== null) {
      return hasInstructedMeasurements;
    }
  }

  const isValidQuestion =
    question?.values &&
    previousQuestion?.values &&
    questionsVisibleStatus[previousQuestion.key] !== undefined &&
    hasCommonUseData;
  if (showQuestion && Object.keys(notes).length && isValidQuestion) {
    const questionAnswer = notes[previousQuestion.key];
    const hasInstructedMeasurements = _hasInstructedMeasurements({
      questionAnswer,
      previousVisibleQuestion: previousQuestion,
      question,
      sliceInfo,
    });
    if (hasInstructedMeasurements !== null) {
      return hasInstructedMeasurements;
    }
    if (!questionsVisibleStatus[previousQuestion.key]) {
      questionsVisibleStatus[question.key] = false;
      return false;
    }
    previousQuestion = question;
    questionsVisibleStatus[question.key] = true;
    return true;
  }
  if (showQuestion) {
    previousQuestion = question;
    questionsVisibleStatus[question.key] = showQuestion;
  }
  return showQuestion;
};

/**
 * Get question keys of hidden questions in the given conditions of another question
 * @param {Array} conditions
 * @param {Number} sliceNo
 * @param {Object} notes
 * @param {Array<string>} allProcessableHiddenQns
 * @param {Array<string>} hiddenQnsInCondition (Optional for initial call)
 * @returns {Array<string>} Updated hiddenQnsInCondition
 */
const _getHiddenQuestionsInCondition = (
  conditions,
  sliceNo,
  notes,
  allProcessableHiddenQns,
  props,
  stateNotes,
  previousVisibleQuestion,
  hiddenQnsInCondition = []
) => {
  if (!conditions || !conditions.length) {
    return [];
  }

  const { projectConfig } = props;

  for (let c = 0; c < conditions.length; c++) {
    const condition = conditions[c];
    if (!condition) {
      continue;
    }
    const type = Object.keys(condition)[0]; // can be and, or, ==, != etc...
    if (type == 'and' || type == 'or') {
      // Get sub-condition questions
      _getHiddenQuestionsInCondition(
        condition[type],
        sliceNo,
        notes,
        allProcessableHiddenQns,
        props,
        stateNotes,
        previousVisibleQuestion,
        hiddenQnsInCondition
      );
      continue;
    }

    const questionKey = condition[type][0].var;
    const isQuestionProcessable =
      Object.keys(notes).includes(questionKey) ||
      allProcessableHiddenQns.includes(questionKey);
    const isHidden =
      allProcessableHiddenQns.includes(questionKey) ||
      !_canShowQuestion(
        questionKey,
        sliceNo,
        projectConfig.bSlices,
        stateNotes,
        projectConfig
      );
    if (isHidden) {
      if (!isQuestionProcessable) {
        // Check if the hidden question is evaluation eligible
        const hiddenQuestion = _getQuestion(questionKey, projectConfig);
        const isHiddenQuestionProcessable = _evaluateQuestionCondition(
          hiddenQuestion,
          sliceNo,
          notes,
          allProcessableHiddenQns,
          props,
          stateNotes,
          previousVisibleQuestion
        );
        // If this question is evaluation eligible,
        // push to list of all eligible hidden questions
        if (
          isHiddenQuestionProcessable &&
          !allProcessableHiddenQns.includes(questionKey)
        ) {
          allProcessableHiddenQns.push(questionKey);
        }
      } else {
        // If this question is evaluation eligible,
        // push to list of all eligible hidden questions
        if (!allProcessableHiddenQns.includes(questionKey)) {
          allProcessableHiddenQns.push(questionKey);
        }
        // Push processable hidden questions for generating
        // condition combinations of current question
        if (!hiddenQnsInCondition.includes(questionKey)) {
          hiddenQnsInCondition.push(questionKey);
        }
      }
    }
  }
  return hiddenQnsInCondition;
};

/**
 * Get answers of hidden question eligible for automatic selection
 * @param {string} questionKey
 * @returns {Array<string>} Eligible answers for the hidden question
 */
const _getHiddenQuestionEligibleAnswers = (questionKey, props) => {
  const { projectConfig } = props;
  const question = _getQuestion(questionKey, projectConfig);
  if (!question) {
    return [];
  }

  const answers = question.values.map(v => v.value);
  // If positive answers specified, don't use those as answer to hidden question
  const positiveAnswers = question.positiveAnswers || [];
  return answers.filter(item => !positiveAnswers.includes(item));
};

/**
 * Generate possible combinations of answers from applicable answers
 * @param {Object} extraAnswers Possible answers for hidden questions
 * @param {Number} itemIndex
 * @param {Array} combinations Possible combinations of notes for hidden questions
 * @returns {Array} Updated combinations with given extra answers
 */
const _generateCombinations = (extraAnswers, itemIndex, combinations) => {
  const questionKey = Object.keys(extraAnswers)[itemIndex];
  const values = extraAnswers[questionKey];
  let newCombinations = values.length ? [] : combinations;
  if (values.length) {
    if (!combinations.length) {
      combinations.push({});
    }
    for (let c = 0; c < combinations.length; c++) {
      newCombinations.push(
        ...values.map(v => ({ ...combinations[c], [questionKey]: v }))
      );
    }
    if (itemIndex + 1 < Object.keys(extraAnswers).length) {
      newCombinations = _generateCombinations(
        extraAnswers,
        itemIndex + 1,
        newCombinations
      );
    }
  }
  return newCombinations;
};

const _checkAnswerExistInAnySlice = (
  key,
  bSlice,
  exceptSlice = 0,
  notes,
  projectConfig
) => {
  const slices = notes.slices;
  if (!slices) {
    return false;
  }

  const question = _getQuestion(key, projectConfig);
  // check question is answered already in a slice other than current slice
  let answeredSlice = Object.keys(slices).find(sliceNo => {
    if (sliceNo == exceptSlice) {
      return false;
    }
    const answer = slices[sliceNo][key];
    if (question?.positiveAnswers?.length) {
      // verify if the answer is a positive answer
      return question.positiveAnswers.includes(answer);
    }
    return !!answer;
  });

  // check question is specifically required in a slice other than current slice
  if (!answeredSlice && bSlice.required?.specific) {
    answeredSlice = Object.keys(bSlice.required?.specific).find(sliceNo => {
      if (sliceNo == exceptSlice) {
        return false;
      }
      return bSlice.required?.specific[sliceNo].includes(key);
    });
  }
  return !!answeredSlice;
};

const validateWhileQuestionSwitches = (props, visibleQuestions, notes) => {
  const { projectConfig } = props;
  const allowDraft = projectConfig?.allowDraft !== false;
  const state = store.getState();
  let questionValidation = null;
  if (allowDraft) {
    let questionNotes = {};
    if (!isCurrentFile(state) && projectConfig.bSlices) {
      questionNotes.slices = {};
      const bSlices = getBSlices(projectConfig);
      for (const sliceNo of bSlices) {
        questionNotes.slices[sliceNo] = getQuestionsInNotes(
          sliceNo,
          props,
          notes
        );
      }
    } else {
      questionNotes = visibleQuestions;
    }
    const { errors } = validateForm(
      questionNotes,
      notes,
      props.measurementLabels,
      projectConfig
    );
    if (!isCurrentFile(state) && projectConfig.bSlices) {
      const bSlices = getBSlices(projectConfig);
      for (const sliceNo of bSlices) {
        if (
          errors.slices &&
          errors.slices[sliceNo] &&
          Object.keys(errors.slices[sliceNo]).length > 0
        ) {
          questionValidation = false;
        } else if (
          errors.slices &&
          errors.slices[sliceNo] &&
          Object.keys(errors.slices[sliceNo]).length === 0
        ) {
          questionValidation = true;
        }
      }
    } else {
      if (Object.keys(errors).length > 0) {
        questionValidation = false;
      } else if (Object.keys(errors).length === 0) {
        questionValidation = true;
      }
    }
  }
  return questionValidation;
};

const validateForm = (
  questions,
  answers,
  measurements,
  projectConfig,
  props = null,
  studyFormState = null
) => {
  let errors = {};
  const notes = {};
  let allLabels = [];
  let allTools = [];
  const state = store.getState();
  if (!isCurrentFile(state) && projectConfig.bSlices) {
    notes.slices = {};
    errors.slices = {};
    const bSlices = getBSlices(projectConfig);
    for (const sliceNo of bSlices) {
      if (answers.slices && answers.slices[sliceNo]) {
        for (const question of questions.slices[sliceNo]) {
          const { key, validate, values } = question;
          if (!notes.slices[sliceNo]) {
            notes.slices[sliceNo] = {};
          }
          if (!errors.slices[sliceNo]) {
            errors.slices[sliceNo] = {};
          }
          notes.slices[sliceNo][key] = answers.slices[sliceNo][key];

          if (
            projectConfig.studyFormWorkflow === 'Form' &&
            typeof answers.slices[sliceNo][key] === 'object'
          ) {
            const validateDetail = _validateSubFormQuestion(
              answers.slices[sliceNo],
              sliceNo,
              key,
              errors.slices[sliceNo],
              projectConfig,
              measurements,
              allLabels,
              allTools,
              notes.slices[sliceNo]
            );
            errors.slices[sliceNo] = validateDetail.errors;
            allLabels = validateDetail.allLabels;
            allTools = validateDetail.allTools;
          }

          if (validate && validate.required && !notes.slices[sliceNo][key]) {
            errors.slices[sliceNo][key] = 'required';
          } else if (values) {
            let selected = null;
            if (Array.isArray(notes.slices[sliceNo][key])) {
              selected = values.find(({ value }) =>
                notes.slices[sliceNo][key].includes(value)
              );
            } else if (typeof notes.slices[sliceNo][key] === 'object') {
              selected = values.find(
                ({ value }) => notes.slices[sliceNo][key]?.value === value
              );
            } else {
              selected = values.find(
                ({ value }) => value === notes.slices[sliceNo][key]
              );
            }
            if (!selected) {
              continue;
            }
            // deprecated, use form.io component.validate.custom
            if (selected.require) {
              for (const [requireKey, requireValue] of Object.entries(
                selected.require
              )) {
                if (answers.slices[sliceNo][requireKey] !== requireValue) {
                  errors.slices[sliceNo][key] = 'invalid';
                }
              }
            }
            if (hasInstructionRange(selected) || selected?.instructionSet) {
              const hasCompleted = hasAnnotatedBasedOnInstructionRange(
                question,
                selected,
                sliceNo,
                projectConfig.bSlices?.settings || null
              );
              if (!hasCompleted) {
                errors.slices[sliceNo][key] = 'invalid';
              }
            } else if (Array.isArray(selected)) {
              if (
                selected.some(
                  option =>
                    !_isOptionValid(
                      option,
                      measurements,
                      projectConfig,
                      sliceNo
                    )
                )
              ) {
                errors.slices[sliceNo][key] = 'invalid';
              }
            } else if (
              !_isOptionValid(selected, measurements, projectConfig, sliceNo)
            ) {
              errors.slices[sliceNo][key] = 'invalid';
              allLabels.push(selected.requireMeasurements[0]);
              selected.measurementTools.map(item => {
                allTools.push(item);
              });
            }
          }
          if (validate && Array.isArray(answers.slices[sliceNo][key])) {
            if (
              validate.minSelectedCount &&
              answers.slices[sliceNo][key].length < validate.minSelectedCount
            ) {
              errors.slices[sliceNo][key] = 'invalid';
            }
            if (
              validate.maxSelectedCount &&
              answers.slices[sliceNo][key].length > validate.maxSelectedCount
            ) {
              errors.slices[sliceNo][key] = 'invalid';
            }
          }
          if (validate && validate.custom) {
            if (!jsonLogic.apply(validate.custom, answers.slices[sliceNo])) {
              errors.slices[sliceNo][key] = 'invalid';
            }
          }
        }

        if (studyFormState) {
          const formState = cloneDeep(studyFormState);
          formState.currentSliceInfo.sliceNumber = sliceNo;
          const visibleQuestionsOnSliceNumber = questions.slices[sliceNo];
          refillDependentQuestions(
            answers.slices[sliceNo],
            notes.slices[sliceNo],
            formState,
            props,
            visibleQuestionsOnSliceNumber
          );
        }
      }
    }
  } else {
    for (const question of questions) {
      const { key, validate, values } = question;
      notes[key] = answers[key];

      let sliceNo = -1;
      if (
        projectConfig.studyFormWorkflow === 'Form' &&
        typeof answers[key] === 'object'
      ) {
        const validateDetail = _validateSubFormQuestion(
          answers,
          sliceNo,
          key,
          errors,
          projectConfig,
          measurements,
          allLabels,
          allTools,
          notes
        );
        errors = validateDetail.errors;
        allLabels = validateDetail.allLabels;
        allTools = validateDetail.allTools;
      }

      if (validate && validate.required && !notes[key]) {
        errors[key] = 'required';
      } else if (values) {
        let selected = null;
        if (Array.isArray(notes[key])) {
          selected = values.find(({ value }) => notes[key].includes(value));
        } else if (typeof notes[key] === 'object') {
          selected = values.find(({ value }) => notes[key]?.value === value);
        } else {
          selected = values.find(({ value }) => value === notes[key]);
        }
        if (!selected) {
          continue;
        }
        // deprecated, use form.io component.validate.custom
        if (selected.require) {
          for (const [requireKey, requireValue] of Object.entries(
            selected.require
          )) {
            if (answers[requireKey] !== requireValue) {
              errors[key] = 'invalid';
            }
          }
        }
        if (hasInstructionRange(selected) || selected?.instructionSet) {
          const hasCompleted = hasAnnotatedBasedOnInstructionRange(
            question,
            selected
          );
          if (!hasCompleted) {
            errors[key] = 'invalid';
          }
        } else if (Array.isArray(selected)) {
          if (
            selected.some(
              option => !_isOptionValid(option, measurements, projectConfig)
            )
          ) {
            errors[key] = 'invalid';
          }
        } else if (!_isOptionValid(selected, measurements, projectConfig)) {
          if (typeof errors[key] !== 'object') {
            errors[key] = 'invalid';
          }
          allLabels.push(selected.requireMeasurements[0]);
          selected?.measurementTools?.map(item => {
            allTools.push(item);
          });
        }
      }
      if (validate && Array.isArray(answers[key])) {
        if (
          validate.minSelectedCount &&
          answers[key].length < validate.minSelectedCount
        ) {
          errors[key] = 'invalid';
        }
        if (
          validate.maxSelectedCount &&
          answers[key].length > validate.maxSelectedCount
        ) {
          errors[key] = 'invalid';
        }
      }
      if (validate && validate.custom) {
        if (!jsonLogic.apply(validate.custom, answers)) {
          errors[key] = 'invalid';
        }
      }
    }

    if (studyFormState) {
      refillDependentQuestions(
        answers,
        notes,
        studyFormState,
        props,
        questions
      );
    }
  }
  return { errors, notes, allLabels, allTools };
};

const _isOptionValidSubForm = (
  option,
  measurementLabels,
  projectConfig,
  sliceNo,
  subFormQuestion,
  subFormName,
  subFormAnnotationId
) => {
  const state = store.getState();
  if (
    option.requireMeasurements &&
    !option.requireMeasurements.every(check => {
      const regex = new RegExp(check);
      return !isCurrentFile(state) && projectConfig.bSlices
        ? measurementLabels &&
          measurementLabels.slices &&
          measurementLabels.slices[sliceNo] &&
          measurementLabels.slices[sliceNo]?.[subFormQuestion]?.[subFormName]?.[
            subFormAnnotationId
          ]
          ? measurementLabels.slices[sliceNo]?.[subFormQuestion]?.[
              subFormName
            ]?.[subFormAnnotationId]?.some(label => regex.test(label))
          : false
        : measurementLabels[subFormQuestion]?.[subFormName]?.[
            subFormAnnotationId
          ]?.some(label => regex.test(label));
    })
  ) {
    return false;
  }
  return true;
};

const _isOptionValid = (option, measurementLabels, projectConfig, sliceNo) => {
  const state = store.getState();
  if (
    option.requireMeasurements &&
    option.directive &&
    option.requireMeasurements.some(check => {
      const regex = new RegExp(check);
      return !isCurrentFile(state) && projectConfig.bSlices
        ? measurementLabels?.slices?.[sliceNo]?.component?.some
          ? measurementLabels.slices[sliceNo]?.component?.some(label =>
              regex.test(label)
            )
          : false
        : (Array.isArray(measurementLabels)
            ? measurementLabels
            : measurementLabels.component || []
          ).some(label => regex.test(label));
    })
  ) {
    return true;
  }

  if (
    option.requireMeasurements &&
    !option.requireMeasurements.every(check => {
      const regex = new RegExp(check);
      return !isCurrentFile(state) && projectConfig.bSlices
        ? measurementLabels?.slices?.[sliceNo]?.component
          ? measurementLabels.slices[sliceNo].component?.some(label =>
              regex.test(label)
            )
          : false
        : (Array.isArray(measurementLabels)
            ? measurementLabels
            : measurementLabels.component || []
          ).some(label => regex.test(label));
    })
  ) {
    return false;
  }
  return true;
};

const completeQuestionCheck = (question, currentQuestionKey) => {
  if (question.conditional && question.conditional.json) {
    return jsonLogic.apply(question.conditional.json, currentQuestionKey);
  }
};

const draftStudyForm = (questions, studyFormState, props) => {
  const notes = {};
  const { projectConfig } = props;
  const state = store.getState();
  const answers = studyFormState.notes;
  if (!isCurrentFile(state) && projectConfig.bSlices) {
    notes.slices = {};
    const bSlices = getBSlices(projectConfig);
    for (const sliceNo of bSlices) {
      if (answers.slices?.[sliceNo]) {
        for (const { key } of questions.slices[sliceNo]) {
          if (!notes.slices[sliceNo]) {
            notes.slices[sliceNo] = {};
          }
          notes.slices[sliceNo][key] = answers.slices[sliceNo][key];
        }

        const formState = cloneDeep(studyFormState);
        formState.currentSliceInfo.sliceNumber = sliceNo;
        const visibleQuestionsOnSliceNumber = questions.slices[sliceNo];
        refillDependentQuestions(
          answers.slices[sliceNo],
          notes.slices[sliceNo],
          formState,
          props,
          visibleQuestionsOnSliceNumber
        );
      }
    }
  } else {
    for (const { key } of questions) {
      notes[key] = answers[key];
    }

    refillDependentQuestions(answers, notes, studyFormState, props, questions);
  }
  return { notes };
};

const refillDependentQuestions = (
  answers,
  notes,
  studyFormState,
  props,
  questions
) => {
  let keyDiffs = difference(
    Object.keys(answers || {}),
    Object.keys(notes || {})
  );
  if (keyDiffs.length) {
    const questionKeys = questions.map(question => question.key);

    keyDiffs?.forEach(item => {
      const independentAnswers = { ...answers };
      delete independentAnswers[item];
      const formState = { ...studyFormState, notes: independentAnswers };
      const independentQuestions = getVisibleQuestions(formState, props);
      const independentQuestionKeys = independentQuestions.map(
        question => question.key
      );
      if (!questionKeys.every(key => independentQuestionKeys.includes(key))) {
        // if item is removed from notes, it will affect some other questions
        // so adding the question back to notes.
        notes[item] = answers[item];
      }
    });
  }
};

const bSliceRequiredValidation = (projectConfig, bSlices, notes) => {
  const errorAll = [];
  const all = projectConfig.bSlices?.required?.all;
  if (all && all.length > 0) {
    all.forEach(key => {
      const question = projectConfig.studyForm?.components?.find(
        question => question.key === key
      );
      const allQuestionValues = question?.values?.map(x => x.value);
      bSlices.forEach(sliceNo => {
        const sliceNumber = parseInt(sliceNo);
        let allValue = '';
        if (typeof notes.slices?.[sliceNumber]?.[key] === 'object') {
          allValue = notes.slices?.[sliceNumber]?.[key].value;
        } else {
          allValue = notes.slices?.[sliceNumber]?.[key];
        }
        if (
          !allValue ||
          !allQuestionValues.includes(allValue) ||
          (question?.positiveAnswers &&
            !question?.positiveAnswers?.includes(allValue))
        ) {
          errorAll.push(key);
        }
      });
    });
  }

  let errorAny = [];
  const any = projectConfig.bSlices?.required?.any;
  if (any && any.length > 0) {
    any.forEach(key => {
      const question = projectConfig.studyForm?.components?.find(
        question => question.key === key
      );
      const anyQuestionValues = question?.values?.map(x => x.value);
      let isAny = false;
      bSlices.forEach(sliceNo => {
        const sliceNumber = parseInt(sliceNo);
        let anyValue = '';
        if (typeof notes.slices?.[sliceNumber]?.[key] === 'object') {
          anyValue = notes.slices?.[sliceNumber]?.[key].value;
        } else {
          anyValue = notes.slices?.[sliceNumber]?.[key];
        }
        if (anyValue && anyQuestionValues.includes(anyValue)) {
          if (
            question?.positiveAnswers &&
            question?.positiveAnswers?.includes(anyValue)
          ) {
            isAny = true;
          } else if (!question?.positiveAnswers) {
            isAny = true;
          }
        }
      });
      if (!isAny) {
        errorAny.push(key);
      }
    });
  }

  let errorOne = [];
  const one = projectConfig.bSlices?.required?.one;
  if (one && one.length > 0) {
    one.forEach(key => {
      const question = projectConfig.studyForm?.components?.find(
        question => question.key === key
      );
      const oneQuestionValues = question?.values?.map(x => x.value);
      let isOne = false;
      bSlices.forEach(sliceNo => {
        const sliceNumber = parseInt(sliceNo);
        let oneValue = '';
        if (typeof notes.slices?.[sliceNumber]?.[key] === 'object') {
          oneValue = notes.slices?.[sliceNumber]?.[key].value;
        } else {
          oneValue = notes.slices?.[sliceNumber]?.[key];
        }
        if (oneValue && oneQuestionValues.includes(oneValue)) {
          if (
            question?.positiveAnswers &&
            question?.positiveAnswers?.includes(oneValue)
          ) {
            isOne = true;
          } else if (!question?.positiveAnswers) {
            isOne = true;
          }
        }
      });
      if (!isOne) {
        errorOne.push(key);
      }
    });
  }

  let errorSpecific = {};
  const specific = projectConfig.bSlices?.required?.specific;
  bSlices.forEach(sliceNo => {
    if (specific && specific[sliceNo]) {
      const sliceSpecific = specific[sliceNo];
      if (sliceSpecific.length > 0) {
        sliceSpecific.forEach(key => {
          const question = projectConfig.studyForm?.components?.find(
            question => question.key === key
          );
          const specificQuestionValues = question?.values?.map(x => x.value);
          const sliceNumber = parseInt(sliceNo);
          let specificValue = '';
          if (typeof notes.slices?.[sliceNumber]?.[key] === 'object') {
            specificValue = notes.slices?.[sliceNumber]?.[key].value;
          } else {
            specificValue = notes.slices?.[sliceNumber]?.[key];
          }
          if (
            !specificValue ||
            !specificQuestionValues.includes(specificValue) ||
            (question?.positiveAnswers &&
              !question?.positiveAnswers?.includes(specificValue))
          ) {
            if (errorSpecific[sliceNumber] === undefined) {
              errorSpecific[sliceNumber] = [];
            }
            errorSpecific[sliceNumber].push(key);
          }
        });
      }
    }
  });

  return {
    all: errorAll,
    any: errorAny,
    one: errorOne,
    specific: errorSpecific,
  };
};

const _getQuestionLabels = (questionKeyList, projectConfig) => {
  const questionLabelList = [];
  const keyList = questionKeyList.filter(
    (item, index) => questionKeyList.indexOf(item) === index
  );
  keyList.forEach(key => {
    projectConfig.studyForm?.components?.forEach(question => {
      if (question.key === key) {
        questionLabelList.push(question.label);
      }
    });
  });
  return questionLabelList;
};

const errorSetting = (
  isbSlice,
  errors,
  sliceNo,
  visibleQuestions,
  questionRefs,
  props,
  stateNotes
) => {
  // scroll first error into view
  const firstQuestion = isbSlice
    ? getQuestionsInNotes(sliceNo, props, stateNotes).find(
        question => question.key in errors.slices[sliceNo]
      )
    : visibleQuestions.find(question => question.key in errors);

  let subFormKey;
  let annotationId;
  let subFormName;
  let subFormQuestion;

  if (isbSlice) {
    if (typeof errors.slices[sliceNo]?.[firstQuestion.key] === 'object') {
      Object.keys(errors.slices[sliceNo][firstQuestion.key])?.forEach(
        formName => {
          errors.slices[sliceNo][firstQuestion.key]?.[formName]?.forEach(
            subformQuestionNotes => {
              Object.keys(subformQuestionNotes)?.forEach(key => {
                if (key !== 'annotationId' && !subFormKey) {
                  subFormKey = key;
                  annotationId = subformQuestionNotes['annotationId'];
                  subFormName = formName;
                  subFormQuestion = props.projectConfig.studyForm?.subForms?.[
                    formName
                  ]?.components?.find(q => subFormKey === q.key);
                }
              });
            }
          );
        }
      );
    }
  } else {
    if (typeof errors[firstQuestion.key] === 'object') {
      Object.keys(errors[firstQuestion.key])?.forEach(formName => {
        errors[firstQuestion.key]?.[formName]?.forEach(subformQuestionNotes => {
          Object.keys(subformQuestionNotes)?.forEach(key => {
            if (key !== 'annotationId' && !subFormKey) {
              subFormKey = key;
              annotationId = subformQuestionNotes['annotationId'];
              subFormName = formName;
              subFormQuestion = props.projectConfig.studyForm?.subForms?.[
                formName
              ]?.components?.find(q => subFormKey === q.key);
            }
          });
        });
      });
    }
  }

  if (questionRefs[firstQuestion?.key]) {
    questionRefs[firstQuestion?.key].scrollIntoView();
  }

  const errorData = { isbSlice: isbSlice };
  let message = '';

  if (isbSlice && !subFormQuestion) {
    message =
      `Please complete required items. \n ` +
      firstQuestion?.label +
      ' / Slice - ' +
      sliceNo;
    if (stateNotes.slices[sliceNo]?.[firstQuestion?.key]) {
      const value =
        typeof stateNotes.slices[sliceNo]?.[firstQuestion?.key] === 'object'
          ? stateNotes.slices[sliceNo]?.[firstQuestion?.key].value
          : stateNotes.slices[sliceNo]?.[firstQuestion?.key];
      const question = [...props.projectConfig.studyForm?.components];
      const answer = question
        .find(item => item.key === firstQuestion?.key)
        .values?.find(data => data.value === value);
      if (answer.requireMeasurements.length) {
        let requireMultipleMeasurements = true;
        if (answer.hasOwnProperty('requiremultipleMeasurements')) {
          requireMultipleMeasurements = answer?.requiremultipleMeasurements;
        }
        let requireMeasurements = answer.requireMeasurements;
        if (!requireMultipleMeasurements) {
          requireMeasurements = answer.requireMeasurements.filter(x =>
            props.measurementLabels.slices?.[sliceNo]?.component.includes(x)
          );
        }
        requireMeasurements.forEach(label => {
          if (
            !props.measurementLabels.slices?.[sliceNo]?.component?.includes(
              label
            )
          ) {
            message += ` \n  / Label - ` + label;
          }
        });
      }
    }
    errorData.message = message;
  } else if (isbSlice && subFormQuestion) {
    message =
      `Please complete required items. \n  study form question - ` +
      firstQuestion?.label +
      ` \n subform question - ` +
      `` +
      subFormQuestion?.label +
      ' / Slice - ' +
      sliceNo;

    if (stateNotes.slices[sliceNo]?.[firstQuestion?.key]?.[subFormName]) {
      const annotationData = stateNotes.slices[sliceNo]?.[firstQuestion?.key]?.[
        subFormName
      ]?.find(item => item.annotationId === annotationId);
      const value = annotationData[subFormQuestion?.key];
      const question = [
        ...props.projectConfig.studyForm?.subForms?.[subFormName]?.components,
      ];
      const answer = question
        .find(item => item.key === subFormQuestion?.key)
        .values?.find(data => data.value === value);
      if (answer.requireMeasurements.length) {
        answer.requireMeasurements.forEach(label => {
          if (
            !props.measurementLabels.slices?.[sliceNo]?.[firstQuestion?.key]?.[
              subFormName
            ]?.[annotationId]?.includes(label)
          ) {
            message += ` \n  / Label - ` + label;
          }
        });
      }
    }

    errorData.message = message;
  } else if (!isbSlice && subFormQuestion) {
    message =
      `Please complete required items. \n  study form question - ` +
      firstQuestion?.label +
      ` \n subform question - ` +
      `` +
      subFormQuestion?.label;

    if (stateNotes?.[firstQuestion?.key]?.[subFormName]) {
      const annotationData = stateNotes?.[firstQuestion?.key]?.[
        subFormName
      ]?.find(item => item.annotationId === annotationId);
      const value = annotationData[subFormQuestion?.key];
      const question = [
        ...props.projectConfig.studyForm?.subForms?.[subFormName]?.components,
      ];
      const answer = question
        .find(item => item.key === subFormQuestion?.key)
        .values?.find(data => data.value === value);
      if (answer.requireMeasurements.length) {
        answer.requireMeasurements.forEach(label => {
          if (
            !props.measurementLabels?.[firstQuestion?.key]?.[subFormName]?.[
              annotationId
            ]?.includes(label)
          ) {
            message += ` \n  / Label - ` + label;
          }
        });
      }
    }

    errorData.message = message;
  } else if (!isbSlice && !subFormQuestion) {
    message = `Please complete required items. \n ` + firstQuestion?.label;

    if (stateNotes?.[firstQuestion?.key]) {
      const value =
        typeof stateNotes?.[firstQuestion?.key] === 'object'
          ? stateNotes?.[firstQuestion?.key].value
          : stateNotes?.[firstQuestion?.key];
      const question = [...props.projectConfig.studyForm?.components];
      const answer = question
        .find(item => item.key === firstQuestion?.key)
        .values?.find(data => data.value === value);
      if (answer.requireMeasurements.length) {
        answer.requireMeasurements.forEach(label => {
          if (!props.measurementLabels?.component?.includes(label)) {
            message += ` \n  / Label - ` + label;
          }
        });
      }
    }

    errorData.message = message;
  }
  return errorData;
};

const getBSlices = projectConfig => {
  const bSlices =
    projectConfig?.bSlices?.settings &&
    Object.keys(projectConfig.bSlices?.settings);
  return bSlices;
};

/**
 * Get question in form using question key
 * @param {string} questionKey
 * @param {object} projectConfig
 * @returns {Object} Question corresponding to given key
 */
const _getQuestion = (questionKey, projectConfig) => {
  return projectConfig.studyForm?.components?.find(q => q.key === questionKey);
};

const getQuestionsInNotes = (sliceNo, props, stateNotes) => {
  const notes = _getSliceNotes(sliceNo, props, stateNotes) || {};
  const visibleQuestionKeys = Object.keys(notes);
  const { projectConfig } = props;
  return projectConfig.studyForm?.components?.filter(q =>
    visibleQuestionKeys.includes(q.key)
  );
};

/**
 * Get notes for given slice
 * @param {number} sliceNo
 * @returns {Object} Notes
 */
const _getSliceNotes = (sliceNo, props, stateNotes) => {
  const { projectConfig } = props;
  const state = store.getState();
  const bSlices = getBSlices(projectConfig);
  return !isCurrentFile(state) && bSlices?.length && stateNotes.slices
    ? { ...stateNotes.slices[sliceNo] }
    : { ...stateNotes };
};

/**
 *
 * @param {*} taskId
 * @param {*} currentStatus
 * @param {*} previousStatus
 * @param {*} props
 */
const sendTaskSignals = (
  taskId,
  currentStatus,
  previousStatus,
  props,
  readStatus
) => {
  const { RemoteService: remoteService } = props.servicesManager.services;
  const task = { _id: taskId };
  if (previousStatus && previousStatus !== currentStatus) {
    remoteService.signalTaskStatusChanged(task, previousStatus, currentStatus);
    if (currentStatus === readStatus.Complete) {
      remoteService.signalTaskClosed(task);
    }
  } else {
    remoteService.signalTaskSaved(task);
  }
};

const isFormViewOnly = studyFormState => {
  const state = store.getState();
  const user = selectUser(state);
  const {
    readerTask,
    hasEditFormOthersPermission,
    ownFormResponsesPermission,
    readOnly,
  } = studyFormState;
  if (readerTask) {
    if (
      (readerTask.assignee !== user._id && !hasEditFormOthersPermission) ||
      (readerTask.assignee === user._id && !ownFormResponsesPermission)
    ) {
      return true;
    }
  } else {
    return readOnly;
  }
  return false;
};

const isAnnotationsViewOnly = studyFormState => {
  const state = store.getState();
  const user = selectUser(state);
  const {
    readerTask,
    hasEditAnnotationsOthersPermission,
    ownAnnotationPermission,
    readOnly,
  } = studyFormState;
  if (readerTask) {
    if (
      (readerTask.assignee !== user._id &&
        !hasEditAnnotationsOthersPermission) ||
      (readerTask.assignee === user._id && !ownAnnotationPermission)
    ) {
      return true;
    }
  } else {
    return readOnly;
  }
  return false;
};

const getCurrentMeasurements = currentSlice => {
  const measurements = measurementApi.Instance.getAllMeasurements();
  if (currentSlice) {
    const currentSliceMeasurements = [];
    (measurements || []).forEach(measurement => {
      if (
        getImageIdForImagePath(measurement.imagePath) === currentSlice.imageId
      ) {
        currentSliceMeasurements.push(measurement);
      }
    });
    return currentSliceMeasurements;
  }
  return measurements;
};

const flattenMeasurements = measurements => {
  // idempotent for a previously-flattened array
  return Object.values(measurements).reduce(
    (flatMeasures, measureList) => flatMeasures.concat(measureList),
    []
  );
};

const getMeasurementLabels = (measurements, projectConfig, slice) => {
  const measurementList = flattenMeasurements(measurements);
  const state = store.getState();
  if (!isCurrentFile(state) && projectConfig.bSlices) {
    let list = {};
    measurementList.map(x => {
      if (x.location) {
        if (!list.slices) {
          list.slices = {};
        }
        if (x.sliceNumber) {
          if (!list.slices[x.sliceNumber]) {
            list.slices[x.sliceNumber] = {};
          }
          if (!list.slices[x.sliceNumber].component) {
            list.slices[x.sliceNumber].component = [];
          }
          if (x.isSubForm) {
            if (!list.slices[x.sliceNumber]?.[x.question]) {
              list.slices[x.sliceNumber][x.question] = {};
            }
            if (!list.slices[x.sliceNumber]?.[x.question]?.[x.subFormName]) {
              list.slices[x.sliceNumber][x.question][x.subFormName] = {};
            }
            if (
              !list.slices[x.sliceNumber]?.[x.question]?.[x.subFormName]?.[
                x.subFormAnnotationId
              ]
            ) {
              list.slices[x.sliceNumber][x.question][x.subFormName][
                x.subFormAnnotationId
              ] = [];
            }
            list.slices[x.sliceNumber]?.[x.question]?.[x.subFormName]?.[
              x.subFormAnnotationId
            ].push(x.location);
          } else {
            list.slices[x.sliceNumber].component.push(x.location);
          }
        } else {
          if (!list.slices[slice]) {
            list.slices[slice] = {};
          }
          if (!list.slices[slice].component) {
            list.slices[slice].component = [];
          }
          if (x.isSubForm) {
            if (!list.slices[slice]?.[x.question]) {
              list.slices[slice][x.question] = {};
            }
            if (!list.slices[slice]?.[x.question]?.[x.subFormName]) {
              list.slices[slice][x.question][x.subFormName] = {};
            }
            if (
              !list.slices[slice]?.[x.question]?.[x.subFormName]?.[
                x.subFormAnnotationId
              ]
            ) {
              list.slices[slice][x.question][x.subFormName][
                x.subFormAnnotationId
              ] = [];
            }
            list.slices[slice]?.[x.question]?.[x.subFormName]?.[
              x.subFormAnnotationId
            ].push(x.location);
          } else {
            list.slices[slice].component.push(x.location);
          }
        }
      }
    });
    return list;
  } else {
    let labelList = {};
    measurementList
      .filter(measure => !!measure.location) // filter incomplete measurement
      .map(measure => {
        if (measure.isSubForm) {
          if (!labelList[measure.question]) {
            labelList[measure.question] = {};
          }
          if (!labelList[measure.question]?.[measure.subFormName]) {
            labelList[measure.question][measure.subFormName] = {};
          }
          if (
            !labelList[measure.question]?.[measure.subFormName]?.[
              measure.subFormAnnotationId
            ]
          ) {
            labelList[measure.question][measure.subFormName][
              measure.subFormAnnotationId
            ] = [];
          }
          labelList[measure.question]?.[measure.subFormName]?.[
            measure.subFormAnnotationId
          ].push(measure.location);
        } else {
          if (!labelList.component) {
            labelList.component = [];
          }
          labelList.component.push(measure.location);
        }
      });
    return labelList;
  }
};

const isOptionAvailable = (
  option,
  measurementLabels,
  projectConfig,
  sliceNumber,
  isSubForm,
  studyFormState
) => {
  const slice = sliceNumber;
  const state = store.getState();
  const { subFormQuestion, subFormName, subFormAnnotationId } = studyFormState;
  if (
    isSubForm &&
    option.excludeMeasurements &&
    option.excludeMeasurements.some(check => {
      const regex = new RegExp(check);
      return !isCurrentFile(state) && projectConfig.bSlices
        ? measurementLabels.slices[slice]?.[subFormQuestion]?.[subFormName]?.[
            subFormAnnotationId
          ]?.some(label => regex.test(label))
        : measurementLabels?.[subFormQuestion]?.[subFormName]?.[
            subFormAnnotationId
          ]?.some(label => regex.test(label));
    })
  ) {
    return false;
  } else if (
    option.excludeMeasurements &&
    option.excludeMeasurements.some(check => {
      const regex = new RegExp(check);
      return !isCurrentFile(state) && projectConfig.bSlices
        ? measurementLabels.slices[slice]?.component.some(label =>
            regex.test(label)
          )
        : measurementLabels?.component.some(label => regex.test(label));
    })
  ) {
    return false;
  }
  return true;
};

const bSliceQuestionValidation = (requiredError, projectConfig) => {
  let msg = '';
  if (
    requiredError &&
    (requiredError.all.length > 0 ||
      requiredError.any.length > 0 ||
      requiredError.one.length > 0 ||
      requiredError.specific.length > 0)
  ) {
    if (requiredError.all.length > 0) {
      const questionAllLabels = _getQuestionLabels(
        requiredError.all,
        projectConfig
      );
      msg = `Please answer the below question for all bSlices. \n `;
      questionAllLabels.length > 0 &&
        questionAllLabels.forEach(label => {
          msg += label + ` \n `;
        });
      if (questionAllLabels.length === 0) {
        msg += 'Question not found for key "' + requiredError.all[0] + '"';
      }
    } else if (requiredError.any.length > 0) {
      const questionAnyLabels = _getQuestionLabels(
        requiredError.any,
        projectConfig
      );
      msg = 'Please answer the below question for atleast one bSlice. \n ';
      questionAnyLabels.length > 0 &&
        questionAnyLabels.forEach(label => {
          msg += label + ' \n ';
        });
      if (questionAnyLabels.length === 0) {
        msg += 'Question not found for key "' + requiredError.any[0] + '"';
      }
    } else if (requiredError.one.length > 0) {
      const questionOneLabels = _getQuestionLabels(
        requiredError.one,
        projectConfig
      );
      msg = 'Please answer the below question for exactly one bSlice. \n ';
      questionOneLabels.length > 0 &&
        questionOneLabels.forEach(label => {
          msg += label + ' \n ';
        });
      if (questionOneLabels.length === 0) {
        msg += 'Question not found for key "' + requiredError.one[0] + '"';
      }
    } else if (requiredError.specific.length > 0) {
      const questionSpecificLabels = _getQuestionLabelsForSpecific(
        requiredError.specific,
        projectConfig
      );
      msg = 'Please answer the below questions for following bSlices. \n ';
      Object.keys(questionSpecificLabels).length > 0 &&
        Object.keys(questionSpecificLabels).forEach(slice => {
          questionSpecificLabels[slice].forEach(label => {
            msg += label.replace('?', '') + ' on image #' + slice + '? \n ';
          });
        });
      if (Object.keys(questionSpecificLabels).length === 0) {
        msg +=
          'Question not found for key "' +
          requiredError.specific[Object.keys(questionSpecificLabels)[0]][0] +
          '"';
      }
    }
  }
  return msg;
};

const getVisibleQuestions = (studyFormState, props) => {
  const state = store.getState();
  const sliceNo = studyFormState.currentSliceInfo?.sliceNumber || 1;
  const notesForSlice = _getSliceNotes(sliceNo, props, studyFormState.notes);
  if (!isEmpty(get(props.projectConfig, 'studyForm.components'))) {
    let newComponents = [];
    props.projectConfig?.studyForm?.components?.forEach(question => {
      newComponents.push(question);
      if (
        (props.projectConfig?.studyFormWorkflow === 'Form' ||
          props.projectConfig?.studyFormWorkflow === 'ROI' ||
          props.projectConfig?.studyFormWorkflow === 'Mixed') &&
        question?.values?.length > 0
      ) {
        for (let i = 0; i < question?.values?.length; i++) {
          if (question?.values[i]?.subForm) {
            let subForm = {};
            subForm.components =
              props.projectConfig?.studyForm?.subForms?.[
                question?.values[i]?.subForm
              ]?.components;
            subForm.label =
              props.projectConfig?.studyForm?.subForms?.[
                question?.values[i]?.subForm
              ]?.label;
            subForm.key = question.key;
            subForm.positiveAnswers = question.positiveAnswers;
            subForm.subFormName = question?.values[i]?.subForm;
            newComponents.push(subForm);
            break;
          }
        }
      }
    });
    // List of hidden questions that would have been visible with available answers in notes, if they were not hidden
    // Other hidden questions below the unanswered non-hidden questions need not be considered as processable
    const allProcessableHiddenQns = [];
    previousQuestion = null;
    return newComponents.filter(question => {
      if (
        props.projectConfig?.bSlices?.settings?.[sliceNo] &&
        question.components?.length > 0 &&
        (props.projectConfig?.studyFormWorkflow === 'Form' ||
          props.projectConfig?.studyFormWorkflow === 'ROI' ||
          props.projectConfig?.studyFormWorkflow === 'Mixed')
      ) {
        if (question[question.key] !== undefined) {
          question[question.key][sliceNo] = [];
        }
        studyFormState.annotationIdForQuestionKey[sliceNo]?.[
          question.key
        ]?.forEach(uuid => {
          if (question[question.key] === undefined) {
            question[question.key] = {};
          }
          if (question[question.key][sliceNo] === undefined) {
            question[question.key][sliceNo] = [];
          }
          if (
            !question[question.key][sliceNo].some(
              annotation => annotation.annotationId === uuid
            )
          ) {
            const tabData = {
              annotationId: uuid,
            };
            question[question.key][sliceNo].push(tabData);
          }
        });
        return true;
      } else if (
        !props.projectConfig?.bSlices &&
        question.components?.length > 0 &&
        (props.projectConfig?.studyFormWorkflow === 'Form' ||
          props.projectConfig?.studyFormWorkflow === 'ROI' ||
          props.projectConfig?.studyFormWorkflow === 'Mixed')
      ) {
        if (question[question.key] !== undefined) {
          question[question.key] = [];
        }
        studyFormState.annotationIdForQuestionKey?.[question.key]?.forEach(
          uuid => {
            if (question[question.key] === undefined) {
              question[question.key] = [];
            }
            if (
              !question[question.key].some(
                annotation => annotation.annotationId === uuid
              )
            ) {
              const tabData = {
                annotationId: uuid,
              };
              question[question.key].push(tabData);
            }
          }
        );
        return true;
      } else {
        return _checkQuestionVisibility(
          question,
          sliceNo,
          notesForSlice,
          allProcessableHiddenQns,
          props,
          studyFormState.notes,
          props.previousVisibleQuestion
        );
      }
    });
  } else if (props.projectConfig?.questions) {
    return props.projectConfig.questions
      .filter(question => {
        if (question.when) {
          for (const [key, value] of Object.entries(question.when)) {
            if (notesForSlice[key] === value) {
              return true;
            }
          }
          notesForSlice[question.key] = null;
          return false;
        }
        return true;
      })
      .map(question => {
        // convert old schema to the formio schema
        let formatted = pick(question, ['key', 'required', 'type']);
        formatted.label = question.prompt;
        formatted.values = question.options;
        formatted.contourVisibility = question?.contourVisibility;
        formatted.validate = {
          required: question.required,
        };
        return formatted;
      });
  }
  return [];
};

const _getQuestionLabelsForSpecific = (questionKeyList, projectConfig) => {
  const questionLabelList = {};
  Object.keys(questionKeyList).forEach(slice => {
    questionKeyList[slice].forEach(key => {
      projectConfig.studyForm?.components?.forEach(question => {
        if (question.key === key) {
          if (questionLabelList[slice] === undefined) {
            questionLabelList[slice] = [];
          }
          questionLabelList[slice].push(question.label);
        }
      });
    });
  });
  return questionLabelList;
};

const isMeasurementTools = item => {
  if (item?.values) {
    return item.values.some(data => data?.measurementTools?.length > 0);
  }
};

const _validateSubFormQuestion = (
  answers,
  sliceNo,
  key,
  errors,
  projectConfig,
  measurements,
  allLabels,
  allTools,
  notes
) => {
  const answerValue = answers[key].value;
  const currentKeyQuestion = projectConfig.studyForm?.components?.find(
    qs => qs.key === key
  );
  const currentQuestionSubFormName = currentKeyQuestion.values.find(
    val => val.value === answerValue
  )?.subForm;

  const allUuid = [];
  const allMeasurements = store.getState().timepointManager?.measurements;
  Object.keys(allMeasurements).forEach(key => {
    allMeasurements[key].length &&
      allMeasurements[key].forEach(annotation => {
        allUuid.push(annotation.uuid);
      });
  });

  currentQuestionSubFormName &&
    projectConfig.studyForm?.subForms?.[
      currentQuestionSubFormName
    ]?.components?.forEach(subFormQuestion => {
      const annotationKey = subFormQuestion.key;
      const annotationValue = subFormQuestion.values;
      const annotationValidate = subFormQuestion.validate;
      notes[key][currentQuestionSubFormName].forEach(annotationItem => {
        Object.keys(annotationItem).forEach(subFormAnnotationKey => {
          if (
            annotationItem[annotationKey] &&
            allUuid.includes(annotationItem.annotationId)
          ) {
            if (
              annotationValidate &&
              annotationValidate.required &&
              !annotationItem[subFormAnnotationKey]
            ) {
              let obj = {};
              if (errors[key][currentQuestionSubFormName]) {
                obj = errors[key][currentQuestionSubFormName].find(item => {
                  item.annotationId === annotationItem.annotationId;
                });
                if (!obj) {
                  obj = {
                    annotationId: annotationItem.annotationId,
                    [subFormAnnotationKey]: 'required',
                  };
                  errors[key][currentQuestionSubFormName].push(obj);
                } else {
                  errors[key][currentQuestionSubFormName].find(item => {
                    item.annotationId === annotationItem.annotationId;
                  })[subFormAnnotationKey] = 'required';
                }
              }
              if (!errors[key][currentQuestionSubFormName]) {
                errors[key][currentQuestionSubFormName] = [];
                obj = {
                  annotationId: annotationItem.annotationId,
                  [subFormAnnotationKey]: 'required',
                };
                errors[key][currentQuestionSubFormName].push(obj);
              }
            } else if (annotationValue) {
              let selected = null;
              if (Array.isArray(annotationItem[subFormAnnotationKey])) {
                selected = annotationValue.find(({ value }) =>
                  annotationItem[subFormAnnotationKey].includes(value)
                );
              } else if (
                typeof annotationItem[subFormAnnotationKey] === 'object'
              ) {
                selected = annotationValue.find(
                  ({ value }) =>
                    annotationItem[subFormAnnotationKey]?.value === value
                );
              } else {
                selected = annotationValue.find(
                  ({ value }) => value === annotationItem[subFormAnnotationKey]
                );
              }
              if (!selected) {
                return;
              }
              // deprecated, use form.io component.validate.custom
              if (selected.require) {
                for (const [requireKey, requireValue] of Object.entries(
                  selected.require
                )) {
                  if (answers[requireKey] !== requireValue) {
                    errors[key] = 'invalid';
                  }
                }
              }
              let isAnnotationExist = true;
              const state = store.getState();
              if (isCurrentFile(state)) {
                isAnnotationExist = measurements[key]?.[
                  currentQuestionSubFormName
                ]
                  ? measurements[key]?.[currentQuestionSubFormName]?.[
                      annotationItem.annotationId
                    ]
                  : true;
              }
              if (isAnnotationExist) {
                if (Array.isArray(selected)) {
                  if (
                    selected.some(
                      option =>
                        !_isOptionValidSubForm(
                          option,
                          measurements,
                          projectConfig,
                          sliceNo,
                          key,
                          currentQuestionSubFormName,
                          annotationItem.annotationId
                        )
                    )
                  ) {
                    if (!errors[key]) {
                      errors[key] = {};
                    }
                    if (!errors[key][currentQuestionSubFormName]) {
                      errors[key][currentQuestionSubFormName] = [];
                    }
                    if (
                      !errors[key][currentQuestionSubFormName].find(item => {
                        item.annotationId === annotationItem.annotationId;
                      })
                    ) {
                      let obj = {
                        annotationId: annotationItem.annotationId,
                        [subFormAnnotationKey]: 'invalid',
                      };
                      errors[key][currentQuestionSubFormName].push(obj);
                    } else {
                      errors[key][currentQuestionSubFormName].find(item => {
                        item.annotationId === annotationItem.annotationId;
                      })[subFormAnnotationKey] = 'invalid';
                    }
                  }
                } else if (
                  !_isOptionValidSubForm(
                    selected,
                    measurements,
                    projectConfig,
                    sliceNo,
                    key,
                    currentQuestionSubFormName,
                    annotationItem.annotationId
                  )
                ) {
                  if (!errors[key]) {
                    errors[key] = {};
                  }
                  if (!errors[key][currentQuestionSubFormName]) {
                    errors[key][currentQuestionSubFormName] = [];
                  }
                  if (
                    !errors[key][currentQuestionSubFormName].find(item => {
                      item.annotationId === annotationItem.annotationId;
                    })
                  ) {
                    let obj = {
                      annotationId: annotationItem.annotationId,
                      [subFormAnnotationKey]: 'invalid',
                    };
                    errors[key][currentQuestionSubFormName].push(obj);
                  } else {
                    errors[key][currentQuestionSubFormName].find(item => {
                      item.annotationId === annotationItem.annotationId;
                    })[subFormAnnotationKey] = 'invalid';
                  }
                  allLabels.push(selected.requireMeasurements[0]);
                  selected.measurementTools.map(item => {
                    allTools.push(item);
                  });
                }
              }
            }
          }
        });
      });
    });
  return { errors, allLabels, allTools };
};

const hasAnnotatedBasedOnInstructionRange = (
  question,
  selectedOption,
  slice = null,
  bSliceSettings = null,
  subFormAnnotationId = ''
) => {
  const instructionSet = selectedOption?.instructionSet || [selectedOption];
  const hasCompleted = instructionSet.every(instruction => {
    let requireMultipleMeasurements = true;
    if (instruction.hasOwnProperty('requiremultipleMeasurements')) {
      requireMultipleMeasurements = instruction.requiremultipleMeasurements;
    }
    if (instruction?.min === 0 && !instruction?.exact) {
      return true;
    }
    const fn = requireMultipleMeasurements ? 'every' : 'some';
    return (instruction?.requireMeasurements || [])[fn](x => {
      const isLimitReached = hasLabelsLimitReached(
        [x],
        question,
        selectedOption,
        slice,
        bSliceSettings,
        subFormAnnotationId
      );
      if (instruction?.min && !instruction?.exact) {
        const { min, max = null } = instruction;
        const count = getLabelCount(
          x,
          question,
          selectedOption,
          slice,
          bSliceSettings,
          subFormAnnotationId
        );
        if (min && max) {
          return min <= count && max >= count;
        }
        const limit = getLabelLimits(x, selectedOption);
        return min <= count && limit >= count;
      }
      return isLimitReached;
    });
  });
  return hasCompleted;
};

export {
  validateWhileQuestionSwitches,
  validateForm,
  completeQuestionCheck,
  draftStudyForm,
  bSliceRequiredValidation,
  errorSetting,
  getQuestionsInNotes,
  getBSlices,
  sendTaskSignals,
  isFormViewOnly,
  isAnnotationsViewOnly,
  getMeasurementLabels,
  getCurrentMeasurements,
  isOptionAvailable,
  bSliceQuestionValidation,
  getVisibleQuestions,
  isMeasurementTools,
};
