import React, { Component } from 'react';
import cloneDeep from 'lodash.clonedeep';
import PropTypes from 'prop-types';

import { SimpleDialog } from '@ohif/ui';
import store from '@ohif/viewer/src/store';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import bounding from '@ohif/viewer/src/lib/utils/bounding.js';
import { getDialogStyle } from '@ohif/viewer/src/components/Labelling/labellingPositionUtils.js';
import OHIFLabellingData from '@ohif/viewer/src/components/Labelling/OHIFLabellingData';
import { Select } from '@ohif/ui';

import './EditLabelDescriptionDialog.css';

const { selectedQuestion } = FlywheelCommonRedux.selectors;

const DEFAULT_LABEL = '--- Select Label ---';
const DEFAULT_COLOR = 'rgba(255, 255, 0, 0.2)';
export default class EditLabelDescriptionDialog extends Component {
  static defaultProps = {
    componentRef: React.createRef(),
    componentStyle: {},
  };

  static propTypes = {
    measurementData: PropTypes.object.isRequired,
    onCancel: PropTypes.func.isRequired,
    componentRef: PropTypes.object,
    componentStyle: PropTypes.object,
    onUpdate: PropTypes.func.isRequired,
    updateLabelling: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    const label = props.measurementData.location || '';
    this.state = {
      description: props.measurementData.description || '',
      color: props.measurementData.color || DEFAULT_COLOR,
      label: this.getLabel(label),
      currentItems: this.loadLabels(props),
    };

    this.mainElement = React.createRef();
  }

  componentDidMount = () => {
    bounding(this.mainElement);
  };

  componentDidUpdate(prevProps) {
    const label = this.props?.measurementData?.location || '';
    const previousLabel = prevProps?.measurementData?.location || '';
    const description =
      this.props?.measurementData?.description || this.props?.description || '';
    if (label !== previousLabel) {
      this.setState({
        description: description,
        currentItems: this.loadLabels(),
        label: this.getLabel(label),
      });
    }
  }

  getLabel(label = this.state.label, hasValue = false) {
    const labels = this.loadLabels();
    const labelObj = labels.find(x => x.item === label || x.label === label);
    if (labelObj) {
      if (hasValue) {
        return labelObj.item;
      }
      return labelObj.label;
    }
    return '';
  }

  loadLabels(props = this.props) {
    let initialItems = OHIFLabellingData;
    const projectConfig = props.projectConfig;
    if (projectConfig?.labels) {
      initialItems = projectConfig.labels;
    }

    const location = props.measurementData.location;
    if (props.availableLabels && props.tableType === 'FORM_ANNOTATION_TABLE') {
      if (props.measurementPanelData) {
        const labels = initialItems.filter(x =>
          props.selectedLabels.includes(x.value)
        );
        const state = store.getState();
        let measurements = [];
        const measures = state.timepointManager.measurements;
        const toolTypes = Object.keys(measures);
        toolTypes.forEach(toolType => {
          measures[toolType].forEach(item => {
            if (
              props.selectedLabels.includes(item.location) &&
              item.imagePath === props.measurementData.imagePath
            ) {
              measurements.push(item);
            }
          });
        });
        initialItems = labels.filter(
          x =>
            !measurements.some(item => item.location === x.value) ||
            x.value === location
        );
      } else {
        initialItems = initialItems.filter(
          ({ value }) =>
            props.availableLabels.includes(value) || value === location
        );
        if (!initialItems.some(x => x.limit)) {
          initialItems = projectConfig?.labels;
        }
      }
    } else if (props.tableType === 'ANNOTATION_TABLE') {
      if (projectConfig?.studyForm) {
        initialItems = initialItems.filter(x => {
          const measurementList = this.getLabelCount(x.value);
          if (
            (x.limit &&
              (x.value === location || measurementList.length !== x.limit)) ||
            !x.limit
          ) {
            return x;
          }
        });
      }
    }
    if (
      location &&
      !initialItems.find(x => x.label === location || x.value === location)
    ) {
      initialItems.push({
        key: location,
        item: location,
        label: location,
        value: location,
      });
    }
    initialItems = initialItems.reduce((accumulator, labelObj) => {
      accumulator.push({
        ...labelObj,
        key: labelObj.label,
        item: labelObj.value,
        value: labelObj.label,
      });
      return accumulator;
    }, []);
    initialItems = cloneDeep(initialItems);
    initialItems.unshift({
      key: DEFAULT_LABEL,
      label: DEFAULT_LABEL,
      value: DEFAULT_LABEL,
    });
    return initialItems;
  }

  getLabelCount(location) {
    const state = store.getState();
    const measurements = state.timepointManager.measurements;
    const measureList = [];
    Object.keys(measurements).map(toolType => {
      measurements[toolType].map(x => {
        if (x.location === location) {
          measureList.push(x);
        }
      });
    });
    return measureList;
  }

  render() {
    const style = getDialogStyle(this.props.componentStyle);
    const value = this.state.label || '';
    const label = this.getLabel(value);
    return (
      <SimpleDialog
        headerTitle="Edit Label"
        onClose={this.props.onCancel}
        onConfirm={this.onConfirm}
        rootClass="editLabelDescriptionDialog inner-header inner-content"
        componentRef={this.mainElement}
        componentStyle={style}
        showFooterButtons={false}
      >
        <div>
          <div className="inner-container">
            <div className="fs-12 mb-5">Label</div>
            <div>
              <Select
                style={{
                  width: '100%',
                  margin: '0px 0px 15px 0px',
                  backgroundColor: '#1c232e',
                  boxShadow: 'none',
                }}
                value={label}
                onChange={this.onChange}
                options={this.state.currentItems}
              />
            </div>
            <div className="fs-12">Description</div>
            <div>
              <textarea
                value={this.state.description}
                className="text-area-style"
                id="description"
                autoComplete="off"
                autoFocus
                onChange={this.handleChange}
              />
            </div>
          </div>

          <div className="footer-container">
            <div className="cancel-button" onClick={this.props.onCancel}>
              Cancel
            </div>
            <div className="save-button" onClick={this.onConfirm}>
              Save Changes
            </div>
          </div>
        </div>
      </SimpleDialog>
    );
  }

  onConfirm = e => {
    e.preventDefault();
    this.props.onUpdate(this.state.description);
    const label = this.getLabel(this.state.label, true);
    const labelDetails = this.loadLabels()?.find(
      x => x.item === label || x.label === label
    );
    const color = labelDetails?.color || this.state.color;
    const question = selectedQuestion(store.getState());
    let questionKey,
      answer = '';
    if (question?.questionKey && question?.answer) {
      questionKey =
        this.props.measurementData?.questionKey || question.questionKey;
      answer = this.props.measurementData?.answer || question.answer;
    }

    if (question.isSubForm) {
      this.props.updateLabelling({
        location: label,
        description: this.state.description,
        color,
        questionKey: questionKey,
        answer: answer,
        isSubForm: question.isSubForm,
        subFormName: question.subFormName,
        subFormAnnotationId: question.subFormAnnotationId,
        question: question.question,
      });
    } else {
      this.props.updateLabelling({
        location: label,
        description: this.state.description,
        color,
        questionKey: questionKey,
        answer: answer,
      });
    }
  };

  handleChange = event => {
    this.setState({ description: event.target.value });
  };

  onChange = event => {
    this.setState({ label: event.target.value });
  };
}
