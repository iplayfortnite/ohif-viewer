import React, { useEffect, useState } from 'react';

let stopTime = true;
let timerFunction = 0;

const TimerTool = ({ projectConfig, showStudyForm, adjustedReadStartTime }) => {
  const [timeData, setTimeData] = useState({
    hr: 0,
    min: 0,
    sec: 0,
  });

  useEffect(() => {
    timerFunction = setTimeout(() => timerCycle(adjustedReadStartTime), 1000);
  }, [timeData]);

  useEffect(() => {
    return () => {
      clearTimeout(timerFunction);
      stopTime = true;
    };
  }, []);

  const timerCycle = readStartTime => {
    if (!stopTime) {
      const currentReadTime = Date.now() - readStartTime;
      const timeSpan = new Date(currentReadTime);
      const obj = {
        hr: timeSpan.getUTCHours(),
        min: timeSpan.getUTCMinutes(),
        sec: timeSpan.getUTCSeconds(),
      };
      if (obj !== timeData) {
        setTimeData(obj);
      }
    }
  };

  useEffect(() => {
    if (projectConfig && projectConfig.timerOn && showStudyForm) {
      if (stopTime) {
        stopTime = false;
        if (timerFunction) {
          clearTimeout(timerFunction);
        }
        timerCycle(adjustedReadStartTime);
      }
    }
  }, [showStudyForm, projectConfig]);

  const zeroPad = (num, count = 2) => {
    let val = num.toString();
    let pad = count - val.length;
    while (pad-- > 0) {
      val = '0' + val;
    }
    return val;
  };

  const { hr, min, sec } = timeData;
  return projectConfig && projectConfig.timerVisible && showStudyForm ? (
    <div className="timer-container">
      <div className="timer-title">Timer:</div>
      <div className="timer-value">
        <span className="material-icons" style={{ marginRight: 3 }}>
          timer
        </span>
        {zeroPad(hr) + ':' + zeroPad(min) + ':' + zeroPad(sec)}
      </div>{' '}
    </div>
  ) : null;
};

export default TimerTool;
