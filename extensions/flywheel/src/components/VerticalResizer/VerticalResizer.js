import React, { useCallback, useState, useEffect, useRef } from 'react';
import './VerticalResizer.css';

const handleHeight = 10; // handle resizer height
let resizedHeight = 0;

function VerticalResizer(props) {
  const {
    containerTop,
    containerBottom,
    containerTopMinHeight,
    containerBottomMinHeight,
    onResize,
    formHeight,
  } = props;
  const sidebarRef = useRef(null);
  const contentRef = useRef(null);
  const appContentRef = useRef(null);
  const [isResizing, setIsResizing] = useState(false);
  const [sidebarHeight, setSidebarHeight] = useState(null);

  useEffect(() => {
    if (formHeight) {
      setSidebarHeight(formHeight);
    }
  }, [formHeight]);

  useEffect(() => {
    return () => {
      if (resizedHeight) {
        onResize(resizedHeight);
      }
    };
  }, []);

  const startResizing = useCallback(mouseDownEvent => {
    setIsResizing(true);
  }, []);

  const stopResizing = useCallback(() => {
    setIsResizing(false);
    if (resizedHeight) {
      onResize(resizedHeight);
    }
  }, []);

  const resize = useCallback(
    mouseMoveEvent => {
      if (isResizing) {
        const total =
          contentRef.current.clientHeight + appContentRef.current.clientHeight;
        let height =
          mouseMoveEvent.clientY -
          sidebarRef.current.getBoundingClientRect().top;
        height = (height / total) * 100;
        if (height < 100 && 0 < height) {
          setSidebarHeight(height);
          resizedHeight = height;
        }
      }
    },
    [isResizing]
  );

  const restrictHeight = useCallback(() => {
    if (
      contentRef?.current?.clientHeight &&
      appContentRef?.current?.clientHeight
    ) {
      const total =
        contentRef.current.clientHeight + appContentRef.current.clientHeight;
      if (!sidebarHeight) {
        return 50;
      }
      const handle_height = (handleHeight / total) * 100;
      const top_min_height =
        (containerTopMinHeight / total) * 100 + handle_height;
      const height = sidebarHeight;
      const bottom_min_height = (containerBottomMinHeight / total) * 100;
      if (height < top_min_height) {
        return top_min_height;
      }
      if (height > 100 - bottom_min_height) {
        return 100 - bottom_min_height;
      }
    }
    return sidebarHeight;
  }, [sidebarHeight]);

  useEffect(() => {
    window.addEventListener('mousemove', resize);
    window.addEventListener('mouseup', stopResizing);
    return () => {
      window.removeEventListener('mousemove', resize);
      window.removeEventListener('mouseup', stopResizing);
    };
  }, [resize, stopResizing]);

  const restrictedHeight = restrictHeight();
  return (
    <div className="app-container">
      <div
        ref={sidebarRef}
        className="app-sidebar"
        style={{ height: `${restrictedHeight}%` }}
      >
        <div className="app-sidebar-content" ref={contentRef}>
          {containerTop}
        </div>
        <div className="app-sidebar-resizer" onMouseDown={startResizing}>
          <div className="resize-handle margin-bottom-2"></div>
          <div className="resize-handle"></div>
        </div>
      </div>
      <div className="app-frame zIndex" ref={appContentRef}>
        {containerBottom}
      </div>
    </div>
  );
}

export default VerticalResizer;
