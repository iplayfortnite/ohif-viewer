import React from 'react';
import { SimpleDialog } from '@ohif/ui';
import './SaveSegmentationDialog.css';

export default function SaveSegmentationDialog(props) {
  const { label, onClose, onExitEdit, onSave } = props;

  return (
    <SimpleDialog
      headerTitle="Save Segmentation"
      onClose={onClose}
      onConfirm={() => {}}
      showFooterButtons={false}
    >
      <div className="contentLabel">{label}</div>
      <div className="buttonContainer">
        <div className="buttonStyle" onClick={onClose}>
          Cancel
        </div>
        {onExitEdit && (
          <div className="buttonStyle" onClick={onExitEdit}>
            Exit edit
          </div>
        )}
        {onSave && (
          <div className="buttonStyle" onClick={onSave}>
            Save
          </div>
        )}
      </div>
    </SimpleDialog>
  );
}
