import React from 'react';
import { SimpleDialog, Icon, ScrollableArea } from '@ohif/ui';

import './HelpDialog.styl';

class HelpDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = { currentTab: 'hotkeys' };
  }

  componentDidMount() {
    window.addEventListener('resize', () => this.forceUpdate());
  }

  componentWillUnmount() {
    window.removeEventListener('resize', () => this.forceUpdate());
  }

  render() {
    const { currentTab } = this.state;
    const { hotkeysManager } = this.props;
    return (
      <SimpleDialog
        headerTitle="Help"
        onClose={this.props.contentProps.onClose}
        onConfirm={this.props.contentProps.onConfirm}
        showFooterButtons={false}
      >
        <div>
          <a
            className={`tab-link btn btn-primary ${
              currentTab === 'hotkeys' ? 'active' : ''
            }`}
            onClick={() => (this.state.currentTab = 'hotkeys')}
          >
            Hotkeys
          </a>
          <a
            className={`tab-link btn btn-primary ${
              currentTab === 'glossary' ? 'active' : ''
            }`}
            onClick={() => (this.state.currentTab = 'glossary')}
          >
            Glossary
          </a>
        </div>
        {currentTab === 'hotkeys' && (
          <HotkeysContent hotkeysManager={hotkeysManager} />
        )}
        {currentTab === 'glossary' && <GlossaryContent />}
      </SimpleDialog>
    );
  }
}

const getHotkeysToDisplay = (hotkeysFromManager = {}, hotkeysLocal = []) => {
  const filterWindowLevelPresets = object => {
    return _.pickBy(object, (value, key) => {
      return (
        (_.startsWith(key, 'windowLevelPreset') ||
          _.startsWith(key, 'setFullDynamicWWWC')) &&
        !_.isEmpty(value.keys)
      );
    });
  };

  const adaptToComponent = object => {
    return _.values(object).map(content => {
      return {
        hotkey: content.keys,
        description: content.label,
      };
    });
  };

  const wwwcOnly = filterWindowLevelPresets(hotkeysFromManager) || {};

  return [...hotkeysLocal, ...adaptToComponent(wwwcOnly)];
};
const HotkeysContent = ({ hotkeysManager }) => {
  const config = window.store.getState()?.flywheel?.projectConfig;
  const configHotkeys = config?.hotkeys;
  let hotkeysToDisplay = getHotkeysToDisplay(
    hotkeysManager.hotkeyDefinitions,
    hotkeys
  );
  if (configHotkeys) {
    hotkeysToDisplay = configHotkeys.map(content => {
      return {
        hotkey: content.keys,
        description: content.label,
      };
    });
  }

  return (
    <div className="flex-container">
      {hotkeysToDisplay.map((entry, index) => {
        return (
          <React.Fragment key={index}>
            <div className="flex-item hotkey-combo">{entry.hotkey}</div>
            <div className="flex-item hotkey-desc">{entry.description}</div>
          </React.Fragment>
        );
      })}
    </div>
  );
};

const GlossaryContent = () => {
  return (
    <ScrollableArea class="glossary-scrollable-container">
      <div className="glossary-flex-container">
        {glossary.map((entry, index) => {
          const iconClassName = entry.class
            ? entry.class + ' glossary-icon'
            : 'glossary-icon';
          return (
            <React.Fragment key={index}>
              <div className="glossary-list-item">
                <Icon
                  name={entry.icon}
                  icon={entry.icon}
                  className={iconClassName}
                />
                <div className="glossary-desc">
                  <span className="glossary-name">{entry.name}</span>
                  <span>{entry.description}</span>
                </div>
              </div>
            </React.Fragment>
          );
        })}
      </div>
    </ScrollableArea>
  );
};

const hotkeys = [
  {
    hotkey: 'ESC',
    description: 'Default Tool / Exit Fullscreen',
  },
  {}, // newline
  {
    hotkey: 'Z',
    description: 'Zoom',
  },
  {
    hotkey: 'W',
    description: 'W/L',
  },
  {
    hotkey: 'P',
    description: 'Pan',
  },
  {
    hotkey: 'A',
    description: 'Angle Measurement',
  },
  {
    hotkey: 'S',
    description: 'Scroll Stack',
  },
  {
    hotkey: 'M',
    description: 'Magnify',
  },
  {
    hotkey: ']',
    description: 'Next Measurement Tool',
  },
  {
    hotkey: '[',
    description: 'Previous Measurement Tool',
  },
  {
    hotkey: 'H',
    description: 'Flip Horizontally',
  },
  {
    hotkey: 'V',
    description: 'Flip Vertically',
  },
  {
    hotkey: 'R',
    description: 'Rotate Right',
  },
  {
    hotkey: 'L',
    description: 'Rotate Left',
  },
  {
    hotkey: 'I',
    description: 'Invert',
  },
  {}, // newline
  {
    hotkey: 'DOWN',
    description: 'Scroll Down',
  },
  {
    hotkey: 'UP',
    description: 'Scroll Up',
  },
  {
    hotkey: 'END',
    description: 'Scroll to Last Image',
  },
  {
    hotkey: 'HOME',
    description: 'Scroll to First Image',
  },
  {
    hotkey: 'RIGHT',
    description: 'Next Image Viewport',
  },
  {
    hotkey: 'LEFT',
    description: 'Previous Image Viewport',
  },
  {
    hotkey: '.',
    description: 'Next Time',
  },
  {
    hotkey: ',',
    description: 'Previous Time',
  },
  {
    hotkey: 'D',
    description: '2D MPR',
  },
  {
    hotkey: 'c',
    description: 'Toggle Proximity ROI Cursor Display',
  },
  {
    hotkey: 'b',
    description: 'Next B Slice',
  },
  {
    hotkey: 'ctrl+z',
    description: 'Undo operation',
  },
  {
    hotkey: 'ctrl+shift+z',
    description: 'Redo operation',
  },
  {
    hotkey: 'shift+s',
    description: 'Toggle Scale Display',
  },
];

const glossary = [
  {
    name: 'Zoom',
    description: 'Zooms in and out by left-clicking and dragging',
    icon: 'search-plus',
  },
  {
    name: 'Levels',
    description:
      'Changes the window width or window center by swiping horizontally or vertically',
    icon: 'level',
  },
  {
    name: 'Pan',
    description:
      'Allows navigating horizontally or vertically on the image by dragging',
    icon: 'arrows',
  },
  {
    name: 'Reset',
    description:
      "Resets all modifications made to the current image's viewport",
    icon: 'reset',
  },
  {
    name: 'CINE',
    description:
      'Shows playback dialog with finer controls for playing sequences',
    icon: 'youtube',
  },
  {
    name: 'Crosshairs',
    description: 'Synchronizes different slices of the same data',
    icon: 'crosshairs',
  },
  {
    name: 'Layout',
    description: 'Allows the selection of the number and position of viewports',
    icon: 'th',
  },
  {
    name: 'Length',
    description: 'Calculates the length between two points',
    icon: 'measure-temp',
  },
  {
    name: 'Parallel',
    description: 'Generates parallel length measurements',
    icon: 'power_input',
    class: 'material-icons',
  },
  {
    name: 'Annotate',
    description: 'Adds an arrow with a custom text as annotation',
    icon: 'measure-non-target',
  },
  {
    name: 'Angle',
    description: 'Determines the smallest angle between three points',
    icon: 'angle-left',
  },
  {
    name: 'Bidirectional',
    description: 'Draws two perpendicular lines with width and length',
    icon: 'measure-target',
  },
  {
    name: 'Rectangle',
    description: 'Draws a rectangle with measurements between two points',
    icon: 'square-o',
  },
  {
    name: 'Free hand',
    description: 'Draws a freehand region within beginning and ending points',
    icon: 'square-o',
  },
  {
    name: 'Open Freehand',
    description: 'Draws an open-ended freehand annotation',
    icon: 'waves',
    class: 'material-icons',
  },
  {
    name: 'Sculpt',
    description: 'Tool to sculpt the freehand ROI or open-freehand ROI',
    icon: 'fiber_smart_record',
    class: 'material-icons',
  },
  {
    name: 'Ellipse',
    description: 'Draws an ellipse with measurements between two points',
    icon: 'circle-o',
    class: 'ellipse-icon',
  },
  {
    name: 'Circle',
    description:
      'Draws a circle with one point as center and distance between two points as radius',
    icon: 'circle-o',
  },
  {
    name: 'ROI Window',
    description:
      'Sets the window width/length of the viewport as the average value in the rectangular region',
    icon: 'stop',
  },
  {
    name: 'Stack Scroll',
    description:
      'Navigates quickly on the stack of images by dragging up and down inside a viewport',
    icon: 'bars',
  },
  {
    name: 'Magnify',
    description: 'Magnifies the region of the image under the pointer',
    icon: 'circle',
  },
  {
    name: 'Probe',
    description: 'Determines the numeric image value under the cursor',
    icon: 'dot-circle',
  },
  {
    name: 'Invert',
    description: 'Inverts the colors of the image in the active viewport',
    icon: 'adjust',
  },
  {
    name: 'Manual',
    description: 'Add or remove points to a segmentation layer.',
    icon: 'brush',
    class: 'material-icons',
  },
  {
    name: 'Smart CT',
    description:
      'Add painted tissues falls under selected density range to segmentation.',
    icon: 'blur_on',
    class: 'material-icons',
  },
  {
    name: 'Auto CT',
    description:
      'Works similar to SmartCT. Density range selection is auto determined based on mouse click point.',
    icon: 'blur_circular',
    class: 'material-icons',
  },
  {
    name: 'Save Segments',
    description: 'Save each segments as NiFTI files to the analyses container',
    icon: 'upload_file',
    class: 'material-icons',
  },
  {
    name: 'Circle Scissors',
    description: 'Tool for manipulating labelmap data by drawing a circle.',
    icon: 'all_out',
    class: 'material-icons',
  },
  {
    name: 'Rectangle Scissors',
    description: 'Tool for manipulating labelmap data by drawing a rectangle.',
    icon: 'crop_16_9',
    class: 'material-icons',
  },
  {
    name: 'Freehand Scissors',
    description:
      'Tool for manipulating labelmap data by drawing a freehand polygon.',
    icon: 'content_cut',
    class: 'material-icons',
  },
  {
    name: 'Threshold Tool',
    description:
      'Tool for segmentation based on configured density ranges and a seed point under mouse with preview.',
    icon: 'format_paint',
    class: 'material-icons',
  },
  {
    name: 'Rotate Right',
    description: 'Rotates the image in the active viewport by 90deg clockwise',
    icon: 'rotate-right',
  },
  {
    name: 'Flip H',
    description: 'Flips the image in the active viewport horizontally',
    icon: 'ellipse-h',
  },
  {
    name: 'Flip V',
    description: 'Flips the image in the active viewport vertically',
    icon: 'ellipse-v',
  },
  {
    name: 'W/L Preset',
    description: 'Predefined window width and window level preset',
    icon: 'level',
  },
  {
    name: 'Eraser',
    description: 'Erases a selected measurement',
    icon: 'eraser',
  },
  {
    name: 'Clear',
    description: 'Clears all current measurements',
    icon: 'trash',
  },
  {
    name: 'Temporal Next',
    description: 'Changes to next time slice image',
    icon: 'step-forward',
  },
  {
    name: 'Temporal Previous',
    description: 'Changes to previous time slice image',
    icon: 'step-backward',
  },
  {
    name: 'Temporal Cine',
    description:
      'Shows playback dialog with finer controls for playing time slice sequences',
    icon: 'youtube',
  },
  {
    name: 'Colormap',
    description: 'Changes image colormap',
    icon: 'color-map',
  },
  {
    name: 'Link',
    description: 'Synchronizes different viewports while using specific tools',
    icon: 'link',
  },
  {
    name: 'Help',
    description: 'Opens this help dialog',
    icon: 'question-circle',
  },
  {
    name: 'Download',
    description: 'Download image',
    icon: 'create-screen-capture',
  },
  {
    name: '2DMPR',
    description: 'Switch to the 2D MPR viewer',
    icon: 'cube',
  },
  {
    name: 'Segmentation',
    description:
      'Loads segmentation overlays on to active NIFTI or DICOM viewports',
    icon: 'layers',
    class: 'material-icons',
  },
  {
    name: 'Manual Rotate',
    description:
      'Rotates the image in the active viewport in the pointer direction',
    icon: 'autorenew',
    class: 'material-icons',
  },
  {
    name: 'Color Selector',
    description: 'Choose custom ROI colors to visually group or delineate ROIs',
    icon: 'palette',
    class: 'material-icons',
  },
  {
    name: 'Undo',
    description: 'Undo(Reverse) the last operation',
    icon: 'undo',
    class: 'material-icons',
  },
  {
    name: 'Redo',
    description: 'Reverse the last undo operation',
    icon: 'redo',
    class: 'material-icons',
  },
];

export default HelpDialog;
