import React from 'react';
import { useSelector } from 'react-redux';
import { Link, useLocation } from 'react-router-dom';

import flywheelSelectors from '../redux/selectors';
import './ErrorPage.styl';

export default function ErrorPage() {
  const { state } = useLocation();
  const activeProject = useSelector(flywheelSelectors.selectActiveProject);

  let { errorMessage, projectId } = state || {};
  if (!projectId) {
    projectId = activeProject && activeProject._id;
  }

  return (
    <div className="error-page">
      <h1>Error</h1>
      <p className="white-space">
        {errorMessage || 'An unknown error has occurred.'}
      </p>
      {projectId ? (
        <Link to={`/project/${projectId}`}>Go to Study List</Link>
      ) : (
        <Link to={`/`}>Go to Project List</Link>
      )}
    </div>
  );
}
