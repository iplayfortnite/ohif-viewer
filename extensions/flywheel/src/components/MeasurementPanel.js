import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import OHIF from '@ohif/core';
import store from '@ohif/viewer/src/store';
import { useSnackbarContext } from '@ohif/ui';
import { connect, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { containersUtils } from '../utils';
import { storeContainer } from './StudyForm/utils/CustomInfo';
import Redux from '../redux';
import * as ComponentsUtils from './utils';
import _ from 'lodash';
import ContourCollisionDialog from './ContourCollisionDialog/ContourCollisionDialog';

import StudyForm from './StudyForm/StudyForm';
import TimerTool from './TimerTool';
import {
  Utils,
  HTTP as FlywheelCommonHTTP,
  Utils as FlywheelCommonUtils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import VerticalResizer from './VerticalResizer/VerticalResizer';

import './MeasurementPanel.styl';

import cornerstone from 'cornerstone-core';
import debounce from 'lodash/debounce';
import classNames from 'classnames';

const timerToolHeight = 34;
const measurementTableHeaderHeight = 40;
const containerTopMinHeight = 70;
const containerBottomMinHeight = 34 + 40; // (study form header height) + (table buttons height);
const {
  MeasurementHandlers,
  getImageIdForImagePath,
  getModality,
} = OHIF.measurements;
const {
  getAllMeasurementsForImage,
  measurementToolUtils,
  getRouteParams,
  shouldShowStudyForm,
} = FlywheelCommonUtils;
const { isOverlapWithOtherROI } = measurementToolUtils.checkMeasurementOverlap;
const { roiTools, getActiveTool } = measurementToolUtils;
const {
  setHighlightOverlappedStatus,
  setResizedFormHeight,
  setScissorsStrategy,
  setThresholdToleranceValue,
  setActiveTool,
} = FlywheelCommonRedux.actions;
const { RightHandPanelModes } = FlywheelCommonRedux.constants;
const { getUser } = FlywheelCommonHTTP.services;

const {
  selectProjectConfig,
  selectSessionRead,
  selectSessionFileRead,
  selectUser,
  hasPermission,
} = Redux.selectors;
const { getContainersIds, findActiveSession } = containersUtils;

const ERROR_MSG_LABEL_MAP = {
  location: 'label',
  x: 'x co-ordinate value',
  y: 'y co-ordinate value',
  boundingBox: 'bounding box',
  'field required': 'field is required and is missing',
  Unauthorized:
    'The viewer has lost connection and no additional work can be completed at this time. Please exit and reload.',
};

const _shouldShowMeasurements = projectConfig => {
  return !projectConfig || projectConfig.hideMeasurements !== true;
};

const MeasurementPanel = props => {
  const {
    showStudyForm,
    showMeasurements,
    projectConfig,
    sessionRead,
    location,
    servicesManager,
    adjustedReadStartTime,
    showStudyFormButtonsOnly,
    activeViewportIndex,
    commandsManager,
    formHeight,
  } = props;
  const [saveLabel, setSaveLabel] = useState(null);
  const [studyFormSliceInfo, setStudyFormSliceInfo] = useState(null);
  const [
    measurementSaveDisableStatus,
    setMeasurementSaveDisableStatus,
  ] = useState(false);
  const [startTime, setStartTime] = useState(Date.now());
  const snackbar = useSnackbarContext();

  const saveMeasurements = () => {
    const overLappedMeasurements = isOverlappedMeasurements();
    if (overLappedMeasurements.length) {
      const { UIDialogService } = servicesManager.services;
      store.dispatch(setHighlightOverlappedStatus(true));
      const measurementData = overLappedMeasurements[0];
      MeasurementHandlers.onItemClick({ ...overLappedMeasurements[0] });
      const dialogId = UIDialogService.create({
        content: ContourCollisionDialog,
        defaultPosition: { x: 420, y: 420 },
        showOverlay: true,
        contentProps: {
          measurementData: { ...measurementData },
          overlappedMeasurements: overLappedMeasurements,
          hideDeleteButton: true,
          hideAutoAdjustButton: true,
          showIgnoreContinueButton: true,
          label:
            'Overlapping contours are not allowed based on viewer configuration. How would you like to proceed?',
          onClose: () => UIDialogService.dismiss({ id: dialogId }),
          onContinue: () => {
            UIDialogService.dismiss({ id: dialogId });
            store.dispatch(setHighlightOverlappedStatus(false));
            continueSaveMeasurement();
          },
        },
      });
    } else {
      continueSaveMeasurement();
    }
  };

  const continueSaveMeasurement = () => {
    ComponentsUtils.updateComponentByPromise(
      {
        default: null,
        init: 'Saving Measurements...',
        success: 'Measurements Saved',
        error: 'Problems saving!',
      },
      FlywheelCommonHTTP.services.saveStudySession,
      (label, error) => {
        if (error) {
          showUserMessageOnError(error);
        }
        setSaveLabel(label);
      },
      2000
    );
  };

  const showUserMessageOnError = async error => {
    snackbar.hideAll();

    if (error.code === 401 || error.message === 'Unauthorized') {
      showPopup(
        error,
        'Viewer lost connection with server:',
        error.message
          ? ERROR_MSG_LABEL_MAP[error.message]
          : ERROR_MSG_LABEL_MAP['Unauthorized']
      );
    } else if (error.code === 403 || error.message === 'Forbidden') {
      let message = error.message || 'Forbidden error';
      if (error.messagePromise) {
        const messageJson = await error.messagePromise;
        message = messageJson.message;
      }
      showPopup(error, 'Permission error', message);
    } else if (error.code === 422 || error.message === 'Unprocessable Entity') {
      let message =
        error.message || 'Unprocessable Entity, error in input data';
      const fieldInfoMsgs = [];
      if (error.messagePromise) {
        const filterFields = ['body', 'data'];
        const messageJson = await error.messagePromise;
        (messageJson?.detail || []).forEach(errorData => {
          const fieldInfo = errorData.loc || [];
          let fieldStr = '';
          let fieldInfoMsg =
            fieldInfo.indexOf(filterFields[0]) >= 0 &&
            fieldInfo.indexOf(filterFields[1]) >= 0
              ? 'Measurement '
              : 'Input data ';
          fieldInfo.forEach(field => {
            if (filterFields.indexOf(field) === -1) {
              if (fieldStr.length) {
                fieldStr += '=> ';
              }
              fieldStr += (ERROR_MSG_LABEL_MAP[field] || field) + ' ';
            }
          });
          if (fieldStr.length) {
            fieldInfoMsg +=
              fieldStr + (ERROR_MSG_LABEL_MAP[errorData.msg] || errorData.msg);
            if (fieldInfoMsgs.indexOf(fieldInfoMsg) === -1) {
              fieldInfoMsgs.push(fieldInfoMsg);
            }
          }
        });
        message = fieldInfoMsgs.length
          ? 'Error in input data properties, Please find the details: '
          : message;
      }
      showPopup(error, 'Missing Required properties', message, fieldInfoMsgs);
    }
  };

  function showPopup(error, title, message, messageAsList = null) {
    snackbar.show({
      title,
      message,
      messageAsList,
      type: 'error',
      error,
      autoClose: true,
      duration: 10000,
      position: 'middleCenter',
    });
  }

  function isOverlappedMeasurements() {
    const state = store.getState();
    //Overlapping conditions for SM should be handled in story FLYW-13411.
    if (
      state.viewports.viewportSpecificData[state.viewports.activeViewportIndex]
        ?.Modality === 'SM'
    ) {
      return [];
    }

    const projectConfig = state.flywheel.projectConfig;
    const labels = projectConfig ? projectConfig.labels || [] : [];
    const contourUnique =
      projectConfig && projectConfig.contourUnique ? true : false;

    const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
      state.viewports.activeViewportIndex
    );

    let overLappedMeasurements = {};
    const isBoundaryCheck = labels.some(
      item => item.boundary && item.boundary.length
    );
    if (isBoundaryCheck || contourUnique) {
      const allMeasurements = state.timepointManager.measurements;
      const allMeasurementsPerImage = getMeasurementsForAllImage(
        allMeasurements
      );
      const keys = Object.keys(allMeasurementsPerImage);
      for (let i = 0; i < keys.length; i++) {
        const measurements = allMeasurementsPerImage[keys[i]];
        measurements.forEach(measurement => {
          if (roiTools.hasOwnProperty(measurement.toolType)) {
            const imageId = getImageIdForImagePath(measurement.imagePath);
            const clonnedMeasurementList = measurements.slice(
              0,
              measurements.length
            );
            let boundaryMeasurements = [];
            if (isBoundaryCheck) {
              boundaryMeasurements = MeasurementHandlers.handleContourBoundary(
                measurement,
                imageId,
                labels,
                clonnedMeasurementList,
                element
              );
              if (boundaryMeasurements && boundaryMeasurements.length) {
                overLappedMeasurements[measurement._id] = measurement;
                boundaryMeasurements.forEach(boundaryMeasurement => {
                  if (!overLappedMeasurements[boundaryMeasurement._id]) {
                    overLappedMeasurements[
                      boundaryMeasurement._id
                    ] = boundaryMeasurement;
                  }
                });
              }
            }
            if (!overLappedMeasurements[measurement._id] && contourUnique) {
              const overLappedMeasurementList = isOverlapWithOtherROI(
                measurement.toolType,
                measurement,
                imageId,
                element
              );
              const filteredMeasurements = [];
              // Check the overlapped measurements are there in the pending measurement list
              overLappedMeasurementList.forEach(overlappedMeasure => {
                if (
                  clonnedMeasurementList.find(
                    originalMeasurement =>
                      originalMeasurement._id === overlappedMeasure._id
                  )
                ) {
                  let labelConfig = labels.find(
                    label => label.value === overlappedMeasure.location
                  );
                  if (labelConfig) {
                    if (
                      !labelConfig.boundary ||
                      labelConfig.boundary.length === 0
                    ) {
                      filteredMeasurements.push(overlappedMeasure);
                    } else if (
                      labelConfig.boundary.length &&
                      !labelConfig.boundary.find(
                        x => x.label === measurement.location
                      )
                    ) {
                      filteredMeasurements.push(overlappedMeasure);
                    }
                  }
                }
              });
              if (filteredMeasurements.length) {
                overLappedMeasurements[measurement._id] = measurement;
                filteredMeasurements.forEach(overlappedMeasurement => {
                  if (!overLappedMeasurements[overlappedMeasurement._id]) {
                    overLappedMeasurements[
                      overlappedMeasurement._id
                    ] = overlappedMeasurement;
                  }
                });
              }
            }
          }
        });
      }
    }
    const overLappedMeasurementList = [];
    Object.keys(overLappedMeasurements).forEach(key => {
      overLappedMeasurementList.push(overLappedMeasurements[key]);
    });
    return overLappedMeasurementList;
  }

  function getMeasurementsForAllImage(measurements) {
    let imageMeasurements = {};
    Object.keys(measurements).forEach(measurementKey => {
      if (!roiTools.hasOwnProperty(measurementKey)) {
        return;
      }
      const measurement = measurements[measurementKey];
      if (!measurement || !measurement.length) {
        return;
      }

      measurement.forEach(x => {
        if (!imageMeasurements[x.imagePath]) {
          imageMeasurements[x.imagePath] = getAllMeasurementsForImage(
            x.imagePath,
            measurements,
            roiTools
          );
        }
      });
    });
    return imageMeasurements;
  }

  const getReadTime = () => {
    return (Date.now() - adjustedReadStartTime) / 1000;
  };

  const featureConfig = useSelector(store => {
    return store.flywheel.featureConfig;
  });

  async function updateReadOnlyStatus() {
    const state = store.getState();
    const { taskId } = getRouteParams(window.location);
    let user = selectUser(state);
    if (!user) {
      user = await getUser();
    }
    const isSiteAdmin = user.roles.includes('site_admin');
    if (featureConfig?.features?.reader_tasks) {
      // Read-only user not able to view and save the study form and measurements if running in non-task mode
      if (!isSiteAdmin && !taskId) {
        const ownAnnotationPermission =
          (await hasPermission(state, 'annotations_own')) ||
          (await hasPermission(state, 'annotations_manage'));
        const hasEditAnnotationsOthersPermission = await hasPermission(
          state,
          'annotations_edit_others'
        );
        setMeasurementSaveDisableStatus(
          !ownAnnotationPermission && !hasEditAnnotationsOthersPermission
        );
      }
    }
  }

  useEffect(() => {
    updateReadOnlyStatus();
  }, [featureConfig]);

  useEffect(() => {
    const { index, element } = getViewportIndex();
    const containerIds = getContainersIds();

    if (containerIds) {
      const { acquisitionId, sessionId } = containerIds;
      const { acquisitions } = state.flywheel;
      const activeAcquisition = acquisitions[acquisitionId];
      const activeSession = findActiveSession(sessionId);

      !activeAcquisition && storeContainer('Acquisition', containerIds);
      !activeSession && storeContainer('Session', containerIds);
    }
    if (index < 0 || !element) {
      return;
    }
    element.removeEventListener(
      cornerstone.EVENTS.IMAGE_RENDERED,
      eventListener
    );

    const eventListener = () =>
      cornerstone.requestAnimationFrame(() => {
        const sliceInfo = FlywheelCommonUtils.getActiveSliceInfo(index) || {};
        // when stack position changes
        if (
          !studyFormSliceInfo ||
          sliceInfo.imageId !== studyFormSliceInfo.imageId
        ) {
          setStudyFormSliceInfo(sliceInfo);
        }
      });
    element.addEventListener(cornerstone.EVENTS.IMAGE_RENDERED, eventListener);
    return () => {
      if (eventListener) {
        element.removeEventListener(
          cornerstone.EVENTS.IMAGE_RENDERED,
          eventListener
        );
      }
    };
  });

  // To show current bSlice measurements when switch tabs.
  useEffect(() => {
    const { index, element } = getViewportIndex();
    if (index < 0 || !element) {
      return;
    }
    const eventListener = setTimeout(() => {
      const sliceInfo = FlywheelCommonUtils.getActiveSliceInfo(index) || {};
      // when stack position changes
      if (
        !studyFormSliceInfo ||
        sliceInfo.imageId !== studyFormSliceInfo.imageId
      ) {
        setStudyFormSliceInfo(sliceInfo);
      }
    }, 100);
    return () => {
      clearTimeout(eventListener);
    };
  }, [activeViewportIndex]);

  function getViewportIndex() {
    const viewports = store.getState().viewports || {};
    const { viewportSpecificData } = viewports;
    let element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
      activeViewportIndex
    );
    let index = activeViewportIndex;
    if (!element) {
      const keys = Object.keys(viewportSpecificData);
      const seriesInstanceUID =
        viewportSpecificData?.[activeViewportIndex]?.SeriesInstanceUID;
      if (seriesInstanceUID) {
        const cornerstoneViewportIndex = keys.find(
          key =>
            viewportSpecificData[key].plugin === 'cornerstone' &&
            viewportSpecificData[key].SeriesInstanceUID === seriesInstanceUID
        );
        if (cornerstoneViewportIndex >= 0) {
          element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
            cornerstoneViewportIndex
          );
          index = cornerstoneViewportIndex;
        }
      }
      if (!element) {
        return { elmeent: null, index: -1 };
      }
    }
    return { element, index };
  }

  function getMeasurementTableHeight() {
    if (showStudyForm) {
      if (showStudyFormButtonsOnly && projectConfig.timerVisible) {
        let totalHeight = timerToolHeight + measurementTableHeaderHeight;
        return `calc(100% - ${totalHeight}px)`;
      } else if (showStudyFormButtonsOnly) {
        return `calc(100% - ${measurementTableHeaderHeight}px)`;
      } else if (projectConfig.timerVisible) {
        return `calc(50% - ${timerToolHeight}px)`;
      }
      return '50%';
    }
    return `calc(100% - ${measurementTableHeaderHeight}px)`;
  }

  function getStudyFormPanelHeight() {
    if (
      (showStudyFormButtonsOnly && !showMeasurements) ||
      (showStudyFormButtonsOnly && projectConfig.timerVisible)
    ) {
      return '';
    } else if (
      showStudyFormButtonsOnly ||
      (showStudyFormButtonsOnly && projectConfig.timerVisible)
    ) {
      return `${timerToolHeight}px`;
    } else if (showMeasurements) {
      return '50%';
    } else if (projectConfig.timerVisible) {
      return `calc(100% - ${measurementTableHeaderHeight}px)`;
    }
    return '100%';
  }

  function onResize(value) {
    store.dispatch(setResizedFormHeight(value));
  }

  function isErrorMessage(message) {
    return message === 'Problems saving!';
  }

  const activeTool = store.getState().infusions?.activeTool;
  const state = store.getState();
  const isMultipleReaderTask = state.multipleReaderTask.TaskIds.length > 1;

  // do not pass children as props
  const childProps = _.omit(props, ['children']);
  const childrenProps = { ...childProps, sliceInfo: studyFormSliceInfo };
  if (!showStudyFormButtonsOnly && showStudyForm && showMeasurements) {
    return (
      <div className="flywheel-measurement-panel">
        <VerticalResizer
          formHeight={formHeight}
          onResize={onResize}
          containerTopMinHeight={
            projectConfig.timerVisible ? containerTopMinHeight : timerToolHeight
          }
          containerTop={
            <div className="flywheel-measurement-panel flex-column container-top">
              <TimerTool
                projectConfig={projectConfig}
                showStudyForm={showStudyForm}
                adjustedReadStartTime={adjustedReadStartTime}
              />
              <div className="measurement-container">
                {props.children(childrenProps)}
              </div>
            </div>
          }
          containerBottomMinHeight={containerBottomMinHeight}
          containerBottom={
            <div className=" flex-column height-full padding-top-25">
              {showStudyForm && (
                <div className="study-form-container">
                  <StudyForm
                    saveStudy={FlywheelCommonHTTP.services.saveStudySession}
                    showUserErrorMessage={showUserMessageOnError}
                    getReadTime={getReadTime}
                    servicesManager={servicesManager}
                    commandsManager={commandsManager}
                    sliceInfo={studyFormSliceInfo}
                  />
                </div>
              )}
              {!showStudyForm && (
                <button
                  disabled={props.isSaving}
                  onClick={saveMeasurements}
                  className="save-measurements-button"
                  style={{
                    fontWeight: isErrorMessage(saveLabel) ? 'bold' : 'normal',
                  }}
                >
                  {saveLabel || 'Save Measurements'}
                </button>
              )}
            </div>
          }
        />
      </div>
    );
  }

  let panel_container = 'flywheel-measurement-panel';
  if (
    showStudyFormButtonsOnly &&
    !showMeasurements &&
    !projectConfig.timerVisible
  ) {
    panel_container =
      'flywheel-measurement-panel not-visible-measurement-and-timer';
  } else if (showStudyFormButtonsOnly && !showMeasurements) {
    panel_container = 'flywheel-measurement-panel not-visible-measurement';
  }
  return (
    <div className={panel_container}>
      <TimerTool
        projectConfig={projectConfig}
        showStudyForm={showStudyForm}
        adjustedReadStartTime={adjustedReadStartTime}
        sessionRead={sessionRead}
        location={location}
        setStartTime={setStartTime}
        startTime={startTime}
      />
      {showMeasurements && (
        <div
          style={{
            height: getMeasurementTableHeight(),
          }}
        >
          {props.children(childrenProps)}
        </div>
      )}
      {showStudyForm && (
        <div
          style={{
            height: getStudyFormPanelHeight(),
          }}
        >
          <StudyForm
            saveStudy={FlywheelCommonHTTP.services.saveStudySession}
            showUserErrorMessage={showUserMessageOnError}
            getReadTime={getReadTime}
            servicesManager={servicesManager}
            commandsManager={commandsManager}
            sliceInfo={studyFormSliceInfo}
            showStudyFormButtonsOnly={showStudyFormButtonsOnly}
          />
        </div>
      )}
      {!showStudyForm && (
        <button
          disabled={props.isSaving || measurementSaveDisableStatus}
          onClick={saveMeasurements}
          className="save-measurements-button"
          style={{
            opacity:
              props.isSaving || measurementSaveDisableStatus ? '0.5' : '1',
            pointerEvents:
              props.isSaving || measurementSaveDisableStatus ? 'none' : 'auto',
            fontWeight: isErrorMessage(saveLabel) ? 'bold' : 'normal',
          }}
        >
          {saveLabel || 'Save Measurements'}
        </button>
      )}
    </div>
  );
};

MeasurementPanel.propTypes = {
  projectConfig: PropTypes.object,
};

function mapStateToProps(state, ownProps) {
  const { location } = ownProps;
  const { panelMode } = state.rightHandPanel;
  const projectConfig = _.cloneDeep(selectProjectConfig(state));
  const showStudyFormButtonsOnly = ownProps.showStudyFormButtonsOnly;
  const showStudyForm = shouldShowStudyForm(projectConfig, state, location);
  const showMeasurements = _shouldShowMeasurements(projectConfig);
  const adjustedReadStartTime = state.timer.adjustedReadStartTime;
  const formHeight = state.measurementPanelProperties.formHeight;
  const viewports = state.viewports || {};
  const { activeViewportIndex } = viewports;

  if (projectConfig) {
    // Do not show timer if timer is off
    projectConfig.timerVisible =
      projectConfig.timerOn && projectConfig.timerVisible;
  }

  let sessionRead = null;
  if (Utils.isCurrentFile(state)) {
    if (state.flywheel.readerTaskWithFormResponse) {
      sessionRead = state.flywheel.readerTaskWithFormResponse.info;
    } else {
      sessionRead = selectSessionFileRead(state);
    }
  } else {
    if (state.flywheel.readerTaskWithFormResponse) {
      sessionRead = state.flywheel.readerTaskWithFormResponse.info;
    } else {
      sessionRead = selectSessionRead(state);
    }
  }

  return {
    projectConfig,
    showMeasurements,
    showStudyForm,
    isSaving: state.timepointManager.isSaving,
    sessionRead: sessionRead,
    adjustedReadStartTime,
    showStudyFormButtonsOnly,
    activeViewportIndex,
    formHeight,
    panelMode,
  };
}

const ConnectedMeasurementPanel = withRouter(
  connect(mapStateToProps)(MeasurementPanel)
);

export default ConnectedMeasurementPanel;
