import React, { useEffect, useState, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import './SmartCTRange.styl';
import csTools from 'cornerstone-tools';
import classnames from 'classnames';
import {
  getActiveLabelmapIndex,
  setSegmentToolActive,
} from '@flywheel/extension-cornerstone-infusions';

const { setSmartCTRange } = FlywheelCommonRedux.actions;
const { getAvailableSmartCTRanges } = FlywheelCommonRedux.selectors;
const { getSmartCTRanges } = FlywheelCommonRedux.smartCTRanges;
const segmentationModule = csTools.getModule('segmentation');

const SmartCTRange = () => {
  const dispatch = useDispatch();
  const activeSmartCTRange = useSelector(store => store.smartCT.selectedRange);
  const [isRangeSelectDropdownOpen, setDropdownStatus] = useState(false);
  const [selectedItem, setSelectedItem] = useState(null);
  const [selectedRange, setRange] = useState(activeSmartCTRange);
  const [smartCTRangeList, setSmartCTRangeList] = useState(getCTRanges());
  let smartCTRangesFromConfig = useSelector(getAvailableSmartCTRanges);
  const segmentationData =
    useSelector(store => store.segmentation.segmentationData) || {};
  const [
    shouldDisableRangeSelectionDropdown,
    setDropdownEnableDisableStatus,
  ] = useState(false);
  const ref = useRef();

  useEffect(() => {
    const CTRanges = getCTRanges();
    setSmartCTRangeList(CTRanges);
    if (!activeSmartCTRange) {
      dispatch(setSmartCTRange(CTRanges?.[0]));
    }
  }, [smartCTRangesFromConfig]);

  useEffect(() => {
    setRange(activeSmartCTRange);
    activeSmartCTRange
      ? setSelectedItem(activeSmartCTRange)
      : setSelectedItem(selectOptions[0]);
    if (!activeSmartCTRange) {
      const segmentationColor =
        csTools.store.modules.segmentation.state.colorLutTables;

      const defaultRange = {
        label: 'Unknown',
        range: {
          min: -1000,
          max: 3000,
        },
        color: segmentationColor[0][1],
      };
      let defaultSmartRange = smartCTRangeList?.[0];
      if (smartCTRangesFromConfig) {
        defaultSmartRange = smartCTRangesFromConfig?.[0];
      }
      dispatch(setSmartCTRange(defaultSmartRange || defaultRange));
    }
  }, [activeSmartCTRange]);

  useEffect(() => {
    const checkIfClickedOutside = e => {
      // If the dropdown is open and the clicked target is not within the dropdown,
      // then close the dropdown
      if (
        isRangeSelectDropdownOpen &&
        ref.current &&
        !ref.current.contains(e.target)
      ) {
        setDropdownStatus(false);
      }
    };
    document.addEventListener('mousedown', checkIfClickedOutside);
    return () => {
      document.removeEventListener('mousedown', checkIfClickedOutside);
    };
  }, [isRangeSelectDropdownOpen]);

  useEffect(() => {
    const activeLabelmapIndex = getActiveLabelmapIndex();
    let editStatus = false;
    if (activeLabelmapIndex > 0) {
      editStatus = true;
      setSegmentToolActive();
    }

    setDropdownEnableDisableStatus(editStatus);
  }, [segmentationData]);

  function getCTRanges() {
    if (smartCTRangesFromConfig) {
      let colorLUT = [];
      smartCTRangesFromConfig.forEach(ranges => {
        colorLUT.push([...ranges.color.slice(0, 3), ranges.color[3] * 255]);
      });
      segmentationModule.setters.colorLUT(0, colorLUT);
    } else {
      smartCTRangesFromConfig = getSmartCTRanges();
    }
    return smartCTRangesFromConfig;
  }

  function getSelectedSmartCTRange() {
    if (selectedRange) {
      const index = smartCTRangeList.findIndex(
        x => x.label === selectedRange.label
      );
      return index;
    }
    return -1;
  }

  const displaySegmentationLabel = segmentation => {
    let segmentationLabel = segmentation?.label;
    if (
      isFinite(segmentation?.range?.min) &&
      isFinite(segmentation?.range?.max)
    ) {
      segmentationLabel = `${segmentation.label} [${segmentation?.range?.min} - ${segmentation?.range?.max}]`;
    }
    return segmentationLabel;
  };

  let selectOptions = [];
  smartCTRangeList.forEach((x, index) => {
    selectOptions.push({
      value: index,
      label: displaySegmentationLabel(x),
      color: x.color,
    });
  });

  const toggleOptions = () => {
    setDropdownStatus(
      !isRangeSelectDropdownOpen && !shouldDisableRangeSelectionDropdown
    );
  };

  function selected(item) {
    let index = Number(item.value);
    setSelectedItem(item);
    if (index > -1) {
      store.dispatch(setSmartCTRange(smartCTRangeList[index]));
      setRange(smartCTRangeList[index]);
    }
    toggleOptions();
  }

  const checkSelectedLabel = displaySegmentationLabel(selectedItem);

  const listItems = selectOptions.map(item => (
    <li
      key={item.value}
      onClick={() => {
        selected(item);
      }}
    >
      <div
        className="color-picker-container"
        style={{
          backgroundColor: `rgba(${item.color[0]}, ${item.color[1]}, ${item.color[2]}, 1.0 )`,
        }}
      ></div>
      <div
        style={{ color: checkSelectedLabel === item.label ? 'red' : 'white' }}
      >
        {item.label}
      </div>
    </li>
  ));

  const disabled = shouldDisableRangeSelectionDropdown;
  return (
    <div ref={ref} className="smartCT-container">
      <div
        value={getSelectedSmartCTRange()}
        className={classnames('display-label', { disabled })}
        onClick={toggleOptions}
      >
        <div
          className="color-picker-container"
          style={{
            backgroundColor: `rgba(${selectedItem?.color[0]}, ${selectedItem?.color[1]}, ${selectedItem?.color[2]}, 1.0 )`,
          }}
        ></div>
        <span>{selectedItem?.label}</span>
      </div>
      {isRangeSelectDropdownOpen && (
        <div className="select-container">
          <ul>{listItems}</ul>
        </div>
      )}
    </div>
  );
};

export default SmartCTRange;
