import { Component } from 'react';
import React from 'react';
import PropTypes from 'prop-types';
import { SimpleDialog } from '@ohif/ui';
import OHIF from '@ohif/core';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import bounding from '@ohif/viewer/src/lib/utils/bounding.js';
import './ContourCollisionDialog.css';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
const {
  measurementToolUtils,
  getAllMeasurementsForImage,
  getBoundaryErrorMessage,
} = FlywheelCommonUtils;
const { roiTools } = measurementToolUtils;

const { setActiveTool, setSubFormPopupClose } = FlywheelCommonRedux.actions;
const { MeasurementHandlers, getImageIdForImagePath } = OHIF.measurements;
const { autoAdjustMeasurement } = measurementToolUtils.autoAdjustMeasurement;
export default class ContourCollisionDialog extends Component {
  static defaultProps = {
    componentRef: React.createRef(),
    componentStyle: {},
  };

  static propTypes = {
    measurementData: PropTypes.object.isRequired,
    onClose: PropTypes.func.isRequired,
    onContinue: PropTypes.func,
    componentRef: PropTypes.object,
    componentStyle: PropTypes.object,
    showIgnoreContinueButton: PropTypes.bool,
    hideDeleteButton: PropTypes.bool,
    hideAutoAdjustButton: PropTypes.bool,
  };

  constructor(props) {
    super(props);
    this.state = {
      label: props.label,
      hideAutoAdjustButton: props.hideAutoAdjustButton,
    };
    this.mainElement = React.createRef();
  }

  componentDidMount = () => {
    bounding(this.mainElement);
  };

  render() {
    return (
      <SimpleDialog
        headerTitle="Overlapping Error"
        onClose={() => {}}
        onConfirm={() => {}}
        componentRef={this.mainElement}
        showFooterButtons={false}
        hideCloseButton={true}
      >
        <div className="labelStyle">{this.state.label}</div>
        <div className="btnContainer">
          {!this.props.hideDeleteButton ? (
            <div className="btnStyle" onClick={() => this.onDelete()}>
              Delete and Redraw
            </div>
          ) : null}
          {!this.state.hideAutoAdjustButton ? (
            <div className="btnStyle" onClick={() => this.handleAutoAdjust()}>
              Automatically Adjust
            </div>
          ) : null}
          <div onClick={() => this.handleManualAdjust()} className="btnStyle">
            Manually Adjust
          </div>
          {this.props.showIgnoreContinueButton ? (
            <div
              onClick={() => this.handleIgnoreAndContinue()}
              className="btnStyle"
            >
              Ignore and Continue
            </div>
          ) : null}
        </div>
      </SimpleDialog>
    );
  }

  handleIgnoreAndContinue() {
    this.onClose(true);
  }

  showHiddenOverlappedLines = () => {
    const overlappedMeasurements = this.props.overlappedMeasurements || [];
    overlappedMeasurements.forEach(measurement => {
      if (!measurement.visible) {
        MeasurementHandlers.onVisible(
          {
            detail: {
              toolType: measurement.toolType,
              measurementData: {
                _id: measurement._id,
                lesionNamingNumber: measurement.lesionNamingNumber,
                measurementNumber: measurement.measurementNumber,
              },
            },
          },
          true
        );
      }
    });
  };

  handleAutoAdjust = () => {
    let measurementData = this.props.measurementData;
    const imageId = getImageIdForImagePath(measurementData.imagePath);
    const state = store.getState();
    const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
      state.viewports.activeViewportIndex
    );
    const measurements = getAllMeasurementsForImage(
      measurementData.imagePath,
      state.timepointManager.measurements,
      roiTools
    );
    if (state.flywheel?.projectConfig?.studyFormWorkflow) {
      const measurement = measurements?.find(
        data => data._id === measurementData._id
      );
      measurementData = measurement ? measurement : measurementData;
    }
    const projectConfig = state.flywheel.projectConfig;
    const labels = projectConfig?.labels || [];
    // Boundary compared measurements will filter out from the image measurements list
    const boundaryMeasurements = MeasurementHandlers.handleContourBoundary(
      measurementData,
      imageId,
      labels,
      measurements,
      element
    );
    const boundaryError = getBoundaryErrorMessage(
      boundaryMeasurements,
      measurementData
    );
    const autoAdjust = autoAdjustMeasurement(
      measurementData.toolType,
      measurementData,
      imageId
    );
    if (autoAdjust) {
      this.setState({
        label: this.props.label,
        hideAutoAdjustButton: this.props.hideAutoAdjustButton,
      });
      this.onClose();
    } else {
      this.setState({
        label: `The contour can not be adjusted automatically, please adjust manually.
          ${boundaryError ? boundaryError + '.' : ''}`,
        hideAutoAdjustButton: true,
      });
    }
    cornerstone.getEnabledElements().forEach(enabledElement => {
      if (enabledElement.image) {
        cornerstone.updateImage(enabledElement.element);
      }
    });
    if (state.flywheel?.projectConfig?.studyFormWorkflow) {
      const data = { delete: false, autoAdjust: true };
      store.dispatch(setSubFormPopupClose(data));
    }
  };

  handleManualAdjust = () => {
    const measurementData = this.props.measurementData;
    const store = window.store;
    if (
      measurementData.toolType === 'OpenFreehandRoi' ||
      measurementData.toolType === 'FreehandRoi'
    ) {
      store.dispatch(setActiveTool('FreehandRoiSculptor'));
    } else {
      store.dispatch(setActiveTool(measurementData.toolType));
    }
    this.onClose();
  };

  onDelete = () => {
    let measurementData = this.props.measurementData;
    MeasurementHandlers.onRemoved({
      detail: {
        toolType: measurementData.toolType,
        measurementData: {
          _id: measurementData._id,
          lesionNamingNumber: measurementData.lesionNamingNumber,
          measurementNumber: measurementData.measurementNumber,
        },
      },
    });
    const state = store.getState();
    if (state.flywheel?.projectConfig?.studyFormWorkflow) {
      const data = { delete: true, autoAdjust: false };
      store.dispatch(setSubFormPopupClose(data));
    }
    this.onClose();
  };

  onClose = (isContinue = false) => {
    this.showHiddenOverlappedLines();
    if (isContinue && this.props.onContinue) {
      this.props.onContinue();
    } else {
      this.props.onClose();
    }
  };
}
