import Redux from './redux';
import Reactive from './reactive';
import HTTP from './http';
import Context from './context';
import {
  cornerstoneUtils,
  DataAdapters,
  isCurrentFile,
  isCurrentNifti,
  isCurrentMetaImage,
  isCurrentVolumetricImage,
  isCurrentWebImage,
  isCurrentZipFile,
  isSingleFileMode,
  getRouteParams,
  getParamsFromContainerFileUrl,
  filterToolbarButtonList,
  getAllMeasurementsForImage,
  bSliceColorUtils,
  getActiveSliceInfo,
  isBSliceApplicable,
  parseSliceIndex,
  isProtocolApplicable,
  getUuid,
  getActiveFile,
  showErrorPage,
  validateBSlice,
  validateAllBSlice,
  shouldShowStudyForm,
  isMicroscopyData,
  isSliceOrderSame,
  getSmartCTRangeList,
  getBoundaryErrorMessage,
  getCondensedProjectConfig,
  hasToolLimitReached,
  hasActiveToolLimitReached,
  hasLabelsLimitReached,
  getMeasurementToolsForAnswer,
  getBSliceSettings,
  getLabelsForTool,
  getBSliceTools,
  getToolsAndLabelsForQuestionAnswer,
  getQuestionAndAnswerForMeasurement,
  activateToolsAndLabelsForAnswer,
  hasInstructionRange,
  getSelectedOptionForQuestion,
  getLabelLimits,
  getCountForLabel,
  getLabelCount,
  getPixelDetailsInTargetSeries,
  shouldIgnoreUserSettings,
  isHpForWebImages,
  isImageOpenedFromSession,
} from './utils';
import measurementToolUtils from './tools/utils';
import commonToolUtils from './tools/commonUtils';
import undoRedoHandlers from './tools/undoRedoHandlers';
import initExtension from './init';
import { default as Tools, unitUtils } from './tools';
import * as segmentationUtils from './VolumetricImage/segmentation/manageSegments';

// control what will be exposed from utils folder
const Utils = {
  cornerstoneUtils,
  DataAdapters,
  isCurrentFile,
  isCurrentNifti,
  isCurrentMetaImage,
  isCurrentVolumetricImage,
  isCurrentWebImage,
  isCurrentZipFile,
  isSingleFileMode,
  getRouteParams,
  getParamsFromContainerFileUrl,
  filterToolbarButtonList,
  getAllMeasurementsForImage,
  measurementToolUtils,
  commonToolUtils,
  bSliceColorUtils,
  getActiveSliceInfo,
  isBSliceApplicable,
  segmentationUtils,
  undoRedoHandlers,
  parseSliceIndex,
  isProtocolApplicable,
  getUuid,
  getActiveFile,
  showErrorPage,
  validateBSlice,
  validateAllBSlice,
  shouldShowStudyForm,
  isMicroscopyData,
  isSliceOrderSame,
  getSmartCTRangeList,
  getBoundaryErrorMessage,
  getCondensedProjectConfig,
  hasToolLimitReached,
  hasActiveToolLimitReached,
  hasLabelsLimitReached,
  getMeasurementToolsForAnswer,
  getBSliceSettings,
  getLabelsForTool,
  getBSliceTools,
  getToolsAndLabelsForQuestionAnswer,
  getQuestionAndAnswerForMeasurement,
  activateToolsAndLabelsForAnswer,
  hasInstructionRange,
  getSelectedOptionForQuestion,
  getLabelLimits,
  getCountForLabel,
  getLabelCount,
  getPixelDetailsInTargetSeries,
  shouldIgnoreUserSettings,
  isHpForWebImages,
  isImageOpenedFromSession,
};

const ToolsUtils = {
  unitUtils,
};

export default {
  id: 'flywheel-common-extension',
  preRegistration({ commandsManager, servicesManager, appConfig }) {
    initExtension({ commandsManager, servicesManager, appConfig });
  },
};

export { Context, HTTP, Reactive, Redux, Tools, ToolsUtils, Utils };
