import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';

import getActiveTool from './utils/getActiveTool';
import updatePerpendicularLineHandles from './utils/updatePerpendicularLineHandles';

import commonToolUtils from './commonUtils';

import * as unitUtils from './unit';

import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import measurementToolUtils from './utils';
import { getDefaultBoundingBox } from './textBoxContent';
import { shouldEnableAnnotationTool } from './utils/shouldEnableAnnotationTool';

import * as TextBoxContentUtils from './textBoxContent';

const { setNextPalette } = FlywheelCommonRedux.actions;

const {
  findToolIndex,
  saveActionState,
  upOrEndEventsManager,
} = commonToolUtils;

const draw = csTools.importInternal('drawing/draw');
const drawLine = csTools.importInternal('drawing/drawLine');
const getNewContext = csTools.importInternal('drawing/getNewContext');
const setShadow = csTools.importInternal('drawing/setShadow');
const drawHandles = csTools.importInternal('drawing/drawHandles');
const drawLinkedTextBox = csTools.importInternal('drawing/drawLinkedTextBox');
const moveNewHandle = csTools.importInternal('manipulators/moveNewHandle');
const anyHandlesOutsideImage = csTools.importInternal(
  'manipulators/anyHandlesOutsideImage'
);
const getPixelSpacing = csTools.importInternal('util/getPixelSpacing');
const triggerEvent = csTools.importInternal('util/triggerEvent');

const {
  addToolState,
  EVENTS,
  getToolState,
  removeToolState,
  toolColors,
} = csTools;

// set an alias
const CornerstoneBidirectionalTool = csTools.BidirectionalTool;

/**
 * @public
 * @class BidirectionalTool
 * @memberof Tools.Annotation
 * @classdesc Create and position an annotation that measures the
 * length and width of a region.
 * @extends CornerstoneBidirectionalTool
 */
export default class BidirectionalTool extends CornerstoneBidirectionalTool {
  static toolName = 'BidirectionalTool';

  constructor(props = {}) {
    super(props);
    this.parentHandleSelectedCallback = this.handleSelectedCallback;
    this.handleSelectedCallback = this.handleCustomSelectedCallback.bind(this);
    this._endDrawing = this._endDrawing.bind(this);
  }

  renderToolData = evt => {
    const eventData = evt.detail;
    const { element, canvasContext, image } = eventData;
    const {
      handleRadius,
      drawHandlesOnHover,
      hideHandlesIfMoving,
      renderDashed,
    } = this.configuration;

    const lineDash = csTools.getModule('globalConfiguration').configuration
      .lineDash;

    // If we have no toolData for this element, return immediately as there is nothing to do
    const toolData = getToolState(element, this.name);

    if (!toolData) {
      return;
    }

    const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(image);

    // We have tool data for this element - iterate over each one and draw it
    const context = getNewContext(canvasContext.canvas);

    const lineWidth = csTools.toolStyle.getToolWidth();

    for (let i = 0; i < toolData.data.length; i++) {
      const data = toolData.data[i];

      if (data.visible === false) {
        continue;
      }

      let color = data.active
        ? toolColors.getActiveColor()
        : data.color || toolColors.getColorIfActive(data);
      color = unitUtils.convertRgbToRgba(color, data.color);
      color = color.substring(0, color.lastIndexOf(',')) + ', 1)';
      if (measurementToolUtils.hasColorIntensity(data)) {
        color = measurementToolUtils.reduceColorOpacity(color);
      }

      // Calculate the data measurements
      if (data.invalidated === true) {
        if (data.longestDiameter && data.shortestDiameter) {
          this.throttledUpdateCachedStats(image, element, data);
        } else {
          this.updateCachedStats(image, element, data);
        }
      }

      draw(context, context => {
        // Configurable shadow
        setShadow(context, this.configuration);

        const {
          start,
          end,
          perpendicularStart,
          perpendicularEnd,
          textBox,
        } = data.handles;

        const lineOptions = { color };
        const perpendicularLineOptions = { color, strokeWidth };

        if (renderDashed) {
          lineOptions.lineDash = lineDash;
          perpendicularLineOptions.lineDash = lineDash;
        }

        // Draw the measurement line
        drawLine(context, element, start, end, lineOptions);

        // Draw perpendicular line
        const strokeWidth = lineWidth;

        updatePerpendicularLineHandles(eventData, data);

        drawLine(
          context,
          element,
          perpendicularStart,
          perpendicularEnd,
          perpendicularLineOptions
        );

        // Draw the handles
        const handleOptions = {
          color,
          handleRadius,
          drawHandlesIfActive: drawHandlesOnHover,
          hideHandlesIfMoving,
        };

        // Draw the handles
        if (this.configuration.drawHandles) {
          drawHandles(context, eventData, data.handles, handleOptions);
        }

        if (!data.handles.textBox.hasMoved) {
          const coords = {
            x: Math.max(data.handles.start.x, data.handles.end.x),
          };

          // Depending on which handle has the largest x-value,
          // Set the y-value for the text box
          if (coords.x === data.handles.start.x) {
            coords.y = data.handles.start.y;
          } else {
            coords.y = data.handles.end.y;
          }

          data.handles.textBox.x = coords.x;
          data.handles.textBox.y = coords.y;
        }

        if (unitUtils.isMeasurementOutputHidden()) {
          // Temporary status flag to indicate measurement visibility in
          // viewport, not to be saved in server.
          data.handles.textBox.isVisible = false;
          data.handles.textBox.boundingBox = getDefaultBoundingBox(
            data.handles.textBox
          );
        } else {
          // Draw the textbox
          // Move the textbox slightly to the right and upwards
          // So that it sits beside the length tool handle
          const xOffset = 10;
          const textBoxAnchorPoints = handles => [
            handles.start,
            handles.end,
            handles.perpendicularStart,
            handles.perpendicularEnd,
          ];
          const textLines = getTextBoxText(
            data,
            rowPixelSpacing,
            colPixelSpacing
          );

          drawLinkedTextBox(
            context,
            element,
            textBox,
            textLines,
            data.handles,
            textBoxAnchorPoints,
            color,
            lineWidth,
            xOffset,
            true
          );
        }
      });
    }
  };

  addNewMeasurement = (evt, interactionType) => {
    const eventData = evt.detail;
    const { element, image, buttons } = eventData;

    const config = this.configuration;

    const measurementData = this.createNewMeasurement(eventData);

    const doneCallback = () => {
      measurementData.active = false;
      cornerstone.updateImage(element);
    };

    // Associate this data with this imageId so we can render it and manipulate it
    addToolState(element, this.name, measurementData);
    cornerstone.updateImage(element);

    const timestamp = new Date().getTime();
    const { end, perpendicularStart } = measurementData.handles;

    moveNewHandle(
      eventData,
      this.name,
      measurementData,
      end,
      {},
      interactionType,
      success => {
        if (!success) {
          removeToolState(element, this.name, measurementData);

          return;
        }
        const { handles, longestDiameter, shortestDiameter } = measurementData;
        const hasHandlesOutside = anyHandlesOutsideImage(eventData, handles);
        const longestDiameterSize = parseFloat(longestDiameter) || 0;
        const shortestDiameterSize = parseFloat(shortestDiameter) || 0;
        const isTooFast = new Date().getTime() - timestamp < 150;

        let minSize = 1;
        if (image.columnPixelSpacing > 0 && image.rowPixelSpacing > 0) {
          const minImageDimension = Math.min(
            image.columns * image.columnPixelSpacing,
            image.rows * image.rowPixelSpacing
          );
          if (minImageDimension < 50) {
            // in case of microscopic scans < 5cm
            minSize = minImageDimension / 100; // annotations can be drawn in minimum of 1/100th of the physical size
          }
        }

        const isTooSmal =
          longestDiameterSize < minSize || shortestDiameterSize < minSize;

        if (hasHandlesOutside || isTooSmal || isTooFast) {
          // Delete the measurement
          measurementData.cancelled = true;
          removeToolState(element, this.name, measurementData);
        } else {
          // Set lesionMeasurementData Session
          if (!isEnabledAutomatedLabelling()) {
            config.getMeasurementLocationCallback(
              measurementData,
              eventData,
              doneCallback
            );
          }
        }

        // Update perpendicular line and disconnect it from the long-line
        updatePerpendicularLineHandles(eventData, measurementData);
        perpendicularStart.locked = false;

        measurementData.invalidated = true;

        cornerstone.updateImage(element);

        const activeTool = getActiveTool(element, buttons, interactionType);

        if (activeTool instanceof BidirectionalTool) {
          activeTool.updateCachedStats(image, element, measurementData);
        }

        const modifiedEventData = {
          toolName: this.name,
          toolType: this.name, // Deprecation notice: toolType will be replaced by toolName
          element,
          measurementData,
        };

        triggerEvent(element, EVENTS.MEASUREMENT_MODIFIED, modifiedEventData);
        triggerEvent(element, EVENTS.MEASUREMENT_COMPLETED, modifiedEventData);
      }
    );
  };

  createNewMeasurement = mouseEventData => {
    const { x, y } = mouseEventData.currentPoints.image;
    // Create the measurement data for this tool with the end handle activated
    const measurementData = {
      toolName: this.name,
      toolType: this.name, // Deprecation notice: toolType will be replaced by toolName
      isCreating: true,
      visible: true,
      active: true,
      invalidated: true,
      handles: {
        start: getHandle(x, y, 0),
        end: getHandle(x, y, 1, { active: true }),
        perpendicularStart: getHandle(x, y, 2, { locked: true }),
        perpendicularEnd: getHandle(x, y, 3),
        textBox: getHandle(x, y, null, {
          highlight: false,
          hasMoved: false,
          active: false,
          movesIndependently: false,
          drawnIndependently: true,
          allowedOutsideImage: true,
          hasBoundingBox: true,
        }),
      },
      longestDiameter: 0,
      shortestDiameter: 0,
    };

    const colorPalette = store.getState().colorPalette;
    const color = colorPalette.roiColor.next;
    store.dispatch(setNextPalette());
    return Object.assign({}, measurementData, { color: color });
  };

  pointNearTool(element, data, coords, interactionType = 'mouse') {
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (data.readonly || !data.visible || !isAnnotationToolEnabled) {
      return false;
    }
    return super.pointNearTool(element, data, coords, interactionType);
  }

  /**
   * Custom callback for when a handle is selected.
   * @method handleSelectedCallback
   * @memberof Tools.Base.BaseAnnotationTool
   *
   * @param  {*} evt    -
   * @param  {*} toolData   -
   * @param  {*} handle - The selected handle.
   * @param  {String} interactionType -
   * @returns {void}
   */
  handleCustomSelectedCallback(
    evt,
    toolData,
    handle,
    interactionType = 'mouse'
  ) {
    const eventData = evt.detail;
    const { element } = eventData;
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (toolData.readonly || !toolData.visible || !isAnnotationToolEnabled) {
      return;
    }
    this.parentHandleSelectedCallback(evt, toolData, handle, interactionType);
    this._initializeDrawing(evt, handle, interactionType);
  }

  /**
   * Custom callback for when a tool is selected.
   *
   * @method toolSelectedCallback
   * @memberof Tools.Base.BaseAnnotationTool
   *
   * @param  {*} evt
   * @param  {*} annotation
   * @param  {string} [interactionType=mouse]
   * @returns {void}
   */
  toolSelectedCallback(evt, annotation, interactionType = 'mouse') {
    if (!annotation.readonly && annotation.visible) {
      super.toolSelectedCallback(evt, annotation, interactionType);
      this._initializeDrawing(evt, annotation.handles?.start, interactionType);
    }
  }

  /**
   * Initialize the drawing loop when tool is active and a click event happens.
   *
   * @private
   * @param {Object} evt - The event.
   * @param  {*} handle - Selected handle or any of the handle.
   * @param  {string} [interactionType=mouse]
   * @returns {void}
   */
  _initializeDrawing(evt, handle, interactionType = 'mouse') {
    const { element } = evt.detail;
    const currentTool = findToolIndex(element, this.name, handle);
    if (currentTool >= 0) {
      this.configuration.currentTool = currentTool;
      this._startDrawing(evt);
      upOrEndEventsManager.registerHandler(
        element,
        interactionType,
        this._endDrawing
      );
    }
  }

  /**
   * Beginning of drawing loop when tool is active and a click event happens.
   *
   * @private
   * @param {Object} evt - The event.
   * @returns {void}
   */
  _startDrawing(evt) {
    const { element } = evt.detail;
    const toolState = getToolState(element, this.name);
    const data = toolState.data[this.configuration.currentTool];
    saveActionState(data, 'measurements', 'Modify', true, false);
  }

  /**
   * Ends the active drawing loop and completes the polygon.
   *
   * @private
   * @param {Object} evt - The event.
   * @returns {void}
   */
  _endDrawing(evt) {
    const { element } = evt.detail;
    const interactionType = upOrEndEventsManager.getCurrentInteractionType(evt);
    upOrEndEventsManager.removeHandler(
      element,
      interactionType,
      this._endDrawing
    );
    const toolState = getToolState(element, this.name);
    const data = toolState.data[this.configuration.currentTool];
    saveActionState(data, 'measurements', 'Modify', false, true);
    this.configuration.currentTool = -1;
  }

  /** Ends the active drawing loop forcefully.
   *
   * @public
   * @param {Object} element - The element on which the roi is being drawn.
   * @returns {null}
   */
  finishDrawing(element) {
    const evt = { detail: { element } };
    if (this.configuration.currentTool >= 0) {
      this._endDrawing(evt);
    }
  }

  getTextInfo(measurementData, data = null) {
    if (data) {
      const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(data.image);
      const text = getTextBoxText(
        measurementData,
        rowPixelSpacing,
        colPixelSpacing
      );
      return text;
    }
    return [];
  }
}

function getTextBoxText(data, rowPixelSpacing, colPixelSpacing) {
  const hasPixelSpacing = rowPixelSpacing && colPixelSpacing;
  const longestDiameterUnit = unitUtils.getLengthUnit(
    hasPixelSpacing,
    data.longestDiameter
  );
  const shortestDiameterUnit = unitUtils.getLengthUnit(
    hasPixelSpacing,
    data.shortestDiameter
  );

  const isConvertLongestDiameterToMicrometer = unitUtils.shouldConvertToMicrometer(
    data.longestDiameter
  );
  const isConvertShortestDiameterToMicrometer = unitUtils.shouldConvertToMicrometer(
    data.shortestDiameter
  );
  const mmToMicronConversionFactor = 1000;

  const longestDiameter =
    isConvertLongestDiameterToMicrometer && data.longestDiameter
      ? data.longestDiameter * mmToMicronConversionFactor
      : parseFloat(data.longestDiameter);
  const shortestDiameter =
    isConvertShortestDiameterToMicrometer && data.shortestDiameter
      ? data.shortestDiameter * mmToMicronConversionFactor
      : parseFloat(data.shortestDiameter);

  const longestDiameterFormattedValue = longestDiameter
    ? TextBoxContentUtils.common.formatValueToOneDecimalPlace(longestDiameter)
    : 0.0;
  const shortestDiameterFormattedValue = shortestDiameter
    ? TextBoxContentUtils.common.formatValueToOneDecimalPlace(shortestDiameter)
    : 0.0;

  const lengthText = ` L : ${longestDiameterFormattedValue} ${longestDiameterUnit}`;
  const widthText = ` W : ${shortestDiameterFormattedValue} ${shortestDiameterUnit}`;

  const { labels } = data;

  if (labels && Array.isArray(labels)) {
    return [...labels, lengthText, widthText];
  }

  return [lengthText, widthText];
}

function isEnabledAutomatedLabelling() {
  const store = window.store.getState();
  const infusions = store.infusions;
  const projectConfig = store.flywheel.projectConfig;
  let isEnabledAutomatedLabelling = true;
  if (projectConfig && projectConfig.hasOwnProperty('automatedLabelling')) {
    isEnabledAutomatedLabelling = projectConfig.automatedLabelling;
  }
  return (
    isEnabledAutomatedLabelling &&
    infusions.availableLabels &&
    infusions.availableLabels.length === 1
  );
}

const getHandle = (x, y, index, extraAttributes = {}) =>
  Object.assign(
    {
      x,
      y,
      index,
      drawnIndependently: false,
      allowedOutsideImage: false,
      highlight: true,
      active: false,
    },
    extraAttributes
  );
