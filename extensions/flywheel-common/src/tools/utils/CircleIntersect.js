import cornerstoneMath from 'cornerstone-math';
import cornerstone from 'cornerstone-core';

const getDistance = cornerstoneMath.point.distance;
const pixelToCanvas = cornerstone.pixelToCanvas;
const canvasToPixel = cornerstone.canvasToPixel;

/**
 * Is lineSegment inside circle
 * @param {[Object]} points points to check in image coordinates.
 * @param {Object} center center of the circle in image coordinates.
 * @param {const} radius radius of the circle  in canvas units.
 * @param {boolean} openFreehandTool true if tool type is OpenFreeHandRoi else false
 * @param {const} element DOM element in which circle drawn
 * @return {boolean} true if inside else false
 */

export function isLineSegmentInsideCircle(
  points,
  center,
  radius,
  element,
  openFreehandTool
) {
  const length = points.length;
  if (length < 2) {
    return false;
  }
  function isInside(lineSegment) {
    const start = pixelToCanvas(element, lineSegment.start);
    const end = pixelToCanvas(element, lineSegment.end);
    const lineSegment_ = {
      start,
      end,
    };
    const center_ = pixelToCanvas(element, center);
    const distance = cornerstoneMath.lineSegment.distanceToPoint(
      lineSegment_,
      center_
    );
    return distance <= radius;
  }

  let overlap = points.some((point, i) => {
    let lineSegment;
    if (openFreehandTool) {
      if (i < length - 1) {
        lineSegment = {
          start: { x: point.x, y: point.y },
          end: {
            x: points[i + 1].x,
            y: points[i + 1].y,
          },
        };
        return isInside(lineSegment);
      }
    } else {
      lineSegment = {
        start: { x: point.x, y: point.y },
        end: {
          x: points[(i + 1) % length].x,
          y: points[(i + 1) % length].y,
        },
      };
      return isInside(lineSegment);
    }
  });
  return overlap;
}

/**
 * Is point inside circle
 * @param {object} point point to check in image coordinates.
 * @param {object} center center of the circle in image coordinates.
 * @param {const} radius radius of the target circle  in canvas units.
 * @param {const} element DOM element in which circle drawn
 * @return {boolean} true if inside else false
 */
export function isPointInsideCircle(point, center, radius, element) {
  const pointToCheck = pixelToCanvas(element, point);
  const centerOfCircle = pixelToCanvas(element, center);
  const distance = getDistance(pointToCheck, centerOfCircle);
  return distance <= radius;
}

/**
 * Is circle intersecting circle
 * @param {const} targetRadius radius of the target circle in canvas units.
 * @param {const} circleRadius radius of the circle  in canvas units.
 * @param {Object} targetCenter center of the target circle in image coordinates.
 * @param {Object} circleCenterPoint center of the circle in image coordinates.
 * @param {const} element DOM element in which circle drawn
 * @return {boolean} true if inside else false
 */
export function isCircleIntersectCircle(
  targetRadius,
  circleRadius,
  targetCenter,
  circleCenterPoint,
  element
) {
  const pointToCheck = pixelToCanvas(element, targetCenter);
  const centerOfCircle = pixelToCanvas(element, circleCenterPoint);
  const distance = getDistance(pointToCheck, centerOfCircle);
  return distance <= circleRadius + targetRadius;
}

/**
 *
 * @param {Object} start circle start point in image coordinates.
 * @param {Object} end circle end point in image coordinates.
 * @param {const} element DOM Element in which circle drawn.
 * @return {Object} radius of circle in canvas units, centre in image coordinates and
 * array of end points of the polygon along the circle circumference in image coordinates.
 */
export function getCircleData(start, end, element) {
  const center = pixelToCanvas(element, start);
  const pointOnCircumference = pixelToCanvas(element, end);
  const radius = getDistance(center, pointOnCircumference);
  const linePoints = getPointsOnCircleCircumference(center, radius, element);
  return {
    radius,
    center: start,
    linePoints,
  };
}

/**
 *
 * @param {Object} centerOnCanvas centre of circle in canvas units.
 * @param {Object} radiusOnCanvas radius of circle in canvas units.
 * @param {const} element DOM Element in which circle drawn.
 * @param {const} angleResolution angle between two adjacent points along the circle circumference in degrees.
 * @return {Array} Array of end points of the polygon along the circle circumference in image coordinates.
 */
export function getPointsOnCircleCircumference(
  centerOnCanvas,
  radiusOnCanvas,
  element,
  angleResolution = 5
) {
  let linePoints = [];

  for (let i = 0; i <= 360; i = i + angleResolution) {
    const angleInRadiansFromXAxis = (i * Math.PI) / 180;
    const point = {
      x: radiusOnCanvas * Math.cos(angleInRadiansFromXAxis) + centerOnCanvas.x,
      y: radiusOnCanvas * Math.sin(angleInRadiansFromXAxis) + centerOnCanvas.y,
    };
    const pointPixel = canvasToPixel(element, point);
    linePoints.push(pointPixel);
  }

  return linePoints;
}
