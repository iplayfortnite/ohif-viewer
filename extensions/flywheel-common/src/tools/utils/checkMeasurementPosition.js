import { getEllipseData } from './EllipseIntersect.js';
import { getCircleData } from './CircleIntersect.js';
import { getFreehandData } from './FreehandIntersect.js';
import { compareContourPosition } from './openFreehandIntersect';
import { checkLinesIntersect } from './LineIntersect.js';
import {
  isBoundingBoxLeftRight,
  isBoundingBoxTopBottom,
  getRectangleData,
} from './RectangleIntersect.js';
import roiTools from './roiTools';

/**
 * Compare to find the source measurement inside the target measurement
 * @param {Object} srcMeasurement source measurement
 * @param {string} srcToolType source measurement tool type
 * @param {Object} tgtMeasurement target measurement
 * @param {string} tgtToolType target tool type
 * @param {Object} imageDimension image dimension
 * @param {boolean} isHorizontal to specify which direction to be validated ("up/down" = true, "left/right" = false)
 * @param {const} element DOM element
 * @return {boolean} true if succeed the comparison of leftRight/topBottom based on isHorizontal
 */
function compareForPosition(
  srcMeasurement,
  srcToolType,
  tgtMeasurement,
  tgtToolType,
  imageDimension,
  isHorizontal,
  element
) {
  let status = false;
  const pts1 = getPoints(srcMeasurement, srcToolType, imageDimension, element);
  const pts2 = getPoints(tgtMeasurement, tgtToolType, imageDimension, element);
  if (tgtToolType === 'OpenFreehandRoi' || srcToolType === 'OpenFreehandRoi') {
    const openFreehandPoints = tgtToolType === 'OpenFreehandRoi' ? pts2 : pts1;
    const refMeasurementPoints =
      tgtToolType !== 'OpenFreehandRoi' ? pts2 : pts1;

    status = !compareContourPosition(
      openFreehandPoints,
      refMeasurementPoints,
      imageDimension,
      isHorizontal
    );
    if (status) {
      status = !checkLinesIntersect(openFreehandPoints, refMeasurementPoints);
    }
  } else {
    if (isHorizontal) {
      status = isBoundingBoxTopBottom(pts1, pts2);
    } else {
      status = isBoundingBoxLeftRight(pts1, pts2);
    }
  }
  return status;
}

/**
 * To get measurement points
 * @param {Object} measurement
 * @param {String} toolType
 * @param {Object} imageDimension
 * @param {const} element DOM element
 * @returns {Array} returns array of points
 */
function getPoints(measurement, toolType, imageDimension, element) {
  let points = [];
  switch (toolType) {
    case roiTools.RectangleRoi: {
      const data = getRectangleData(
        measurement.handles.start,
        measurement.handles.end,
        imageDimension,
        measurement.handles.initialRotation
      );
      points = data.points;
      break;
    }
    case roiTools.EllipticalRoi: {
      const data = getEllipseData(
        measurement.handles.start,
        measurement.handles.end,
        imageDimension,
        measurement.handles.initialRotation
      );
      points = data.cornerPoints;
      break;
    }
    case roiTools.FreehandRoi: {
      const data = getFreehandData(measurement.handles.points, true);
      points = data.points;
      break;
    }
    case roiTools.OpenFreehandRoi: {
      const lines = measurement.handles.points;
      for (let i = 0; i < lines.length; i++) {
        const data = getFreehandData(lines[i], false);
        points.push(data.points);
      }
      break;
    }
    case roiTools.CircleRoi: {
      const data = getCircleData(
        measurement.handles.start,
        measurement.handles.end,
        element
      );
      points = data.linePoints;
      break;
    }
  }
  return points;
}

export default { compareForPosition };
