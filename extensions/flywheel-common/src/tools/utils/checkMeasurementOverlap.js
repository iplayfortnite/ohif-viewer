import { checkLinesIntersect } from './LineIntersect.js';
import { isPointInsideEllipse, getEllipseData } from './EllipseIntersect.js';
import { isPointInsideFreehand, getFreehandData } from './FreehandIntersect.js';
import {
  isPointInsideRect,
  getRectangleData,
  isBoundingBoxIntersect,
} from './RectangleIntersect.js';
import roiTools from './roiTools';
import {
  isCircleIntersectCircle,
  isLineSegmentInsideCircle,
  getCircleData,
} from './CircleIntersect.js';

/**
 * To check the measurement is overlapping with any of the other measurements in image
 * @param {string} srcToolType source measurement tool type
 * @param {Object} srcMeasurement source measurement
 * @param {string} imageId image id
 * @param {const} element DOM element
 * @return {[Object]} collection of overlapping measurements
 */
export function isOverlapWithOtherROI(
  srcToolType,
  srcMeasurement,
  imageId,
  element
) {
  if (!imageId) {
    return [];
  }
  const overlappedMeasurements = [];
  if (!roiTools.hasOwnProperty(srcToolType)) {
    return overlappedMeasurements;
  }
  const toolState = cornerstoneTools.globalImageIdSpecificToolStateManager.saveToolState();
  const imagePlane = cornerstone.metaData.get('imagePlaneModule', imageId);
  const imageDimension = { x: imagePlane.columns, y: imagePlane.rows };

  toolState[imageId] &&
    Object.keys(toolState[imageId]).forEach(toolType => {
      if (roiTools.hasOwnProperty(toolType)) {
        const data = toolState[imageId][toolType].data;
        data.forEach(measurement => {
          if (srcMeasurement._id !== measurement._id && measurement.toolType) {
            if (
              compareForOverlapping(
                srcMeasurement,
                srcToolType,
                measurement,
                toolType,
                imageDimension,
                element
              )
            ) {
              overlappedMeasurements.push(measurement);
            }
          }
        });
      }
    });
  return overlappedMeasurements;
}

/**
 * Compare 2 measurements for overlapping
 * @param {Object} measurement1 first measurement
 * @param {string} toolType1 first measurement tool type
 * @param {Object} measurement2 second measurement
 * @param {string} toolType2 second measurement tool type
 * @param {Object} imageDimension image dimension
 * @param {const} element DOM element
 * @return {boolean} true if overlapping else false
 */
export function compareForOverlapping(
  measurement1,
  toolType1,
  measurement2,
  toolType2,
  imageDimension,
  element
) {
  let isOverlapping = false;
  if (
    toolType1 === roiTools.RectangleRoi ||
    toolType2 === roiTools.RectangleRoi
  ) {
    // If one of the measurement is rectangle
    const measurements =
      toolType1 === roiTools.RectangleRoi
        ? {
            rectangle: measurement1,
            target: measurement2,
            targetToolType: toolType2,
          }
        : {
            rectangle: measurement2,
            target: measurement1,
            targetToolType: toolType1,
          };
    isOverlapping = compareWithRectangle(
      measurements.rectangle,
      measurements.target,
      measurements.targetToolType,
      imageDimension,
      element
    );
  } else if (
    toolType1 === roiTools.EllipticalRoi ||
    toolType2 === roiTools.EllipticalRoi
  ) {
    // If one of the measurement is ellipse
    const measurements =
      toolType1 === roiTools.EllipticalRoi
        ? {
            ellipse: measurement1,
            target: measurement2,
            targetToolType: toolType2,
          }
        : {
            ellipse: measurement2,
            target: measurement1,
            targetToolType: toolType1,
          };
    isOverlapping = compareWithEllipse(
      measurements.ellipse,
      measurements.target,
      measurements.targetToolType,
      imageDimension,
      element
    );
  } else if (
    toolType1 === roiTools.CircleRoi ||
    toolType2 === roiTools.CircleRoi
  ) {
    // If one of the measurement is circle
    const measurements =
      toolType1 === roiTools.CircleRoi
        ? {
            circle: measurement1,
            target: measurement2,
            targetToolType: toolType2,
          }
        : {
            circle: measurement2,
            target: measurement1,
            targetToolType: toolType1,
          };
    isOverlapping = compareWithCircle(
      measurements.circle,
      measurements.target,
      measurements.targetToolType,
      imageDimension,
      element
    );
  } else {
    // If both are freehand types
    isOverlapping = compareWithFreehand(
      measurement1,
      toolType1,
      measurement2,
      toolType2,
      imageDimension,
      element
    );
  }
  return isOverlapping;
}

/**
 * Compare a rectangle and another measurement for overlapping
 * @param {Object} rectangleMeasurement rectangle measurement
 * @param {Object} targetMeasurement target measurement
 * @param {string} targetToolType target measurement tool type
 * @param {Object} imageDimension image dimension
 * @param {const} element DOM element
 * @return {boolean} true if overlapping else false
 */
function compareWithRectangle(
  rectangleMeasurement,
  targetMeasurement,
  targetToolType,
  imageDimension,
  element
) {
  let isOverlapping = false;
  const rectangleHandles = rectangleMeasurement.handles;
  const targetHandles = targetMeasurement.handles;
  switch (targetToolType) {
    case roiTools.RectangleRoi: {
      const rectangleData1 = getRectangleData(
        rectangleHandles.start,
        rectangleHandles.end,
        imageDimension,
        rectangleHandles.initialRotation
      );
      const rectangleData2 = getRectangleData(
        targetHandles.start,
        targetHandles.end,
        imageDimension,
        targetHandles.initialRotation
      );
      isOverlapping = !!rectangleData1.points.find(point =>
        isPointInsideRect(
          rectangleData2.points,
          point,
          imageDimension,
          targetHandles.initialRotation
        )
      );
      isOverlapping =
        isOverlapping ||
        !!rectangleData2.points.find(point =>
          isPointInsideRect(
            rectangleData1.points,
            point,
            imageDimension,
            rectangleHandles.initialRotation
          )
        );
      if (!isOverlapping) {
        isOverlapping = checkLinesIntersect(
          rectangleData1.linePoints,
          rectangleData2.linePoints
        );
      }
      break;
    }
    case roiTools.EllipticalRoi: {
      const rectangleData = getRectangleData(
        rectangleHandles.start,
        rectangleHandles.end,
        imageDimension,
        rectangleHandles.initialRotation
      );
      const ellipseData = getEllipseData(
        targetHandles.start,
        targetHandles.end,
        imageDimension,
        targetHandles.initialRotation
      );
      isOverlapping = !!ellipseData.points.find(point =>
        isPointInsideRect(
          rectangleData.points,
          point,
          imageDimension,
          rectangleHandles.initialRotation
        )
      );
      isOverlapping =
        isOverlapping ||
        !!rectangleData.points.find(point =>
          isPointInsideEllipse(
            ellipseData.cornerPoints,
            point,
            imageDimension,
            targetHandles.initialRotation
          )
        );
      if (!isOverlapping) {
        isOverlapping = checkLinesIntersect(
          ellipseData.linePoints,
          rectangleData.linePoints
        );
      }
      break;
    }
    case roiTools.FreehandRoi: {
      const rectangleData = getRectangleData(
        rectangleHandles.start,
        rectangleHandles.end,
        imageDimension,
        rectangleHandles.initialRotation
      );
      const freehandData = getFreehandData(targetHandles.points, true);
      isOverlapping = !!freehandData.points.find(point =>
        isPointInsideRect(
          rectangleData.points,
          point,
          imageDimension,
          rectangleHandles.initialRotation
        )
      );
      // Check first the rectangle is intersecting with freehand bounding box to filter out effectively
      let boundBoxOverlapping =
        isOverlapping ||
        !!rectangleData.points.find(point =>
          isPointInsideRect(
            freehandData.boundingBoxPoints,
            point,
            imageDimension,
            0
          )
        );
      if (boundBoxOverlapping) {
        isOverlapping =
          isOverlapping ||
          !!rectangleData.points.find(point =>
            isPointInsideFreehand(freehandData.points, point)
          );
      }
      if (!isOverlapping) {
        isOverlapping = checkLinesIntersect(
          rectangleData.linePoints,
          freehandData.points
        );
      }
      break;
    }
    case roiTools.OpenFreehandRoi: {
      const rectangleData = getRectangleData(
        rectangleHandles.start,
        rectangleHandles.end,
        imageDimension,
        rectangleHandles.initialRotation
      );
      const lines = targetHandles.points;
      for (let i = 0; i < lines.length; i++) {
        const freehandData = getFreehandData(lines[i], false);
        isOverlapping = !!freehandData.points.find(point =>
          isPointInsideRect(
            rectangleData.points,
            point,
            imageDimension,
            rectangleHandles.initialRotation
          )
        );
        if (!isOverlapping) {
          isOverlapping = checkLinesIntersect(
            rectangleData.linePoints,
            freehandData.points
          );
        }
        if (isOverlapping) {
          break;
        }
      }
      break;
    }
    case roiTools.CircleRoi: {
      const rectangleData = getRectangleData(
        rectangleHandles.start,
        rectangleHandles.end,
        imageDimension,
        rectangleHandles.initialRotation
      );
      const circleData = getCircleData(
        targetHandles.start,
        targetHandles.end,
        element
      );
      isOverlapping = !!isPointInsideRect(
        rectangleData.points,
        circleData.center,
        imageDimension,
        rectangleHandles.initialRotation
      );
      isOverlapping =
        isOverlapping ||
        !!isLineSegmentInsideCircle(
          rectangleData.points,
          circleData.center,
          circleData.radius,
          element,
          false
        );
      break;
    }

    default:
      break;
  }
  return isOverlapping;
}

/**
 * Compare an ellipse and another measurement for overlapping
 * @param {Object} ellipseMeasurement ellipse measurement
 * @param {Object} targetMeasurement target measurement
 * @param {string} targetToolType target measurement tool type
 * @param {Object} imageDimension image dimension
 * @param {const} element DOM element
 * @return {boolean} true if overlapping else false
 */
function compareWithEllipse(
  ellipseMeasurement,
  targetMeasurement,
  targetToolType,
  imageDimension,
  element
) {
  let isOverlapping = false;
  const ellipseHandles = ellipseMeasurement.handles;
  const targetHandles = targetMeasurement.handles;
  switch (targetToolType) {
    case roiTools.EllipticalRoi: {
      const ellipseData1 = getEllipseData(
        ellipseHandles.start,
        ellipseHandles.end,
        imageDimension,
        ellipseHandles.initialRotation
      );
      const ellipseData2 = getEllipseData(
        targetHandles.start,
        targetHandles.end,
        imageDimension,
        targetHandles.initialRotation
      );
      isOverlapping = !!ellipseData1.points.find(point =>
        isPointInsideEllipse(
          ellipseData2.cornerPoints,
          point,
          imageDimension,
          targetHandles.initialRotation
        )
      );
      isOverlapping =
        isOverlapping ||
        !!ellipseData2.points.find(point =>
          isPointInsideEllipse(
            ellipseData1.cornerPoints,
            point,
            imageDimension,
            ellipseHandles.initialRotation
          )
        );
      if (!isOverlapping) {
        isOverlapping = checkLinesIntersect(
          ellipseData1.linePoints,
          ellipseData2.linePoints
        );
      }
      break;
    }
    case roiTools.FreehandRoi: {
      const ellipseData = getEllipseData(
        ellipseHandles.start,
        ellipseHandles.end,
        imageDimension,
        ellipseHandles.initialRotation
      );
      const freehandData = getFreehandData(targetHandles.points, true);
      isOverlapping = !!freehandData.points.find(point =>
        isPointInsideEllipse(
          ellipseData.cornerPoints,
          point,
          imageDimension,
          ellipseHandles.initialRotation
        )
      );
      // Check first the ellipse bounding points is intersecting with freehand bounding box to filter out effectively
      let boundBoxOverlapping =
        isOverlapping ||
        !!ellipseData.cornerPoints.find(point =>
          isPointInsideRect(
            freehandData.boundingBoxPoints,
            point,
            imageDimension,
            0
          )
        );
      if (boundBoxOverlapping) {
        isOverlapping =
          isOverlapping ||
          !!ellipseData.points.find(point =>
            isPointInsideFreehand(freehandData.points, point)
          );
      }
      if (!isOverlapping) {
        isOverlapping = checkLinesIntersect(
          ellipseData.linePoints,
          freehandData.points
        );
      }
      break;
    }
    case roiTools.OpenFreehandRoi: {
      const ellipseData = getEllipseData(
        ellipseHandles.start,
        ellipseHandles.end,
        imageDimension,
        ellipseHandles.initialRotation
      );
      const lines = targetHandles.points;
      for (let i = 0; i < lines.length; i++) {
        const freehandData = getFreehandData(lines[i], false);
        isOverlapping = !!freehandData.points.find(point =>
          isPointInsideEllipse(
            ellipseData.cornerPoints,
            point,
            imageDimension,
            ellipseHandles.initialRotation
          )
        );
        if (!isOverlapping) {
          isOverlapping = checkLinesIntersect(
            ellipseData.linePoints,
            freehandData.points
          );
        }
        if (isOverlapping) {
          break;
        }
      }
      break;
    }
    case roiTools.CircleRoi: {
      const ellipseData = getEllipseData(
        ellipseHandles.start,
        ellipseHandles.end,
        imageDimension,
        ellipseHandles.initialRotation
      );
      const circleData = getCircleData(
        targetHandles.start,
        targetHandles.end,
        element
      );
      isOverlapping = !!isPointInsideEllipse(
        ellipseData.cornerPoints,
        circleData.center,
        imageDimension,
        ellipseHandles.initialRotation
      );
      isOverlapping =
        isOverlapping ||
        !!isLineSegmentInsideCircle(
          ellipseData.points,
          circleData.center,
          circleData.radius,
          element,
          false
        );

      break;
    }
    default:
      break;
  }
  return isOverlapping;
}

/**
 * Compare a circle and another measurement for overlapping
 * @param {Object} circleMeasurement circle measurement
 * @param {Object} targetMeasurement target measurement
 * @param {string} targetToolType target measurement tool type
 * @param {Object} imageDimension image dimension
 * @param {const} element DOM element
 * @return {boolean} true if overlapping else false
 */

function compareWithCircle(
  circleMeasurement,
  targetMeasurement,
  targetToolType,
  imageDimension,
  element
) {
  let isOverlapping = false;
  const circleHandles = circleMeasurement.handles;
  const targetHandles = targetMeasurement.handles;
  switch (targetToolType) {
    case roiTools.RectangleRoi: {
      const circleData = getCircleData(
        circleHandles.start,
        circleHandles.end,
        element
      );
      const rectangleData = getRectangleData(
        targetHandles.start,
        targetHandles.end,
        imageDimension,
        targetHandles.initialRotation
      );
      isOverlapping = !!isLineSegmentInsideCircle(
        rectangleData.points,
        circleData.center,
        circleData.radius,
        element,
        false
      );

      isOverlapping =
        isOverlapping ||
        !!isPointInsideRect(
          rectangleData.points,
          circleData.center,
          imageDimension,
          rectangleHandles.initialRotation
        );
      break;
    }
    case roiTools.EllipticalRoi: {
      const circleData = getCircleData(
        circleHandles.start,
        circleHandles.end,
        element
      );
      const ellipseData = getEllipseData(
        targetHandles.start,
        targetHandles.end,
        imageDimension,
        targetHandles.initialRotation
      );
      isOverlapping = !!isLineSegmentInsideCircle(
        ellipseData.points,
        circleData.center,
        circleData.radius,
        element,
        false
      );

      isOverlapping =
        isOverlapping ||
        !!isPointInsideEllipse(
          ellipseData.cornerPoints,
          circleData.center,
          imageDimension,
          ellipseHandles.initialRotation
        );
      break;
    }
    case roiTools.FreehandRoi: {
      const circleData = getCircleData(
        circleHandles.start,
        circleHandles.end,
        element
      );
      const freehandData = getFreehandData(targetHandles.points, true);
      isOverlapping = !!isLineSegmentInsideCircle(
        freehandData.points,
        circleData.center,
        circleData.radius,
        element,
        false
      );
      let boundBoxOverlapping =
        isOverlapping ||
        !!isPointInsideRect(
          freehandData.boundingBoxPoints,
          circleData.center,
          imageDimension,
          0
        );
      if (boundBoxOverlapping) {
        isOverlapping =
          isOverlapping ||
          !!isPointInsideFreehand(freehandData.points, circleData.center);
      }
      break;
    }
    case roiTools.OpenFreehandRoi: {
      const circleData = getCircleData(
        circleHandles.start,
        circleHandles.end,
        element
      );
      const lines = targetHandles.points;
      for (let i = 0; i < lines.length; i++) {
        const freehandData = getFreehandData(lines[i], false);
        isOverlapping = !!isLineSegmentInsideCircle(
          freehandData.points,
          circleData.center,
          circleData.radius,
          element,
          true
        );
        if (isOverlapping) {
          break;
        }
      }
      break;
    }
    case roiTools.CircleRoi: {
      const circleData1 = getCircleData(
        circleHandles.start,
        circleHandles.end,
        element
      );
      const circleData2 = getCircleData(
        targetHandles.start,
        targetHandles.end,
        element
      );
      isOverlapping = !!isCircleIntersectCircle(
        circleData2.radius,
        circleData1.radius,
        circleData2.center,
        circleData1.center,
        element
      );

      break;
    }
    default:
      break;
  }
  return isOverlapping;
}

/**
 * Compare 2 freehand measurements(open/closed) for overlapping
 * @param {Object} freehandMeasurement1 first freehand measurement
 * @param {string} freehandToolType1 first freehand measurement tool type(open/closed)
 * @param {Object} freehandMeasurement2 second freehand measurement
 * @param {string} freehandToolType2 second freehand measurement tool type(open/closed)
 * @param {Object} imageDimension image dimension
 * @return {boolean} true if overlapping else false
 */
function compareWithFreehand(
  freehandMeasurement1,
  freehandToolType1,
  freehandMeasurement2,
  freehandToolType2,
  imageDimension
) {
  let isOverlapping = false;
  const isSameType = freehandToolType1 === freehandToolType2;
  const isOpenFreehand1 = freehandToolType1 === roiTools.OpenFreehandRoi;
  const isOpenFreehand2 = freehandToolType2 === roiTools.OpenFreehandRoi;
  let freehandData1 = [];
  if (isOpenFreehand1) {
    freehandMeasurement1.handles.points.forEach(line => {
      freehandData1.push(getFreehandData(line, false));
    });
  } else {
    freehandData1 = getFreehandData(
      freehandMeasurement1.handles.points,
      freehandToolType1 === roiTools.FreehandRoi
    );
  }
  let freehandData2 = [];
  if (isOpenFreehand2) {
    freehandMeasurement2.handles.points.forEach(line => {
      freehandData2.push(getFreehandData(line, false));
    });
  } else {
    freehandData2 = getFreehandData(
      freehandMeasurement2.handles.points,
      freehandToolType2 === roiTools.FreehandRoi
    );
  }
  // Check first the two freehand bounding box's intersecting each other to filter out effectively
  let boundBoxOverlapping = false;
  if (isSameType && isOpenFreehand1 && isOpenFreehand2) {
    for (let i = 0; i < freehandData1.length; i++) {
      for (let j = 0; j < freehandData2.length; j++) {
        boundBoxOverlapping = isBoundingBoxIntersect(
          freehandData1[i].boundingBoxPoints,
          freehandData2[j].boundingBoxPoints,
          imageDimension
        );
        if (boundBoxOverlapping) {
          break;
        }
      }
      if (boundBoxOverlapping) {
        break;
      }
    }
  } else if (isOpenFreehand1 || isOpenFreehand2) {
    const refData = !isOpenFreehand1 ? freehandData1 : freehandData2;
    const targetData = isOpenFreehand1 ? freehandData1 : freehandData2;
    for (let i = 0; i < targetData.length; i++) {
      boundBoxOverlapping = isBoundingBoxIntersect(
        targetData[i].boundingBoxPoints,
        refData.boundingBoxPoints,
        imageDimension
      );
      if (boundBoxOverlapping) {
        break;
      }
    }
  } else {
    boundBoxOverlapping = isBoundingBoxIntersect(
      freehandData1.boundingBoxPoints,
      freehandData2.boundingBoxPoints,
      imageDimension
    );
  }
  if (!boundBoxOverlapping) {
    return isOverlapping;
  }
  if (isSameType && freehandToolType1 === roiTools.FreehandRoi) {
    // If both are closed Freehand ROI's
    isOverlapping = !!freehandData1.points.find(point =>
      isPointInsideFreehand(freehandData2.points, point)
    );
    isOverlapping =
      isOverlapping ||
      !!freehandData2.points.find(point =>
        isPointInsideFreehand(freehandData1.points, point)
      );
  } else if (!isSameType && !isOpenFreehand1 && !isOpenFreehand2) {
    if (freehandToolType1 === roiTools.FreehandRoi) {
      isOverlapping = !!freehandData2.points.find(point =>
        isPointInsideFreehand(freehandData1.points, point)
      );
    } else {
      isOverlapping = !!freehandData1.points.find(point =>
        isPointInsideFreehand(freehandData2.points, point)
      );
    }
  }

  if (!isOverlapping && isOpenFreehand1 && isOpenFreehand2) {
    for (let i = 0; i < freehandData1.length; i++) {
      for (let j = 0; j < freehandData2.length; j++) {
        isOverlapping = checkLinesIntersect(
          freehandData1[i].points,
          freehandData2[j].points
        );
        if (isOverlapping) {
          return isOverlapping;
        }
      }
    }
  } else if (!isOverlapping && (isOpenFreehand1 || isOpenFreehand2)) {
    const refData = !isOpenFreehand1 ? freehandData1 : freehandData2;
    const targetData = isOpenFreehand1 ? freehandData1 : freehandData2;
    for (let i = 0; i < targetData.length; i++) {
      isOverlapping = checkLinesIntersect(targetData[i].points, refData.points);
      if (isOverlapping) {
        return isOverlapping;
      }
    }
  } else if (!isOverlapping && !isOpenFreehand1 && !isOpenFreehand2) {
    isOverlapping = checkLinesIntersect(
      freehandData1.points,
      freehandData2.points
    );
  }
  return isOverlapping;
}

export default { isOverlapWithOtherROI, roiTools, compareForOverlapping };
