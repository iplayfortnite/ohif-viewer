import { isPointInsideEllipse, getEllipseData } from './EllipseIntersect.js';
import { isPointInsideFreehand, getFreehandData } from './FreehandIntersect.js';
import { isPointInsideRect, getRectangleData } from './RectangleIntersect.js';
import { isPointInsideCircle, getCircleData } from './CircleIntersect.js';
import roiTools from './roiTools';

/**
 * Compare to find the source measurement inside the target measurement
 * @param {Object} srcMeasurement source measurement
 * @param {string} srcToolType source measurement tool type
 * @param {Object} tgtMeasurement target measurement
 * @param {string} tgtToolType target tool type
 * @param {Object} imageDimension image dimension
 * @param {boolean} checkInside if true check completely inside else completely outside
 * @param {const} element DOM element
 * @return {boolean} true if inside/outside else false based on checkInside
 */
function compareForInsideOutside(
  srcMeasurement,
  srcToolType,
  tgtMeasurement,
  tgtToolType,
  imageDimension,
  checkInside,
  element
) {
  let status = false;
  if (tgtToolType === roiTools.RectangleRoi) {
    status = compareWithRectangle(
      tgtMeasurement,
      srcMeasurement,
      srcToolType,
      imageDimension,
      checkInside,
      element
    );
  } else if (tgtToolType === roiTools.EllipticalRoi) {
    status = compareWithEllipse(
      tgtMeasurement,
      srcMeasurement,
      srcToolType,
      imageDimension,
      checkInside,
      element
    );
  } else if (tgtToolType === roiTools.CircleRoi) {
    status = compareWithCircle(
      tgtMeasurement,
      srcMeasurement,
      srcToolType,
      imageDimension,
      checkInside,
      element
    );
  } else {
    status = compareWithFreehand(
      tgtMeasurement,
      tgtToolType,
      srcMeasurement,
      srcToolType,
      imageDimension,
      checkInside,
      element
    );
  }
  return status;
}

/**
 * Compare the target measurement for inside rectangle or not
 * @param {Object} rectangleMeasurement rectangle measurement
 * @param {Object} targetMeasurement target measurement
 * @param {string} targetToolType target measurement tool type
 * @param {Object} imageDimension image dimension
 * @param {boolean} checkInside if true check completely inside else completely outside
 * @param {const} element DOM element
 * @return {boolean} true if inside else false
 */
function compareWithRectangle(
  rectangleMeasurement,
  targetMeasurement,
  targetToolType,
  imageDimension,
  checkInside,
  element
) {
  let status = false;
  const rectangleHandles = rectangleMeasurement.handles;
  const rectangleData = getRectangleData(
    rectangleHandles.start,
    rectangleHandles.end,
    imageDimension,
    rectangleHandles.initialRotation
  );
  const targetPoints = getTargetPoints(
    targetMeasurement,
    targetToolType,
    imageDimension,
    element
  );
  if (checkInside) {
    if (targetToolType === roiTools.OpenFreehandRoi) {
      status = !targetPoints.find(line =>
        line.find(
          point =>
            !isPointInsideRect(
              rectangleData.points,
              point,
              imageDimension,
              rectangleHandles.initialRotation
            )
        )
      );
    } else {
      status = !targetPoints.find(
        point =>
          !isPointInsideRect(
            rectangleData.points,
            point,
            imageDimension,
            rectangleHandles.initialRotation
          )
      );
    }
  } else {
    if (targetToolType === roiTools.OpenFreehandRoi) {
      status = !targetPoints.find(line =>
        line.find(point =>
          isPointInsideRect(
            rectangleData.points,
            point,
            imageDimension,
            rectangleHandles.initialRotation
          )
        )
      );
    } else {
      status = !targetPoints.find(point =>
        isPointInsideRect(
          rectangleData.points,
          point,
          imageDimension,
          rectangleHandles.initialRotation
        )
      );
    }
  }
  return status;
}

/**
 * Compare the target measurement for inside ellipse or not
 * @param {Object} ellipseMeasurement ellipse measurement
 * @param {Object} targetMeasurement target measurement
 * @param {string} targetToolType target measurement tool type
 * @param {Object} imageDimension image dimension
 * @param {boolean} checkInside if true check completely inside else completely outside
 * @param {const} element DOM element
 * @return {boolean} true if inside else false
 */
function compareWithEllipse(
  ellipseMeasurement,
  targetMeasurement,
  targetToolType,
  imageDimension,
  checkInside,
  element
) {
  let status = false;
  const ellipseHandles = ellipseMeasurement.handles;
  const ellipseData = getEllipseData(
    ellipseHandles.start,
    ellipseHandles.end,
    imageDimension,
    ellipseHandles.initialRotation
  );
  const targetPoints = getTargetPoints(
    targetMeasurement,
    targetToolType,
    imageDimension,
    element
  );
  if (checkInside) {
    if (targetToolType === roiTools.OpenFreehandRoi) {
      status = !targetPoints.find(line =>
        line.find(
          point =>
            !isPointInsideEllipse(
              ellipseData.cornerPoints,
              point,
              imageDimension,
              ellipseHandles.initialRotation
            )
        )
      );
    } else {
      status = !targetPoints.find(
        point =>
          !isPointInsideEllipse(
            ellipseData.cornerPoints,
            point,
            imageDimension,
            ellipseHandles.initialRotation
          )
      );
    }
  } else {
    if (targetToolType === roiTools.OpenFreehandRoi) {
      status = !targetPoints.find(line =>
        line.find(point =>
          isPointInsideEllipse(
            ellipseData.cornerPoints,
            point,
            imageDimension,
            ellipseHandles.initialRotation
          )
        )
      );
    } else {
      status = !targetPoints.find(point =>
        isPointInsideEllipse(
          ellipseData.cornerPoints,
          point,
          imageDimension,
          ellipseHandles.initialRotation
        )
      );
    }
  }
  return status;
}
/**
 * Compare the target measurement for inside circle or not
 * @param {Object} circleMeasurement circle measurement
 * @param {Object} targetMeasurement target measurement
 * @param {string} targetToolType target measurement tool type
 * @param {Object} imageDimension image dimension
 * @param {boolean} checkInside if true check completely inside else completely outside
 * @param {const} element DOM element
 * @return {boolean} true if inside else false
 */

function compareWithCircle(
  circleMeasurement,
  targetMeasurement,
  targetToolType,
  imageDimension,
  checkInside,
  element
) {
  let status = false;
  const circleHandles = circleMeasurement.handles;
  const circleData = getCircleData(
    circleHandles.start,
    circleHandles.end,
    element
  );
  const targetPoints = getTargetPoints(
    targetMeasurement,
    targetToolType,
    imageDimension,
    element
  );
  if (checkInside) {
    if (targetToolType === roiTools.OpenFreehandRoi) {
      status = !targetPoints.find(line =>
        line.find(
          point =>
            !isPointInsideCircle(
              point,
              circleData.center,
              circleData.radius,
              element
            )
        )
      );
    } else {
      status = !targetPoints.find(
        point =>
          !isPointInsideCircle(
            point,
            circleData.center,
            circleData.radius,
            element
          )
      );
    }
  } else {
    if (targetToolType === roiTools.OpenFreehandRoi) {
      status = !targetPoints.find(line =>
        line.find(point =>
          isPointInsideCircle(
            point,
            circleData.center,
            circleData.radius,
            element
          )
        )
      );
    } else {
      status = !targetPoints.find(point =>
        isPointInsideCircle(
          point,
          circleData.center,
          circleData.radius,
          element
        )
      );
    }
  }
  return status;
}

/**
 * Compare the target measurement for inside closed freehand or not, if freehand is open then return false
 * @param {Object} freehandMeasurement freehand measurement
 * @param {string} freehandToolType freehand measurement tool type(open/closed)
 * @param {Object} targetMeasurement second measurement
 * @param {string} targetToolType second measurement tool type(open/closed)
 * @param {Object} imageDimension image dimension
 * @param {boolean} checkInside if true check completely inside else completely outside
 * @param {const} element DOM element
 * @return {boolean} true if inside else false
 */
function compareWithFreehand(
  freehandMeasurement,
  freehandToolType,
  targetMeasurement,
  targetToolType,
  imageDimension,
  checkInside,
  element
) {
  let status = false;
  if (freehandToolType !== roiTools.FreehandRoi) {
    // If not closed freehand return inside as false
    return status;
  }
  const freehandData = getFreehandData(
    freehandMeasurement.handles.points,
    true
  );
  const targetPoints = getTargetPoints(
    targetMeasurement,
    targetToolType,
    imageDimension,
    element
  );
  if (checkInside) {
    if (targetToolType === roiTools.OpenFreehandRoi) {
      status = !targetPoints.find(line =>
        line.find(point => !isPointInsideFreehand(freehandData.points, point))
      );
    } else {
      status = !targetPoints.find(
        point => !isPointInsideFreehand(freehandData.points, point)
      );
    }
  } else {
    if (targetToolType === roiTools.OpenFreehandRoi) {
      status = !targetPoints.find(line =>
        line.find(point => isPointInsideFreehand(freehandData.points, point))
      );
    } else {
      status = !targetPoints.find(point =>
        isPointInsideFreehand(freehandData.points, point)
      );
    }
  }
  return status;
}
/**
 * To get the points to compare in target measurement
 * @param {Object} measurement
 * @param {String} toolType
 * @param {Object} imageDimension
 * @returns {Array} returns points collection
 */
function getTargetPoints(measurement, toolType, imageDimension, element) {
  let points = [];
  switch (toolType) {
    case roiTools.RectangleRoi: {
      const data = getRectangleData(
        measurement.handles.start,
        measurement.handles.end,
        imageDimension,
        measurement.handles.initialRotation
      );
      points = data.points;
      break;
    }
    case roiTools.EllipticalRoi: {
      const data = getEllipseData(
        measurement.handles.start,
        measurement.handles.end,
        imageDimension,
        measurement.handles.initialRotation
      );
      points = data.linePoints;
      break;
    }
    case roiTools.FreehandRoi: {
      const data = getFreehandData(measurement.handles.points, true);
      points = data.points;
      break;
    }
    case roiTools.OpenFreehandRoi: {
      const lines = measurement.handles.points;
      for (let i = 0; i < lines.length; i++) {
        const data = getFreehandData(lines[i], false);
        points.push(data.points);
      }
      break;
    }
    case roiTools.CircleRoi: {
      const data = getCircleData(
        measurement.handles.start,
        measurement.handles.end,
        element
      );
      points = data.linePoints;
      break;
    }
  }
  return points;
}
export default { compareForInsideOutside };
