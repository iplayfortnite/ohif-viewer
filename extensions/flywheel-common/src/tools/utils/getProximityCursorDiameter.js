import cornerstoneMath from 'cornerstone-math';
import csTools from 'cornerstone-tools';

const getPixelSpacing = csTools.importInternal('util/getPixelSpacing');

/**
 *
 * @param {Object} evt - event data
 * @param {*} proximityCursorProperties - proximity cursor information
 * @returns number - diameter
 */
function getProximityCursorDiameter(evt, proximityCursorProperties) {
  const eventData = evt.detail;
  const { image } = eventData;

  const coords = { x: 0, y: 0 };

  const physicalRad = proximityCursorProperties?.distance || 0.125; // 125 micrometer
  const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(image);
  const hasPixelSpacing = rowPixelSpacing && colPixelSpacing;
  if (!hasPixelSpacing) {
    return 0;
  }
  const imageRad = physicalRad / colPixelSpacing;

  const coordsInCircumferenceImage = {
    x: coords.x + imageRad,
    y: coords.y,
  };

  const diameter = Math.abs(
    cornerstoneMath.point.distance(coordsInCircumferenceImage, coords)
  );

  return Math.round(diameter);
}

export default getProximityCursorDiameter;
