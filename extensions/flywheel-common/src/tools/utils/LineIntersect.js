/**
 * Check the 2 line points array for interaction.
 * @export @public @method
 * @name checkLinesIntersect
 *
 * @param {Object} srcLinePoints - collection of points with two points represent a line.
 * @param {Object} tgtLinePoints - collection of points with two points represent a line.
 * @returns {boolean} - true if intersect else false
 */
export function checkLinesIntersect(srcLinePoints, tgtLinePoints) {
  let isIntersect = false;
  if (isArrayOfArray(srcLinePoints) || isArrayOfArray(tgtLinePoints)) {
    return checkOpenFreehandLineIntersect(srcLinePoints, tgtLinePoints);
  }
  for (let i = 0; i < srcLinePoints.length - 1; i++) {
    for (let j = 0; j < tgtLinePoints.length - 1; j++) {
      isIntersect = doesIntersect(
        srcLinePoints[i],
        srcLinePoints[i + 1],
        tgtLinePoints[j],
        tgtLinePoints[j + 1]
      );
      if (isIntersect) {
        break;
      }
    }
    if (isIntersect) {
      break;
    }
  }
  return isIntersect;
}

function checkOpenFreehandLineIntersect(srcLinePoints, tgtLinePoints) {
  let isIntersect = false;
  const isOpenFreehand1 = isArrayOfArray(srcLinePoints);
  const isOpenFreehand2 = isArrayOfArray(tgtLinePoints);
  let l = 0;
  let p = 0;
  if (isOpenFreehand1 && isOpenFreehand2) {
    for (l = 0; l < srcLinePoints.length; l++) {
      for (p = 0; p < tgtLinePoints.length; p++) {
        isIntersect = checkLinesIntersect(srcLinePoints[l], tgtLinePoints[p]);
        if (isIntersect) {
          return isIntersect;
        }
      }
    }
  } else if (isOpenFreehand1 || isOpenFreehand2) {
    const refMeasurements = !isOpenFreehand1 ? srcLinePoints : tgtLinePoints;
    const openFreehandMeasure = isOpenFreehand1 ? srcLinePoints : tgtLinePoints;
    for (l = 0; l < openFreehandMeasure.length; l++) {
      isIntersect = checkLinesIntersect(
        openFreehandMeasure[l],
        refMeasurements
      );
      if (isIntersect) {
        return isIntersect;
      }
    }
  }
  return isIntersect;
}

/**
 * To check the points are array of array format
 * @param {Array} points
 * @returns {Boolean}
 */
function isArrayOfArray(points) {
  if (points?.length && Array.isArray(points[0])) {
    return true;
  }
  return false;
}

/**
 * Checks whether the line (p1,q1) intersects the line (p2,q2) via an orientation algorithm.
 * @export @public @method
 * @function doesIntersect
 *
 * @param {Object} p1 Coordinates of the start of the line 1.
 * @param {Object} q1 Coordinates of the end of the line 1.
 * @param {Object} p2 Coordinates of the start of the line 2.
 * @param {Object} q2 Coordinates of the end of the line 2.
 * @returns {boolean} Whether lines (p1,q1) and (p2,q2) intersect.
 */
export function doesIntersect(p1, q1, p2, q2) {
  let result = false;

  const orient = [
    orientation(p1, q1, p2),
    orientation(p1, q1, q2),
    orientation(p2, q2, p1),
    orientation(p2, q2, q1),
  ];

  // General Case
  if (orient[0] !== orient[1] && orient[2] !== orient[3]) {
    return true;
  }

  // Special Cases
  if (orient[0] === 0 && onSegment(p1, p2, q1)) {
    // If p1, q1 and p2 are colinear and p2 lies on segment p1q1
    result = true;
  } else if (orient[1] === 0 && onSegment(p1, q2, q1)) {
    // If p1, q1 and p2 are colinear and q2 lies on segment p1q1
    result = true;
  } else if (orient[2] === 0 && onSegment(p2, p1, q2)) {
    // If p2, q2 and p1 are colinear and p1 lies on segment p2q2
    result = true;
  } else if (orient[3] === 0 && onSegment(p2, q1, q2)) {
    // If p2, q2 and q1 are colinear and q1 lies on segment p2q2
    result = true;
  }

  return result;
}

/**
 * Checks if point q lines on the segment (p,r).
 * @private
 * @function onSegment
 *
 * @param {Object} p Point p.
 * @param {Object} q Point q.
 * @param {Object} r Point r.
 * @returns {boolean} - If q lies on line segment (p,r).
 */
function onSegment(p, q, r) {
  if (
    q.x <= Math.max(p.x, r.x) &&
    q.x >= Math.min(p.x, r.x) &&
    q.y <= Math.max(p.y, r.y) &&
    q.y >= Math.min(p.y, r.y)
  ) {
    return true;
  }

  return false;
}

/**
 * Checks the orientation of 3 points.
 * @private
 * @function orientation
 *
 * @param {Object} p First point.
 * @param {Object} q Second point.
 * @param {Object} r Third point.
 * @returns {number} - 0: Colinear, 1: Clockwise, 2: Anticlockwise
 */
function orientation(p, q, r) {
  const orientationValue =
    (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);

  if (orientationValue === 0) {
    return 0; // Colinear
  }

  return orientationValue > 0 ? 1 : 2;
}
