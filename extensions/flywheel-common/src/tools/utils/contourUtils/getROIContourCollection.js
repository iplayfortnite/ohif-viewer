import { getToolState } from 'cornerstone-tools';
import getROIContourData from './getROIContourData.js';

/**
 * getROIContourCollection - Generates a collection of the 2D
 * polygons in difference slices that make up the ROIContour.
 *
 * @param  {Object} element  cornerstone element.
 * @param  {Object} filterParams {key: propertyName to compare, value: value of property}.
 * @param  {boolean} onlyContourImage if true include contour existing images only.
 * @return {object[]}           The list of contours in the stack.
 */

export default function getROIContourCollection(
  element,
  filterParams,
  onlyContourImage = false
) {
  const stackToolState = getToolState(element, 'stack');
  const roiImageContours = getROIContourData(
    stackToolState.data[0].imageIds,
    filterParams,
    true
  );
  const referenceContours = [];
  (roiImageContours || []).forEach(contoursOnSlice => {
    (contoursOnSlice.contours || []).forEach(contour => {
      referenceContours.push(contour);
    });
  });

  return referenceContours;
}
