import csTools from 'cornerstone-tools';
import * as TextBoxContentUtils from '../../../textBoxContent';

const uuidv4 = csTools.import('util/uuidv4');

export default class Polygon {
  constructor(
    points,
    sopInstanceUid,
    seriesInstanceUID,
    ROIContourUid,
    frameNumber,
    interpolated
  ) {
    this._polyPoints = this._deepCopyPoints(points);
    this._sopInstanceUid = sopInstanceUid;
    this._seriesInstanceUID = seriesInstanceUID;
    this._ROIContourUid = ROIContourUid;
    this._frameNumber = frameNumber;
    this._interpolated = interpolated;
    this._color = '';
  }

  _deepCopyPoints(points) {
    // Creates a deep copy of the points array
    const polyPoints = [];
    const isZ = points[0].z !== undefined;

    for (let i = 0; i < points.length; i++) {
      polyPoints.push({
        x: points[i].x,
        y: points[i].y,
      });

      if (isZ) {
        polyPoints[i].z = points[i].z;
      }
    }

    return polyPoints;
  }

  getFreehandToolData(importType) {
    const seriesInstanceUID = this._seriesInstanceUID;
    const ROIContourUid = this._ROIContourUid;

    const data = {
      SeriesInstanceUID: seriesInstanceUID,
      ROIContourUid,
      visible: true,
      active: false,
      invalidated: true,
      handles: {
        points: [],
      },
    };

    if (this._sopInstanceUid) {
      data.sopInstanceUID = this._sopInstanceUid;
    }

    if (this._interpolated) {
      data.interpolated = true;
    }

    this._generatePoints(data.handles.points);

    data.handles.textBox = {
      active: false,
      hasMoved: false,
      movesIndependently: false,
      drawnIndependently: true,
      allowedOutsideImage: true,
      hasBoundingBox: true,
    };

    data.polyBoundingBox = this._generatePolyBoundingBox(data.handles.points);

    if (!data.handles.textBox.hasMoved) {
      // Find the rightmost side of the polyBoundingBox at its vertical center, and place the textbox here
      // Note that this calculates it in image coordinates
      data.handles.textBox.x =
        data.polyBoundingBox.left + data.polyBoundingBox.width;
      data.handles.textBox.y =
        data.polyBoundingBox.top + data.polyBoundingBox.height / 2;
    }

    data.handles.textBox.boundingBox = TextBoxContentUtils.getDefaultBoundingBox(
      data.handles.textBox
    );

    data.toBeScaled = importType;

    data.uuid = uuidv4();

    data.dirty = true;

    return data;
  }

  _generatePoints(points) {
    // Construct data.handles.points array
    for (let i = 0; i < this._polyPoints.length; i++) {
      points.push(this._deepCopyOnePoint(i));
    }

    // Generate lines to be drawn
    for (let i = 0; i < points.length; i++) {
      if (i === points.length - 1) {
        points[i].lines.push(points[0]);
      } else {
        points[i].lines.push({ x: points[i + 1].x, y: points[i + 1].y });
      }
    }
  }

  _deepCopyOnePoint(i) {
    let point = {
      x: this._polyPoints[i].x,
      y: this._polyPoints[i].y,
      lines: [],
    };

    if (this._polyPoints[i].z !== undefined) {
      point.z = this._polyPoints[i].z;
    }

    return point;
  }

  _generatePolyBoundingBox(points) {
    const bounds = {
      left: points[0].x,
      right: points[0].x,
      bottom: points[0].y,
      top: points[0].y,
    };

    for (let i = 0; i < points.length; i++) {
      bounds.left = Math.min(bounds.left, points[i].x);
      bounds.right = Math.max(bounds.right, points[i].x);
      bounds.bottom = Math.min(bounds.bottom, points[i].y);
      bounds.top = Math.max(bounds.top, points[i].y);
    }

    const polyBoundingBox = {
      left: bounds.left,
      top: bounds.bottom,
      width: Math.abs(bounds.right - bounds.left),
      height: Math.abs(bounds.top - bounds.bottom),
    };
    return polyBoundingBox;
  }

  get polyPoints() {
    return this._polyPoints;
  }
  get sopInstanceUid() {
    return this._sopInstanceUid;
  }

  get frameNumber() {
    return this._frameNumber;
  }

  get color() {
    return this._color;
  }

  set color(value) {
    this._color = value;
  }
}
