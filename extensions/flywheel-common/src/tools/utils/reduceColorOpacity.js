import store from '@ohif/viewer/src/store';

export function reduceColorOpacity(measurementColor) {
  if (!measurementColor.includes('rgba')) {
    return measurementColor;
  }
  let splitColor = measurementColor.split('(');
  splitColor = splitColor.length === 2 ? splitColor[1].split(')') : [];
  splitColor = splitColor.length === 2 ? splitColor[0] : '';
  if (splitColor) {
    splitColor = splitColor.split(',');
    if (splitColor.length === 4) {
      let alphaColor = splitColor[3];
      alphaColor = parseFloat(alphaColor).toFixed(1);
      splitColor[3] = '0.1';
      splitColor = splitColor.join(',');
      return `rgba(${splitColor})`;
    }
  }
  return measurementColor;
}

export function hasColorIntensity(measurement = null) {
  const state = store.getState();
  const measurementColorIntensity = state.measurementColorIntensity;
  if (measurementColorIntensity && measurementColorIntensity.colorIntensity) {
    if (measurement) {
      return !measurementColorIntensity.activeHoveredMeasurements.some(
        x => x.measurementId === measurement._id
      );
    }
    return true;
  }
  return false;
}
