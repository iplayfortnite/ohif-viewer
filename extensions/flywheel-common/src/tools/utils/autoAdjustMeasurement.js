import OHIF from '@ohif/core';
import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import cornerstoneMath from 'cornerstone-math';
import store from '@ohif/viewer/src/store';
import { getEnabledElement } from '@ohif/extension-cornerstone/src/state';
import OHIFLabellingData from '@ohif/viewer/src/components/Labelling/OHIFLabellingData';
import {
  Utils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import { isOverlapWithOtherROI } from './checkMeasurementOverlap';
import { isPointInsideFreehand } from './FreehandIntersect';
import { doesIntersect } from './LineIntersect';
import { getRectangleData } from './RectangleIntersect';
import { getEllipseData } from './EllipseIntersect';
import roiTools from './roiTools';
import cloneDeep from 'lodash.clonedeep';
import freehandRoiTool from '../FreehandRoiTool';
import { setAutoLabellingOnce } from '@ohif/viewer/src/appExtensions/MeasurementsPanel/actions';
const freehandUtils = csTools.importInternal('util/freehandUtils');
const { FreehandHandleData } = freehandUtils;
const { setActiveTool } = FlywheelCommonRedux.actions;
const { Vector3 } = cornerstoneMath;

/**
 * To check the measurement is overlapping with any of the other measurements in image
 * @param {string} srcToolType source measurement tool type
 * @param {Object} srcMeasurement source measurement
 * @param {string} imageId image id
 * @return {[Object]} collection of overlapping measurements
 */
function autoAdjustMeasurement(srcToolType, srcMeasurement, imageId) {
  const imagePlane = cornerstone.metaData.get('imagePlaneModule', imageId);
  const imageDimension = {
    x: imagePlane.columns,
    y: imagePlane.rows,
  };

  const state = store.getState();
  const element = Utils.cornerstoneUtils.getEnabledElement(
    state.viewports.activeViewportIndex
  );
  const overlappedMeasurements = isOverlapWithOtherROI(
    srcToolType,
    srcMeasurement,
    imageId,
    element
  );
  //This feature is not supported for Circle roi tool now.
  if (srcToolType === 'CircleRoi') {
    console.warn(
      'Automatic adjust feature is not supported for Circle Roi tool now'
    );
    return false;
  }
  for (let i = 0; i < overlappedMeasurements.length; i++) {
    const measurement = overlappedMeasurements[i];
    const toolType = measurement.toolType;
    //This feature is not supported for Circle roi tool now.
    if (toolType === 'CircleRoi') {
      console.warn(
        'Automatic adjust feature is not supported for Circle Roi tool now'
      );
      continue;
    }
    if (toolType !== roiTools.OpenFreehandRoi) {
      // Check whether second measurement inside first
      const isSecondMeasurementInside = !checkMeasurementOutside(
        measurement,
        toolType,
        srcMeasurement,
        srcToolType,
        imageDimension
      );

      // Check whether first measurement inside second
      const isFirstMeasurementInside = !checkMeasurementOutside(
        srcMeasurement,
        srcToolType,
        measurement,
        toolType,
        imageDimension
      );
      if (isFirstMeasurementInside || isSecondMeasurementInside) {
        return false;
      }
    }
    if (
      srcToolType === roiTools.RectangleRoi ||
      toolType === roiTools.RectangleRoi
    ) {
      // If one of the measurement is rectangle
      srcMeasurement = autoAdjustRectangle(
        measurement,
        toolType,
        srcMeasurement,
        srcToolType,
        imageDimension
      );
      srcToolType = srcMeasurement.toolType;
    } else if (
      srcToolType === roiTools.EllipticalRoi ||
      toolType === roiTools.EllipticalRoi
    ) {
      // If one of the measurement is ellipse
      srcMeasurement = autoAdjustEllipse(
        measurement,
        toolType,
        srcMeasurement,
        srcToolType,
        imageDimension
      );
      srcToolType = srcMeasurement.toolType;
    } else {
      // If both are freehand types
      srcMeasurement = autoAdjustFreeHand(
        measurement,
        toolType,
        srcMeasurement,
        srcToolType
      );
    }
  }
  if (srcToolType === 'FreehandRoi') {
    const freehandROIInstance = new freehandRoiTool();
    const enabledElement = cornerstone.getEnabledElement(element);
    const image = enabledElement.image;
    freehandROIInstance.updateCachedStats(image, element, srcMeasurement);
    const measurementApi = OHIF.measurements.MeasurementApi.Instance;
    measurementApi.updateMeasurement(srcToolType, srcMeasurement);
    OHIF.measurements.MeasurementApi.syncToolStateWithMeasurementData(
      srcMeasurement
    );
  }

  const projectConfig = state.flywheel.projectConfig;
  const labels = projectConfig?.labels || [];
  const measurements = Utils.getAllMeasurementsForImage(
    srcMeasurement.imagePath,
    state.timepointManager.measurements,
    roiTools
  );
  // Boundary compared measurements will filter out from the image measurements list
  const boundaryMeasurements = OHIF.measurements.MeasurementHandlers.handleContourBoundary(
    srcMeasurement,
    imageId,
    labels,
    measurements,
    element
  );
  if (boundaryMeasurements?.length) {
    return false;
  }
  return true;
}

/**
 * Adjust 2 freehand measurements which overlaps
 * @param {Object} measurement1 first measurement
 * @param {string} toolType1 first measurement tool type
 * @param {Object} measurement2 second measurement
 * @param {string} toolType2 second measurement tool type
 */
function autoAdjustFreeHand(
  freehandMeasurement1,
  freehandToolType1,
  freehandMeasurement2,
  freehandToolType2
) {
  //If both are open freehand
  if (
    freehandToolType1 === roiTools.OpenFreehandRoi &&
    freehandToolType2 === roiTools.OpenFreehandRoi
  ) {
    if (
      isArrayOfArray(freehandMeasurement1.handles.points) &&
      isArrayOfArray(freehandMeasurement2.handles.points)
    ) {
      const freehandPoints1 = cloneDeep(freehandMeasurement1.handles.points);
      for (let k = 0; k < freehandMeasurement2.handles.points.length; k++) {
        const freehandPoints2 = cloneDeep(
          freehandMeasurement2.handles.points[k]
        );
        freehandPoints2.push(freehandPoints2[freehandPoints2.length - 1]);
        const newPoints = getNewFreehandPoints(
          freehandPoints1,
          freehandPoints2
        );
        freehandMeasurement2.handles.points[k] = formatLines(newPoints, false);
      }
    }
  } else if (freehandToolType2 === roiTools.OpenFreehandRoi) {
    const freehandPoints1 = cloneDeep(freehandMeasurement1.handles.points);
    freehandPoints1.push(freehandPoints1[0]);
    for (let k = 0; k < freehandMeasurement2.handles.points.length; k++) {
      const freehandPoints2 = cloneDeep(freehandMeasurement2.handles.points[k]);
      freehandPoints2.push(freehandPoints2[freehandPoints2.length - 1]);
      const newPoints = getNewOpenFreehandPoints(
        freehandPoints1,
        freehandPoints2
      );
      freehandMeasurement2.handles.points[k] = formatLines(newPoints, false);
    }
  } else if (freehandToolType1 === roiTools.OpenFreehandRoi) {
    const freehandPoints1 = cloneDeep(freehandMeasurement1.handles.points);
    const freehandPoints2 = cloneDeep(freehandMeasurement2.handles.points);
    freehandPoints2.push(freehandPoints2[0]);
    const newPoints = getNewClosedFreehandPoints(
      freehandPoints1,
      freehandPoints2
    );
    freehandMeasurement2.handles.points = formatLines(newPoints, true);
  } else {
    let freehandPoints1 = cloneDeep(freehandMeasurement2.handles.points);
    let freehandPoints2 = cloneDeep(freehandMeasurement1.handles.points);

    freehandPoints1 = rearrangeMeasureData(freehandPoints2, freehandPoints1);
    freehandPoints2 = rearrangeMeasureData(freehandPoints1, freehandPoints2);

    freehandPoints1.push(freehandPoints1[0]);
    freehandPoints2.push(freehandPoints2[0]);

    const newPoints = getNewFreehandPoints(freehandPoints2, freehandPoints1);
    freehandMeasurement2.handles.points = formatLines(newPoints, true);
  }
  return freehandMeasurement2;
}

/**
 * Adjust 2 measurements which overlaps
 * @param {Object} measurement1 first measurement
 * @param {string} toolType1 first measurement tool type
 * @param {Object} measurement2 second measurement
 * @param {string} toolType2 second measurement tool type
 */
function autoAdjustRectangle(
  measurement1,
  toolType1,
  measurement2,
  toolType2,
  imageDimension
) {
  let rectangleHandles;
  let rectangleData;
  let rectanglePoints;

  let secondRectangleHandles;
  let secondRectangleData;
  let secondRectanglePoints;

  let ellipseHandles;
  let ellipseData;
  let ellipsePoints;

  let freehandPoints;
  let newPoints;

  //First tool is rectangle
  if (toolType1 === roiTools.RectangleRoi) {
    // Convert rectangle points to freehand points
    rectangleHandles = measurement1.handles;
    rectangleData = getRectangleData(
      rectangleHandles.start,
      rectangleHandles.end,
      imageDimension,
      rectangleHandles.initialRotation
    );
    rectanglePoints = rectangleData.points;

    switch (toolType2) {
      case roiTools.RectangleRoi:
        secondRectangleHandles = measurement2.handles;
        secondRectangleData = getRectangleData(
          secondRectangleHandles.start,
          secondRectangleHandles.end,
          imageDimension,
          secondRectangleHandles.initialRotation
        );
        secondRectanglePoints = secondRectangleData.points;

        measurement2 = convertMeasurementToFreehand(
          measurement2,
          secondRectanglePoints
        );

        rectanglePoints = rearrangeMeasureData(
          secondRectanglePoints,
          rectanglePoints
        );
        secondRectanglePoints = rearrangeMeasureData(
          rectanglePoints,
          secondRectanglePoints
        );

        rectanglePoints.push(rectanglePoints[0]);
        secondRectanglePoints.push(secondRectanglePoints[0]);

        newPoints = getNewFreehandPoints(
          rectanglePoints,
          secondRectanglePoints
        );
        measurement2.handles.points = formatLines(newPoints, true);
        break;

      case roiTools.EllipticalRoi:
        ellipseHandles = measurement2.handles;
        ellipseData = getEllipseData(
          ellipseHandles.start,
          ellipseHandles.end,
          imageDimension,
          ellipseHandles.initialRotation,
          9
        );
        ellipsePoints = ellipseData.linePoints;

        measurement2 = convertMeasurementToFreehand(
          measurement2,
          ellipsePoints
        );

        rectanglePoints = rearrangeMeasureData(ellipsePoints, rectanglePoints);
        ellipsePoints = rearrangeMeasureData(rectanglePoints, ellipsePoints);

        rectanglePoints.push(rectanglePoints[0]);
        ellipsePoints.push(ellipsePoints[0]);

        newPoints = getNewFreehandPoints(rectanglePoints, ellipsePoints);
        measurement2.handles.points = formatLines(newPoints, true);
        break;

      case roiTools.FreehandRoi:
        freehandPoints = cloneDeep(measurement2.handles.points);

        rectanglePoints = rearrangeMeasureData(freehandPoints, rectanglePoints);
        freehandPoints = rearrangeMeasureData(rectanglePoints, freehandPoints);

        rectanglePoints.push(rectanglePoints[0]);
        freehandPoints.push(freehandPoints[0]);

        newPoints = getNewFreehandPoints(rectanglePoints, freehandPoints);
        measurement2.handles.points = formatLines(newPoints, true);
        break;

      case roiTools.OpenFreehandRoi:
        rectanglePoints.push(rectanglePoints[0]);
        for (let k = 0; k < measurement2.handles.points.length; k++) {
          freehandPoints = cloneDeep(measurement2.handles.points[k]);
          freehandPoints.push(freehandPoints[freehandPoints.length - 1]);
          newPoints = getNewOpenFreehandPoints(rectanglePoints, freehandPoints);
          measurement2.handles.points[k] = formatLines(newPoints, false);
        }
        break;
    }
  } else {
    // If second measurement is rectangle
    rectangleHandles = measurement2.handles;
    rectangleData = getRectangleData(
      rectangleHandles.start,
      rectangleHandles.end,
      imageDimension,
      rectangleHandles.initialRotation
    );
    rectanglePoints = rectangleData.points;

    switch (toolType1) {
      case roiTools.EllipticalRoi:
        ellipseHandles = measurement1.handles;
        ellipseData = getEllipseData(
          ellipseHandles.start,
          ellipseHandles.end,
          imageDimension,
          ellipseHandles.initialRotation,
          9
        );
        ellipsePoints = ellipseData.linePoints;
        measurement2 = convertMeasurementToFreehand(
          measurement2,
          rectanglePoints
        );
        ellipsePoints = rearrangeMeasureData(rectanglePoints, ellipsePoints);
        rectanglePoints = rearrangeMeasureData(ellipsePoints, rectanglePoints);
        ellipsePoints.push(ellipsePoints[0]);
        rectanglePoints.push(rectanglePoints[0]);

        newPoints = getNewFreehandPoints(ellipsePoints, rectanglePoints);
        measurement2.handles.points = formatLines(newPoints, true);
        break;

      case roiTools.FreehandRoi:
        let freehandPoints2 = cloneDeep(measurement1.handles.points);
        measurement2 = convertMeasurementToFreehand(
          measurement2,
          rectanglePoints
        );
        rectanglePoints = rearrangeMeasureData(
          freehandPoints2,
          rectanglePoints
        );
        freehandPoints2 = rearrangeMeasureData(
          rectanglePoints,
          freehandPoints2
        );
        rectanglePoints.push(rectanglePoints[0]);
        freehandPoints2.push(freehandPoints2[0]);

        newPoints = getNewFreehandPoints(freehandPoints2, rectanglePoints);
        measurement2.handles.points = formatLines(newPoints, true);
        break;

      case roiTools.OpenFreehandRoi:
        freehandPoints = cloneDeep(measurement1.handles.points);
        measurement2 = convertMeasurementToFreehand(
          measurement2,
          rectanglePoints
        );
        rectanglePoints.push(rectanglePoints[0]);
        newPoints = getNewClosedFreehandPoints(freehandPoints, rectanglePoints);
        measurement2.handles.points = formatLines(newPoints, true);
        break;
    }
  }
  return measurement2;
}

/**
 * Adjust 2 measurements which overlaps
 * @param {Object} measurement1 first measurement
 * @param {string} toolType1 first measurement tool type
 * @param {Object} measurement2 second measurement
 * @param {string} toolType2 second measurement tool type
 */
function autoAdjustEllipse(
  measurement1,
  toolType1,
  measurement2,
  toolType2,
  imageDimension
) {
  let secondEllipseHandles;
  let secondEllipseData;
  let secondEllipsePoints;

  let ellipseHandles;
  let ellipseData;
  let ellipsePoints;

  let freehandPoints;
  let newPoints;

  // If first measurement is Ellipse
  if (toolType1 === roiTools.EllipticalRoi) {
    ellipseHandles = measurement1.handles;
    ellipseData = getEllipseData(
      ellipseHandles.start,
      ellipseHandles.end,
      imageDimension,
      ellipseHandles.initialRotation,
      9
    );
    ellipsePoints = ellipseData.linePoints;

    switch (toolType2) {
      case roiTools.EllipticalRoi:
        secondEllipseHandles = measurement2.handles;
        secondEllipseData = getEllipseData(
          secondEllipseHandles.start,
          secondEllipseHandles.end,
          imageDimension,
          secondEllipseHandles.initialRotation,
          9
        );
        secondEllipsePoints = secondEllipseData.linePoints;

        measurement2 = convertMeasurementToFreehand(
          measurement2,
          secondEllipsePoints
        );

        ellipsePoints = rearrangeMeasureData(
          secondEllipsePoints,
          ellipsePoints
        );
        secondEllipsePoints = rearrangeMeasureData(
          ellipsePoints,
          secondEllipsePoints
        );

        ellipsePoints.push(ellipsePoints[0]);
        secondEllipsePoints.push(secondEllipsePoints[0]);

        newPoints = getNewFreehandPoints(ellipsePoints, secondEllipsePoints);
        measurement2.handles.points = formatLines(newPoints, true);
        break;

      case roiTools.FreehandRoi:
        freehandPoints = cloneDeep(measurement2.handles.points);

        ellipsePoints = rearrangeMeasureData(freehandPoints, ellipsePoints);
        freehandPoints = rearrangeMeasureData(ellipsePoints, freehandPoints);

        ellipsePoints.push(ellipsePoints[0]);
        freehandPoints.push(freehandPoints[0]);

        newPoints = getNewFreehandPoints(ellipsePoints, freehandPoints);
        measurement2.handles.points = formatLines(newPoints, true);
        break;

      case roiTools.OpenFreehandRoi:
        ellipsePoints.push(ellipsePoints[0]);
        for (let k = 0; k < measurement2.handles.points.length; k++) {
          freehandPoints = cloneDeep(measurement2.handles.points[k]);
          freehandPoints.push(freehandPoints[freehandPoints.length - 1]);

          newPoints = getNewOpenFreehandPoints(ellipsePoints, freehandPoints);
          measurement2.handles.points[k] = formatLines(newPoints, false);
        }
        break;
    }
  } else {
    // If second measurement is Ellipse
    ellipseHandles = measurement2.handles;
    ellipseData = getEllipseData(
      ellipseHandles.start,
      ellipseHandles.end,
      imageDimension,
      ellipseHandles.initialRotation,
      9
    );
    ellipsePoints = ellipseData.linePoints;

    switch (toolType1) {
      case roiTools.FreehandRoi:
        let freehandPoints2 = cloneDeep(measurement1.handles.points);
        measurement2 = convertMeasurementToFreehand(
          measurement2,
          ellipsePoints
        );
        ellipsePoints = rearrangeMeasureData(freehandPoints2, ellipsePoints);
        freehandPoints2 = rearrangeMeasureData(ellipsePoints, freehandPoints2);
        ellipsePoints.push(ellipsePoints[0]);
        freehandPoints2.push(freehandPoints2[0]);
        newPoints = getNewFreehandPoints(freehandPoints2, ellipsePoints);
        measurement2.handles.points = formatLines(newPoints, true);
        break;

      case roiTools.OpenFreehandRoi:
        freehandPoints = cloneDeep(measurement1.handles.points);
        measurement2 = convertMeasurementToFreehand(
          measurement2,
          ellipsePoints
        );
        ellipsePoints.push(ellipsePoints[0]);
        newPoints = getNewClosedFreehandPoints(freehandPoints, ellipsePoints);
        measurement2.handles.points = formatLines(newPoints, true);
        break;

      default:
        break;
    }
  }
  return measurement2;
}

/**
 * To get freehand points which does not overlap.
 * @param {Array} freehandPoints1 freehand coordinates of first measurement
 * @param {Array} freehandPoints2 freehand coordinates of second measurement
 * @param {Array} newPoints array length
 */
function getNewFreehandPoints(freehandPoints1, freehandPoints2) {
  let oddIntersection = false; // Set true when freehand intersect odd times
  let newPoints = [];
  let freehand2SliceIndex1;
  let freehand2SliceIndex2;
  let intersectionPoint = false;

  // For open freehand
  if (isArrayOfArray(freehandPoints1)) {
    for (let i = 0; i < freehandPoints2.length; i++) {
      if (i > 0) {
        const P1 = freehandPoints2[i - 1];
        const P2 = freehandPoints2[i];
        if (!oddIntersection) {
          newPoints.push(P1);
        }
        for (let m = 0; m < freehandPoints1.length; m++) {
          // Take intersection points and index in ascending order of the distance from P1.
          // Adding offset with intersection point to avoid overlapping popup again
          // If lines intersecting even times, common points inserted to new points before inserting intersection point.
          const intersections = getOrderedLineIntersectionIndices(
            P1,
            P2,
            freehandPoints1[m]
          );
          for (let j = 0; j < intersections.length; j++) {
            const pointInfo = intersections[j];
            intersectionPoint = pointInfo.intersection;
            // If intersection point is on the line, point treat in next iteration.
            if (
              intersectionPoint.x.toFixed(3) === P1.x.toFixed(3) &&
              intersectionPoint.y.toFixed(3) === P1.y.toFixed(3)
            ) {
              newPoints.pop(); // Removing existing point since it will be on the line.
              continue;
            }
            intersectionPoint = getOffsetPoint(
              intersectionPoint,
              P1,
              P2,
              oddIntersection
            );
            if (!oddIntersection) {
              freehand2SliceIndex1 = pointInfo.index;
              if (
                newPoints.every(
                  newPoint =>
                    distanceBetweenPoints(intersectionPoint, newPoint) > 0.5
                )
              ) {
                newPoints.push(intersectionPoint);
              }
            } else {
              freehand2SliceIndex2 = pointInfo.index;
              const reversePoints = freehand2SliceIndex1 > freehand2SliceIndex2;
              let existPoints = getOffsetPointsForOpenFreehand(
                freehandPoints1[m],
                reversePoints ? freehand2SliceIndex2 : freehand2SliceIndex1,
                reversePoints ? freehand2SliceIndex1 : freehand2SliceIndex2,
                newPoints[newPoints.length - 1],
                reversePoints
              );
              for (let existPoint of existPoints) {
                if (
                  newPoints.every(
                    newPoint =>
                      distanceBetweenPoints(existPoint, newPoint) > 0.5
                  )
                ) {
                  newPoints.push(existPoint);
                }
              }
              if (
                newPoints.every(
                  newPoint =>
                    distanceBetweenPoints(intersectionPoint, newPoint) > 0.5
                )
              ) {
                newPoints.push(intersectionPoint);
              }
            }
            oddIntersection = !oddIntersection;
          }
        }
      }
    }
  } else {
    let closedLoopStartIndex = 0;
    let reversePoints = false;
    let existPoints = [];
    for (let index = 0; index < freehandPoints2.length; index++) {
      if (index > 0) {
        const P1 = freehandPoints2[index - 1];
        const P2 = freehandPoints2[index];
        if (!oddIntersection && !closedLoopStartIndex) {
          newPoints.push(P1);
        } else if (!oddIntersection) {
          existPoints.push(P1);
        }

        // Take intersection points and index in ascending order of the distance from P1.
        // Adding offset with intersection point to avoid overlapping popup again
        // If lines intersecting even times, common points inserted to new points before inserting intersection point.
        const intersections = getOrderedLineIntersectionIndices(
          P1,
          P2,
          freehandPoints1
        );
        for (let j = 0; j < intersections.length; j++) {
          const pointInfo = intersections[j];
          intersectionPoint = pointInfo.intersection;
          // If intersection point is on the line, point treat in next iteration.
          if (
            intersectionPoint.x.toFixed(3) === P1.x.toFixed(3) &&
            intersectionPoint.y.toFixed(3) === P1.y.toFixed(3)
          ) {
            newPoints.pop(); // Removing existing point since it will be on the line.
            continue;
          }
          intersectionPoint = getOffsetPoint(
            intersectionPoint,
            P1,
            P2,
            oddIntersection
          );
          if (!oddIntersection) {
            // If overlapped measurement has more than one closed parts.
            if (closedLoopStartIndex) {
              existPoints = [];
            } else {
              freehand2SliceIndex1 = pointInfo.index;
              if (
                newPoints.every(
                  newPoint =>
                    distanceBetweenPoints(intersectionPoint, newPoint) > 0.5
                )
              ) {
                newPoints.push(intersectionPoint);
              }
            }
          } else {
            freehand2SliceIndex2 = pointInfo.index;

            // Reversing existing points if freehand is processed in reverse order
            reversePoints = freehand2SliceIndex1 > freehand2SliceIndex2;
            existPoints = getOffsetPointsForClosedFreehand(
              freehandPoints1,
              reversePoints ? freehand2SliceIndex2 : freehand2SliceIndex1,
              reversePoints ? freehand2SliceIndex1 : freehand2SliceIndex2
            );
            existPoints = reversePoints ? existPoints.reverse() : existPoints;
            existPoints.push(intersectionPoint);
            closedLoopStartIndex = newPoints.length - 1;
          }
          oddIntersection = !oddIntersection;
        }
      }
    }
    for (let existPoint of existPoints) {
      if (
        newPoints.every(
          newPoint => distanceBetweenPoints(existPoint, newPoint) > 0.5
        )
      ) {
        newPoints.push(existPoint);
      }
    }
  }
  return newPoints;
}

/**
 * To get freehand points which does not overlap.
 * @param {Array} freehandPoints1 freehand coordinates of first measurement
 * @param {Array} freehandPoints2 freehand coordinates of second measurement
 * @param {Array} newPoints array length
 */
function getNewOpenFreehandPoints(freehandPoints1, freehandPoints2) {
  let oddIntersection = false; // Set true when freehand intersect odd times
  let newPoints = [];
  let freehand2SliceIndex1;
  let freehand2SliceIndex2;
  let intersectionPoint = false;

  let reversePoints = false;
  for (let index = 0; index < freehandPoints2.length; index++) {
    if (index > 0) {
      const P1 = freehandPoints2[index - 1];
      const P2 = freehandPoints2[index];
      const pointInFreehand = isPointInsideFreehand(freehandPoints1, P1);
      if (!oddIntersection && !pointInFreehand) {
        newPoints.push(P1);
      }

      // Take intersection points and index in ascending order of the distance from P1.
      // Adding offset with intersection point to avoid overlapping popup again
      // If lines intersecting even times, common points inserted to new points before inserting intersection point.
      const intersections = getOrderedLineIntersectionIndices(
        P1,
        P2,
        freehandPoints1
      );
      for (let j = 0; j < intersections.length; j++) {
        const pointInfo = intersections[j];
        intersectionPoint = pointInfo.intersection;
        // If intersection point is on the line, point treat in next iteration.
        if (
          intersectionPoint.x.toFixed(3) === P1.x.toFixed(3) &&
          intersectionPoint.y.toFixed(3) === P1.y.toFixed(3)
        ) {
          newPoints.pop(); // Removing existing point since it will be on the line.
          continue;
        }
        intersectionPoint = getOffsetPoint(
          intersectionPoint,
          P1,
          P2,
          oddIntersection
        );
        if (!oddIntersection) {
          freehand2SliceIndex1 = pointInfo.index;
          newPoints.push(intersectionPoint);
          if (!pointInFreehand) {
            oddIntersection = true;
          }
        } else {
          freehand2SliceIndex2 = pointInfo.index;

          // Reversing existing points if freehand is processed in reverse order
          reversePoints = freehand2SliceIndex1 > freehand2SliceIndex2;
          let existPoints = getOffsetPointsForClosedFreehand(
            freehandPoints1,
            reversePoints ? freehand2SliceIndex2 : freehand2SliceIndex1,
            reversePoints ? freehand2SliceIndex1 : freehand2SliceIndex2
          );
          existPoints = reversePoints ? existPoints.reverse() : existPoints;

          for (let existPoint of existPoints) {
            if (
              newPoints.every(
                newPoint => distanceBetweenPoints(existPoint, newPoint) > 0.5
              )
            ) {
              newPoints.push(existPoint);
            }
          }
          if (
            newPoints.every(
              newPoint =>
                distanceBetweenPoints(intersectionPoint, newPoint) > 0.5
            )
          ) {
            newPoints.push(intersectionPoint);
          }
          oddIntersection = false;
        }
      }
    }
  }
  return newPoints;
}

/**
 * To get freehand points which does not overlap when measurement1 is open freehand and measurement2 is a closed freehand.
 * @param {Array} freehandPoints1 freehand coordinates of first measurement
 * @param {Array} freehandPoints2 freehand coordinates of second measurement
 * @param {Array} newPoints array length
 */
function getNewClosedFreehandPoints(freehandPoints1, freehandPoints2) {
  let newPoints = [];
  for (let m = 0; m < freehandPoints1.length; m++) {
    let freehand2SliceIndex1;
    let freehand2SliceIndex2;
    let intersectionPoint = false;
    let offsetPoints;
    let oddIntersectionIndex;
    let oddIntersection = false;
    newPoints = [];

    // If first point of fragmented ROI is inside freehand roi, then process fragment in reverse order.
    if (isPointInsideFreehand(freehandPoints2, freehandPoints1[m][0])) {
      freehandPoints1[m] = freehandPoints1[m].reverse();
    }
    for (let i = 0; i < freehandPoints2.length; i++) {
      if (i > 0) {
        const P1 = freehandPoints2[i - 1];
        const P2 = freehandPoints2[i];
        if (!oddIntersection) {
          newPoints.push(P1);
        }

        // Take intersection points and index in ascending order of the distance from P1.
        // Adding offset with intersection point to avoid overlapping popup again
        // If lines intersecting even times, common points inserted to new points before inserting intersection point.
        const intersections = getOrderedLineIntersectionIndices(
          P1,
          P2,
          freehandPoints1[m]
        );
        for (let j = 0; j < intersections.length; j++) {
          const pointInfo = intersections[j];
          intersectionPoint = pointInfo.intersection;
          // If intersection point is on the line, point treat in next iteration.
          if (
            intersectionPoint.x.toFixed(3) === P1.x.toFixed(3) &&
            intersectionPoint.y.toFixed(3) === P1.y.toFixed(3)
          ) {
            newPoints.pop(); // Removing existing point since it will be on the line.
            continue;
          }

          // Add an offset value with intersection points to avoid overlapping again.
          offsetPoints = getOffsetPointForClosedFreehand(
            intersectionPoint,
            P1,
            P2,
            oddIntersection
          );
          if (!oddIntersection) {
            oddIntersectionIndex = i;
            freehand2SliceIndex1 = pointInfo.index;
            newPoints.push(offsetPoints[0]);
          } else {
            freehand2SliceIndex2 = pointInfo.index;
            const reversePoints = freehand2SliceIndex1 > freehand2SliceIndex2;

            let existPoints = getOffsetPointsForOpenFreehand(
              freehandPoints1[m],
              reversePoints ? freehand2SliceIndex2 : freehand2SliceIndex1,
              reversePoints ? freehand2SliceIndex1 : freehand2SliceIndex2,
              newPoints[newPoints.length - 1],
              reversePoints
            );

            for (let existPoint of existPoints) {
              newPoints.push(existPoint);
            }
            newPoints.push(offsetPoints[0]);
          }
          oddIntersection = !oddIntersection;
        }

        if (i === freehandPoints2.length - 1) {
          if (oddIntersection) {
            // If open freehand roi fragment points overlapping odd times with measurement2 points.
            const existingPoints = getOffsetPointsForOpenFreehandOdd(
              freehandPoints1[m],
              freehand2SliceIndex1,
              newPoints[newPoints.length - 1],
              offsetPoints
            );
            for (let existingPoint of existingPoints.existingPoints1) {
              newPoints.push(existingPoint);
            }
            for (let existingPoint of existingPoints.existingPoints2.reverse()) {
              newPoints.push(existingPoint);
            }
            newPoints.push(offsetPoints[1]);
            for (
              let k = oddIntersectionIndex;
              k < freehandPoints2.length - 1;
              k++
            ) {
              newPoints.push(freehandPoints2[k]);
            }
          }
        }
      }
    }

    // Points processed after current fragment is given to process with next fragment
    freehandPoints2 = cloneDeep(newPoints);
    if (m !== freehandPoints1.length - 1) {
      freehandPoints2.push(freehandPoints2[0]);
    }
  }
  return newPoints;
}

/**
 * To check two lines are intersecting
 * @param {Object} P1 first point of first line
 * @param {Object} P2 second point of first line
 * @param {Object} P3 first point of second line
 * @param {Object} P4 second point of second line
 * @returns {Object} intersection point
 */
function lineIntersection(P1, P2, P3, P4) {
  if (doesIntersect(P1, P2, P3, P4)) {
    // Line AB represented as a1x + b1y = c1
    const a1 = P2.y - P1.y;
    const b1 = P1.x - P2.x;
    const c1 = a1 * P1.x + b1 * P1.y;

    // Line CD represented as a2x + b2y = c2
    const a2 = P4.y - P3.y;
    const b2 = P3.x - P4.x;
    const c2 = a2 * P3.x + b2 * P3.y;

    const determinant = a1 * b2 - a2 * b1;

    if (determinant === 0) {
      // The lines are parallel. This is simplified
      // by returning a pair of empty object
      return false;
    } else {
      const x = (b2 * c1 - b1 * c2) / determinant;
      const y = (a1 * c2 - a2 * c1) / determinant;
      return {
        x,
        y,
      };
    }
  } else {
    return false;
  }
}

/**
 * To check the points are array of array format
 * @param {Array} points
 * @returns {Boolean}
 */
function isArrayOfArray(points) {
  if (points?.length && Array.isArray(points[0])) {
    return true;
  }
  return false;
}

/**
 * To insert elements in lines array of freehand points
 * @param {Array} freehandPoints freehand points.
 * @param {Boolean} isClosedFreehand closed freehand or open freehand.
 * @param {Array} freehandPoints freehand points with lines.
 */
function formatLines(freehandPoints, isClosedFreehand = false) {
  const newFreehandPoints = [];
  for (let i = 0; i < freehandPoints.length; i++) {
    const newHandleData = new FreehandHandleData(freehandPoints[i]);
    if (i < freehandPoints.length - 1) {
      let nextPoint = {
        x: freehandPoints[i + 1].x,
        y: freehandPoints[i + 1].y,
      };
      newHandleData.lines.push(nextPoint);
    } else {
      if (isClosedFreehand) {
        let nextPoint = {
          x: freehandPoints[0].x,
          y: freehandPoints[0].y,
        };
        newHandleData.lines.push(nextPoint);
      }
    }
    newFreehandPoints[i] = newHandleData;
  }

  return newFreehandPoints;
}

/**
 * Return point just outside of first measurement.
 * @param {Array} freehandPoints1 freehand coordinates of first measurement
 * @param {Integer} freehand2SliceIndex1 Index of first intersection line.
 * @param {Array} newPoints Points after adding offset.
 */
function getOffsetPointsForClosedFreehand(
  freehandPoints1,
  freehand2SliceIndex1,
  freehand2SliceIndex2,
  offset = 0.5
) {
  let points = [];
  for (let i = freehand2SliceIndex1; i < freehand2SliceIndex2; i++) {
    const firstPoint = freehandPoints1[i - 1];
    const secondPoint = freehandPoints1[i];
    const thirdPoint = freehandPoints1[i + 1];

    const ab = {
      x: secondPoint.x - firstPoint.x,
      y: secondPoint.y - firstPoint.y,
    };

    const bc = {
      x: thirdPoint.x - secondPoint.x,
      y: thirdPoint.y - secondPoint.y,
    };

    const vectorA = new Vector3(ab.x, ab.y, 0);
    const vectorB = new Vector3(bc.x, bc.y, 0);

    vectorA.normalize();
    vectorB.normalize();

    let vector = new Vector3();
    vector.subVectors(vectorA, vectorB).normalize();
    if (!vector.x || !vector.y) {
      if (vectorA.x == 0) {
        vector.x = 1;
        vector.y = 0;
      } else if (vectorA.y == 0) {
        vector.x = 0;
        vector.y = 1;
      }
    }
    let newPoint = {
      x: secondPoint.x + vector.x * offset,
      y: secondPoint.y + vector.y * offset,
    };
    if (isPointInsideFreehand(freehandPoints1, newPoint)) {
      newPoint = {
        x: secondPoint.x - vector.x * offset,
        y: secondPoint.y - vector.y * offset,
      };
    }
    points.push(newPoint);
  }
  return points;
}

/**
 * Return point just outside of first measurement.
 * @param {Array} freehandPoints1 freehand coordinates of first measurement
 * @param {Integer} freehand2SliceIndex1 Index of first intersection line.
 * @param {Array} newPoints Points after adding offset.
 */
function getOffsetPointsForOpenFreehand(
  freehandPoints1,
  freehand2SliceIndex1,
  freehand2SliceIndex2,
  lastPoint,
  reversePoints,
  offset = 0.5
) {
  let points = [];
  let prevPoint;
  if (reversePoints) {
    for (let i = freehand2SliceIndex2; i > freehand2SliceIndex1; i--) {
      const firstPoint = freehandPoints1[i];
      const secondPoint = freehandPoints1[i - 1];
      const thirdPoint = freehandPoints1[i - 2];

      if (i === freehand2SliceIndex2) {
        prevPoint = lastPoint;
      } else {
        prevPoint = points[points.length - 1];
      }

      const ab = {
        x: secondPoint.x - firstPoint.x,
        y: secondPoint.y - firstPoint.y,
      };

      const bc = {
        x: thirdPoint.x - secondPoint.x,
        y: thirdPoint.y - secondPoint.y,
      };

      const vectorA = new Vector3(ab.x, ab.y, 0);
      const vectorB = new Vector3(bc.x, bc.y, 0);

      vectorA.normalize();
      vectorB.normalize();

      let vector = new Vector3();
      vector.subVectors(vectorA, vectorB).normalize();
      if (!vector.x || !vector.y) {
        if (vectorA.x == 0) {
          vector.x = 1;
          vector.y = 0;
        } else if (vectorA.y == 0) {
          vector.x = 0;
          vector.y = 1;
        }
      }
      let newPoint = {
        x: secondPoint.x + vector.x * offset,
        y: secondPoint.y + vector.y * offset,
      };

      // Check whether proposed line intersect with line any other lines in freehand, if so offset point with opposite direction is selected.
      if (
        doesIntersect(newPoint, prevPoint, firstPoint, secondPoint) ||
        doesIntersect(newPoint, prevPoint, secondPoint, thirdPoint)
      ) {
        newPoint = {
          x: secondPoint.x - vector.x * offset,
          y: secondPoint.y - vector.y * offset,
        };
      }
      points.push(newPoint);
    }
  } else {
    for (let i = freehand2SliceIndex1; i < freehand2SliceIndex2; i++) {
      const firstPoint = freehandPoints1[i - 1];
      const secondPoint = freehandPoints1[i];
      const thirdPoint = freehandPoints1[i + 1];

      if (i === freehand2SliceIndex1) {
        prevPoint = lastPoint;
      } else {
        prevPoint = points[points.length - 1];
      }

      const ab = {
        x: secondPoint.x - firstPoint.x,
        y: secondPoint.y - firstPoint.y,
      };

      const bc = {
        x: thirdPoint.x - secondPoint.x,
        y: thirdPoint.y - secondPoint.y,
      };

      const vectorA = new Vector3(ab.x, ab.y, 0);
      const vectorB = new Vector3(bc.x, bc.y, 0);

      vectorA.normalize();
      vectorB.normalize();

      let vector = new Vector3();
      vector.subVectors(vectorA, vectorB).normalize();
      if (!vector.x || !vector.y) {
        if (vectorA.x == 0) {
          vector.x = 1;
          vector.y = 0;
        } else if (vectorA.y == 0) {
          vector.x = 0;
          vector.y = 1;
        }
      }
      let newPoint = {
        x: secondPoint.x + vector.x * offset,
        y: secondPoint.y + vector.y * offset,
      };
      // Check whether proposed line intersect with line any other lines in freehand, if so offset point with opposite direction is selected.
      if (
        doesIntersect(newPoint, prevPoint, firstPoint, secondPoint) ||
        doesIntersect(newPoint, prevPoint, secondPoint, thirdPoint)
      ) {
        newPoint = {
          x: secondPoint.x - vector.x * offset,
          y: secondPoint.y - vector.y * offset,
        };
      }
      points.push(newPoint);
    }
  }

  return points;
}

/**
 * Return point just outside of first measurement.
 * @param {*} freehandPoints1 freehandPoints1 freehand coordinates of first measurement
 * @param {*} freehand2SliceIndex1 freehand2SliceIndex1 Index of first intersection line.
 * @param {*} lastPointFreehand2 last point in second freehand
 * @param {*} offsetPoints offset points of intersection points.
 * @returns
 */
function getOffsetPointsForOpenFreehandOdd(
  freehandPoints1,
  freehand2SliceIndex1,
  lastPointFreehand2,
  offsetPoints,
  offset = 0.5
) {
  let existingPoints1 = [];
  let existingPoints2 = [];
  let prevPoint;
  for (let i = freehand2SliceIndex1; i < freehandPoints1.length - 1; i++) {
    const firstPoint = freehandPoints1[i - 1];
    const secondPoint = freehandPoints1[i];
    const thirdPoint = freehandPoints1[i + 1];

    if (i === freehand2SliceIndex1) {
      prevPoint = lastPointFreehand2;
    } else {
      prevPoint = existingPoints1[existingPoints1.length - 1];
    }

    const ab = {
      x: secondPoint.x - firstPoint.x,
      y: secondPoint.y - firstPoint.y,
    };

    const bc = {
      x: thirdPoint.x - secondPoint.x,
      y: thirdPoint.y - secondPoint.y,
    };

    const vectorA = new Vector3(ab.x, ab.y, 0);
    const vectorB = new Vector3(bc.x, bc.y, 0);

    vectorA.normalize();
    vectorB.normalize();

    let vector = new Vector3();
    vector.subVectors(vectorA, vectorB).normalize();
    if (!vector.x || !vector.y) {
      if (vectorA.x == 0) {
        vector.x = 1;
        vector.y = 0;
      } else if (vectorA.y == 0) {
        vector.x = 0;
        vector.y = 1;
      }
    }
    let newPoint = {
      x: secondPoint.x + vector.x * offset,
      y: secondPoint.y + vector.y * offset,
    };

    // Check whether proposed line intersect with line any other lines in freehand, if so offset point with opposite direction is selected.
    if (
      doesIntersect(newPoint, prevPoint, firstPoint, secondPoint) ||
      doesIntersect(newPoint, prevPoint, secondPoint, thirdPoint)
    ) {
      existingPoints2.push(newPoint);
      existingPoints1.push({
        x: secondPoint.x - vector.x * offset,
        y: secondPoint.y - vector.y * offset,
      });
    } else {
      existingPoints2.push({
        x: secondPoint.x - vector.x,
        y: secondPoint.y - vector.y,
      });
      existingPoints1.push(newPoint);
    }
  }

  const lastPont = freehandPoints1[freehandPoints1.length - 1];
  const secondLastPont = freehandPoints1[freehandPoints1.length - 2];

  const ls = {
    x: secondLastPont.x - lastPont.x,
    y: secondLastPont.y - lastPont.y,
  };
  const vectorC = new Vector3(ls.x, ls.y, 0);
  vectorC.normalize();

  let lastExistPoint = {
    x: lastPont.x + vectorC.x,
    y: lastPont.y + vectorC.y,
  };

  const points = {
    lastExistPoint,
    existingPoints1,
    offsetPoints,
    lastPont,
    secondLastPont,
    existingPoints2,
  };
  const newLineIntersectWithFreehand1 = doesNewLineIntersectWithFreehand1(
    points
  );
  if (newLineIntersectWithFreehand1) {
    lastExistPoint = {
      x: lastPont.x - vectorC.x,
      y: lastPont.y - vectorC.y,
    };
  }
  existingPoints1.push(lastExistPoint);
  return {
    existingPoints1,
    existingPoints2,
  };
}

function doesNewLineIntersectWithFreehand1(points) {
  const {
    lastExistPoint,
    existingPoints1,
    offsetPoints,
    lastPont,
    secondLastPont,
    existingPoints2,
  } = points;

  return (
    doesIntersect(
      lastExistPoint,
      existingPoints1.length
        ? existingPoints1[existingPoints1.length - 1]
        : offsetPoints[0],
      lastPont,
      secondLastPont
    ) ||
    doesIntersect(
      lastExistPoint,
      existingPoints2.length
        ? existingPoints2[existingPoints2.length - 1]
        : offsetPoints[1],
      lastPont,
      secondLastPont
    )
  );
}

/**
 * Rearrange points when points start inside freehandData roi so that starting point in points falls outside freehandData roi.
 * @param {*} freehandData
 * @param {*} points
 * @returns
 */
function rearrangeMeasureData(freehandData, points) {
  let newFreehandData = [];
  let insidePoints = [];
  let outsidePoints = [];
  let firstPointInside = false;
  let lineOutside = false;
  if (isPointInsideFreehand(freehandData, points[0])) {
    firstPointInside = true;
    insidePoints.push(points[0]);
  }
  if (firstPointInside) {
    for (let i = 1; i < points.length; i++) {
      if (isPointInsideFreehand(freehandData, points[i]) && !lineOutside) {
        insidePoints.push(points[i]);
      } else {
        outsidePoints.push(points[i]);
        lineOutside = true;
      }
    }
    newFreehandData = outsidePoints.concat(insidePoints);
  } else {
    newFreehandData = points;
  }
  return newFreehandData;
}

/**
 * Convert overlapped measurement to freehand roi.
 * @param {*} measurementData
 * @param {*} points
 * @returns
 */
function convertMeasurementToFreehand(measurementData, points) {
  const freeHandTool = new freehandRoiTool();
  const evtData = {
    currentPoints: {
      image: {},
    },
  };
  freeHandTool._drawing = true;
  const newFreehandMeasurementData = freeHandTool.createNewMeasurement(evtData);
  newFreehandMeasurementData.color = measurementData.color;
  newFreehandMeasurementData.location = measurementData.location;
  newFreehandMeasurementData.isSubForm = measurementData?.isSubForm;
  newFreehandMeasurementData.question = measurementData?.question;
  newFreehandMeasurementData.subFormAnnotationId =
    measurementData?.subFormAnnotationId;
  newFreehandMeasurementData.subFormName = measurementData?.subFormName;
  newFreehandMeasurementData.unit = measurementData?.unit;
  newFreehandMeasurementData.questionKey = measurementData?.questionKey;
  newFreehandMeasurementData.handles.points = formatLines(points, true);

  const state = store.getState();
  const activeViewportIndex = state.viewports.activeViewportIndex;
  const element = getEnabledElement(activeViewportIndex);
  // Associate this data with this imageId so we can render it and manipulate it
  csTools.addToolState(element, 'FreehandRoi', newFreehandMeasurementData);

  const toolState = csTools.getToolState(element, 'FreehandRoi');
  freeHandTool.configuration.currentTool = toolState.data.length - 1;
  freeHandTool.configuration.currentHandle = undefined;
  freeHandTool.element = element;
  freeHandTool._activeDrawingToolReference =
    toolState.data[freeHandTool.configuration.currentTool];
  const projectConfig = state.flywheel.projectConfig;
  let labelData = OHIFLabellingData;
  store.dispatch(setActiveTool('FreehandRoi'));
  if (projectConfig && projectConfig.labels) {
    labelData = projectConfig.labels;
  }

  // Assign the same label of old measurement to new converted measurement.
  const location =
    Object.keys(labelData).find(
      key => labelData[key].value === measurementData.location
    ) || '';
  const measurementLocation = labelData[location];
  store.dispatch(setAutoLabellingOnce(measurementLocation));
  freeHandTool.completeDrawing(element);

  deleteMeasurement(measurementData);
  store.dispatch(setActiveTool(measurementData.toolType));
  return newFreehandMeasurementData;
}

/**
 * Return intersection points and index in ascending order of the distance from P1
 * @param {*} P1 point in second freehand
 * @param {*} P2 point in second freehand
 * @param {*} freehandPoints1
 * @returns
 */
function getOrderedLineIntersectionIndices(P1, P2, freehandPoints1) {
  const indices = [];
  for (let j = 0; j < freehandPoints1.length; j++) {
    if (j > 0) {
      const P3 = freehandPoints1[j - 1];
      const P4 = freehandPoints1[j];

      adjustCommonPoint(P1, P2, P3, P4);
      const intersection = lineIntersection(P1, P2, P3, P4);
      if (intersection) {
        const distance = distanceBetweenPoints(P1, intersection);
        if (
          !indices.some(
            index => index.distance.toFixed(2) === distance.toFixed(2)
          )
        ) {
          indices.push({
            index: j,
            distance: distance,
            intersection,
          });
        }
      }
    }
  }
  return indices.sort((i1, i2) => i1.distance - i2.distance);
}

/**
 * Return offset point
 * @param {*} intersection Intersection line coordinates
 * @param {*} P1 first point coordinates
 * @param {*} P2 second point coordinates
 * @param {*} oddIntersection true intersects odd times
 * @param {*} offset distance between existing line and intersection point
 * @returns
 */
function getOffsetPoint(intersection, P1, P2, oddIntersection, offset = 0.5) {
  const P5 = {
    x: P2.x - P1.x,
    y: P2.y - P1.y,
  };

  let vector = new Vector3(P5.x, P5.y, 0);
  vector = vector.normalize();
  const P6 = {
    x: intersection.x + vector.x * offset,
    y: intersection.y + vector.y * offset,
  };

  const P7 = {
    x: intersection.x - vector.x * offset,
    y: intersection.y - vector.y * offset,
  };

  const distSquared1 = distanceSquared(P1, P6);
  const distSquared2 = distanceSquared(P1, P7);

  // When oddIntersection is false we take nearest point to P1.
  // When oddIntersection is true we take furthest point to P1.
  if (distSquared1 >= distSquared2 !== oddIntersection) {
    return P7;
  } else {
    return P6;
  }
}

/**
 * Return offset points
 * @param {*} intersection Intersection line coordinates
 * @param {*} P1 first point coordinates
 * @param {*} P2 second point coordinates
 * @param {*} oddIntersection true intersects odd times
 * @param {*} offset distance between existing line and intersection point
 * @returns
 */
function getOffsetPointForClosedFreehand(
  intersection,
  P1,
  P2,
  oddIntersection,
  offset = 0.5
) {
  const P5 = {
    x: P2.x - P1.x,
    y: P2.y - P1.y,
  };

  let vector = new Vector3(P5.x, P5.y, 0);
  vector = vector.normalize();
  const P6 = {
    x: intersection.x + vector.x * offset,
    y: intersection.y + vector.y * offset,
  };

  const P7 = {
    x: intersection.x - vector.x * offset,
    y: intersection.y - vector.y * offset,
  };

  const distSquared1 = distanceSquared(P1, P6);
  const distSquared2 = distanceSquared(P1, P7);

  if (distSquared1 > distSquared2 !== oddIntersection) {
    return [P7, P6];
  } else {
    return [P6, P7];
  }
}

/**
 * To delete measurement
 * @param {*} measurementData measurement data to delete
 */
function deleteMeasurement(measurementData) {
  OHIF.measurements.MeasurementHandlers.onRemoved({
    detail: {
      toolType: measurementData.toolType,
      measurementData: {
        _id: measurementData._id,
        lesionNamingNumber: measurementData.lesionNamingNumber,
        measurementNumber: measurementData.measurementNumber,
      },
    },
  });
}

/**
 * Check whether measurement2 is completely inside the measurement1
 * @param {*} measurement1
 * @param {*} toolType1
 * @param {*} measurement2
 * @param {*} toolType2
 * @param {*} imageDimension
 * @returns True if one point of measurement2 points is outside of measurement1
 */
function checkMeasurementOutside(
  measurement1,
  toolType1,
  measurement2,
  toolType2,
  imageDimension
) {
  let measurementPoints1;
  let measurementPoints2;
  if (
    toolType1 === roiTools.OpenFreehandRoi ||
    toolType2 === roiTools.OpenFreehandRoi
  ) {
    return true;
  }
  switch (toolType1) {
    case roiTools.RectangleRoi:
      const rectangleHandles = measurement1.handles;
      const rectangleData = getRectangleData(
        rectangleHandles.start,
        rectangleHandles.end,
        imageDimension,
        rectangleHandles.initialRotation
      );

      measurementPoints1 = rectangleData.points;
      break;

    case roiTools.EllipticalRoi:
      const ellipseHandles = measurement1.handles;
      const ellipseData = getEllipseData(
        ellipseHandles.start,
        ellipseHandles.end,
        imageDimension,
        ellipseHandles.initialRotation,
        9
      );
      measurementPoints1 = ellipseData.linePoints;
      break;

    case roiTools.FreehandRoi:
      measurementPoints1 = measurement1.handles.points;
      break;
  }

  switch (toolType2) {
    case roiTools.RectangleRoi:
      const rectangleHandles = measurement2.handles;
      const rectangleData = getRectangleData(
        rectangleHandles.start,
        rectangleHandles.end,
        imageDimension,
        rectangleHandles.initialRotation
      );

      measurementPoints2 = rectangleData.points;
      break;

    case roiTools.EllipticalRoi:
      const ellipseHandles = measurement2.handles;
      const ellipseData = getEllipseData(
        ellipseHandles.start,
        ellipseHandles.end,
        imageDimension,
        ellipseHandles.initialRotation,
        9
      );
      measurementPoints2 = ellipseData.linePoints;
      break;

    case roiTools.FreehandRoi:
      measurementPoints2 = measurement2.handles.points;
      break;
  }
  let measurementOutside = false;
  for (let i = 0; i < measurementPoints2.length; i++) {
    if (!isPointInsideFreehand(measurementPoints1, measurementPoints2[i])) {
      measurementOutside = true;
      break;
    }
  }
  return measurementOutside;
}

function adjustCommonPoint(P1, P2, P3, P4) {
  const adjustment = 0.1;
  if ((P1.x === P3.x && P1.y === P3.y) || (P1.x === P4.x && P1.y === P4.y)) {
    P1.x += adjustment;
    P1.y += adjustment;
  } else if (
    (P2.x === P3.x && P2.y === P3.y) ||
    (P2.x === P4.x && P2.y === P4.y)
  ) {
    P2.x += adjustment;
    P2.y += adjustment;
  } else {
    return false;
  }
}

const distanceSquared = (P1, P2) =>
  (P1.x - P2.x) * (P1.x - P2.x) + (P1.y - P2.y) * (P1.y - P2.y);
const distanceBetweenPoints = (P1, P2) => Math.sqrt(distanceSquared(P1, P2));

export default {
  autoAdjustMeasurement,
};
