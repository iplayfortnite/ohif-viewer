import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import * as unitUtils from './unit';
import * as TextBoxContentUtils from './textBoxContent';
import moveHandle from './utils/MoveHandle.js';

import commonToolUtils from './commonUtils';

import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import measurementToolUtils from './utils';
import { shouldEnableAnnotationTool } from './utils/shouldEnableAnnotationTool';

const { setNextPalette } = FlywheelCommonRedux.actions;

const {
  findToolIndex,
  saveActionState,
  upOrEndEventsManager,
} = commonToolUtils;

// set an alias
const CornerstoneLengthTool = csTools.LengthTool;

// Drawing
const getNewContext = csTools.importInternal('drawing/getNewContext');
const draw = csTools.importInternal('drawing/draw');
const setShadow = csTools.importInternal('drawing/setShadow');
const drawLine = csTools.importInternal('drawing/drawLine');
const drawHandles = csTools.importInternal('drawing/drawHandles');
const drawLinkedTextBox = csTools.importInternal('drawing/drawLinkedTextBox');

// State
const getToolState = csTools.getToolState;
const toolColors = csTools.toolColors;
const toolStyle = csTools.toolStyle;
const { state } = csTools.store;

// Util
const getPixelSpacing = csTools.importInternal('util/getPixelSpacing');

// State
const getModule = csTools.getModule;

/**
 * Improvement on csTool Length tool to use FLYW custom measurement label implementation
 * renderToolData method is similar to its original version (csTools)
 *
 * @public
 * @class LengthTool
 * @memberof Tools.Annotation
 * @classdesc Tool for measuring distances
 * @extends CornerstoneLengthTool
 */
export default class LengthTool extends CornerstoneLengthTool {
  static toolName = 'LengthTool';

  constructor(props = {}) {
    super(props);
    this._endDrawing = this._endDrawing.bind(this);
  }
  createNewMeasurement(eventData) {
    const colorPalette = store.getState().colorPalette;
    const data = super.createNewMeasurement(eventData);
    const color = colorPalette.roiColor.next;
    store.dispatch(setNextPalette());
    return Object.assign({}, data, { color: color });
  }

  // Overwrite method to use custom Unit strategy
  renderToolData(evt) {
    const eventData = evt.detail;
    const {
      handleRadius,
      drawHandlesOnHover,
      hideHandlesIfMoving,
      renderDashed,
    } = this.configuration;
    const toolData = getToolState(evt.currentTarget, this.name);

    if (!toolData) {
      return;
    }

    // We have tool data for this element - iterate over each one and draw it
    const context = getNewContext(eventData.canvasContext.canvas);
    const { image, element } = eventData;
    const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(image);

    const lineWidth = toolStyle.getToolWidth();
    const lineDash = getModule('globalConfiguration').configuration.lineDash;

    // Meta
    const seriesModule =
      cornerstone.metaData.get('generalSeriesModule', image.imageId) || {};
    const hasPixelSpacing = rowPixelSpacing && colPixelSpacing;
    const modality = seriesModule.modality;

    for (let i = 0; i < toolData.data.length; i++) {
      const data = toolData.data[i];

      if (data.visible === false) {
        continue;
      }

      draw(context, context => {
        // Configurable shadow
        setShadow(context, this.configuration);

        let color = data.active
          ? toolColors.getActiveColor()
          : data.color || toolColors.getColorIfActive(data);
        color = unitUtils.convertRgbToRgba(color, data.color);
        color = color.substring(0, color.lastIndexOf(',')) + ', 1)';
        if (measurementToolUtils.hasColorIntensity(data)) {
          color = measurementToolUtils.reduceColorOpacity(color);
        }

        const lineOptions = { color };

        if (renderDashed) {
          lineOptions.lineDash = lineDash;
        }

        // Draw the measurement line
        drawLine(
          context,
          element,
          data.handles.start,
          data.handles.end,
          lineOptions
        );

        // Draw the handles
        const handleOptions = {
          color,
          handleRadius,
          drawHandlesIfActive: drawHandlesOnHover,
          hideHandlesIfMoving,
        };

        if (this.configuration.drawHandles) {
          drawHandles(context, eventData, data.handles, handleOptions);
        }

        if (!data.handles.textBox.hasMoved) {
          const coords = {
            x: Math.max(data.handles.start.x, data.handles.end.x),
          };

          // Depending on which handle has the largest x-value,
          // Set the y-value for the text box
          if (coords.x === data.handles.start.x) {
            coords.y = data.handles.start.y;
          } else {
            coords.y = data.handles.end.y;
          }

          data.handles.textBox.x = coords.x;
          data.handles.textBox.y = coords.y;
        }

        // Move the textbox slightly to the right and upwards
        // So that it sits beside the length tool handle
        const xOffset = 10;

        // Update textbox stats
        if (data.invalidated === true) {
          if (data.length) {
            this.throttledUpdateCachedStats(image, element, data);
          } else {
            this.updateCachedStats(image, element, data);
          }
        }

        if (
          unitUtils.isMeasurementOutputHidden() ||
          (data.hasParallels && unitUtils.isParallelMeasurementOutputHidden())
        ) {
          // Temporary status flag to indicate measurement visibility in
          // viewport, not to be saved in server.
          data.handles.textBox.isVisible = false;
          data.handles.textBox.boundingBox = TextBoxContentUtils.getDefaultBoundingBox(
            data.handles.textBox
          );
        } else {
          const textBoxAnchorPoints = handles =>
            TextBoxContentUtils.findTextBoxAnchorPoints(
              handles.start,
              handles.end
            );

          const textBoxContent = this.textBoxContent(data, hasPixelSpacing);

          drawLinkedTextBox(
            context,
            element,
            data.handles.textBox,
            textBoxContent,
            data.handles,
            textBoxAnchorPoints,
            color,
            lineWidth,
            xOffset,
            true
          );
        }
      });
    }
  }

  textBoxContent(data, hasPixelSpacing) {
    const unit = unitUtils.getLengthUnit(hasPixelSpacing, data.length);
    const shouldConvertToMicro = unitUtils.shouldConvertToMicrometer(
      data.length
    );
    const mmToMicronConversionFactor = 1000;
    const length =
      shouldConvertToMicro && data.length
        ? data.length * mmToMicronConversionFactor
        : data.length;
    const formattedValue = data.length
      ? TextBoxContentUtils.common.formatValue(length)
      : '';

    const textBoxContent = formattedValue ? `${formattedValue} ${unit}` : '';
    return textBoxContent;
  }

  pointNearTool(element, data, coords, skipReadOnly = false) {
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (
      (data.readonly && !skipReadOnly) ||
      !data.visible ||
      !isAnnotationToolEnabled
    ) {
      return false;
    }
    return super.pointNearTool(element, data, coords);
  }

  /**
   * Custom callback for when a handle is selected.
   * @method handleSelectedCallback
   * @memberof Tools.Base.BaseAnnotationTool
   *
   * @param  {*} evt    -
   * @param  {*} toolData   -
   * @param  {*} handle - The selected handle.
   * @param  {String} interactionType -
   * @returns {void}
   */
  handleSelectedCallback(evt, toolData, handle, interactionType = 'mouse') {
    const eventData = evt.detail;
    const { element } = eventData;
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (!toolData.readonly && toolData.visible && isAnnotationToolEnabled) {
      moveHandleNearImagePoint(evt, this, toolData, handle, interactionType);
      this._initializeDrawing(evt, handle, interactionType);
    }
  }

  /**
   * Custom callback for when a tool is selected.
   *
   * @method toolSelectedCallback
   * @memberof Tools.Base.BaseAnnotationTool
   *
   * @param  {*} evt
   * @param  {*} annotation
   * @param  {string} [interactionType=mouse]
   * @returns {void}
   */
  toolSelectedCallback(evt, annotation, interactionType = 'mouse') {
    if (!annotation.readonly && annotation.visible) {
      super.toolSelectedCallback(evt, annotation, interactionType);
      this._initializeDrawing(evt, annotation.handles?.start, interactionType);
    }
  }

  /**
   * Initialize the drawing loop when tool is active and a click event happens.
   *
   * @private
   * @param {Object} evt - The event.
   * @param  {*} handle - Selected handle or any of the handle.
   * @param  {string} [interactionType=mouse]
   * @returns {void}
   */
  _initializeDrawing(evt, handle, interactionType = 'mouse') {
    const { element } = evt.detail;
    const currentTool = findToolIndex(element, this.name, handle);
    if (currentTool >= 0) {
      this.configuration.currentTool = currentTool;
      this._startDrawing(evt);
      upOrEndEventsManager.registerHandler(
        element,
        interactionType,
        this._endDrawing
      );
    }
  }

  /**
   * Beginning of drawing loop when tool is active and a click event happens.
   *
   * @private
   * @param {Object} evt - The event.
   * @returns {void}
   */
  _startDrawing(evt) {
    const { element } = evt.detail;
    const toolState = getToolState(element, this.name);
    const data = toolState.data[this.configuration.currentTool];
    saveActionState(data, 'measurements', 'Modify', true, false);
  }

  /**
   * Ends the active drawing loop and completes the polygon.
   *
   * @private
   * @param {Object} evt - The event.
   * @returns {void}
   */
  _endDrawing(evt) {
    const { element } = evt.detail;
    const interactionType = upOrEndEventsManager.getCurrentInteractionType(evt);
    upOrEndEventsManager.removeHandler(
      element,
      interactionType,
      this._endDrawing
    );
    const toolState = getToolState(element, this.name);
    const data = toolState.data[this.configuration.currentTool];
    saveActionState(data, 'measurements', 'Modify', false, true);
    this.configuration.currentTool = -1;
  }

  /** Ends the active drawing loop forcefully.
   *
   * @public
   * @param {Object} element - The element on which the roi is being drawn.
   * @returns {null}
   */
  finishDrawing(element) {
    const evt = { detail: { element } };
    if (this.configuration.currentTool >= 0) {
      this._endDrawing(evt);
    }
  }

  getTextInfo(measurementData, data = null) {
    const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(data.image);
    const hasPixelSpacing = rowPixelSpacing && colPixelSpacing;
    const textBoxContent = this.textBoxContent(
      measurementData,
      hasPixelSpacing
    );
    return [`Length : ${textBoxContent}`];
  }
}

// Same function as it is implemented in cornerstone tools.
const moveHandleNearImagePoint = function(
  evt,
  tool,
  toolData,
  handle,
  interactionType
) {
  toolData.active = true;
  state.isToolLocked = true;
  if (toolData.hasParallels) {
    tool.options.restrictOrientationChange = true;
  } else {
    tool.options.restrictOrientationChange = false;
  }

  moveHandle(
    evt.detail,
    tool.name,
    toolData,
    handle,
    tool.options,
    interactionType
  );

  evt.stopImmediatePropagation();
  evt.stopPropagation();
  evt.preventDefault();
};
