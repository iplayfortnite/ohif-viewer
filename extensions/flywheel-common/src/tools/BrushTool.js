import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import store from '@ohif/viewer/src/store';
import commonToolUtils from './commonUtils';
import { shouldEnableAnnotationTool } from './utils/shouldEnableAnnotationTool';
import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
import { getSmartCTRangeList, getDefaultSmartCTRangeList } from '../utils';

const CornerstoneBrushTool = csTools.BrushTool;
const { saveActionState, segmentationRangeManager } = commonToolUtils;
const segmentationModule = csTools.getModule('segmentation');
const { getCircle } = csTools.importInternal('util/segmentationUtils');
const { setActiveTool } = FlywheelCommonRedux.actions;
/**
 * @public
 * @class BrushTool
 * @memberof Tools.Brush
 * @classdesc Tool for drawing segmentations on an image.
 * @extends BrushTool
 */
export default class BrushTool extends CornerstoneBrushTool {
  static toolName = 'BrushTool';

  constructor(props = {}) {
    const defaultProps = {
      name: 'Brush',
      supportedInteractionTypes: ['Mouse', 'Touch'],
      configuration: {},
      mixins: ['renderBrushMixin'],
    };

    super(props, defaultProps);
    this.updateOnMouseMove = true;
    this.name = 'Brush';
    const defaultKey = [',', '.'];
    const state = store.getState();
    const hotKeyConfigFn = state?.flywheel?.projectConfig?.hotkeys;

    const increaseBrushSizeHotKey = hotKeyConfigFn?.find(
      hotKeys => hotKeys.commandName === 'increaseBrushSize'
    );
    const decreaseBrushSizeHotKey = hotKeyConfigFn?.find(
      hotKeys => hotKeys.commandName === 'decreaseBrushSize'
    );

    const decreaseBrushSizeKey = decreaseBrushSizeHotKey?.keys || defaultKey[0];
    const increaseBrushSizeKey = increaseBrushSizeHotKey?.keys || defaultKey[1];

    Mousetrap.unbind(decreaseBrushSizeKey, 'keypress');
    Mousetrap.unbind(increaseBrushSizeKey, 'keypress');

    Mousetrap.bind(
      decreaseBrushSizeKey,
      e => {
        this.decreaseBrushSize();
        cornerstone.getEnabledElements().forEach(enabledElement => {
          if (enabledElement.image) {
            cornerstone.updateImage(enabledElement.element);
          }
        });
      },
      'keypress'
    );

    Mousetrap.bind(
      increaseBrushSizeKey,
      e => {
        this.increaseBrushSize();
        cornerstone.getEnabledElements().forEach(enabledElement => {
          if (enabledElement.image) {
            cornerstone.updateImage(enabledElement.element);
          }
        });
      },
      'keypress'
    );
  }

  preMouseDownCallback(evt) {
    const eventData = evt.detail;
    const { element } = eventData;
    const toolState = csTools.getToolState(element, this.name);
    if (!toolState) {
      csTools.addToolState(element, this.name, { data: [0] });
    }
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);
    if (!isAnnotationToolEnabled) {
      return true;
    }

    super.preMouseDownCallback(evt);
    return true;
  }

  getViewportIndex(element) {
    const state = store.getState();
    const viewports = state?.viewports?.layout?.viewports;
    let viewportIndex = 0;

    viewports.some((viewport, index) => {
      const iteratedElement = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
        index
      );
      if (iteratedElement === element) {
        viewportIndex = index;
        return true;
      }
    });
    return viewportIndex;
  }

  renderToolData(evt) {
    const eventData = evt.detail;
    const element = eventData.element;
    const state = store.getState();
    const activeTool = state.infusions.activeTool;

    if (activeTool === this.name) {
      if (shouldEnableAnnotationTool(element)) {
        this.renderBrush(evt);
      } else {
        super.renderToolData(evt);
      }
    }
  }

  mouseMoveCallback(evt) {
    const eventData = evt.detail;
    const { element } = eventData;
    const state = store.getState();
    const activeTool = state.infusions.activeTool;

    if (activeTool === this.name) {
      const viewportIndex = this.getViewportIndex(element);

      store.dispatch(setActiveTool(null, viewportIndex));

      const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);
      super.mouseMoveCallback(evt);
      if (!isAnnotationToolEnabled) {
        cornerstone.updateImage(element);
      }
    }
  }

  /**
   * Initialize the label map for drawing segment with brush
   * Note: The 0th index in label map preserved for brush tool and remaining
   * for the loaded segmentation files
   * @param {*} element
   */
  initializeBrushLabelMap(element) {
    const { configuration } = segmentationModule;

    /* Calling getter to set `state.series` object, to avoid
       brushStackState from been undefined on initial painting  */
    segmentationRangeManager.activateDefaultSegmentation(element);

    const toolState = cornerstoneTools.getToolState(element, 'stack');
    const stackData = toolState.data[0];
    const imageId = stackData.imageIds[0];
    const defaultAlpha = 0.2;
    if (!configuration.fillAlphaPerLabelMap[imageId]) {
      configuration.fillAlphaPerLabelMap[imageId] = [defaultAlpha];
    } else {
      configuration.fillAlphaPerLabelMap[imageId][0] = defaultAlpha;
    }
  }

  getWindowRange(evt, pointerArray, data, radius) {
    const state = store.getState();
    const smartCT = state.smartCT;
    const selectedRange = smartCT?.selectedRange;
    if (selectedRange) {
      return selectedRange;
    }
    const defaultRange = getDefaultSmartCTRangeList();
    return defaultRange;
  }

  _startPainting(evt) {
    const state = store.getState();
    const { configuration } = segmentationModule;
    const radius = configuration.radius;
    const eventData = evt.detail;
    const { rows, columns } = eventData.image;
    const brushCenter = {
      x: Math.floor(eventData.currentPoints.image.x),
      y: Math.floor(eventData.currentPoints.image.y),
    };

    let pointerArray = getCircle(
      radius,
      rows,
      columns,
      brushCenter.x,
      brushCenter.y
    );
    const allRanges = getSmartCTRangeList();

    const smartCTRange = this.getWindowRange(
      evt,
      pointerArray,
      brushCenter,
      radius
    );
    const segementationIndex = allRanges.findIndex(
      r => r.label === smartCTRange.label
    );
    this.initializeBrushLabelMap(eventData.element);

    if (segementationIndex !== -1) {
      segmentationModule.setters.activeSegmentIndex(
        eventData.element,
        segementationIndex + 1
      );
    }
    super._startPainting(evt);
  }

  _endPainting(evt) {
    const { element } = evt.detail;
    const toolState = csTools.getToolState(element, 'stack');
    const toolData = toolState.data[0];
    const index = segmentationModule.getters.activeLabelmapIndex(element);
    const activeViewport = store.getState().viewports.viewportSpecificData[0];
    const studyUid = activeViewport.StudyInstanceUID;
    const seriesUid = activeViewport.SeriesInstanceUID;
    const data = {
      toolType: this.name,
      StudyInstanceUID: studyUid,
      SeriesInstanceUID: seriesUid,
      toolData: toolData,
      element: element,
      labelmapIndex: index,
    };
    saveActionState(data, 'segments', 'Add', false, false);
    super._endPainting(evt);
  }
}
