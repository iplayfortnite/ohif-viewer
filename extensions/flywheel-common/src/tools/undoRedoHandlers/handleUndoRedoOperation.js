import cornerstoneTools from 'cornerstone-tools';
import OHIF from '@ohif/core';
import store from '@ohif/viewer/src/store';
import handleMeasurementUndoOperation from './handleMeasurementUndoOperation';
import handleMeasurementRedoOperation from './handleMeasurementRedoOperation';
import {
  handleRedoOperation,
  handleUndoOperation,
} from './handleSegmentUndoRedoOperation';
import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';

const scrollToIndex = cornerstoneTools.import('util/scrollToIndex');
const triggerEvent = cornerstoneTools.importInternal('util/triggerEvent');

const {
  undoLastActionState,
  redoNextActionState,
} = FlywheelCommonRedux.actions;

/**
 * Get the display set with meta data information
 * @param {string} studyInstanceUID
 * @param {string} sopInstanceUID
 * @returns {Object} Identified display set else null
 */
const getDisplaySet = (studyInstanceUID, sopInstanceUID) => {
  const study = OHIF.utils.studyMetadataManager.get(studyInstanceUID);
  if (!study) {
    OHIF.log.error(
      'Study not found when scrolling to image in study: ' + studyInstanceUID
    );
    return null;
  }

  const displaySet = study.findDisplaySet(displaySet =>
    displaySet.images?.find(
      image => image.getSOPInstanceUID() === sopInstanceUID
    )
  );

  return displaySet;
};

const getActiveElement = (action, viewports) => {
  const { StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID } = action.data;
  let viewportIndex = viewports.activeViewportIndex;
  const viewportSpecificData = viewports.viewportSpecificData[viewportIndex];
  if (
    StudyInstanceUID !== viewportSpecificData.StudyInstanceUID ||
    SeriesInstanceUID !== viewportSpecificData.SeriesInstanceUID
  ) {
    const key = Object.keys(viewports.viewportSpecificData).find(
      key =>
        StudyInstanceUID ===
          viewports.viewportSpecificData[key].StudyInstanceUID &&
        SeriesInstanceUID ===
          viewports.viewportSpecificData[key].SeriesInstanceUID &&
        viewports.viewportSpecificData[key].plugin.includes('cornerstone')
    );
    if (key === undefined) {
      return null;
    }
    viewportIndex = parseInt(key);
  }

  return FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(viewportIndex);
};

const triggerUndoRedoUpdateCallback = (
  action,
  element,
  isUndo,
  modifiedProperty
) => {
  if (!element) {
    return;
  }
  const { data, actionType } = action;
  const eventName = data.toolType + 'measurementpropertychangeevent';
  let eventType = 'Modify';
  if (isUndo) {
    eventType =
      actionType === 'Add'
        ? 'Delete'
        : actionType === 'Delete'
        ? 'Initialize'
        : 'Modify';
  } else {
    eventType =
      actionType === 'Add'
        ? 'Initialize'
        : actionType === 'Delete'
        ? 'Delete'
        : 'Modify';
  }
  triggerEvent(element, eventName, {
    measurementData: data,
    eventType,
    operationData: {
      type: isUndo ? 'Undo' : 'Redo',
      modifiedProperty,
    },
  });
};

/**
 * Get the matching slice and corresponding viewport information for the action
 *
 * @param {Object} action - action state information
 * @param {Object} viewports - store viewport state
 * @returns {Object} Matching slice information including slice number, viewport etc
 */
const getActionMatchingSliceInfo = (action, viewports) => {
  const { StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID } = action.data;
  let sliceNumber = action.data.sliceNumber;
  const matchingData = {
    viewportIndex: -1,
    sliceIndex: -1,
    studyInstanceUID: StudyInstanceUID,
    seriesInstanceUID: SeriesInstanceUID,
    sopInstanceUID: SOPInstanceUID,
  };

  if (isNaN(sliceNumber)) {
    const displaySet = getDisplaySet(StudyInstanceUID, SOPInstanceUID);
    if (!displaySet) {
      return matchingData;
    }

    sliceNumber = -1;
    if (
      displaySet.isMultiFrame &&
      displaySet.numImageFrames > 1 &&
      displaySet.frameIndex > 1
    ) {
      sliceNumber = displaySet.frameIndex;
    }
    if (sliceNumber < 1) {
      const sliceIndex = displaySet.images.findIndex(
        image => image.SOPInstanceUID === SOPInstanceUID
      );
      sliceNumber = sliceIndex >= 0 ? sliceIndex + 1 : 1;
    }
  }

  let viewportIndex = viewports.activeViewportIndex;
  const viewportSpecificData = viewports.viewportSpecificData[viewportIndex];
  if (
    StudyInstanceUID !== viewportSpecificData.StudyInstanceUID ||
    SeriesInstanceUID !== viewportSpecificData.SeriesInstanceUID
  ) {
    const key = Object.keys(viewports.viewportSpecificData).find(
      key =>
        StudyInstanceUID ===
          viewports.viewportSpecificData[key].StudyInstanceUID &&
        SeriesInstanceUID ===
          viewports.viewportSpecificData[key].SeriesInstanceUID &&
        viewports.viewportSpecificData[key].plugin.includes('cornerstone')
    );
    if (key === undefined) {
      matchingData.sliceIndex = sliceNumber - 1;
      matchingData.viewportIndex = viewportIndex;
      return matchingData;
    }
    viewportIndex = parseInt(key);
  }

  const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
    viewportIndex
  );
  if (!element) {
    matchingData.sliceIndex = sliceNumber - 1;
    matchingData.viewportIndex = viewportIndex;
    return matchingData;
  }

  const toolState = cornerstoneTools.getToolState(element, 'stack');
  const stackData = toolState.data[0];
  if (sliceNumber - 1 !== stackData.currentImageIdIndex) {
    matchingData.sliceIndex = sliceNumber - 1;
    matchingData.viewportIndex = viewportIndex;
    return matchingData;
  }

  return matchingData;
};

/**
 * Scroll to the slice as per the information in sliceInfo, also set the displaySet in
 * active viewport if it is not already set
 *
 * @param {Object} sliceInfo - Slice information for scrolling to the corresponding slice
 * @returns {void}
 */
const jumpToImage = sliceInfo => {
  const displaySet = getDisplaySet(
    sliceInfo.studyInstanceUID,
    sliceInfo.sopInstanceUID
  );
  if (!displaySet) {
    return;
  }
  displaySet.SOPInstanceUID = sliceInfo.sopInstanceUID;
  displaySet.frameIndex = sliceInfo.sliceIndex;

  store.dispatch(
    OHIF.redux.actions.setViewportSpecificData(
      sliceInfo.viewportIndex,
      displaySet
    )
  );
  if (sliceInfo.sliceIndex > -1) {
    const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
      sliceInfo.viewportIndex
    );
    if (element) {
      scrollToIndex(element, sliceInfo.sliceIndex);
    }
  }
};

/**
 * Handle the undo/redo operation
 * @param {boolean} isUndo - if true, then perform undo or else do redo
 * @param {Object} viewports - store viewport state
 */
export default function(isUndo, viewports) {
  const undoRedoActionState = store.getState().undoRedoActionState;
  const currentIndex = undoRedoActionState.currentIndex;

  if (isUndo) {
    const postUndoIndex = currentIndex - 1;
    if (0 <= postUndoIndex) {
      const actionState = undoRedoActionState.actionStates[postUndoIndex];
      const lastAction = actionState?.start;
      const sliceInfo = getActionMatchingSliceInfo(lastAction, viewports);

      if (sliceInfo.viewportIndex === -1 && sliceInfo.sliceIndex === -1) {
        store.dispatch(undoLastActionState());
        if (lastAction.type === 'measurements') {
          handleMeasurementUndoOperation(lastAction);
          const element = getActiveElement(lastAction, viewports);
          if (element) {
            triggerUndoRedoUpdateCallback(lastAction, element, isUndo);
          }
        }
        if (lastAction.type == 'segments') {
          handleUndoOperation(lastAction);
        }
      } else {
        jumpToImage(sliceInfo);
      }
    }
  } else {
    if (undoRedoActionState.actionStates.length >= currentIndex + 1) {
      const actionState = undoRedoActionState.actionStates[currentIndex];
      const nextAction = actionState?.end || actionState?.start;
      const prevAction = actionState?.start;
      const sliceInfo = getActionMatchingSliceInfo(nextAction, viewports);

      if (sliceInfo.viewportIndex === -1 && sliceInfo.sliceIndex === -1) {
        if (nextAction.type === 'measurements') {
          const element = getActiveElement(nextAction, viewports);
          // Special handling for contour roi measurement for redo of interpolated roi change
          // to key roi by editing/sculpting, this is because the auto creation action of the
          // interpolated rois will not be available in the undo-redo-state
          let modifiedProperty = null;
          if (
            nextAction?.data &&
            prevAction?.data &&
            !!prevAction.data.interpolated !== !!nextAction.data.interpolated
          ) {
            modifiedProperty = 'interpolated';
          }
          handleMeasurementRedoOperation(nextAction);
          if (element) {
            triggerUndoRedoUpdateCallback(
              nextAction,
              element,
              isUndo,
              modifiedProperty
            );
          }
        }
        if (nextAction.type == 'segments') {
          handleRedoOperation(nextAction);
        }
        store.dispatch(redoNextActionState());
      } else {
        jumpToImage(sliceInfo);
      }
    }
  }
}
