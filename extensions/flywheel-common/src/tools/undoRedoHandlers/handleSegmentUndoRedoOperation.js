import csTools from 'cornerstone-tools';

const segmentationModule = csTools.getModule('segmentation');

export function handleUndoOperation(lastAction) {
  segmentationModule.setters.undo(
    lastAction.data.element,
    lastAction.data.labelmapIndex
  );
}

export function handleRedoOperation(nextAction) {
  segmentationModule.setters.redo(
    nextAction.data.element,
    nextAction.data.labelmapIndex
  );
}
