import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import cornerstoneMath from 'cornerstone-math';
import store from '@ohif/viewer/src/store';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';

import getProximityCursorDiameter from './utils/getProximityCursorDiameter';
import {
  setToolCursor,
  hideToolCursor,
  isCursorExist,
} from './utils/setToolCursor';

const {
  addToolState,
  clearToolState,
  importInternal,
  getToolState,
  toolColors,
} = csTools;
const { selectActiveTool } = FlywheelCommonRedux.selectors;

// Cornerstone 3rd party dev kit imports
const getNewContext = importInternal('drawing/getNewContext');
const draw = importInternal('drawing/draw');
const drawCircle = importInternal('drawing/drawCircle');
const BaseAnnotationTool = importInternal('base/BaseAnnotationTool');
const getPixelSpacing = csTools.importInternal('util/getPixelSpacing');
const { state } = csTools.store;

/**
 * @class CircleCursorTool - Render the Circle Cursor
 * @extends cornerstoneTools.BaseAnnotationTool
 */
export default class CircleCursorTool extends BaseAnnotationTool {
  constructor(props = {}) {
    const defaultProps = {
      name: 'CircleCursorTool',
      supportedInteractionTypes: ['Mouse'],
    };

    const initialProps = Object.assign(defaultProps, props);
    super(initialProps);
    this.updateOnMouseMove = true;
    this._drawing = false;
    this.isMouseMoveEnabledForPassive = false;
    this.referencedToolNames = [
      'RectangleRoi',
      'FreehandRoi',
      'EllipticalRoi',
      'CircleRoi',
      'OpenFreehandRoi',
      'ContourRoi',
      'Zoom',
      'Pan',
    ];
    this.registerEvents = this.registerEvents.bind(this);
    this.unRegisterEvents = this.unRegisterEvents.bind(this);
    this.onMouseOut = this.onMouseOut.bind(this);
    this._onNewImageListener = this._onNewImageListener.bind(this);
    this.onMouseDown = this.onMouseDown.bind(this);
  }

  hasPixelSpacing(eventData) {
    const { image } = eventData;
    const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(image);
    return rowPixelSpacing && colPixelSpacing;
  }

  addNewMeasurement(evt, interactionType) {
    const eventData = evt.detail;
    if (!this.hasPixelSpacing(eventData)) {
      return;
    }
    if (!this._drawing && evt.detail.image) {
      const element = evt.detail.element;
      cornerstone.events.removeEventListener(
        cornerstone.EVENTS.ELEMENT_ENABLED,
        this.registerEvents
      );
      cornerstone.events.addEventListener(
        cornerstone.EVENTS.ELEMENT_ENABLED,
        this.registerEvents
      );
      cornerstone.events.removeEventListener(
        cornerstone.EVENTS.ELEMENT_DISABLED,
        this.unRegisterEvents
      );
      cornerstone.events.addEventListener(
        cornerstone.EVENTS.ELEMENT_DISABLED,
        this.unRegisterEvents
      );
      this.setToolState(evt.detail, element);
      this._startDrawing(evt);
      cornerstone.updateImage(element);
      this.registerEvents();
    }
  }

  /**
   * Creates a new annotation.
   *
   * @method createNewMeasurement
   * @memberof Tools.Base.BaseAnnotationTool
   *
   * @param  {type} evt description
   * @returns {type}     description
   */
  createNewMeasurement(eventData) {
    return {
      visible: true,
      active: false,
      center: {
        x: eventData.currentPoints?.image.x || 0,
        y: eventData.currentPoints?.image.y || 0,
      },
      circumference: {
        x: eventData.currentPoints?.image.x || 0,
        y: eventData.currentPoints?.image.y || 0,
      },
    };
  }

  /**
   * Register viewport events
   * @param {Object} evt - The event.
   */
  registerEvents(evt) {
    cornerstone.getEnabledElements().forEach(enabledElement => {
      if (
        evt?.detail?.element &&
        enabledElement.element !== evt?.detail?.element
      ) {
        return;
      }
      enabledElement.element.removeEventListener('mouseout', this.onMouseOut);
      enabledElement.element.addEventListener('mouseout', this.onMouseOut);
      enabledElement.element.removeEventListener(
        cornerstone.EVENTS.NEW_IMAGE,
        this._onNewImageListener
      );
      enabledElement.element.addEventListener(
        cornerstone.EVENTS.NEW_IMAGE,
        this._onNewImageListener
      );
      enabledElement.element.removeEventListener(
        csTools.EVENTS.MOUSE_DOWN,
        this.onMouseDown
      );
      enabledElement.element.addEventListener(
        csTools.EVENTS.MOUSE_DOWN,
        this.onMouseDown
      );
    });
  }

  /**
   * Un-register viewport events
   * @param {Object} evt - The event.
   */
  unRegisterEvents(evt) {
    const enabledElements = cornerstone.getEnabledElements();
    enabledElements.forEach(enabledElement => {
      if (
        evt?.detail?.element &&
        enabledElement.element !== evt?.detail?.element
      ) {
        return;
      }
      enabledElement.element.removeEventListener('mouseout', this.onMouseOut);
      enabledElement.element.removeEventListener(
        cornerstone.EVENTS.NEW_IMAGE,
        this._onNewImageListener
      );
      enabledElement.element.removeEventListener(
        csTools.EVENTS.MOUSE_DOWN,
        this.onMouseDown
      );
    });
    if (enabledElements.length === 1) {
      cornerstone.events.removeEventListener(
        cornerstone.EVENTS.ELEMENT_DISABLED,
        this.unRegisterEvents
      );
    }
  }

  /**
   *
   * Returns true if the given coords are need the tool.
   *
   * @method pointNearTool
   * @memberof Tools.Base.BaseAnnotationTool
   *
   * @param {*} element
   * @param {*} data
   * @param {*} coords
   * @param {string} [interactionType=mouse]
   * @returns {boolean} If the point is near the tool
   */
  pointNearTool(element, data, coords, interactionType = 'mouse') {
    return false;
  }

  /**
   * Beginning of drawing loop when tool is active and a click event happens.
   *
   * @private
   * @param {Object} evt - The event.
   * @returns {void}
   */
  _startDrawing(evt) {
    this._drawing = true;
    this.isMouseMoveEnabledForPassive = true;
    csTools.setToolActive('CircleCursorTool', {});
  }

  /**
   * Ends the active drawing loop and completes the polygon.
   *
   * @private
   * @param {Object} evt - The event.
   * @returns {void}
   */
  _endDrawing(evt) {
    this._drawing = false;
    this.isMouseMoveEnabledForPassive = false;
    csTools.setToolPassive('CircleCursorTool', {});
  }

  /**
   * Event handler for mouse viewport focus out event.
   *
   * @abstract
   * @event
   * @param {Object} evt - The event.
   */
  onMouseOut(evt) {
    const element = evt.currentTarget;
    this.removeCursor(element);
    cornerstone.updateImage(element);
  }

  /**
   * Event handler for MOUSE_DOWN event.
   *
   * @abstract
   * @event
   * @param {Object} evt - The event.
   */
  onMouseDown(evt) {
    const element = evt.currentTarget;
    this.removeCursor(element);
  }

  /**
   * Event handler for MOUSE_MOVE event.
   *
   * @abstract
   * @event
   * @param {Object} evt - The event.
   * @returns {boolean} - True if the image needs to be updated
   */
  mouseMoveCallback(evt) {
    const eventData = evt.detail;
    if (!this.hasPixelSpacing(eventData)) {
      return;
    }
    if (this._drawing) {
      const { currentPoints, element } = eventData;
      const activeTools = this._getActiveTools(element);
      if (!activeTools.length) {
        this.finishDrawing(element);
        return false;
      }
      const proximityCursorProperties = store.getState()?.contourProperties
        .proximityCursorProperties;
      const radiusInImage =
        getProximityCursorDiameter(evt, proximityCursorProperties) / 2;
      const toolState = getToolState(element, this.name);
      const isMouseOutSideImage = this._isMouseOutsideImage(eventData);

      if (!toolState) {
        return false;
      }

      for (let i = 0; i < toolState.data.length; i++) {
        const data = toolState.data[i];

        data.visible =
          !isMouseOutSideImage && proximityCursorProperties?.display;
        if (data.visible) {
          hideToolCursor(element);
        } else {
          this.setCurrentToolCursor(element);
        }
        data.center = {
          x: currentPoints.image.x,
          y: currentPoints.image.y,
        };
        data.circumference = {
          x: data.center.x + radiusInImage,
          y: data.center.y,
        };
      }
    }
  }

  /**
   * Event handler for NEW Image event.
   *
   * @abstract
   * @event
   * @param {Object} evt - The event.
   * @returns {Nil}
   */
  _onNewImageListener(evt) {
    const eventData = evt.detail;
    const { element } = eventData;
    const toolState = getToolState(element, this.name);
    if (!toolState) {
      const measurementData = this.createNewMeasurement(eventData);
      addToolState(element, this.name, measurementData);
    }
  }

  /** Check the mouse position is outside the image.
   *
   * @private
   * @param {Object} eventData - event data.
   * @returns {boolean}
   */
  _isMouseOutsideImage({ image, currentPoints }) {
    return (
      currentPoints.image.x <= 0 ||
      currentPoints.image.y <= 0 ||
      currentPoints.image.x >= image.width ||
      currentPoints.image.y >= image.height
    );
  }

  /** Ends the active drawing loop.
   *
   * @public
   * @param {Object} element - The element on which the cursor is being drawn.
   * @returns {null}
   */
  finishDrawing(element) {
    const evt = { detail: { element } };
    if (this._drawing) {
      cornerstone.events.removeEventListener(
        cornerstone.EVENTS.ELEMENT_ENABLED,
        this.registerEvents
      );
      cornerstone.events.removeEventListener(
        cornerstone.EVENTS.ELEMENT_DISABLED,
        this.unRegisterEvents
      );
      this._endDrawing(evt);
      const toolState = getToolState(element, this.name);

      if (!toolState) {
        return;
      }
      clearToolState(element, this.name);
      this.unRegisterEvents();
    }
  }

  /** Returns active tools on given element.
   *
   * @private
   * @param {Object} element
   * @returns {Array} active tools
   */
  _getActiveTools(element) {
    const currentTool = selectActiveTool(store.getState());
    return state.tools.filter(
      tool =>
        tool.element === element &&
        tool.name === currentTool &&
        this.referencedToolNames.includes(tool.name)
    );
  }

  /** Set Tool state for given element
   *
   * @private
   * @param {Object} eventData - event data.
   * @param {Object} element - enabled element
   */
  setToolState(eventData, element) {
    const toolState = getToolState(element, this.name);
    if (!toolState?.data?.length) {
      const measurementData = this.createNewMeasurement(eventData);
      // Associate this data with this imageId so we can render it and manipulate it
      addToolState(element, this.name, measurementData);
    }
  }

  /** Hide cursor
   *
   * @private
   * @param {Object} element - enabled element
   */
  removeCursor(element) {
    const toolState = getToolState(element, this.name);

    if (!toolState) {
      return;
    }

    for (let i = 0; i < toolState.data.length; i++) {
      const data = toolState.data[i];
      data.visible = false;
    }
  }

  /** Set cursor for current tool
   *
   * @private
   * @param {Object} element - enabled element
   */
  setCurrentToolCursor(element) {
    const currentToolName = selectActiveTool(store.getState());
    const currentTool = state?.tools.find(
      item => currentToolName === item.name
    );
    if (!isCursorExist()) {
      cornerstone.getEnabledElements().forEach(enabledElement => {
        setToolCursor(enabledElement.element, currentTool?.svgCursor);
      });
    }
  }

  /**
   *
   *
   * @param {*} evt
   * @returns {undefined}
   */
  renderToolData(evt) {
    const eventData = evt.detail;
    const { element } = eventData;
    const proximityCursorStyle = store.getState()?.contourProperties
      .proximityCursorProperties?.lineStyle;
    let toolState = getToolState(evt.currentTarget, this.name);

    if (!toolState?.data?.length) {
      const activeTools = this._getActiveTools(element);
      if (activeTools.length) {
        this.setToolState(eventData, element);
        toolState = getToolState(evt.currentTarget, this.name);
      } else {
        return;
      }
    }
    const context = getNewContext(eventData.canvasContext.canvas);
    draw(context, context => {
      // If we have tool data for this element - iterate over each set and draw it
      for (let i = 0; i < toolState.data.length; i++) {
        const data = toolState.data[i];

        if (!data.visible) {
          continue;
        }

        // Configure
        let color = toolColors.getActiveColor();

        let circleOptions;
        if (proximityCursorStyle === 'dotted') {
          circleOptions = { color, lineDash: [5, 5], lineWidth: 2 };
        } else {
          circleOptions = { color };
        }

        const startCanvas = cornerstone.pixelToCanvas(element, data.center);
        const endCanvas = cornerstone.pixelToCanvas(
          element,
          data.circumference
        );

        const radiusCanvas = Math.abs(
          cornerstoneMath.point.distance(startCanvas, endCanvas)
        );

        // Draw
        drawCircle(
          context,
          element,
          data.center,
          radiusCanvas,
          circleOptions,
          'pixel',
          0
        );
      }
    });
  }
}
