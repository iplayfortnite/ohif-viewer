import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import store from '@ohif/viewer/src/store';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';

import commonToolUtils from './commonUtils';
import * as TextBoxContentUtils from './textBoxContent';
import * as unitUtils from './unit';
import measurementToolUtils from './utils';
import { isPointInsideFreehand } from './utils/FreehandIntersect';
import { shouldEnableAnnotationTool } from './utils/shouldEnableAnnotationTool';
import { getEnabledElement } from '../utils/cornerstoneUtils';

const { setNextPalette } = FlywheelCommonRedux.actions;

const { saveActionState } = commonToolUtils;

// set an alias
const CornerstoneFreehandRoiTool = csTools.FreehandRoiTool;

// Drawing
const getNewContext = csTools.importInternal('drawing/getNewContext');
const calculateSUV = csTools.importInternal('util/calculateSUV');
const draw = csTools.importInternal('drawing/draw');
const drawJoinedLines = csTools.importInternal('drawing/drawJoinedLines');
const drawHandles = csTools.importInternal('drawing/drawHandles');
const drawLinkedTextBox = csTools.importInternal('drawing/drawLinkedTextBox');

// State
const { state } = csTools.store;
const getToolState = csTools.getToolState;
const toolColors = csTools.toolColors;
const toolStyle = csTools.toolStyle;

// Util
const getPixelSpacing = csTools.importInternal('util/getPixelSpacing');
const freehandUtils = csTools.importInternal('util/freehandUtils');

const { freehandRoiCursor } = csTools.importInternal('tools/cursors');

const { freehandArea, calculateFreehandStatistics } = freehandUtils;

// State
const getModule = csTools.getModule;

/**
 * Improvement on csTool ROI tool to use FLYW custom textBox implementation.
 * renderToolData method is similar to its original version (csTools)
 *
 * @public
 * @class FreehandRoiTool
 * @memberof Tools.Annotation
 * @classdesc Tool for selecting a region of an image to crop and create a new one
 * @extends CornerstoneFreehandRoiTool
 */
export default class FreehandRoiTool extends CornerstoneFreehandRoiTool {
  static toolName = 'FreehandRoiTool';

  constructor(props = {}) {
    super(props);
    // Initially configured as null and later initalize based on project config
    this.configuration.drawHandles = null;
    this.drawJoinedLines = drawJoinedLines;
    this.drawHandles = drawHandles;
    this.drawLinkedTextBox = drawLinkedTextBox;
  }

  createNewMeasurement(eventData) {
    const colorPalette = store.getState().colorPalette;
    const data = super.createNewMeasurement(eventData);
    const color = colorPalette.roiColor.next;
    store.dispatch(setNextPalette());
    const state = store.getState();
    const projectConfig = state.flywheel?.projectConfig;
    // If configured to hide the vertex point, then forcefully skip the handle drawing
    if (projectConfig) {
      this.configuration.drawHandles = !projectConfig.hideFreehandVertex;
    }
    return Object.assign({}, data, { color: color });
  }

  /**
   *
   * @param {Object} image image
   * @param {Object} element element
   * @param {Object} data data
   *
   * @returns {void}  void
   */
  updateCachedStats(image, element, data) {
    if (data.handles.points) {
      // Define variables for the area and mean/standard deviation
      let meanStdDev, meanStdDevSUV;

      const seriesModule = cornerstone.metaData.get(
        'generalSeriesModule',
        image.imageId
      );
      const modality = seriesModule ? seriesModule.modality : null;

      const points = data.handles.points;
      // If the data has been invalidated, and the tool is not currently active,
      // We need to calculate it again.

      // Retrieve the bounds of the ROI in image coordinates
      const bounds = {
        left: points[0].x,
        right: points[0].x,
        bottom: points[0].y,
        top: points[0].y,
      };

      for (let i = 0; i < points.length; i++) {
        bounds.left = Math.min(bounds.left, points[i].x);
        bounds.right = Math.max(bounds.right, points[i].x);
        bounds.bottom = Math.min(bounds.bottom, points[i].y);
        bounds.top = Math.max(bounds.top, points[i].y);
      }

      const polyBoundingBox = {
        left: bounds.left,
        top: bounds.bottom,
        width: Math.abs(bounds.right - bounds.left),
        height: Math.abs(bounds.top - bounds.bottom),
      };

      // Store the bounding box information for the text box
      data.polyBoundingBox = polyBoundingBox;

      // First, make sure this is not a color image, since no mean / standard
      // Deviation will be calculated for color images.
      if (!image.color) {
        // Retrieve the array of pixels that the ROI bounds cover
        const pixels = cornerstone.getPixels(
          element,
          polyBoundingBox.left,
          polyBoundingBox.top,
          polyBoundingBox.width,
          polyBoundingBox.height
        );

        // Calculate the mean & standard deviation from the pixels and the object shape
        meanStdDev = calculateFreehandStatistics.call(
          this,
          pixels,
          polyBoundingBox,
          data.handles.points
        );
        if (modality === 'PT') {
          // If the image is from a PET scan, use the DICOM tags to
          // Calculate the SUV from the mean and standard deviation.

          // Note that because we are using modality pixel values from getPixels, and
          // The calculateSUV routine also rescales to modality pixel values, we are first
          // Returning the values to storedPixel values before calcuating SUV with them.
          // TODO: Clean this up? Should we add an option to not scale in calculateSUV?
          meanStdDevSUV = {
            mean: calculateSUV(
              image,
              (meanStdDev.mean - image.intercept) / image.slope
            ),
            stdDev: calculateSUV(
              image,
              (meanStdDev.stdDev - image.intercept) / image.slope
            ),
            median: calculateSUV(
              image,
              (meanStdDev.median - image.intercept) / image.slope
            ),
            interQuartileRange: calculateSUV(
              image,
              (meanStdDev.interQuartileRange - image.intercept) / image.slope
            ),
          };
        }

        // If the mean and standard deviation values are sane, store them for later retrieval
        if (meanStdDev && !isNaN(meanStdDev.mean)) {
          data.meanStdDev = meanStdDev;
          data.meanStdDevSUV = meanStdDevSUV;
        }
      }

      // Retrieve the pixel spacing values, and if they are not
      // Real non-zero values, set them to 1
      const columnPixelSpacing = image.columnPixelSpacing || 1;
      const rowPixelSpacing = image.rowPixelSpacing || 1;
      const scaling = columnPixelSpacing * rowPixelSpacing;

      const area = freehandArea(data.handles.points, scaling);

      // If the area value is sane, store it for later retrieval
      if (!isNaN(area)) {
        data.area = area;
      }
      // Set the invalidated flag to false so that this data won't automatically be recalculated
      data.invalidated = false;
    }
  }

  initializeDefaultCursor(evt, isAdd = true) {
    const cursorTools =
      state?.tools.filter(item => 'CircleCursorTool' === item.name) || [];

    if (cursorTools.length) {
      // Start or finish drawing based on isAdd parameter
      cursorTools.forEach(cursorTool =>
        isAdd
          ? cursorTool.addNewMeasurement?.(evt)
          : cursorTool.finishDrawing?.(evt.detail.element)
      );
    }
  }

  // Overwrite method to use custom Unit strategy
  renderToolData(evt) {
    const eventData = evt.detail;

    // If we have no toolState for this element, return immediately as there is nothing to do
    const toolState = getToolState(evt.currentTarget, this.name);

    if (!toolState) {
      return;
    }

    if (this.configuration.drawHandles === null) {
      const state = store.getState();
      const projectConfig = state.flywheel?.projectConfig;
      // If configured to hide the vertex point, then forcefully skip the handle drawing
      this.configuration.drawHandles = !projectConfig?.hideFreehandVertex;
    }
    const { image, element } = eventData;
    const config = this.configuration;
    const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(image);
    const hasPixelSpacing = rowPixelSpacing && colPixelSpacing;

    const seriesModule = cornerstone.metaData.get(
      'generalSeriesModule',
      image.imageId
    );
    const modality = seriesModule ? seriesModule.modality : null;

    // We have tool data for this element - iterate over each one and draw it
    const context = getNewContext(eventData.canvasContext.canvas);
    const lineWidth = toolStyle.getToolWidth();
    const { renderDashed } = config;
    const lineDash = getModule('globalConfiguration').configuration.lineDash;

    const isAnyActiveData = toolState.data.find(data => data.active) || false;
    this.initializeDefaultCursor(evt, !isAnyActiveData);

    for (let i = 0; i < toolState.data.length; i++) {
      const data = toolState.data[i];

      if (data.visible === false) {
        continue;
      }

      draw(context, context => {
        let color = data.active
          ? toolColors.getActiveColor()
          : data.color || toolColors.getColorIfActive(data);
        color = unitUtils.convertRgbToRgba(color, data.color);

        let fillStyle =
          data.toolType === 'OpenFreehandRoi' ? 'rgba(0, 0, 0, 0)' : color;

        color = color.substring(0, color.lastIndexOf(',')) + ', 1)';
        if (measurementToolUtils.hasColorIntensity(data)) {
          color = measurementToolUtils.reduceColorOpacity(color);
          fillStyle = measurementToolUtils.reduceColorOpacity(fillStyle);
        }

        let fillColor;

        if (data.active) {
          if (data.handles.invalidHandlePlacement) {
            color = config.invalidColor;
            fillColor = config.invalidColor;
          } else {
            fillColor = color;
          }
        } else {
          fillColor = color;
        }

        let options = { color, fillStyle };

        if (renderDashed) {
          options.lineDash = lineDash;
        }

        if (data.handles.points.length) {
          const points = data.handles.points;
          const isMatchingTool = this.isToolNameMatching();
          if (isMatchingTool) {
            this.drawJoinedLines(context, element, points[0], points, options);
          } else {
            this.drawJoinedLines(
              context,
              element,
              points[0],
              points,
              options,
              true
            );
          }

          if (data.polyBoundingBox) {
            this.closeEndPoints(context, element, points, options);
          } else {
            let drawPoints = [];
            if (isMatchingTool) {
              drawPoints = points;
            } else {
              drawPoints = points[points.length - 1];
            }
            if (drawPoints?.length) {
              this.drawJoinedLines(
                context,
                element,
                drawPoints[drawPoints.length - 1],
                [config.mouseLocation.handles.start],
                options
              );
            }
          }
        }

        // Draw handles

        options = {
          color,
          fill: fillColor,
        };

        if (config.alwaysShowHandles || (data.active && data.polyBoundingBox)) {
          // Render all handles
          options.handleRadius = config.activeHandleRadius;

          if (this.configuration.drawHandles) {
            this.drawHandles(
              context,
              eventData,
              data.handles.points,
              options,
              'POINTS'
            );
          }
        }

        if (data.canComplete) {
          // Draw large handle at the origin if can complete drawing
          options.handleRadius = config.completeHandleRadius;
          const handle = data.handles.points[0];

          if (this.configuration.drawHandles) {
            this.drawHandles(context, eventData, [handle], options);
          }
        }

        if (data.active && !data.polyBoundingBox) {
          // Draw handle at origin and at mouse if actively drawing
          options.handleRadius = config.activeHandleRadius;

          if (this.configuration.drawHandles) {
            this.drawHandles(
              context,
              eventData,
              config.mouseLocation.handles,
              options
            );
          }

          const firstHandle = data.handles.points[0];

          if (this.configuration.drawHandles) {
            this.drawHandles(context, eventData, [firstHandle], options);
          }
        }

        // Update textbox stats
        if (data.invalidated === true && !data.active) {
          if (data.meanStdDev && data.meanStdDevSUV && data.area) {
            this.throttledUpdateCachedStats(image, element, data);
          } else {
            this.updateCachedStats(image, element, data);
          }
        }

        // Only render text if polygon ROI has been completed and freehand 'shiftKey' mode was not used:
        if (data.polyBoundingBox && !data.handles.textBox.freehand) {
          // If the textbox has not been moved by the user, it should be displayed on the right-most
          // Side of the tool.
          if (!data.handles.textBox.hasMoved) {
            // Find the rightmost side of the polyBoundingBox at its vertical center, and place the textbox here
            // Note that this calculates it in image coordinates
            data.handles.textBox.x =
              data.polyBoundingBox.left + data.polyBoundingBox.width;
            data.handles.textBox.y =
              data.polyBoundingBox.top + data.polyBoundingBox.height / 2;
          }
          if (unitUtils.isRoiOutputHidden()) {
            // Temporary status flag to indicate measurement visibility in
            // viewport, not to be saved in server.
            data.handles.textBox.isVisible = false;
            data.handles.textBox.boundingBox = TextBoxContentUtils.getDefaultBoundingBox(
              data.handles.textBox
            );
          } else {
            const textBoxContent = this.getTextboxContent(
              context,
              image,
              data,
              modality,
              hasPixelSpacing
            );

            this.drawLinkedTextBox(
              context,
              element,
              data.handles.textBox,
              textBoxContent,
              data.handles.points,
              handles => handles,
              color,
              lineWidth,
              0,
              true
            );
          }
        }
      });
    }
  }

  /**
   *
   *
   * @param {*} element element
   * @param {*} data data
   * @param {*} coords coords
   * @returns {Boolean}
   */
  pointNearTool(element, data, coords) {
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (data.readonly || !data.visible || !isAnnotationToolEnabled) {
      return false;
    }
    const isHighlight = super.pointNearTool(element, data, coords);
    return !isHighlight
      ? this.pointInsideTool(element, data, coords)
      : isHighlight;
  }

  pointInsideTool(element, data, coords) {
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (data.readonly || !data.visible || !isAnnotationToolEnabled) {
      return false;
    }

    const validParameters = data && data.handles && data.handles.points;

    if (!validParameters || data.visible === false) {
      return false;
    }

    const point = cornerstone.canvasToPixel(element, coords);
    const isPointInsideTool = isPointInsideFreehand(data.handles.points, point);
    return isPointInsideTool;
  }

  handleSelectedCallback(evt, toolData, handle, interactionType = 'mouse') {
    const eventData = evt.detail;
    const { element } = eventData;
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (toolData.readonly || !toolData.visible || !isAnnotationToolEnabled) {
      return;
    }
    super.handleSelectedCallback(evt, toolData, handle, interactionType);
    saveActionState(toolData, 'measurements', 'Modify', true, false);
  }

  /**
   * To create text-box content
   * @param {Object} context
   * @param {Object} image
   * @param {Object} data
   * @param {Object} modality
   * @param {Boolean} hasPixelSpacing
   * @returns
   */
  getTextboxContent(context, image, data, modality, hasPixelSpacing) {
    return TextBoxContentUtils.createTextBoxContent(
      context,
      image.color,
      data,
      modality,
      hasPixelSpacing,
      this.configuration
    );
  }

  /**
   * To draw close ends.
   * @param {Object} context
   * @param {Element} element
   * @param {Array} points
   * @param {Object} options
   */
  closeEndPoints(context, element, points, options) {
    if (this.isToolNameMatching()) {
      this.drawJoinedLines(
        context,
        element,
        points[points.length - 1],
        [points[0]],
        options
      );
    } else {
      this.drawJoinedLines(
        context,
        element,
        points[points.length - 1],
        [[points[0]]],
        options,
        false
      );
    }
  }

  /**
   * To check the tool name match to this class or an extended tool.
   * @returns {Boolean} true if matching else false
   */
  isToolNameMatching() {
    return this.name === 'FreehandRoi' ? true : false;
  }

  /**
   * Ends the active drawing loop and completes the polygon.
   *
   * @private
   * @param {Object} element - The element on which the roi is being drawn.
   * @param {Object} handleNearby - the handle nearest to the mouse cursor.
   * @returns {undefined}
   */
  _endDrawing(element, handleNearby) {
    const toolState = getToolState(element, this.name);
    const config = this.configuration;
    const data = toolState.data[config.currentTool];

    data.active = false;
    data.highlight = false;
    data.handles.invalidHandlePlacement = false;

    // Connect the end handle to the origin handle
    if (handleNearby !== undefined) {
      const points = data.handles.points;

      points[config.currentHandle - 1].lines.push({
        x: points[0].x,
        y: points[0].y,
      });
    }

    if (this._modifying) {
      this._modifying = false;
      data.invalidated = true;
      saveActionState(data, 'measurements', 'Modify', false, true);
    } else {
      const enabledElement = cornerstone.getEnabledElement(element);
      const image = enabledElement.image;

      if (data.meanStdDev && data.meanStdDevSUV && data.area) {
        this.throttledUpdateCachedStats(image, element, data);
      } else {
        this.updateCachedStats(image, element, data);
      }
    }

    // Reset the current handle
    config.currentHandle = 0;
    config.currentTool = -1;
    data.canComplete = false;

    if (this._drawing) {
      this._deactivateDraw(element);
    }

    cornerstone.updateImage(element);

    this.fireModifiedEvent(element, data);
    this.fireCompletedEvent(element, data);
  }

  /** Ends the active drawing loop forcefully.
   *
   * @public
   * @param {Object} element - The element on which the roi is being drawn.
   * @returns {null}
   */
  finishDrawing(element) {
    const config = this.configuration;
    if (!this._drawing || config.currentTool < 0) {
      return;
    }
    const toolState = getToolState(element, this.name);
    const data = toolState.data[config.currentTool];

    if (!data) {
      if (this._drawing) {
        this._deactivateDraw(element);
      }
      config.currentHandle = 0;
      config.currentTool = -1;
      return;
    }

    data.polyBoundingBox = {};
    this._endDrawing(element);
  }

  getTextInfo(measurementData, data = null) {
    if (data) {
      // Meta
      const seriesModule =
        cornerstone.metaData.get('generalSeriesModule', data.image.imageId) ||
        {};

      // Pixel Spacing
      const modality = seriesModule.modality;

      const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(data.image);

      const hasPixelSpacing = rowPixelSpacing && colPixelSpacing;
      return TextBoxContentUtils.getTextBoxContent(
        data.image.color,
        measurementData,
        modality,
        hasPixelSpacing,
        this.configuration
      );
    }
    return [];
  }
}
