import csTools from 'cornerstone-tools';

const CornerstoneZoomTool = csTools.ZoomTool;

const { state } = csTools.store;
/**
 * Improvement on csTool Zoom tool to use FLYW custom implementation
 *
 * @public
 * @class ZoomTool
 * @memberof Tools
 * @classdesc Tool for changing magnification.
 * @extends CornerstoneZoomTool
 */
export default class ZoomTool extends CornerstoneZoomTool {
  static toolName = 'Zoom';

  initializeDefaultCursor(evt, isAdd = true) {
    const cursorTools =
      state?.tools.filter(item => 'CircleCursorTool' === item.name) || [];

    if (cursorTools.length) {
      // Start or finish drawing based on isAdd parameter
      cursorTools.forEach(cursorTool =>
        isAdd
          ? cursorTool.addNewMeasurement?.(evt)
          : cursorTool.finishDrawing?.(evt.detail.element)
      );
    }
  }
}
