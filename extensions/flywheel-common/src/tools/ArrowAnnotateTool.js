import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import * as unitUtils from './unit';

import commonToolUtils from './commonUtils';

import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import measurementToolUtils from './utils';
import { shouldEnableAnnotationTool } from './utils/shouldEnableAnnotationTool';

const { setNextPalette } = FlywheelCommonRedux.actions;

const {
  findToolIndex,
  saveActionState,
  upOrEndEventsManager,
} = commonToolUtils;

// manipulators
const moveHandleNearImagePoint = csTools.importInternal(
  'manipulators/moveHandleNearImagePoint'
);

// set an alias
const CornerstoneArrowAnnotateTool = csTools.ArrowAnnotateTool;

// Drawing
const getNewContext = csTools.importInternal('drawing/getNewContext');
const draw = csTools.importInternal('drawing/draw');
const setShadow = csTools.importInternal('drawing/setShadow');
const drawHandles = csTools.importInternal('drawing/drawHandles');
const drawLinkedTextBox = csTools.importInternal('drawing/drawLinkedTextBox');
const drawArrow = csTools.importInternal('drawing/drawArrow');

// State
const getToolState = csTools.getToolState;
const toolColors = csTools.toolColors;
const toolStyle = csTools.toolStyle;

// Util
const getPixelSpacing = csTools.importInternal('util/getPixelSpacing');
const moveNewHandle = csTools.importInternal('manipulators/moveNewHandle');
const triggerEvent = csTools.importInternal('util/triggerEvent');

const { EVENTS, addToolState, removeToolState, getModule } = csTools;

/**
 * Improvement on csTool ArrowAnnotate Tool to use FLYW custom measurement label implementation
 * renderToolData method is similar to its original version (csTools)
 *
 * @public
 * @class ArrowAnnotateTool
 * @memberof Tools.Annotation
 * @classdesc Tool for measuring ArrowAnnotate
 * @extends CornerstoneArrowAnnotateTool
 */
export default class ArrowAnnotateTool extends CornerstoneArrowAnnotateTool {
  static toolName = 'ArrowAnnotateTool';

  constructor(props = {}) {
    super(props);
    this._endDrawing = this._endDrawing.bind(this);
  }

  createNewMeasurement(eventData) {
    const colorPalette = store.getState().colorPalette;
    const data = super.createNewMeasurement(eventData);
    const color = colorPalette.roiColor.next;
    store.dispatch(setNextPalette());
    return Object.assign({}, data, { color: color });
  }

  addNewMeasurement(evt, interactionType) {
    const element = evt.detail.element;
    const measurementData = this.createNewMeasurement(evt);

    const { allowEmptyLabel } = this.configuration;

    // Associate this data with this imageId so we can render it and manipulate it
    addToolState(element, this.name, measurementData);
    cornerstone.updateImage(element);

    moveNewHandle(
      evt.detail,
      this.name,
      measurementData,
      measurementData.handles.end,
      this.options,
      interactionType,
      success => {
        if (success) {
          if (measurementData.text === undefined) {
            this.configuration.getTextCallback(text => {
              if (text || allowEmptyLabel) {
                measurementData.text = text;
                measurementData.active = false;

                const modifiedEventData = {
                  toolName: this.name,
                  toolType: this.name, // Deprecation notice: toolType will be replaced by toolName
                  element,
                  measurementData,
                };

                cornerstone.updateImage(element);
                triggerEvent(
                  element,
                  EVENTS.MEASUREMENT_COMPLETED,
                  modifiedEventData
                );
              } else {
                removeToolState(element, this.name, measurementData);
              }
            }, evt.detail);
          }
          const modifiedEventData = {
            toolName: this.name,
            toolType: this.name, // Deprecation notice: toolType will be replaced by toolName
            element,
            measurementData,
          };
          triggerEvent(
            element,
            EVENTS.MEASUREMENT_COMPLETED,
            modifiedEventData
          );
        } else {
          removeToolState(element, this.name, measurementData);
        }

        cornerstone.updateImage(element);
      }
    );
  }

  renderToolData(evt) {
    const { element, enabledElement } = evt.detail;
    const {
      handleRadius,
      drawHandlesOnHover,
      hideHandlesIfMoving,
      renderDashed,
    } = this.configuration;

    // If we have no toolData for this element, return immediately as there is nothing to do
    const toolData = getToolState(element, this.name);

    if (!toolData) {
      return;
    }

    // We have tool data for this element - iterate over each one and draw it
    const canvas = evt.detail.canvasContext.canvas;
    const context = getNewContext(canvas);

    const lineWidth = toolStyle.getToolWidth();

    let lineDash;
    if (renderDashed) {
      lineDash = getModule('globalConfiguration').configuration.lineDash;
    }

    for (let i = 0; i < toolData.data.length; i++) {
      const data = toolData.data[i];

      if (data.visible === false) {
        continue;
      }

      draw(context, context => {
        setShadow(context, this.configuration);

        let color = data.active
          ? toolColors.getActiveColor()
          : data.color || toolColors.getColorIfActive(data);
        color = unitUtils.convertRgbToRgba(color, data.color);
        color = color.substring(0, color.lastIndexOf(',')) + ', 1)';
        if (measurementToolUtils.hasColorIntensity(data)) {
          color = measurementToolUtils.reduceColorOpacity(color);
        }
        // Draw the arrow
        const handleStartCanvas = cornerstone.pixelToCanvas(
          element,
          data.handles.start
        );
        const handleEndCanvas = cornerstone.pixelToCanvas(
          element,
          data.handles.end
        );

        // Config.arrowFirst = false;
        if (this.configuration.arrowFirst) {
          drawArrow(
            context,
            handleEndCanvas,
            handleStartCanvas,
            color,
            lineWidth,
            lineDash
          );
        } else {
          drawArrow(
            context,
            handleStartCanvas,
            handleEndCanvas,
            color,
            lineWidth,
            lineDash
          );
        }

        const handleOptions = {
          color,
          handleRadius,
          drawHandlesIfActive: drawHandlesOnHover,
          hideHandlesIfMoving,
        };

        if (this.configuration.drawHandles) {
          drawHandles(context, evt.detail, data.handles, handleOptions);
        }

        const text = this.textBoxText(data);

        // Draw the text
        if (text && text !== '') {
          // Calculate the text coordinates.
          const padding = 5;
          const textWidth = textBoxWidth(context, text, padding);
          const textHeight = csTools.textStyle.getFontSize() + 10;

          let distance = Math.max(textWidth, textHeight) / 2 + 5;

          if (handleEndCanvas.x < handleStartCanvas.x) {
            distance = -distance;
          }

          if (!data.handles.textBox.hasMoved) {
            let textCoords;

            if (this.configuration.arrowFirst) {
              textCoords = {
                x: handleEndCanvas.x - textWidth / 2 + distance,
                y: handleEndCanvas.y - textHeight / 2,
              };
            } else {
              // If the arrow is at the End position, the text should
              // Be placed near the Start position
              textCoords = {
                x: handleStartCanvas.x - textWidth / 2 - distance,
                y: handleStartCanvas.y - textHeight / 2,
              };
            }

            const transform = cornerstone.internal.getTransform(enabledElement);

            transform.invert();

            const coords = transform.transformPoint(textCoords.x, textCoords.y);

            data.handles.textBox.x = coords.x;
            data.handles.textBox.y = coords.y;
          }

          drawLinkedTextBox(
            context,
            element,
            data.handles.textBox,
            text,
            data.handles,
            textBoxAnchorPoints,
            color,
            lineWidth,
            0,
            false
          );
        }
      });
    }

    function textBoxAnchorPoints(handles) {
      const midpoint = {
        x: (handles.start.x + handles.end.x) / 2,
        y: (handles.start.y + handles.end.y) / 2,
      };

      return [handles.start, midpoint, handles.end];
    }

    function textBoxWidth(context, text, padding) {
      const font = csTools.textStyle.getFont();
      const origFont = context.font;

      if (font && font !== origFont) {
        context.font = font;
      }
      const width = context.measureText(text).width;

      if (font && font !== origFont) {
        context.font = origFont;
      }

      return width + 2 * padding;
    }
  }

  textBoxText(data) {
    return data.text;
  }

  pointNearTool(element, data, coords) {
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (data.readonly || !data.visible || !isAnnotationToolEnabled) {
      return false;
    }
    return super.pointNearTool(element, data, coords);
  }

  /**
   * Custom callback for when a handle is selected.
   * @method handleSelectedCallback
   * @memberof Tools.Base.BaseAnnotationTool
   *
   * @param  {*} evt    -
   * @param  {*} toolData   -
   * @param  {*} handle - The selected handle.
   * @param  {String} interactionType -
   * @returns {void}
   */
  handleSelectedCallback(evt, toolData, handle, interactionType = 'mouse') {
    const eventData = evt.detail;
    const { element } = eventData;
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (!toolData.readonly && toolData.visible && isAnnotationToolEnabled) {
      moveHandleNearImagePoint(evt, this, toolData, handle, interactionType);
      this._initializeDrawing(evt, handle, interactionType);
    }
  }

  /**
   * Custom callback for when a tool is selected.
   *
   * @method toolSelectedCallback
   * @memberof Tools.Base.BaseAnnotationTool
   *
   * @param  {*} evt
   * @param  {*} annotation
   * @param  {string} [interactionType=mouse]
   * @returns {void}
   */
  toolSelectedCallback(evt, annotation, interactionType = 'mouse') {
    if (!annotation.readonly && annotation.visible) {
      super.toolSelectedCallback(evt, annotation, interactionType);
      this._initializeDrawing(evt, annotation.handles?.start, interactionType);
    }
  }

  /**
   * Initialize the drawing loop when tool is active and a click event happens.
   *
   * @private
   * @param {Object} evt - The event.
   * @param  {*} handle - Selected handle or any of the handle.
   * @param  {string} [interactionType=mouse]
   * @returns {void}
   */
  _initializeDrawing(evt, handle, interactionType = 'mouse') {
    const { element } = evt.detail;
    const currentTool = findToolIndex(element, this.name, handle);
    if (currentTool >= 0) {
      this.configuration.currentTool = currentTool;
      this._startDrawing(evt);
      upOrEndEventsManager.registerHandler(
        element,
        interactionType,
        this._endDrawing
      );
    }
  }

  /**
   * Beginning of drawing loop when tool is active and a click event happens.
   *
   * @private
   * @param {Object} evt - The event.
   * @returns {void}
   */
  _startDrawing(evt) {
    const { element } = evt.detail;
    const toolState = getToolState(element, this.name);
    const data = toolState.data[this.configuration.currentTool];
    saveActionState(data, 'measurements', 'Modify', true, false);
  }

  /**
   * Ends the active drawing loop and completes the polygon.
   *
   * @private
   * @param {Object} evt - The event.
   * @returns {void}
   */
  _endDrawing(evt) {
    const { element } = evt.detail;
    const interactionType = upOrEndEventsManager.getCurrentInteractionType(evt);
    upOrEndEventsManager.removeHandler(
      element,
      interactionType,
      this._endDrawing
    );
    const toolState = getToolState(element, this.name);
    const data = toolState.data[this.configuration.currentTool];
    saveActionState(data, 'measurements', 'Modify', false, true);
    this.configuration.currentTool = -1;
  }

  /** Ends the active drawing loop forcefully.
   *
   * @public
   * @param {Object} element - The element on which the roi is being drawn.
   * @returns {null}
   */
  finishDrawing(element) {
    const evt = { detail: { element } };
    if (this.configuration.currentTool >= 0) {
      this._endDrawing(evt);
    }
  }

  getTextInfo(measurementData, data = null) {
    if (measurementData) {
      const text = this.textBoxText(measurementData);
      if (text && text !== '') {
        return [text];
      }
    }
    return [];
  }
}
