import cornerstone from 'cornerstone-core';
import cornerstoneMath from 'cornerstone-math';
import csTools from 'cornerstone-tools';
import FreehandRoiSculptorToolBase from './FreehandRoiSculptorToolBase';
import commonToolUtils from './commonUtils';
import { getPointsOnCircleCircumference } from './utils/CircleIntersect';
import { doesIntersect } from './utils/LineIntersect';
import { shouldEnableAnnotationTool } from './utils/shouldEnableAnnotationTool';
import interpolate from './utils/contourUtils/freehandInterpolate/interpolate.js';

const triggerEvent = csTools.importInternal('util/triggerEvent');

const { state } = csTools.store;

// set an alias
const CornerstoneFreehandRoiSculptorTool = csTools.FreehandRoiSculptorTool;

// Util
const freehandUtils = csTools.importInternal('util/freehandUtils');
const clipToBox = csTools.importInternal('util/clipToBox');

const getToolForElement = csTools.getToolForElement;
const getToolState = csTools.getToolState;

const { saveActionState } = commonToolUtils;

const { FreehandHandleData } = freehandUtils;

/** *
 * @public
 * @class FreehandRoiSculptorTool
 * @memberof Tools.Annotation
 * @classdesc Tool for easily sculpting annotations drawn with
 * the FreehandRoiTool and OpenFreehandRoiTool
 * @extends FreehandRoiSculptorToolBase
 */
export default class FreehandRoiSculptorTool extends FreehandRoiSculptorToolBase {
  static toolName = 'FreehandRoiSculptorTool';
  constructor(props = {}) {
    super(props);
    this.referencedToolName = this.initialConfiguration.referencedToolName;
    this.referencedToolNames = [
      this.referencedToolName,
      'OpenFreehandRoi',
      'ContourRoi',
    ];
    this.eraseModeOn = false;
  }

  /**
   * To unbind the special hotkeys used in Open Freehand support
   */
  unBindKeys() {
    const defaultKey = 'ctrl';
    const hotKeyConfigFn = this.configuration?.getHotKeyConfigForCommand;
    const hotKeys = hotKeyConfigFn
      ? hotKeyConfigFn('partialEraseKey')?.keys || defaultKey
      : defaultKey;
    Mousetrap.unbind(defaultKey, 'keyup');
    Mousetrap.unbind(defaultKey, 'keydown');
    if (hotKeys !== defaultKey) {
      Mousetrap.unbind(hotKeys, 'keyup');
      Mousetrap.unbind(hotKeys, 'keydown');
    }
  }

  /**
   * To register special hotkeys for Open Freehand support
   */
  bindKeys() {
    const defaultKey = 'ctrl';
    const hotKeyConfigFn = this.configuration?.getHotKeyConfigForCommand;
    const hotKeys = hotKeyConfigFn
      ? hotKeyConfigFn('partialEraseKey')?.keys || defaultKey
      : defaultKey;
    Mousetrap.bind(
      hotKeys,
      e => {
        this.eraseModeOn = true;
        cornerstone.getEnabledElements().forEach(enabledElement => {
          if (enabledElement.image) {
            cornerstone.updateImage(enabledElement.element);
          }
        });
      },
      'keydown'
    );
    Mousetrap.bind(
      hotKeys,
      e => {
        this.eraseModeOn = false;
        cornerstone.getEnabledElements().forEach(enabledElement => {
          if (enabledElement.image) {
            cornerstone.updateImage(enabledElement.element);
          }
        });
      },
      'keyup'
    );
  }

  /**
   * Checks the current tool data exist in tool state
   *
   * @private
   * @param  {Object} element
   * @returns {Boolean} true if data exist
   */
  isCurrentToolDataExistInToolState(element) {
    let isDataExist = false;
    this.referencedToolNames.forEach(referToolName => {
      const toolState = getToolState(element, referToolName);

      if (toolState) {
        const data = toolState.data[this.configuration.currentTool];
        isDataExist = data ? true : isDataExist;
      }
    });

    return isDataExist;
  }

  /**
   * Sculpts the freehand ROI with the circular freehandSculpter tool, moving,
   * adding and removing handles as necessary.
   *
   * @private
   * @param {Object} eventData - Data object associated with the event.
   * @param {Array} points - Array of points.
   * @returns {void}
   */
  _sculpt(eventData, points) {
    const config = this.configuration;

    this._sculptData = {
      element: eventData.element,
      image: eventData.image,
      mousePoint: eventData.currentPoints.image,
      mousePointCanvas: eventData.currentPoints.canvas,
      points,
      toolSize: this._toolSizeImage,
      toolSizeCanvas: this._toolSizeCanvas,
      minSpacing: config.minSpacing,
      maxSpacing: Math.max(this._toolSizeCanvas, config.minSpacing * 2),
    };

    const { mousePointCanvas, toolSizeCanvas, element } = this._sculptData;
    if (this.referencedToolName !== 'OpenFreehandRoi') {
      if (!this.eraseModeOn) {
        super._sculpt(eventData, points);

        const sculptToolPoints = getPointsOnCircleCircumference(
          mousePointCanvas,
          toolSizeCanvas,
          element
        );
        sculptToolPoints.push(sculptToolPoints[sculptToolPoints.length - 1]);

        points.push(
          this._generateFreehandPoint(points[0], points[points.length - 1])
        );
        const intersectionPointIndices = this._getIntersectionPointIndices(
          points,
          sculptToolPoints,
          0
        );
        const pointsInSculpt = getPointsInSculpt(
          toolSizeCanvas,
          mousePointCanvas,
          points,
          element
        );

        if (intersectionPointIndices.length && !pointsInSculpt.length) {
          this._insertNewPointsOnEdge(intersectionPointIndices, [points]);
        }
        points.pop();
      } else {
        const pointsToRemove = getPointsInSculpt(
          toolSizeCanvas,
          mousePointCanvas,
          points,
          element
        );
        this._removeFreehandPoints(pointsToRemove, points);
      }
      return;
    }

    if (this.eraseModeOn) {
      this._splitFreehand();
    } else {
      const sculptToolPoints = getPointsOnCircleCircumference(
        mousePointCanvas,
        toolSizeCanvas,
        element
      );
      sculptToolPoints.push(sculptToolPoints[sculptToolPoints.length - 1]);

      const lines = this._sculptData.points;
      for (let l = 0; l < lines.length; l++) {
        const points = lines[l];
        const intersectionPointIndices = this._getIntersectionPointIndices(
          points,
          sculptToolPoints,
          l
        );
        const pointsInSculpt = getPointsInSculpt(
          toolSizeCanvas,
          mousePointCanvas,
          points,
          element
        );
        if (intersectionPointIndices.length && !pointsInSculpt.length) {
          this._insertNewPointsOnEdge(intersectionPointIndices, lines);
        }
      }

      // Push existing handles radially away from tool.
      let pushedHandles = this._pushHandles();

      const editedLines = Object.keys(pushedHandles);
      if (pushedHandles.length) {
        for (let i = 0; i < points.length; i++) {
          if (editedLines.includes(i.toString())) {
            // Check key i exist in pushedHandles
            this._insertNewHandles(pushedHandles[i], points[i]);
          }
        }

        // If any handles have been pushed very close together or even overlap,
        // Combine these into a single handle.
        for (let i = 0; i < points.length; i++) {
          if (editedLines.includes(i.toString())) {
            this._consolidateHandles(points[i]);
          }
        }
      }
      this._cleanUpShortLines(lines);
    }
  }

  _removeFreehandPoints(pointsToRemove, roiPoints) {
    let startIndex = pointsToRemove[pointsToRemove.length - 1];
    let endIndex = pointsToRemove[pointsToRemove.length - 1];
    for (let i = pointsToRemove.length - 1; i >= 0; i--) {
      if (startIndex - 1 !== pointsToRemove[i] || i === 0) {
        const deleteCount = endIndex - startIndex + 1;
        if (roiPoints.length - deleteCount > 2) {
          roiPoints.splice(startIndex, deleteCount);
          const connectTo = startIndex % roiPoints.length;

          if (!startIndex) {
            // For first point
            startIndex = roiPoints.length;
          }
          roiPoints[startIndex - 1].lines = [
            {
              x: roiPoints[connectTo].x,
              y: roiPoints[connectTo].y,
            },
          ];
          if (pointsToRemove[i] > 0) {
            endIndex = pointsToRemove[i];
          } else if (roiPoints.length > 3) {
            roiPoints.splice(0, 1);
            roiPoints[roiPoints.length - 1].lines = [
              {
                x: roiPoints[0].x,
                y: roiPoints[0].y,
              },
            ];
          }
        }
      }
      startIndex = pointsToRemove[i];
    }
  }

  _insertNewPointsOnEdge(intersectionPointIndices, lines) {
    const { minSpacing } = this._sculptData;
    const newPoints = [];
    const {
      lineIndex,
      pointIndex,
      intersectionPoints,
    } = intersectionPointIndices[0];
    let prevPoint = lines[lineIndex][pointIndex - 1];
    let nextPoint = lines[lineIndex][pointIndex];
    let newHandle;
    let distanceToNearestPoint = Math.max(
      cornerstoneMath.point.distance(prevPoint, intersectionPoints[0]),
      cornerstoneMath.point.distance(nextPoint, intersectionPoints[0])
    );

    if (distanceToNearestPoint > minSpacing) {
      newHandle = this._generateFreehandPoint(
        intersectionPoints[0],
        prevPoint,
        nextPoint
      );
      newPoints.push(newHandle);
    }

    prevPoint = newHandle || prevPoint;
    nextPoint = lines[lineIndex][pointIndex];

    distanceToNearestPoint = Math.max(
      cornerstoneMath.point.distance(prevPoint, intersectionPoints[1]),
      cornerstoneMath.point.distance(nextPoint, intersectionPoints[1])
    );

    if (distanceToNearestPoint > minSpacing) {
      newHandle = this._generateFreehandPoint(
        intersectionPoints[1],
        prevPoint,
        nextPoint
      );
      newPoints.push(newHandle);
    }
    if (newPoints.length) {
      lines[lineIndex].splice(pointIndex, 0, ...newPoints);
    }
  }

  /**
   * Attaches event listeners to the element such that is is visible, modifiable, and new data can be created.
   *
   * @private
   * @param {Object} element - The viewport element to attach event listeners to.
   * @modifies {element}
   * @returns {void}
   */
  _activateSculpt(element) {
    super._activateSculpt(element);
    this.bindKeys();
  }

  /**
   * Removes event listeners from the element.
   *
   * @private
   * @param {Object} element - The viewport element to remove event listeners from.
   * @modifies {element}
   * @returns {void}
   */
  _deactivateSculpt(element) {
    super._deactivateSculpt(element);
    this.eraseModeOn = false;
    this.unBindKeys();
  }

  _splitFreehand() {
    const { mousePointCanvas, toolSizeCanvas, element } = this._sculptData;

    const sculptToolPoints = getPointsOnCircleCircumference(
      mousePointCanvas,
      toolSizeCanvas,
      element
    );
    sculptToolPoints.push(sculptToolPoints[sculptToolPoints.length - 1]);
    const lines = this._sculptData.points;

    for (let l = 0; l < lines.length; l++) {
      const points = lines[l];
      const intersectionPointIndices = this._getIntersectionPointIndices(
        points,
        sculptToolPoints,
        l
      );
      this._splitToLines(intersectionPointIndices, lines);
    }

    // Cleanup single points or points within a tolerance distance
    this._cleanUpShortLines(lines);
  }

  /**
   * Deletes points/lines within the range of mergeDistance
   * @param {array} lines - lines along freehand Roi.
   */
  _cleanUpShortLines(lines) {
    const mergeDistance = 3;
    const isPointInMergeDistance = (point1, point2) =>
      cornerstoneMath.point.distance(point1, point2) < mergeDistance;

    for (let l = lines.length - 1; l >= 0; l--) {
      const points = lines[l];
      if (lines.length > 1) {
        if (!points.length || points.length === 1) {
          lines.splice(l, 1);
        } else if (points.every(p => isPointInMergeDistance(points[0], p))) {
          lines.splice(l, 1);
        }
      }
    }
  }

  /**
   * This function returns intersection coordinates(with lines and sculpt tool),
   * index of line and index of point if any.
   * @param {Array} points
   * @param {Array} circlePoints
   * @param {Integer} lineIndex
   * @returns
   */
  _getIntersectionPointIndices(points, circlePoints, lineIndex) {
    const intersectionPointIndices = [];
    for (let i = 1; i < points.length; i++) {
      const P1 = points[i - 1];
      const P2 = points[i];
      const intersections = getOrderedLineIntersectionPoints(
        P1,
        P2,
        circlePoints
      );
      if (intersections.length) {
        intersectionPointIndices.push({
          lineIndex,
          pointIndex: i,
          intersectionPoints: intersections.map(p => p.intersectionPoint),
        });
      }
    }
    return intersectionPointIndices;
  }

  _splitToLines(intersectionPointIndex, lines) {
    const { mousePointCanvas, toolSizeCanvas, element } = this._sculptData;
    for (let l = 0; l < intersectionPointIndex.length; l++) {
      const intersectionInfo = intersectionPointIndex[l];
      const lineIndex = intersectionInfo.lineIndex;
      const pointIndex = intersectionInfo.pointIndex;
      const intersectionPoints = intersectionInfo.intersectionPoints;

      let fragment1;
      let fragment2;
      let points = lines[lineIndex];

      const pointsInSculpt = getPointsInSculpt(
        toolSizeCanvas,
        mousePointCanvas,
        points,
        element
      );
      if (pointsInSculpt.length) {
        for (let i = pointsInSculpt.length - 1; i >= 0; i--) {
          const intersectionPoint = new FreehandHandleData(
            intersectionPoints[0]
          );
          if (pointsInSculpt[i] === 0) {
            // Move first point
            intersectionPoint.lines.push({
              x: points[1].x,
              y: points[1].y,
            });
            points[0] = intersectionPoint;
            this._mergeToNearestPoint(points, 0);
          } else if (pointsInSculpt[i] === points.length - 1) {
            // Move last point
            points[points.length - 2].lines = [intersectionPoints[0]];
            points[points.length - 1] = intersectionPoint;
            this._mergeToNearestPoint(points, points.length - 1);
          } else {
            // Remove intermediate point and split into two lines.
            if (pointIndex === pointsInSculpt[i] + 1) {
              // Second line segment
              const firstLineIntersectionInfo = intersectionPointIndex[l - 1];

              if (!firstLineIntersectionInfo) {
                // Check first or last point comes inside sculpt circle to
                // determine the edge.
                if (
                  this._removeMultiplePointsFromEnds(
                    points,
                    intersectionPoints,
                    lineIndex
                  )
                ) {
                  return;
                }
                break;
              }

              fragment1 = points.slice(0, pointsInSculpt[i]);
              fragment1[fragment1.length - 1].lines = [
                firstLineIntersectionInfo.intersectionPoints[0],
              ];
              const intersectionPoint1 = new FreehandHandleData(
                firstLineIntersectionInfo.intersectionPoints[0]
              );
              fragment1.push(intersectionPoint1);

              const intersectionPoint2 = new FreehandHandleData(
                intersectionPoints[0]
              );
              fragment2 = points.slice(pointsInSculpt[i] + 1, points.length);
              intersectionPoint2.lines.push({
                x: fragment2[0].x,
                y: fragment2[0].y,
              });
              fragment2.unshift(intersectionPoint2);

              this._sculptData.points[lineIndex] = fragment1;
              this._sculptData.points.splice(lineIndex, 0, fragment2);
              return; // Other intersection points will be processed as part of processing fragment2.
            } else {
              if (
                this._removeMultiplePointsFromEnds(
                  points,
                  intersectionPoints,
                  lineIndex
                )
              ) {
                return;
              }
              // Ignore first intersection point of a corner and process it with second point above.
            }
          }
        }
      } else {
        fragment1 = points.slice(0, pointIndex);
        fragment1[fragment1.length - 1].lines = [intersectionPoints[0]];
        const intersectionPoint1 = new FreehandHandleData(
          intersectionPoints[0]
        );
        fragment1.push(intersectionPoint1);
        this._sculptData.points[lineIndex] = fragment1;

        if (intersectionPoints.length > 1) {
          const intersectionPoint2 = new FreehandHandleData(
            intersectionPoints[1]
          );
          fragment2 = points.slice(pointIndex, points.length);
          intersectionPoint2.lines.push({
            x: fragment2[0].x,
            y: fragment2[0].y,
          });
          fragment2.unshift(intersectionPoint2);
          this._sculptData.points.splice(lineIndex, 0, fragment2);
        }
        break;
      }
    }
  }

  _removeMultiplePointsFromEnds(points, intersectionPoints, lineIndex) {
    const edgeInfo = this._getEndPointsInSculpt(points);
    let isLineModified = false;
    let fragment1;
    // Remove all points till intersectionPoints[0].pointIndex and
    // insert intersectionPoint to the edge.
    if (edgeInfo.lastIndex < points.length) {
      fragment1 = points.slice(0, edgeInfo.lastIndex);
      const intersectionPoint1 = this._generateFreehandPoint(
        intersectionPoints[0],
        fragment1[edgeInfo.lastIndex - 1]
      );
      fragment1.push(intersectionPoint1);
      this._sculptData.points[lineIndex] = fragment1;
      points = fragment1;
      isLineModified = true;
    }

    if (edgeInfo.firstIndex > -1) {
      fragment1 = points.slice(edgeInfo.firstIndex + 1);
      const intersectionPoint1 = this._generateFreehandPoint(
        intersectionPoints[0],
        undefined,
        fragment1[0]
      );
      fragment1.unshift(intersectionPoint1);
      this._sculptData.points[lineIndex] = fragment1;
      isLineModified = true;
    }
    return isLineModified;
  }

  _getEndPointsInSculpt(line) {
    const { mousePointCanvas, toolSizeCanvas, element } = this._sculptData;

    const isPointInsideTool = point => {
      return (
        cornerstoneMath.point.distance(
          cornerstone.pixelToCanvas(element, point),
          mousePointCanvas
        ) < toolSizeCanvas
      );
    };

    const edges = {
      firstIndex: -1,
      lastIndex: line.length,
    };

    if (isPointInsideTool(line[0])) {
      edges.firstIndex = 0;
      for (let i = 1; i < line.length; i++) {
        if (isPointInsideTool(line[i])) {
          edges.firstIndex = i;
        } else {
          break;
        }
      }
    }

    if (isPointInsideTool(line[line.length - 1])) {
      edges.lastIndex = line.length - 1;
      for (let i = line.length - 1; i >= 0; i--) {
        if (isPointInsideTool(line[i])) {
          edges.lastIndex = i;
        } else {
          break;
        }
      }
    }
    return edges;
  }

  /**
   * Merges two consecutive points in a line
   * @param {Object} line
   * @param {Integer} pointIndex
   */
  _mergeToNearestPoint(line, pointIndex) {
    const mergeDistance = 3;

    // Skip second last point
    if (line.length < 3) {
      return false;
    }
    if (pointIndex - 1 >= 0) {
      if (
        cornerstoneMath.point.distance(line[pointIndex - 1], line[pointIndex]) <
        mergeDistance
      ) {
        line.splice(pointIndex, 1);
      }
    }
    if (pointIndex + 1 < line.length) {
      if (
        cornerstoneMath.point.distance(line[pointIndex + 1], line[pointIndex]) <
        mergeDistance
      ) {
        line.splice(pointIndex, 1);
      }
    }
  }

  /**
   * Creates a Freehand handle and sets lines array and previous handle's lines.
   * @param {object} newPoint
   * @param {object} previousPoint
   * @param {object} nextPoint
   * @returns {object} created handle
   */
  _generateFreehandPoint(newPoint, previousPoint, nextPoint) {
    const newHandle = new FreehandHandleData(newPoint);
    if (previousPoint?.lines) {
      previousPoint.lines = [
        {
          x: newPoint.x,
          y: newPoint.y,
        },
      ];
    }
    if (nextPoint?.lines) {
      newHandle.lines = [
        {
          x: nextPoint.x,
          y: nextPoint.y,
        },
      ];
    }
    return newHandle;
  }

  /**
   * _pushHandles -Pushes the points radially away from the mouse if they are
   * contained within the circle defined by the freehandSculpter's toolSize and
   * the mouse position.
   *
   * @returns {Object}  The first and last pushedHandles.
   */
  _pushHandles() {
    if (this.referencedToolName !== 'OpenFreehandRoi') {
      return super._pushHandles();
    }
    const { mousePointCanvas, toolSizeCanvas, element } = this._sculptData;
    const pushedHandles = [];
    const lines = this._sculptData.points;
    for (let l = 0; l < lines.length; l++) {
      const points = lines[l];
      for (let i = 0; i < points.length; i++) {
        const distanceToHandleCanvas = cornerstoneMath.point.distance(
          cornerstone.pixelToCanvas(element, points[i]),
          mousePointCanvas
        );

        if (
          distanceToHandleCanvas > toolSizeCanvas ||
          isNaN(distanceToHandleCanvas)
        ) {
          continue;
        }

        // Push point if inside circle, to edge of circle.
        this._pushOneHandle(i, distanceToHandleCanvas, points);
        if (pushedHandles[l] === undefined) {
          pushedHandles[l] = {};
          pushedHandles[l].first = i;
          pushedHandles[l].last = i;
        } else {
          pushedHandles[l].last = i;
        }
      }
    }

    return pushedHandles;
  }

  /**
   * Inserts additional handles in sparsely sampled regions of the contour. The
   * new handles are placed on the circle defined by the the freehandSculpter's
   * toolSize and the mouse position.
   * @private
   * @param {Array} pushedHandles
   * @param {Array} points - Array of points.
   * @returns {void}
   */
  _insertNewHandles(pushedHandles, points) {
    if (this.referencedToolName !== 'OpenFreehandRoi') {
      super._insertNewHandles(pushedHandles);
      return;
    }
    const indiciesToInsertAfter = this._findNewHandleIndicies(
      pushedHandles,
      points
    );
    let newIndexModifier = 0;

    for (let i = 0; i < indiciesToInsertAfter.length; i++) {
      const insertIndex = indiciesToInsertAfter[i] + 1 + newIndexModifier;

      this._insertHandleRadially(insertIndex, points);
      newIndexModifier++;
    }
  }

  /**
   * Inserts a handle on the surface of the circle defined by toolSize and the
   * mousePoint.
   *
   * @private
   * @param {number} insertIndex - The index to insert the new handle.
   * @param {Array} points - Array of points.
   * @returns {void}
   */
  _insertHandleRadially(insertIndex, points) {
    if (this.referencedToolName !== 'OpenFreehandRoi') {
      points = this._sculptData.points;
    }
    const previousIndex = insertIndex - 1;

    const nextIndex = this.constructor._getNextHandleIndexBeforeInsert(
      insertIndex,
      points.length
    );

    const insertPosition =
      this.referencedToolName !== 'OpenFreehandRoi'
        ? super._getInsertPosition(insertIndex, previousIndex, nextIndex)
        : this._getInsertPosition(
            insertIndex,
            previousIndex,
            nextIndex,
            points
          );

    const handleData = new FreehandHandleData(insertPosition);

    if (isNaN(handleData.y) || isNaN(handleData.x)) {
      return;
    }

    // To restrict points in between start and end points for OpenFreehandRoi
    const isAllowed =
      (insertIndex !== points.length &&
        this.referencedToolName === 'OpenFreehandRoi') ||
      this.referencedToolName !== 'OpenFreehandRoi';

    if (isAllowed) {
      points.splice(insertIndex, 0, handleData); // Add the line from the previous handle to the inserted handle (note the tool is now one increment longer)
    }

    points[previousIndex].lines.pop();
    if (isAllowed) {
      // Override the base to push only the co-ordinates to lines, not the FreehandHandleData
      points[previousIndex].lines.push({
        x: insertPosition.x,
        y: insertPosition.y,
      });
      this.addLinePoints(points, insertIndex);
    }
  }

  /**
   * Checks for any close points and consolidates these to a
   * single point.
   * @param {Array} points - Array of points.
   * @private
   * @returns {void}
   */
  _consolidateHandles(points) {
    if (this.referencedToolName !== 'OpenFreehandRoi') {
      return super._consolidateHandles();
    }
    // Don't merge handles if it would destroy the polygon.
    if (points.length <= 3) {
      return;
    }

    const closePairs = this._findCloseHandlePairs(points);

    this._mergeCloseHandles(closePairs, points);
  }

  /**
   * Merges points given a list of close pairs. The points are merged in an
   * iterative fashion to prevent generating a singularity in some edge cases.
   *
   * @private
   * @param {Array} closePairs - An array of pairs of handle indicies.
   * @param {Array} points - Array of points.
   * @returns {void}
   */
  _mergeCloseHandles(closePairs, points) {
    if (this.referencedToolName !== 'OpenFreehandRoi') {
      return super._mergeCloseHandles(closePairs);
    }
    let removedIndexModifier = 0;

    for (let i = 0; i < closePairs.length; i++) {
      const pair = this.constructor._getCorrectedPair(
        closePairs[i],
        removedIndexModifier
      );

      this._combineHandles(pair, points);
      removedIndexModifier++;
    }

    // Recursively remove problem childs
    const newClosePairs = this._findCloseHandlePairs(points);

    if (newClosePairs.length) {
      this._mergeCloseHandles(newClosePairs, points);
    }
  }

  /**
   * Combines two handles defined by the indicies in handlePairs.
   *
   * @private
   * @param {Object} handlePair - A pair of handle indicies.
   * @param {Array} points - Array of points.
   * @returns {void}
   */
  _combineHandles(handlePair, points) {
    if (this.referencedToolName !== 'OpenFreehandRoi') {
      return super._combineHandles(handlePair);
    }
    const { image } = this._sculptData;

    // Calculate combine position: half way between the handles.
    const midPoint = {
      x: (points[handlePair[0]].x + points[handlePair[1]].x) / 2.0,
      y: (points[handlePair[0]].y + points[handlePair[1]].y) / 2.0,
    };

    clipToBox(midPoint, image);

    // Move first point to midpoint
    points[handlePair[0]].x = midPoint.x;
    points[handlePair[0]].y = midPoint.y;

    // Link first point to handle that second point links to.
    const handleAfterPairIndex = this.constructor._getNextHandleIndex(
      handlePair[1],
      points.length
    );

    points[handlePair[0]].lines.pop();
    // Override the base to push only the co-ordinates to lines, not the FreehandHandleData
    points[handlePair[0]].lines.push({
      x: points[handleAfterPairIndex].x,
      y: points[handleAfterPairIndex].y,
    });

    // Link handle before first point with first point
    const handleBeforePairIndex = this.constructor._getPreviousHandleIndex(
      handlePair[0],
      points.length
    );

    points[handleBeforePairIndex].lines.pop();
    // Override the base to push only the co-ordinates to lines, not the FreehandHandleData
    points[handleBeforePairIndex].lines.push({
      x: points[handlePair[0]].x,
      y: points[handlePair[0]].y,
    });

    // Remove the latter handle
    points.splice(handlePair[1], 1);
  }

  /**
   * AddLinePoints - Adds a line point to a specific index of a freehand tool points array.
   *
   * @param  {Object[]} points      The array of points.
   * @param  {Number} insertIndex The index to insert the line point.
   * @returns {Null}             description
   */
  addLinePoints(points, insertIndex) {
    // Override the base to push only the co-ordinates to lines, not the FreehandHandleData
    // Add the line from the inserted handle to the handle after
    if (insertIndex === points.length - 1) {
      points[insertIndex].lines.push({ x: points[0].x, y: points[0].y });
    } else {
      points[insertIndex].lines.push({
        x: points[insertIndex + 1].x,
        y: points[insertIndex + 1].y,
      });
    }
  }

  /**
   * Ends the drawing loop.
   *
   * @private
   * @param {Object} evt - The event.
   * @returns {void}
   */
  _activeEnd(evt) {
    const eventData = evt.detail;
    const element = eventData.element;
    const toolState = getToolState(element, this.referencedToolName);
    const config = this.configuration;
    if (config.currentTool === null) {
      return;
    }
    const data = toolState.data[this.configuration.currentTool];
    if (this.referencedToolName === 'ContourRoi') {
      data.interpolated = false;
      interpolate([data], element);
      const eventType = csTools.EVENTS.MEASUREMENT_MODIFIED;

      triggerEvent(element, eventType, {
        toolName: this.referencedToolName,
        toolType: this.referencedToolName, // Deprecation notice: toolType will be replaced by toolName
        element,
        measurementData: data,
      });
    }
    saveActionState(data, 'measurements', 'Modify', false, true);
    data.dirty = true;
    super._activeEnd(evt);
    data.active = false;
    this.configuration.showCollisionDialog(evt, data, element);
  }

  /**
   * Choose the tool radius from the mouse position relative to the active freehand
   * tool, and begin sculpting.
   *
   * @private
   * @param {Object} evt - The event.
   * @returns {void}
   */
  _initialiseSculpting(evt) {
    const eventData = evt.detail;
    const config = this.configuration;
    const element = eventData.element;
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (!isAnnotationToolEnabled) {
      return;
    }

    this._deselectAllTools(evt);
    this._selectFreehandTool(eventData);

    if (config.currentTool === null) {
      return;
    }
    this._active = true;

    // Interupt event dispatcher
    state.isMultiPartToolActive = true;

    this._configureToolSize(eventData);
    this._getMouseLocation(eventData);

    this._activateFreehandTool(element, config.currentTool);
    this._activateSculpt(element);

    const toolState = getToolState(element, this.referencedToolName);
    const data = toolState.data[this.configuration.currentTool];
    saveActionState(data, 'measurements', 'Modify', true);

    try {
      cornerstone.updateImage(eventData.element);
    } catch (e) {}
  }

  /**
   * Deactivates all freehand ROIs and change currentTool to null
   *
   * @private
   * @param {Object} evt - The event.
   * @returns {void}
   */
  // eslint-disable-next-line no-unused-vars
  _deselectAllTools(evt) {
    const freehandRoiTool = getToolForElement(
      this.element,
      'FreehandRoiSculptor'
    );
    if (
      !freehandRoiTool ||
      freehandRoiTool.mode === 'enabled' ||
      freehandRoiTool.mode === 'active'
    ) {
      super._deselectAllTools(evt);
      return;
    }
    const config = this.configuration;
    const toolData = getToolState(this.element, this.referencedToolName);

    config.currentTool = null;

    if (toolData) {
      for (let i = 0; i < toolData.data.length; i++) {
        toolData.data[i].active = false;
      }
    }

    try {
      cornerstone.updateImage(this.element);
    } catch (e) {}
  }

  /**
   * Finds the nearest handle to the mouse cursor for all freehand
   * data on the element.
   *
   * @private
   * @param {Object} element - The element.
   * @param {Object} eventData - Data object associated with the event.
   * @returns {Number} The tool index of the closest freehand tool.
   */
  _getClosestFreehandToolOnElement(element, eventData) {
    const pixelCoords = eventData.currentPoints.image;
    const closest = {
      distance: Infinity,
      toolIndex: undefined,
      referToolName: null,
    };
    this.referencedToolNames.forEach(referToolName => {
      const freehand = getToolForElement(element, referToolName);
      const toolState = getToolState(element, referToolName);

      if (!toolState) {
        return;
      }
      const data = toolState.data;

      for (let i = 0; i < data.length; i++) {
        if (data[i].readonly || !data[i].visible) {
          continue;
        }
        const distanceFromToolI = freehand.distanceFromPoint(
          element,
          data[i],
          pixelCoords
        );

        if (distanceFromToolI === -1) {
          continue;
        }

        if (distanceFromToolI < closest.distance) {
          closest.distance = distanceFromToolI;
          closest.toolIndex = i;
          closest.referToolName = referToolName;
        }
      }
    });
    this.referencedToolName = closest.referToolName;
    return closest.toolIndex;
  }
}

/**
 * To check two lines are intersecting
 * @param {Object} P1 first point of first line
 * @param {Object} P2 second point of first line
 * @param {Object} P3 first point of second line
 * @param {Object} P4 second point of second line
 * @returns {Object} intersection point
 */
function lineIntersection(P1, P2, P3, P4) {
  if (doesIntersect(P1, P2, P3, P4)) {
    // Line AB represented as a1x + b1y = c1
    const a1 = P2.y - P1.y;
    const b1 = P1.x - P2.x;
    const c1 = a1 * P1.x + b1 * P1.y;

    // Line CD represented as a2x + b2y = c2
    const a2 = P4.y - P3.y;
    const b2 = P3.x - P4.x;
    const c2 = a2 * P3.x + b2 * P3.y;

    const determinant = a1 * b2 - a2 * b1;

    if (determinant === 0) {
      // The lines are parallel. This is simplified
      // by returning a pair of empty object
      return false;
    } else {
      const x = (b2 * c1 - b1 * c2) / determinant;
      const y = (a1 * c2 - a2 * c1) / determinant;
      return { x, y };
    }
  } else {
    return false;
  }
}

/**
 * Return intersection points and index in ascending order of the distance from P1
 * @param {Object} P1 point in second freehand
 * @param {Object} P2 point in second freehand
 * @param {Array} circlePoints sculpt points
 * @returns
 */
function getOrderedLineIntersectionPoints(P1, P2, circlePoints) {
  const intersectionPoints = [];
  for (let j = 0; j < circlePoints.length; j++) {
    if (j > 0) {
      let oddIntersection = false;
      const P3 = circlePoints[j - 1];
      const P4 = circlePoints[j];
      const intersectionPoint = lineIntersection(P1, P2, P3, P4);
      if (intersectionPoint) {
        const distSquared = distanceSquared(P1, intersectionPoint);
        intersectionPoints.push({
          index: j,
          distanceSquared: distSquared,
          intersectionPoint,
          oddIntersection,
        });
        oddIntersection = !oddIntersection;
      }
    }
  }
  return intersectionPoints.sort(
    (i1, i2) => i1.distanceSquared - i2.distanceSquared
  );
}

/**
 * Returns points inside the sculpt tool.
 * @param {Number} toolSizeCanvas  sculpt tool size in canvas
 * @param {Object} mousePointCanvas mouse point coordinates in canvas
 * @param {Object} points freehand points
 * @param {Object} element
 * @returns
 */
function getPointsInSculpt(toolSizeCanvas, mousePointCanvas, points, element) {
  const insidePoints = [];
  for (let i = 0; i < points.length; i++) {
    const distanceToHandleCanvas = cornerstoneMath.point.distance(
      cornerstone.pixelToCanvas(element, points[i]),
      mousePointCanvas
    );
    if (distanceToHandleCanvas < toolSizeCanvas) {
      insidePoints.push(i);
    }
  }
  return insidePoints;
}

const distanceSquared = (P1, P2) =>
  (P1.x - P2.x) * (P1.x - P2.x) + (P1.y - P2.y) * (P1.y - P2.y);
