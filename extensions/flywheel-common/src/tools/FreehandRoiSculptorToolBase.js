import cornerstone from 'cornerstone-core';
import cornerstoneMath from 'cornerstone-math';
import csTools from 'cornerstone-tools';
const { state } = csTools.store;

// Drawing
const getNewContext = csTools.importInternal('drawing/getNewContext');
const drawHandles = csTools.importInternal('drawing/drawHandles');

// Util
const clipToBox = csTools.importInternal('util/clipToBox');

const getToolForElement = csTools.getToolForElement;
const getToolState = csTools.getToolState;
const CornerstoneFreehandRoiSculptorTool = csTools.FreehandRoiSculptorTool;

/**
 * @public
 * @class FreehandRoiSculptorToolBase
 * @memberof Tools
 *
 * @classdesc Tool for easily sculpting annotations drawn with
 * the FreehandRoiTool.
 * Note: This is extended as part of fixing the cornerstone FreehandRoiSculptorTool
 * handles update and cursor positioning due to mix usage of pixel and canvas coordinates position/distance
 * Here updated to use the canvas coordinates consistently in all comparison scenarios which also avoids the
 * complexity of having the image with non-square pixel spacing
 *
 * @extends CornerstoneFreehandRoiSculptorTool
 */
export default class FreehandRoiSculptorToolBase extends CornerstoneFreehandRoiSculptorTool {
  constructor(props = {}) {
    super(props);
  }

  renderToolData(evt) {
    const eventData = evt.detail;

    if (this.configuration.currentTool === null) {
      return false;
    }

    const element = eventData.element;

    if (!this._isCurrentToolDataExistInToolState(element)) {
      return false;
    }

    if (this._active) {
      const context = getNewContext(eventData.canvasContext.canvas);
      const options = {
        color: this.configuration.dragColor,
        fill: null,
        handleRadius: this._toolSizeCanvas,
      };

      drawHandles(
        context,
        eventData,
        this.configuration.mouseLocation.handles,
        options
      );
    } else if (this.configuration.showCursorOnHover && !this._recentTouchEnd) {
      this._renderHoverCursor(evt);
    }
  }

  /**
   * Checks the current tool data exist in tool state
   *
   * @private
   * @param  {Object} element
   * @returns {Boolean} true if data exist
   */
  _isCurrentToolDataExistInToolState(element) {
    const toolState = getToolState(element, this.referencedToolName);
    const data = toolState.data[this.configuration.currentTool];

    return data ? true : false;
  }

  /**
   * Renders the cursor
   *
   * @private
   * @param  {type} evt description
   * @returns {void}
   */
  _renderHoverCursor(evt) {
    const eventData = evt.detail;
    const element = eventData.element;
    const context = getNewContext(eventData.canvasContext.canvas);

    const toolState = getToolState(element, this.referencedToolName);
    const data = toolState.data[this.configuration.currentTool];

    this._recentTouchEnd = false;

    let coords;

    if (this.configuration.mouseUpRender) {
      coords = this.configuration.mouseLocation.handles.start;
      this.configuration.mouseUpRender = false;
    } else {
      coords = state.mousePositionImage;
    }

    const freehandRoiTool = getToolForElement(element, this.referencedToolName);
    let radiusCanvas = freehandRoiTool.distanceFromPointCanvas(
      element,
      data,
      coords
    );

    this.configuration.mouseLocation.handles.start.x = coords.x;
    this.configuration.mouseLocation.handles.start.y = coords.y;

    if (this.configuration.limitRadiusOutsideRegion) {
      const unlimitedRadius = radiusCanvas;

      radiusCanvas = this._limitCursorRadiusCanvas(eventData, radiusCanvas);

      // Fade if distant
      if (
        unlimitedRadius >
        this.configuration.hoverCursorFadeDistance * radiusCanvas
      ) {
        context.globalAlpha = this.configuration.hoverCursorFadeAlpha;
      }
    }

    const options = {
      fill: null,
      color: this.configuration.hoverColor,
      handleRadius: radiusCanvas,
    };

    drawHandles(
      context,
      eventData,
      this.configuration.mouseLocation.handles,
      options
    );

    if (this.configuration.limitRadiusOutsideRegion) {
      context.globalAlpha = 1.0; // Reset drawing alpha for other draw calls.
    }
  }

  /**
   * Sculpts the freehand ROI with the circular freehandSculpter tool, moving,
   * adding and removing handles as necessary.
   *
   * @private
   * @param {Object} eventData - Data object associated with the event.
   * @param {Object} points - ROI continuous points.
   * @returns {void}
   */
  _sculpt(eventData, points) {
    const config = this.configuration;

    this._sculptData = {
      element: eventData.element,
      image: eventData.image,
      mousePoint: eventData.currentPoints.image,
      mousePointCanvas: eventData.currentPoints.canvas,
      points,
      toolSize: this._toolSizeImage,
      toolSizeCanvas: this._toolSizeCanvas,
      minSpacing: config.minSpacing,
      maxSpacing: Math.max(this._toolSizeCanvas, config.minSpacing * 2),
    };

    // Push existing handles radially away from tool.
    const pushedHandles = this._pushHandles();
    // Insert new handles in sparsely populated areas of the
    // Pushed part of the contour.

    if (pushedHandles.first !== undefined) {
      this._insertNewHandles(pushedHandles);
      // If any handles have been pushed very close together or even overlap,
      // Combine these into a single handle.
      this._consolidateHandles();
    }
  }

  /**
   * _pushHandles -Pushes the points radially away from the mouse if they are
   * contained within the circle defined by the freehandSculpter's toolSize and
   * the mouse position.
   *
   * @returns {Object}  The first and last pushedHandles.
   */
  _pushHandles() {
    const {
      points,
      mousePointCanvas,
      toolSizeCanvas,
      element,
    } = this._sculptData;
    const pushedHandles = {};

    for (let i = 0; i < points.length; i++) {
      const distanceToHandleCanvas = cornerstoneMath.point.distance(
        cornerstone.pixelToCanvas(element, points[i]),
        mousePointCanvas
      );

      if (
        distanceToHandleCanvas > toolSizeCanvas ||
        isNaN(distanceToHandleCanvas)
      ) {
        continue;
      }

      // Push point if inside circle, to edge of circle.
      this._pushOneHandle(i, distanceToHandleCanvas);
      if (pushedHandles.first === undefined) {
        pushedHandles.first = i;
        pushedHandles.last = i;
      } else {
        pushedHandles.last = i;
      }
    }

    return pushedHandles;
  }

  /**
   * Pushes one handle.
   *
   * @private
   * @param {number} i - The index of the handle to push.
   * @param {number} distanceToHandle - The distance between the mouse cursor and the handle.
   * @param {Array} points - ROI continuous points if null use it from the sculpt data.
   * @returns {void}
   */
  _pushOneHandle(i, distanceToHandleCanvas, points = this._sculptData.points) {
    const {
      mousePointCanvas,
      toolSizeCanvas,
      element,
      image,
    } = this._sculptData;
    const handle = points[i];
    const handleCanvas = cornerstone.pixelToCanvas(element, points[i]);

    const directionUnitVector = {
      x: (handleCanvas.x - mousePointCanvas.x) / distanceToHandleCanvas,
      y: (handleCanvas.y - mousePointCanvas.y) / distanceToHandleCanvas,
    };

    const positionCanvas = {
      x: mousePointCanvas.x + toolSizeCanvas * directionUnitVector.x,
      y: mousePointCanvas.y + toolSizeCanvas * directionUnitVector.y,
    };

    const position = cornerstone.canvasToPixel(element, positionCanvas);
    clipToBox(position, image);

    handle.x = position.x;
    handle.y = position.y;

    if (isNaN(handle.x) || isNaN(handle.y)) {
      return;
    }

    // Push lines
    const lastHandleIndex = this.constructor._getPreviousHandleIndex(
      i,
      points.length
    );

    points[lastHandleIndex].lines.pop();
    points[lastHandleIndex].lines.push({ x: handle.x, y: handle.y });
  }

  /**
   * Returns an array of indicies that describe where new handles should be
   * inserted (where the distance between subsequent handles is >
   * config.maxSpacing).
   *
   * @private
   * @param {Object} pushedHandles - The first and last handles that were pushed.
   * @param {Array} points - ROI continuous points if null use it from the sculpt data.
   * @returns {Object} An array of indicies that describe where new handles should be inserted.
   */
  _findNewHandleIndicies(pushedHandles, points = this._sculptData.points) {
    const { maxSpacing } = this._sculptData;
    const indiciesToInsertAfter = [];

    for (let i = pushedHandles.first; i <= pushedHandles.last; i++) {
      this._checkSpacing(i, points, indiciesToInsertAfter, maxSpacing);
    }

    const pointAfterLast = this.constructor._getNextHandleIndex(
      pushedHandles.last,
      points.length
    );

    // Check points before and after those pushed.
    if (pointAfterLast !== pushedHandles.first) {
      this._checkSpacing(
        pointAfterLast,
        points,
        indiciesToInsertAfter,
        maxSpacing
      );

      const pointBeforeFirst = this.constructor._getPreviousHandleIndex(
        pushedHandles.first,
        points.length
      );

      if (pointBeforeFirst !== pointAfterLast) {
        this._checkSpacing(
          pointBeforeFirst,
          points,
          indiciesToInsertAfter,
          maxSpacing
        );
      }
    }

    return indiciesToInsertAfter;
  }

  /**
   * _checkSpacing - description
   *@modifies indiciesToInsertAfter
   *
   * @param {number} i - The index to check.
   * @param {Object} points - The points.
   * @param {Array} indiciesToInsertAfter - The working list of indicies to insert new points after.
   * @param {number} maxSpacing
   * @returns {void}
   */
  _checkSpacing(i, points, indiciesToInsertAfter, maxSpacing) {
    const { element } = this._sculptData;
    const nextHandleIndex = this.constructor._getNextHandleIndex(
      i,
      points.length
    );

    const distanceToNextHandle = cornerstoneMath.point.distance(
      cornerstone.pixelToCanvas(element, points[i]),
      cornerstone.pixelToCanvas(element, points[nextHandleIndex])
    );

    if (distanceToNextHandle > maxSpacing) {
      indiciesToInsertAfter.push(i);
    }
  }

  /**
   * Finds pairs of close handles with seperations < config.minSpacing. No handle
   * is included in more than one pair, to avoid spurious deletion of densely
   * populated regions of the contour (see mergeCloseHandles).
   * @param {Array} points - ROI continuous points if null use it from the sculpt data.
   * @private
   * @returns {Array} An array of close pairs in points.
   */
  _findCloseHandlePairs(points = this._sculptData.points) {
    const { minSpacing, element } = this._sculptData;
    const closePairs = [];
    let length = points.length;

    for (let i = 0; i < length; i++) {
      const nextHandleIndex = this.constructor._getNextHandleIndex(
        i,
        points.length
      );

      const distanceToNextHandle = cornerstoneMath.point.distance(
        cornerstone.pixelToCanvas(element, points[i]),
        cornerstone.pixelToCanvas(element, points[nextHandleIndex])
      );

      if (distanceToNextHandle < minSpacing) {
        const pair = [i, nextHandleIndex];

        closePairs.push(pair);

        // Don't check last node if first in pair to avoid double counting.
        if (i === 0) {
          length -= 1;
        }

        // Don't double count pairs in order to prevent your polygon collapsing to a singularity.
        i++;
      }
    }

    return closePairs;
  }

  /**
   * Limits the cursor radius so that it its maximum area is the same as the
   * ROI being sculpted.
   * Note: Overriding the base implementation as the limits return NAN for canvas radius
   * when the images are rotated, so base version updated to use the absolute value of canvas area
   *
   * @private
   * @param  {Object}  eventData    Data object associated with the event.
   * @param  {Number}  radius       The distance from the mouse to the ROI.
   * @param  {Boolean} canvasCoords Whether the calculation should be performed
   *                                In canvas coordinates.
   * @returns {Number}              The limited radius.
   */
  _limitCursorRadius(eventData, radius, canvasCoords = false) {
    const element = eventData.element;
    const image = eventData.image;
    const config = this.configuration;

    const toolState = getToolState(element, this.referencedToolName);
    const data = toolState.data[config.currentTool];

    let areaModifier = 1.0;

    if (canvasCoords) {
      const topLeft = cornerstone.pixelToCanvas(element, {
        x: 0,
        y: 0,
      });
      const bottomRight = cornerstone.pixelToCanvas(element, {
        x: image.width,
        y: image.height,
      });

      // Taken absolute value of in canvas area calculation to taken care image rotation scenarios
      const canvasArea = Math.abs(
        (bottomRight.x - topLeft.x) * (bottomRight.y - topLeft.y)
      );

      areaModifier = canvasArea / (image.width * image.height);
    }

    const measureArea = (data.area ? data.area : 0) * areaModifier;
    const minDim = Math.min(image.width, image.height);

    // Considering the area of 1/5th of the minimum dimension square for maximum radius calculation.
    // This is to adjust the radius in scenarios where freehand drawing as a line like shape where
    // area is negligible.
    const imageSampleArea = Math.pow(minDim / 5, 2) * areaModifier;
    const maxRadius = Math.max(
      Math.pow(imageSampleArea / Math.PI, 0.5),
      Math.pow(measureArea / Math.PI, 0.5)
    );

    return Math.min(radius, maxRadius);
  }

  /**
   * Calculates the position that a new handle should be inserted.
   *
   * @private
   * @static
   * @param {Number} insertIndex - The index to insert the new handle.
   * @param {Number} previousIndex - The previous index.
   * @param {Number} nextIndex - The next index.
   * @param {Array} points - ROI continuous points if null use it from the sculpt data.
   * @returns {Object} The position the handle should be inserted.
   */
  _getInsertPosition(
    insertIndex,
    previousIndex,
    nextIndex,
    points = this._sculptData.points
  ) {
    const {
      toolSizeCanvas,
      mousePointCanvas,
      element,
      image,
    } = this._sculptData;

    // Calculate insert position: half way between the handles, then pushed out
    // Radially to the edge of the freehandSculpter.
    const midPoint = {
      x: (points[previousIndex].x + points[nextIndex].x) / 2.0,
      y: (points[previousIndex].y + points[nextIndex].y) / 2.0,
    };

    const midPointCanvas = cornerstone.pixelToCanvas(element, midPoint);

    const distanceToMidPointCanvas = cornerstoneMath.point.distance(
      mousePointCanvas,
      midPointCanvas
    );

    let insertPosition;

    if (distanceToMidPointCanvas < toolSizeCanvas) {
      const directionUnitVector = {
        x: (midPointCanvas.x - mousePointCanvas.x) / distanceToMidPointCanvas,
        y: (midPointCanvas.y - mousePointCanvas.y) / distanceToMidPointCanvas,
      };

      insertPosition = {
        x: mousePointCanvas.x + toolSizeCanvas * directionUnitVector.x,
        y: mousePointCanvas.y + toolSizeCanvas * directionUnitVector.y,
      };
      insertPosition = cornerstone.canvasToPixel(element, insertPosition);
    } else {
      insertPosition = midPoint;
    }

    clipToBox(insertPosition, image);

    return insertPosition;
  }
}
