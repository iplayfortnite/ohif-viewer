import areaLineBuilder from './areaLineBuilder';
import meanStdLineBuilder from './meanStdLineBuilder';
import minMaxLineBuilder from './minMaxLineBuilder';

const lineBuilders = {
  areaLineBuilder,
  meanStdLineBuilder,
  minMaxLineBuilder,
};

export default lineBuilders;
