import { getLineItem, inlineSpace } from '../common';
import * as unitUtils from '../../unit';

/**
 * * It returns a method line builder for given line type.
 * @param {number} min min value
 * @param {number} max max value
 * @param {string} modality Study modality
 * @param {boolean} showMinMax flag to show or not current line
 * @param {boolean} hasPixelSpacing if Study has pixel spacing value
 * @return {function} Area line builder
 */
const getMinMaxBuilder = (min, max, modality, showMinMax, hasPixelSpacing) => {
  return () => {
    if (!showMinMax) {
      return;
    }
    const unit = unitUtils.getUnitByModality(modality, hasPixelSpacing);
    let minString = getLineItem(min, 'Min:', unit);
    const maxString = getLineItem(max, 'Max:', unit);

    return inlineSpace(minString, maxString);
  };
};

export default getMinMaxBuilder;
