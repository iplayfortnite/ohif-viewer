import {
  addDefaultSpacing,
  addExtraSpacing,
  inlineSpace,
  getLineItem,
  maxStringLengthDifference,
} from '../common';
import * as unitUtils from '../../unit';

const noFormatter = value => value;

/**
 * It returns the lines representing mean and stdDev. It can add extra info regarding SUV (Standardized uptake value)
 * @param {number} mean mean
 * @param {number} stdDev standard deviation
 * @param {number} median median
 * @param {number} interQuartileRange Inter quartile Range
 * @param {Object} meanStdDevSUV Object containing mean standard deviation Standardized uptake value
 * @param {string} modality Study modality
 * @param {boolean} hasPixelSpacing if Study has pixel spacing value
 * @return {string | Array<string>}
 */
const getMeanStdLine = (
  mean,
  stdDev,
  median,
  interQuartileRange,
  meanStdDevSUV = {},
  modality,
  hasPixelSpacing
) => {
  const unit = unitUtils.getUnitByModality(modality, hasPixelSpacing);
  const meanString = getLineItem(mean, 'Mean:', unit);
  const stdDevString = getLineItem(stdDev, 'Std Dev:', unit);
  const medianString = getLineItem(median, 'Median:', unit);
  const IQRString = getLineItem(
    interQuartileRange,
    'Interquartile Range:',
    unit
  );

  const meanSuvString = getLineItem(
    meanStdDevSUV.mean,
    'SUV:',
    unit,
    true,
    noFormatter
  );
  const stdDevSuvString = getLineItem(
    meanStdDevSUV.stdDev,
    'SUV:',
    unit,
    true,
    noFormatter
  );

  if (meanSuvString && stdDevSuvString) {
    const extraSpacing = maxStringLengthDifference(
      meanString,
      addDefaultSpacing(stdDevString)
    );
    const line1 = `${addExtraSpacing(
      meanString,
      extraSpacing
    )}${meanSuvString}`;
    const line2 = inlineSpace(stdDevString, stdDevSuvString);

    return [line1, line2];
  }

  return [meanString, stdDevString, medianString, IQRString];
};

/**
 * It returns a method line builder for given line type.
 * @param {number} mean mean
 * @param {number} stdDev standard devination
 * @param {Object} meanStdDevSUV Object containing mean standard devination SUV
 * @param {string} modality Study modality
 * @param {boolean} showMeanStd flag to show or not current line
 * @param {boolean} hasPixelSpacing Study has pixel spacing value
 * @return {function} Area line builder
 */
const getMeanStdBuilder = (
  mean,
  stdDev,
  median,
  interQuartileRange,
  meanStdDevSUV,
  modality,
  showMeanStd,
  hasPixelSpacing
) => {
  return () =>
    showMeanStd &&
    getMeanStdLine(
      mean,
      stdDev,
      median,
      interQuartileRange,
      meanStdDevSUV,
      modality,
      hasPixelSpacing
    );
};

export default getMeanStdBuilder;
