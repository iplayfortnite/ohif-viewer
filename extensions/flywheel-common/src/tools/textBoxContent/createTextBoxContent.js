import {
  addExtraSpacing,
  getMaxLength,
  getStringLength,
  MINIMUM_SPACE_BETWEEN_STR_BLOCKS,
} from './common';

import lineBuilders from './lineBuilders';

const { areaLineBuilder, meanStdLineBuilder, minMaxLineBuilder } = lineBuilders;

/**
 * It returns justified lines based on given lines param.
 * Its required to previous identify the max length among lines
 *
 * @param {Array<lines>} lines to be processed
 * @param {number} maxLength the max length value from given lines
 * @param {Object} context
 * @return {Array<string>} lines
 */
const _justifyLines = (lines = [], maxLength, context) => {
  return lines.map(line => {
    const lineStrings = line.split(MINIMUM_SPACE_BETWEEN_STR_BLOCKS);
    // do not justify in case one word block only
    if (lineStrings.length === 1) {
      return line;
    }
    // groups means non wrap words
    const groupLength = getStringLength(lineStrings.join(''), context);
    const differenceLength = Math.max(0, maxLength - groupLength);
    // space to add among each group
    const extraSpacing = Math.floor(
      differenceLength / (lineStrings.length - 1)
    );

    return lineStrings.reduce((acc, value, index) => {
      return index ? `${addExtraSpacing(acc, extraSpacing)}${value}` : value;
    }, '');
  });
};

/**
 * It iterates on lineBuilders iterator and mount an array of lines
 *
 * @param {Array} iterators Array of line builders
 * @param {Object} context App context
 * @param {boolean} justify to justify lines or not
 * @return {Array<string>} lines
 */
const _iterateLines = (iterators = [], context, justify = false) => {
  let lines = [];
  let maxLength = 0;

  iterators.forEach(lineBuilder => {
    const result = lineBuilder();
    if (typeof result === 'string') {
      lines.push(result);
    }

    if (Array.isArray(result)) {
      lines.push(...result);
    }
    maxLength = getMaxLength(maxLength, context, result);
  });

  if (justify) {
    lines = _justifyLines(lines, maxLength, context);
  }

  return lines;
};

/**
 * Copied from csTools/annotation/Elliptical with some Flywheel adjustments
 * It returns a list of strings representing the text box content.
 * @param {Object} context
 * @param {boolean} isColorImage
 * @param {Object} { area, mean, stdDev, min, max, meanStdDevSUV }
 * @param {string} modality Study modality
 * @param {boolean} hasPixelSpacing if Study has pixel spacing value
 * @param {*} [options={}] - { showMinMax, showHounsfieldUnits }
 * @returns {string[]}
 */
export default function createTextBoxContent(
  context,
  isColorImage,
  {
    area,
    mean = 0,
    stdDev = 0,
    median = 0,
    interQuartileRange = 0,
    min,
    max,
    meanStdDev = {},
    meanStdDevSUV,
  } = {},
  modality,
  hasPixelSpacing,
  options = {}
) {
  const showMinMax = options.showMinMax || false;

  const _mean = meanStdDev.mean || mean;
  const _stdDev = meanStdDev.stdDev || stdDev;
  const _median = meanStdDev.median || median;
  const _interQuartileRange =
    meanStdDev.interQuartileRange || interQuartileRange;

  const linesIterator = [];
  // get line builders
  const areaBuilder = areaLineBuilder(area, hasPixelSpacing);
  const meanStdBuilder = meanStdLineBuilder(
    _mean,
    _stdDev,
    _median,
    _interQuartileRange,
    meanStdDevSUV,
    modality,
    !isColorImage,
    hasPixelSpacing
  );
  const minMaxBuilder = minMaxLineBuilder(
    min,
    max,
    modality,
    !isColorImage && showMinMax,
    hasPixelSpacing
  );
  // default display order
  linesIterator.push(areaBuilder);
  linesIterator.push(meanStdBuilder);
  linesIterator.push(minMaxBuilder);

  return _iterateLines(linesIterator, context);
}
