import cornerstone from 'cornerstone-core';
import cornerstoneMath from 'cornerstone-math';
import csTools from 'cornerstone-tools';
import store from '@ohif/viewer/src/store';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';

import commonToolUtils from './commonUtils';
import * as TextBoxContentUtils from './textBoxContent';
import * as unitUtils from './unit';
import measurementToolUtils from './utils';
import { isPointInsideCircle, getCircleData } from './utils/CircleIntersect';
import { shouldEnableAnnotationTool } from './utils/shouldEnableAnnotationTool';

// Drawing
const getNewContext = csTools.importInternal('drawing/getNewContext');
const draw = csTools.importInternal('drawing/draw');
const setShadow = csTools.importInternal('drawing/setShadow');
const drawCircle = csTools.importInternal('drawing/drawCircle');
const drawHandles = csTools.importInternal('drawing/drawHandles');
const drawLinkedTextBox = csTools.importInternal('drawing/drawLinkedTextBox');

// manipulators
const moveHandleNearImagePoint = csTools.importInternal(
  'manipulators/moveHandleNearImagePoint'
);

const getHandleNearImagePoint = csTools.importInternal(
  'manipulators/getHandleNearImagePoint'
);

// State
const getToolState = csTools.getToolState;
const toolColors = csTools.toolColors;
const toolStyle = csTools.toolStyle;
const getModule = csTools.getModule;
const { state } = csTools.store;

// Util
const triggerEvent = csTools.importInternal('util/triggerEvent');
const getROITextBoxCoords = csTools.importInternal('util/getROITextBoxCoords');
const getPixelSpacing = csTools.importInternal('util/getPixelSpacing');
const getCircleCoords = csTools.importInternal('util/getCircleCoords');
const moveNewHandle = csTools.importInternal('manipulators/moveNewHandle');

const { circleRoiCursor } = csTools.importInternal('tools/cursors');

// set an alias
const CornerstoneCircleRoiTool = csTools.CircleRoiTool;

const { setNextPalette } = FlywheelCommonRedux.actions;
const {
  findToolIndex,
  saveActionState,
  upOrEndEventsManager,
} = commonToolUtils;

const { EVENTS, addToolState, removeToolState } = csTools;

/**
 * Improvement on csTool ROI tool to use FLYW custom textBox implementation
 * renderToolData method is similar to its original version (csTools)
 *
 * @public
 * @class CircleRoiTool
 * @memberof Tools
 * @classdesc Tool for drawing circular regions of interest, and measuring
 * the statistics of the enclosed pixels.
 * @extends CornerstoneCircleRoiTool
 */
export default class CircleRoiTool extends CornerstoneCircleRoiTool {
  static toolName = 'CircleRoiTool';

  constructor(props = {}) {
    super(props);
    this._endDrawing = this._endDrawing.bind(this);
  }

  // Modifies end handle to create a circle of configured default diameter, when
  // user creates single point annotations.
  setDefaultDiameterForSingleClick(element, image, measurementData) {
    const { start, end } = measurementData.handles;

    const centerOnCanvas = cornerstone.pixelToCanvas(element, start);
    const circumferencePointOnCanvas = cornerstone.pixelToCanvas(element, end);

    const radiusOnCanvas = cornerstoneMath.point.distance(
      centerOnCanvas,
      circumferencePointOnCanvas
    );

    // mouse drag tolerance in canvas while user annotates single points.
    const DRAG_TOLERANCE = 3;
    if (radiusOnCanvas > DRAG_TOLERANCE) {
      return;
    }
    const projectConfig = store.getState().flywheel.projectConfig;
    const defaultDiameter = projectConfig?.defaultCircleDiameter
      ? projectConfig.defaultCircleDiameter
      : 1;

    // Row pixel spacing and column pixel spacing is compared to create a
    // smaller circle in canvas with given diameter.
    const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(image);
    let radiusX = 0;
    let radiusY = 0;
    rowPixelSpacing < colPixelSpacing
      ? (radiusY = defaultDiameter / 2)
      : (radiusX = defaultDiameter / 2);
    end.x = start.x + radiusX;
    end.y = start.y + radiusY;

    const eventType = EVENTS.MEASUREMENT_MODIFIED;
    const eventData = {
      toolName: this.name,
      toolType: this.name, // Deprecation notice: toolType will be replaced by toolName
      element,
      measurementData,
    };
    triggerEvent(element, eventType, eventData);
  }

  createNewMeasurement(eventData) {
    const colorPalette = store.getState().colorPalette;
    const data = super.createNewMeasurement(eventData);
    const color = colorPalette.roiColor.next;
    store.dispatch(setNextPalette());
    return { ...data, color };
  }

  // Overriden method to create circle with configured diameter on single click.
  addNewMeasurement = (evt, interactionType) => {
    evt.preventDefault();
    evt.stopPropagation();

    const eventData = evt.detail;
    const { element, image } = eventData;

    const measurementData = this.createNewMeasurement(eventData);
    if (!measurementData) {
      return;
    }

    addToolState(element, this.name, measurementData);

    cornerstone.updateImage(element);

    moveNewHandle(
      eventData,
      this.name,
      measurementData,
      measurementData.handles.end,
      { ...this.options, hasMoved: true },
      interactionType,
      success => {
        if (measurementData.cancelled) {
          return;
        }

        if (success) {
          this.setDefaultDiameterForSingleClick(
            element,
            image,
            measurementData
          );

          const eventType = EVENTS.MEASUREMENT_COMPLETED;
          const eventData = {
            toolName: this.name,
            toolType: this.name, // Deprecation notice: toolType will be replaced by toolName
            element,
            measurementData,
          };
          triggerEvent(element, eventType, eventData);
        } else {
          removeToolState(element, this.name, measurementData);
        }
      }
    );
  };

  initializeDefaultCursor(evt, isAdd = true) {
    const cursorTools =
      state?.tools.filter(item => 'CircleCursorTool' === item.name) || [];

    if (cursorTools.length) {
      // Start or finish drawing based on isAdd parameter
      cursorTools.forEach(cursorTool =>
        isAdd
          ? cursorTool.addNewMeasurement?.(evt)
          : cursorTool.finishDrawing?.(evt.detail.element)
      );
    }
  }

  // Overriden method to use custom Unit strategy
  renderToolData(evt) {
    const toolData = getToolState(evt.currentTarget, this.name);

    if (!toolData) {
      return;
    }
    const getDistance = cornerstoneMath.point.distance;

    const eventData = evt.detail;
    const { image, element } = eventData;
    const lineWidth = toolStyle.getToolWidth();
    const lineDash = getModule('globalConfiguration').configuration.lineDash;
    const { handleRadius, renderDashed } = this.configuration;
    const context = getNewContext(eventData.canvasContext.canvas);
    const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(image);

    // Meta
    const seriesModule =
      cornerstone.metaData.get('generalSeriesModule', image.imageId) || {};

    // Pixel Spacing
    const modality = seriesModule.modality;
    const hasPixelSpacing = rowPixelSpacing && colPixelSpacing;

    const isAnyActiveData = toolData.data.find(data => data.active) || false;
    this.initializeDefaultCursor(evt, !isAnyActiveData);

    draw(context, context => {
      // If we have tool data for this element - iterate over each set and draw it
      for (let i = 0; i < toolData.data.length; i++) {
        const data = toolData.data[i];

        if (!data.visible) {
          continue;
        }

        // Configure
        let color = data.active
          ? toolColors.getActiveColor()
          : data.color || toolColors.getColorIfActive(data);
        color = unitUtils.convertRgbToRgba(color, data.color);

        let fillStyle = color;

        color = color.substring(0, color.lastIndexOf(',')) + ', 1)';
        if (measurementToolUtils.hasColorIntensity(data)) {
          color = measurementToolUtils.reduceColorOpacity(color);
          fillStyle = measurementToolUtils.reduceColorOpacity(fillStyle);
        }

        setShadow(context, this.configuration);

        const circleOptions = { color, fillStyle };

        if (renderDashed) {
          circleOptions.lineDash = lineDash;
        }
        const startCanvas = cornerstone.pixelToCanvas(
          element,
          data.handles.start
        );

        const endCanvas = cornerstone.pixelToCanvas(element, data.handles.end);
        const radius = getDistance(startCanvas, endCanvas);

        // Draw
        drawCircle(
          context,
          element,
          data.handles.start,
          radius,
          circleOptions,
          'pixel',
          data.handles.initialRotation
        );

        let { drawHandlesOnHover } = this.configuration;
        if (!drawHandlesOnHover) {
          const handleRad =
            data.handles.start.radius ||
            data.handles.end.radius ||
            handleRadius ||
            state.handleRadius;
          drawHandlesOnHover = radius <= handleRad;
        }

        const handleOptions = {
          color,
          handleRadius,
          drawHandlesIfActive: drawHandlesOnHover,
        };

        drawHandles(context, eventData, data.handles, handleOptions);

        // Update textbox stats
        if (data.invalidated) {
          if (data.cachedStats) {
            this.throttledUpdateCachedStats(image, element, data);
          } else {
            this.updateCachedStats(image, element, data);
          }
        }

        // Default to textbox on right side of ROI
        if (!data.handles.textBox.hasMoved) {
          const defaultCoords = getROITextBoxCoords(
            eventData.viewport,
            data.handles
          );

          data.handles.textBox = {
            ...data.handles.textBox,
            ...defaultCoords,
          };
        }

        const textBoxAnchorPoints = handles => {
          const circleCoords = getCircleCoords(
            handles.start,
            handles.end,
            element
          );
          return TextBoxContentUtils.findTextBoxAnchorPoints(
            { x: circleCoords.left, y: circleCoords.top },
            {
              x: circleCoords.left + circleCoords.width,
              y: circleCoords.top + circleCoords.height,
            }
          );
        };

        const textBoxContent = TextBoxContentUtils.createTextBoxContent(
          context,
          image.color,
          data.cachedStats,
          modality,
          hasPixelSpacing,
          this.configuration
        );

        data.unit = unitUtils.getUnitByModality(
          modality,
          hasPixelSpacing,
          this.configuration.showHounsfieldUnits
        );

        if (unitUtils.isRoiOutputHidden(this.name)) {
          // Temporary status flag to indicate measurement visibility in
          // viewport, not to be saved in server.
          data.handles.textBox.isVisible = false;
          data.handles.textBox.boundingBox = TextBoxContentUtils.getDefaultBoundingBox(
            data.handles.textBox
          );
        } else {
          drawLinkedTextBox(
            context,
            element,
            data.handles.textBox,
            textBoxContent,
            data.handles,
            textBoxAnchorPoints,
            color,
            lineWidth,
            10,
            true
          );
        }
      }
    });
  }

  pointNearTool(
    element,
    data,
    coords,
    interactionType,
    mouseProximity = state.handleRadius || 6
  ) {
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (data.readonly || !data.visible || !isAnnotationToolEnabled) {
      return false;
    }

    const hasStartAndEndHandles =
      data && data.handles && data.handles.start && data.handles.end;

    const getDistance = cornerstoneMath.point.distance;

    if (!hasStartAndEndHandles) {
      logger.warn(
        `invalid parameters supplied to tool ${this.name}'s pointNearTool`
      );
    }

    if (!hasStartAndEndHandles || data.visible === false) {
      return false;
    }

    const handleNearImagePoint = getHandleNearImagePoint(
      element,
      data.handles,
      coords,
      mouseProximity
    );

    if (handleNearImagePoint) {
      return true;
    }

    const distance = interactionType === 'mouse' ? mouseProximity : 25;

    const startCanvas = cornerstone.pixelToCanvas(element, data.handles.start);

    const endCanvas = cornerstone.pixelToCanvas(element, data.handles.end);

    // StartCanvas is the center of the circle
    const distanceFromCenter = getDistance(startCanvas, coords);

    // Getting radius of circle annotation in canvas
    const radius = getDistance(startCanvas, endCanvas);

    // Checking if point is near the tool by comparing its distance from the center of the circle
    const isHighlight =
      distanceFromCenter > radius - distance / 2 &&
      distanceFromCenter < radius + distance / 2;

    return !isHighlight
      ? this.pointInsideTool(element, data, coords)
      : isHighlight;
  }

  pointInsideTool(element, data, coords) {
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (data.readonly || !data.visible || !isAnnotationToolEnabled) {
      return false;
    }

    const hasStartAndEndHandles =
      data && data.handles && data.handles.start && data.handles.end;

    if (!hasStartAndEndHandles || data.visible === false) {
      return false;
    }

    const { start, end } = data.handles;
    const { radius, center } = getCircleData(start, end, element);
    const point = cornerstone.canvasToPixel(element, coords);
    const isPointInsideTool = isPointInsideCircle(
      point,
      center,
      radius,
      element
    );
    return isPointInsideTool;
  }

  /**
   * Custom callback for when a handle is selected.
   * @method handleSelectedCallback
   * @memberof Tools.Base.BaseAnnotationTool
   *
   * @param  {*} evt    -
   * @param  {*} toolData   -
   * @param  {*} handle - The selected handle.
   * @param  {String} interactionType -
   * @returns {void}
   */
  handleSelectedCallback(evt, toolData, handle, interactionType = 'mouse') {
    const eventData = evt.detail;
    const { element } = eventData;
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (!toolData.readonly && toolData.visible && isAnnotationToolEnabled) {
      moveHandleNearImagePoint(evt, this, toolData, handle, interactionType);
      this._initializeDrawing(evt, handle, interactionType);
    }
  }

  /**
   * Custom callback for when a tool is selected.
   *
   * @method toolSelectedCallback
   * @memberof Tools.Base.BaseAnnotationTool
   *
   * @param  {*} evt
   * @param  {*} annotation
   * @param  {string} [interactionType=mouse]
   * @returns {void}
   */
  toolSelectedCallback(evt, annotation, interactionType = 'mouse') {
    if (!annotation.readonly && annotation.visible) {
      super.toolSelectedCallback(evt, annotation, interactionType);
      this._initializeDrawing(evt, annotation.handles?.start, interactionType);
    }
  }

  /**
   * Initialize the drawing loop when tool is active and a click event happens.
   *
   * @private
   * @param {Object} evt - The event.
   * @param  {*} handle - Selected handle or any of the handle.
   * @param  {string} [interactionType=mouse]
   * @returns {void}
   */
  _initializeDrawing(evt, handle, interactionType = 'mouse') {
    const { element } = evt.detail;
    const currentTool = findToolIndex(element, this.name, handle);
    if (currentTool >= 0) {
      this.configuration.currentTool = currentTool;
      this._startDrawing(evt);
      upOrEndEventsManager.registerHandler(
        element,
        interactionType,
        this._endDrawing
      );
    }
  }

  /**
   * Beginning of drawing loop when tool is active and a click event happens.
   *
   * @private
   * @param {Object} evt - The event.
   * @returns {void}
   */
  _startDrawing(evt) {
    const { element } = evt.detail;
    const toolState = getToolState(element, this.name);
    const data = toolState.data[this.configuration.currentTool];
    saveActionState(data, 'measurements', 'Modify', true, false);
  }

  /**
   * Ends the active drawing loop and completes the polygon.
   *
   * @private
   * @param {Object} evt - The event.
   * @returns {void}
   */
  _endDrawing(evt) {
    const { element } = evt.detail;
    const interactionType = upOrEndEventsManager.getCurrentInteractionType(evt);
    upOrEndEventsManager.removeHandler(
      element,
      interactionType,
      this._endDrawing
    );
    const toolState = getToolState(element, this.name);
    const data = toolState.data[this.configuration.currentTool];
    saveActionState(data, 'measurements', 'Modify', false, true);

    this.configuration.currentTool = -1;
    this.fireModifiedEvent(element, data);
  }

  /** Ends the active drawing loop forcefully.
   *
   * @public
   * @param {Object} element - The element on which the roi is being drawn.
   * @returns {null}
   */
  finishDrawing(element) {
    const evt = { detail: { element } };
    if (this.configuration.currentTool >= 0) {
      this._endDrawing(evt);
    }
  }

  /**
   * Fire MEASUREMENT_MODIFIED event on provided element
   * @param {any} element which freehand data has been modified
   * @param {any} measurementData the measurement data
   * @returns {void}
   */
  fireModifiedEvent(element, measurementData) {
    const eventType = csTools.EVENTS.MEASUREMENT_MODIFIED;
    const eventData = {
      toolName: this.name,
      toolType: this.name, // Deprecation notice: toolType will be replaced by toolName
      element,
      measurementData,
    };

    triggerEvent(element, eventType, eventData);
  }

  getTextInfo(measurementData, data = null) {
    if (data) {
      const seriesModule =
        cornerstone.metaData.get('generalSeriesModule', data.image.imageId) ||
        {};
      const modality = seriesModule.modality;
      const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(data.image);
      const hasPixelSpacing = rowPixelSpacing && colPixelSpacing;
      return TextBoxContentUtils.getTextBoxContent(
        data.image.color,
        measurementData.cachedStats,
        modality,
        hasPixelSpacing,
        this.configuration
      );
    }
    return [];
  }
}
