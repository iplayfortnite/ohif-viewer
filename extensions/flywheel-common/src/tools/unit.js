const UNIT_MAP = {
  HU: 'HU',
  AU: 'AU',
  NONE: '',
  HZ: 'Hz',
  PX: 'px',
  MM: 'mm',
  SUV: 'SUV',
  Micron: 'μm',
  MicroPX: 'μpx',
};

/**
 * Get unit based on given modality
 * @param {string} modality Study modality
 * @param {boolean} showHounsfieldUnits boolean to show HU or not
 * @return {string} unit value
 */
export function getUnitByModality(
  modality,
  hasPixelSpacing,
  showHounsfieldUnits
) {
  let unit = '';
  switch (modality) {
    case 'CT':
      unit = showHounsfieldUnits !== false ? UNIT_MAP.HU : UNIT_MAP.NONE; // default implementation consider showHounsfieldUnits
      break;
    case 'MR':
      unit = UNIT_MAP.AU;
      break;
    case 'US':
      unit = UNIT_MAP.HZ;
      break;
    case 'PT':
      unit = UNIT_MAP.SUV;
      break;
    case 'OP':
      unit = UNIT_MAP.PX;
      break;
    case 'OPT':
      unit = UNIT_MAP.MM;
      break;
    default:
      unit = getLengthUnit(hasPixelSpacing);
  }

  return unit;
}

/**
 * Get length unit. It considers if there is pixelSpacing values for given study.
 * If present csTools can calculate real length, otherwise use px
 * @param {boolean} hasPixelSpacing
 * @param {number} area
 * @return {string} unit value
 */
export function getLengthUnit(hasPixelSpacing, measurement) {
  const shouldConvertToMicro = shouldConvertToMicrometer(measurement);
  let unit =
    !shouldConvertToMicro || measurement === 0.0
      ? UNIT_MAP.PX
      : UNIT_MAP.MicroPX;
  if (hasPixelSpacing) {
    unit =
      !shouldConvertToMicro || measurement === 0.0
        ? UNIT_MAP.MM
        : UNIT_MAP.Micron;
  }
  return unit;
}

/**
 * It returns squared value for given unit.
 *
 * @param {string} unit string unit value
 * @return {string} squared string unit value
 */
export const squareUnit = unit => `${unit}${String.fromCharCode(178)}`;

/**
 * It returns a method builder for given line type.
 * @param {number} measurementValue measurement value
 * @return {boolean} shouldConvertToMicro
 */
export const shouldConvertToMicrometer = measurementValue => {
  const shouldConvertToMicro = Math.floor(measurementValue * 100) === 0;
  return shouldConvertToMicro;
};

/**
 * It converts rgba color form rgb color and returns rgba color
 *
 * @param {string} color string color value
 * @param {string} measurementColor string measurementColor value
 * @return {string} rgba string color value
 */
export const convertRgbToRgba = (color, measurementColor) => {
  if (color.indexOf('rgb(') === 0) {
    const opacity = measurementColor
      ? measurementColor.substring(
          measurementColor.lastIndexOf(',') + 1,
          measurementColor.length - 1
        )
      : '0.2';
    color = color.replace('rgb(', 'rgba(').replace(')', ', ' + opacity + ')');
  }
  return color;
};

export const isRoiOutputHidden = tool => {
  const store = window.store.getState();
  const projectConfig = store.flywheel.projectConfig;
  if (
    projectConfig?.hideROIOutput ||
    projectConfig?.hideMeasurementsForTools?.includes(tool)
  ) {
    return true;
  }
  return false;
};

export const isMeasurementOutputHidden = () => {
  const store = window.store.getState();
  const projectConfig = store.flywheel.projectConfig;
  if (projectConfig && projectConfig.hideMeasurementOutput) {
    return true;
  }
  return false;
};

export const isParallelMeasurementOutputHidden = () => {
  const projectConfig = store.getState().flywheel?.projectConfig;
  if (projectConfig && projectConfig.hideParallelLineMeasurement) {
    return true;
  }
  return false;
};
