import csTools from 'cornerstone-tools';
import upOrEndEventsManager from './commonUtils/manageUpOrEndEvents';

// set an alias
const CornerstoneEraserTool = csTools.EraserTool;

/**
 * @public
 * @class EraserTool
 * @memberof Tools
 *
 * @classdesc Tool for deleting the data of other Annotation Tools.
 * @extends CornerstoneEraserTool
 */
export default class EraserTool extends CornerstoneEraserTool {
  _deleteAllNearbyTools(evt) {
    const interactionType = upOrEndEventsManager.getCurrentInteractionType(evt);
    const consumeEvent = super._deleteAllNearbyTools(evt, interactionType);

    evt.preventDefault();
    evt.stopPropagation();

    return consumeEvent;
  }
}
