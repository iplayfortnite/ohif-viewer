import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import LengthTool from './LengthTool';
import { shiftToBoxInSameOrientation, shiftPoints } from './utils/adjustPoints';
import cornerstoneMath from 'cornerstone-math';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';

const { point } = cornerstoneMath;
const { setNextPalette } = FlywheelCommonRedux.actions;

// set an alias
const FlywheelLengthTool = LengthTool;

// State
const getToolState = csTools.getToolState;
const addToolState = csTools.addToolState;

// Util
const triggerEvent = csTools.importInternal('util/triggerEvent');

// Manipulators
const anyHandlesOutsideImage = csTools.importInternal(
  'manipulators/anyHandlesOutsideImage'
);

/**
 * New tool on flywheel Length tool to create parallel length measurements.
 *
 * @public
 * @class ParallelTool
 * @memberof Tools.Annotation
 * @classdesc Tool for creating parallel length measurements.
 * @extends FlywheelLengthTool
 */
export default class ParallelTool extends FlywheelLengthTool {
  static toolName = 'ParallelTool';

  static referencedTools = ['Length'];
  static lineOffset = 30;

  constructor() {
    const props = {
      name: 'Parallel',
      referencedToolNames: ParallelTool.referencedTools,
    };
    super(props);
  }

  createNewMeasurement() {}

  preMouseDownCallback(evt) {
    const { element, currentPoints } = evt.detail;
    const coords = currentPoints.canvas;
    let activeData;
    let referencedTool;

    for (let i = 0; i < ParallelTool.referencedTools.length; i++) {
      referencedTool = ParallelTool.referencedTools[i];
      const toolState = getToolState(element, referencedTool);

      for (let d = 0; d < toolState?.data?.length; d++) {
        const data = toolState.data[d];
        const nearTool = super.pointNearTool(element, data, coords, true);
        if (nearTool) {
          activeData = data;
          break;
        }
      }
    }

    if (activeData) {
      let { start, end } = activeData.handles;

      let activeDataMidPoint = {
        x: (start.x + end.x) / 2,
        y: (start.y + end.y) / 2,
      };

      const length = point.distance(start, end);

      // Normalized  direction vector
      let activeDataDir = {
        x: (end.x - start.x) / length,
        y: (end.y - start.y) / length,
      };

      let imageCenter = {
        x: evt.detail.image.width / 2,
        y: evt.detail.image.height / 2,
      };

      const offset =
        ParallelTool.lineOffset * 0.75 +
        (ParallelTool.lineOffset / 2) * Math.random();
      let perpendicularDir = {
        x: -activeDataDir.y * offset,
        y: activeDataDir.x * offset,
      };

      // Perpendicular point at lineoffset distance from mid point
      let perpendicularPoint = {
        x: activeDataMidPoint.x + perpendicularDir.x,
        y: activeDataMidPoint.y + perpendicularDir.y,
      };

      if (
        point.distanceSquared(activeDataMidPoint, imageCenter) <
        point.distanceSquared(perpendicularPoint, imageCenter)
      ) {
        // Find perpendicualr point closer to direction of image center
        perpendicularDir = {
          x: -perpendicularDir.x,
          y: -perpendicularDir.y,
        };
      }

      activeData.hasParallels = true;
      let parallelData = createNewLengthData(activeData, perpendicularDir);

      Object.keys(parallelData.handles).forEach(key => {
        if (anyHandlesOutsideImage(evt.detail, [parallelData.handles[key]])) {
          shiftToBoxInSameOrientation(
            parallelData.handles[key],
            parallelData.handles,
            evt.detail.image
          );
        }
      });
      try {
        addToolState(element, referencedTool, parallelData);
        cornerstone.updateImage(element);

        const modifiedEventData = {
          toolName: referencedTool,
          toolType: referencedTool,
          element,
          measurementData: parallelData,
        };

        triggerEvent(
          element,
          csTools.EVENTS.MEASUREMENT_MODIFIED,
          modifiedEventData
        );
        triggerEvent(
          element,
          csTools.EVENTS.MEASUREMENT_COMPLETED,
          modifiedEventData
        );
      } catch (err) {
        console.error(err);
      }
    }
  }
}

function createNewLengthData(activeData, perpendicularDir) {
  const colorPalette = store.getState().colorPalette;
  const color = colorPalette.roiColor.next;
  store.dispatch(setNextPalette());
  let newData = {
    visible: true,
    active: false,
    color: color,
    invalidated: activeData.invalidated,
    handles: _.cloneDeep(activeData.handles),
    toolType: 'Length',
    hasParallels: true,
    length: activeData.length,
  };

  shiftPoints(newData.handles, perpendicularDir.x, perpendicularDir.y);
  return { ...newData };
}
