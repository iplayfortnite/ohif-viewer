import cloneDeep from 'lodash.clonedeep';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';

const recentActions = [];
let recentActionsTimer = 0;

/**
 * Save the action state to store.
 * @export @public @method
 * @name saveActionState
 *
 * @param {Object} actionState - action state information.
 * @param {string} actionGroup - action group.
 * @param {string} actionType - action type.
 * @param {boolean} isActionStart - is action state represents start of the action
 * @param {boolean} isActionEnd - is action state represents end of the action
 * @returns {void}
 */
export default function saveActionState(
  actionState,
  actionGroup,
  actionType,
  isActionStart,
  isActionEnd,
  isSubAction = false
) {
  recentActions.push([
    {
      toolType: actionState.toolType,
      type: actionGroup,
      data: cloneDeep(actionState),
      actionType: actionType,
      isSubAction,
    },
    isActionStart,
    isActionEnd,
  ]);
  // prevent more than maxActionCount to be updated in state on single operation
  // as each will impact performance due to UI updates on state change
  clearTimeout(recentActionsTimer);
  recentActionsTimer = setTimeout(() => {
    const mainActionsCount = recentActions.forEach(
      action => !action[0]?.isSubAction
    );
    let maxActionCount = store.getState().undoRedoActionState.maxActionCount;
    maxActionCount =
      mainActionsCount > maxActionCount ? maxActionCount : recentActions.length;
    recentActions.slice(-maxActionCount).forEach(action => {
      store.dispatch(FlywheelCommonRedux.actions.saveActionState(...action));
    });
    recentActions.length = 0;
  });
}
