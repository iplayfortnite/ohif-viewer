import csTools from 'cornerstone-tools';
import commonToolUtils from './commonUtils';
import { getSmartCTRangeList, getDefaultSmartCTRangeList } from '../utils';
import store from '@ohif/viewer/src/store';
import { shouldEnableAnnotationTool } from './utils/shouldEnableAnnotationTool';

const CornerstoneFreehandScissorsTool = csTools.FreehandScissorsTool;
const segmentationModule = csTools.getModule('segmentation');
const { saveActionState } = commonToolUtils;

/**
 * @public
 * @class FreehandScissorsTool
 * @memberof Tools
 * @classdesc Tool for manipulating labelmap data by drawing a freehand polygon.
 * @extends Tools.Base.BaseTool
 */
export default class FreehandScissorsTool extends CornerstoneFreehandScissorsTool {
  /** @inheritdoc */
  static toolName = 'FreehandScissorsTool';

  constructor(props = {}) {
    super(props);
    this.name = 'FreehandScissors';
  }

  getWindowRange() {
    const state = store.getState();
    const smartCT = state.smartCT;
    const selectedRange = smartCT?.selectedRange;
    if (selectedRange) {
      return selectedRange;
    }
    const defaultRange = getDefaultSmartCTRangeList();
    return defaultRange;
  }

  mouseClickOrDownCallback = evt => {
    const eventData = evt.detail;
    const { element } = eventData;
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);
    if (!isAnnotationToolEnabled) {
      return;
    }
    this._startOutliningRegion(evt);
  };

  postMouseDownCallback = evt => {
    this.mouseClickOrDownCallback(evt);
  };

  mouseClickCallback = evt => {
    this.mouseClickOrDownCallback(evt);
  };

  mouseDragCallback = evt => {
    const eventData = evt.detail;
    const { element } = eventData;
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);
    if (!isAnnotationToolEnabled) {
      return;
    }
    this._setHandlesAndUpdate(evt);
  };

  preMouseDownCallback = evt => {
    const eventData = evt.detail;
    const activeStrategy = store.getState().scissorsActionState
      ?.selectedStrategy;
    if (activeStrategy) {
      this.setActiveStrategy(activeStrategy);
    }
    const allRanges = getSmartCTRangeList();
    const smartCTRange = this.getWindowRange();
    const segmentationIndex = allRanges.findIndex(
      r => r.label === smartCTRange.label
    );
    if (segmentationIndex !== -1) {
      segmentationModule.setters.activeSegmentIndex(
        eventData.element,
        segmentationIndex + 1
      );
    }
  };

  preMouseUpCallback = evt => {
    const { element } = evt.detail;
    const toolState = csTools.getToolState(element, 'stack');
    const toolData = toolState.data[0];
    const index = segmentationModule.getters.activeLabelmapIndex(element);
    const activeViewport = store.getState().viewports.viewportSpecificData[0];
    const studyUid = activeViewport.StudyInstanceUID;
    const seriesUid = activeViewport.SeriesInstanceUID;
    const data = {
      toolType: this.name,
      StudyInstanceUID: studyUid,
      SeriesInstanceUID: seriesUid,
      toolData: toolData,
      element: element,
      labelmapIndex: index,
    };
    saveActionState(data, 'segments', 'Add', false, false);
  };
}
