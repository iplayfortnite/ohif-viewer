import { Observable } from 'rxjs';
import { distinctUntilChanged, map, shareReplay } from 'rxjs/operators';

let storeObservable$;

const createObservable = store => {
  storeObservable$ = new Observable(subscriber => {
    subscriber.next(store.getState());
    return store.subscribe(() => subscriber.next(store.getState()));
  }).pipe(shareReplay(1));

  return storeObservable$;
};

/** Reactive object structure */

// define package object
const reactive = { createObservable };
Object.defineProperty(reactive, 'observables', {
  get: () => {
    return {
      storeObservable$,
    };
  },
  configurable: false,
  enumerable: false,
});

export default reactive;
