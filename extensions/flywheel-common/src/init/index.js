import initStore from './initStore';
import initCornerstoneTools from './initCornerstoneTools';
import initReactive from './initReactive';

export default function initExtension({ appConfig }) {
  const { store } = appConfig;
  if (!store) {
    return;
  }
  initStore(store);
  initCornerstoneTools(store);
  initReactive(store);
}
