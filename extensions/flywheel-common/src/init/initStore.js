import { combineReducers } from 'redux';
import { redux as OhifRedux } from '@ohif/core';
import Redux from '../redux';

export default function initStore(store) {
  // TODO OHIF to provide a API to consume reducers from extensions
  // add infusions reducer
  OhifRedux.reducers.infusions = Redux.reducers.common;
  OhifRedux.reducers.multiSessionData = Redux.reducers.multiSessionData;
  OhifRedux.reducers.segmentation = Redux.reducers.segmentation;
  OhifRedux.reducers.colorPalette = Redux.reducers.colorPalette;
  OhifRedux.reducers.contourProperties = Redux.reducers.contourProperties;
  OhifRedux.reducers.viewportSyncCommands = Redux.reducers.viewportSyncCommands;
  OhifRedux.reducers.undoRedoActionState = Redux.reducers.undoRedoActionState;
  OhifRedux.reducers.measurementColorIntensity =
    Redux.reducers.measurementColorIntensity;
  OhifRedux.reducers.timer = Redux.reducers.timer;
  OhifRedux.reducers.ohifConfigProperties = Redux.reducers.ohifConfigProperties;
  OhifRedux.reducers.measurementPanelProperties =
    Redux.reducers.measurementPanelProperties;
  OhifRedux.reducers.subForm = Redux.reducers.setSubFormRoiState;
  OhifRedux.reducers.smartCT = Redux.reducers.smartCT;
  OhifRedux.reducers.scissorsActionState = Redux.reducers.scissorsActionState;
  OhifRedux.reducers.studyFormBSliceValidatedInfo =
    Redux.reducers.studyFormBSliceValidatedInfo;
  OhifRedux.reducers.fovGridRestoreInfo = Redux.reducers.fovGridRestoreInfo;
  OhifRedux.reducers.rightHandPanel = Redux.reducers.rightHandPanel;
  OhifRedux.reducers.viewerAvatar = Redux.reducers.viewerAvatar;
  store.replaceReducer(combineReducers(OhifRedux.reducers));
}
