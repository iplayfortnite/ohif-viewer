export const API_ROOT = ''; // all requests will contain this prefix (in case we want to point a different server host address)
export const API_ROOT_V1 = '/api';
export const API_ROOT_V2 = '/io-proxy';
export const MONAI_LABEL_ROOT = '/monai-label';

// CONFIG
export const GET_CONFIG = `${API_ROOT_V1}/config`;

// USERS
export const GET_USERS = `${API_ROOT_V1}/users`;
export const GET_USER = `${API_ROOT_V1}/users/self`;

// PROJECTS
export const GET_PROJECTS = `${API_ROOT_V1}/projects`;
export const GET_PROJECT = `${API_ROOT_V1}/projects/:projectId`;
export const GET_PROJECT_FILE = `${API_ROOT_V1}/projects/:projectId/files/:fileId`;
export const GET_PROJECT_ASSOCIATIONS = `${API_ROOT_V2}/projects/:projectId/session_uids`;
export const GET_PROJECT_ACQUISITION_ASSOCIATION = `${API_ROOT_V2}/projects/:projectId/study_uids`;
export const GET_PROJECT_JOBS = `${API_ROOT_V1}/projects/:projectId/jobs`;

//READER_TASK
export const GET_READER_TASK = `${API_ROOT_V1}/readertasks/:readerTaskId`;
export const GET_VIEWER_CONFIG_JSON = `${API_ROOT_V1}/viewerconfigs/:viewerConfigId/config`;
export const GET_VIEWER_FORM_JSON = `${API_ROOT_V1}/forms/:formId`;
export const GET_ALL_VIEWER_FORM_JSON = `${API_ROOT_V1}/forms`;
export const GET_VIEWER_FORM_RESPONSE = `${API_ROOT_V1}/forms/:formId/responses`;
export const SET_VIEWER_FORM_RESPONSE = `${API_ROOT_V1}/formresponses`;
export const UPDATE_VIEWER_FORM_RESPONSE = `${API_ROOT_V1}/formresponses/:formResponseId`;
export const GET_VIEWER_TASK_CONTEXT = `${API_ROOT_V1}/readertasks/:readerTaskId/launch_viewer`;
export const GET_PROTOCOL_JSON = `${API_ROOT_V1}/read_task_protocols/:protocolId`;

// SUBJECTS
export const GET_SUBJECTS = `${API_ROOT_V1}/subjects`;
export const GET_SUBJECTS_ANALYSES = `${API_ROOT_V1}/subjects/:subjectId/analyses`;

// SESSIONS
export const GET_SESSIONS = `${API_ROOT_V1}/sessions`;
export const GET_SESSION = `${API_ROOT_V1}/sessions/:sessionId`;
export const GET_SESSION_ANALYSES = `${API_ROOT_V1}/sessions/:sessionId/analyses`;
export const SET_SESSION_INFO = `${API_ROOT_V1}/sessions/:sessionId/info`;
export const ADD_SESSION_ANALYSES = `${API_ROOT_V1}/sessions/:sessionId/analyses`;
export const ADD_ANALYSES_FILE = `${API_ROOT_V1}/analyses/:analysesId/files`;

// ACQUISITIONS
export const GET_ACQUISITIONS = `${API_ROOT_V1}/sessions/:sessionId/acquisitions`;
export const GET_ACQUISITION = `${API_ROOT_V1}/acquisitions/:acquisitionId`;

// CONTAINERS
export const GET_CONTAINER = `${API_ROOT_V1}/containers/:containerId`;
export const GET_CONTAINER_FILES = `${API_ROOT_V1}/containers/:containerId/files`;
export const GET_CONTAINER_FILE = `${API_ROOT_V1}/containers/:containerId/files/:filename`;
export const GET_CONTAINER_FILE_INFO = `${API_ROOT_V1}/containers/:containerId/files/:filename/info`;
export const SET_CONTAINER_FILE_INFO = `${API_ROOT_V1}/containers/:containerId/files/:filename/info`;
export const GET_ALL_ANALYSES = `${API_ROOT_V1}/containers/:containerId/analyses`;
export const REPLACE_CONTAINER_FILES = GET_CONTAINER_FILES;

// STUDY
export const GET_INSTANCE_TAGS = `${API_ROOT_V2}/wado/projects/:projectId/studies/:studyId/series/:seriesId/instances/:instanceId/tags`;

// FILES
export const GET_COLORMAPS = `:baseRoot/assets/customColormaps.json`;
export const GET_FILE = `${API_ROOT_V1}/files/:fileId`;

// ZIP FILES
export const GET_ZIP_INFO = `${API_ROOT_V1}/files/:fileId/zip_info`;
export const GET_ZIP_MEMBER = `${API_ROOT_V1}/files/:fileId/zip_member/:zipMember`;

// READER_TASKS_ANNOTATION_APIS
export const GET_ANNOTATIONS_BY_TASK = `${API_ROOT_V1}/readertasks/:taskId/annotations`;
export const GET_ANNOTATIONS_BY_FILE = `${API_ROOT_V1}/annotations?filter=file_ref.file_id=:fileId`;
export const CREATE_ANNOTATION = `${API_ROOT_V1}/annotations`;
export const MODIFY_DELETE_ANNOTATION = `${API_ROOT_V1}/annotations/:id`;
export const UPDATE_READER_TASK = `${API_ROOT_V1}/readertasks/:taskId/modify`;

// ROLES
export const GET_ROLES = `${API_ROOT_V1}/roles`;

// Url to open Zendesk ticket
export const TICKET_URL = `https://docs.flywheel.io/hc/en-us/requests/new`;

// Monai label
export const GET_MONAI_LABEL_INFO = `${MONAI_LABEL_ROOT}/info/`;
export const UPLOAD_MONAI_LABEL_SEGMENTATION = `${MONAI_LABEL_ROOT}/infer/:monaiLabelModel`;
