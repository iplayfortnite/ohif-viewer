import ApiClient from '../client';
import {
  GET_COLORMAPS,
  GET_CONTAINER_FILE,
  GET_ZIP_INFO,
  GET_ZIP_MEMBER,
  ADD_ANALYSES_FILE,
  REPLACE_CONTAINER_FILES,
} from '../urls';

export function getColorMapsJson(baseRoot) {
  const urlOptions = {
    url: GET_COLORMAPS,
    urlParams: {
      pathParams: {
        baseRoot,
      },
    },
  };

  const config = {
    parseToJson: true, // return response as json
    prefixRoot: false, // root must be passed by param
  };

  return ApiClient.get(urlOptions, config);
}

export function getFile(url, contentType) {
  const urlOptions = {
    url,
  };

  const config = {
    parseTo: contentType,
  };

  return ApiClient.get(urlOptions, config);
}

export function getFileData(containerId, filename) {
  filename = decodeURIComponent(filename);
  const urlOptions = {
    url: GET_CONTAINER_FILE,
    urlParams: {
      pathParams: {
        containerId,
        filename,
      },
    },
  };

  const config = {
    parseToJson: false,
    adaptToCase: true, // return response and adapted to case
  };

  return ApiClient.get(urlOptions, config).catch(error => {
    if (error.code === 404) {
      throw new Error(
        `File ${filename} was not found for container ${containerId}`
      );
    }
    throw new Error('Could not get file');
  });
}

export function getZipInfo(fileId) {
  const urlOptions = {
    url: GET_ZIP_INFO,
    urlParams: {
      pathParams: {
        fileId,
      },
    },
  };

  const config = {
    parseToJson: true,
    adaptToCase: true, // return response and adapted to case
  };

  return ApiClient.get(urlOptions, config).catch(error => {
    if (error.code === 404) {
      throw new Error(`Zip info was not found for file ${fileId}`);
    }
    throw new Error('Could not get zip info');
  });
}

export function getZipMember(fileId, zipMember) {
  const urlOptions = {
    url: GET_ZIP_MEMBER,
    urlParams: {
      pathParams: {
        fileId,
        zipMember,
      },
    },
  };

  const config = {
    parseToJson: false,
    adaptToCase: true, // return response and adapted to case
  };

  return ApiClient.get(urlOptions, config).catch(error => {
    if (error.code === 404) {
      throw new Error(`Zip member was not found for file ${fileId}`);
    }
    throw new Error('Could not get zip member');
  });
}

export function deleteFile(containerId, filename) {
  filename = decodeURIComponent(filename);
  const urlOptions = {
    url: GET_CONTAINER_FILE,
    urlParams: {
      pathParams: {
        containerId,
        filename,
      },
    },
  };

  const config = {
    parseToJson: false,
    adaptToCase: true, // return response and adapted to case
  };

  return ApiClient.delete(urlOptions, config).catch(error => {
    if (error.code === 404) {
      throw new Error(
        `File ${filename} was not found for container ${containerId}`
      );
    }
    throw new Error('Could not delete file');
  });
}

export function uploadAnalysesFile(analysesId, fileData) {
  const urlOptions = {
    url: ADD_ANALYSES_FILE,
    urlParams: {
      pathParams: {
        analysesId,
      },
    },
  };

  const config = {
    parseToJson: true,
    adaptToCase: true, // return response and adapted to case
  };

  return ApiClient.post(urlOptions, fileData, config).catch(error => {
    throw new Error(
      `Uploading NiFTI file to the analyses container ${analysesId} failed with error ${error.message ||
        error}`
    );
  });
}

export function replaceFileInContainer(containerId, fileData) {
  const urlOptions = {
    url: REPLACE_CONTAINER_FILES,
    urlParams: {
      pathParams: {
        containerId,
      },
    },
  };

  const config = {
    parseToJson: true,
    adaptToCase: true, // return response and adapted to case
  };

  return ApiClient.post(urlOptions, fileData, config).catch(error => {
    throw new Error(
      `Uploading file to the container ${containerId} failed with error ${error.message ||
        error}`
    );
  });
}
