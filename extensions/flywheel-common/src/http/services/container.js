import ApiClient from '../client';
import {
  GET_CONTAINER,
  GET_CONTAINER_FILE_INFO,
  SET_CONTAINER_FILE_INFO,
} from '../urls';

export function getContainer(containerId) {
  const urlOptions = {
    url: GET_CONTAINER,
    urlParams: {
      pathParams: {
        containerId,
      },
    },
  };

  const config = {
    parseToJson: true, // return response as json
  };

  return ApiClient.get(urlOptions, config).catch(() => {
    throw new Error('Could not get container');
  });
}

export function getFileMetadata(containerId, filename) {
  filename = decodeURIComponent(filename);
  const urlOptions = {
    url: GET_CONTAINER_FILE_INFO,
    urlParams: {
      pathParams: {
        containerId,
        filename,
      },
    },
  };

  const config = {
    parseToJson: false,
    adaptToCase: true, // return response and adapted to case
  };

  return ApiClient.get(urlOptions, config)
    .then(response => response.json())
    .catch(error => {
      if (error.code === 404) {
        throw new Error(
          `File ${filename} was not found for container ${containerId}`
        );
      }
      throw new Error('Could not get file');
    });
}

export function setFileInfo(containerId, filename, ohifInfo) {
  const urlOptions = {
    url: SET_CONTAINER_FILE_INFO,
    urlParams: {
      pathParams: {
        containerId,
        filename,
      },
    },
  };

  const data = {
    set: {
      ohifViewer: ohifInfo,
    },
  };

  return ApiClient.post(urlOptions, data);
}
