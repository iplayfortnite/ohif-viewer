import ApiClient from '../client';
import { GET_ACQUISITIONS } from '../urls';
import { getUser } from './user';

export function listAcquisitions(sessionId) {
  return getUser().then(user => {
    const urlOptions = {
      url: GET_ACQUISITIONS.replace(':sessionId', sessionId),
      urlParams: {
        queryParams: {
          sessionId,
        },
      },
    };

    const config = {
      parseToJson: true, // return response as json
    };

    if (user.roles.includes('site_admin')) {
      config.isAdmin = true;
    }

    return ApiClient.get(urlOptions, config);
  });
}
