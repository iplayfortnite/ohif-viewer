import ApiClient from '../client';
import {
  GET_ALL_VIEWER_FORM_JSON,
  GET_PROJECT,
  GET_PROJECTS,
  GET_PROJECT_ACQUISITION_ASSOCIATION,
  GET_PROJECT_ASSOCIATIONS,
  GET_PROJECT_FILE,
  GET_FILE,
  GET_PROJECT_JOBS,
  GET_READER_TASK,
  GET_VIEWER_CONFIG_JSON,
  GET_VIEWER_FORM_JSON,
  GET_VIEWER_FORM_RESPONSE,
  GET_VIEWER_TASK_CONTEXT,
  GET_PROTOCOL_JSON,
  SET_VIEWER_FORM_RESPONSE,
  UPDATE_VIEWER_FORM_RESPONSE,
} from '../urls';
import { getUser } from './user';

export function listProjects() {
  return getUser().then(user => {
    const urlOptions = {
      url: GET_PROJECTS,
    };

    const config = {
      parseToJson: true, // return response as json
      queryAll: true, // request all records
    };

    if (user.roles.includes('site_admin')) {
      config.isAdmin = true;
    }

    return ApiClient.get(urlOptions, config).catch(error => {
      if (error.code === 404) {
        throw new Error(`Projects not found for the user`);
      }
      throw new Error('Could not get projects');
    });
  });
}

export function getProject(projectId) {
  const urlOptions = {
    url: GET_PROJECT,
    urlParams: {
      pathParams: {
        projectId,
      },
    },
  };

  const config = {
    parseToJson: true, // return response as json
  };

  return ApiClient.get(urlOptions, config).catch(error => {
    if (error.code === 404) {
      throw new Error(`Project ${projectId} was not found`);
    }
    throw new Error('Could not get project');
  });
}

export function getFileJson(fileId) {
  const urlOptions = {
    url: GET_FILE,
    urlParams: {
      pathParams: {
        fileId,
      },
    },
  };

  const config = {
    parseToJson: true, // return response as json
  };

  return ApiClient.get(urlOptions, config).catch(() => {
    throw new Error(`Could not get file info with file id ${fileId}`);
  });
}

export function getProjectFileJson(projectId, fileId) {
  const urlOptions = {
    url: GET_PROJECT_FILE,
    urlParams: {
      pathParams: {
        projectId,
        fileId,
      },
    },
  };

  const config = {
    parseToJson: true, // return response as json
  };

  return ApiClient.get(urlOptions, config).catch(err => {
    if (err instanceof Error) {
      throw new Error(err.message + ' (OHIF config file)');
    } else if (err instanceof string) {
      throw new Error(err + ' (OHIF config file)');
    } else {
      throw new Error('Could not get project OHIF configuration file');
    }
  });
}

export function getProjectAssociations(projectId, queryParams = {}) {
  const urlOptions = {
    url: GET_PROJECT_ASSOCIATIONS,
    urlParams: {
      pathParams: {
        projectId,
      },
      queryParams,
    },
  };

  const config = {
    parseToJson: true, // return response as json
    queryAll: true, // request all records
    queryAllLimit: 5000, // cap large collection
  };

  return ApiClient.get(urlOptions, config);
}

export function getAcquisitionAssociation(projectId, acquisitionId) {
  const urlOptions = {
    url: GET_PROJECT_ACQUISITION_ASSOCIATION,
    urlParams: {
      pathParams: {
        projectId,
      },
      queryParams: {
        acquisitionId,
      },
    },
  };

  const config = {
    parseToJson: true, // return response as json
  };

  return ApiClient.get(urlOptions, config);
}

export function getReaderTask(taskId) {
  const urlOptions = {
    url: GET_READER_TASK,
    urlParams: {
      pathParams: {
        readerTaskId: taskId,
      },
    },
  };
  const config = {
    parseToJson: true, // return response as json
  };
  return ApiClient.get(urlOptions, config);
}

export function getReaderTaskProtocol(protocolId) {
  const urlOptions = {
    url: GET_PROTOCOL_JSON,
    urlParams: {
      pathParams: {
        protocolId,
      },
    },
  };
  const config = {
    parseToJson: true, // return response as json
  };
  return ApiClient.get(urlOptions, config);
}

export function getProjectJobs(projectId) {
  const urlOptions = {
    url: GET_PROJECT_JOBS.replace(':projectId', projectId),
    urlParams: {
      pathParams: {
        projectId,
      },
    },
  };

  const config = {
    parseToJson: true, // return response as json
  };

  return ApiClient.get(urlOptions, config).catch(error => {
    if (error.code === 404) {
      throw new Error(`Project jobs ${projectId} was not found`);
    }
    throw new Error('Could not get project jobs');
  });
}

export function getViewerTaskContext(taskId) {
  return getUser().then(user => {
    const urlOptions = {
      url: GET_VIEWER_TASK_CONTEXT,
      urlParams: {
        pathParams: {
          readerTaskId: taskId,
        },
      },
    };

    const config = {
      parseToJson: true, // return response as json
    };

    if (user.roles.includes('site_admin')) {
      config.isAdmin = true;
    }

    return ApiClient.get(urlOptions, config);
  });
}

export function getViewerConfig(viewerConfigId) {
  return getUser().then(user => {
    const urlOptions = {
      url: GET_VIEWER_CONFIG_JSON,
      urlParams: {
        pathParams: {
          viewerConfigId,
        },
      },
    };

    const config = {
      parseToJson: true, // return response as json
    };

    if (user.roles.includes('site_admin')) {
      config.isAdmin = true;
    }

    return ApiClient.get(urlOptions, config);
  });
}

export function getViewerForm(formId) {
  return getUser().then(user => {
    const urlOptions = {
      url: GET_VIEWER_FORM_JSON,
      urlParams: {
        pathParams: {
          formId,
        },
      },
    };

    const config = {
      parseToJson: true, // return response as json
    };

    if (user.roles.includes('site_admin')) {
      config.isAdmin = true;
    }

    return ApiClient.get(urlOptions, config);
  });
}

export function getAllViewerForm(formId) {
  return getUser().then(user => {
    const urlOptions = {
      url: GET_ALL_VIEWER_FORM_JSON,
      urlParams: {
        queryParams: {
          filter: `_id=${formId}`,
        },
      },
    };

    const config = {
      parseToJson: true, // return response as json
    };

    if (user.roles.includes('site_admin')) {
      config.isAdmin = true;
    }

    return ApiClient.get(urlOptions, config);
  });
}

export function getViewerFormResponse(formId, taskId) {
  return getUser().then(user => {
    const urlOptions = {
      url: GET_VIEWER_FORM_RESPONSE,
      urlParams: {
        pathParams: {
          formId,
        },
        queryParams: {
          filter: `task_id=${taskId}`,
        },
      },
    };

    const config = {
      parseToJson: true, // return response as json
    };

    if (user.roles.includes('site_admin')) {
      config.isAdmin = true;
    }

    return ApiClient.get(urlOptions, config);
  });
}

export function setViewerFormResponse(data) {
  return getUser().then(user => {
    const urlOptions = {
      url: SET_VIEWER_FORM_RESPONSE,
      urlParams: {
        pathParams: {},
      },
    };

    const config = {
      parseToJson: true, // return response as json
    };

    if (user.roles.includes('site_admin')) {
      config.isAdmin = true;
    }

    return ApiClient.post(urlOptions, data, config);
  });
}

export function updateViewerFormResponse(formResponseId, data) {
  return getUser().then(user => {
    const urlOptions = {
      url: UPDATE_VIEWER_FORM_RESPONSE,
      urlParams: {
        pathParams: {
          formResponseId,
        },
      },
    };

    const config = {
      parseToJson: true, // return response as json
    };

    if (user.roles.includes('site_admin')) {
      config.isAdmin = true;
    }

    return ApiClient.put(urlOptions, data, config);
  });
}
