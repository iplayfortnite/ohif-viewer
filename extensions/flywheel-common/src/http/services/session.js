import ApiClient from '../client';
import {
  GET_ACQUISITION,
  GET_SESSION,
  GET_SESSIONS,
  GET_SESSION_ANALYSES,
  SET_SESSION_INFO,
  ADD_SESSION_ANALYSES,
} from '../urls';
import { getUser } from './user';

import OHIF from '@ohif/core';

let acquisitionPromises = {};

export function listSessions(projectId, filters = []) {
  return getUser().then(user => {
    const projectFilters = [`project=${projectId}`, ...filters];

    const urlOptions = {
      url: GET_SESSIONS,
      urlParams: {
        queryParams: {
          filter: projectFilters.join(','),
        },
      },
    };

    const config = {
      parseToJson: true, // return response as json
    };

    if (user.roles.includes('site_admin')) {
      config.isAdmin = true;
    }

    return ApiClient.get(urlOptions, config);
  });
}

export function listSessionsBySubjectId(subjectId, filters = []) {
  return getUser().then(user => {
    const sessionFilters = [`subject=${subjectId}`, ...filters];

    const urlOptions = {
      url: GET_SESSIONS,
      urlParams: {
        queryParams: {
          filter: sessionFilters.join(','),
        },
      },
    };

    const config = {
      parseToJson: true, // return response as json
    };

    if (user.roles.includes('site_admin')) {
      config.isAdmin = true;
    }

    return ApiClient.get(urlOptions, config);
  });
}

export function listSessionAnalyses(sessionId) {
  return getUser().then(user => {
    const urlOptions = {
      url: GET_SESSION_ANALYSES.replace(':sessionId', sessionId),
      urlParams: {
        queryParams: {
          sessionId,
        },
      },
    };

    const config = {
      parseToJson: true, // return response as json
      queryAll: true, // request all records
    };

    if (user.roles.includes('site_admin')) {
      config.isAdmin = true;
    }

    return ApiClient.get(urlOptions, config);
  });
}

export function getSession(sessionId) {
  const urlOptions = {
    url: GET_SESSION,
    urlParams: {
      pathParams: {
        sessionId,
      },
    },
  };

  const config = {
    parseToJson: false,
    adaptToCase: true, // return response and adapted to case
  };

  return ApiClient.get(urlOptions, config)
    .then(response => response.json())
    .catch(error => {
      if (error.code === 404) {
        throw new Error(`Session ${sessionId} was not found`);
      }
      throw new Error('Could not get session');
    });
}

export function setSessionInfo(sessionId, ohifInfo) {
  const urlOptions = {
    url: SET_SESSION_INFO,
    urlParams: {
      pathParams: {
        sessionId,
      },
    },
  };

  const data = {
    set: {
      ohifViewer: ohifInfo,
    },
  };

  return ApiClient.post(urlOptions, data);
}

export function getAcquisition(acquisitionId) {
  const urlOptions = {
    url: GET_ACQUISITION,
    urlParams: {
      pathParams: {
        acquisitionId,
      },
    },
  };

  const config = {
    parseToJson: true,
    adaptToCase: true, // return response and adapted to case
  };

  let acquisitionPromise = acquisitionPromises[acquisitionId] || null;
  if (!acquisitionPromise) {
    acquisitionPromises[acquisitionId] = ApiClient.get(urlOptions, config);
    acquisitionPromise = acquisitionPromises[acquisitionId];
  }

  return acquisitionPromise;
}

export function saveStudySession(
  notes,
  readTime,
  readStatus,
  slice,
  taskDisplayProperties
) {
  // bypass OHIF MeasurementAPI service
  return OHIF.measurements.MeasurementApi.Instance.storeMeasurements(
    undefined,
    { notes, readTime, readStatus, slice, taskDisplayProperties }
  );
}

export function createAnalyses(sessionId, analysesInputInfo) {
  const urlOptions = {
    url: ADD_SESSION_ANALYSES,
    urlParams: {
      pathParams: {
        sessionId,
      },
    },
  };

  return ApiClient.post(urlOptions, analysesInputInfo)
    .then(response => response.json())
    .catch(error => {
      throw new Error(
        `Analyses creation failed for session ${sessionId} with error ${error.message ||
          error}`
      );
    });
}

export function clearAcquisitionPromises() {
  acquisitionPromises = {};
}
