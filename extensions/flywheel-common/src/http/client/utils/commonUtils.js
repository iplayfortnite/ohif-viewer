import { generatePath } from 'react-router';
import { snakeCase, camelCase } from 'change-case';

const METHODS = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
};

/**
 * Replace path params of given url
 * @param {string} url url string format
 * @param {object} pathParams
 *
 * @example 1
 * replacePathParams('given/:param1/path/:param2/url', {param1: 1, param2: 2}) => 'given/1/path/2/url'
 */
const replacePathParams = (url, pathParams) => {
  const _url = generatePath(url, pathParams);
  return _url;
};

/**
 * Encode param key to be api format
 * @param {string} key param key
 * @return {string} snack case of key string
 *
 * @example 1
 * encodeParamKey('testOther') => 'test_other'
 */
const encodeParamKey = key => {
  return snakeCase(key);
};

/**
 * Decode param key to be app format
 * @param {string} key param key
 * @return {string} camel case of key string
 *
 * @example 1
 * encodeParamKey('test_other') => 'testOther'
 */
const decodeParamKey = key => {
  return camelCase(key);
};

export { METHODS, decodeParamKey, encodeParamKey, replacePathParams };
