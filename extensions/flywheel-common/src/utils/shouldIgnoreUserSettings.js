import getRouteParams from './getRouteParams';

const regExPattern = /^true$/i;

function shouldIgnoreUserSettings(state) {
  const { ignoreUserSettings } = getRouteParams();
  const isTrueSet = regExPattern.test(ignoreUserSettings);
  if (isTrueSet) {
    return true;
  }
  return hasMultipleReaderTask(state);
}

function hasMultipleReaderTask(state) {
  if (state?.multipleReaderTask?.TaskIds?.length) {
    return true;
  }
  return false;
}

export default shouldIgnoreUserSettings;
