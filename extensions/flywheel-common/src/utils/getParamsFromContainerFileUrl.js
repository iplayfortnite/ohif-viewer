import pathToRegexp from 'path-to-regexp';

/**
 * It returns object containing adapted location keys/properties
 * @param {string} fileUrl container file url
 * @return {Object} it picks from url, the container id and file name
 */
function getParamsFromContainerFileUrl(fileUrl) {
  const match = (pathRegExp, url, options = { start: false }) => {
    const keys = [];
    const regExp = pathToRegexp(pathRegExp, keys, options);
    const result = url.match(regExp);
    return result || [];
  };
  const fileApi = '/containers/:containerId/files/:fileName';

  const [, containerId, name] = match(fileApi, fileUrl);

  const fileContainerId = containerId;
  const fileName = name;

  return {
    fileContainerId,
    fileName: fileName && decodeURIComponent(fileName),
  };
}

export default getParamsFromContainerFileUrl;
