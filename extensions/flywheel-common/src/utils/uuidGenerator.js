export default function getUuid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(
    toReplace
  ) {
    const randomNumber = (Math.random() * 16) | 0;
    const replacedValue =
      toReplace == 'x' ? randomNumber : (randomNumber & 0x3) | 0x8;

    return replacedValue.toString(16);
  });
}
