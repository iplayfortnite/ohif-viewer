import store from '@ohif/viewer/src/store';
import { isCurrentFile } from './isCurrentFile';
import { getSessionRead } from './bSliceColorUtils';

function getSelectedOptionForQuestion(
  question,
  sliceNumber,
  subFormAnnotationId
) {
  const state = store.getState();
  const projectConfig = state.flywheel.projectConfig;
  const notes = getSessionRead()?.notes || {};
  let answerValue = '';
  if (subFormAnnotationId) {
    const subFormInfo = getSubFormInfoFromMeasurement(
      subFormAnnotationId,
      projectConfig
    );
    answerValue =
      !isCurrentFile(state) && projectConfig?.bSlices
        ? notes.slices?.[sliceNumber]?.[subFormInfo.questionKey]?.[
            subFormInfo.subFormName
          ]?.find(item => item.annotationId === subFormAnnotationId)?.[
            question.key
          ]
        : notes?.[subFormInfo.questionKey]?.[subFormInfo.subFormName]?.find(
            item => item.annotationId === subFormAnnotationId
          )?.[question.key];
  } else {
    answerValue =
      !isCurrentFile(state) && projectConfig?.bSlices
        ? notes.slices?.[sliceNumber]?.[question.key]
        : notes[question.key];
  }
  const answer = answerValue?.value ? answerValue?.value : answerValue;
  const selectedOption = question?.values?.find(x => x.value === answer);
  return selectedOption;
}

function getSubFormInfoFromMeasurement(subFormAnnotationId, projectConfig) {
  const annotation = getAnnotationById(subFormAnnotationId);
  const questions = projectConfig?.studyForm?.components || [];
  if (questions.length) {
    const question = questions.find(x => x.key === annotation.questionKey);
    const questionOption = question.values.find(
      x => x.value === annotation.answer
    );
    return {
      subFormName: questionOption.subForm,
      questionKey: annotation.questionKey,
    };
  }
}

function getAnnotationById(annotationId) {
  const state = store.getState();
  const measurements = state.timepointManager.measurements;
  let annotation = null;
  Object.keys(measurements).some(toolType => {
    annotation = measurements[toolType].find(
      measurement => measurement.uuid === annotationId
    );
    return annotation;
  });
  return annotation;
}

export default getSelectedOptionForQuestion;
