import store from '@ohif/viewer/src/store';

/**
 * Return boundary error message
 * @param {Array} boundaryMeasurements
 * @param {Object} measurementData
 * @returns
 */
const getBoundaryErrorMessage = (boundaryMeasurements, measurementData) => {
  let boundaryError = '';
  if (boundaryMeasurements) {
    const state = store.getState();
    const projectConfig = state.flywheel.projectConfig;
    const labels = projectConfig ? projectConfig.labels || [] : [];
    const labelObj = labels.find(x => x.value === measurementData.location);
    const boundaryMeasurementLabels = _.uniq(
      boundaryMeasurements.map(measurement => measurement.location)
    );

    boundaryMeasurementLabels.forEach(measurement => {
      const boundaryLocation = Object.keys(labels).find(
        key => labels[key].value === measurement
      );
      const boundaryLocationLabel = labels[boundaryLocation]?.label || '';
      const boundaryLocationValue = labels[boundaryLocation]?.value || '';
      if (labelObj?.boundary) {
        const location = labelObj.boundary.filter(
          label =>
            label.label === boundaryLocationLabel ||
            label.label === boundaryLocationValue
        );
        if (location?.length) {
          const direction = location[0].direction;
          boundaryError +=
            '\n' +
            measurementData.location +
            ' should be ' +
            direction +
            ' to ' +
            measurement;
        }
      }
    });
  }
  return boundaryError;
};

export default getBoundaryErrorMessage;
