import store from '@ohif/viewer/src/store';
import { getSmartCTRanges } from '../redux/constants/SmartCTRanges';
import selectors from '../redux/selectors';

const { selectAvailableSmartCTRanges } = selectors;

export const getSmartCTRangeList = () => {
  const smartCTRangesFromConfig = selectAvailableSmartCTRanges(
    store.getState()
  );
  const smartCTRanges = smartCTRangesFromConfig || getSmartCTRanges();
  return smartCTRanges;
};

export const getDefaultSmartCTRangeList = () => {
  const smartCTRangesFromConfig = selectAvailableSmartCTRanges(
    store.getState()
  );
  const defaultRange = smartCTRangesFromConfig?.[0] || getSmartCTRanges()[0];
  return defaultRange;
};
