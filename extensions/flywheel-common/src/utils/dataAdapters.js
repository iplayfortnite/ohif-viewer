import OHIF from '@ohif/core';
import _ from 'lodash';

const toAppCaseChangeMap = {
  studyinstanceuid: 'StudyInstanceUID',
  seriesinstanceuid: 'SeriesInstanceUID',
  sopinstanceuid: 'SOPInstanceUID',
  studydate: 'StudyDate',
  timedate: 'timeDate',
  patientname: 'PatientName',
  patientid: 'PatientID',
};
/**
 * It iterates over given object and adapt keys to app case.
 * It uses caseChangeMap to adapt each key (if existing on map)
 *
 * @param {Object} object
 * @param {Object} [caseChangeMap = toAppCaseChangeMap] Map with case changing keys
 * @return {Object} object with keys name adapted
 */
const adaptToCase = (object, caseChangeMap = toAppCaseChangeMap) => {
  const changeCase = key => {
    const _key = (key || '').toLowerCase();
    const caseKey = caseChangeMap[_key];

    return caseKey || key;
  };
  const deepMapKeys = (obj, mapKeys) =>
    mapKeys(
      _.mapValues(obj, value => {
        if (_.isPlainObject(value)) {
          return deepMapKeys(value, mapKeys);
        } else if (_.isArray(value)) {
          return _.map(value, arrayValue => deepMapKeys(arrayValue, mapKeys));
        } else {
          return value;
        }
      })
    );

  try {
    if (_.isEmpty(_.get(object, 'info.ohifViewer.measurements'))) {
      return object;
    }
    const adapted = _.cloneDeep(object);
    adapted.info.ohifViewer.measurements = deepMapKeys(
      adapted.info.ohifViewer.measurements,
      innerObject => _.mapKeys(innerObject, (val, key) => changeCase(key))
    );
    return adapted;
  } catch (e) {
    OHIF.log.warn('Failed to adapt data keys');
    return object;
  }
};

export { adaptToCase };
