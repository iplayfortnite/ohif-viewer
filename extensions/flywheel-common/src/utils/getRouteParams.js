import queryString from 'query-string';
import pathToRegexp from 'path-to-regexp';

/**
 * It returns object containing adapted location keys/proterties
 * @param {Object} location history location object
 * @return {Object} it picks from location some usefull properties
 */
function getRouteParams(location = window.location) {
  const query = queryString.parse(location.search);
  let pathname = location.pathname;
  const match = (pathRegExp, url, options = { start: false }) => {
    const keys = [];
    const regExp = pathToRegexp(pathRegExp, keys, options);
    const result = url.match(regExp);
    return result || [];
  };

  const defaultProjectPath = '/project/:projectId/(.*)?';
  const defaultViewerPath =
    '/project/:projectId/viewer/:StudyInstanceUID/:SeriesInstanceUID?';
  const niftiViewerPath =
    '/project/:projectId/nifti/:niftiContainerId/:niftiFilename';
  const metaIOViewerPath =
    '/project/:projectId/metaio/:metaIOContainerId/:metaIOFilename';
  const imageViewerPath =
    '/project/:projectId/image/:imageContainerId/:imageFilename';
  const tiffViewerPath =
    '/project/:projectId/tiff/:tiffContainerId/:tiffFilename';
  const imageViewerPathFromSession = '/project/:projectId/image/:sessionId';

  const [, projectId] = match(defaultProjectPath, pathname);
  const [, studyProjectId, StudyInstanceUID, SeriesInstanceUID] = match(
    defaultViewerPath,
    pathname
  );
  const [, niftiProjectId, niftiContainerId, niftiFilename] = match(
    niftiViewerPath,
    pathname
  );
  const [, metaIOProjectId, metaIOContainerId, metaIOFilename] = match(
    metaIOViewerPath,
    pathname
  );
  const [, imageProjectId, imageContainerId, imageFilename] = match(
    imageViewerPath,
    pathname
  );
  const [, tiffProjectId, tiffContainerId, tiffFilename] = match(
    tiffViewerPath,
    pathname
  );
  const [, imageProjectIdFromSession, imageSessionId] = match(
    imageViewerPathFromSession,
    pathname
  );
  const fileContainerId =
    niftiContainerId ||
    metaIOContainerId ||
    imageContainerId ||
    tiffContainerId;
  const filename =
    niftiFilename || metaIOFilename || imageFilename || tiffFilename;
  const fileType = niftiFilename
    ? 'nifti'
    : metaIOFilename
    ? 'metaimage'
    : imageFilename || imageSessionId
    ? 'image'
    : tiffFilename
    ? 'tiff'
    : undefined;

  return {
    ...query,
    projectId:
      projectId ||
      studyProjectId ||
      niftiProjectId ||
      metaIOProjectId ||
      imageProjectId ||
      tiffProjectId ||
      imageProjectIdFromSession,
    StudyInstanceUID: StudyInstanceUID && decodeURIComponent(StudyInstanceUID),
    SeriesInstanceUID:
      SeriesInstanceUID && decodeURIComponent(SeriesInstanceUID),
    fileContainerId,
    filename: filename && decodeURIComponent(filename),
    fileType: fileType,
    imageSessionId,
  };
}

export default getRouteParams;
