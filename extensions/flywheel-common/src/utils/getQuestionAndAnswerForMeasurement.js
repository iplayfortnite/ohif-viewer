import store from '@ohif/viewer/src/store';
import { getSessionRead } from './bSliceColorUtils';
import getActiveSliceInfo from './getActiveSliceInfo';
import { isCurrentFile } from './isCurrentFile';

function getQuestionAndAnswerForMeasurement(measurement) {
  const state = store.getState();
  const projectConfig = state.flywheel.projectConfig;
  if (!projectConfig?.studyForm?.components) {
    return { question: null, selectedOption: null };
  }

  const question = _getQuestion(measurement, projectConfig.studyForm);
  const selectedOption = _getSelectedOption(measurement, question);

  return { question, selectedOption };
}

function _getQuestion(measurement, studyForm) {
  const { components, subForms } = studyForm;
  let question = {};
  if (measurement.isSubForm) {
    const qn = components.find(q => q.key === measurement.question);
    const qnOption = qn.values.find(
      option => option.subForm === measurement.subFormName
    );
    const subForm = subForms[qnOption.subForm].components.find(
      x => x.key === measurement.questionKey
    );
    question = subForm;
  } else {
    if (measurement.questionKey) {
      question = components.find(
        question => question.key === measurement.questionKey
      );
    } else {
      question = components.find(question => {
        return question?.values?.some(v => {
          if (v.instructionSet?.length) {
            return v.instructionSet?.some(i =>
              i.requireMeasurements?.includes(measurement.location)
            );
          } else {
            return v.requireMeasurements?.includes(measurement.location);
          }
        });
      });
    }
  }
  return question;
}

function _getSelectedOption(measurement, question) {
  const state = store.getState();
  const projectConfig = state.flywheel.projectConfig;
  const notes = getSessionRead()?.notes || {};
  const slice = measurement?.sliceNumber || getActiveSliceInfo()?.sliceNumber;
  let answerValue = '';
  if (measurement.isSubForm) {
    answerValue =
      !isCurrentFile(state) && projectConfig?.bSlices
        ? notes.slices?.[slice]?.[measurement.question]?.[
            measurement.subFormName
          ]?.find(
            item => item.annotationId === measurement.subFormAnnotationId
          )?.[question.key]
        : notes?.[measurement.question]?.[measurement.subFormName]?.find(
            item => item.annotationId === measurement.subFormAnnotationId
          )?.[question.key];
  } else {
    answerValue =
      !isCurrentFile(state) && projectConfig?.bSlices
        ? notes.slices?.[slice]?.[question.key]
        : notes[question.key];
  }
  const answer = answerValue?.value ? answerValue?.value : answerValue;
  const selectedOption = question?.values?.find(x => x.value === answer);
  return selectedOption;
}

export default getQuestionAndAnswerForMeasurement;
