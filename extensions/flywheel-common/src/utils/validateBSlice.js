import { isCurrentFile } from '.';
import store from '@ohif/viewer/src/store';

const validateBSlice = (projectConfig, slice, notes) => {
  const components = projectConfig.studyForm?.components;
  const state = store.getState();
  const measurements = state.timepointManager.measurements;
  const measurementLabels = {};
  Object.keys(measurements).forEach(toolName => {
    measurements[toolName]?.forEach(item => {
      if (item.sliceNumber > 0 && item.location) {
        if (!measurementLabels[item.sliceNumber]) {
          measurementLabels[item.sliceNumber] = [];
        }
        measurementLabels[item.sliceNumber].push(item.location);
      }
    });
  });
  let isValidionSuccessForCurrentSlice = false;
  if (!isCurrentFile(state) && projectConfig?.bSlices) {
    const currentSliceNotes = notes?.slices?.[slice];
    projectConfig?.bSlices?.required?.all?.forEach(item => {
      const question = components.find(data => data.key === item);
      if (!currentSliceNotes?.[item]) {
        isValidionSuccessForCurrentSlice = true;
      }
      const value = question?.values?.find(
        value => value.value === currentSliceNotes?.[item]
      );
      value?.requireMeasurements?.forEach(label => {
        const limitValue = projectConfig?.labels?.find(
          item => item.value === label
        ).limit;
        const labels = measurementLabels?.[slice]?.filter(
          item => item === label
        )?.length;
        if ((labels && labels > limitValue) || !labels) {
          isValidionSuccessForCurrentSlice = true;
        }
      });
    });
    projectConfig?.bSlices?.required?.specific?.[slice]?.forEach(item => {
      const question = components.find(data => data.key === item);
      if (!currentSliceNotes?.[item]) {
        isValidionSuccessForCurrentSlice = true;
      }
      const value = question?.values?.find(
        value => value.value === currentSliceNotes?.[item]
      );
      value?.requireMeasurements?.forEach(label => {
        const limitValue = projectConfig?.labels?.find(
          item => item.value === label
        ).limit;
        const labels = measurementLabels?.[slice]?.filter(
          item => item === label
        )?.length;
        if ((labels && labels > limitValue) || !labels) {
          isValidionSuccessForCurrentSlice = true;
        }
      });
    });
    projectConfig?.bSlices?.settings?.[slice]?.hiddenQuestions?.forEach(
      item => {
        if (
          currentSliceNotes?.[item] &&
          !projectConfig?.bSlices?.required?.all?.includes(item) &&
          !projectConfig?.bSlices?.required?.specific?.[slice]?.includes(item)
        ) {
          isValidionSuccessForCurrentSlice = true;
        }
      }
    );
  } else {
    isValidionSuccessForCurrentSlice = true;
  }
  return isValidionSuccessForCurrentSlice;
};

const validateAllBSlice = (projectConfig, notes) => {
  const bSlices = Object.keys(projectConfig?.bSlices?.settings || {});
  const studyFormCompletedForBSlice = {};
  bSlices.forEach(slice => {
    const isBSliceCompleted = validateBSlice(projectConfig, slice, notes);
    studyFormCompletedForBSlice[slice] = !isBSliceCompleted;
  });
  return studyFormCompletedForBSlice;
};

export { validateBSlice, validateAllBSlice };
