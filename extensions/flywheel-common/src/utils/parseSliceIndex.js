import getRouteParams from './getRouteParams';

/**
 * returns frame number to launch if given in URL
 * @param {Array<string>} seriesMatchTexts An array of strings that can match to series name/UID in URL parameter.
 * @param {Number} defaultIndex
 * @returns frame number to launch for the series.
 */
const parseSliceIndex = (seriesMatchTexts, defaultIndex = 0, instances) => {
  const { slice } = getRouteParams(location);
  if (slice === undefined) {
    return defaultIndex;
  }
  let sliceIndex = defaultIndex;
  if (slice.includes('|')) {
    let seriesArray = slice.split(',');
    if (seriesArray.length) {
      for (const seriesSlice of seriesArray) {
        // Each item will be in the format 'SeriesMatchText|FrameIndex'.
        let seriesBasedSlice = seriesSlice.split('|');
        if (
          seriesMatchTexts.findIndex(
            text => text.toLowerCase() === seriesBasedSlice[0].toLowerCase()
          ) !== -1
        ) {
          sliceIndex = parseInt(seriesBasedSlice[1]);
          break;
        }
      }
    }
  } else {
    sliceIndex = parseInt(slice);
  }
  // slice is less than one then it will load first slice.
  if (sliceIndex < 1) {
    sliceIndex = 1;
  }
  // slice is greater than available slice index then it will load the last slice.
  if (sliceIndex > instances.length) {
    sliceIndex = instances.length;
  }
  return isNaN(sliceIndex) ? defaultIndex : sliceIndex - 1;
};

export default parseSliceIndex;
