import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import getToolsAndLabelsForQuestionAnswer from './getToolsAndLabelsForQuestionAnswer';
import { isCurrentFile } from './isCurrentFile.js';
import getRouteParams from './getRouteParams.js';
import store from '@ohif/viewer/src/store';
const {
  allowedActiveTools,
  selectActiveTool,
  measurementTools,
  segmentationTools,
} = FlywheelCommonRedux.selectors;

const { setAvailableLabels, setActiveTool } = FlywheelCommonRedux.actions;

function activateToolsAndLabelsForAnswer(
  question,
  selectedOption,
  sliceNumber
) {
  const dispatch = store.dispatch;
  if (!question) {
    dispatch(
      setAvailableLabels(
        [''],
        [...measurementTools, ...segmentationTools, ...allowedActiveTools]
      )
    );
    return;
  }
  const { questionTools, questionLabels } = getToolsAndLabelsForQuestionAnswer(
    question,
    selectedOption,
    sliceNumber
  );

  let tools = [...questionTools];
  tools.push(...allowedActiveTools);
  if (!questionLabels.length) {
    questionLabels.push('');
  }
  const state = store.getState();
  const isFile = isCurrentFile(state);
  const activeTool = selectActiveTool(state);
  const nextActiveTool = tools.includes(activeTool) ? activeTool : tools[0];

  const toolList = [...new Set(tools)];
  const isSetAvailableLabelsFire =
    toolList.length === 1 && toolList.includes('DragProbe');
  const { taskId } = getRouteParams();
  const isNiftiOrMHDLaunchedFromAcquisition = isFile && !taskId;

  if (!isSetAvailableLabelsFire && !isNiftiOrMHDLaunchedFromAcquisition) {
    if (
      state.flywheel?.projectConfig?.studyFormWorkflow === 'Mixed' ||
      state.flywheel?.projectConfig?.studyFormWorkflow === 'ROI'
    ) {
      const allTools = state.infusions?.workFlowTools;
      dispatch(setAvailableLabels(questionLabels, allTools));
    } else {
      dispatch(setAvailableLabels(questionLabels, [...new Set(tools)]));
    }
  }

  dispatch(setActiveTool(nextActiveTool));
}

export default activateToolsAndLabelsForAnswer;
