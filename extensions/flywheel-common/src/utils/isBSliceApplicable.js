import isSingleFileMode from './isSingleFileMode';
import getRouteParams from './getRouteParams';
import { utils } from '@ohif/core';
import { isCurrentFile } from './isCurrentFile';

const { studyMetadataManager } = utils;
/**
 * It checks if bSlice applicable or not
 * @param {Object} state app redux store
 * @return {boolean} true in case if b slice applicable
 */
const isBSliceApplicable = state => {
  const location = window.location;
  const { StudyInstanceUID } = getRouteParams(location);
  const study = studyMetadataManager.get(StudyInstanceUID);
  const displaySets = [];
  const selectDisplaySetValue = study?.displaySets?.filter(
    displaySet =>
      displaySet.Modality !== 'RTSTRUCT' && displaySet.Modality !== 'SEG'
  );
  if (selectDisplaySetValue) {
    displaySets.push(selectDisplaySetValue);
  }
  if (
    (displaySets?.length > 1 && !isSingleFileMode(state, location)) ||
    isCurrentFile(state)
  ) {
    return false;
  }
  return true;
};

export default isBSliceApplicable;
