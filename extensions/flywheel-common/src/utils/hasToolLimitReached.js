import store from '@ohif/viewer/src/store';
import {
  allowedActiveTools,
  selectActiveTool,
  measurementTools as AnnotationTools,
  segmentationTools,
} from '../redux/selectors/tools';
import getActiveSliceInfo from './getActiveSliceInfo';
import isBSliceApplicable from './isBSliceApplicable';

function hasToolLimitReached(
  tool,
  question,
  answerKey,
  sliceNumber,
  subFormAnnotationId
) {
  if (!question?.values?.length) {
    return false;
  }
  if (!AnnotationTools.includes(tool)) {
    return false;
  }
  const answer = answerKey?.value || answerKey;
  const selectedOption = question.values.find(x => x.value === answer);
  const bSliceSettings = getBSliceSettings();
  const measurementTools = getMeasurementToolsForAnswer(
    selectedOption,
    tool,
    question,
    bSliceSettings,
    sliceNumber
  );
  if (!measurementTools?.length) {
    return false;
  }
  if (!measurementTools.some(x => x === tool)) {
    return true;
  }
  const bSliceTools = getBSliceTools(question, bSliceSettings, sliceNumber);
  const toolLabels = getLabelsForTool(tool, selectedOption, bSliceTools);

  return hasLabelsLimitReached(
    toolLabels,
    question,
    selectedOption,
    sliceNumber,
    bSliceSettings,
    subFormAnnotationId
  );
}

function hasLabelsLimitReached(
  labels,
  question,
  selectedOption,
  sliceNumber,
  bSliceSettings,
  subFormAnnotationId
) {
  if (!question) {
    return false;
  }
  return !labels.some(label => {
    // Check if any label has not reached limit
    const limit = getLabelLimits(label, selectedOption);
    if (limit === Infinity) {
      return true;
    }

    const count = getCountForLabel(
      label,
      question,
      sliceNumber,
      bSliceSettings,
      subFormAnnotationId
    );
    return limit > count;
  });
}

function getMeasurementToolsForAnswer(
  selectedOption,
  activeTool,
  question,
  bSliceSettings,
  sliceNumber
) {
  if (!question) {
    return [...AnnotationTools, ...segmentationTools, ...allowedActiveTools];
  }
  let measurementTools = selectedOption?.measurementTools || [];
  const bSliceTools = getBSliceTools(question, bSliceSettings, sliceNumber);
  if (bSliceTools) {
    return bSliceTools;
  }

  if (selectedOption?.instructionSet) {
    const instruction = selectedOption.instructionSet.find(x =>
      x.measurementTools?.includes(activeTool)
    );
    if (instruction) {
      measurementTools = instruction.measurementTools;
    } else {
      const instructionTools = selectedOption.instructionSet.reduce(
        (tools, instruction) => {
          if (instruction.measurementTools?.length) {
            instruction.measurementTools.forEach(x => tools.add(x));
          }
          return tools;
        },
        new Set()
      );
      if (instructionTools.size) {
        measurementTools = Array.from(instructionTools);
      }
    }
  }

  return measurementTools;
}

function getBSliceTools(question, bSliceSettings, sliceNumber) {
  if (
    question &&
    bSliceSettings?.[sliceNumber]?.measurementTools?.[question.key]?.length
  ) {
    return bSliceSettings[sliceNumber].measurementTools[question.key];
  }
  return null;
}

function getCountForLabel(
  label,
  question,
  sliceNumber,
  bSliceSettings,
  subFormAnnotationId
) {
  const defaultFilterCriteria = annotation =>
    annotation.location === label &&
    (!question || question.key === annotation.questionKey);
  const bSliceFilterCriteria = annotation =>
    annotation.sliceNumber === Number(sliceNumber) &&
    defaultFilterCriteria(annotation);
  const subFormFilterCriteria = annotation =>
    annotation.subFormAnnotationId === subFormAnnotationId &&
    defaultFilterCriteria(annotation);
  let filterCriteria = defaultFilterCriteria;
  if (bSliceSettings?.[sliceNumber]) {
    filterCriteria = bSliceFilterCriteria;
    if (subFormAnnotationId) {
      filterCriteria = annotation =>
        bSliceFilterCriteria(annotation) && subFormFilterCriteria(annotation);
    }
  } else if (subFormAnnotationId) {
    filterCriteria = subFormFilterCriteria;
  }
  const state = store.getState();
  const { measurements } = state.timepointManager;

  return Object.keys(measurements).reduce((count, toolType) => {
    return count + measurements[toolType].filter(filterCriteria).length;
  }, 0);
}

function getLabelLimits(label, selectedOption) {
  const state = store.getState();
  const projectConfig = state.flywheel.projectConfig;
  if (!projectConfig?.studyForm) {
    return Infinity;
  }
  const instructionLimit = _getLimitFromInstruction(label, selectedOption);
  if (instructionLimit !== void 0) {
    return instructionLimit;
  }
  const labels = projectConfig?.labels || [];
  const labelConfig = labels.find(x => x.value === label || x.label === label);
  return labelConfig?.limit || Infinity;
}

function _getLimitFromInstruction(label, selectedOption) {
  let instructions = selectedOption?.instructionSet || [selectedOption];
  let instructionLimit = {};
  instructions.forEach(instruction => {
    if (!instruction?.requireMeasurements?.includes(label)) {
      return;
    }

    if (instruction.exact) {
      instructionLimit.exact = instructionLimit.exact
        ? Math.max(instructionLimit.exact, instruction.exact)
        : instruction.exact;
      return;
    }

    if (instruction.max) {
      instructionLimit.max = instructionLimit.max
        ? Math.max(instructionLimit.max, instruction.max)
        : instruction.max;
      return;
    }
  });

  const limit = instructionLimit.exact || instructionLimit.max;

  return limit;
}

function getLabelsForTool(tool, selectedOption, bSliceTools) {
  const labels = [];

  if (selectedOption?.instructionSet?.length) {
    selectedOption.instructionSet.forEach(x => {
      if (
        (bSliceTools || x.measurementTools)?.includes(tool) &&
        x.requireMeasurements?.length
      ) {
        labels.push(...x.requireMeasurements);
      }
    });
    if (
      selectedOption.instructionSet.some(x => x.requireMeasurements?.length)
    ) {
      return labels;
    }
  }
  const { measurementTools = [], requireMeasurements = [] } =
    selectedOption || {};
  if (
    (bSliceTools || measurementTools)?.includes(tool) &&
    requireMeasurements?.length
  ) {
    labels.push(...requireMeasurements);
  }
  return labels;
}

function hasActiveToolLimitReached(activeTool) {
  const state = store.getState();
  activeTool = activeTool || selectActiveTool(state);
  const currentQuestion = state.infusions.currentSelectedQuestion;
  const { subFormAnnotationId, answer } = currentQuestion || {};
  const sliceInfo = getActiveSliceInfo(state.viewports.activeViewportIndex);
  return hasToolLimitReached(
    activeTool,
    currentQuestion,
    answer,
    sliceInfo?.sliceNumber,
    subFormAnnotationId
  );
}

function getBSliceSettings() {
  const state = store.getState();
  if (isBSliceApplicable(state)) {
    const projectConfig = state.flywheel.projectConfig;
    const settings = projectConfig?.bSlices?.settings;
    return settings && Object.keys(settings).length ? settings : null;
  }
  return null;
}

export {
  hasToolLimitReached,
  hasActiveToolLimitReached,
  hasLabelsLimitReached,
  getMeasurementToolsForAnswer,
  getBSliceSettings,
  getLabelsForTool,
  getBSliceTools,
  getLabelLimits,
  getCountForLabel,
};
