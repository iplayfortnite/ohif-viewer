import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import dcmjs from 'dcmjs';
import store from '@ohif/viewer/src/store';
import VolumetricImageSegmentation from './VolumetricImageSegmentation';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import {
  getSmartCTRangeList,
  isCurrentFile,
  isSliceOrderSame,
  cornerstoneUtils,
} from '../../utils';
import { setSegmentToolActive } from '@flywheel/extension-cornerstone-infusions/src/utils/segmentationUtils';

const { setters, getters } = cornerstoneTools.getModule('segmentation');
const { setSegmentationData } = FlywheelCommonRedux.actions;
const { setSmartCTRange } = FlywheelCommonRedux.actions;

// TODO segmentation docs and evaluate to move it from here
function _makeColorLUTAndGetIndex(segMetadata) {
  const { state } = cornerstoneTools.getModule('segmentation');
  const { colorLutTables } = state;
  const colorLUTIndex = _getNextColorLUTIndex();

  const { data } = segMetadata;

  if (
    !data.some(
      segment =>
        segment &&
        (segment.ROIDisplayColor || segment.RecommendedDisplayCIELabValue)
    )
  ) {
    // Use default cornerstoneTools colorLUT.
    return 0;
  }

  const colorLUT = [];

  for (let i = 0; i < data.length; i++) {
    const segment = data[i];
    if (!segment) {
      continue;
    }

    const { ROIDisplayColor, RecommendedDisplayCIELabValue } = segment;

    if (RecommendedDisplayCIELabValue) {
      const rgb = dcmjs.data.Colors.dicomlab2RGB(
        RecommendedDisplayCIELabValue
      ).map(x => Math.round(x * 255));

      colorLUT[i] = [...rgb, 255];
    } else if (ROIDisplayColor) {
      colorLUT[i] = [...ROIDisplayColor, 255];
    } else {
      colorLUT[i] = [...colorLutTables[0][i]];
    }
  }

  colorLUT.shift();
  setters.colorLUT(colorLUTIndex, colorLUT);

  return colorLUTIndex;
}
// TODO segmentation docs and evaluate to move it from here
function _getNextLabelmapIndex(firstImageId) {
  const { state } = cornerstoneTools.getModule('segmentation');
  const brushStackState = state.series[firstImageId];

  let labelmapIndex = 1;

  if (brushStackState) {
    const { labelmaps3D } = brushStackState;
    labelmapIndex = labelmaps3D.length || 1;

    for (let i = 1; i < labelmaps3D.length; i++) {
      if (!labelmaps3D[i]) {
        labelmapIndex = i;
        break;
      }
    }
  }

  return labelmapIndex;
}
// TODO segmentation docs and evaluate to move it from here
function _getNextColorLUTIndex() {
  const { state } = cornerstoneTools.getModule('segmentation');
  const { colorLutTables } = state;

  let colorLUTIndex = colorLutTables.length;

  for (let i = 0; i < colorLutTables.length; i++) {
    if (!colorLutTables[i]) {
      colorLUTIndex = i;
      break;
    }
  }

  return colorLUTIndex;
}
// TODO segmentation docs and evaluate to move it from here
function _getImageIdsForDisplaySet(
  studies,
  StudyInstanceUID,
  SeriesInstanceUID,
  displaySetsSource = 'displaySets'
) {
  const study = studies.find(
    study => study.StudyInstanceUID === StudyInstanceUID
  );

  const displaySets = study[displaySetsSource].filter(displaySet => {
    return displaySet.SeriesInstanceUID === SeriesInstanceUID;
  });

  if (displaySets.length > 1) {
    console.warn(
      'More than one display set with the same SeriesInstanceUID. This is not supported yet...'
    );
    // TODO -> We could make check the instance list and see if any match?
    // Do we split the segmentation into two cornerstoneTools segmentations if there are images in both series?
    // ^ Will that even happen?
  }

  const referencedDisplaySet = displaySets[0];

  if (
    referencedDisplaySet.isMultiFrame &&
    referencedDisplaySet.numImageFrames > 1
  ) {
    const imageIds = [];
    for (let i = 0; i < referencedDisplaySet.numImageFrames; i++) {
      imageIds.push(referencedDisplaySet.images[0].getImageId(i));
    }
    return imageIds;
  }

  return referencedDisplaySet.images.map(image => image.getImageId());
}

/**
 * Get the first image id
 * @param {Object} referencedDisplaySet - source display set
 * @param {Array} [studies] - study list
 */
function getFirstImageId(referencedDisplaySet, studies) {
  const { StudyInstanceUID } = referencedDisplaySet;

  const imageIds = _getImageIdsForDisplaySet(
    studies,
    StudyInstanceUID,
    referencedDisplaySet.SeriesInstanceUID
  );
  return imageIds[0];
}

/**
 * Reposition the segments in the label map collections
 * @param {Object} segDisplaySet - segment display set
 * @param {Object} referencedDisplaySet - source display set
 * @param {Array} [studies] - study list
 * @param {number} [labelmapIndex] - label map index
 */
function rePositionSegment(
  segDisplaySet,
  referencedDisplaySet,
  studies,
  labelmapIndex
) {
  const firstImageId = getFirstImageId(referencedDisplaySet, studies);

  const { state } = cornerstoneTools.getModule('segmentation');
  const brushStackState = state.series[firstImageId];
  if (!brushStackState || labelmapIndex === undefined) {
    return;
  }
  const [reOrderedItems] = brushStackState.labelmaps3D.splice(
    segDisplaySet.labelmapIndex,
    1
  );
  brushStackState.labelmaps3D.splice(labelmapIndex, 0, reOrderedItems);
}

/**
 * Remove the segment data inside the label map on reposition
 * @param {Object} segDisplaySet - segment display set
 * @param {Object} referencedDisplaySet - source display set
 * @param {Array} [studies] - study list
 * @param {number} [curSegIndex] - current position of the segment
 */
function removeSegment(
  segDisplaySet,
  referencedDisplaySet,
  studies,
  curSegIndex
) {
  const firstImageId = getFirstImageId(referencedDisplaySet, studies);
  const { state } = cornerstoneTools.getModule('segmentation');
  const brushStackState = state.series[firstImageId];
  const labelmapIndex = segDisplaySet.labelmapIndex;
  const segmentationIndex = segDisplaySet.segmentationIndex;
  if (!brushStackState || labelmapIndex === undefined) {
    return;
  }
  const labelmap3D = brushStackState.labelmaps3D[labelmapIndex];

  if (!labelmap3D) {
    return;
  } // Delete metadata if present.

  delete labelmap3D.metadata[segmentationIndex];
  const labelmaps2D = labelmap3D.labelmaps2D; // Clear segment's voxels.

  for (let i = 0; i < labelmaps2D.length; i++) {
    const labelmap2D = labelmaps2D[i]; // If the labelmap2D has data, and it contains the segment, delete it.

    if (
      labelmap2D &&
      labelmap2D.segmentsOnLabelmap.includes(segmentationIndex)
    ) {
      const pixelData = labelmap2D.pixelData; // Remove this segment from the list.
      const indexOfSegment = labelmap2D.segmentsOnLabelmap.indexOf(
        segmentationIndex
      );
      labelmap2D.segmentsOnLabelmap.splice(indexOfSegment, 1); // Delete the label for this segment.
      for (let p = 0; p < pixelData.length; p++) {
        if (pixelData[p] === segmentationIndex) {
          pixelData[p] = 0;
        }
      }
    }
  }
  //var indexOfSegment = labelmap3D.segmentsHidden.indexOf(segmentationIndex);
  labelmap3D.segmentsHidden.splice(segmentationIndex, 1); // Delete the label for this segment.
  brushStackState.labelmaps3D.splice(labelmapIndex, 1);
  brushStackState.activeLabelmapIndex = 0;
}

// TODO segmentation docs and evaluate to move it from here
async function loadSegmentation(
  segDisplaySet,
  referencedDisplaySet,
  studies,
  segmentationLoaderFn,
  segmentationIndex = 1
) {
  const { StudyInstanceUID } = referencedDisplaySet;

  const imageIds = _getImageIdsForDisplaySet(
    studies,
    StudyInstanceUID,
    referencedDisplaySet.SeriesInstanceUID
  );

  const referencedImageIds = _getImageIdsForDisplaySet(
    studies,
    StudyInstanceUID,
    segDisplaySet.SeriesInstanceUID,
    'derivedDisplaySets'
  );

  const {
    imageOrientationPatient: refImageOrientation,
  } = cornerstone.metaData.get('imagePlaneModule', imageIds[0]);

  // Set here is loading is asynchronous.
  // If this function throws its set back to false.
  segDisplaySet.isLoaded = true;
  const dataSegments = await Promise.all(
    referencedImageIds.map(imageId =>
      segmentationLoaderFn(imageId, refImageOrientation)
    )
  ).catch(error => {
    segDisplaySet.isLoaded = false;
    segDisplaySet.loadError = true;
    segDisplaySet.segmentationIndex = segmentationIndex; // For removal from store
    return Promise.reject({
      segInfo: {
        ...segDisplaySet,
        ...{
          referencedDisplaySetUID: referencedDisplaySet.StudyInstanceUID,
        },
      },
      message: 'Failed to load the segmentation file ' + segDisplaySet.imageId,
    });
  });
  if (!dataSegments || !dataSegments[0]) {
    segDisplaySet.isLoaded = false;
    segDisplaySet.loadError = true;
    segDisplaySet.segmentationIndex = segmentationIndex; // For removal from store
    return Promise.reject({
      segInfo: {
        ...segDisplaySet,
        ...{
          referencedDisplaySetUID: referencedDisplaySet.StudyInstanceUID,
        },
      },
      message: 'Failed to load the segmentation for ' + segDisplaySet.imageId,
    });
  }

  const projectConfig = store.getState().flywheel?.projectConfig;

  // If the slice ordering in base dicom is different from that of nifti overlay,
  // update referenceSOPInstanceUID in overlay to point to correct base image.
  if (
    referencedDisplaySet.dataProtocol !== 'nifti' &&
    projectConfig?.useRadiologyOrientationForNiftiOverDicomOverlay &&
    dataSegments.length > 1 &&
    !isSliceOrderSame(imageIds, [
      dataSegments[0].imageId,
      dataSegments[1].imageId,
    ])
  ) {
    const isMultiFrame =
      referencedDisplaySet.isMultiFrame &&
      referencedDisplaySet.numImageFrames > 1;
    if (isMultiFrame) {
      dataSegments.reverse();
    } else {
      const refImages = studies
        .find(
          study =>
            study.StudyInstanceUID === referencedDisplaySet.StudyInstanceUID
        )
        .series.find(
          series =>
            series.SeriesInstanceUID === referencedDisplaySet.SeriesInstanceUID
        ).instances;
      const length = refImages.length;

      if (refImages.length !== dataSegments.length) {
        console.warn(
          'No of slices is different for base data and segmentation data.'
        );
      }

      dataSegments.forEach((segment, index) => {
        const imageId = segment.imageId;
        const instanceData = cornerstone.metaData.get('instance', imageId);
        instanceData.ReferencedSOPInstanceUID =
          refImages[length - 1 - index]?.metadata?.SOPInstanceUID;
      });
    }
  }

  const segmentation = new VolumetricImageSegmentation(
    imageIds,
    dataSegments,
    cornerstone.metaData,
    segmentationIndex,
    referencedDisplaySet.isMultiFrame && referencedDisplaySet.numImageFrames > 1
  );
  return new Promise((resolve, reject) => {
    let results;

    try {
      results = segmentation.generateToolState(
        dataSegments,
        cornerstone.metaData
      );
    } catch (error) {
      segDisplaySet.isLoaded = false;
      segDisplaySet.loadError = true;
      segDisplaySet.segmentationIndex = segmentationIndex; // For removal from store
      reject({
        ...error,
        ...{
          segInfo: {
            ...segDisplaySet,
            ...{
              referencedDisplaySetUID: referencedDisplaySet.StudyInstanceUID,
            },
          },
        },
      });
    }
    setDisplaySetInstanceUID(segDisplaySet, segmentationIndex);
    const {
      labelmapBuffer,
      labelmapBufferArray,
      segMetadata,
      segmentsOnFrame,
    } = results;

    // TODO: Could define a color LUT based on colors in the SEG.
    const labelmapIndex = _getNextLabelmapIndex(imageIds[0]);
    const colorLUTIndex = 0;

    // TODO segmentation check weather using labelmapBufferArray or labelmapBuffer
    let currentLabelmapIndex = 0;

    setters.labelmap3DByFirstImageId(
      imageIds[0],
      labelmapBufferArray[0] || labelmapBuffer,
      labelmapIndex,
      segMetadata,
      imageIds.length,
      segmentsOnFrame,
      colorLUTIndex
    );
    const { state } = cornerstoneTools.getModule('segmentation');
    const brushStackState = state.series[imageIds[0]];
    brushStackState.labelmaps3D[
      labelmapIndex
    ].activeSegmentIndex = segmentationIndex;
    segDisplaySet.labelmapIndex = labelmapIndex;
    segDisplaySet.segmentationIndex = segmentationIndex;
    if (labelmapIndex > 1) {
      // Push the new segment to front to manage the rendering order.
      rePositionSegment(segDisplaySet, referencedDisplaySet, studies, 1);
      segDisplaySet.labelmapIndex = 1;

      if (!!brushStackState || labelmapIndex !== undefined) {
        brushStackState.activeLabelmapIndex = labelmapIndex;
      }
    }
    const storeState = store.getState();
    const config = storeState?.flywheel?.projectConfig;
    let loadMaskAsEditable = !!config?.loadMaskAsEditable;
    if (loadMaskAsEditable && !isCurrentFile(storeState)) {
      // In order to enable edit mode for the newly loaded segmentation mask
      currentLabelmapIndex = 1;
      setSegmentToolActive();
    }

    cornerstone.getEnabledElements().forEach(enabledElement => {
      setters.activeLabelmapIndex(enabledElement.element, currentLabelmapIndex);
    });

    setActiveLabelMapForNewlyAddedSegment(segDisplaySet);

    if (loadMaskAsEditable && !isCurrentFile(storeState)) {
      updateSegmentationData(false, loadMaskAsEditable, segDisplaySet);
      updateCTRange(segDisplaySet.segmentationIndex);
    }

    console.log('Segmentation loaded.');
    const event = new CustomEvent('extensiondicomsegmentationsegloaded');
    document.dispatchEvent(event);

    resolve(segDisplaySet.labelmapIndex);
  });
}

function setActiveLabelMapForNewlyAddedSegment(segDisplaySet) {
  const state = store.getState();
  const segmentationData = getCurrentSegmentationDataIndex(segDisplaySet);
  const allRanges = getSmartCTRangeList();
  const selectedRange = state?.smartCT?.selectedRange;
  const segmentIndex = allRanges.findIndex(
    r => r.label === selectedRange?.label
  );
  if (segmentationData) {
    const element = cornerstoneUtils.getEnabledElement(
      state.viewports.activeViewportIndex
    );
    if (
      segmentationData.modality === 'SEG' &&
      segmentationData.containerId === '' &&
      segmentationData?.displaySetInstanceUIDs.includes(
        segDisplaySet.displaySetInstanceUID
      )
    ) {
      setters.activeLabelmapIndex(element, segDisplaySet.labelmapIndex);
      setters.activeSegmentIndex(element, segDisplaySet.segmentationIndex);
      segDisplaySet.segmentationIndex = segmentIndex + 1;
      updateSegmentationData(true, true, segDisplaySet);
      setSegmentToolActive();
    }
  }
}

function getCurrentSegmentationDataIndex(segDisplaySet) {
  const activeReferenceUID = getActiveReferenceUID();
  const segmentationData = store.getState().segmentation.segmentationData[
    activeReferenceUID
  ];
  const index = segmentationData.findIndex(data =>
    data.displaySetInstanceUIDs.includes(segDisplaySet.displaySetInstanceUID)
  );
  return segmentationData[index];
}

function updateSegmentationData(
  isUpdatingNewSegment,
  loadMaskAsEditable,
  segDisplaySet
) {
  const activeReferenceUID = getActiveReferenceUID();
  const segmentationData = store.getState().segmentation.segmentationData[
    activeReferenceUID
  ];
  const index = segmentationData.findIndex(data =>
    data.displaySetInstanceUIDs.includes(segDisplaySet.displaySetInstanceUID)
  );
  if (index > -1) {
    const newSegmentData = segmentationData[index];
    let editedSegmentData = { ...newSegmentData };
    editedSegmentData.isEditing = loadMaskAsEditable;
    if (isUpdatingNewSegment) {
      const fileName = cornerstoneUtils.getFileName(segDisplaySet.imageId);
      editedSegmentData.name = fileName;
      editedSegmentData.studyInstanceUID = fileName;
    }
    let newMaskArray = [...segmentationData];
    newMaskArray[index] = editedSegmentData;

    if (newSegmentData?.modality !== 'RTSTRUCT') {
      store.dispatch(
        setSegmentationData([...newMaskArray], activeReferenceUID)
      );
    }
  }
}

function updateCTRange(activeSegmentIndex) {
  const smartCTRangeList = getSmartCTRangeList();
  const selectedSegmentationColor = smartCTRangeList?.[activeSegmentIndex - 1];

  if (selectedSegmentationColor) {
    store.dispatch(setSmartCTRange(selectedSegmentationColor));
  }
}

function setDisplaySetInstanceUID(segDisplaySet, segmentationIndex) {
  const activeReferenceUID = getActiveReferenceUID();
  const segmentationData = store.getState().segmentation.segmentationData[
    activeReferenceUID
  ];
  const index = segmentationData.findIndex(
    data => data.originalId === segmentationIndex
  );
  if (index > -1) {
    const newSegmentData = segmentationData[index];
    const editedSegmentData = { ...newSegmentData };
    editedSegmentData.displaySetInstanceUIDs =
      editedSegmentData?.displaySetInstanceUIDs || [];
    editedSegmentData.displaySetInstanceUIDs.push(
      segDisplaySet.displaySetInstanceUID
    );
    const newMaskArray = [...segmentationData];
    newMaskArray[index] = editedSegmentData;

    if (newSegmentData?.modality !== 'RTSTRUCT') {
      store.dispatch(
        setSegmentationData([...newMaskArray], activeReferenceUID)
      );
    }
  }
}

function getActiveReferenceUID() {
  const state = store.getState();
  let referenceUID = null;
  const currentVolumetricImage =
    state.flywheel?.currentNiftis[0] || state.flywheel?.currentMetaImage;
  const { viewportSpecificData, activeViewportIndex } = state.viewports;
  if (viewportSpecificData[activeViewportIndex]) {
    referenceUID = currentVolumetricImage
      ? viewportSpecificData[activeViewportIndex].StudyInstanceUID
      : viewportSpecificData[activeViewportIndex].SeriesInstanceUID;
  }
  return referenceUID;
}

/**
 * Update the segmentation visible status by checking the existing status
 * @param {number} [labelmapIndex] - label map index
 * @param {number} [segmentationIndex] - segmentation index
 * @param {boolean} [isVisible] - segmentation to be visible or not
 */
async function setSegmentVisibility(
  labelmapIndex,
  segmentationIndex,
  isVisible
) {
  cornerstone.getEnabledElements().forEach(enabledElement => {
    if (
      isVisible !==
      getters.isSegmentVisible(
        enabledElement.element,
        segmentationIndex,
        labelmapIndex
      )
    ) {
      setters.toggleSegmentVisibility(
        enabledElement.element,
        segmentationIndex,
        labelmapIndex
      );
    }
  });
}

export {
  rePositionSegment,
  removeSegment,
  setSegmentVisibility,
  loadSegmentation,
};
