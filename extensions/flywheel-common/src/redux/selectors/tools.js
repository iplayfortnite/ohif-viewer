import { createSelector } from 'reselect';
import { selectAvailableLabels } from './labels';
import { selectViewports } from './viewports';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import { enableToolsForBSlice } from '../../tools/utils/enableToolsForBSlice';
import store from '@ohif/viewer/src/store';

export const selectActiveTool = state => {
  return state.infusions.activeTool;
};

export const selectAvailableTools = state => {
  if (state.infusions.availableTools) {
    return [...state.infusions.availableTools, ...segmentationTools];
  }
  return state.infusions.availableTools;
};

export const staticTools = [
  'Series',
  'Layout',
  'Stack Scroll',
  'Levels',
  'Pan',
  'Orient',
  'Probe',
  'Color Map',
];

export const measurementTools = [
  'Length',
  'Parallel',
  'ArrowAnnotate',
  'Angle',
  'DragProbe',
  'RectangleRoi',
  'EllipticalRoi',
  'CircleRoi',
  'FreehandRoi',
  'OpenFreehandRoi',
  'Bidirectional',
  'ContourRoi',
];

export const segmentationTools = [
  'Brush',
  'SmartBrush',
  'AutoSmartBrush',
  'CircleScissors',
  'RectangleScissors',
  'FreehandScissors',
  'ThresholdTool',
];

export const performOnSwitchTools = [
  { toolName: 'FreehandRoiSculptor', value: 'disabled' },
];

export const allowedActiveTools = ['DragProbe'];

const readOnlyValidationTools = [
  'FreehandRoiSculptor',
  'Eraser',
  'Clear',
  ...segmentationTools,
].concat(measurementTools.filter(tool => !allowedActiveTools.includes(tool)));

export const scaleDisplayValidationTools = [
  'Zoom',
  'ManualRotate',
  'Rotate',
  'RotateRight',
  'FlipV',
  'FlipH',
];

export const selectAllowedActiveTools = createSelector(
  selectAvailableLabels,
  selectAvailableTools,
  selectViewports,
  state => state.flywheel?.projectConfig,
  (state, activeViewportIndex) => activeViewportIndex,
  (
    availableLabels,
    availableTools,
    viewports,
    originalProjectConfig,
    activeViewportIndex
  ) => {
    const state = store.getState();
    const isScaleDisplayed = state.infusions.isScaleDisplayed;
    const projectConfig = FlywheelCommonUtils.getCondensedProjectConfig(
      originalProjectConfig
    );
    const bSlicesConfig = projectConfig?.bSlices;
    const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
      activeViewportIndex
    );
    let enableToolsForSlice = true;
    if (
      bSlicesConfig &&
      element &&
      FlywheelCommonUtils.isBSliceApplicable(state)
    ) {
      enableToolsForSlice = enableToolsForBSlice(bSlicesConfig, element);
    }
    const isReadOnly = viewports?.[activeViewportIndex]?.readOnly;
    const enabled =
      !(availableLabels && availableLabels.length === 0) && enableToolsForSlice;
    const activeModeTools =
      isReadOnly || !enableToolsForSlice
        ? readOnlyValidationTools
        : [...measurementTools];

    // TODO: Special handling for removing the Contour ROI tool for now when some
    // specific configs are set
    let disableSpecificTool = {};
    if (
      projectConfig?.contourUnique ||
      (bSlicesConfig?.settings && Object.keys(bSlicesConfig?.settings)) ||
      (projectConfig?.labels || []).find(label => label.limit >= 1)
    ) {
      disableSpecificTool['ContourRoi'] = true;
    }

    let allowedTools = Object.assign(
      {},
      ...activeModeTools.map(name => ({
        [name]:
          !isReadOnly &&
          enabled &&
          (!availableTools || availableTools.includes(name)) &&
          !disableSpecificTool[name],
      }))
    );
    if (isScaleDisplayed) {
      const tools = Object.assign(
        {},
        ...scaleDisplayValidationTools.map(name => ({
          [name]: false,
        }))
      );
      allowedTools = { ...allowedTools, ...tools };
    }
    return allowedTools;
  }
);

export const selectPerformOnSwitchTool = createSelector(
  selectActiveTool,
  activeTool => {
    const switchTool = performOnSwitchTools.find(
      tool => tool.toolName === activeTool
    );
    return switchTool;
  }
);
