export function selectAvailableLabels(state) {
  return state.infusions.availableLabels;
}

export function selectedLabels(state) {
  return state.infusions.selectedLabels;
}

export function selectedQuestion(state) {
  return state.infusions.currentSelectedQuestion;
}
