export const selectViewports = (state = {}) => state.viewports.layout.viewports;
export const selectViewportSpecificData = (state = {}) =>
  state.viewports.viewportSpecificData;
export const selectActiveViewportIndex = (state = {}) =>
  state.viewports.activeViewportIndex;
