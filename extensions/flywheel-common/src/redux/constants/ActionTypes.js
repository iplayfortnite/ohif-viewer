export const SET_ACTIVE_TOOL = 'ohif/infusions/SET_ACTIVE_TOOL';
export const SET_AVAILABLE_LABELS = 'ohif/infusions/SET_AVAILABLE_LABELS';
export const SET_AVAILABLE_TOOLS = 'ohif/infusions/SET_AVAILABLE_TOOLS';
export const REMOVE_AVAILABLE_TOOLS = 'ohif/infusions/REMOVE_AVAILABLE_TOOLS';
export const SET_LOADER_DISPLAY_STATUS =
  'ohif/infusions/SET_LOADER_DISPLAY_STATUS';
export const SET_PROTOCOL_LAYOUT = 'ohif/infusions/SET_PROTOCOL_LAYOUT';
export const SET_SCALE_INDICATOR_STATUS =
  'ohif/infusions/SET_SCALE_INDICATOR_STATUS';

export const SET_ACTIVE_PROJECT = 'ohif/flywheel/SET_ACTIVE_PROJECT';

export const ADD_MULTI_SESSION_DATA = 'ohif/navigate/ADD_MULTI_SESSION_DATA';
export const CLEAR_MULTI_SESSION_DATA =
  'ohif/navigate/CLEAR_MULTI_SESSION_DATA';

export const UPDATE_ROI_COLORS = 'ohif/flywheel/UPDATE_ROI_COLORS';
export const SET_NEXT_PALETTE_COLOR = 'ohif/flywheel/SET_NEXT_PALETTE_COLOR';
export const UPDATE_PALETTE_COLORS = 'ohif/flywheel/UPDATE_PALETTE_COLORS';
export const SET_PALETTE_COLORS_FROM_CONFIG =
  'ohif/flywheel/SET_PALETTE_COLORS_FROM_CONFIG';

export const SAVE_ACTION_STATE = 'ohif/flywheel/SAVE_ACTION_STATE';
export const UNDO_LAST_ACTION_STATE = 'ohif/flywheel/UNDO_LAST_ACTION_STATE';
export const REDO_NEXT_ACTION_STATE = 'ohif/flywheel/REDO_NEXT_ACTION_STATE';
export const CLEAR_ALL_ACTION_STATES = 'ohif/flywheel/CLEAR_ALL_ACTION_STATES';

export const SET_ACTIVE_VTK_TOOL = 'ohif/infusions/SET_ACTIVE_VTK_TOOL';
export const SET_MEASUREMENT_COLOR_INTENSITY =
  'ohif/flywheel/SET_MEASUREMENT_COLOR_INTENSITY';
export const SET_HOVERED_MEASUREMENTS =
  'ohif/flywheel/SET_HOVERED_MEASUREMENTS';
export const SET_PREVIOUS_SESSION_READ_TIME =
  'ohif/flywheel/SET_PREVIOUS_SESSION_READ_TIME';
export const SET_STUDY_FORM_AVAILABILITY =
  'ohif/flywheel/SET_STUDY_FORM_AVAILABILITY';

export const SET_ACTIVE_QUESTION = 'ohif/flywheel/SET_ACTIVE_QUESTION';
export const SET_RESIZED_FORM_HEIGHT = 'ohif/flywheel/SET_RESIZED_FORM_HEIGHT';
export const SET_RESIZED_SEGMENTATION_PANEL_HEIGHT =
  'ohif/flywheel/SET_RESIZED_SEGMENTATION_PANEL_HEIGHT';
export const SET_EDIT_SUBFORM_ROI_LAYOUT =
  'ohif/subForm/SET_EDIT_SUBFORM_ROI_LAYOUT';
export const SET_SUBFORM_POPUP_VISIBILITY =
  'ohif/subForm/SET_SUBFORM_POPUP_VISIBILITY';
export const SET_LABEL_FOR_QUESTION = 'ohif/subForm/SET_LABEL_FOR_QUESTION';
export const SET_SUBFORM_QUESTION_ANSWERED =
  'ohif/subForm/SET_SUBFORM_QUESTION_ANSWERED';
export const SET_STUDYFORM_ANSWER_IN_MIXED_MODE =
  'ohif/subForm/SET_STUDYFORM_ANSWER_IN_MIXED_MODE';
export const SET_SUBFORM_POPUP_CLOSE = 'ohif/subForm/SET_SUBFORM_POPUP_CLOSE';
export const SET_STUDYFORM_BSLICE_VALIDATED_INFO =
  'ohif/studyFormBSliceValidatedInfo/SET_STUDYFORM_BSLICE_VALIDATED_INFO';

export const SET_SELECTED_SCISSORS_STRATEGY =
  'ohif/flywheel/SET_SELECTED_SCISSORS_STRATEGY';
export const SET_FOV_GRID_RESTORE_INFO =
  'ohif/flywheel/SET_FOV_GRID_RESTORE_INFO';

export const EXPAND_RIGHT_HAND_PANEL = 'ohif/flywheel/EXPAND_RIGHT_HAND_PANEL';

export const RightHandPanelModes = {
  NORMAL: 'NORMAL',
  EXPAND: 'EXPAND',
  COLLAPSE: 'COLLAPSE',
};

export const SET_VIEWER_AVATAR = 'ohif/flywheel/SET_VIEWER_AVATAR';
export const SET_SINGLE_AVATAR = 'ohif/flywheel/SET_SINGLE_AVATAR';
export const SET_STUDY_STATE_NOTE = 'ohif/flywheel/SET_STUDY_STATE_NOTE';
export const SET_MULTIPLE_TASK_NOTE =
  'ohif/viewerAvatar/SET_MULTIPLE_TASK_NOTE';
export const SET_MULTIPLE_TASK_USER =
  'ohif/viewerAvatar/SET_MULTIPLE_TASK_USER';
export const SET_MULTIPLE_TASK_NOTES_USER_FORM_RESPONSE =
  'ohif/viewerAvatar/SET_MULTIPLE_TASK_NOTES_USER_FORM_RESPONSE';
export const SET_MULTIPLE_TASK_FORM_RESPONSE =
  'ohif/viewerAvatar/SET_MULTIPLE_TASK_FORM_RESPONSE';
export const SET_WORKFLOW_TOOLS = 'ohif/infusions/SET_WORKFLOW_TOOLS';
export const SET_PANEL_SWITCHED = 'ohif/infusions/SET_PANEL_SWITCHED';
export const SET_TOOL_WHILE_PANEL_SWITCH =
  'ohif/infusions/SET_TOOL_WHILE_PANEL_SWITCH';
export const SET_RELABEL = 'ohif/infusions/SET_RELABEL';
