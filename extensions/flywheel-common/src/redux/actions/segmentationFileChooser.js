export const clearSegmentationData = () => {
  return {
    type: 'CLEAR_SEGMENTATION_DATA',
  };
};

export const setSegmentationData = (data, referenceUID) => {
  return {
    type: 'SET_SEGMENTATION_DATA',
    data,
    referenceUID,
  };
};

export const addSegmentationData = data => {
  return {
    type: 'ADD_SEGMENTATION_DATA',
    data,
  };
};

export const removeSegmentationData = data => {
  return {
    type: 'REMOVE_SEGMENTATION_DATA',
    data,
  };
};

export const rearrangeSegmentationData = (
  data,
  currentReferenceUID,
  newReferenceUID
) => {
  return {
    type: 'REARRANGE_SEGMENTATION_DATA',
    data,
    currentReferenceUID,
    newReferenceUID,
  };
};

export const setThresholdToleranceValue = sliderRange => {
  return {
    type: 'SET_THRESHOLD_TOLERANCE',
    sliderRange,
  };
};
