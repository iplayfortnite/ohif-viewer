import {
  SET_RESIZED_FORM_HEIGHT,
  SET_RESIZED_SEGMENTATION_PANEL_HEIGHT,
} from '../constants/ActionTypes';

export const setResizedFormHeight = height => {
  return {
    type: SET_RESIZED_FORM_HEIGHT,
    height,
  };
};

export const setResizedSegmentationDivisionPanelHeight = height => {
  return {
    type: SET_RESIZED_SEGMENTATION_PANEL_HEIGHT,
    height,
  };
};
