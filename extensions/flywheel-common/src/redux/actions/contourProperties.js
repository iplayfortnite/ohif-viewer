export const setHighlightOverlappedStatus = status => {
  return {
    type: 'SET_HIGHLIGHT_OVERLAPPED_STATUS',
    status,
  };
};

export const setProximityCursorProperties = (
  display,
  distance = null,
  lineStyle = null
) => {
  return {
    type: 'SET_PROXIMITY_CURSOR_PROPERTIES',
    display,
    distance,
    lineStyle,
  };
};
