import { SET_PROTOCOL_LAYOUT } from '../constants/ActionTypes';

export function setProtocolLayout(layout) {
  return {
    type: SET_PROTOCOL_LAYOUT,
    layout,
  };
}
