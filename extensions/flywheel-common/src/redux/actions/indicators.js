import { SET_SCALE_INDICATOR_STATUS } from '../constants/ActionTypes';

export function setIndicatorStatus(indicator) {
  return {
    type: SET_SCALE_INDICATOR_STATUS,
    indicator,
  };
}
