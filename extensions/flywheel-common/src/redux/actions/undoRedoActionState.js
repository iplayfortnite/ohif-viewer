import {
  SAVE_ACTION_STATE,
  UNDO_LAST_ACTION_STATE,
  REDO_NEXT_ACTION_STATE,
  CLEAR_ALL_ACTION_STATES,
} from '../constants/ActionTypes';

export function saveActionState(actionState, start = false, end = false) {
  return {
    type: SAVE_ACTION_STATE,
    actionState,
    start,
    end,
  };
}

export function undoLastActionState() {
  return {
    type: UNDO_LAST_ACTION_STATE,
  };
}

export function redoNextActionState() {
  return {
    type: REDO_NEXT_ACTION_STATE,
  };
}

export function clearAllActionStates() {
  return {
    type: CLEAR_ALL_ACTION_STATES,
  };
}
