import * as cornerstoneUtils from '../../utils/cornerstoneUtils';
import {
  SET_ACTIVE_TOOL,
  SET_ACTIVE_VTK_TOOL,
  SET_AVAILABLE_TOOLS,
  REMOVE_AVAILABLE_TOOLS,
  SET_WORKFLOW_TOOLS,
  SET_PANEL_SWITCHED,
  SET_TOOL_WHILE_PANEL_SWITCH,
  SET_RELABEL,
} from '../constants/ActionTypes';

import selectors from '../selectors';

const {
  selectActiveTool,
  selectAllowedActiveTools,
  selectPerformOnSwitchTool,
  selectActiveViewportIndex,
} = selectors;

export function setActiveTool(toolName, viewportIndex = null) {
  return (dispatch, getState) => {
    const state = getState();
    const activeTool = selectActiveTool(state);
    if (toolName === activeTool) {
      return;
    }
    toolName = toolName || activeTool;

    const activeViewportIndex =
      viewportIndex !== null ? viewportIndex : selectActiveViewportIndex(state);

    const allowedTools = selectAllowedActiveTools(state, activeViewportIndex);
    let hasToolLimitReached = false;
    if (toolName === activeTool) {
      const { hasActiveToolLimitReached } = require('../../utils');
      hasToolLimitReached = hasActiveToolLimitReached(toolName);
    }
    if (allowedTools[toolName] === false || hasToolLimitReached) {
      if (toolName !== activeTool) {
        // hack to change tool cursor to new passive tool
        cornerstoneUtils.setToolMode(toolName, 'active', activeViewportIndex);
      }
      // allow modifications but no creations
      cornerstoneUtils.setToolMode(toolName, 'passive', activeViewportIndex);
    } else {
      cornerstoneUtils.setToolMode(toolName, 'active', activeViewportIndex);
    }
    const switchToolData = selectPerformOnSwitchTool(state);
    if (activeTool !== toolName) {
      // On switching from current tool, set tool switching mode from switch tool data if
      // its already configured for that tool
      if (switchToolData) {
        cornerstoneUtils.setToolMode(activeTool, switchToolData.value);
      } else if (!allowedTools[activeTool]) {
        // prevent modifications unless relevant tool is selected
        cornerstoneUtils.setToolMode(activeTool, 'enabled');
      }
    }
    return dispatch({
      type: SET_ACTIVE_TOOL,
      tool: toolName,
    });
  };
}

export function setActiveVtkTool(toolName) {
  return {
    type: SET_ACTIVE_VTK_TOOL,
    tool: toolName,
  };
}

export function setWorkFlowTools(tools) {
  return { type: SET_WORKFLOW_TOOLS, tools };
}

export function setPanelSwitched(data) {
  return { type: SET_PANEL_SWITCHED, data };
}

export function setToolWhilePanelSwitch(toolWhilePanelSwitch) {
  return { type: SET_TOOL_WHILE_PANEL_SWITCH, toolWhilePanelSwitch };
}

export function setRelabel(status) {
  return { type: SET_RELABEL, status };
}

export function setAvailableTools(tools) {
  return { type: SET_AVAILABLE_TOOLS, tools };
}
export function removeAvailableTools(tools) {
  return (dispatch, getState) => {
    const state = getState();
    const availableTools = state.infusions.availableTools;
    if (!availableTools) {
      return;
    }
    const updatedTools = availableTools.filter(x => !tools.includes(x));
    return dispatch({ type: REMOVE_AVAILABLE_TOOLS, tools: updatedTools });
  };
}
