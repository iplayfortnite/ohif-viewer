import { SET_SELECTED_SCISSORS_STRATEGY } from '../constants/ActionTypes';

export const setScissorsStrategy = selectedStrategy => {
  return {
    type: SET_SELECTED_SCISSORS_STRATEGY,
    selectedStrategy,
  };
};
