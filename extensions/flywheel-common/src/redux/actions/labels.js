import {
  SET_ACTIVE_QUESTION,
  SET_AVAILABLE_LABELS,
} from '../constants/ActionTypes';
import { scaleDisplayValidationTools } from '../selectors/tools';

export function setAvailableLabels(labels, tools) {
  return (dispatch, getState) => {
    const state = getState();
    const isScaleDisplayed = state.infusions.isScaleDisplayed;

    if (!isScaleDisplayed) {
      tools?.push(...scaleDisplayValidationTools);
      tools = [...new Set(tools)];
    }

    return dispatch({
      type: SET_AVAILABLE_LABELS,
      labels,
      tools,
    });
  };
}

export function setActiveQuestion(question) {
  return { type: SET_ACTIVE_QUESTION, question };
}
