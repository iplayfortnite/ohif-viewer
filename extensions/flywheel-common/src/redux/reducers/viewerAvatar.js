import cloneDeep from 'lodash.clonedeep';
import {
  SET_VIEWER_AVATAR,
  SET_SINGLE_AVATAR,
  SET_STUDY_STATE_NOTE,
  SET_MULTIPLE_TASK_NOTE,
  SET_MULTIPLE_TASK_USER,
  SET_MULTIPLE_TASK_NOTES_USER_FORM_RESPONSE,
  SET_MULTIPLE_TASK_FORM_RESPONSE,
} from '../constants/ActionTypes';

const defaultState = {
  viewerAvatar: [],
  singleAvatar: false,
  taskId: null,
  isStudyStateNoteUpdate: false,
  multipleTaskNotes: {},
  multipleTaskUser: [],
  multipleTaskFormResponses: [],
};

export default function viewerAvatar(state = defaultState, action) {
  switch (action.type) {
    case SET_VIEWER_AVATAR: {
      return Object.assign({}, state, { viewerAvatar: cloneDeep(action.data) });
    }
    case SET_SINGLE_AVATAR: {
      return Object.assign({}, state, {
        singleAvatar: action.data.status,
        taskId: action.data.taskId,
        isStudyStateNoteUpdate: true,
      });
    }
    case SET_STUDY_STATE_NOTE: {
      return Object.assign({}, state, {
        isStudyStateNoteUpdate: action.status,
      });
    }
    case SET_MULTIPLE_TASK_NOTE: {
      return Object.assign({}, state, {
        multipleTaskNotes: cloneDeep(action.data),
      });
    }
    case SET_MULTIPLE_TASK_USER: {
      return Object.assign({}, state, {
        multipleTaskUser: cloneDeep(action.data),
      });
    }
    case SET_MULTIPLE_TASK_NOTES_USER_FORM_RESPONSE: {
      return Object.assign({}, state, {
        multipleTaskNotes: cloneDeep(action.data.multipleTaskNotes),
        multipleTaskUser: cloneDeep(action.data.multipleTaskUser),
        multipleTaskFormResponses: cloneDeep(
          action.data.multipleTaskFormResponses
        ),
      });
    }
    case SET_MULTIPLE_TASK_FORM_RESPONSE: {
      return Object.assign({}, state, {
        multipleTaskFormResponses: cloneDeep(action.data),
      });
    }
    default:
      return state;
  }
}
