import {
  SET_RESIZED_FORM_HEIGHT,
  SET_RESIZED_SEGMENTATION_PANEL_HEIGHT,
} from '../constants/ActionTypes';

const defaultState = {
  formHeight: null,
  segmentationPanelDivisionHeight: null,
};

export default function measurementPanelProperties(
  state = defaultState,
  action
) {
  switch (action.type) {
    case SET_RESIZED_FORM_HEIGHT: {
      return { ...state, formHeight: action.height };
    }

    case SET_RESIZED_SEGMENTATION_PANEL_HEIGHT: {
      return { ...state, segmentationPanelDivisionHeight: action.height };
    }
    default:
      return state;
  }
}
