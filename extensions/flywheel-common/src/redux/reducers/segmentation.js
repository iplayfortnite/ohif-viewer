import cloneDeep from 'lodash.clonedeep';

const defaultState = {
  segmentationData: {},
  enableLoader: false,
  createdCount: {},
  sliderRange: 10,
};

export default function segmentation(state = defaultState, action) {
  switch (action.type) {
    case 'ADD_SEGMENTATION_DATA': {
      const segmentationData = cloneDeep(state.segmentationData);
      const createdCount = cloneDeep(state.createdCount);
      let obj = cloneDeep(action.data);
      createdCount[obj.modality] = createdCount[obj.modality]
        ? createdCount[obj.modality] + 1
        : 1;
      const referenceUID = obj.referenceUID;
      if (!segmentationData[referenceUID]) {
        segmentationData[referenceUID] = [];
      }
      segmentationData[referenceUID].push({
        id: segmentationData[referenceUID].length + 1,
        originalId: createdCount[obj.modality],
        referenceUID: referenceUID,
        isVisible: obj.isVisible ? obj.isVisible : true,
        isEditing: !!obj.isEditing,
        opacity: 30,
        name: obj.name,
        containerId: obj.parent_ref.id,
        fileId: obj.file_id,
        modality: obj.modality,
        isLoadedInitially: obj.isLoadedInitially
          ? obj.isLoadedInitially
          : false,
        studyInstanceUID: obj.studyInstanceUID,
        seriesInstanceUID: obj.seriesInstanceUID,
      });
      return Object.assign({}, state, {
        segmentationData: segmentationData,
        createdCount: createdCount,
      });
    }
    case 'SET_SEGMENTATION_DATA': {
      let segmentationData = cloneDeep(state.segmentationData);
      const createdCount = cloneDeep(state.createdCount);
      segmentationData[action.referenceUID] = action.data;
      return Object.assign({}, state, {
        segmentationData: segmentationData,
        createdCount: createdCount,
      });
    }
    case 'REMOVE_SEGMENTATION_DATA': {
      let segmentationData = cloneDeep(state.segmentationData);
      const createdCount = cloneDeep(state.createdCount);
      let obj = cloneDeep(action.data);
      if (segmentationData[obj.referenceUID]) {
        let index = segmentationData[obj.referenceUID].findIndex(
          segData => obj.fileId === segData.fileId
        );
        if (index >= 0) {
          segmentationData[obj.referenceUID].splice(index, 1);
        } else if (obj.originalId) {
          index = segmentationData[obj.referenceUID].findIndex(
            segData => obj.originalId === segData.originalId
          );
          if (index >= 0) {
            segmentationData[obj.referenceUID].splice(index, 1);
          }
        }
      }
      return Object.assign({}, state, {
        segmentationData: segmentationData,
        createdCount: createdCount,
      });
    }
    case 'REARRANGE_SEGMENTATION_DATA': {
      const segmentationData = cloneDeep(state.segmentationData);
      const createdCount = cloneDeep(state.createdCount);
      const obj = cloneDeep(action.data);
      const currentReferenceUID = action.currentReferenceUID;
      const newReferenceUID = action.newReferenceUID;
      // Rearrange the segment reference UID to the correct one by removing from
      // the existing map to the new reference UID map
      if (segmentationData[currentReferenceUID]) {
        const index = segmentationData[currentReferenceUID].findIndex(
          segData => obj.fileId === segData.fileId
        );
        if (index >= 0) {
          segmentationData[currentReferenceUID].splice(index, 1);
        }
        if (!segmentationData[newReferenceUID]) {
          segmentationData[newReferenceUID] = [];
        }
        obj.id = segmentationData[newReferenceUID].length + 1;
        obj.referenceUID = newReferenceUID;
        segmentationData[newReferenceUID].push(obj);
      }
      return Object.assign({}, state, {
        segmentationData: segmentationData,
        createdCount: createdCount,
      });
    }
    case 'CLEAR_SEGMENTATION_DATA': {
      return Object.assign({}, { ...defaultState });
    }
    case 'SET_THRESHOLD_TOLERANCE': {
      return { ...state, sliderRange: action.sliderRange };
    }
    default:
      return state;
  }
}
