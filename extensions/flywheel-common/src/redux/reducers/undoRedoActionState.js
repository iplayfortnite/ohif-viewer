import cloneDeep from 'lodash.clonedeep';

import {
  SAVE_ACTION_STATE,
  UNDO_LAST_ACTION_STATE,
  REDO_NEXT_ACTION_STATE,
  CLEAR_ALL_ACTION_STATES,
} from '../constants/ActionTypes';

const defaultState = {
  actionStates: [],
  currentIndex: 0,
  maxActionCount: 10,
};

export default function undoRedoActionState(state = defaultState, action) {
  switch (action.type) {
    case SAVE_ACTION_STATE: {
      const actionStates = cloneDeep(state.actionStates);
      let currentIndex = state.currentIndex;

      // If max action states are reached, then remove the most old action(first from array)
      if (actionStates.length === state.maxActionCount) {
        actionStates.splice(0, 1);
        currentIndex = currentIndex - 1 < 0 ? 0 : currentIndex - 1;
      }

      let newIndex = currentIndex + 1;

      // If the action is of type start and end, then update the currentIndex only at the time end update
      // Ex: "Modify" a measurement
      if (action.actionState.isSubAction) {
        newIndex = currentIndex;
        const prevIndex = newIndex - 1;
        if (action.end && actionStates[prevIndex].end) {
          if (!actionStates[prevIndex].end.sub) {
            actionStates[prevIndex].end.sub = [];
          }
          actionStates[prevIndex].end.sub.push(action.actionState);
        } else if (actionStates[prevIndex].start) {
          if (!actionStates[prevIndex].start.sub) {
            actionStates[prevIndex].start.sub = [];
          }
          actionStates[prevIndex].start.sub.push(action.actionState);
        }
      } else {
        if (action.start && !action.end) {
          newIndex = currentIndex;
          actionStates.splice(
            currentIndex,
            actionStates.length - currentIndex,
            {
              start: action.actionState,
            }
          );
        } else if (!action.start && action.end) {
          if (actionStates[currentIndex].start) {
            actionStates[currentIndex].end = action.actionState;
          } else {
            actionStates[currentIndex].start = action.actionState;
          }
        } else {
          actionStates.splice(
            currentIndex,
            actionStates.length - currentIndex,
            {
              start: action.actionState,
            }
          );
        }
      }

      return Object.assign({}, state, {
        actionStates: actionStates,
        currentIndex: newIndex,
      });
    }
    case UNDO_LAST_ACTION_STATE: {
      let currentIndex = state.currentIndex;

      if (0 <= currentIndex - 1) {
        currentIndex--;
      }

      return Object.assign({}, state, {
        currentIndex: currentIndex,
      });
    }
    case REDO_NEXT_ACTION_STATE: {
      let currentIndex = state.currentIndex;

      if (state.actionStates.length >= currentIndex + 1) {
        currentIndex++;
      }

      return Object.assign({}, state, {
        currentIndex: currentIndex,
      });
    }
    case CLEAR_ALL_ACTION_STATES: {
      return Object.assign({}, { ...defaultState });
    }
    default:
      return state;
  }
}
