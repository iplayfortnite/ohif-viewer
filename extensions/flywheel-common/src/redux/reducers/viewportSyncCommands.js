import cloneDeep from 'lodash.clonedeep';

const defaultState = {
  crossViewportSyncCommands: {},
};

export default function viewportSyncCommands(state = defaultState, action) {
  switch (action.type) {
    case 'SET_VIEWPORT_NOTIFY_COMMAND': {
      const crossViewportSyncCommands = cloneDeep(
        state.crossViewportSyncCommands
      );
      if (
        crossViewportSyncCommands[action.plugin] &&
        crossViewportSyncCommands[action.plugin].commandName ===
          action.commandName
      ) {
        crossViewportSyncCommands[action.plugin].viewportCount += 1;
      } else {
        crossViewportSyncCommands[action.plugin] = {
          commandName: action.commandName,
          viewportCount: 1,
        };
      }
      return Object.assign({}, state, { crossViewportSyncCommands });
    }
    case 'REMOVE_VIEWPORT_NOTIFY_COMMAND': {
      const crossViewportSyncCommands = cloneDeep(
        state.crossViewportSyncCommands
      );
      if (crossViewportSyncCommands[action.plugin]) {
        crossViewportSyncCommands[action.plugin].viewportCount -= 1;
        if (crossViewportSyncCommands[action.plugin].viewportCount <= 0) {
          crossViewportSyncCommands[action.plugin] = null;
          delete crossViewportSyncCommands[action.plugin];
        }
      }
      return Object.assign({}, state, { crossViewportSyncCommands });
    }
    default:
      return state;
  }
}
