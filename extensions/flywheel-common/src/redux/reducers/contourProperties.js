const defaultState = {
  highlightOverlapped: false,
  proximityCursorProperties: null,
};

export default function contourUniqueData(state = defaultState, action) {
  switch (action.type) {
    case 'SET_HIGHLIGHT_OVERLAPPED_STATUS': {
      return Object.assign({}, state, { highlightOverlapped: action.status });
    }
    case 'SET_PROXIMITY_CURSOR_PROPERTIES': {
      return {
        ...defaultState,
        ...{
          proximityCursorProperties: {
            display: action.display || false,
            distance:
              action.distance ||
              state.proximityCursorProperties?.distance ||
              0.125,
            lineStyle:
              action.lineStyle ||
              state.proximityCursorProperties?.lineStyle ||
              'solid',
          },
        },
      };
    }
    default:
      return state;
  }
}
