import {
  SET_HOVERED_MEASUREMENTS,
  SET_MEASUREMENT_COLOR_INTENSITY,
} from '../constants/ActionTypes';

const defaultState = {
  colorIntensity: false,
  activeHoveredMeasurements: [],
};

export default function measurementColorIntensity(
  state = defaultState,
  action
) {
  switch (action.type) {
    case SET_MEASUREMENT_COLOR_INTENSITY: {
      return { ...state, colorIntensity: action.status };
    }
    case SET_HOVERED_MEASUREMENTS: {
      return {
        ...state,
        activeHoveredMeasurements: action.measurements,
      };
    }
    default:
      return state;
  }
}
