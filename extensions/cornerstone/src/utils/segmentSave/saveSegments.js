import saveSegmentsAsNiftiFiles from './saveSegmentsAsNiftiFiles';

/**
 * Save the active viewport segments as files as per the required format.
 * @param {Object} activeViewport
 * @param {String} sourceDataProtocol - source image data protocol
 * @param {String} saveAsType - format for file to create and save
 * @returns {Promise} Promise of the file save
 */
export default function saveSegments(
  activeViewport,
  sourceDataProtocol = 'dicom',
  saveAsType = 'nifti'
) {
  let promise = null;
  switch (saveAsType) {
    case 'nifti': {
      promise = saveSegmentsAsNiftiFiles(activeViewport, sourceDataProtocol);
      break;
    }
    default:
      promise = Promise.resolve({});
      break;
  }
  return promise;
}
