import store from '@ohif/viewer/src/store';
import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import { Redux } from '@flywheel/extension-flywheel-common';

import TOOL_NAMES from '../toolNames';
import {
  setToolEnabledIfNotEnabled,
  setToolDisabledIfNotDisabled,
} from './setToolMode';
import loadPercentData from './PercentageScale';

const scaleIndicatorDisplayTool = TOOL_NAMES.SCALE_INDICATOR_TOOL;
const {
  setIndicatorStatus,
  setAvailableTools,
  removeAvailableTools,
} = Redux.actions;
const { scaleDisplayValidationTools, measurementTools } = Redux.selectors;

const loadScaleIndicator = (isImagePositionChanged = false) => {
  const hasElement = checkHasElement();
  if (!hasElement) {
    return;
  }
  const state = store.getState();
  const isScaleDisplayed = state.infusions.isScaleDisplayed;

  cornerstone.getEnabledElements().forEach((enabledElement, index) => {
    const element = enabledElement.element;
    const projectConfig = store.getState().flywheel.projectConfig;
    const scaleType = projectConfig?.scaleIndicator?.type || 'percent';
    const indicatorModule = csTools.getModule('indicator');

    const tool = csTools.getToolForElement(element, scaleIndicatorDisplayTool);
    tool.clearToolData();
    const toolData = tool.getToolData(element);
    const activeInstance = cornerstone.metaData.get(
      'instance',
      enabledElement.image.imageId
    );
    const indicatorSet = {
      indicatorSetLabel: 'Scale Indicator',
      SeriesInstanceUID: activeInstance.SeriesInstanceUID,
      indicators: [],
      visible: true,
    };
    setTools();
    if (index === 0) {
      indicatorModule.setters.clearIndicatorSetForLabel('Scale Indicator');
    }
    if (!isScaleDisplayed || isImagePositionChanged) {
      indicatorModule.setters.indicatorSet(indicatorSet);
    }
    if (isScaleDisplayed && !isImagePositionChanged) {
      return;
    }
    switch (scaleType) {
      case 'percent':
        loadPercentData(enabledElement, toolData, indicatorSet);
        break;
    }
  });
  if (isScaleDisplayed && !isImagePositionChanged) {
    setToolDisabledIfNotDisabled(scaleIndicatorDisplayTool);
    store.dispatch(setIndicatorStatus(false));
    setTools();
  } else {
    setToolEnabledIfNotEnabled(scaleIndicatorDisplayTool);
    store.dispatch(setIndicatorStatus(true));
    store.dispatch(removeAvailableTools(scaleDisplayValidationTools));
  }
};

const setTools = () => {
  const state = store.getState();
  const isStudyFormExist =
    !!state.flywheel.projectConfig?.studyForm ||
    !!state.flywheel.projectConfig?.questions;
  const currentTools = state.infusions?.availableTools;
  let availableTools = [...scaleDisplayValidationTools];

  if (currentTools) {
    availableTools = [...availableTools, ...currentTools];
  }
  if (!isStudyFormExist) {
    availableTools = [...availableTools, ...measurementTools];
  }
  availableTools = [...new Set(availableTools)];
  store.dispatch(setAvailableTools(availableTools));
};

const clearScaleIndicator = () => {
  const state = store.getState();
  const isScaleDisplayed = state.infusions.isScaleDisplayed;
  if (isScaleDisplayed) {
    loadScaleIndicator();
  }
};

const checkHasElement = () => {
  const hasElement = cornerstone.getEnabledElements().some(enabledElement => {
    if (enabledElement.image) {
      return true;
    }
    return false;
  });
  return hasElement;
};

export { loadScaleIndicator, clearScaleIndicator };
