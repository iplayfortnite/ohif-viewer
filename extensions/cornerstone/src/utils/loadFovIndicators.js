import csTools from 'cornerstone-tools';
import TOOL_NAMES from '../toolNames';
import { setToolEnabledIfNotEnabled } from './setToolMode';
import { cornerstoneUtils } from '@flywheel/extension-cornerstone-infusions';
const imagePointToPatientPoint = csTools.import(
  'util/imagePointToPatientPoint'
);
const projectPatientPointToImagePlane = csTools.import(
  'util/projectPatientPointToImagePlane'
);
const convertToVector3 = csTools.import('util/convertToVector3');
const clipToBox = csTools.importInternal('util/clipToBox');

const globalImageIdSpecificToolStateManager =
  csTools.globalImageIdSpecificToolStateManager;

/**
 * Load the fov indicator reference lines which represents the FOV center and circle.
 * @param {Object} enabledElement - cornerstone element
 * @param {Object} fovReferencesInfo - fov indicator reference object
 * @return void
 */
const loadFovIndicators = (enabledElement, fovReferencesInfo) => {
  const fovPerImage = {};
  const activeInstance = cornerstone.metaData.get('instance', enabledElement.image.imageId);
  const indicatorDisplayTool = TOOL_NAMES.INDICATOR_DISPLAY_TOOL;
  const direction = convertToVector3(fovReferencesInfo.referenceLineDir);

  const indicatorModule = csTools.getModule('indicator');
  const toolState = globalImageIdSpecificToolStateManager.saveToolState();

  const stackToolState = cornerstoneTools.getToolState(
    enabledElement.element,
    'stack'
  );
  const stackData = stackToolState.data[0];

  const indicatorSet = {
    indicatorSetLabel: "FOV Indicator",
    SeriesInstanceUID: activeInstance.SeriesInstanceUID,
    indicators: [],
    visible: true,
  };

  indicatorModule.setters.clearIndicatorSetForLabel('FOV Indicator');

  indicatorModule.setters.indicatorSet(indicatorSet);

  const length = 10;
  const centerPos = fovReferencesInfo.center ? convertToVector3(fovReferencesInfo.center) : null;
  const centerImgIndex = centerPos ? cornerstoneUtils.getNearestSliceIndex(enabledElement, centerPos) : -1;
  let isHorizontal = undefined;

  // Finds the FOV center position reference line in the 2D image
  if (centerImgIndex >= 0) {
    const imgIndexStr = `${centerImgIndex}`;
    const centerImagePlane = cornerstone.metaData.get('imagePlaneModule', stackData.imageIds[centerImgIndex]);
    const image = { width: centerImagePlane.columns, height: centerImagePlane.rows };
    const centerLineStart = centerPos.clone().add(direction.clone().multiplyScalar(length));
    const centerLineEnd = centerPos.clone().add(direction.clone().multiplyScalar(-1 * length));
    const centerLineStartImage = projectPatientPointToImagePlane(centerLineStart, centerImagePlane);
    const centerLineEndImage = projectPatientPointToImagePlane(centerLineEnd, centerImagePlane);
    isHorizontal = Math.abs(centerLineStartImage.x - centerLineEndImage.x) > Math.abs(centerLineStartImage.y - centerLineEndImage.y);
    fovPerImage[imgIndexStr] = {};
    if (fovReferencesInfo.isCenterEnabled) {
      fovPerImage[imgIndexStr].centerLine = clipAndExtend(centerLineStartImage, centerLineEndImage, isHorizontal, image);
    }
  }

  // Finds the horizontal/vertical line end points through circumference for applicable slices
  if (fovReferencesInfo?.circumferencePts?.length) {
    fovReferencesInfo.circumferencePts.forEach(pt => {
      const circlePt = convertToVector3(pt);
      const imgIndex = cornerstoneUtils.getNearestSliceIndex(enabledElement, pt);
      const imgIndexStr = `${imgIndex}`;

      const imagePlane = cornerstone.metaData.get('imagePlaneModule', stackData.imageIds[imgIndex]);
      const image = { width: imagePlane.columns, height: imagePlane.rows };

      const imagePt1 = projectPatientPointToImagePlane(
        circlePt.clone().add(direction.clone().multiplyScalar(length)),
        imagePlane);
      const imagePt2 = projectPatientPointToImagePlane(
        circlePt.clone().add(direction.clone().multiplyScalar(-1 * length)),
        imagePlane);
      let imgRect = {
        left: Math.min(imagePt1.x, imagePt2.x), top: Math.min(imagePt1.y, imagePt2.y),
        right: Math.max(imagePt1.x, imagePt2.x), bottom: Math.max(imagePt1.y, imagePt2.y)
      };
      if (isHorizontal === undefined) {
        isHorizontal = Math.abs(imagePt1.x - imagePt2.x) > Math.abs(imagePt1.y - imagePt2.y);
      }
      if (!fovPerImage[imgIndexStr]) {
        fovPerImage[imgIndexStr] = {};
      }
      if (fovPerImage[imgIndexStr].rect) {
        imgRect = {
          left: Math.min(imgRect.left, fovPerImage[imgIndexStr].rect.left),
          top: Math.min(imgRect.top, fovPerImage[imgIndexStr].rect.top),
          right: Math.max(imgRect.right, fovPerImage[imgIndexStr].rect.right),
          bottom: Math.max(imgRect.bottom, fovPerImage[imgIndexStr].rect.bottom)
        };
      }
      fovPerImage[imgIndexStr].rect = imgRect;

      const referencePoint1 = imagePointToPatientPoint({ x: imgRect.left, y: imgRect.top }, imagePlane);
      const referencePoint2 = imagePointToPatientPoint({ x: imgRect.right, y: imgRect.bottom }, imagePlane);

      const line1Start = referencePoint1.clone().add(direction.clone().multiplyScalar(length));
      const line1End = referencePoint1.clone().add(direction.clone().multiplyScalar(-1 * length));
      const line1StartImage = projectPatientPointToImagePlane(line1Start, imagePlane);
      const line1EndImage = projectPatientPointToImagePlane(line1End, imagePlane);
      fovPerImage[imgIndexStr].line1 = clipAndExtend(line1StartImage, line1EndImage, isHorizontal, image);

      const line2Start = referencePoint2.clone().add(direction.clone().multiplyScalar(length));
      const line2End = referencePoint2.clone().add(direction.clone().multiplyScalar(-1 * length));
      const line2StartImage = projectPatientPointToImagePlane(line2Start, imagePlane);
      const line2EndImage = projectPatientPointToImagePlane(line2End, imagePlane);
      fovPerImage[imgIndexStr].line2 = clipAndExtend(line2StartImage, line2EndImage, isHorizontal, image);
    });
  }

  stackData.imageIds.forEach((imageId, index) => {
    const sliceStr = `${index}`;

    if (!toolState[imageId]) {
      toolState[imageId] = {};
    }

    if (toolState[imageId].hasOwnProperty(indicatorDisplayTool)) {
      delete toolState[imageId].IndicatorDisplayTool;
    }
    if (!fovPerImage[sliceStr]) {
      return;
    }
    const toolData = _getOrCreateImageIdSpecificToolData(
      toolState,
      imageId,
      indicatorDisplayTool
    );

    const lines = [fovPerImage[sliceStr].line1, fovPerImage[sliceStr].line2,
    fovPerImage[sliceStr].centerLine];

    lines.forEach((line, index) => {
      if (!line) {
        return;
      }
      const IndicatorData = {
        IndicatorNumber: (index + 1),
        visible: true,
      };

      indicatorSet.indicators.push(IndicatorData);

      const referenceLineData = {
        handles: {
          points: line,
        },
        type: 'OPEN_PLANAR',
        indicatorSeriesInstanceUid: indicatorSet.SeriesInstanceUID,
        indicatorNumber: (index + 1),
      };

      toolData.push(referenceLineData);
    });
  });
  setToolEnabledIfNotEnabled(indicatorDisplayTool);
};

/**
 * Clip the line within the image limit but extend upto image boundary.
 * @param {Object} startPt - point with x and y co-ordinate
 * @param {Object} endPt - point with x and y co-ordinate
 * @param {boolean} isHor - true if the line is horizontal along the image else false
 * @param {boolean} image - image width and height information
 * @return Array - updated start and end points after clipping and/or extending
 */
function clipAndExtend(startPt, endPt, isHor, image) {
  clipToBox(startPt, image);
  clipToBox(endPt, image);
  if (isHor) {
    startPt.x = 0;
    startPt.y = Math.min(startPt.y, endPt.y);
    endPt.x = image.width;
    endPt.y = startPt.y;
  } else {
    startPt.x = Math.min(startPt.x, endPt.x);
    startPt.y = 0;
    endPt.x = startPt.x;
    endPt.y = image.height;
  }
  return [startPt, endPt];
}
/**
 * Get the image specific tool data, create if not already exist.
 * @param {Object} toolState cornerstone tool state
 * @param {string} imageId - image id
 * @param {string} toolName - tool name
 * @return Object - image specific tool data
 */
 function _getOrCreateImageIdSpecificToolData(toolState, imageId, toolName) {
  if (!toolState.hasOwnProperty(imageId)) {
    toolState[imageId] = {};
  }

  const imageIdToolState = toolState[imageId];

  // If we don't have tool state for this type of tool, add an empty object
  if (!imageIdToolState.hasOwnProperty(toolName)) {
    imageIdToolState[toolName] = {
      data: [],
    };
  }

  return imageIdToolState[toolName].data;
}


export default loadFovIndicators;
