/**
 * Enable the tool if not already and updating image.
 * @param {string} toolName - tool name
 * @return void
 */

function setToolEnabledIfNotEnabled(toolName) {
  cornerstone.getEnabledElements().forEach(enabledElement => {
    const { element, image } = enabledElement;
    const tool = cornerstoneTools.getToolForElement(element, toolName);

    if (tool.mode !== 'enabled') {
      cornerstoneTools.setToolEnabled(toolName);
    }

    if (image) {
      cornerstone.updateImage(element);
    }
  });
}
function setToolDisabledIfNotDisabled(toolName) {
  cornerstone.getEnabledElements().forEach(enabledElement => {
    const { element, image } = enabledElement;
    const tool = cornerstoneTools.getToolForElement(element, toolName);

    if (tool.mode !== 'disabled') {
      cornerstoneTools.setToolDisabled(toolName);
    }

    if (image) {
      cornerstone.updateImage(element);
    }
  });
}

export { setToolEnabledIfNotEnabled, setToolDisabledIfNotDisabled };
