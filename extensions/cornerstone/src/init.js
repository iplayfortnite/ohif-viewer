import OHIF from '@ohif/core';
import store from '@ohif/viewer/src/store';
import { SimpleDialog } from '@ohif/ui';
import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import merge from 'lodash.merge';
import initCornerstoneTools from './initCornerstoneTools.js';
import cloneDeep from 'lodash.clonedeep';
import measurementServiceMappingsFactory from './utils/measurementServiceMappings/measurementServiceMappingsFactory';
import { updateNiftiViewports } from './utils/setCornerstoneNiftiLayout.js';
import {
  Tools as FlywheelCommonTools,
  HTTP as FlywheelCommonHTTP,
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';

import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';
import { getEnabledElement } from './state';
import {
  linkedToolUtils,
  cornerstoneUtils as cornerstoneInfusionUtils,
  updateOtherViewports,
  CrosshairsTool,
} from '@flywheel/extension-cornerstone-infusions';
import IndicatorDisplayTool from './IndicatorDisplayTool';
import ScaleIndicatorTool from './scaleIndicatorTool.js';
import indicatorModule from './indicatorModule';

const { setSessionInfo } = FlywheelCommonHTTP.services;

const { setOrientation, setRotationStatus } = OHIF.redux.actions;
const {
  setActiveTool,
  setProximityCursorProperties,
  setSmartCTRange,
} = FlywheelCommonRedux.actions;
const { measurementTools } = FlywheelCommonRedux.selectors;
const {
  selectActiveSessions,
  selectAssociations,
  selectActiveProject,
  selectProjectConfig,
  selectReaderTask,
} = FlywheelRedux.selectors;
const { setReaderTaskWithFormResponse } = FlywheelRedux.actions;
const { getRouteParams } = FlywheelCommonUtils;
const { getSmartCTRanges } = FlywheelCommonRedux.smartCTRanges;

const OHIF_CONFIG_FILENAME = 'ohif_config.json';

const wheelActions = ['StackScrollMouseWheel', 'ZoomMouseWheel'];

const skipSingleViewOnTools = measurementTools.filter(
  tool => tool !== 'DragProbe'
);

const getDefaultConfiguration = () => {
  const defaultConfig = {
    interpolate: true,
    showFreehandStats: false,
  };

  return defaultConfig;
};

/**
 *
 * @param {Object} servicesManager
 * @param {Object} configuration
 * @param {Object|Array} configuration.csToolsConfig
 */
export default function init({
  servicesManager,
  commandsManager,
  configuration,
}) {
  const { UIDialogService, MeasurementService } = servicesManager.services;

  const callInputDialog = (data, event, callback) => {
    if (UIDialogService) {
      let dialogId = UIDialogService.create({
        centralize: true,
        isDraggable: false,
        content: SimpleDialog.InputDialog,
        useLastPosition: false,
        showOverlay: true,
        contentProps: {
          title: 'Enter your annotation',
          label: 'New label',
          measurementData: data ? { description: data.text } : {},
          onClose: () => UIDialogService.dismiss({ id: dialogId }),
          onSubmit: value => {
            callback(value);
            UIDialogService.dismiss({ id: dialogId });
          },
        },
      });
    }
  };

  const { csToolsConfig } = configuration;
  const metadataProvider = OHIF.cornerstone.metadataProvider;

  cornerstone.metaData.addProvider(
    metadataProvider.get.bind(metadataProvider),
    9999
  );

  // ~~
  const defaultCsToolsConfig = csToolsConfig || {
    globalToolSyncEnabled: true,
    showSVGCursors: true,
    autoResizeViewports: false,
  };

  initCornerstoneTools(defaultCsToolsConfig);

  const toolsGroupedByType = {
    touch: [csTools.PanMultiTouchTool, csTools.ZoomTouchPinchTool],
    annotations: [
      FlywheelCommonTools.ArrowAnnotateTool,
      FlywheelCommonTools.BidirectionalTool,
      FlywheelCommonTools.LengthTool,
      FlywheelCommonTools.ParallelTool,
      FlywheelCommonTools.AngleTool,
      FlywheelCommonTools.FreehandRoiTool,
      FlywheelCommonTools.OpenFreehandRoiTool,
      FlywheelCommonTools.EllipticalRoiTool,
      FlywheelCommonTools.RectangleRoiTool,
      FlywheelCommonTools.FreehandRoiSculptorTool,
      FlywheelCommonTools.CircleRoiTool,
      FlywheelCommonTools.CircleCursorTool,
      FlywheelCommonTools.ContourRoiTool,
      csTools.DragProbeTool,
    ],
    other: [
      FlywheelCommonTools.BrushTool,
      FlywheelCommonTools.SmartBrushTool,
      FlywheelCommonTools.AutoSmartBrushTool,
      FlywheelCommonTools.CircleScissorsTool,
      FlywheelCommonTools.RectangleScissorsTool,
      FlywheelCommonTools.FreehandScissorsTool,
      FlywheelCommonTools.ThresholdTool,
      FlywheelCommonTools.EraserTool,
      FlywheelCommonTools.ZoomTool,
      FlywheelCommonTools.PanTool,
      csTools.RotateTool,
      csTools.WwwcTool,
      csTools.WwwcRegionTool,
      csTools.MagnifyTool,
      csTools.StackScrollTool,
      csTools.StackScrollMouseWheelTool,
      csTools.OverlayTool,
      csTools.ZoomMouseWheelTool,
      csTools.CrosshairsTool,
    ],
  };

  let tools = [];
  Object.keys(toolsGroupedByType).forEach(toolsGroup =>
    tools.push(...toolsGroupedByType[toolsGroup])
  );

  /* Measurement Service */
  _connectToolsToMeasurementService(commandsManager, MeasurementService);

  /* Add extension tools configuration here. */
  const internalToolsConfig = {
    ArrowAnnotate: {
      configuration: {
        getTextCallback: (callback, eventDetails) =>
          callInputDialog(null, eventDetails, callback),
        changeTextCallback: (data, eventDetails, callback) =>
          callInputDialog(data, eventDetails, callback),
      },
    },
  };

  /* Abstract tools configuration using extension configuration. */
  const parseToolProps = (props, tool) => {
    const { annotations } = toolsGroupedByType;
    // An alternative approach would be to remove the `drawHandlesOnHover` config
    // from the supported configuration properties in `cornerstone-tools`
    const toolsWithHideableHandles = annotations.filter(
      tool =>
        !['RectangleRoiTool', 'EllipticalRoiTool', 'CircleRoiTool'].includes(
          tool.name
        )
    );

    let parsedProps = { ...props };

    /**
     * drawHandles - Never/Always show handles
     * drawHandlesOnHover - Only show handles on handle hover (pointNearHandle)
     *
     * Does not apply to tools where handles aren't placed in predictable
     * locations.
     */
    if (
      configuration.hideHandles !== false &&
      toolsWithHideableHandles.includes(tool)
    ) {
      if (props.configuration) {
        parsedProps.configuration.drawHandlesOnHover = true;
      } else {
        parsedProps.configuration = { drawHandlesOnHover: true };
      }
    }

    return parsedProps;
  };

  /* Add tools with its custom props through extension configuration. */
  tools.forEach(tool => {
    const toolName = tool.name.replace('Tool', '');
    const externalToolsConfig = configuration.tools || {};
    const externalToolProps = externalToolsConfig[toolName] || {};
    const internalToolProps = internalToolsConfig[toolName] || {};
    if (
      toolName === 'Zoom' ||
      toolName === 'ZoomTouchPinch' ||
      toolName === 'ZoomMouseWheel'
    ) {
      return;
    }
    const props = merge(
      internalToolProps,
      parseToolProps(externalToolProps, tool)
    );
    csTools.addTool(tool, props);
  });

  // TODO -> We need a better way to do this with maybe global tool state setting all tools passive.
  const BaseAnnotationTool = csTools.importInternal('base/BaseAnnotationTool');
  tools.forEach(tool => {
    if (tool.prototype instanceof BaseAnnotationTool) {
      // BaseAnnotationTool would likely come from csTools lib exports
      const toolName = new tool().name;
      csTools.setToolPassive(toolName); // there may be a better place to determine name; may not be on uninstantiated class
    }
  });

  csTools.register('module', 'indicator', indicatorModule);
  csTools.addTool(IndicatorDisplayTool);
  csTools.addTool(ScaleIndicatorTool);

  const getMouseButtonMask = button => {
    switch (button) {
      case 'left':
        return { mouseButtonMask: 1 };
      case 'right':
        return { mouseButtonMask: 2 };
      case 'middle':
        return { mouseButtonMask: 4 };
      case 'wheel':
        return {};
      default:
        return {};
    }
  };

  const setDefaultMouseActions = () => {
    csTools.setToolActive('Pan', { mouseButtonMask: 4 });
    csTools.setToolActive('Zoom', { mouseButtonMask: 2 });
    csTools.setToolActive('Wwwc', { mouseButtonMask: 1 });
    csTools.setToolActive('StackScrollMouseWheel', {});
    csTools.setToolActive('PanMultiTouch', { pointers: 2 });
    csTools.setToolActive('ZoomTouchPinch', {});
    csTools.setToolEnabled('Overlay', {});
  };

  const setZoomConfiguration = config => {
    const defaultMinZoom = 0.25;
    const defaultMaxZoom = 20;
    const zoomToolsConfig = {
      Zoom: {
        configuration: {
          minScale: config?.zoomLevel
            ? config.zoomLevel.minimum
            : defaultMinZoom,
          maxScale: config?.zoomLevel
            ? config.zoomLevel.maximum
            : defaultMaxZoom,
          invert: false,
          preventZoomOutsideImage: false,
        },
      },
      ZoomTouchPinch: {
        configuration: {
          minScale: config?.zoomLevel
            ? config.zoomLevel.minimum
            : defaultMinZoom,
          maxScale: config?.zoomLevel
            ? config.zoomLevel.maximum
            : defaultMaxZoom,
        },
      },
      ZoomMouseWheel: {
        configuration: {
          minScale: config?.zoomLevel
            ? config.zoomLevel.minimum
            : defaultMinZoom,
          maxScale: config?.zoomLevel
            ? config.zoomLevel.maximum
            : defaultMaxZoom,
          invert: false,
        },
      },
    };
    tools.forEach(tool => {
      const toolName = tool.name.replace('Tool', '');
      const externalToolsConfig = configuration.tools || {};
      const externalToolProps = externalToolsConfig[toolName] || {};
      const internalToolProps = zoomToolsConfig[toolName] || {};
      if (
        toolName !== 'Zoom' &&
        toolName !== 'ZoomTouchPinch' &&
        toolName !== 'ZoomMouseWheel'
      ) {
        return;
      }
      const props = merge(
        internalToolProps,
        parseToolProps(externalToolProps, tool)
      );
      csTools.addTool(tool, props);
    });
    csTools.setToolActive('Zoom', { mouseButtonMask: 2 });
    csTools.setToolActive('ZoomTouchPinch', {});
  };

  // Sets mouse actions and zoom configuration based on project configuration
  const initializeMouseConfg = config => {
    if (config) {
      const mouseActions = config.mouseActions;
      setZoomConfiguration(config);
      if (mouseActions) {
        mouseActions.forEach(action => {
          if (
            action.button === 'wheel' &&
            !wheelActions.includes(action.toolName)
          ) {
            csTools.setToolActive('StackScrollMouseWheel', {});
          } else {
            csTools.setToolActive(
              action.toolName,
              getMouseButtonMask(action.button)
            );
            // To reset the default active tool in localstorage with the configured tool.
            if (action.button === 'left') {
              if (action.toolName === 'Crosshair') {
                CrosshairsTool.toggleCrosshairs();
              }
              store.dispatch(setActiveTool(action.toolName));
            }
          }
        });
      } else {
        setDefaultMouseActions();
      }
    } else {
      setZoomConfiguration(null);
      setDefaultMouseActions();
    }
  };

  const setSelectedSmartCTRange = config => {
    const activeSmartCTRange = store.smartCT?.selectedRange;
    if (!activeSmartCTRange) {
      let selectedRange;
      if (config?.smartCTRanges?.length) {
        selectedRange = config.smartCTRanges[0];
      } else {
        const CTRanges = getSmartCTRanges();
        selectedRange = CTRanges[0];
      }
      store.dispatch(setSmartCTRange(selectedRange));
    }
  };

  const interval = setInterval(async () => {
    const state = store.getState();
    const project = selectActiveProject(state);
    const readerTask = await selectReaderTask(state);
    if (
      project &&
      (readerTask ||
        project.files.some(file => file.name === OHIF_CONFIG_FILENAME))
    ) {
      const config = selectProjectConfig(state);
      if (config) {
        clearInterval(interval);
        initializeMouseConfg(config);
        setSelectedSmartCTRange(config);
        store.dispatch(
          setProximityCursorProperties(
            config.proximityRoiCursor?.display || false,
            config.proximityRoiCursor?.distance || 0.125,
            config.proximityRoiCursor?.lineStyle || "solid"
          )
        );
      }
    } else if (project) {
      clearInterval(interval);
      initializeMouseConfg(null);
      setSelectedSmartCTRange(null);
      store.dispatch(setProximityCursorProperties(false));
    }
  }, 100);
}

const _initMeasurementService = measurementService => {
  /* Initialization */
  const { toAnnotation, toMeasurement } = measurementServiceMappingsFactory(
    measurementService
  );
  const csToolsVer4MeasurementSource = measurementService.createSource(
    'CornerstoneTools',
    '4'
  );

  /* Matching Criterias */
  const matchingCriteria = {
    valueType: measurementService.VALUE_TYPES.POLYLINE,
    points: 2,
  };

  /* Mappings */
  measurementService.addMapping(
    csToolsVer4MeasurementSource,
    'Length',
    matchingCriteria,
    toAnnotation,
    toMeasurement
  );

  return csToolsVer4MeasurementSource;
};

const _connectToolsToMeasurementService = (
  commandsManager,
  measurementService
) => {
  const csToolsVer4MeasurementSource = _initMeasurementService(
    measurementService
  );
  const {
    id: sourceId,
    addOrUpdate,
    getAnnotation,
  } = csToolsVer4MeasurementSource;

  cornerstone.events.addEventListener(
    cornerstone.EVENTS.IMAGE_LOADED,
    event => {
      const state = store.getState();
      let viewportSpecificData = state.viewports.viewportSpecificData;
      let orientations = state.timepointManager.orientations;

      const getItem = view => {
        return orientations.find(
          item =>
            view.StudyInstanceUID === item.StudyInstanceUID &&
            view.SeriesInstanceUID === item.SeriesInstanceUID
        );
      };

      !state.timepointManager.isRotating &&
        Object.keys(viewportSpecificData).forEach((key, index) => {
          let item = getItem(viewportSpecificData[key]);
          if (item) {
            let enabledElements = cornerstone.getEnabledElements();
            if (enabledElements[index]) {
              let enabledElement = enabledElements[index].element;
              let viewport = cornerstone.getViewport(enabledElement);
              if (viewport) {
                viewport.rotation = item.rotation;
                cornerstone.setViewport(enabledElement, viewport);
              }
            }
          }
        });

      // In mix viewports mode, sync the other type(ex: vtk js) viewports properties like Wwwc
      // which are normally initializing from cornerstone viewport
      if (
        Object.keys(viewportSpecificData).find(
          displaySet => !displaySet.plugin?.includes('cornerstone')
        )
      ) {
        Object.keys(viewportSpecificData).forEach(key => {
          const displaySet = viewportSpecificData[key];
          if (displaySet.plugin?.includes('cornerstone')) {
            const { StudyInstanceUID, SeriesInstanceUID } = displaySet;
            cornerstone.getEnabledElements().forEach(enabledElement => {
              const element = enabledElement.element;

              if (
                enabledElement?.image?.imageId?.includes(StudyInstanceUID) &&
                (enabledElement?.image?.imageId?.includes(SeriesInstanceUID) ||
                  displaySet.dataProtocol === 'nifti')
              ) {
                updateOtherViewports(
                  commandsManager,
                  ['Wwwc'],
                  displaySet,
                  element
                );
              }
            });
          }
        });
      }
    }
  );

  /* Measurement Service Events */
  cornerstone.events.addEventListener(
    cornerstone.EVENTS.ELEMENT_ENABLED,
    event => {
      const {
        MEASUREMENT_ADDED,
        MEASUREMENT_UPDATED,
      } = measurementService.EVENTS;

      measurementService.subscribe(
        MEASUREMENT_ADDED,
        ({ source, measurement }) => {
          if (![sourceId].includes(source.id)) {
            const annotation = getAnnotation('Length', measurement.id);

            console.log(
              'Measurement Service [Cornerstone]: Measurement added',
              measurement
            );
            console.log('Mapped annotation:', annotation);
          }
        }
      );

      measurementService.subscribe(
        MEASUREMENT_UPDATED,
        ({ source, measurement }) => {
          if (![sourceId].includes(source.id)) {
            const annotation = getAnnotation('Length', measurement.id);

            console.log(
              'Measurement Service [Cornerstone]: Measurement updated',
              measurement
            );
            console.log('Mapped annotation:', annotation);
          }
        }
      );

      const addOrUpdateMeasurement = csToolsAnnotation => {
        try {
          const { toolName, toolType, measurementData } = csToolsAnnotation;
          const csTool = toolName || measurementData.toolType || toolType;
          csToolsAnnotation.id = measurementData._measurementServiceId;
          const measurementServiceId = addOrUpdate(csTool, csToolsAnnotation);

          if (!measurementData._measurementServiceId) {
            addMeasurementServiceId(measurementServiceId, csToolsAnnotation);
          }
        } catch (error) {
          console.warn('Failed to add or update measurement:', error);
        }
      };

      const addMeasurementServiceId = (id, csToolsAnnotation) => {
        const { measurementData } = csToolsAnnotation;
        Object.assign(measurementData, { _measurementServiceId: id });
      };

      [csTools.EVENTS.MOUSE_DRAG].forEach(csToolsEvtName => {
        event.detail.element.addEventListener(
          csToolsEvtName,
          ({ detail: eventData }) => {
            const state = store.getState();
            if (state.infusions.activeTool === 'Rotate') {
              if (!state.timepointManager.isRotating) {
                store.dispatch(setRotationStatus(true));
              }
              let activeViewportIndex = state.viewports.activeViewportIndex;
              let viewportSpecificData = state.viewports.viewportSpecificData;

              if (viewportSpecificData?.[activeViewportIndex]) {
                let currentViewport = viewportSpecificData[activeViewportIndex];
                let enabledElements = cornerstone.getEnabledElements();
                Object.keys(viewportSpecificData).forEach((key, index) => {
                  if (index != activeViewportIndex) {
                    let view = viewportSpecificData[key];
                    if (
                      view.SeriesInstanceUID ===
                        currentViewport.SeriesInstanceUID &&
                      view.StudyInstanceUID === currentViewport.StudyInstanceUID
                    ) {
                      if (enabledElements[index]?.element) {
                        let enabledElement = enabledElements[index].element;
                        let viewport = cornerstone.getViewport(enabledElement);
                        viewport.rotation = eventData.viewport.rotation;
                        cornerstone.setViewport(enabledElement, viewport);
                      }
                    }
                  }
                });
              }
            }
          }
        );
      });

      [csTools.EVENTS.MOUSE_UP].forEach(csToolsEvtName => {
        event.detail.element.addEventListener(
          csToolsEvtName,
          ({ detail: eventData }) => {
            const state = store.getState();
            if (state.infusions.activeTool === 'Rotate') {
              let activeViewportIndex = state.viewports.activeViewportIndex;
              let viewportSpecificData = state.viewports.viewportSpecificData;
              if (viewportSpecificData?.[activeViewportIndex]) {
                const linkRotationEnabled = linkedToolUtils.getLinkButtonStatus(
                  'Orient'
                );
                let activeViewportOrientation;
                let enabledElements;
                if (linkRotationEnabled) {
                  enabledElements = [
                    ...FlywheelCommonUtils.cornerstoneUtils.getEnabledElements(),
                  ];
                  activeViewportOrientation = cornerstoneInfusionUtils.getOrientation(
                    enabledElements[activeViewportIndex]
                  );
                }
                const sessions = selectActiveSessions(state);
                const associations = selectAssociations(state);
                Object.keys(viewportSpecificData).forEach(
                  (key, viewportIndex) => {
                    if (viewportIndex !== activeViewportIndex) {
                      if (linkRotationEnabled) {
                        const curViewportOrientation = cornerstoneInfusionUtils.getOrientation(
                          enabledElements[viewportIndex]
                        );
                        if (
                          activeViewportOrientation !== curViewportOrientation
                        ) {
                          return;
                        }
                      } else {
                        return;
                      }
                    }
                    const currentViewport = viewportSpecificData[key];
                    const data = {
                      rotation: eventData.viewport.rotation,
                      SeriesInstanceUID: currentViewport.SeriesInstanceUID,
                      StudyInstanceUID: currentViewport.StudyInstanceUID,
                      SeriesNumber: currentViewport.SeriesNumber,
                    };
                    store.dispatch(setOrientation(data));
                    store.dispatch(setRotationStatus(false));

                    const assoc = associations[data.StudyInstanceUID] || {};
                    const sessionId = assoc.session_id;
                    const session =
                      sessions !== null
                        ? sessions.find(session => session._id === sessionId)
                        : null;

                    let orientations = state.timepointManager.orientations;
                    const index = orientations.findIndex(
                      item =>
                        item.SeriesInstanceUID === data.SeriesInstanceUID &&
                        item.StudyInstanceUID === data.StudyInstanceUID
                    );
                    if (index > -1) {
                      orientations[index] = cloneDeep(data);
                    } else {
                      orientations.push(data);
                    }

                    const { taskId } = getRouteParams();
                    if (taskId) {
                      let readerTask = _.cloneDeep(
                        state.flywheel.readerTaskWithFromResponse
                      );
                      if (readerTask) {
                        if (!readerTask.info) {
                          readerTask.info = {};
                        }
                        let activeOrientations =
                          readerTask.info.orientations || [];
                        const orientationIndex = activeOrientations.findIndex(
                          item =>
                            item.SeriesInstanceUID === data.SeriesInstanceUID &&
                            item.StudyInstanceUID === data.StudyInstanceUID
                        );

                        if (orientationIndex > -1) {
                          activeOrientations[orientationIndex] = cloneDeep(
                            data
                          );
                        } else {
                          activeOrientations.push(data);
                        }

                        readerTask.info = {
                          ...readerTask.info,
                          orientations: [...activeOrientations],
                        };
                        store.dispatch(
                          setReaderTaskWithFormResponse(readerTask)
                        );
                      }
                    } else if (session !== null) {
                      let activeSessionOrientations =
                        session?.info?.ohifViewer?.orientations || [];
                      const indexInSession = activeSessionOrientations.findIndex(
                        item =>
                          item.SeriesInstanceUID === data.SeriesInstanceUID &&
                          item.StudyInstanceUID === data.StudyInstanceUID
                      );

                      if (indexInSession > -1) {
                        activeSessionOrientations[indexInSession] = cloneDeep(
                          data
                        );
                      } else {
                        activeSessionOrientations.push(data);
                      }

                      const requestData = {
                        ...session.info.ohifViewer,
                        orientations: [...activeSessionOrientations],
                      };

                      setSessionInfo(session._id, requestData);
                    }
                  }
                );
              }
            } else if (
              state.infusions.activeTool === 'Wwwc' ||
              state.infusions.activeTool === 'StackScroll' ||
              state.infusions.activeTool === 'Zoom' ||
              state.infusions.activeTool === 'Crosshairs'
            ) {
              const activeViewportIndex = state.viewports.activeViewportIndex;
              const viewportSpecificData =
                state.viewports.viewportSpecificData[activeViewportIndex];
              if (viewportSpecificData) {
                const element = getEnabledElement(activeViewportIndex);
                updateOtherViewports(
                  commandsManager,
                  [state.infusions.activeTool],
                  viewportSpecificData,
                  element,
                  state.infusions.activeTool === 'StackScroll'
                );
              }
            }
          }
        );
      });

      [csTools.EVENTS.MOUSE_WHEEL].forEach(csToolsEvtName => {
        event.detail.element.addEventListener(
          csToolsEvtName,
          ({ detail: eventData }) => {
            const state = store.getState();
            const activeViewportIndex = state.viewports.activeViewportIndex;
            const viewportSpecificData =
              state.viewports.viewportSpecificData[activeViewportIndex];
            if (viewportSpecificData) {
              const element = getEnabledElement(activeViewportIndex);
              updateOtherViewports(
                commandsManager,
                wheelActions,
                viewportSpecificData,
                element,
                true
              );
            }
          }
        );
      });

      [
        csTools.EVENTS.MEASUREMENT_ADDED,
        csTools.EVENTS.MEASUREMENT_MODIFIED,
      ].forEach(csToolsEvtName => {
        event.detail.element.addEventListener(
          csToolsEvtName,
          ({ detail: csToolsAnnotation }) => {
            console.log(`Cornerstone Element Event: ${csToolsEvtName}`);
            addOrUpdateMeasurement(csToolsAnnotation);
          }
        );
      });

      [csTools.EVENTS.MOUSE_DOUBLE_CLICK].forEach(csToolsEvtName => {
        event.detail.element.addEventListener(
          csToolsEvtName,
          ({ detail: eventData }) => {
            const state = store.getState();
            if (!skipSingleViewOnTools.includes(state.infusions.activeTool)) {
              const activeViewportIndex = state.viewports.activeViewportIndex;
              const viewportSpecificData =
                state.viewports.viewportSpecificData[activeViewportIndex];
              const toolNames = [
                'StackScrollMouseWheel',
                'ZoomMouseWheel',
                'Wwwc',
                'Rotate',
              ];
              if (viewportSpecificData?.plugin === 'cornerstone::nifti') {
                const element = getEnabledElement(activeViewportIndex);
                updateNiftiViewports(
                  commandsManager,
                  toolNames,
                  viewportSpecificData,
                  element,
                  true
                );
              }
            }
          }
        );
      });
    }
  );
};
