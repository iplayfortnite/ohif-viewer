import React, { Component } from 'react';
import { vec3 } from 'gl-matrix';

import ConnectedCornerstoneViewport from './ConnectedCornerstoneViewport';
import OHIF from '@ohif/core';
import store from '@ohif/viewer/src/store';
import PropTypes from 'prop-types';
import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';

import {
  Redux as FlywheelCommonRedux,
  Utils,
} from '@flywheel/extension-flywheel-common';
import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';
import { getEnabledElement, clearEnabledElement } from './state';
import SliderDirection from './SliderDirection';

const { StackManager } = OHIF.utils;
const { setViewportActive } = OHIF.redux.actions;
const { removeViewportNotifyCommand } = FlywheelCommonRedux.actions;
const { selectActiveViewportIndex } = FlywheelCommonRedux.selectors;
const { selectProjectConfig } = FlywheelRedux.selectors;
const { bSliceColorUtils, isBSliceApplicable, isCurrentFile } = Utils;

class OHIFCornerstoneViewport extends Component {
  state = {
    viewportData: null,
    border: null,
    sliceSettings: null,
    scrollDirection: { show: false },
  };

  static defaultProps = {
    customProps: {},
  };

  static propTypes = {
    studies: PropTypes.object,
    displaySet: PropTypes.object,
    viewportIndex: PropTypes.number,
    children: PropTypes.node,
    customProps: PropTypes.object,
  };

  static id = 'OHIFCornerstoneViewport';

  static init() {
    console.log('OHIFCornerstoneViewport init()');
  }

  static destroy() {
    console.log('OHIFCornerstoneViewport destroy()');
    StackManager.clearStacks();
  }

  /**
   * Obtain the CornerstoneTools Stack for the specified display set.
   *
   * @param {Object[]} studies
   * @param {String} StudyInstanceUID
   * @param {String} displaySetInstanceUID
   * @param {String} [SOPInstanceUID]
   * @param {Number} [frameIndex=1]
   * @return {Object} CornerstoneTools Stack
   */
  static getCornerstoneStack(
    studies,
    StudyInstanceUID,
    displaySetInstanceUID,
    SOPInstanceUID,
    frameIndex = 0
  ) {
    if (!studies || !studies.length) {
      throw new Error('Studies not provided.');
    }

    if (!StudyInstanceUID) {
      throw new Error('StudyInstanceUID not provided.');
    }

    if (!displaySetInstanceUID) {
      throw new Error('StudyInstanceUID not provided.');
    }

    // Create shortcut to displaySet
    const study = studies.find(
      study => study.StudyInstanceUID === StudyInstanceUID
    );

    if (!study) {
      throw new Error('Study not found.');
    }

    const displaySet = study.displaySets.find(set => {
      return set.displaySetInstanceUID === displaySetInstanceUID;
    });

    if (!displaySet) {
      throw new Error('Display Set not found.');
    }

    // Get stack from Stack Manager
    const storedStack = StackManager.findOrCreateStack(study, displaySet);

    // Clone the stack here so we don't mutate it
    const stack = Object.assign({}, storedStack);
    const isMultiFrame =
      displaySet.numImageFrames > 1 && displaySet.isMultiFrame;
    stack.currentImageIdIndex = frameIndex;

    if (SOPInstanceUID) {
      const index = stack.imageIds.findIndex(imageId => {
        const imageIdSOPInstanceUID = cornerstone.metaData.get(
          'SOPInstanceUID',
          imageId
        );
        if (isMultiFrame) {
          const currentFrameIndex = stack.imageIds.indexOf(imageId);
          return (
            imageIdSOPInstanceUID === SOPInstanceUID &&
            currentFrameIndex === stack.currentImageIdIndex
          );
        } else {
          return imageIdSOPInstanceUID === SOPInstanceUID;
        }
      });

      if (index > -1) {
        stack.currentImageIdIndex = index;
      } else {
        console.warn(
          'SOPInstanceUID provided was not found in specified DisplaySet'
        );
      }
    }

    return stack;
  }

  getViewportData = async (
    studies,
    StudyInstanceUID,
    displaySetInstanceUID,
    SOPInstanceUID,
    frameIndex
  ) => {
    let viewportData;

    const stack = OHIFCornerstoneViewport.getCornerstoneStack(
      studies,
      StudyInstanceUID,
      displaySetInstanceUID,
      SOPInstanceUID,
      frameIndex
    );

    viewportData = {
      StudyInstanceUID,
      displaySetInstanceUID,
      stack,
    };

    return viewportData;
  };

  setStateFromProps() {
    const { studies, displaySet } = this.props.viewportData;
    const {
      StudyInstanceUID,
      displaySetInstanceUID,
      sopClassUIDs,
      SOPInstanceUID,
      frameIndex,
    } = displaySet;

    if (!StudyInstanceUID || !displaySetInstanceUID) {
      return;
    }

    if (sopClassUIDs?.length > 1) {
      console.warn(
        'More than one SOPClassUID in the same series is not yet supported.'
      );
    }

    this.getViewportData(
      studies,
      StudyInstanceUID,
      displaySetInstanceUID,
      SOPInstanceUID,
      frameIndex
    ).then(viewportData => {
      this.setState({
        viewportData,
      });
    });
  }

  componentDidMount() {
    this.setStateFromProps();
  }

  getScrollDirection() {
    const {
      getOrientationString,
      invertOrientationString,
    } = cornerstoneTools.orientation;

    const { imageIds, currentImageIdIndex } = this.state.viewportData.stack;

    const { rowCosines, columnCosines } =
      cornerstone.metaData.get(
        'imagePlaneModule',
        imageIds[currentImageIdIndex]
      ) || {};
    const axis = [];
    vec3.cross(axis, rowCosines, columnCosines);
    const ANGLE_TOLERANCE = 15;
    let downString = getOrientationString(
      axis,
      Math.cos(((90 - ANGLE_TOLERANCE) * Math.PI) / 180)
    );
    let upString = invertOrientationString(downString);

    //If the slices are arranged along normal,distance(from center of reference) to slice0 < slice1 < slice2.
    //If the slices are arranged in opposite direction,distance to slice0 > slice1.
    const currentSliceIndex =
      currentImageIdIndex === 0 ? 1 : currentImageIdIndex;
    const currentSlice = cornerstone.metaData.get(
      'imagePlaneModule',
      imageIds[currentSliceIndex]
    );
    const prevSlice = cornerstone.metaData.get(
      'imagePlaneModule',
      imageIds[currentSliceIndex - 1]
    );
    var currentSliceDist = vec3.dot(axis, currentSlice.imagePositionPatient);
    var prevSliceDist = vec3.dot(axis, prevSlice.imagePositionPatient);
    if (prevSliceDist > currentSliceDist) {
      [upString, downString] = [downString, upString];
    }
    return {
      up: getFullOrientationString(upString),
      down: getFullOrientationString(downString),
    };
  }

  //sets scroll direction display values on mouse hover.
  changeOrientationDispaly = event => {
    if (!isScrollbarLabelVisible()) {
      return;
    }

    const slider = event.currentTarget;
    const thumbHeight = window.getComputedStyle(
      slider,
      '::-webkit-slider-thumb'
    ).height;
    const thumbStepCount = slider.max / parseInt(thumbHeight);

    let min = parseInt(slider.value) - thumbStepCount / 2;
    let max = parseInt(slider.value) + thumbStepCount / 2;
    if (min < 0) {
      min = 0;
      max = thumbStepCount;
    }
    if (max > slider.max) {
      min = slider.max - thumbStepCount;
      max = slider.max;
    }
    // Checks the mouse is over slider thumb.
    const hoveredValue = (event.offsetX / slider.offsetWidth) * slider.max;
    if (hoveredValue < min || hoveredValue > max) {
      this.setState({
        scrollDirection: {
          show: false,
        },
      });
      return;
    }
    let scrollDirectionValue = this.state.scrollDirection.value;
    if (!scrollDirectionValue) {
      scrollDirectionValue = this.getScrollDirection();
    }

    const enabledElement = getEnabledElement(this.props.viewportIndex);
    const image = cornerstone.pageToPixel(
      enabledElement,
      event.pageX,
      event.pageY
    );
    const canvas = cornerstone.pixelToCanvas(enabledElement, image);
    this.setState({
      scrollDirection: {
        show: true,
        position: [canvas.x - 5, canvas.y - 40],
        value: { ...scrollDirectionValue },
      },
    });
  };

  //disable scroll direction display on mouse out.
  removeOrientationDisplay = event => {
    if (!isScrollbarLabelVisible()) {
      return;
    }

    if (this.state.scrollDirection.show) {
      this.setState({
        scrollDirection: {
          show: false,
        },
      });
    }
  };

  boundChangeOrientationDisplay = this.changeOrientationDispaly.bind(this);
  boundRemoveOrientationDisplay = this.removeOrientationDisplay.bind(this);

  componentDidUpdate(prevProps) {
    const { displaySet } = this.props.viewportData;
    const prevDisplaySet = prevProps.viewportData.displaySet;

    if (
      displaySet.displaySetInstanceUID !==
        prevDisplaySet.displaySetInstanceUID ||
      displaySet.SOPInstanceUID !== prevDisplaySet.SOPInstanceUID ||
      displaySet.frameIndex !== prevDisplaySet.frameIndex
    ) {
      this.setStateFromProps();
    }
    const state = store.getState();
    let config = selectProjectConfig(state);

    // Special handling to restrict the b slice configurations only for single series
    // Todo - Need to revisit once this restriction can be relaxed
    if (!isBSliceApplicable(state)) {
      config = {
        ...config,
        ...{ bSlices: null },
      };
    }
    //Keep the B-slice settings and border color in state
    const sliceSettings = config?.bSlices?.settings;
    if (sliceSettings) {
      const currentImageIdIndex = this.state.viewportData?.stack
        ?.currentImageIdIndex;
      const sliceIndex =
        currentImageIdIndex >= 0 ? currentImageIdIndex : displaySet.frameIndex;
      const sliceNumberKey = sliceIndex ? `${sliceIndex + 1}` : '1';
      let border = null;
      if (sliceSettings[sliceNumberKey]) {
        border = {
          color:
            bSliceColorUtils.getBSliceIndicatorColor(
              sliceNumberKey,
              sliceSettings
            ) || null,
        };
      }
      if (
        !this.state.sliceSettings ||
        this.state.border?.color !== border?.color
      ) {
        this.setState({
          sliceSettings,
          border,
        });
      }
    } else if (!sliceSettings && this.state.sliceSettings) {
      this.setState({
        sliceSettings,
      });
    }

    // Hack to hide the viewport scrollbar element based on configuration as there
    // is no option to pass the scrollbar status through props
    // TODO - Need to revisit by extending the viewport and hiding the element.
    if (config?.hideViewportScrollbar) {
      const elements = document.getElementsByClassName('scroll');
      if (elements?.length) {
        for (let i = 0; i < elements.length; i++) {
          elements[i].style.display = 'none';
        }
      }
    } else {
      // To display slider direction when hovering mouse on slider thumb.
      // Needs to be displayed only when slider is not hidden.
      const viewportElement = document.getElementsByClassName(
        `viewport-${this.props.viewportIndex}`
      )[0];
      if (viewportElement) {
        const sliderElements = viewportElement.getElementsByClassName(
          'imageSlider'
        );
        if (sliderElements && sliderElements.length > 0) {
          sliderElements[0].removeEventListener(
            'mouseover',
            this.boundChangeOrientationDisplay
          );
          sliderElements[0].addEventListener(
            'mouseover',
            this.boundChangeOrientationDisplay
          );
          sliderElements[0].removeEventListener(
            'mousemove',
            this.boundChangeOrientationDisplay
          );
          sliderElements[0].addEventListener(
            'mousemove',
            this.boundChangeOrientationDisplay
          );

          sliderElements[0].removeEventListener(
            'mouseout',
            this.boundRemoveOrientationDisplay
          );
          sliderElements[0].addEventListener(
            'mouseout',
            this.boundRemoveOrientationDisplay
          );
        }
      }
    }
  }

  componentWillUnmount() {
    const { displaySet } = this.props.viewportData;
    clearEnabledElement(this.props.viewportIndex);
    store.dispatch(
      removeViewportNotifyCommand(displaySet.plugin || 'cornerstone')
    );

    const viewportElement = document.getElementsByClassName(
      `viewport-${this.props.viewportIndex}`
    )[0];
    if (viewportElement) {
      const sliderElements = viewportElement.getElementsByClassName(
        'imageSlider'
      );
      if (sliderElements && sliderElements.length > 0) {
        sliderElements[0].removeEventListener(
          'mouseover',
          this.boundChangeOrientationDisplay
        );
        sliderElements[0].removeEventListener(
          'mousemove',
          this.boundChangeOrientationDisplay
        );
        sliderElements[0].removeEventListener(
          'mouseout',
          this.boundRemoveOrientationDisplay
        );
      }
    }
  }

  render() {
    let childrenWithProps = null;

    if (!this.state.viewportData) {
      return null;
    }
    const { viewportIndex } = this.props;
    const {
      imageIds,
      currentImageIdIndex,
      dataProtocol,
      // If this comes from the instance, would be a better default
      // `FrameTime` in the instance
      // frameRate = 0,
    } = this.state.viewportData.stack;

    // TODO: Does it make more sense to use Context?
    if (this.props.children?.length) {
      childrenWithProps = this.props.children.map((child, index) => {
        return (
          child &&
          React.cloneElement(child, {
            viewportIndex: this.props.viewportIndex,
            key: index,
          })
        );
      });
    }

    let orientationIndicator = null;
    if (this.state.scrollDirection.show) {
      orientationIndicator = (
        <SliderDirection
          scrollDirectionValue={this.state.scrollDirection.value || {}}
          position={this.state.scrollDirection.position || []}
        />
      );
    }

    const newImageHandler = ({ currentImageIdIndex, sopInstanceUid }) => {
      const state = store.getState();
      const { displaySet } = this.props.viewportData;
      const { StudyInstanceUID } = displaySet;

      if (currentImageIdIndex > 0) {
        const currentActiveViewportIndex = selectActiveViewportIndex(state);
        this.props.onNewImage({
          StudyInstanceUID,
          SOPInstanceUID: sopInstanceUid,
          frameIndex: currentImageIdIndex,
          activeViewportIndex: viewportIndex,
        });
        store.dispatch(setViewportActive(currentActiveViewportIndex));
      }

      // Update border state while scrolling
      let border = null;
      const sliceNumberKey = `${currentImageIdIndex + 1}`;
      if (
        this.state.sliceSettings &&
        this.state.sliceSettings[sliceNumberKey]
      ) {
        border = {
          color:
            bSliceColorUtils.getBSliceIndicatorColor(
              sliceNumberKey,
              this.state.sliceSettings
            ) || null,
        };
      }

      this.setState({
        border,
        viewportData: !this.state.viewportData
          ? this.state.viewportData
          : {
              ...this.state.viewportData,
              stack: { ...this.state.viewportData.stack, currentImageIdIndex },
            },
      });
    };

    let style = {
      width: '100%',
      height: '100%',
      position: 'relative',
      border: '3px solid transparent',
    };
    const state = store.getState();
    const projectConfig = selectProjectConfig(state);
    if (this.state.border && !isCurrentFile(state) && projectConfig?.bSlices) {
      const color = this.state.border.color || 'red';
      style.border = '6px solid ' + color;
    }

    return (
      <>
        <div style={style} onContextMenu={e => e.preventDefault()}>
          <ConnectedCornerstoneViewport
            viewportIndex={viewportIndex}
            imageIds={imageIds}
            imageIdIndex={currentImageIdIndex}
            dataProtocol={dataProtocol}
            onNewImage={newImageHandler}
            onNewImageDebounceTime={700}
            // ~~ Connected (From REDUX)
            // frameRate={frameRate}
            // isPlaying={false}
            // isStackPrefetchEnabled={true}
            // onElementEnabled={() => {}}
            // setViewportActive{() => {}}
            {...this.props.customProps}
          />
          {childrenWithProps}
          {orientationIndicator}
        </div>
      </>
    );
  }
}

const getFullOrientationString = shortString => {
  let fullString = '';
  [...shortString].forEach(c => {
    switch (c) {
      case 'L': {
        fullString += 'Left';
        break;
      }
      case 'R': {
        fullString += 'Right';
        break;
      }
      case 'A': {
        fullString += 'Anterior';
        break;
      }
      case 'P': {
        fullString += 'Posterior';
        break;
      }
      case 'H': {
        fullString += 'Head';
        break;
      }
      case 'F': {
        fullString += 'Foot';
        break;
      }
      default: {
        break;
      }
    }
    fullString += ' ';
  });
  return fullString.length > 0
    ? fullString.substring(0, fullString.length - 1)
    : fullString;
};

const isScrollbarLabelVisible = () => {
  const state = store.getState();
  let config = selectProjectConfig(state);
  return config?.hideScrollbarLabel === false;
};

export default OHIFCornerstoneViewport;
