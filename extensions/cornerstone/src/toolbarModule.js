import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import { Components as FlywheelComponents } from '@flywheel/extension-flywheel';
// TODO: A way to add Icons that don't already exist?
// - Register them and add
// - Include SVG Source/Inline?
// - By URL, or own component?

// What KINDS of toolbar buttons do we have...
// - One's that dispatch commands
// - One's that set tool's active
// - More custom, like CINE
//    - Built in for one's like this, or custom components?

// Visible?
// Disabled?
// Based on contexts or misc. criteria?
//  -- ACTIVE_ROUTE::VIEWER
//  -- ACTIVE_VIEWPORT::CORNERSTONE
// setToolActive commands should receive the button event that triggered
// so we can do the "bind to this button" magic

const TOOLBAR_BUTTON_TYPES = {
  COMMAND: 'command',
  SET_TOOL_ACTIVE: 'setToolActive',
  BUILT_IN: 'builtIn',
  SEPARATOR: 'separator',
};

const TOOLBAR_BUTTON_BEHAVIORS = {
  CINE: 'CINE',
  DOWNLOAD_SCREEN_SHOT: 'DOWNLOAD_SCREEN_SHOT',
};

/* TODO: Export enums through a extension manager. */
const enums = {
  TOOLBAR_BUTTON_TYPES,
  TOOLBAR_BUTTON_BEHAVIORS,
};

const definitions = [
  {
    id: 'StudyList',
    label: 'Study List',
    icon: 'contact_page',
    class: 'material-icons',
    CustomComponent: FlywheelComponents.StudyListComponent,
  },
  {
    id: 'StackScroll',
    label: 'Stack Scroll',
    icon: 'bars',
    //
    type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
    commandName: 'setToolActive',
    commandOptions: { toolName: 'StackScroll' },
  },
  {
    id: 'ZoomMenu',
    label: 'Zoom',
    icon: 'search-plus',
    //
    buttons: [
      {
        id: 'Zoom',
        label: 'Zoom',
        icon: 'search-plus',
        //
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'Zoom' },
      },
      {
        id: 'Magnify',
        label: 'Magnify',
        icon: 'circle',
        //
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'Magnify' },
      },
    ],
  },
  {
    id: 'LevelsMenu',
    label: 'Levels',
    icon: 'level',
    //
    buttons: [
      {
        id: 'Wwwc',
        label: 'Levels',
        icon: 'level',
        //
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'Wwwc' },
      },
      {
        id: 'Invert',
        label: 'Invert',
        icon: 'adjust',
        //
        type: TOOLBAR_BUTTON_TYPES.COMMAND,
        commandName: 'invertViewport',
      },
      {
        id: 'WwwcRegion',
        label: 'ROI Window',
        icon: 'stop',
        //
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'WwwcRegion' },
      },
    ],
  },
  {
    id: 'Pan',
    label: 'Pan',
    icon: 'arrows',
    //
    type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
    commandName: 'setToolActive',
    commandOptions: { toolName: 'Pan' },
  },
  {
    id: 'OrientMenu',
    label: 'Orient',
    icon: 'rotate',
    buttons: [
      {
        id: 'ManualRotate',
        label: 'Manual Rotate',
        icon: 'autorenew',
        class: 'material-icons',
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'Rotate' },
      },
      {
        id: 'RotateRight',
        label: 'Rotate Right',
        icon: 'rotate-right',
        //
        type: TOOLBAR_BUTTON_TYPES.COMMAND,
        commandName: 'rotateViewportCW',
      },
      {
        id: 'FlipH',
        label: 'Flip H',
        icon: 'ellipse-h',
        //
        type: TOOLBAR_BUTTON_TYPES.COMMAND,
        commandName: 'flipViewportHorizontal',
      },
      {
        id: 'FlipV',
        label: 'Flip V',
        icon: 'ellipse-v',
        //
        type: TOOLBAR_BUTTON_TYPES.COMMAND,
        commandName: 'flipViewportVertical',
      },
    ],
  },
  {
    id: 'ViewportOperationSeparator',
    label: 'Separator',
    type: TOOLBAR_BUTTON_TYPES.SEPARATOR,
  },
  {
    id: 'AnnotateMenu',
    label: 'Annotate',
    icon: 'draw',
    class: 'material-icons',
    //
    buttons: [
      {
        id: 'EllipticalRoi',
        label: 'Ellipse',
        icon: 'circle-o',
        class: 'ellipse-icon',
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'EllipticalRoi' },
        options: {
          group: 'ROI',
        },
      },
      {
        id: 'CircleRoi',
        label: 'Circle',
        icon: 'circle-o',
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'CircleRoi' },
        options: {
          group: 'ROI',
        },
      },
      {
        id: 'RectangleRoi',
        label: 'Rectangle',
        icon: 'square-o',
        //
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'RectangleRoi' },
        options: {
          group: 'ROI',
        },
      },
      {
        id: 'FreehandRoi',
        label: 'Freehand',
        icon: 'freehand',
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'FreehandRoi' },
        options: {
          group: 'ROI',
        },
      },
      {
        id: 'OpenFreehandRoi',
        label: 'Open Freehand',
        icon: 'waves',
        class: 'material-icons',
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'OpenFreehandRoi' },
        options: {
          group: 'ROI',
        },
      },
      {
        id: 'ContourRoi',
        label: 'Contour',
        icon: 'freehand',
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: {
          toolName: 'ContourRoi',
        },
        options: {
          group: 'ROI',
        },
      },
      {
        id: 'FreehandRoiSculptor',
        label: 'Sculpt',
        icon: 'fiber_smart_record',
        class: 'material-icons',
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'FreehandRoiSculptor' },
        options: {
          group: 'ROI',
        },
      },
      {
        id: 'ROISeparator',
        label: 'Separator',
        type: TOOLBAR_BUTTON_TYPES.SEPARATOR,
      },
      {
        id: 'Length',
        label: 'Length',
        icon: 'measure-temp',
        //
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'Length' },
        options: {
          group: 'Measure',
        },
      },
      {
        id: 'Parallel',
        label: 'Parallel',
        icon: 'power_input',
        class: 'material-icons',
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'Parallel' },
      },
      {
        id: 'Bidirectional',
        label: 'Bidirectional',
        icon: 'measure-target',
        //
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'Bidirectional' },
        options: {
          group: 'Measure',
        },
      },
      {
        id: 'Angle',
        label: 'Angle',
        icon: 'angle-left',
        //
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'Angle' },
        options: {
          group: 'Measure',
        },
      },
      {
        id: 'MeasureSeparator',
        label: 'Separator',
        type: TOOLBAR_BUTTON_TYPES.SEPARATOR,
      },
      {
        id: 'ArrowAnnotate',
        label: 'Annotate',
        icon: 'measure-non-target',
        //
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'ArrowAnnotate' },
        options: {
          group: 'SinglePoint',
        },
      },
      {
        id: 'SinglePointSeparator',
        label: 'Separator',
        type: TOOLBAR_BUTTON_TYPES.SEPARATOR,
      },
      {
        id: 'Eraser',
        label: 'Eraser',
        icon: 'eraser',
        //
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'Eraser' },
        options: {
          group: 'Trash',
        },
      },
      {
        id: 'Clear',
        label: 'Clear',
        icon: 'trash',
        //
        type: TOOLBAR_BUTTON_TYPES.COMMAND,
        commandName: 'clearAnnotations',
        options: {
          group: 'Trash',
        },
      },
    ],
  },
  {
    id: 'AnnotateSeparator',
    label: 'Separator',
    type: TOOLBAR_BUTTON_TYPES.SEPARATOR,
  },
  {
    id: 'SegmentMenu',
    label: 'Segment',
    icon: 'segment',
    class: 'material-icons',
    buttons: [
      {
        id: 'Brush',
        label: 'Manual',
        icon: 'brush',
        class: 'material-icons',
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'Brush' },
      },
      {
        id: 'SmartBrush',
        label: 'Smart CT',
        icon: 'blur_on',
        class: 'material-icons',
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'SmartBrush' },
      },
      {
        id: 'AutoSmartBrush',
        label: 'Auto CT',
        icon: 'blur_circular',
        class: 'material-icons',
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'AutoSmartBrush' },
      },
      {
        id: 'SaveSegmentsAsNIfTI',
        label: 'Save Segments',
        icon: 'upload_file',
        class: 'material-icons',
        type: TOOLBAR_BUTTON_TYPES.COMMAND,
        commandName: 'saveSegments',
        commandOptions: {},
      },
      {
        id: 'BrushSeparator',
        label: 'Separator',
        type: TOOLBAR_BUTTON_TYPES.SEPARATOR,
      },
      {
        id: 'CircleScissors',
        label: 'Circle',
        icon: 'all_out',
        class: 'material-icons',
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'CircleScissors' },
      },
      {
        id: 'RectangleScissors',
        label: 'Rectangle',
        icon: 'crop_16_9',
        class: 'material-icons',
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'RectangleScissors' },
      },
      {
        id: 'FreehandScissors',
        label: 'Freehand',
        icon: 'content_cut',
        class: 'material-icons',
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'FreehandScissors' },
      },
      {
        id: 'ScissorsSeparator',
        label: 'Separator',
        type: TOOLBAR_BUTTON_TYPES.SEPARATOR,
      },
      {
        id: 'ThresholdTool',
        label: 'Threshold Tool',
        icon: 'format_paint',
        class: 'material-icons',
        type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
        commandName: 'setToolActive',
        commandOptions: { toolName: 'ThresholdTool' },
      },
    ],
  },
  {
    id: 'SegmentationSeparator',
    label: 'Separator',
    type: TOOLBAR_BUTTON_TYPES.SEPARATOR,
  },
  {
    id: 'Cine',
    label: 'CINE',
    icon: 'youtube',
    //
    type: TOOLBAR_BUTTON_TYPES.BUILT_IN,
    options: {
      behavior: TOOLBAR_BUTTON_BEHAVIORS.CINE,
    },
  },
  {
    id: 'DragProbe',
    label: 'Probe',
    icon: 'dot-circle',
    //
    type: TOOLBAR_BUTTON_TYPES.SET_TOOL_ACTIVE,
    commandName: 'setToolActive',
    commandOptions: { toolName: 'DragProbe' },
  },
  {
    id: 'Download',
    label: 'Download',
    icon: 'create-screen-capture',
    //
    type: TOOLBAR_BUTTON_TYPES.BUILT_IN,
    options: {
      behavior: TOOLBAR_BUTTON_BEHAVIORS.DOWNLOAD_SCREEN_SHOT,
      togglable: true,
      pull: 'right',
    },
  },
  {
    id: 'Reset',
    label: 'Reset',
    icon: 'reset',
    //
    type: TOOLBAR_BUTTON_TYPES.COMMAND,
    commandName: 'resetViewport',
    options: {
      pull: 'right',
    },
  },
];

export default {
  definitions,
  defaultContext: 'ACTIVE_VIEWPORT::CORNERSTONE',
};
