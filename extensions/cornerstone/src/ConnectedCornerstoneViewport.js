import PropTypes from 'prop-types';
import CornerstoneViewport from 'react-cornerstone-viewport';
import OHIF from '@ohif/core';
import { connect } from 'react-redux';
import throttle from 'lodash.throttle';
import { setEnabledElement } from './state';

import {
  Utils as FlywheelCommonUtils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import { Components as FlywheelCornerstoneInfusionsComponents } from '@flywheel/extension-cornerstone-infusions';
import store from '@ohif/viewer/src/store';

const {
  CustomizedViewportOverlay: FlywheelCustomizedViewportOverlay,
} = FlywheelCornerstoneInfusionsComponents;

const { utils } = OHIF;
const { CornerstoneColormapService } = utils;
const { setViewportActive, setViewportSpecificData } = OHIF.redux.actions;
const { setViewportNotifyCommand, setActiveTool } = FlywheelCommonRedux.actions;
const {
  onAdded,
  onRemoved,
  onModified,
} = OHIF.measurements.MeasurementHandlers;

// TODO: Transition to enums for the action names so that we can ensure they stay up to date
// everywhere they're used.
const MEASUREMENT_ACTION_MAP = {
  added: onAdded,
  removed: onRemoved,
  modified: throttle(event => {
    return onModified(event);
  }, 300),
};

const mapStateToProps = (state, ownProps) => {
  let dataFromStore;

  // TODO: This may not be updated anymore :thinking:
  if (state.extensions && state.extensions.cornerstone) {
    dataFromStore = state.extensions.cornerstone;
  }

  const { viewports = {} } = state;
  const { colormapId } = viewports || {};
  // update colormap service
  CornerstoneColormapService.setColormapId(colormapId);

  // If this is the active viewport, enable prefetching.
  const { viewportIndex } = ownProps; //.viewportData;
  const isActive = viewportIndex === viewports.activeViewportIndex;
  const viewportSpecificData =
    viewports.viewportSpecificData[viewportIndex] || {};

  // CINE
  let isPlaying = false;
  let frameRate = 24;

  if (viewportSpecificData && viewportSpecificData.cine) {
    const cine = viewportSpecificData.cine;

    // cornerstone-react-viewport controls isPlaying from stack point of view
    // isPlaying from temporal point of view is controlled by this app
    isPlaying = cine.isPlaying === true && !cine.isTemporal;
    frameRate = cine.cineFrameRate || frameRate;
  }

  return {
    // layout: viewports.layout,
    isActive,
    className: `viewport-${viewportIndex}`,
    // TODO: Need a cleaner and more versatile way.
    // Currently justing using escape hatch + commands
    // activeTool: activeButton && activeButton.command,
    ...dataFromStore,
    isStackPrefetchEnabled: isActive,
    isPlaying,
    frameRate,
    //stack: viewportSpecificData.stack,
    // viewport: viewportSpecificData.viewport,
    viewportOverlayComponent: FlywheelCustomizedViewportOverlay,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { viewportIndex, dataProtocol = '' } = ownProps;
  const plugin = dataProtocol ? `cornerstone::${dataProtocol}` : 'cornerstone';

  return {
    setViewportActive: () => {
      dispatch(setViewportActive(viewportIndex));
      dispatch(setActiveTool(null, viewportIndex));
    },

    setViewportSpecificData: data => {
      dispatch(setViewportSpecificData(viewportIndex, data));
    },

    /**
     * Our component "enables" the underlying dom element on "componentDidMount"
     * It listens for that event, and then emits the enabledElement. We can grab
     * a reference to it here, to make playing with cornerstone's native methods
     * easier.
     */
    onElementEnabled: event => {
      const enabledElement = event.detail.element;
      setEnabledElement(viewportIndex, enabledElement);
      if (!FlywheelCommonUtils.isCurrentWebImage()) {
        CornerstoneColormapService.bindOnNewImage(
          viewportIndex,
          enabledElement
        );
      }
      dispatch(
        setViewportSpecificData(viewportIndex, {
          // TODO: Hack to make sure our plugin info is available from the outset
          plugin,
        })
      );
      dispatch(setViewportNotifyCommand(plugin, 'update2DViewport'));
    },

    onMeasurementsChanged: (event, action) => {
      return MEASUREMENT_ACTION_MAP[action](event);
    },
  };
};

CornerstoneViewport.propTypes.viewportOverlayComponent = PropTypes.elementType;

const ConnectedCornerstoneViewport = connect(
  mapStateToProps,
  mapDispatchToProps
)(CornerstoneViewport);

export default ConnectedCornerstoneViewport;
