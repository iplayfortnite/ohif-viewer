const TOOL_NAMES = {
  INDICATOR_DISPLAY_TOOL: 'IndicatorDisplayTool',
  SCALE_INDICATOR_TOOL: 'ScaleIndicatorTool',
};

export default TOOL_NAMES;
